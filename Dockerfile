FROM tomcat:8.5-jdk8-openjdk-slim
RUN apt-get update && apt-get install -y libfreetype6
RUN apt-get update && apt-get install -y xorg && apt-get install -y libxrender1 && apt-get install -y libxtst6 && apt-get install -y libxi6
ADD /target/DorbiTarasBack-1.0-SNAPSHOT.war /usr/local/tomcat/webapps
EXPOSE 8085
CMD ["catalina.sh", "run"]

# Establecer la imagen base de Java 8 con Maven 3.6
#FROM maven:3.6.3-jdk-8 as build

# Copiar el archivo .jar al contenedor
#COPY EstandarGell-1.0-SNAPSHOT.jar /usr/src/mymaven/

# Establecer el directorio de trabajo
#WORKDIR /usr/src/mymaven

# Agregar el archivo .jar al repositorio local de Maven
#RUN mvn install:install-file -Dfile=EstandarGell-1.0-SNAPSHOT.jar -DgroupId=com.gell -DartifactId=EstandarGell -Dversion=1.0-SNAPSHOT -Dpackaging=jar

# Copiar los archivos de la aplicación al contenedor
#COPY src /usr/src/mymaven/src
#COPY pom.xml /usr/src/mymaven

# Compilar la aplicación y generar el archivo .war
#RUN mvn package

# Crear un nuevo contenedor con Tomcat
#FROM tomcat:9-jre8-slim

#RUN apt-get update && apt-get install -y libfreetype6
#RUN apt-get update && apt-get install -y xorg && apt-get install -y libxrender1 && apt-get install -y libxtst6 && apt-get install -y libxi6

# Copiar el archivo WAR generado en el contenedor de Tomcat
#COPY --from=build /usr/src/mymaven/target/DorbiTarasBack-1.0-SNAPSHOT.war /usr/local/tomcat/webapps/DorbiTarasBack-1.0-SNAPSHOT.war

# Exponer el puerto 8080
#EXPOSE 8085

# Establecer el comando de ejecución
#CMD ["catalina.sh", "run"]

