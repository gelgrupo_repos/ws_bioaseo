/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

/**
 *
 * @author God
 */
public class EEstadoGenerico
{

  public static final String ACTIVO = "A";
  public static final String ELIMINAR = "E";
  public static final String INACTIVAR = "I";
  public static final String PENDIENTE = "P";
  public static final String BLOQUEADO = "B";
  public static final String SI = "S";
  public static final String NO = "N";
  public static final String TARAS = "T";
  public static final String CERRADO = "C";
  public static final String CERTIFICADO = "CE";
  public static final String LIQUIDACIONES_TARAS = "TR";
  public static final String CALCULADO = "CL";
  public static final String CALCULADO_TA = "CT";
  public static final String DENEGAR = "D";
}
