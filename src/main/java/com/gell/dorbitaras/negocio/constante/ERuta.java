/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

/**
 * Clase encargada de registrar todas las Rutas del sistema
 *
 * @author god
 */
public class ERuta
{

  /**
   * Las rutas que se registran en esta clase es porque no requieren
   * autenticación y debe empezar con la palabra "global"
   */
  public final static class Global
  {

    public static final String INICIO_SESION = "/global/iniciosesion";
    public static final String INICIO_SESION_PRISMA = "/global/iniciosesion/prisma";
    public static final String MENU = "/api/global/menu";
    public static final String FECHA_SISTEMA = "/api/global/fechasistema";
    public static final String ARCHIVO_ADJUNTAR = "/api/global/archivo/adjuntar";
    public static final String ARCHIVO_CONSULTAR = "/api/global/archivo/consultar";
    public static final String ARCHIVO_CONSULTAR_GRANDE = "/api/global/archivo/grande";

  }

  public final static class ConfiguracionUnidad
  {

    public static final String TIPO_CONFIGURACION = "/api/configuracion/tipos/consultar";
    public static final String GUARDAR = "/api/configuracion/guardar";
    public static final String CONSULTAR = "/api/configuracion/consultar";
    public static final String CONSULTAR_UNIDAD_CLASE = "/api/configuracion/consultarunidad";
    public static final String CONSULTAR_UNIDAD_CLASE_PROGRAMA = "/api/configuracion/consultarunidadprograma";
  }

  public final static class Generico
  {

    public static final String CONSULTAR_CONCEPTO = "api/generico/consultar/concepto";
    public static final String CONSULTAR_CONCEPTO_INDICADORES = "api/generico/consultar/concepto/indicadores";
  }

  public final static class RegimenTarifario
  {

    public static final String GUARDAR = "/api/regimentarifario/guardar";
    public static final String CONSULTAR = "/api/regimentarifario/consultar";
    public static final String CONSULTAR_TIPO = "/api/regimentarifario/consultartipo";
    public static final String CONSULTAR_ENTIDAD_EMITE = "/api/regimentarifario/consultarentidademite";

  }

  public final static class AreaPrestacion
  {

    public static final String GUARDAR = "/api/areaprestacion/guardar";
    public static final String CONSULTAR = "/api/areaprestacion/consultar";
    public static final String CONSULTAR_ESTRATOS = "/api/areaprestacion/consultar/estratos";
    public static final String CONSULTAR_PROYECTOS = "/api/proyectos/consultar";
    public static final String CONSULTAR_TIPO = "/api/regimentarifario/consultartipo";
    public static final String CONSULTAR_ENTIDAD_EMITE = "/api/regimentarifario/consultarentidademite";

  }

  public final static class Rutas
  {

    public static final String GUARDAR = "/api/ruta/guardar";
    public static final String CONSULTAR = "/api/ruta/consultar";
    public static final String CONSULTAR_EMPRESAS = "/api/ruta/consultar/empresas";
    public static final String CONSULTAR_CICLO = "/api/ruta/ciclo/consultar";
    public static final String CONSULTAR_RUTAS_RNA= "/api/ruta/rna/certificados";

  }

  public final static class RutaRecoleccion
  {

    public static final String TIPORUTA = "/api/rutarecoleccion/consultar/tiporuta";
    public static final String CONSULTAR_MICRO_RUTA = "/api/rutarecoleccion/consultar/microruta";
    public static final String GUARDAR = "/api/rutarecoleccion/guardar";
    public static final String CONSULTAR = "/api/rutarecoleccion/consultar";

  }

  public final static class Importacion
  {

    public static final String GUARDAR = "/api/importacion/guardar";
    public static final String CONSULTAR = "/api/importacion/consultar";
    public static final String CONSULTAR_TABLA_DESTINO = "/api/importacion/consultartabla";
    public static final String CONSULTAR_DEPENDENCIA = "/api/importacion/consultardependencia";
    public static final String CONSULTAR_CAMPOS = "/api/importacion/consultarcampos";
    public static final String CONSULTAR_ATRIBUTOS = "/api/importacion/consultaratributos";
    public static final String CONSULTAR_EXPRESION = "/api/importacion/consultarexpresion";

  }

  public final static class Periodos
  {

    public static final String GUARDAR = "/api/periodos/guardar";
    public static final String CONSULTAR = "/api/periodos/consultar";
    public static final String CONSULTAR_ANIO = "/api/periodos/anio";
    public static final String CONSULTAR_SEMESTRE = "/api/periodos/semestre";
    public static final String CONSULTAR_MESES = "/api/periodos/meses";
    public static final String CONSULTAR_PERIODOS_SEMESTRALES_AREA = "/api/periodos/consultar/periodos/semestrales/area";
    public static final String CONSULTAR_SEMESTRES_AREA = "/api/periodos/consultar/semestres/area";
    public static final String CONSULTAR_SEMESTRES_REGIMEN = "/api/periodos/consultar/semestres/regimen";
    public static final String CERRAR_SEMESTRE = "/api/periodos/cerrar/semestre";
    public static final String CONSULTAR_MESES_PERIODO = "/api/periodos/semestre/meses";
  }

  /**
   * Gestión de contactos y/o terceros
   */
  public static final class Terceros
  {

    public static final String CONSULTAR_TERCERO_ENTIDAD = "/api/terceros/consultar/entidad";
    public static final String CONSULTAR_TIPO_RUTA = "/api/ruta/consultar/tiporuta";
    public static final String CONSULTAR_TERCERO_APROVECHADOR = "/api/terceros/consultar/aprovechador";

  }

  public static final class Variables
  {

    public static final String GUARDAR_VARIABLES_PERIODOS = "/api/variables/guardar/variables/periodos";
    public static final String CONSULTAR_VARIABLES_PERIODOS = "/api/variables/consultar/variables/periodos";
    public static final String CONSULTAR_VARIABLES_PERIODOS_APROVECHAMIENTO = "/api/variables/consultar/variables/periodos/aprovechamiento";
    public static final String GUARDAR_VARIABLES_PERIODOS_APROVECHAMIENTO = "/api/variables/guardar/variables/periodos/aprovechamiento";
    public static final String CONSULTAR_VARIABLES_APROVECHAMIENTO_PENDIENTE = "/api/variables/consultar/variables/aprovechamiento/pendiente";
    public static final String CONSULTAR_VARIABLES_PENDIENTES = "/api/variables/consultar/variables/pendientes";
    public static final String CONSULTAR_VARIABLES_CALCULADAS = "/api/variables/consultar/variables/calculadas";
    public static final String CONSULTAR_VARIABLES_BASE_CALCULO = "/api/variables/consultar/variables/base";
    public static final String CERTIFICAR_VARIABLES_CALCULADAS = "/api/variables/certificar/variables/calculadas";
    public static final String GUARDAR_VARIABLES_PERIODOS_MICRO = "/api/variables/guardar/variables/periodos/micro";
     public static final String CONSULTAR_PORCENTAJES_PRIMERO_SEGUNDO = "/api/variables/consultar/porcentajes";
  }

  public static final class Liquidacion
  {

    public static final String CONSULTAR = "/api/liquidacion/consultar";
    public static final String CALCULAR_BALANCE_MASAS = "/api/liquidacion/calcular/balance/masas";
    public static final String CALCULAR_VARIABLES = "/api/liquidacion/calcular/variables";
  }
  
  public static final class GestionActualizacionCostos
  {
      public static final String OBTENER_VARIACIONES = "/api/gestion/costos/obtenervariaciones";
      public static final String INSERTAR_VARIACIONES = "/api/gestion/costos/insertarvariacion";
      public static final String CONSULTAR_CONCEPTO_INDICADOR_PRODUCTIVIDAD = "api/getion/consultar/concepto/indicador/productividad";
      public static final String CAMBIO_BASE_VARIACIONES = "/api/gestion/costos/cambiobasevariaciones";
      public static final String OBTENER_VARIACIONES_TOTALES = "/api/gestion/costos/obtenervariacionestotales";
  
  }        

    public static final class RecalculoAprovechamiento
  {
      public static final String OBTENER_HISTORICO_TONELADAS = "/api/recalculo/aprovechamiento/obtenertoneladas";
      public static final String INSERTAR_TONELADAS = "/api/recalculo/aprovechamiento/insertartoneladas";
      public static final String VALIDA_TA = "/api/recalculo/aprovechamiento/validarTA";
      public static final String CALCULA_PRIMER_TA = "/api/recalculo/aprovechamiento/calculaPrimerTA";
      public static final String RECALCULAR = "/api/recalculo/aprovechamiento/recalcular";
      public static final String OBTENER_CONCEPTOS = "/api/recalculo/aprovechamiento/obtenerConcepto";
      public static final String OBTENER_CONCEPTOS_CALCULADOS = "/api/recalculo/aprovechamiento/obtenerConceptoCalculado";
      public static final String OBTENER_PORCENTAJES_CALCULADOS = "/api/recalculo/aprovechamiento/porcentajesCalculados";
      public static final String OBTENER_HISTORICO = "/api/recalculo/aprovechamiento/obtenerHistoricoRecalculoidPeriodo";
      public static final String OBTENER_RECALCULO_PORCENTAJESPROMEDIOS = "/api/recalculo/consultar/porcentajespromedios";
      public static final String ELIMINAR_TONELADAS = "/api/recalculo/toneladas/eliminar";
      
  } 
      public static final class RecalculoVariables
  {
      public static final String OBTENER_CONCEPTOS_CONSTANTE = "/api/recalculo/variables/obtenerConceptosConstante";
      public static final String ACTUALIZAR_CONSTANTES = "/api/recalculo/variables/actualizarConstantes";
      public static final String OBTENER_CONCEPTOS_BASE = "/api/recalculo/variables/obtenerConceptosBase";
      public static final String ACTUALIZAR_BASE = "/api/recalculo/variables/actualizarBases";
      public static final String HISTORICO_DEVOLUCIONES = "/api/recalculo/variables/obtenerHistoricoDevoluciones";
      public static final String DEVOLUCIONES_RECALCULAR = "/api/recalculo/variables/recalcular";
      public static final String OBTENER_CONCEPTOS_CALCULADOS = "/api/recalculo/variables/obtenerConceptoCalculado";
      public static final String ACTUALIZAR_HISTORICO_DEVOLUCION = "/api/recalculo/variables/actualizarHistorico";
  }
      public static final class Reportes
  {
      public static final String ESTADO_CUENTA_OT = "/api/reportes/estadocuentaot";
      public static final String REPORTE_ACTUALIZACION_COSTOS = "/api/reportes/actualizacionCostos";
      public static final String REPORTE_VARIABLES_INGRESADAS = "/api/reportes/variablesingresadas";
      public static final String REPORTE_INFORMACION_CONSULTOR = "/api/reportes/informacionconsultor";
      public static final String REPORTE_VALORES_COMPONENTES = "/api/reportes/valorescomponentes";
      public static final String REPORTE_TONELADAS_ASOCIACIONES = "/api/reportes/toneladasasociaciones";
      public static final String REPORTE_VALORES_COMPONENTES_TFS = "/api/reportes/valorescomponentestfs";
      public static final String REPORTE_ACTIVIDADES_LU = "/api/reportes/activiadeslu";   
  }
   

}
