/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

/**
 *
 * @author God
 */
public class EParametro
{

  public static final String FECHA_CORTA = "yyyy-MM-dd";
  public static final String PAIS = "es-CO";
  public static final String UBICACION = "America/Bogota";
  public static final String FECHA_LARGA = "yyyy-MM-dd HH:mm:ss";
  public static final String FORMULA = "F";
  public static final String HORA_CORTA = "HH:mm";
  public static final String HORA = "HH:mm:ss";
  public static final String CICLO_TARAS = "Taras";
  public static final String SEMESTRE = "Semestre";
  public static final int LIQUIDACION_CALCULO_TA = 6176;//5522
  public static final int LIQUIDACION_CALCULO_PORCENTAJES_SIN_DINC = 6177;//5518
  public static final int LIQUIDACION_CALCULO_PORCENTAJES_CON_DINC = 5565;
  public static final int LIQUIDACION_DEVOLUCIONES_SIN_DINC = 5569;
  public static final int LIQUIDACION_DEVOLUCIONES = 5568;
 
//  public static final int LIQUIDACION_CALCULO_TA_AJUSTE = 5522;
//  public static final int LIQUIDACION_CALCULO_TA_AJUSTE_DINC = 5522;
  public static final int LIQUIDACION_APROVECHAMIENO_AJUSTES = 6180;
  public static final int LIQUIDACION_APROVECHAMIENTO_AJUSTES_DINC = 6183;
  public static final int LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES = 6181;
  public static final int LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES_DINC = 6182;
  

}
