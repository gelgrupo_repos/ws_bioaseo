/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

/**
 * Clase que registra todos los identificadores de la tabla de cla_clase
 *
 * @author god
 */
public class EClase
{

  /**
   * Clase para las variables registradas para el proyecto de dorbitaras
   */
  public static final Integer TIPO_REGIMEN = 56;
  public static final Integer TIPO_RUTA = 57;
  public static final Integer TIPO_DOCUMENTOS_GENERALES = 58;
  public static final Integer ENTIDAD_EMISORA = 61;
  public static final Integer CICLOS = 62;
  public static final Integer ESTRATOS = 63;
  public static final Integer SEMESTRES = 89;
  public static final Integer MESES = 90;
  public static final Integer APROVECHADOR = 843;
}
