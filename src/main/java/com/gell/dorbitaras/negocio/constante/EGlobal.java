/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

/**
 *
 * @author god
 */
public class EGlobal
{

  /**
   * Se asigna un identificador a los contactos que se necesita crear un usuario
   */
  public static final int PERIODOS_MENSUALES = 1;
  public static final int PERIODOS_SEMESTRALES = 6;

  public static final class Acciones
  {

    public static final String INSERTAR = "I";
    public static final String ELIMINAR = "E";
    public static final String MODIFICAR = "M";
    public static final String NO_APLICA = "N";
  }

  public static final class Concepto
  {

    public static final String TIPO_FORMULA = "F";
    public static final String TIPO_VALOR = "V";
    public static final String NO_APLICA = "N";
    public static final String INFORMATIVO = "I";

    public static final class Formula
    {

      public static final String CONCEPTO = "con";
      public static final String FUNCION = "fun";
      public static final String PARENTESIS_ABRE = "parAbre";
      public static final String PARENTESIS_CIERRA = "parCierra";
    }

    public static final class BalanceMasas
    {

      public static final String QZ = "qz-";
      public static final String QBL = "qbl-";
      public static final String QLUz = "qluz-";
      public static final String QNAz = "qnaz-";
      public static final String QAz = "qaz-";
    }

  }
}
