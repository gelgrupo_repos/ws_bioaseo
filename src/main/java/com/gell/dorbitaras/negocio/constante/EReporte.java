package com.gell.dorbitaras.negocio.constante;

import com.gell.estandar.plantilla.IGenericoReporte;

/**
 *
 * @author God
 */
public enum EReporte implements IGenericoReporte {
  REPORTE_ESTADO_CUENTA_OT(
          "/reportes/reporteestadocuentaot.jasper",
          "Reporte estado de cuenta opcion tarifaria"),
   REPORTE_ACTUALIZACION_COSTOS(
           "/reportes/reporteactualizacioncostos.jasper",
           "Reporte Actualizacion de Costos"),
   REPORTE_VARIABLES_INGRESADAS(
           "/reportes/ReporteVariablesIngresadas.jasper",
           "Reporte Variables Ingresadas"),
   REPORTE_INFORMACION_CONSULTOR(
           "/reportes/informacionconsultortaras.jasper",
           "Reporte Informacion al Consultor"),
   REPORTE_VALORES_COMPONENTES(
           "/reportes/componentes_de_tarifa_taras.jasper",
           "Reporte Tarifa por Producción"),
   REPORTE_TONELADAS_ASOCIACIONES(
           "/reportes/toneladas_aprovechamiento.jasper",
           "Reporte Toneladas de aprovechamiento"),
   REPORTE_ACTIVIDADES_LIMPIEZA_URBANA(
           "/reportes/actividades_limpieza_urbana.jasper",
           "Reporte de Costos y Provisión Fiducia"),
   REPORTE_VALORES_COMPONENTES_TFS(
           "/reportes/componentes_de_tarifa_taras_produccion.jasper",
           "Reporte Tarifa por Componente"),
   EJEMPLO("", "");
  private final String nombre;
  private final String descripcion;

  private EReporte(String nombre, String descripcion)
  {
    this.nombre = nombre;
    this.descripcion = descripcion;
  }

  @Override
  public String getDescripcion()
  {
    return descripcion;
  }

  @Override
  public String getNombre()
  {
    return nombre;
  }

}
