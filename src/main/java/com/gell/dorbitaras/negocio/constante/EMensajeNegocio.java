/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.constante;

import com.gell.estandar.plantilla.IGenericoMensaje;

/**
 *
 * @author god
 */
public enum EMensajeNegocio implements IGenericoMensaje
{
  NO_RESULTADOS(0, "No se encontraron resultados "),
  OK(1, "Petición ejecutada correctamente"),
  ERROR(-1, "Error al procesar la petición"),
  ERROR_SESION_EXPIRADO(-2, "La sesión ha expirado"),
  ERROR_TOKEN_CORRUPTO(-3, "El token es incorrecto"),
  ERROR_FATAL(-4, "Error inesperado, comuníquese con el administrador del sistema "),
  ERROR_JSON(-5, "Error al converitr al JSON"),
  ERROR_NOMBRE_APLICACION(-6, "Error el nombre de la aplicación es obligatorio"),
  ERROR_NUAP(-7, "Error el código NUAP ya existe"),
  ERROR_NUSD(-8, "Error el código NUSD ya existe"),
  ERROR_EVALUAR_FUNCION(-9, "Error al evaluar la fórmula del concepto __COMPLEMENTO__"),
  ERROR_CONCEPTO_RELACIONADO(-10, "Error el concepto relacionado no se encuentra  (__COMPLEMENTO__) "),
  ERROR_CONCEPTO_LIQUIDADO(-11, "Error al calcular el concepto __COMPLEMENTO__ "),
  ERROR_CONCEPTO_NO_ENCONTRADA(-12, "Error no hay ningua variable que tenga la abreviatura __COMPLEMENTO__ "),
  ERROR_UNIDAD_CODIGO(-13, "Error el código ya existe"),
  ERROR_CONCEPTO_NO_ENCONTRADO(-14, "Error no hay ningún concepto que tenga la abreviatura __COMPLEMENTO__ "),
  ERROR_CONCEPTO_RANGOS(-15, "Error el concepto __COMPLEMENTO__ no se encontraron rangos "),
  ERROR_CONCEPTO_RANGOS_NULO(-16, "Error el concepto __COMPLEMENTO__ no tiene valor ni fórmula"),
  ERROR_CONCEPTO_NULO(-17, "Error el concepto __COMPLEMENTO__ es obligatorio "),
  ERROR_ESTADO_VARIABLE(-18, "Error al actualizar el estado de la variable"),
  ERROR_VARIABLES_CALCULADAS(-19, "No existen variables calculadas"),
  ERROR_PERIODOS_INCONSISTENTES(-20, "El periodo anterior se encuentra inconsistente"),
  ERROR_VARIABLES_CERTIFICADAS(-21, "Ya se encuentran registradas variables en estado certificado"),
  ERROR_VARIABLES_SIN_CERTIFICAR(-22, "Se encontraron variables sin certificar"),
  ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO_PADRE(-23, "Se encontraron variables sin certificar en el periodo padre"),
  ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO(-24, "Se encontraron variables sin certificar en el periodo __COMPLEMENTO__"),
  ERROR_SEMESTRE_SIGUIENTE(-25, "No se ha parametrizado el siguiente semestre"),
  ERROR_MESES_SEMESTRE_SIGUIENTE(-26, "Se deben parametrizar todos los meses del siguiente semestre"),
  ERROR_PETICION_GRANDE(-27, "Error el tamaño del archivo es demasiado grande"),
  ERROR_PERMISO(-45, "No tiene permiso al programa solicitado"),
  ERROR_NOMBRE_AREAPRESTACION(-46, "Ya se encuentra agregada un área de prestación con el nombre __COMPLEMENTO__"),
  ERROR_NUAP_AREAPRESTACION(-47, "Ya se encuentra agregada un área de prestación con el nuap __COMPLEMENTO__"),
  ERROR_NUSD_AREAPRESTACION(-48, "Ya se encuentra agregada un área de prestación con el nusd __COMPLEMENTO__"),
  ERROR_NOMBRE_REGIMEN(-49, "Ya se encuentra agregado un régimen tarifario con el nombre __COMPLEMENTO__"),
  ERROR_VARIABLES_INICIALES(-50, "No se encontrarón variables iniciales."),
  ERROR_NO_CALCULO_NORMAL(-51, "Por favor realice un calculo semestral antes __COMPLEMENTO__");


  /**
   * Código del error
   */
  private final int codigo;
  /**
   * Mensaje del evento
   */
  private final String mensaje;

  private EMensajeNegocio(int codigo, String mensaje)
  {
    this.codigo = codigo;
    this.mensaje = mensaje;
  }

  @Override
  public int getCodigo()
  {
    return codigo;
  }

  @Override
  public String getMensaje()
  {
    return mensaje;
  }

}
