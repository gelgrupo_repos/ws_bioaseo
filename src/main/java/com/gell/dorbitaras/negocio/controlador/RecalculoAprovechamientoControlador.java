/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.GestionVariablesServicio;
import com.gell.dorbitaras.negocio.servicio.RecalculoAprovechamientoServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jrangel
 */
@RestController
public class RecalculoAprovechamientoControlador extends GenericoControlador{
    @Autowired
    private RecalculoAprovechamientoServicio recalculoAprovechamientoServicio;
    @Autowired
    private GestionVariablesServicio variablesServicio;

    /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idArea Identificador del área de prestación.
     * @param idPeriodo Identificador del periodo.
     * @param idAsociacion Identificador de asociacion.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_HISTORICO_TONELADAS)
    public RespuestaDTO obtenerListadoToneladas(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idAsociacion") Long idAsociacion
    )
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio
                    .obtenerListadoToneladas(idArea, idPeriodo, idAsociacion));
    }
    
    
     /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idArea Identificador del área de prestación.
     * @param idPeriodo Identificador del periodo.
     * @param idAsociacion Identificador de asociacion.
     * @param valor valor a registrar.
     * @param numeroActualizacion numero actualización.
     * @param idPeriodoPadre periodo padre.
     * @param observacion descripción del valor a insertar.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.INSERTAR_TONELADAS)
    public RespuestaDTO insertarToneladas(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idAsociacion") Long idAsociacion,
            @JsonArgumento("valor") Double valor,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion,
            @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre,
            @JsonArgumento("observacion") String observacion,
            @JsonArgumento("comercializacion") Boolean comercializacion
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.insertarToneladas
        (idArea, idPeriodo, idAsociacion, valor,numeroActualizacion,idPeriodoPadre, observacion,comercializacion));
    }
    
    /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idPeriodo Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.VALIDA_TA)
    public RespuestaDTO verificarPrimerTA(
            @JsonArgumento("idPeriodo") Integer idPeriodo
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.validarTA(idPeriodo, "R"));
    }
    
     /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idPeriodo Identificador del periodo.
     * @param idPeriodoPadre Identificador del periodo padre.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
   /* @PostMapping(ERuta.RecalculoAprovechamiento.CALCULA_PRIMER_TA)
    public RespuestaDTO calculoPrimerTA(
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre
    )throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.calculoPrimerQA(idPeriodoPadre, idPeriodo));
    }*/
    
      /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idArea Identificador del periodo.
     * @param idPeriodo Identificador del periodo.
     * @param idPeriodoPadre  Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.RECALCULAR)
    public RespuestaDTO recalcular(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.recalcular(idArea,idPeriodoPadre, idPeriodo));
    }
    
    /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_CONCEPTOS)
    public RespuestaDTO obtenerConceptos(
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.obtenerConceptos());
    }
    
        /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param numeroActualizacion numero actualizacion.
     * @param idPeriodo Identificador del periodo.
     * 
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_CONCEPTOS_CALCULADOS)
    public RespuestaDTO obtenerConceptosCalculados(
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion,
            @JsonArgumento("idPeriodo") Integer idPeriodo
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio
                        .obtenerConceptosCalculados(numeroActualizacion, idPeriodo));
    }
    
            /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     *
     * @param idPeriodoPadre Identificador del periodo.
     * 
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_PORCENTAJES_CALCULADOS)
    public RespuestaDTO obtenerPorcentajesAsociacion(
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion
    )
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio
                        .obtenerPorcentajesAsociacion(idPeriodo, numeroActualizacion));
    }
    
    
        /**
     * Método encargado de obtener historico de tonealadas para una area, periodo y
     * asociacion dada
     * @param idPeriodo Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_HISTORICO)
    public RespuestaDTO obtenerHistoricos(
        @JsonArgumento("idPeriodo") Integer idPeriodo
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoAprovechamientoServicio.obtenerHistorico(idPeriodo));
    }
    
    @PostMapping(ERuta.RecalculoAprovechamiento.OBTENER_RECALCULO_PORCENTAJESPROMEDIOS)
    public RespuestaDTO obtenerPorcentajesPromedios(
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion
    )
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(variablesServicio.obtenerPorcentajesprimerysegundo(idPeriodo, numeroActualizacion,"R"));
    }
    
    @PostMapping(ERuta.RecalculoAprovechamiento.ELIMINAR_TONELADAS)
    public RespuestaDTO EliminarTonelada(
            @JsonArgumento("idTonelada") Integer idTonelada
    )
        throws AplicacionExcepcion {
        recalculoAprovechamientoServicio.eliminarTonelada(idTonelada);
        return new RespuestaDTO()
                .setDatos(true);
    }
    
}
