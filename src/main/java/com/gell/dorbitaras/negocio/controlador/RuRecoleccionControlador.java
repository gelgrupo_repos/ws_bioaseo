/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.RuRecoleccionServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class RuRecoleccionControlador extends GenericoControlador
{

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private RuRecoleccionServicio ruRecoleccionServicio;

  /**
   * Método que controla los datos al guardar
   *
   * @param rutRecoleccion registro a guardar Ruta Recolección
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.RutaRecoleccion.GUARDAR)
  public RespuestaDTO guardar(@RequestBody RureRutrecoleccion rutRecoleccion)
          throws AplicacionExcepcion
  {
    ruRecoleccionServicio.guardar(rutRecoleccion);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta rutas
   *
   * @param tipoRuta macro - micro
   * @return Respuesta con la lista de las rutas dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.RutaRecoleccion.TIPORUTA)
  public RespuestaDTO consultarEstratos(
          @JsonArgumento("tipoRuta") String tipoRuta)
          throws AplicacionExcepcion
  {

    List<RutRuta> listaMacro = ruRecoleccionServicio.consultarMacro(tipoRuta);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaMacro);

  }

  /**
   * Método encargado de consultar las micro rutas que no estan asociadas a una
   * macroruta.
   *
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.RutaRecoleccion.CONSULTAR_MICRO_RUTA)
  public RespuestaDTO consultarMicroRuta()
          throws AplicacionExcepcion
  {
    List<RutRuta> listaMicroRuta = ruRecoleccionServicio.consultarMicro();
    return new RespuestaDTO().setDatos(listaMicroRuta);
  }

  /**
   * Consulta Ruta Recolección
   *
   * @param areaPrestacion Área de prestación
   * @param macroRuta Macro Ruta
   * @return Respuesta con la lista de las rutas dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.RutaRecoleccion.CONSULTAR)
  public RespuestaDTO consultarRecoleccion(
          @JsonArgumento("areaPrestacion") Integer areaPrestacion,
          @JsonArgumento("macroRuta") Integer macroRuta)
          throws AplicacionExcepcion
  {

    List<RureRutrecoleccion> listaRutaRecoleccion
            = ruRecoleccionServicio.consultarRecoleccion(areaPrestacion, macroRuta);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaRutaRecoleccion);

  }
}
