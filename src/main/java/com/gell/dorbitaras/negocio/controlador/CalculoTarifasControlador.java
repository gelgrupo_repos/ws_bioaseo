/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.CalculoTarifasServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Desarrollador
 */
@RestController
public class CalculoTarifasControlador extends GenericoControlador
{

  @Autowired
  private CalculoTarifasServicio calculoServicio;

  /**
   * Método encargado de realizar el calculo del balance de masas.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Liquidacion.CALCULAR_BALANCE_MASAS)
  public RespuestaDTO calcularBalanceMasas(
          @JsonArgumento("idArea") Integer idArea,
          @JsonArgumento("idPeriodo") Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    List<ValorConceptoDTO> lista = calculoServicio.calcularBalanceMasas(idArea, idPeriodo);
    return new RespuestaDTO().setCodigo(10001)
                              .setMensaje("Calculo de Balance de Masas Correcto");
  }

  /**
   * Método encargado de realizar el calculo de las tarifas semestrales.
   *
   * @param idArea Identificador del área de prestación
   * @param idPeriodo Identificador del periodo.
   * @param idPeriodoPadre Identificador del periodo padre.
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Liquidacion.CALCULAR_VARIABLES)
  public RespuestaDTO calcularVariables(
          @JsonArgumento("idArea") Integer idArea,
          @JsonArgumento("idPeriodo") Integer idPeriodo,
          @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre
  )
          throws AplicacionExcepcion
  {
    List<ValorConceptoDTO> lista = calculoServicio.calcularVariables(idArea, idPeriodo, idPeriodoPadre);
    return new RespuestaDTO().setDatos(lista);
  }
}
