/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.*;
import com.gell.dorbitaras.negocio.servicio.EmpresaServicio;
import com.gell.dorbitaras.negocio.servicio.ReporteServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.Base64;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class ReportesControlador extends GenericoControlador {

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private ReporteServicio reporteServicio;

  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Reportes.ESTADO_CUENTA_OT)
  public RespuestaDTO generarReporteDispersion(
          @JsonArgumento("suscripcion") Long idsuscripcion)
          throws AplicacionExcepcion
  {
    System.out.print("Entrando");
    byte[] archivo = reporteServicio.ReporteEstadoCuenta(idsuscripcion);
    String info = Base64.getEncoder().encodeToString(archivo);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
  }
  
  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_ACTUALIZACION_COSTOS)
   public RespuestaDTO generarReporteActualizacionCostos()
           throws AplicacionExcepcion
   {
     System.out.print("Entrando");
     byte[] archivo = reporteServicio.ReporteActualizacionCostos();
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }
   
  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_VARIABLES_INGRESADAS)
   public RespuestaDTO generarReporteVariablesIngresadas()
           throws AplicacionExcepcion
   {
     System.out.print("Entrando");
     byte[] archivo = reporteServicio.ReporteActualizacionVariablesIngresadas();
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }
   
  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_INFORMACION_CONSULTOR)
   public RespuestaDTO generarReporteInformacionConsultor(
        @JsonArgumento("periodo") Integer periodo
        )
           throws AplicacionExcepcion
   {
     byte[] archivo = reporteServicio.ReporteInformacionConsultor(periodo);
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }

  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_VALORES_COMPONENTES)
   public RespuestaDTO generarReporteValoresComponentes(
        @JsonArgumento("fecha") String fecha
        )
           throws AplicacionExcepcion
   {
     System.out.print("fecha->"+fecha);
     byte[] archivo = reporteServicio.ReporteValoresComponente(fecha);
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }



  /**
   * generacion de reporte de estado de cuenta de opcion tarifaria
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_TONELADAS_ASOCIACIONES)
   public RespuestaDTO generarReporteToneladasAsociaciones()
           throws AplicacionExcepcion
   {
     byte[] archivo = reporteServicio.ReporteToneladasAsociaciones();
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }
   
  /**
   * 
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_VALORES_COMPONENTES_TFS)
   public RespuestaDTO generarReporteValoresComponentesTFS(
        @JsonArgumento("fecha") String fecha
        )
           throws AplicacionExcepcion
   {
     System.out.print("fecha->"+fecha);
     byte[] archivo = reporteServicio.ReporteValoresComponenteTFS(fecha);
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }
   
  /**
   * 
   *
   * @param suscripcion id de la suscripcion
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
   @PostMapping(ERuta.Reportes.REPORTE_ACTIVIDADES_LU)
   public RespuestaDTO generarReporteActividadesLU(
        @JsonArgumento("fecha") String fecha
        )
           throws AplicacionExcepcion
   {
     System.out.print("fecha->"+fecha);
     byte[] archivo = reporteServicio.ReporteActividadesLU(fecha);
     String info = Base64.getEncoder().encodeToString(archivo);
     return new RespuestaDTO(EMensajeNegocio.OK).setDatos(info);
   }

}
