/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.RegimenTarifarioServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.RgtaRgitarifario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author god
 */
@RestController
public class RegimenTarifarioControlador extends GenericoControlador
{

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private RegimenTarifarioServicio regimenServicio;

  /**
   * Método que controla los datos al guardar
   *
   * @param regimen registro a guardar Regimen Tarifario
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.RegimenTarifario.GUARDAR)
  public RespuestaDTO guardar(@RequestBody RgtaRgitarifario regimen)
          throws AplicacionExcepcion
  {
    regimenServicio.guardarRegimen(regimen);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta todos los régimen tarifarios
   *
   * @param nombre consultar por nombre de régimen tarifario
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.RegimenTarifario.CONSULTAR)
  public RespuestaDTO consultar(@JsonArgumento("criterio") String nombre)
          throws AplicacionExcepcion
  {
    List<RgtaRgitarifario> listaRegimen = regimenServicio.consultar(nombre);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaRegimen);

  }
}
