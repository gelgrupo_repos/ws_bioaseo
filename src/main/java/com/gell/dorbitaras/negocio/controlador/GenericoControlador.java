
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.negocio.util.AuditoriaUtil;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.util.List;
import org.springframework.core.NestedRuntimeException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.util.NestedServletException;

/**
 *
 * @author god
 */
public class GenericoControlador
{

  @ExceptionHandler({
    AplicacionExcepcion.class,
    PersistenciaExcepcion.class,
    NegocioExcepcion.class
  })
  @ResponseStatus(HttpStatus.OK)
  public RespuestaDTO controlarError(AplicacionExcepcion e)
  {
    return new RespuestaDTO(e.getCodigo(), e.getMensaje())
            .setDatos(e.getDatos());
  }

  @ResponseStatus(HttpStatus.OK)
  @ExceptionHandler({
    MultipartException.class
  })
  public RespuestaDTO controlarError(MultipartException ex)
  {
    return new RespuestaDTO(EMensajeNegocio.ERROR_PETICION_GRANDE);
  }

  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseStatus(value = HttpStatus.OK)
  public RespuestaDTO handleException(MethodArgumentNotValidException ex)
  {
    List<ObjectError> listaErrores = ex.getBindingResult().getAllErrors();
    StringBuilder mensajes = new StringBuilder();

    for (ObjectError error : listaErrores) {
      mensajes.append(error.getDefaultMessage())
              .append(" ");
    }
    return new RespuestaDTO(EMensajeNegocio.ERROR).setMensaje(mensajes.toString());
  }

  @ExceptionHandler({
    JsonProcessingException.class,
    MismatchedInputException.class,
    JsonMappingException.class
  })
  @ResponseStatus(HttpStatus.OK)
  public RespuestaDTO controlarError(JsonProcessingException ex)
  {
    return new RespuestaDTO(EMensajeNegocio.ERROR_JSON);
  }

  @ExceptionHandler({
    Throwable.class
  })
  @ResponseStatus(HttpStatus.OK)
  public RespuestaDTO controlarError(Throwable ex)
  {
    LogUtil.error(ex);
    return new RespuestaDTO(EMensajeNegocio.ERROR_FATAL);
  }

  public AuditoriaDTO auditoria()
  {
    return AuditoriaUtil.auditoria();
  }}
