/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.LiquidacionServicio;
import com.gell.dorbitaras.persistencia.entidades.LiqLiquidacion;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Desarrollador
 */
@RestController
public class LiquidacionesControlador extends GenericoControlador
{

  /**
   * Clase que contiene la logica del negocio.
   */
  @Autowired
  private LiquidacionServicio liquidacionServicio;

  /**
   * Método encargado de consultas las liquidaciones de tipo tarifas.
   *
   * @return Lista liquidaciones.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Liquidacion.CONSULTAR)
  public RespuestaDTO consultarLiquidacion()
          throws AplicacionExcepcion

  {
    List<LiqLiquidacion> listaLiquidacion = liquidacionServicio.consultarLiquidacion();
    return new RespuestaDTO().setDatos(listaLiquidacion);
  }
}
