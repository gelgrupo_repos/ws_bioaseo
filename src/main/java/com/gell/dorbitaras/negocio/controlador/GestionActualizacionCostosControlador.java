/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.GestionActualizacionCostosServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoIndicadorProductividad;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author maestro
 */
@RestController
public class GestionActualizacionCostosControlador {
    @Autowired
    private GestionActualizacionCostosServicio gestionActualizacionCostosServicio;

    /**
     * Método encargado de obtener variaciones calculadas para costos y factor
     * de productividad
     *
     * @param idArea Identificador del área de prestación.
     * @param idPeriodo Identificador del periodo.
     * @param idConcepto Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.GestionActualizacionCostos.OBTENER_VARIACIONES)
    public RespuestaDTO obtenerVariaciones(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idConcepto") Integer idConcepto
    )
            throws AplicacionExcepcion {
        List<VariacionCostosProductividadDTO> lista = gestionActualizacionCostosServicio
                .obtenerVariaciones(idArea, idPeriodo, idConcepto);
        return new RespuestaDTO().setDatos(lista);
    }
    
    /**
     * Método encargado de obtener variaciones calculadas para costos y factor
     * de productividad
     *
     * @param idArea Identificador del área de prestación.
     * @param idPeriodo Identificador del periodo.
     * @param idConcepto Identificador del periodo.
     * @param idMes Identificador del periodo.
     * @param valorIngresado Identificador del periodo.
     * @param valorProductividad Identificador del periodo.
     * @param tieneProductividad Identificador del periodo.
     * @param valorAcumulado valor acumulado
     * @param idIndicadorVariacion id del indicador de variación
     * @param valorFactorProductividad valor ingresado de productividad
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.GestionActualizacionCostos.INSERTAR_VARIACIONES)
    public RespuestaDTO insertarVariaciones(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idConcepto") Integer idConcepto,
            @JsonArgumento("idMes") Integer idMes,
            @JsonArgumento("valorIngresado") Double valorIngresado,
            @JsonArgumento("valorProductividad") Double valorProductividad,
            @JsonArgumento("tieneProductividad") Boolean tieneProductividad,
            @JsonArgumento("valorAcumulado") Double valorAcumulado,
            @JsonArgumento("idIndicadorVariacion") Integer idIndicadorVariacion,
            @JsonArgumento("valorFactorProductividad") Double valorFactorProductividad
    )
            throws AplicacionExcepcion {
        gestionActualizacionCostosServicio
                .insertarVariaciones(idArea, idPeriodo, idConcepto, idMes, 
                        valorIngresado,valorProductividad, tieneProductividad,
                        valorAcumulado,idIndicadorVariacion,valorFactorProductividad);

        return new RespuestaDTO().setDatos(true);
    }

      /**
   * Método que controla la busqueda de conceptos.
   *
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.GestionActualizacionCostos.CONSULTAR_CONCEPTO_INDICADOR_PRODUCTIVIDAD)
  public RespuestaDTO consultarConceptoIndicadorProductividad(
  )
          throws AplicacionExcepcion
  {
    List<ConConceptoIndicadorProductividad> listaConcepto = gestionActualizacionCostosServicio.consultarConceptoIndicadorProductividad();
    return new RespuestaDTO().setDatos(listaConcepto);
  }
  
      
    /**
     * Método encargado de actualizar el cambio de base en la variación de un período.
     *
     * @param idVariacion Identificador de la variación.
     * @param valorActualizacion Valor a cambiar base.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.GestionActualizacionCostos.CAMBIO_BASE_VARIACIONES)
    public RespuestaDTO cambioBaseVariaciones(
            @JsonArgumento("idVariacion") Integer idVariacion,
            @JsonArgumento("valorActualizacion") Double valorActualizacion
    )
            throws AplicacionExcepcion {
        gestionActualizacionCostosServicio
                .cambioBaseVariaciones(idVariacion, valorActualizacion);
        return new RespuestaDTO().setDatos(true);
    }
    
    /**
     * 
     *
     * @param idPeriodo Identificador del periodo.
     * @param idConcepto Identificador del periodo.
     * @param idArea Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.GestionActualizacionCostos.OBTENER_VARIACIONES_TOTALES)
    public RespuestaDTO verificarPrimerTA(
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idConcepto") Integer idConcepto,
            @JsonArgumento("idArea") Integer idArea
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(gestionActualizacionCostosServicio.obtenerVariacionesTotales(idArea, idPeriodo, idConcepto));
    }
}
