/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.TerceroServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.TerTercero;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author god
 */
@RestController
public class TerceroControlador extends GenericoControlador
{

  @Autowired
  private TerceroServicio terceroServicio;

  /**
   * Consulta todos los terceros
   *
   * @param criterio nombre o documento del tercero a buscar
   * @return Lista de todos los terceros que cumplen con las condiciones
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Terceros.CONSULTAR_TERCERO_ENTIDAD)
  public RespuestaDTO consultarTerceroEntidad(@JsonArgumento("criterio") String criterio)
          throws AplicacionExcepcion
  {
    List<TerTercero> listaTerceros = terceroServicio
            .consultarEntidad(criterio);
    return new RespuestaDTO().setDatos(listaTerceros);
  }

  /**
   * Consulta todos los terceros
   *
   * @param criterio nombre o documento de la ruta a buscar
   * @return Lista de todos las rutas que cumplen con las condiciones
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Terceros.CONSULTAR_TIPO_RUTA)
  public RespuestaDTO consultarTipoRuta(@JsonArgumento("criterio") String criterio)
          throws AplicacionExcepcion
  {
    List<TerTercero> listaTipoRutas = terceroServicio
            .consultarTipoRuta(criterio);
    return new RespuestaDTO().setDatos(listaTipoRutas);
  }

  /**
   * Método encargado de consultar los terceros aprovechadores.
   *
   * @return Lista de terceros.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Terceros.CONSULTAR_TERCERO_APROVECHADOR)
  public RespuestaDTO consultarTerceroAprovechador()
          throws AplicacionExcepcion
  {
    List<TerTercero> listaTerceroAprovechador = terceroServicio
            .consultarTerceroAprovechador();
    return new RespuestaDTO().setDatos(listaTerceroAprovechador);
  }
}
