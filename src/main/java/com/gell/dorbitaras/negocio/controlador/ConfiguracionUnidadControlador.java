/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.ConfiguracionUnidadServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.EstEstructura;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase encargada de encargar todos los párametros del sistema en la tabla de
 * uni_unidad
 *
 * @author god
 */
@RestController
public class ConfiguracionUnidadControlador extends GenericoControlador
{

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private ConfiguracionUnidadServicio unidadServicio;

  /**
   * Servicio que expone para guardar unidad básica la información de la unidad
   *
   * @param unidad registro a guardar unidad básica
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.ConfiguracionUnidad.GUARDAR)
  public RespuestaDTO guardar(@RequestBody UniUnidad unidad)
          throws AplicacionExcepcion
  {
    unidadServicio.guardarUnidadBasica(unidad);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta todas las unidades dependiendo del tipo de estrucutra
   *
   * @param criterio nombre o código de la unidad
   * @param estIderegistro Identificador de la estructura
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.ConfiguracionUnidad.CONSULTAR)
  public RespuestaDTO consultar(
          @JsonArgumento("criterio") String criterio,
          @JsonArgumento("estIderegistro") Long estIderegistro
  )
          throws AplicacionExcepcion
  {

    List<UniUnidad> listaUnidades = unidadServicio.consultar(criterio, estIderegistro);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaUnidades);

  }

  /**
   * Método que consulta todos los tipos de unidades que se van a parametrizar
   *
   * @return
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.ConfiguracionUnidad.TIPO_CONFIGURACION)
  public RespuestaDTO consultarEstructurasConfiguracion()
          throws AplicacionExcepcion
  {
    List<EstEstructura> listaEstructuras = unidadServicio.consultarEstructurasConfiguracion();
    return new RespuestaDTO<>(EMensajeNegocio.OK).setDatos(listaEstructuras);

  }

  /**
   * Consulta todas las unidades de una clase especificada
   *
   * @param idClase Identificador de la clase
   * @param criterio Nombre o parte de la unidad a consultar
   * @return
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.ConfiguracionUnidad.CONSULTAR_UNIDAD_CLASE)
  public RespuestaDTO consultarUnidadClase(
          @JsonArgumento("idClase") Integer idClase,
          @JsonArgumento("criterio") String criterio
  )
          throws AplicacionExcepcion
  {

    List<UniUnidad> listaUnidades = unidadServicio.consultarUnidadPorClase(idClase, criterio);
    return new RespuestaDTO().setDatos(listaUnidades);

  }

  /**
   * Consulta todas las unidades de una clase especificada
   *
   * @param idClase Identificador de la clase
   * @param criterio Nombre o parte de la unidad a consultar
   * @return
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.ConfiguracionUnidad.CONSULTAR_UNIDAD_CLASE_PROGRAMA)
  public RespuestaDTO consultarUnidadClasePrograma(
          @JsonArgumento("idClase") Integer idClase,
          @JsonArgumento("criterio") String criterio)
          throws AplicacionExcepcion
  {
    //Integer idPrograma = Integer.parseInt(auditoria().getParametro("idPrograma"));
    List<ConConcepto> listaUnidades = unidadServicio
            .consultarUnidadProgramaClase(criterio, idClase, 513);
    return new RespuestaDTO().setDatos(listaUnidades);
  }

}
