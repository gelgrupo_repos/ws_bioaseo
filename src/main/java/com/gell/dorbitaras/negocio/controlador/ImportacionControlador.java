/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.*;
import com.gell.dorbitaras.negocio.servicio.ImportacionServicio;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class ImportacionControlador extends GenericoControlador {

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private ImportacionServicio importacionServicio;

  /**
   * Método que controla los datos al guardar
   *
   * @param importar registro a guardar
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.Importacion.GUARDAR)
  public RespuestaDTO guardar(@RequestBody ImpImportar importar)
          throws AplicacionExcepcion
  {
    importacionServicio.insertar(importar);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta todos los datos de importación
   *
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR)
  public RespuestaDTO consultar(
  )
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaImp = importacionServicio.consultar();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaImp);

  }

  /**
   * Consulta todos las tabla destino
   *
   * @return Respuesta con la lista de las tablas destino 
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR_TABLA_DESTINO)
  public RespuestaDTO consultarTablaDestino()
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaTabla = importacionServicio.consultarTablaDestino();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaTabla);

  }
  
    /**
   * Consulta Dependencia Funcional
   *
   * @return Respuesta con la lista de la dependencia Funcional
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR_DEPENDENCIA)
  public RespuestaDTO consultarDependencia()
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaDependencia = importacionServicio.consultarDependencia();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaDependencia);

  }
  
    /**
   * Consulta que lista los atributos vinculados a la tabla que 
   * se seleccione en la dependencia funcional  
   *
   * @return Respuesta con la lista de las tablas destino 
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR_CAMPOS)
  public RespuestaDTO consultarCampos()
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaCampos = importacionServicio.consultarCampos();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaCampos);

  }
  
    /**
   * Consulta que obtiene el listado de campos que tiene una tabla según la tabla
   *
   * @return Respuesta con la lista de las tablas destino 
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR_ATRIBUTOS)
  public RespuestaDTO consultarAtributos()
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaAtributos = importacionServicio.consultarAtributos();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaAtributos);

  }
  
    /**
   * Consulta la expresión regular
   *
   * @return Respuesta con la lista de las tablas destino 
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Importacion.CONSULTAR_EXPRESION)
  public RespuestaDTO consultarExpresion()
          throws AplicacionExcepcion
  {

    List<ImpImportar> listaExpresion = importacionServicio.consultarExpresion();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaExpresion);

  }
}
