/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.GestionVariablesServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.dto.VarcBaseRepuestaDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoDTO;
import com.gell.dorbitaras.persistencia.dto.VrtaVarteraprDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.VarcVarcalculo;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.dorbitaras.persistencia.entidades.VrmrVarmicroruta;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarterapr;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Clase que controla el ingreso de variables para el area de prestación,
 * periodo y concepto.
 *
 * @author Desarrollador
 */
@RestController
public class GestionVariablesControlador extends GenericoControlador
{

  /**
   * Objeto que contiene la logica del negocio.
   */
  @Autowired
  private GestionVariablesServicio variablesServicio;

  /**
   * Método que controla la busqueda de conceptos.
   *
   * @param uniLiquidacion Identificador de la liquidación
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Generico.CONSULTAR_CONCEPTO)
  public RespuestaDTO consultarConcepto(
          @JsonArgumento("uniLiquidacion") Integer uniLiquidacion
  )
          throws AplicacionExcepcion
  {
    List<ConConcepto> listaConcepto = variablesServicio.consultarConcepto(uniLiquidacion);
    return new RespuestaDTO().setDatos(listaConcepto);
  }
  
  /**
   * Método que controla la busqueda de conceptos.
   *
   * @param uniLiquidacion Identificador de la liquidación
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Generico.CONSULTAR_CONCEPTO_INDICADORES)
  public RespuestaDTO consultarConceptoIndicadores()
          throws AplicacionExcepcion
  {
    List<ConConcepto> listaConcepto = variablesServicio.consultarConceptoIndicadores();
    return new RespuestaDTO().setDatos(listaConcepto);
  }

  /**
   * Método encargado de consultar si ya existe una variable.
   *
   * @param periodo Identificador del periodo.
   * @param areaPrestacion Identificador del área de prestación.
   * @param concepto Identificador del concepto.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_PERIODOS)
  public RespuestaDTO consultarVariablesPeriodos(
          @JsonArgumento("periodo") Integer periodo,
          @JsonArgumento("areaPrestacion") Integer areaPrestacion,
          @JsonArgumento("concepto") Integer concepto
  )
          throws AplicacionExcepcion
  {
    List<VarcVarcalculo> listaDatos
            = variablesServicio.consultarVariablesPeriodos(periodo, areaPrestacion, concepto);
    return new RespuestaDTO().setDatos(listaDatos);
  }

  /**
   * Método encargado de guardar las variables en la tabla varc_varcalculo
   *
   * @param listaVarCalculo Datos de las variables a guardar.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.GUARDAR_VARIABLES_PERIODOS)
  public RespuestaDTO guardarVariablesPeriodo(
          @RequestBody List<VarcVarcalculo> listaVarCalculo
  )
          throws AplicacionExcepcion
  {
    variablesServicio.guardarVariablesPeriodo(listaVarCalculo);
    return new RespuestaDTO();
  }
  
  /**
   * Método encargado de guardar las variables en la tabla varmr_microruta para 
   * el tema de indicadores de calidad
   *
   * @param listavarmrmicro Datos de las variables a guardar.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.GUARDAR_VARIABLES_PERIODOS_MICRO)
  public RespuestaDTO guardarVariablesPeriodoMicroRuta(
          @RequestBody List<VrmrVarmicroruta> listaVrmrVarmicroruta
  )
          throws AplicacionExcepcion
  {
    variablesServicio.guardarVariablesPeriodoMicro(listaVrmrVarmicroruta);
    return new RespuestaDTO();
  }

  /**
   * Método encargado de consultar las variables en estado pendiente.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_APROVECHAMIENTO_PENDIENTE)
  public RespuestaDTO consultarVariablesAprovechamientoPendientes(
          @JsonArgumento("periodo") Integer periodo,
          @JsonArgumento("tercero") Integer tercero,
          @JsonArgumento("areaPrestacion") Integer areaPrestacion
  )
          throws AplicacionExcepcion
  {
    List<VrtaVarteraprDTO> listaDatos
            = variablesServicio.consultarVariablesAprovechamientoPendientes(periodo, areaPrestacion, tercero);
    return new RespuestaDTO().setDatos(listaDatos);
  }

  /**
   * Método encargado de consultar las variables en estado pendiente según área
   * de prestación y periodo.
   *
   * @param periodo Identificador del periodo.
   * @param areaPrestacion Identificador del área de prestación.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_PENDIENTES)
  public RespuestaDTO consultarVariablesPendientes(
          @JsonArgumento("periodo") Integer periodo,
          @JsonArgumento("areaPrestacion") Integer areaPrestacion
  )
          throws AplicacionExcepcion
  {
    List<VarcVarcalculoDTO> listaDatos
            = variablesServicio.consultarVariablesPendientes(periodo, areaPrestacion);
    return new RespuestaDTO().setDatos(listaDatos);
  }

  /**
   * Método encargado de consultar si ya existe una variable con aprovechador.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @param concepto Identificador de la variable.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_PERIODOS_APROVECHAMIENTO)
  public RespuestaDTO consultarVariablesAprovechamiento(
          @JsonArgumento("periodo") Integer periodo,
          @JsonArgumento("tercero") Integer tercero,
          @JsonArgumento("areaPrestacion") Integer areaPrestacion,
          @JsonArgumento("concepto") Integer concepto
  )
          throws AplicacionExcepcion
  {
    List<VrtaVarterapr> listaDatos
            = variablesServicio.consultarVariablesAprovechamiento(periodo, areaPrestacion, tercero, concepto);
    return new RespuestaDTO().setDatos(listaDatos);
  }

  /**
   * Método encargado de guardar las variables en la tabla vrta_varterapr.
   *
   * @param listaVrta Datos de las variables a guardar.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.GUARDAR_VARIABLES_PERIODOS_APROVECHAMIENTO)
  public RespuestaDTO guardarVariablesPeriodosAprovechamiento(
          @RequestBody List<VrtaVarterapr> listaVrta
  )
          throws AplicacionExcepcion
  {
    variablesServicio.guardarVariablesPeriodoAprovechamiento(listaVrta);
    return new RespuestaDTO();
  }

  /**
   * Método encargado de consultar las variables calculadas.
   *
   * @param areaPrestacion Identificador del área de prestación.
   * @param periodo Identificador del periodo.
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_CALCULADAS)
  public RespuestaDTO consultarVariablesCalculadas(
          @JsonArgumento("idArea") Integer areaPrestacion,
          @JsonArgumento("idPeriodo") Integer periodo
  )
          throws AplicacionExcepcion
  {
    List<VarprVarperreg> listaDatos = variablesServicio
            .consultarVariablesCalculadas(areaPrestacion, periodo);
    return new RespuestaDTO().setDatos(listaDatos);
  }

  /**
   * Método encargado de consultar las conceptos base para el calculo.
   *
   * @param idLiquidacion Identificador de la liquidación.
   * @param idPeriodo Identificador del periodo.
   * @param areaPrestacion Identificador del area de Prestacion.
   * @return RespuestaDTO.
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CONSULTAR_VARIABLES_BASE_CALCULO)
  public RespuestaDTO consultarVariablesBaseCalculo(
          @JsonArgumento("uniLiquidacion") Integer idLiquidacion,
          @JsonArgumento("idPeriodo") Integer idPeriodo , 
          @JsonArgumento("idArea") Integer areaPrestacion
  )
          throws AplicacionExcepcion
  {
    VarcBaseRepuestaDTO datos
            = variablesServicio.consultarVariablesBaseCalculo(idLiquidacion, idPeriodo ,areaPrestacion );
    return new RespuestaDTO().setDatos(datos);
  }

  /**
   * Método encargado de certificar las variables calculadas por área de
   * prestación y periodo.
   *
   * @param idArea Identificador del área de prestación
   * @param idPeriodo Identificador del periodo
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Variables.CERTIFICAR_VARIABLES_CALCULADAS)
  public RespuestaDTO certificarVariablesCalculadas(
          @JsonArgumento("idArea") Integer idArea,
          @JsonArgumento("idPeriodo") Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    variablesServicio.certificarVariablesCalculadas(idArea, idPeriodo);
    return new RespuestaDTO();
  }
  
   
   @PostMapping(ERuta.Variables.CONSULTAR_PORCENTAJES_PRIMERO_SEGUNDO)
    public RespuestaDTO obtenerPorcentajesAsociacion(
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion
    )
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(variablesServicio.obtenerPorcentajesprimerysegundo(idPeriodo, numeroActualizacion,"C"));
    }

}
