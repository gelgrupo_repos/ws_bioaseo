/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.comunicacion.ClienteToken;
import com.gell.estandar.constante.EAplicacion;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.SeguridadServicio;
import com.gell.dorbitaras.negocio.util.AuditoriaUtil;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.estandar.dto.ArchivoDTO;
import com.gell.estandar.util.LogUtil;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author god
 */
@RestController
public class SeguridadControlador extends GenericoControlador
{

  @Autowired
  private SeguridadServicio seguridadServicio;
  
  @Value("${url.auth}")
  private String urlAuth;
  @Value("${url.auth.archivos}")
  private String urlArchivos;

  /**
   * Servicio para generar el token
   *
   * @param idAcceso
   * @return Respuesta genérico con el token
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Global.INICIO_SESION_PRISMA)
  public RespuestaDTO inicioSesion(
          @JsonArgumento("idAcceso") Integer idAcceso)
          throws AplicacionExcepcion
  {
    String token = new ClienteToken(EAplicacion.DORBI,urlAuth)
            .autenticarConPrisma(idAcceso);
    return new RespuestaDTO().setDatos(token);
  }

  /**
   * Construye el menú con todas las opciones
   *
   * @return Listado de las opciones y en orden jerárguico
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Global.MENU)
  public RespuestaDTO menu()
          throws AplicacionExcepcion
  {
    String token = AuditoriaUtil.auditoria().getToken();
    ClienteToken cliente = new ClienteToken(EAplicacion.NOCON,urlAuth);
    return cliente.menuPrismaGenerico(token);
  }

  /**
   * Consulta la fecha del sistema
   *
   * @return Respuesta Genérica con la fecha del sistema
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Global.FECHA_SISTEMA)
  public RespuestaDTO fechaSistema()
          throws AplicacionExcepcion
  {
    return new RespuestaDTO()
            .setDatos(new Date());
  }

  /**
   * Servicio que expone el método de adjuntar los archivos de soporte de la
   * aplicación
   *
   * @param listaArchivos Información del listado de archivos
   * @return
   * @throws AplicacionExcepcion Error al adjuntar los archivos
   */
  @PostMapping(ERuta.Global.ARCHIVO_ADJUNTAR)
  public RespuestaDTO adjuntar(@RequestParam("archivo") MultipartFile[] listaArchivos)
          throws AplicacionExcepcion
  {
    return seguridadServicio.adjuntar(listaArchivos,urlArchivos);
  }

  /**
   * Método encargado de consultar la información de un archivo
   *
   * @param idArchivo identificador del archivo
   * @return
   * @throws AplicacionExcepcion Error al consultar el archivo
   */
  @PostMapping(ERuta.Global.ARCHIVO_CONSULTAR)
  public RespuestaDTO consultarArchivo(@JsonArgumento("id") String idArchivo)
          throws AplicacionExcepcion
  {
    return seguridadServicio.consultarArchivo(idArchivo,urlArchivos);
  }

  /**
   * Método encargado de consultar un archivo demasiado grando
   *
   * @param idArchivo Identificador del archivo
   * @param response Objeto de respuesta de spring
   * @throws AplicacionExcepcion Error al consultar el archivo
   */
  @GetMapping(value = ERuta.Global.ARCHIVO_CONSULTAR_GRANDE,
          produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public void consultarArchivo(@RequestParam("idArchivo") String idArchivo, HttpServletResponse response)
          throws AplicacionExcepcion
  {
    try {
      RespuestaDTO<ArchivoDTO> respuesta = seguridadServicio.consultarArchivo(idArchivo,urlArchivos);
      ArchivoDTO archivo = respuesta.getDatos();
      String contenido = archivo.getContenido();
      byte[] infoArchivo = Base64.getDecoder().decode(contenido);
      InputStream is = new ByteArrayInputStream(infoArchivo);
      response.setHeader("Content-Disposition", "attachment; filename=\"" + archivo.getNombreOriginal() + "\"");
      int read;
      byte[] bytes = new byte[1024];
      try ( OutputStream salida = response.getOutputStream()) {
        while ((read = is.read(bytes)) != -1) {
          salida.write(bytes, 0, read);
        }
        salida.flush();
      }
    } catch (IOException ex) {
      LogUtil.error(ex);
    }
  }

}
