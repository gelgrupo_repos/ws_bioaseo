/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.*;
import com.gell.dorbitaras.negocio.servicio.EmpresaServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class EmpresasControlador extends GenericoControlador {

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private EmpresaServicio empresaServicio;

  /**
   * Consulta todas las empresas
   *
   * @param codigo Codigo de empresa
   * @return Respuesta con la lista de empresas
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Rutas.CONSULTAR_EMPRESAS)
  public RespuestaDTO consultarEmpresas(
          @JsonArgumento("codigo") String codigo
  )
          throws AplicacionExcepcion
  {

    List<Empresas> listaRuta = empresaServicio.consultarEmpresa(codigo);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaRuta);

  }

}
