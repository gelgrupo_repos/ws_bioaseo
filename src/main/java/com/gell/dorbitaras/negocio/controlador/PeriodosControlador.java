/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.*;
import com.gell.dorbitaras.negocio.servicio.PeriodosServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.dto.SmperSemperiodosDTO;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class PeriodosControlador extends GenericoControlador
{

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private PeriodosServicio periodosServicio;

  /**
   * Método que controla los datos al guadar.
   *
   * @param periodos Registros a guardar.
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.Periodos.GUARDAR)
  public RespuestaDTO guardar(@RequestBody List<SmperSemperiodo> periodos)
          throws AplicacionExcepcion
  {
    periodosServicio.guardar(periodos);
    return new RespuestaDTO();
  }

  /**
   * Consulta los años enlazados en per_periodo y cic_ciclo
   *
   * @param idCiclo Parametro opcional identificador del ciclo.
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_ANIO)
  public RespuestaDTO consultarAnio(
          @JsonArgumento("idCiclo") Integer idCiclo
  )
          throws AplicacionExcepcion
  {
    List<PerPeriodo> listaAnios = periodosServicio.consultarAnios(idCiclo);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaAnios);

  }

  /**
   * Consulta los meses dependiendo del semestre.
   *
   * @param idSemestre Identificador cic_periodos del semestre.
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_MESES)
  public RespuestaDTO consultarMesesPeriodos(@JsonArgumento("cicSemestre") Integer idSemestre)
          throws AplicacionExcepcion
  {

    List<PerPeriodo> listaMeses = periodosServicio.consultarMesesPeriodos(idSemestre);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaMeses);

  }

  /**
   * Consulta los periodos semestrales.
   *
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_SEMESTRE)
  public RespuestaDTO consultarSemestres()
          throws AplicacionExcepcion
  {

    List<PerPeriodo> listaSemestres = periodosServicio.consultarSemestres();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaSemestres);

  }

  /**
   * Consulta los Periodos según los parametros
   *
   * @param regimen Identificador del régimen tarifario
   * @param semestre Identificador del periodo semestral.
   * @param anio Identificador del año del periodo semestral.
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Periodos.CONSULTAR)
  public RespuestaDTO consultar(
          @JsonArgumento("regimen") Integer regimen,
          @JsonArgumento("semestre") Integer semestre,
          @JsonArgumento("anio") Integer anio
  )
          throws AplicacionExcepcion
  {

    List<SmperSemperiodosDTO> listaPeriodos = periodosServicio
            .consultar(regimen, semestre, anio);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaPeriodos);

  }

  /**
   * Método encargado de consultar los periodos semestrales de un régimen
   * tarifario y que se encuentren activos.
   *
   * @param idArea Identificador del área de prestación.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_PERIODOS_SEMESTRALES_AREA)
  public RespuestaDTO consultaPeriodosSemestralesArea(
          @JsonArgumento("idArea") Integer idArea
  )
          throws AplicacionExcepcion
  {
    List<SmperSemperiodo> listaPeriodosSemestrales
            = periodosServicio.consultaPeriodosSemestralesArea(idArea);
    return new RespuestaDTO().setDatos(listaPeriodosSemestrales);
  }

  /**
   * Método encargado de consultar los periodos semestrales por área de
   * prestación
   *
   * @param idArea Identificador del área de prestación
   * @return
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_SEMESTRES_AREA)
  public RespuestaDTO consultarSemestresArea(
          @JsonArgumento("idArea") Integer idArea
  )
          throws AplicacionExcepcion
  {
    List<SmperSemperiodo> listaPeriodosSemestrales
            = periodosServicio.consultarSemestresArea(idArea);
    return new RespuestaDTO().setDatos(listaPeriodosSemestrales);
  }

  /**
   * Método encargado de consultar los periodos semestrales por régimen
   * tarifario.
   *
   * @param idRegimen Identificador del régimen tarifario.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_SEMESTRES_REGIMEN)
  public RespuestaDTO consultarSemestresRegimen(
          @JsonArgumento("idRegimen") Integer idRegimen
  )
          throws AplicacionExcepcion
  {
    List<SmperSemperiodo> listaPeriodosSemestrales
            = periodosServicio.consultarSemestresRegimen(idRegimen);
    return new RespuestaDTO().setDatos(listaPeriodosSemestrales);
  }

  /**
   * Método encargado de cerrar el periodo seleccionado.
   *
   * @param idRegimen Identificador del régimen tarifario.
   * @param idPeriodo Identificador del periodo.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Periodos.CERRAR_SEMESTRE)
  public RespuestaDTO cerrarSemestre(
          @JsonArgumento("idRegimen") Integer idRegimen,
          @JsonArgumento("idPeriodo") Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    periodosServicio.cerrarSemestre(idRegimen, idPeriodo);
    return new RespuestaDTO();
  }
  
   /**
   * Método encargado de cerrar el periodo seleccionado.
   *
   * @param idArea Identificador del area.
   * @return RespuestaDTO
   * @throws AplicacionExcepcion
   */
  @PostMapping(ERuta.Periodos.CONSULTAR_MESES_PERIODO)
  public RespuestaDTO consultarMesesPeriodo(
          @JsonArgumento("idArea") Integer idArea
  )
          throws AplicacionExcepcion
  {
    return new RespuestaDTO().setDatos(periodosServicio.consultarMesesPeriodo(idArea));
  }

}
