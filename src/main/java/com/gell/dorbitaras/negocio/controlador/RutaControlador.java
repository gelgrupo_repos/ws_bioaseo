/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.*;
import com.gell.dorbitaras.negocio.servicio.RutaServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class RutaControlador extends GenericoControlador {

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private RutaServicio rutaServicio;

  /**
   * Método que controla los datos al guardar
   *
   * @param rutRuta registro a guardar Area Prestación
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.Rutas.GUARDAR)
  public RespuestaDTO guardar(@RequestBody RutRuta rutRuta)
          throws AplicacionExcepcion
  {
    rutaServicio.guardarRuta(rutRuta);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta todos los régimen tarifarios
   *
   * @param nombre consultar por nombre de régimen tarifario
   * @param tipo Tipo de Ruta
   * @param ciclo Ciclo
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Rutas.CONSULTAR)
  public RespuestaDTO consultar(
          @JsonArgumento("criterio") String nombre,
          @JsonArgumento("tipo") Integer tipo,
          @JsonArgumento("ciclo") Integer ciclo
  )
          throws AplicacionExcepcion
  {

    List<RutRuta> listaRuta = rutaServicio.consultar(nombre, tipo, ciclo);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaRuta);

  }

  /**
   * Consulta todos los régimen tarifarios
   *
   * @return Respuesta con la lista de las empresas dependiendo del
   * identificador
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Rutas.CONSULTAR_CICLO)
  public RespuestaDTO consultar()
          throws AplicacionExcepcion
  {

    List<CicCiclo> listaEmp = rutaServicio.consultarCiclo();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaEmp);

  }
  
  /**
   * Consulta los valores del indicador RNA en estado certificado
   *
   * @return Respuesta con la lista de las empresas dependiendo del
   * identificador
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.Rutas.CONSULTAR_RUTAS_RNA)
  public RespuestaDTO consultarRutasIndicadorRNACer(
          @JsonArgumento("tipoRuta") String tiporuta,
          @JsonArgumento("areaPrestacion") Integer area,
          @JsonArgumento("periodo") Integer periodo)
          throws AplicacionExcepcion
  {

    List<VrmrVarmicroruta> lista = rutaServicio.consultarRutIndicadores(periodo,area,tiporuta);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(lista);

  }
}
