/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.RecalculoVariablesServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jrangel
 */
@RestController
public class RecalculoVariablesControlador extends GenericoControlador{
    @Autowired
    private RecalculoVariablesServicio recalculoVariablesServicio;

    /**
     * Obtiene listado de conceptos constante
     *
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoVariables.OBTENER_CONCEPTOS_CONSTANTE)
    public RespuestaDTO obtenerConceptosTipoConstante()
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                    .obtenerConceptosTipoConstante());
    }
    
    /**
     * Obtiene listado de conceptos constante
     *
     * @param uniLiquidacion id de liquidacion asociada al concepto
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoVariables.OBTENER_CONCEPTOS_BASE)
    public RespuestaDTO obtenerConceptosTipoBase(
            @JsonArgumento("uniLiquidacion") Integer uniLiquidacion)
        throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                    .obtenerConceptosTipoBase(uniLiquidacion));
    }
    
    
     /**
     * Actualiza valor de concepto seleccionado
     *
     * @param idConcepto Identificador del área de prestación.
     * @param valor Identificador del periodo.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoVariables.ACTUALIZAR_CONSTANTES)
    public RespuestaDTO actualizarTipoBase(
            @JsonArgumento("idConcepto") Integer idConcepto,
            @JsonArgumento("valor") Double valor
    )throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .actualizarTipoConstante(idConcepto, valor));
    }
    
      /**
     * almacena modificaciones a valore base
     *
     * @param idConcepto Identificador del concepto.
     * @param idPeriodo Identificador del periodo.
     * @param raco_ideregistr  Identificador del periodo padre.
     * @param valor  valor a actualizar.
     * @param descripcion  descripcion del Raco.
     * @return RespuestaDTO.
     * @throws AplicacionExcepcion
     */
    @PostMapping(ERuta.RecalculoVariables.ACTUALIZAR_BASE)
    public RespuestaDTO GuardarValoresBase(
            @JsonArgumento("idConcepto") Integer idConcepto,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("raco_ideregistr") Integer raco_ideregistr,
            @JsonArgumento("valor") Double valor,
            @JsonArgumento("descripcion") String descripcion
    )
            throws AplicacionExcepcion {
         return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .actualizarTipoBase(idConcepto, idPeriodo, raco_ideregistr, valor, descripcion));
    }
 
    /**
    * Obtiene los registros de la tabla historico devoluciones
    *
    * @param idPeriodo Identificador del periodo.
    * @return RespuestaDTO.
    * @throws AplicacionExcepcion
    */
    @PostMapping(ERuta.RecalculoVariables.HISTORICO_DEVOLUCIONES)
    public RespuestaDTO obtenerHistoricoDevoluciones(
            @JsonArgumento("idPeriodo") Integer idPeriodo            
    )
            throws AplicacionExcepcion {
         return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .obtenerHistoricoDevoluciones(idPeriodo));
    }
    
    /**
    * Obtiene los registros de la tabla historico devoluciones
    *
    * @param idPeriodo Identificador del periodo.
    * @return RespuestaDTO.
    * @throws AplicacionExcepcion
    */
    @PostMapping(ERuta.RecalculoVariables.DEVOLUCIONES_RECALCULAR)
    public RespuestaDTO recalcular(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre           
    )
        throws AplicacionExcepcion {
            // Obtener numero de actualizacion
            Integer numero = recalculoVariablesServicio.obtenerUltimaActualizacion(idArea, idPeriodo);
            // Recalculo Semestral
            recalculoVariablesServicio.recalculoSemestral(idArea, idPeriodoPadre, idPeriodo, numero);
            
            // Recalculo Mensual
            recalculoVariablesServicio.recalculoMensual(idArea, idPeriodoPadre, idPeriodo, numero);
            
            return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .recalculoDiferencias(idArea, idPeriodoPadre, idPeriodo, numero));
    }
    
    /**
    * Método encargado de obtener historico de tonealadas para una area, periodo y
    * asociacion dada
    *
    * @param numeroActualizacion numero actualizacion.
    * @param idPeriodo Identificador del periodo.
    * 
    * @return RespuestaDTO.
    * @throws AplicacionExcepcion
    */
    @PostMapping(ERuta.RecalculoVariables.OBTENER_CONCEPTOS_CALCULADOS)
    public RespuestaDTO obtenerConceptosCalculados(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion,
            @JsonArgumento("idPeriodo") Integer idPeriodo
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .obtenerConceptosCalculados(idArea, numeroActualizacion, idPeriodo));
    }
    
    /**
    * Método encargado de obtener historico de tonealadas para una area, periodo y
    * asociacion dada
    *
    * @param numeroActualizacion numero actualizacion.
    * @param idPeriodo Identificador del periodo.
    * 
    * @return RespuestaDTO.
    * @throws AplicacionExcepcion
    */
    @PostMapping(ERuta.RecalculoVariables.ACTUALIZAR_HISTORICO_DEVOLUCION)
    public RespuestaDTO obtenerConceptosCalculados(
            @JsonArgumento("idArea") Integer idArea,
            @JsonArgumento("numeroActualizacion") Integer numeroActualizacion,
            @JsonArgumento("idPeriodoPadre") Integer idPeriodoPadre,
            @JsonArgumento("idPeriodo") Integer idPeriodo,
            @JsonArgumento("accion") String accion
    )
            throws AplicacionExcepcion {
        return new RespuestaDTO()
                .setDatos(recalculoVariablesServicio
                        .actualizarHistorico(idArea, numeroActualizacion, idPeriodoPadre, idPeriodo, accion));
    }
}
