/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package com.gell.dorbitaras.negocio.controlador;

import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.servicio.AreaPrestacionServicio;
import com.gell.dorbitaras.negocio.util.anotacion.JsonArgumento;
import com.gell.dorbitaras.persistencia.entidades.*;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Cristtian
 */
@RestController
public class AreaPrestacionControlador extends GenericoControlador {

  /**
   * Clase que se encarga de tener la lógica de negocio
   */
  @Autowired
  private AreaPrestacionServicio areaPrestacionServicio;

  /**
   * Método que controla los datos al guardar
   *
   * @param areaPrestacion registro a guardar Area Prestación
   * @return Respuesta Genérica si es correcta o incorrecta
   * @throws AplicacionExcepcion Si hay algún error de validación
   */
  @PostMapping(ERuta.AreaPrestacion.GUARDAR)
  public RespuestaDTO guardar(@RequestBody ArprAreaprestacion areaPrestacion)
          throws AplicacionExcepcion
  {
    areaPrestacionServicio.guardarAreaPrestacion(areaPrestacion);
    return new RespuestaDTO<>(EMensajeNegocio.OK);
  }

  /**
   * Consulta todos los régimen tarifarios
   *
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.AreaPrestacion.CONSULTAR_PROYECTOS)
  public RespuestaDTO consultar()
          throws AplicacionExcepcion
  {

    List<Proyectos> listaProyectos = areaPrestacionServicio.consultarProyectos();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaProyectos);

  }

  /**
   * Consulta todos los régimen tarifarios
   *
   * @param nombre consultar por nombre de régimen tarifario
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.AreaPrestacion.CONSULTAR)
  public RespuestaDTO consultar(
          @JsonArgumento("criterio") String nombre
  )
          throws AplicacionExcepcion
  {

    List<ArprAreaprestacion> listaArpr = areaPrestacionServicio.consultar(nombre);
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaArpr);

  }
  /**
   * Consulta todos los estratos
   *
   * @return Respuesta con la lista de las unidades dependiendo de la estructura
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @PostMapping(ERuta.AreaPrestacion.CONSULTAR_ESTRATOS)
  public RespuestaDTO consultarEstratos()
          throws AplicacionExcepcion
  {

    List<EttaEstttarifas> listaEtta = areaPrestacionServicio.consultarEstratos();
    return new RespuestaDTO(EMensajeNegocio.OK).setDatos(listaEtta);

  }
}
