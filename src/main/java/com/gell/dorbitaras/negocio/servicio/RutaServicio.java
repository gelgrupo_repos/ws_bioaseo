/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EGlobal;
import com.gell.dorbitaras.persistencia.dao.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class RutaServicio extends GenericoServicio
{

  /**
   * Método que controla los datos al guardar
   *
   * @param rutRuta Información del registro
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarRuta(RutRuta rutRuta)
          throws AplicacionExcepcion
  {
    rutRuta.validar();
    List<RuemRutempresa> listaEstratos = rutRuta.getListaValores();
    RutRutaDAO rutRutaDAO = new RutRutaDAO(dataSource, auditoria());
    RuemRutempresaDAO rutaEmpresaDAO = new RuemRutempresaDAO(dataSource, auditoria());
    RuemRutempresa rutaEmpresas;
    rutRuta.setUsuIderegistro(auditoria().getIdUsuario());
    rutRutaDAO.guardarRuta(rutRuta);
    for (RuemRutempresa valor : listaEstratos) {
      if (valor.getAccion().equalsIgnoreCase(EGlobal.Acciones.ELIMINAR)) {
        rutaEmpresaDAO.eliminarEmpresa(valor.getRuemIderegistr());
        continue;
      }
      rutaEmpresas = new RuemRutempresa()
              .setRuemIderegistr(valor.getRuemIderegistr())
              .setRutIderegistro(rutRuta)
              .setEmpIderegistro(valor.getEmpIderegistro())
              .setUsuIderegistro(auditoria().getIdUsuario());
      rutaEmpresas.validar();
      rutaEmpresaDAO.insertar(rutaEmpresas);
    }
  }

  /**
   * Método que controla la consulta de ruta
   *
   * @param nombre criterio de busqueda
   * @param tipo Tipo de ruta
   * @param ciclo Ciclo
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<RutRuta> consultar(
          String nombre,
          Integer tipo,
          Integer ciclo)
          throws PersistenciaExcepcion
  {
    return new RutRutaDAO(dataSource, auditoria())
            .consultar(nombre, tipo, ciclo);
  }

  /**
   * Método que controla la consulta de ruta
   *
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<CicCiclo> consultarCiclo()
          throws PersistenciaExcepcion
  {
    return new CicCicloDAO(dataSource, auditoria())
            .consultar();
  }
  
  @Transactional(readOnly = true)
  public List<VrmrVarmicroruta> consultarRutIndicadores(
          Integer periodo,
          Integer areaPrestacion,
          String tipoRuta)
          throws PersistenciaExcepcion
  {
    return new VrmrVarmicrorutaDAO(dataSource, auditoria())
            .consultarIndicadoresCertificados(periodo,areaPrestacion,tipoRuta);
  }

}
