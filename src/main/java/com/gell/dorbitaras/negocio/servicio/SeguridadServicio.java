/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.PrgProgramaDAO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.dto.RespuestaDTO;
import com.gell.estandar.comunicacion.ClienteArchivo;
import com.gell.estandar.constante.EAplicacion;
import com.gell.estandar.dto.ArchivoDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author god
 */
@Service
public class SeguridadServicio extends GenericoServicio
{

  /**
   * Adjunta una lista de archivos al programa de AZDigital
   *
   * @param listaArchivos información del listado de archivos
   * @return Información como quedó en el programa de AZDigital
   * @throws AplicacionExcepcion Error al adjuntar
   */
  public RespuestaDTO adjuntar(MultipartFile[] listaArchivos,String servicio)
          throws AplicacionExcepcion
  {

    ClienteArchivo cliente = new ClienteArchivo(EAplicacion.DORBI, auditoria().getToken(),servicio);
    
    return cliente.adjuntar(listaArchivos);
  }

  /**
   * Método encargado de consultar un archivo en la aplicación de AZDigital
   *
   * @param id Identificador del archivo que se quiere consultar
   * @return Información del archivo
   * @throws AplicacionExcepcion Error al consultar el archivo
   */
  public RespuestaDTO<ArchivoDTO> consultarArchivo(String id,String servicio)
          throws AplicacionExcepcion
  {
    ClienteArchivo cliente = new ClienteArchivo(EAplicacion.DORBI, auditoria().getToken(), servicio);
    return cliente.consultar(id);
  }

  /**
   * Servicio que sirve para corroborar si la URL esta en la BD
   *
   * @param url Identificador que contiene la URL
   * @return Identificador del programa
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public Integer existeURL(String url)
          throws AplicacionExcepcion
  {
    PrgProgramaDAO promgramaDAO = new PrgProgramaDAO(dataSource, auditoria());
    return promgramaDAO.exiteURL(url);
  }

  /**
   * Método encargado de consultar un archivo a AZDigital
   *
   * @param id Identificador del archivo que se quiere consultar
   * @return Archivo en formato de arreglo de bytes
   * @throws AplicacionExcepcion Error al consultar el archivo
   */
  public byte[] consultarArchivoByte(String id)
          throws AplicacionExcepcion
  {
    return new ClienteArchivo(EAplicacion.DORBI, auditoria().getToken())
            .consultarByte(id);
  }

}
