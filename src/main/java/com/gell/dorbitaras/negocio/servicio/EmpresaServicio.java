/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class EmpresaServicio extends GenericoServicio {

  /**
   * Método que controla la consulta de ruta
   *
   * @param codigo Código de la empresa
   * @return Lista de las empresas dependiendo del parametro
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<Empresas> consultarEmpresa(
          String codigo)
          throws PersistenciaExcepcion
  {
    return new EmpresasDAO(dataSource, auditoria())
            .consultarEmpresa(codigo);
  }

 
}
