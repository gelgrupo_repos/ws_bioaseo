/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.DctaDctotarifasDAO;
import com.gell.dorbitaras.persistencia.dao.RgtaRgitarifarioDAO;
import com.gell.dorbitaras.persistencia.entidades.DctaDctotarifas;
import com.gell.dorbitaras.persistencia.entidades.RgtaRgitarifario;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author god
 */
@Service
public class RegimenTarifarioServicio extends GenericoServicio
{

  /**
   * Método que controla los datos al guardar
   *
   * @param regimen Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * Error al ejecutar la consulta
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarRegimen(RgtaRgitarifario regimen)
          throws AplicacionExcepcion
  {

    regimen.validar();
    List<DctaDctotarifas> listaValores = regimen.getListaValores();
    RgtaRgitarifarioDAO regimenDAO = new RgtaRgitarifarioDAO(dataSource, auditoria());
    DctaDctotarifasDAO consumidoDAO = new DctaDctotarifasDAO(dataSource, auditoria());
    regimen.setEmpIderegistro(auditoria().getIdEmpresa());
    if (regimenDAO.validarNombre(regimen.getRgtaNombre(), regimen.getRgtaIderegistro()) > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_NOMBRE_REGIMEN, regimen.getRgtaNombre());
    }
    regimenDAO.guardarRegimen(regimen);
    for (DctaDctotarifas documentosRegimen : listaValores) {
      documentosRegimen.setRgtaIderegistro(regimen)
              .validar();
      consumidoDAO.insertar(documentosRegimen);
    }

  }

  /**
   * Método que controla la consulta del régimen tarifario
   *
   * @param nombre
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<RgtaRgitarifario> consultar(String nombre)
          throws PersistenciaExcepcion
  {
    return new RgtaRgitarifarioDAO(dataSource, auditoria())
            .consultar(nombre);
  }
}
