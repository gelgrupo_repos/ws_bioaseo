/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.ValidarDato;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gell.dorbitaras.persistencia.dao.VarcVarcalculoDAO;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.VarcVarcalculo;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.GestionActualizacionCostosDAO;
import com.gell.dorbitaras.persistencia.dto.ValorCalculadoVariacionDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoIndicadorProductividad;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 *
 * @author jrangel
 */
@Service
public class GestionActualizacionCostosServicio extends GenericoServicio {

    private static Double VARIACION_TRES_POR_CIENTO = 3.0;
    private static String DESCRIPCION_PRODUCTIVIDAD = "valor_productividad";
    private static String SALTO_PRODUCTIVIDAD = "salto con productividad";
    private Integer varcIderegistro = -1;
    private Integer varcIderegistroSalto = -1;
    List<ConConcepto> listaConRelacionado=null;

    /**
     * Método que controla la consulta de semestres
     *
     * @param idArea area seleccionada
     * @param idPeriodo area seleccionada
     * @param idConcepto area seleccionada
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public List<VariacionCostosProductividadDTO> obtenerVariaciones(Integer idArea, Integer idPeriodo, Integer idConcepto)
            throws AplicacionExcepcion {
        ValidarDato.construir()
                .agregar(idArea, "requerido", "El identificador del area es obligatorio")
                .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
                .agregar(idConcepto, "requerido", "El identificador del concepto es obligatorio")
                .validar();
        return new VarcVarcalculoDAO(dataSource, auditoria())
                .consultarVariaciones(idArea, idPeriodo, idConcepto);
    }

    /**
     * Método que controla la consulta de semestres
     *
     * @param idArea area seleccionada
     * @param idPeriodo area seleccionada
     * @param idConcepto area seleccionada
     * @param idMes area seleccionada
     * @param valorIngresado area seleccionada
     * @param valorProductividad area seleccionada
     * @param tieneProductividad area seleccionada
     * @param valorAcumulado area seleccionada
     * @param idIndicadorVariacion area seleccionada
     * @param valorFactorProductividad valor productividad ingresado
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(timeout = 1000)
    public void insertarVariaciones(Integer idArea, Integer idPeriodo,
            Integer idConcepto, Integer idMes, Double valorIngresado,
            Double valorProductividad, Boolean tieneProductividad,
            Double valorAcumulado,
            Integer idIndicadorVariacion,
            Double valorFactorProductividad)
            throws AplicacionExcepcion {
        // <editor-fold defaultstate="collapsed" desc="Valores fijos en VarcVarCalculo"> 
        ValidarDato.construir()
                .agregar(idArea, "requerido", "El identificador del area es obligatorio")
                .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
                .agregar(idConcepto, "requerido", "El identificador del concepto es obligatorio")
                .validar();
       VarcVarcalculoDAO dao =  new VarcVarcalculoDAO(dataSource, auditoria());
       ConConceptoDAO conceptoDAO = new ConConceptoDAO(dataSource, auditoria());
        VarcVarcalculo varcVarCalculo = new VarcVarcalculo();
        varcVarCalculo.setArprIderegistro(new ArprAreaprestacion().setArprIderegistro(idArea));
        varcVarCalculo.setPerIderegistro(new PerPeriodo().setPerIderegistro(idMes));
        varcVarCalculo.setVarcIdMes(idMes);
        varcVarCalculo.setVarcValor(valorIngresado);
        varcVarCalculo.setVarcEstadoregistro(EEstadoGenerico.ACTIVO);
        varcVarCalculo.setVarcEstado(EEstadoGenerico.CERTIFICADO);

        varcVarCalculo.setEmpIderegistro(auditoria().getIdEmpresa());
        varcVarCalculo.setUsuIderegistroGb(auditoria().getIdUsuario());
        varcVarCalculo.setUsuIderegistroCer(auditoria().getIdUsuario());
        varcVarCalculo.setVarcFeccerficicacion(new Date());
        varcVarCalculo.setVarcFecgrabacion(new Date());
        // </editor-fold>
        Double valorP = null;
        
        //verifica si existe una variacion en otros indicadores
        List<ValorCalculadoVariacionDTO> valoresCalculadoVariacionDTO
                = dao.comprobarProductividadPrevia(idArea, idMes);
        List<ValorCalculadoVariacionDTO> valorCalculadoVariacionesDTO = valoresCalculadoVariacionDTO.size() > 0
                ? valoresCalculadoVariacionDTO.stream()
                        .filter(variacion -> Objects.equals(variacion.getIdConcepto(), idIndicadorVariacion))
                        .collect(Collectors.toList()) : Collections.emptyList();
        ValorCalculadoVariacionDTO valorCalculadoVariacionUltimaDTO = dao
            .comprobarVariacionMesPeriodo(idArea, idConcepto, idMes);
        boolean variacion = calcularVariacion(valorCalculadoVariacionUltimaDTO.getValor(), valorCalculadoVariacionUltimaDTO.getBandera(), valorIngresado);
        //Agrega nulo el estado en caso que no exista una variacion
            varcVarCalculo.setVarcEstadoVariacion(variacion ? EEstadoGenerico.ACTIVO : null);
            // <editor-fold defaultstate="expanded" desc="Inserta valores de variacion para el mes"> 
            VarcVarcalculo varcVarCalculoVariacion = new VarcVarcalculo();
            varcVarCalculoVariacion.setArprIderegistro(new ArprAreaprestacion().setArprIderegistro(idArea));
            varcVarCalculoVariacion.setPerIderegistro(new PerPeriodo().setPerIderegistro(idMes));
            varcVarCalculoVariacion.setVarcIdMes(idMes);
            varcVarCalculoVariacion.setVarcEstadoregistro(EEstadoGenerico.ACTIVO);
            varcVarCalculoVariacion.setVarcEstado(EEstadoGenerico.CERTIFICADO);
            varcVarCalculoVariacion.setEmpIderegistro(auditoria().getIdEmpresa());
            varcVarCalculoVariacion.setUsuIderegistroGb(auditoria().getIdUsuario());
            varcVarCalculoVariacion.setUsuIderegistroCer(auditoria().getIdUsuario());
            varcVarCalculoVariacion.setVarcFeccerficicacion(new Date());
            varcVarCalculoVariacion.setVarcFecgrabacion(new Date());
            varcVarCalculoVariacion.setConIderegistro(new ConConcepto().setUniConcepto(new UniUnidad().setUniIderegistro(idIndicadorVariacion)));
            //ir a buscar si para este periodo se registró productividad
            valorP = dao.obtenerValorProductividad(idArea, idConcepto, idPeriodo);
//            if(tieneProductividad){
//                varcVarCalculoVariacion.setVarcValor(valorAcumulado - valorFactorProductividad);
//            }else{
            varcVarCalculoVariacion.setVarcValor(valorAcumulado);
//            }
            varcVarCalculoVariacion.setVarcEstadoVariacion(variacion ? EEstadoGenerico.ACTIVO : null);
            dao.insertar(varcVarCalculoVariacion);
            // </editor-fold>
        //Productividad
        if (tieneProductividad) {
            if(valorP == null){
                ConConcepto conceptoProductividad = new ConConcepto();
                conceptoProductividad=conceptoDAO.consultarConceptoFProductividad();
                // <editor-fold defaultstate="expanded" desc="Llena valores para insertar valor de productividad"> 
                VarcVarcalculo varcVarCalculoProductividad = new VarcVarcalculo();
                varcVarCalculoProductividad.setConIderegistro(conceptoProductividad);
                varcVarCalculoProductividad.setVarcDescripcion(DESCRIPCION_PRODUCTIVIDAD);
                varcVarCalculoProductividad.setArprIderegistro(new ArprAreaprestacion().setArprIderegistro(idArea));
                varcVarCalculoProductividad.setPerIderegistro(new PerPeriodo().setPerIderegistro(idMes));
                varcVarCalculoProductividad.setVarcIdMes(idMes);
                varcVarCalculoProductividad.setVarcEstadoregistro(EEstadoGenerico.ACTIVO);
                varcVarCalculoProductividad.setVarcEstado(EEstadoGenerico.CERTIFICADO);

                varcVarCalculoProductividad.setEmpIderegistro(auditoria().getIdEmpresa());
                varcVarCalculoProductividad.setUsuIderegistroGb(auditoria().getIdUsuario());
                varcVarCalculoProductividad.setUsuIderegistroCer(auditoria().getIdUsuario());
                varcVarCalculoProductividad.setVarcFeccerficicacion(new Date());
                varcVarCalculoProductividad.setVarcFecgrabacion(new Date());
                varcVarCalculoProductividad.setVarcIdConceptoAplicado(varcIderegistro);
                varcVarCalculoProductividad.setVarcIderegistro(idIndicadorVariacion);
                Double valorRedondeado = (double)Math.round(valorFactorProductividad * 10000d) / 10000d;
                varcVarCalculoProductividad.setVarcValor(valorRedondeado);
                varcIderegistro = dao.agregarRegistro(varcVarCalculoProductividad);
            }
            // </editor-fold>
             List<ValorCalculadoVariacionDTO> variacionesActualizar
                 = dao.comprobarProductividadPrevia(idArea, idMes);
           /* if(variacion){
            //actualiza el valor del ultimo acumulado con respecto al factor de productividad
            variacionesActualizar = valoresCalculadoVariacionADTO.stream()
                    .filter(variation -> !Objects.equals(variation.getIdConcepto(), idIndicadorVariacion))
                    .collect(Collectors.toList());
            }else{
                variacionesActualizar = valoresCalculadoVariacionDTO;
            }*/
            if (variacionesActualizar.size() > 0) {
                variacionesActualizar.stream().forEach(vActualizar -> {
//                    System.out.println("-----------Validando errores------------\n");
                    try {
                        listaConRelacionado = conceptoDAO.consultarConceptosCorrelacionados(vActualizar.getIdConcepto());
                        
                    } catch (PersistenciaExcepcion ex) {
                        System.out.print(ex.getMessage());
                    }                    
                    VarcVarcalculo varcVarCalculoSalto = new VarcVarcalculo();
                    varcVarCalculoSalto.setConIderegistro(listaConRelacionado.get(0));
                    varcVarCalculoSalto.setVarcDescripcion(SALTO_PRODUCTIVIDAD);
                    varcVarCalculoSalto.setArprIderegistro(new ArprAreaprestacion().setArprIderegistro(idArea));
                    varcVarCalculoSalto.setPerIderegistro(new PerPeriodo().setPerIderegistro(vActualizar.getIdPeriodo()));
                    varcVarCalculoSalto.setVarcIdMes(vActualizar.getIdPeriodo());
                    varcVarCalculoSalto.setVarcEstadoregistro(EEstadoGenerico.ACTIVO);
                    varcVarCalculoSalto.setVarcEstado(EEstadoGenerico.CERTIFICADO);

                    varcVarCalculoSalto.setEmpIderegistro(auditoria().getIdEmpresa());
                    varcVarCalculoSalto.setUsuIderegistroGb(auditoria().getIdUsuario());
                    varcVarCalculoSalto.setUsuIderegistroCer(auditoria().getIdUsuario());
                    varcVarCalculoSalto.setVarcFeccerficicacion(new Date());
                    varcVarCalculoSalto.setVarcFecgrabacion(new Date());
                    varcVarCalculoSalto.setVarcIdConceptoAplicado(varcIderegistro);
//                    varcVarCalculoSalto.setVarcValor(valorRedondeado);
//                    varcIderegistroSalto = dao.agregarRegistro(varcVarCalculoProductividad);
                
                    VarcVarcalculo valorEditar = new VarcVarcalculo();
                    valorEditar.setVarcIderegistro(vActualizar.getIdeRegistro());
                    // Se almacena ide del concepto donde se aplico productividad
//                    valorEditar.setVarcIdConceptoAplicado(varcIderegistro);
                    if(vActualizar.getIdPeriodo().equals(idMes)){
                        double valorRedondeado = Math.round(vActualizar.getValor() * 10000d) / 10000d;
                        varcVarCalculoSalto.setVarcValor(valorRedondeado - valorFactorProductividad);                        
                    }else{
                        double valorRedondeado = Math.round(vActualizar.getValor() * 10000d) / 10000d;
                        varcVarCalculoSalto.setVarcValor(valorRedondeado*(1 + 0 - valorFactorProductividad));
                    }
                    try {
                        varcIderegistroSalto = dao.agregarRegistro(varcVarCalculoSalto);
                        valorEditar.setVarcIdConceptoAplicado(varcIderegistroSalto);
                        new VarcVarcalculoDAO(dataSource, auditoria())
                                .editarFProductividadIndicadoresAdyacentes(valorEditar);
                    } catch (PersistenciaExcepcion ex) {
                        System.out.print(ex.getMessage());
                    }
                });
            }
        }
        //Inserta valor
        varcVarCalculo.setConIderegistro(new ConConcepto().setUniConcepto(new UniUnidad().setUniIderegistro(idConcepto)));
        dao.insertar(varcVarCalculo);
    }

    /**
     * Método que controla la consulta de semestres
     *
     * @param Valor area seleccionada
     * @param Bandera
     * @param valorIngresado area seleccionada
     * @return
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public Boolean calcularVariacion(Float Valor, String Bandera, Double valorIngresado)
            throws AplicacionExcepcion {
        // Double variacion = (double) Math.round((valorIngresado / Valor) * 10000d) / 10000d;
        Double residuo = Math.abs(valorIngresado - Valor);
        Double porcentajeResiduo = (residuo / Valor) * 100;
        return porcentajeResiduo >= VARIACION_TRES_POR_CIENTO;
    }

    /**
     * Servicio encargado de consultar los conceptos.
     *
     * @return Lista de conceptos.
     * @throws AplicacionExcepcion
     */
    @Transactional(readOnly = true)
    public List<ConConceptoIndicadorProductividad> consultarConceptoIndicadorProductividad()
            throws AplicacionExcepcion {
        ValidarDato.construir()
                .validar();
        return new GestionActualizacionCostosDAO(dataSource, auditoria())
                .consultarConceptoIndicadorProductividad();
    }

    /**
     * Método encargado de actualizar el cambio de base en la variación de un
     * período.
     *
     * @param idVariacion Identificador de la variación.
     * @param valorActualizacion Valor a cambiar base.
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public void cambioBaseVariaciones(Integer idVariacion, Double valorActualizacion)
            throws AplicacionExcepcion {
        ValidarDato.construir()
                .agregar(idVariacion, "requerido", "El identificador de la variación es obligatorio")
                .agregar(valorActualizacion, "requerido", "El valor de cambio de base es obligatorio")
                .validar();
        new VarcVarcalculoDAO((dataSource), auditoria())
                .cambioBaseVariaciones(idVariacion, valorActualizacion);
    }
    
    /**
     * Método trae el consolidado de variaciones multiplicadas
     *
     * @param idArea area seleccionada
     * @param idPeriodo area seleccionada
     * @param idConcepto area seleccionada
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public Double obtenerVariacionesTotales(Integer idArea, Integer idPeriodo, Integer idConcepto)
            throws AplicacionExcepcion {
        ValidarDato.construir()
                .agregar(idArea, "requerido", "El identificador del area es obligatorio")
                .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
                .agregar(idConcepto, "requerido", "El identificador del concepto es obligatorio")
                .validar();
        return new VarcVarcalculoDAO(dataSource, auditoria())
                .VaracionTotales(idConcepto,idArea,idPeriodo);
    }

}
