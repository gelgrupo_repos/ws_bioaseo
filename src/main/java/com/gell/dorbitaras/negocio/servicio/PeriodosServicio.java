/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EEstructura;
import com.gell.dorbitaras.negocio.constante.EGlobal;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.*;
import com.gell.dorbitaras.persistencia.dto.SmperSemperiodosDTO;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.ValidarDato;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import jline.internal.Log;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class PeriodosServicio extends GenericoServicio
{

  /**
   * Método que controla los datos al guardar.
   *
   * @param periodos Información de los registros.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardar(List<SmperSemperiodo> periodos)
          throws AplicacionExcepcion
  {
    SmperSemperiodoDAO periodosDAO = new SmperSemperiodoDAO(dataSource, auditoria());
    for (SmperSemperiodo periodo : periodos) {
      periodo.validar();
      if (procesarRegistro(periodo)) {
        periodosDAO.insertar(periodo);
        continue;
      }
      periodosDAO.inactivarPeriodo(periodo);
    }
  }

  /**
   * Método encargado de determinar si un registro se a insertar, actualizar o
   * eliminar.
   *
   * @param periodo Datos del periodo a procesar
   * @return boolean
   */
  public boolean procesarRegistro(SmperSemperiodo periodo)
  {
    if (periodo.getSmperIderegistro() == null) {
      periodo.setSmperFecgrabacion(new Date());
      periodo.setUsuIderegistroGb(auditoria().getIdUsuario());
      periodo.setSmperSwtact(EEstadoGenerico.ACTIVO);
      return true;
    }
    periodo.setSmperFecact(new Date());
    periodo.setUsuIderegistroAct(auditoria().getIdUsuario());
    return !EGlobal.Acciones.ELIMINAR.equalsIgnoreCase(periodo.getAccion());
  }

  /**
   * Método que controla la consulta de años
   *
   * @param idCiclo Parametro opcional identificador del ciclo.
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<PerPeriodo> consultarAnios(Integer idCiclo)
          throws PersistenciaExcepcion
  {
    return new PerPeriodoDAO(dataSource, auditoria())
            .consultarAnios(idCiclo);
  }

  /**
   * Método que controla la consulta de semestres
   *
   * @param idSemestre Identificador del semestre
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<PerPeriodo> consultarMesesPeriodos(Integer idSemestre)
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idSemestre, "requerido", "El identificador del semestre es obligatorio")
            .validar();
    return new PerPeriodoDAO(dataSource, auditoria())
            .consultarMesesSemestre(idSemestre);
  }

  /**
   * Método que controla la consulta de los meses
   *
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<PerPeriodo> consultarSemestres()
          throws PersistenciaExcepcion
  {
    return new PerPeriodoDAO(dataSource, auditoria())
            .consultarSemestres();
  }

  /**
   * Método que controla la consulta de Periodos
   *
   * @param regimen Identificador del régimen tarifario
   * @param semestre Identificador del periodo semestral.
   * @param anio Identificador del año del periodo semestral.
   * @return Lista de las rutas dependiendo los parametros
   * @throws AplicacionExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<SmperSemperiodosDTO> consultar(
          Integer regimen,
          Integer semestre,
          Integer anio
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(regimen, "requerido", "El identificador del régimen es obligatorio")
            .agregar(semestre, "requerido", "El identificador del semestre es obligatorio")
            .agregar(anio, "requerido", "El identificador del año es obligatorio")
            .validar();
    return new SmperSemperiodoDAO(dataSource, auditoria())
            .consultar(regimen, semestre, anio);
  }

  /**
   * Servicio encargado de consultar los periodos semestrales por régimen
   * tarifario y que se encuentren activos.
   *
   * @param idArea Identificador del Área de Prestación.
   * @return Lista de periodos semestrales.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<SmperSemperiodo> consultaPeriodosSemestralesArea(
          Integer idArea
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .validar();
    return new SmperSemperiodoDAO(dataSource, auditoria())
            .consultarPeriodosSemestralesArea(idArea);
  }

  /**
   * Método encargado de consultar los periodos semestrales por área de
   * prestación
   *
   * @param idArea Identificador del área de prestación
   * @return Lista de periodos semestrales
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<SmperSemperiodo> consultarSemestresArea(
          Integer idArea
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .validar();
    return new SmperSemperiodoDAO(dataSource, auditoria())
            .consultarSemestresArea(idArea);
  }

  /**
   * Método encargado de consultar los periodos semestrales por régimen
   * tarifario
   *
   * @param idRegimen Identificador del régimen tarifario
   * @return Lista de periodos semestrales
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<SmperSemperiodo> consultarSemestresRegimen(
          Integer idRegimen
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idRegimen, "requerido", "El identificador del régimen tarifario es obligatorio")
            .validar();
    return new SmperSemperiodoDAO(dataSource, auditoria())
            .consultarSemestresRegimen(idRegimen);
  }

  /**
   * Método encargado de cerrar el periodo seleccionado.
   *
   * @param idRegimen Identificador del régimen tarifario.
   * @param idPeriodo Identificador del periodo.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void cerrarSemestre(
          Integer idRegimen,
          Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idRegimen, "requerido", "El identificador del régimen tarifario es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
    int secPerSemes=0;
    Date FechaIniVig = new Date();
    Date FechaFinVig = new Date();
    List<String> meses = Arrays.asList("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());
    PerPeriodoDAO periodoDAO = new PerPeriodoDAO(dataSource, auditoria());
    SmperSemperiodoDAO peridoSemestralDAO = new SmperSemperiodoDAO(dataSource, auditoria());
    RgtaRgitarifario regimen = new RgtaRgitarifarioDAO(dataSource, auditoria()).consultar(idRegimen.longValue());
    PerPeriodo periodoSemestral = periodoDAO.consultar(idPeriodo.longValue());
    if(periodoSemestral.getPerIdeorden()==1){
        secPerSemes=periodoSemestral.getPerIdeorden()+1;
    }else{
        secPerSemes=periodoSemestral.getPerIdeorden()-1;
    }
    variableDAO.consultarVariablesCertificadas(periodoSemestral);
    List<PerPeriodo> listaMeses = periodoDAO.consultarMesesSemestrePeriodo(idPeriodo);
    for (PerPeriodo mes : listaMeses) {
      variableDAO.consultarVariablesCertificadas(mes);
    }
    FechaIniVig= periodoSemestral.getPerFecinicial();
    FechaFinVig = periodoSemestral.getPerFecfinal();
    PerPeriodo periodoSemestralSig = new PerPeriodo(); 
    periodoSemestralSig.setCicIderegistro(periodoSemestral.getCicIderegistro());
    periodoSemestralSig.setPerIdeorden(secPerSemes);
    periodoSemestralSig.setPerFecinicial(convertToDate((convertToLocalDate(DateUtils.addMonths(FechaIniVig, 6)).with(TemporalAdjusters.firstDayOfMonth()))));
    periodoSemestralSig.setPerFecfinal(convertToDate((convertToLocalDate(DateUtils.addMonths(FechaFinVig, 6)).with(TemporalAdjusters.lastDayOfMonth()))));
    periodoSemestralSig.setPerNombre("Semestre "+secPerSemes);
    periodoSemestralSig.setPerEstado(EEstadoGenerico.ACTIVO);
    periodoSemestralSig.setPerIderegistro(null);
    periodoSemestralSig.setUsuIderegistro(auditoria().getIdUsuario());
    periodoSemestralSig.setPerBlofecha(EEstadoGenerico.NO);
    periodoDAO.insertar(periodoSemestralSig);
      for (int i = 0; i < 6; i++) {
        Date FechaIniNext = periodoSemestral.getPerFecfinal();
        PerPeriodo periodoMensual = new PerPeriodo();
            periodoMensual.setCicIderegistro(new CicCiclo().setCicIderegistro(EEstructura.CICLO_MENSUAL))
                    .setPerNombre((secPerSemes==1) ? meses.get(i): meses.get(i+6))
                    .setPerIderegistro(null)
                    .setPerEstado(EEstadoGenerico.ACTIVO)
                    .setPerBlofecha(EEstadoGenerico.NO)
                    .setUsuIderegistro(auditoria().getIdUsuario())
                    .setPerFecinicial(convertToDate((convertToLocalDate(DateUtils.addMonths(FechaIniNext, i+1)).with(TemporalAdjusters.firstDayOfMonth()))))
                    .setPerFecfinal(convertToDate((convertToLocalDate(DateUtils.addMonths(FechaIniNext, i+1)).with(TemporalAdjusters.lastDayOfMonth()))))
                    .setPerIdeorden((secPerSemes==1) ? i+1: i+7); 
            periodoDAO.insertar(periodoMensual);
            SmperSemperiodo periodoTaras = new SmperSemperiodo();
              periodoTaras.setPerIdepadre(periodoSemestralSig)
              .setPerIderegistro(periodoMensual)
              .setRgtaIderegistro(regimen)
              .setSmperFecgrabacion(new Date())
              .setSmperNumero(periodoMensual.getPerIdeorden())
              .setSmperDescripcion(periodoMensual.getPerNombre())
              .setSmperSwtact(EEstadoGenerico.ACTIVO)
              .setUsuIderegistroGb(auditoria().getIdUsuario())
              .setSmperIderegistro(null);
      peridoSemestralDAO.insertar(periodoTaras);
      }
    peridoSemestralDAO.inactivarPeriodoSemestral(idPeriodo);
    periodoDAO.inactivarPeriodos(idPeriodo);
//    SmperSemperiodo periodoTaras = new SmperSemperiodo();
//    for (PerPeriodo periodo : listaMesesSemestre) {
//      periodoTaras.setPerIdepadre(semestreSiguiente)
//              .setPerIderegistro(periodo)
//              .setRgtaIderegistro(regimen)
//              .setSmperFecgrabacion(new Date())
//              .setSmperNumero(periodo.getPerIdeorden())
//              .setSmperDescripcion(periodo.getPerNombre())
//              .setSmperSwtact(EEstadoGenerico.ACTIVO)
//              .setUsuIderegistroGb(auditoria().getIdUsuario())
//              .setSmperIderegistro(null);
//      peridoSemestralDAO.insertar(periodoTaras);
//    }

  }
  
    public LocalDate convertToLocalDate(Date dateToConvert) {
        return dateToConvert.toInstant()
          .atZone(ZoneId.systemDefault())
          .toLocalDate();
    }
    
    public Date convertToDate(LocalDate dateToConvert) {
        return Date.from(dateToConvert.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
    /**
   * Método encargado de consultar los periodos semestrales por área de
   * prestación
   *
   * @param idArea Identificador del área de prestación
   * @return Lista de periodos semestrales
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<SmperSemperiodoMeses> consultarMesesPeriodo(
          Integer idArea
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .validar();
    return new SmperSemperiodoDAO(dataSource, auditoria())
            .consultarMesesPeriodo(idArea);
  }
  
}
