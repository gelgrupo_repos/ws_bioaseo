/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ApprAreaproyectoDAO;
import com.gell.dorbitaras.persistencia.dao.ArprAreaprestacionDAO;
import com.gell.dorbitaras.persistencia.dao.EttaEstttarifasDAO;
import com.gell.dorbitaras.persistencia.dao.ProyectosDAO;
import com.gell.dorbitaras.persistencia.entidades.ApprAreaproyecto;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.EttaEstttarifas;
import com.gell.dorbitaras.persistencia.entidades.Proyectos;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class AreaPrestacionServicio extends GenericoServicio
{

  /**
   * Método que controla los datos al guardar
   *
   * @param areaPrestacion Información del registro consulta
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarAreaPrestacion(ArprAreaprestacion areaPrestacion)
          throws AplicacionExcepcion
  {

    areaPrestacion.validar();
    List<EttaEstttarifas> listaEstratos = areaPrestacion.getListaEstratos();
    List<ApprAreaproyecto> listaProyectos = areaPrestacion.getListaProyectos();
    ArprAreaprestacionDAO areaPrestacionDAO = new ArprAreaprestacionDAO(dataSource, auditoria());
    EttaEstttarifasDAO estratoTarifasDAO = new EttaEstttarifasDAO(dataSource, auditoria());
    ApprAreaproyectoDAO areaProyectoDAO = new ApprAreaproyectoDAO(dataSource, auditoria());
    areaPrestacion.setEmpIderegistro(auditoria().getIdEmpresa());
    String nombre = areaPrestacion.getArprNombre();
    Integer idRegistro = areaPrestacion.getArprIderegistro();
    if (areaPrestacionDAO.validarNombre(nombre, idRegistro) > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_NOMBRE_AREAPRESTACION, nombre);
    }
    String nuap = areaPrestacion.getArprNuap();
    if (areaPrestacionDAO.validaNuap(nuap, idRegistro) > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_NUAP_AREAPRESTACION, nuap);
    }
    String nusd = areaPrestacion.getArprNusd();
    if (areaPrestacionDAO.validaNusd(nusd, idRegistro) > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_NUSD_AREAPRESTACION, nusd);
    }
    EttaEstttarifas estratos;
    ApprAreaproyecto areaProyecto;
    areaPrestacion.setEmpIderegistro(auditoria().getIdEmpresa());
    areaPrestacionDAO.guardarArpr(areaPrestacion);
    for (EttaEstttarifas valor : listaEstratos) {
      estratos = new EttaEstttarifas()
              .setEttaIderegistro(valor.getEttaIderegistro())
              .setArprIderegistro(areaPrestacion)
              .setEttaNombre(valor.getEttaNombre())
              .setEttaSwtestado(valor.getEttaSwtestado())
              .setUsuIderegistroGb(auditoria().getIdUsuario())
              .setEttaFecgrabacion(valor.getEttaFecgrabacion());
      estratos.validar();
      estratoTarifasDAO.insertar(estratos);
    }
    for (ApprAreaproyecto valor : listaProyectos) {
      areaProyecto = new ApprAreaproyecto()
              .setApprIderegistro(valor.getApprIderegistro())
              .setArprIderegistro(areaPrestacion)
              .setProyectoIderegistro(valor.getProyectoIderegistro())
              .setApprSwtestado(valor.getApprSwtestado())
              .setUsuIderegistroGb(auditoria().getIdUsuario())
              .setApprFecgrabacion(valor.getApprFecgrabacion());
      areaProyecto.validar();
      areaProyectoDAO.insertar(areaProyecto);
    }

  }

  /**
   * Método que controla la consulta de proyectos
   *
   * @return Lista de proyectos dependiendo de la empresa
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<Proyectos> consultarProyectos()
          throws PersistenciaExcepcion
  {
    return new ProyectosDAO(dataSource, auditoria())
            .consultarProyectos();
  }

  /**
   * Método que controla la consulta del régimen tarifario
   *
   * @param nombre
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ArprAreaprestacion> consultar(String nombre)
          throws PersistenciaExcepcion
  {
    return new ArprAreaprestacionDAO(dataSource, auditoria())
            .consultar(nombre);
  }

  /**
   * Método que controla la consulta de estratos
   *
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<EttaEstttarifas> consultarEstratos()
          throws PersistenciaExcepcion
  {
    return new EttaEstttarifasDAO(dataSource, auditoria())
            .consultarEstrato();
  }
}
