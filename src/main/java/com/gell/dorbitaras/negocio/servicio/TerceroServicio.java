/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.TerTerceroDAO;
import com.gell.dorbitaras.persistencia.entidades.TerTercero;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 *
 * @author god
 */
@Service
public class TerceroServicio extends GenericoServicio
{

  /**
   * Consulta todos los terceros que tengan asociados alguna unidad de la
   * estructura que se está pasando
   *
   * @param criterio nombre o documento del tercero a buscar
   * @return Lista de todos los terceros que cumplen con las condiciones
   * @throws PersistenciaExcepcion
   */
  @Transactional(readOnly = true)
  public List<TerTercero> consultarEntidad(String criterio)
          throws PersistenciaExcepcion
  {

    return new TerTerceroDAO(dataSource, auditoria())
            .consultarPorClasificacion(criterio);
  }

  /**
   * Consulta todas las rutas que tengan asociados alguna unidad de la
   * estructura que se está pasando
   *
   * @param criterio nombre de la ruta a buscar
   * @return Lista de todas las rutas que cumplen con las condiciones
   * @throws PersistenciaExcepcion
   */
  @Transactional(readOnly = true)
  public List<TerTercero> consultarTipoRuta(String criterio)
          throws PersistenciaExcepcion
  {

    return new TerTerceroDAO(dataSource, auditoria())
            .consultarTipoRuta(criterio);
  }

  /**
   * Servicio encargado de consultar los terceros aprovechadores.
   *
   * @return Lista de terceros.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<TerTercero> consultarTerceroAprovechador()
          throws AplicacionExcepcion
  {
    return new TerTerceroDAO(dataSource, auditoria())
            .consultarTerceroAprovechador();
  }
}
