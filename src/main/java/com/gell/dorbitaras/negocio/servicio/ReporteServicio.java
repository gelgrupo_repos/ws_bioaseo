/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.Aplicacion;
import com.gell.dorbitaras.negocio.constante.EReporte;
import com.gell.dorbitaras.persistencia.dao.*;
import com.gell.dorbitaras.persistencia.dao.crud.PerPeriodoCRUD;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.constante.ETipoReporte;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.reporte.ReporteInvocar;
import java.io.InputStream;
import java.sql.Connection;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class ReporteServicio extends GenericoServicio {

  /**
   * Servicio que controla el reporte del cálculo de la liquidación de contrato
   * gnc
   *
   * @param fechaInicial fecha inicial que ingresa el usuario
   * @param fechaFinal fecha final que ingresa el usuario
   * @param idTipoContrato Identificador del tipo de contrato
   * @return Reporte
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public byte[] ReporteEstadoCuenta(
          Long idSuscripcion)
          throws AplicacionExcepcion
  {
    System.out.println("reporte estado de cuenta->"+idSuscripcion+"\n");
    InputStream reporte = Aplicacion.class
            .getResourceAsStream(EReporte.REPORTE_ESTADO_CUENTA_OT.getNombre());
    Connection cnn = DataSourceUtils.getConnection(dataSource);
    ReporteInvocar invocar = ReporteInvocar.getInstancia();
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("PR_INT_DSUS",idSuscripcion);
    parametros.put("PR_INT_ID", auditoria().getIdUsuario());
    System.out.println(parametros);
    byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
    return archivo;
  }
  
 /**

   * @return Reporte
   * @throws AplicacionExcepcion
   */
   @Transactional(readOnly = true)
   public byte[] ReporteActualizacionCostos()
           throws AplicacionExcepcion
   {
     InputStream reporte = Aplicacion.class
             .getResourceAsStream(EReporte.REPORTE_ACTUALIZACION_COSTOS.getNombre());
     Connection cnn = DataSourceUtils.getConnection(dataSource);
     ReporteInvocar invocar = ReporteInvocar.getInstancia();
     Map<String, Object> parametros = new HashMap<>();
     System.out.println(parametros);
     byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
     return archivo;
   }
   
 /**

   * @return Reporte
   * @throws AplicacionExcepcion
   */
   @Transactional(readOnly = true)
   public byte[] ReporteActualizacionVariablesIngresadas()
           throws AplicacionExcepcion
   {
     InputStream reporte = Aplicacion.class
             .getResourceAsStream(EReporte.REPORTE_VARIABLES_INGRESADAS.getNombre());
     Connection cnn = DataSourceUtils.getConnection(dataSource);
     ReporteInvocar invocar = ReporteInvocar.getInstancia();
     Map<String, Object> parametros = new HashMap<>();
     System.out.println(parametros);
     byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
     return archivo;
   }

  @Transactional(readOnly = true)
  public byte[] ReporteInformacionConsultor(
          Integer periodo)
          throws AplicacionExcepcion
  {
    PerPeriodoCRUD perPeriodoCRUD = new PerPeriodoCRUD(dataSource, auditoria());
    PerPeriodo per = perPeriodoCRUD.consultar((long) periodo);
    System.out.print(per.getPerFecinicial()+"\n");
    Format formatter = new SimpleDateFormat("yyyy-MM-dd");
    String s = formatter.format(per.getPerFecinicial());
    System.out.print("fecha formateada->"+s+"\n");
    InputStream reporte = Aplicacion.class
            .getResourceAsStream(EReporte.REPORTE_INFORMACION_CONSULTOR.getNombre());
    Connection cnn = DataSourceUtils.getConnection(dataSource);
    ReporteInvocar invocar = ReporteInvocar.getInstancia();
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("PR_INT_SEMESTRE",s);
    System.out.println(parametros);
    byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
    return archivo;
  }
  
  @Transactional(readOnly = true)
  public byte[] ReporteValoresComponente(
          String fecha)
          throws AplicacionExcepcion
  {
    System.out.println("fecha normal->"+fecha+"\n");
    LocalDate date = LocalDate.parse(fecha);
    System.out.println("fecha parseada->"+date+"\n");
    InputStream reporte = Aplicacion.class
            .getResourceAsStream(EReporte.REPORTE_VALORES_COMPONENTES.getNombre());
    Connection cnn = DataSourceUtils.getConnection(dataSourceTalend);
    ReporteInvocar invocar = ReporteInvocar.getInstancia();
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("PR_STR_SEMESTRE",fecha);
    System.out.println(parametros);
    byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
    return archivo;
  }
  

   @Transactional(readOnly = true)
   public byte[] ReporteToneladasAsociaciones()
           throws AplicacionExcepcion
   {
     InputStream reporte = Aplicacion.class
             .getResourceAsStream(EReporte.REPORTE_TONELADAS_ASOCIACIONES.getNombre());
     Connection cnn = DataSourceUtils.getConnection(dataSource);
     ReporteInvocar invocar = ReporteInvocar.getInstancia();
     Map<String, Object> parametros = new HashMap<>();
     System.out.println(parametros);
     byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
     return archivo;
   }
   
  @Transactional(readOnly = true)
  public byte[] ReporteValoresComponenteTFS(
          String fecha)
          throws AplicacionExcepcion
  {
    System.out.println("fecha normal->"+fecha+"\n");
    LocalDate date = LocalDate.parse(fecha);
    System.out.println("fecha parseada->"+date+"\n");
    InputStream reporte = Aplicacion.class
            .getResourceAsStream(EReporte.REPORTE_VALORES_COMPONENTES_TFS.getNombre());
    Connection cnn = DataSourceUtils.getConnection(dataSourceTalend);
    ReporteInvocar invocar = ReporteInvocar.getInstancia();
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("PR_STR_SEMESTRE",fecha);
    System.out.println(parametros);
    byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
    return archivo;
  }
  
  @Transactional(readOnly = true)
  public byte[] ReporteActividadesLU(
          String fecha)
          throws AplicacionExcepcion
  {
    System.out.println("fecha normal->"+fecha+"\n");
    LocalDate date = LocalDate.parse(fecha);
    System.out.println("fecha parseada->"+date+"\n");
    InputStream reporte = Aplicacion.class
            .getResourceAsStream(EReporte.REPORTE_ACTIVIDADES_LIMPIEZA_URBANA.getNombre());
    Connection cnn = DataSourceUtils.getConnection(dataSourceTalend);
    ReporteInvocar invocar = ReporteInvocar.getInstancia();
    Map<String, Object> parametros = new HashMap<>();
    parametros.put("PR_STR_SEMESTRE",fecha);
    System.out.println(parametros);
    byte[] archivo = invocar.invocar(reporte, parametros, cnn, ETipoReporte.EXCEL);
    return archivo;
  }
 
}
