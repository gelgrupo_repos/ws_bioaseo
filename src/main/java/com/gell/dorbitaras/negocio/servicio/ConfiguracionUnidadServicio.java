/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.EstEstructuraDAO;
import com.gell.dorbitaras.persistencia.dao.UniUnidadDAO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.EstEstructura;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.estandar.constante.EMensajeEstandar;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import static com.gell.estandar.util.FuncionesDatoUtil.valorPorDefecto;
import com.gell.estandar.util.ValidarDato;
import java.util.Map;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author god
 */
@Service
public class ConfiguracionUnidadServicio extends GenericoServicio
{

  /**
   * Método que expone el método de guardar unidad básica y si se envía el
   * identificador de la unidad se edita
   *
   * @param uniUnidad Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * Error al ejecutar la consulta
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarUnidadBasica(UniUnidad uniUnidad)
          throws AplicacionExcepcion
  {
    uniUnidad.validarTipoConfiguracion();
    UniUnidadDAO uniUnidadDAO = new UniUnidadDAO(dataSource, auditoria());
    int estIderegistro = uniUnidad.getEstIderegistro().getEstIderegistro();
    EstEstructuraDAO estructuraDAO = new EstEstructuraDAO(dataSource, auditoria());
    EstEstructura estructura = estructuraDAO.consultar(estIderegistro);
    //Se valida que el nombre ya no esté registrado en la base de datos 
    String uniNombre1 = uniUnidad.getUniNombre1() + "";
    if (uniNombre1.equals("null") || uniNombre1.equals("")) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_NOMBRE_APLICACION);
    }
    uniUnidadDAO.validaNombre(uniNombre1, estIderegistro,
            uniUnidad.getUniIderegistro());
    Map<String, String> propiedades = jsonMap(uniUnidad.getUniPropiedad());
    String codigoUnidad = propiedades.get("codigo");
    if (estructura.getEstValida().equalsIgnoreCase("S")
            && vacio(codigoUnidad)) {
      throw new NegocioExcepcion(EMensajeEstandar.ERROR_VALIDACION_MENSAJE,
              "El código es obligatorio");
    }
    if (tieneValor(codigoUnidad)) {
      uniUnidadDAO.validarCodigo(uniUnidad, codigoUnidad);
    }

    uniUnidadDAO.generarCodigoUnidad(uniUnidad, "UNB");
    uniUnidad.setUniCodigo1(uniUnidad.getUniCodigo());
    uniUnidad.setUniCodigo(estIderegistro + uniUnidad.getUniCodigo());
    uniUnidadDAO.guardarUnidad(uniUnidad);
  }

  /**
   * Consulta todas las unidades dependiendo del tipo de estrucutra
   *
   * @param criterio nombre o código de la unidad
   * @param estIderegistro Identificador de la estructura
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<UniUnidad> consultar(String criterio, long estIderegistro)
          throws PersistenciaExcepcion
  {
    return new UniUnidadDAO(dataSource, auditoria())
            .consultar(criterio, estIderegistro);
  }

  /**
   * Consulta las estructuras disponibles para poder configurar las unidades
   *
   * @return Lista con todas las Estructuras permitidas para el registro de
   * unidades
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion Si
   * ocurre un error al momento de ejecutar
   *
   */
  @Transactional(readOnly = true)
  public List<EstEstructura> consultarEstructurasConfiguracion()
          throws PersistenciaExcepcion
  {

    return new EstEstructuraDAO(dataSource, auditoria())
            .consultarEstructurasConfiguracion();
  }

  /**
   * Consulta todas las unidades relacionadas a una clase especificada
   *
   * @param idClase Identificador de la clase
   * @param criterio Nombre o código de la unidad
   * @return Listado de las unidades pertenecientes a la clase especificadas
   * @throws PersistenciaExcepcion
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<UniUnidad> consultarUnidadPorClase(Integer idClase, String criterio)
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idClase, "requerido", "El identificador de la clase es obligatorio")
            .validar();
    criterio = valorPorDefecto(criterio);
    AuditoriaDTO auditoria = auditoria();
    EstEstructura estructura = new EstEstructuraDAO(dataSource, auditoria)
            .consultar(idClase, auditoria.getIdEmpresa());
    return consultar(criterio, estructura.getEstIderegistro());
  }

  /**
   * Consulta todas las unidades de la empresa que pertenecen un programa y a
   * una clase en específico
   *
   * @param idClase Identificador de la clase
   * @param idPrograma Identificador del programa
   * @param criterio nombre o parte del nombre de la unidad
   * @return Listado de las unidades
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = false)
  public List<ConConcepto> consultarUnidadProgramaClase(String criterio,
          Integer idClase,
          Integer idPrograma)
          throws AplicacionExcepcion
  {
    if (idPrograma == null) {
      idPrograma = Integer.valueOf(auditoria().getParametro("idPrograma"));
    }
    ValidarDato.construir()
            .agregar(idClase, "requerido",
                    "El identificador de la clase es obligatorio")
            .agregar(idPrograma, "requerido",
                    "El identificador del programa es requerido")
            .validar();
    criterio = criterio == null ? "" : criterio;
    return new ConConceptoDAO(dataSource, auditoria())
            .consultarUnidadProgramaClase(idClase, idPrograma, criterio);
  }
}
