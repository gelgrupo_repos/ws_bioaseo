/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.EParametro;
import com.gell.dorbitaras.negocio.delegado.LiquidarConceptoDelegado;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ArprAreaprestacionDAO;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.PerPeriodoDAO;
import com.gell.dorbitaras.persistencia.dao.VarprVarperregDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaHistoricoRecalculosDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaQaTotalHistoricoDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaTaRecalcAprvDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaTerceroPorcenTaDao;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprHistDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprPromDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaPromFinalesDAO;
import com.gell.dorbitaras.persistencia.dto.ActualizacionesToneladasPeriodoDTO;
import com.gell.dorbitaras.persistencia.dto.ConceptosCalculadosDTO;
import com.gell.dorbitaras.persistencia.dto.ListadoTotalPorcentajesRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.MesesxPeriodoPadreDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeFinalRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeRecalculoDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gell.dorbitaras.persistencia.dto.ValoresToneladasDTO;
import com.gell.dorbitaras.persistencia.dto.ValoresToneladasPorPeriodoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.ArgumentoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.VrtaPromFinales;
import com.gell.dorbitaras.persistencia.entidades.TerTercero;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculos;
import com.gell.dorbitaras.persistencia.entidades.VrtaQaTotalHistorico;
import com.gell.dorbitaras.persistencia.entidades.VrtaTaRecalcAprv;
import com.gell.dorbitaras.persistencia.entidades.VrtaTerceroPorcenTa;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarterapr;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarteraprHist;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarteraprProm;
import com.gell.estandar.util.ValidarDato;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import jline.internal.Log;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jrangel
 */
@Service
public class RecalculoAprovechamientoServicio extends GenericoServicio {

    @Autowired
    private CalculoTarifasServicio calculoServicio;
    private static final int TA_SIN_DINC = 2899;
    private static final int TA_CON_DINC = 3897;
    static Long asociacionProcesada = null;
    static Double totalDiferenciasAjuste = 0.0;
    static Double totalDiferenciasDevolucion = 0.0;
    static Double totalPromedioToneladasAjuste = 0.0;
    static Double totalPromedioToneladasDevolucion = 0.0;
    static Integer flagAjusteDevoluciones = 0;

    /**
     * Consulta historico de toneladas
     *
     * @param idArea area seleccionada
     * @param idPeriodo area seleccionada
     * @param idAsociacion area seleccionada
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public List<ValoresToneladasDTO> obtenerListadoToneladas(
            Integer idArea,
            Integer idPeriodo,
            Long idAsociacion) throws AplicacionExcepcion {
        ValidarDato.construir()
                .agregar(idArea, "requerido", "El identificador del area es obligatorio")
                .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
                .agregar(idAsociacion, "requerido", "El identificador de la asociacion es obligatorio")
                .validar();
        ConConcepto toneladasTercero = new ConConceptoDAO(dataSource, auditoria()).consultarPorAbreviatura("vb-TAXTM");
        VrtaVarteraprDAO dao = new VrtaVarteraprDAO(dataSource, auditoria());
        return dao.obtenerHistoricoToneladas(idArea, idPeriodo, idAsociacion,
                toneladasTercero.getUniConcepto().getUniIderegistro());
    }

    /**
     * inserta valores de toneladas
     *
     * @param idArea area seleccionada
     * @param idPeriodo area seleccionada
     * @param idAsociacion area seleccionada
     * @param valor valor a registrar.
     * @param numeroActualizacion
     * @param idPeriodoPadre
     * @param observacion descripción del valor a insertar.
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(rollbackFor = Throwable.class)
    public boolean insertarToneladas(
            Integer idArea,
            Integer idPeriodo,
            Long idAsociacion,
            Double valor,
            Integer numeroActualizacion,
            Integer idPeriodoPadre,
            String observacion,
            Boolean comercializacion) throws AplicacionExcepcion {
        VrtaVarteraprDAO dao = new VrtaVarteraprDAO(dataSource, auditoria());
        VrtaVarteraprHistDAO vrtaHistDao = new VrtaVarteraprHistDAO(dataSource, auditoria());
        VrtaVarterapr valorInsertar = new VrtaVarterapr();
        valorInsertar.setArprIderegistro(new ArprAreaprestacion().setArprIderegistro(idArea));
        valorInsertar.setPerIderegistro(new PerPeriodo().setPerIderegistro(idPeriodo));
        valorInsertar.setTerIderegistro(new TerTercero().setTerIderegistro(idAsociacion));
        valorInsertar
                .setConIderegistro(
                        new ConConcepto().setUniConcepto(
                                new UniUnidad().setUniIderegistro(
                                        new ConConceptoDAO(dataSource, auditoria())
                                                .consultarPorAbreviatura("vb-TAXTM").getUniConcepto().getUniIderegistro())));
        valorInsertar.setVrtaValor(valor);
        valorInsertar.setVrtaDescripcion(observacion);
        valorInsertar.setEmpIderegistro(auditoria().getIdEmpresa());
        valorInsertar.setVrtaFecgrabacion(new Date());
        valorInsertar.setVrtaEstado(EEstadoGenerico.CERTIFICADO);
        valorInsertar.setVrtaEstadoRegistro(EEstadoGenerico.ACTIVO);
        valorInsertar.setUsuIderegistroGb(auditoria().getIdUsuario());
        valorInsertar.setComercializacion((comercializacion==true) ? comercializacion : null);
        boolean insersion = false;
        try {
            dao.InactivarVariablesTercero(new ConConceptoDAO(dataSource, auditoria())
                                                .consultarPorAbreviatura("vb-TAXTM").getUniConcepto().getUniIderegistro(), 
                                                    idAsociacion, 
                                                        idPeriodo);
            Integer id = dao.insertarRetornaId(valorInsertar);
            vrtaHistDao.insertar(new VrtaVarteraprHist(numeroActualizacion, id, new Timestamp(new Date().getTime())));

            insersion = true;
        } catch (PersistenciaExcepcion pe) {
            pe.printStackTrace();
        }
        return insersion;
    }

//    @Transactional(rollbackFor = Throwable.class)
    public void calcularPromedios(VrtaVarteraprDAO dao, Integer numeroActualizacion, Integer idConcepto, Integer idPeriodo, Integer idArea, Integer idPeriodoPadre, String tipo)
            throws AplicacionExcepcion {
        List<ActualizacionesToneladasPeriodoDTO> listadoTerceros;
        // Se traen los terceros de la tabla varterapr para poder verificar uno a uno sus periodos
        if(tipo.equals("C")){
            listadoTerceros  = dao.obtenerTercerosTotal();            
        }else{
            listadoTerceros  = dao.obtenerTercerosUltimoCalculoNormal(idPeriodo);
        }        
        
        System.out.println("terceros->"+listadoTerceros.size());
        
        // Se recorre uno a uno el tercero
        listadoTerceros.stream().forEach(tercero -> {
            try {
                // Limpio el campo que me ayuda a no duplicar datos
                asociacionProcesada = null;
                // se busca si tiene asociacion en el semestre anterior por tercero
                List<MesesxPeriodoPadreDTO> mesesXPeriodoAnterior = dao.obtenerListadoMesesPeriodoAnterior(idPeriodo, idArea, tercero.getIdAsociacion());

                // se busca si tiene asociacion en el semestre actual por tercero
                List<MesesxPeriodoPadreDTO> mesesXPeriodoActual = dao.obtenerListadoMesesPeriodoActual(idPeriodo, tercero.getIdAsociacion());
                
                if (mesesXPeriodoAnterior.size() > 0) {
                    System.out.println("mesesXPeriodoAnterior>"+mesesXPeriodoAnterior.size()+"\n");
                    
                    MultiValuedMap<Long, ValoresToneladasPorPeriodoDTO> valoresCalculadosPAnterior = new ArrayListValuedHashMap<>();
                    mesesXPeriodoAnterior.stream().forEach(item -> {
                        try {
                            // Obtiene en el periodo iterado todas las asociaciones que hallan ese mes
                            List<ActualizacionesToneladasPeriodoDTO> listadoActualizaciones = new ArrayList<ActualizacionesToneladasPeriodoDTO>();
                                    listadoActualizaciones = dao.obtenerActualizacionesToneladasXPeriodoPadreAnterior(idConcepto,
                                            item.getPerIderegistro(), idArea, tercero.getIdAsociacion());

                            //se crea arreglo con id,Asociacion, valor e id periodo hijo
                            listadoActualizaciones.stream().forEach(elemento -> {
                                valoresCalculadosPAnterior.put(elemento.getIdAsociacion(), new ValoresToneladasPorPeriodoDTO(elemento.getVrtaValor(), item.getPerIdepadre()));
                            });
                        } catch (PersistenciaExcepcion ex) {
                            System.out.print(ex.getMessage());
                        }
                    });
                    
                    // Se obtiene la lista de los valores ya calculados del tercer para poder promediar el semestre
                    System.out.println("valoresCalculadosPAnterior33>"+valoresCalculadosPAnterior.size()+"\n");
                    MultiSet<Long> idAsociacionesAnterior = valoresCalculadosPAnterior.keys();
                    idAsociacionesAnterior.stream().forEach(asociacionAnterior -> {
                        System.out.println("asociacionProcesada>"+asociacionProcesada+"\n");
                        System.out.println("asociacionAnterior>"+asociacionAnterior+"\n");
                        if (asociacionProcesada != asociacionAnterior) {
                            try {
                                List<ValoresToneladasPorPeriodoDTO> valoresAsociacion = (List<ValoresToneladasPorPeriodoDTO>) valoresCalculadosPAnterior.get(asociacionAnterior);
                                double valorPromedio = (valoresAsociacion.stream()
                                        .mapToDouble(o -> o.getValor())
                                        .sum()) / valoresAsociacion.size();
                                //inserta en vartera_prom los promedios calculados
                                VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
                                promDao.insertar(new VrtaVarteraprProm(idPeriodo,
                                       numeroActualizacion,
                                       tipo,
                                       asociacionAnterior, true, false,
                                       new Timestamp(new Date().getTime()),
                                       auditoria().getIdUsuario(), valorPromedio, null));
                                
                                guardarPorcentajeTerceroTa(asociacionAnterior, numeroActualizacion, idPeriodo);
                            } catch (PersistenciaExcepcion ex) {
                                System.out.print(ex.getMessage());
                            } catch (AplicacionExcepcion ex) {
                                System.out.print(ex.getMessage());
                            }
                        }
                        asociacionProcesada = asociacionAnterior;
                    });
                } else if (mesesXPeriodoActual.size() > 0)  {
                    MultiValuedMap<Long, ValoresToneladasPorPeriodoDTO> valoresCalculadosPActual = new ArrayListValuedHashMap<>();
                    mesesXPeriodoActual.stream().forEach(item -> {
                        try {
                            //obtiene listado de actualizaciones por mes del periodo anterior
                            List<ActualizacionesToneladasPeriodoDTO> listadoActualizaciones
                                    = dao.obtenerActualizacionesToneladasXPeriodoPadreAnterior(idConcepto,
                                            item.getPerIderegistro(), idArea, tercero.getIdAsociacion());
                            //se crea arreglo con id,Asociacion, valor e id periodo hijo
                            listadoActualizaciones.stream().forEach(elemento -> {
                                valoresCalculadosPActual.put(elemento.getIdAsociacion(), new ValoresToneladasPorPeriodoDTO(elemento.getVrtaValor(), item.getPerIdepadre()));
                            });
                        } catch (PersistenciaExcepcion ex) {
                            System.out.print(ex.getMessage());
                        }
                    });
                    
                    // Se obtienen las asociaciones del mes actual por tercero
                    MultiSet<Long> idAsociacionesActual = valoresCalculadosPActual.keys();
                    
                    idAsociacionesActual.stream().forEach(asociacionAnterior -> {
                        if (asociacionProcesada != asociacionAnterior) {
                            try {
                                List<ValoresToneladasPorPeriodoDTO> valoresAsociacion = (List<ValoresToneladasPorPeriodoDTO>) valoresCalculadosPActual.get(asociacionAnterior);
                                double valorPromedio = (valoresAsociacion.stream()
                                        .mapToDouble(o -> o.getValor())
                                        .sum()) / valoresAsociacion.size();
                                //inserta en vartera_prom los promedios calculados
                                VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
                                promDao.insertar(new VrtaVarteraprProm(idPeriodo,
                                        numeroActualizacion,
                                        tipo,
                                        asociacionAnterior, true, false,
                                        new Timestamp(new Date().getTime()),
                                        auditoria().getIdUsuario(), valorPromedio, null));

                                guardarPorcentajeTerceroTa(asociacionAnterior, numeroActualizacion, idPeriodo);
                            } catch (PersistenciaExcepcion ex) {
                                System.out.print(ex.getMessage());
                            } catch (AplicacionExcepcion ex) {
                                System.out.print(ex.getMessage());
                            }
                        }
                        asociacionProcesada = asociacionAnterior;
                    });
                }else{                   
                    VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
                    promDao.insertar(new VrtaVarteraprProm(idPeriodo,
                                        numeroActualizacion,
                                        tipo,
                                        tercero.getIdAsociacion(), true, false,
                                        new Timestamp(new Date().getTime()),
                                        auditoria().getIdUsuario(), 0.0, null));
                }
            } catch (PersistenciaExcepcion ex) {
                    System.out.print(ex.getMessage());
            }
        });
    }
    
//    @Transactional(rollbackFor = Throwable.class)
    public boolean calculoQA(Integer numActualizacion, Integer idPeriodoPadre, Integer idPeriodo, Integer idArea, String tipo)
            throws AplicacionExcepcion {
        boolean operacionExitosa = false;
        Integer vrtaQaHistIderegistro;
        Integer vrtaSegQaHistIderegistro;
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        List<VrtaVarteraprProm> listaPromediosPAnterior = promDao.obtieneUltimoPeriodoconQA(idPeriodo, idArea, tipo);
        List<VrtaVarteraprProm> listaPrimediosSegundoQA;
        if (listaPromediosPAnterior.size() > 0) {
            double valorPromedio = (listaPromediosPAnterior.stream()
                    .mapToDouble(o -> o.getVrtaPromValor())
                    .sum());
            VrtaQaTotalHistoricoDAO calcHDAO = new VrtaQaTotalHistoricoDAO(dataSource,
                    auditoria());                        
            vrtaQaHistIderegistro = calcHDAO.insertar(new VrtaQaTotalHistorico(numActualizacion,
                                            idPeriodo, tipo, idPeriodoPadre, new Timestamp(new Date().getTime()),
                                            auditoria().getIdUsuario(), valorPromedio, true));
            
            
            // Calculamos el segundo QA, si en el periodo no hay ningun dato se coloca 0
            if(tipo == "C"){
                listaPrimediosSegundoQA = promDao.obtenerSegundoQa(idPeriodo, idArea, tipo);               
            }else{
                listaPrimediosSegundoQA = promDao.obtenerSegundoRecalculoQa(idPeriodo, idArea, tipo);         
            }
           if (listaPrimediosSegundoQA.size() > 0) {
                double valorPromedio2 = (listaPrimediosSegundoQA.stream()
                        .mapToDouble(o -> o.getVrtaPromValor())
                        .sum());

                vrtaSegQaHistIderegistro = calcHDAO.insertar(new VrtaQaTotalHistorico(numActualizacion,
                                                idPeriodo, tipo, idPeriodoPadre, new Timestamp(new Date().getTime()),
                                                auditoria().getIdUsuario(), valorPromedio2, false));
            } else {
                double emptyValue = 0;
                vrtaSegQaHistIderegistro = calcHDAO.insertar(new VrtaQaTotalHistorico(numActualizacion,
                                                idPeriodo, tipo, idPeriodoPadre, new Timestamp(new Date().getTime()),
                                                auditoria().getIdUsuario(), emptyValue, false));
            }

            // Se actualiza la tabla promedios con el id del QA para crear asociacion
            promDao.asignarIdQaPromedios(vrtaQaHistIderegistro, idPeriodoPadre, idPeriodo, numActualizacion, tipo);
            // Se inicia verificacion de promedios
            calcularPromedios(idPeriodoPadre, idPeriodo, numActualizacion, vrtaQaHistIderegistro, vrtaSegQaHistIderegistro, tipo);
            
            operacionExitosa = true;
        }
        
        
        return operacionExitosa;
    }
    
//    @Transactional(rollbackFor = Throwable.class)
    public void calcularPromedios(Integer idPeriodoPadre, Integer idPeriodo, Integer numeroActualizacion, Integer vrtaQaHistIderegistro, Integer vrtaSegQaHistIderegistro,
            String tipo) 
     throws AplicacionExcepcion {
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        VrtaPromFinalesDAO promFinalDao = new VrtaPromFinalesDAO(dataSource, auditoria());
        List<PorcentajeFinalRecalculoDTO> listaPromediosSegundoQA;
        int estAjusDev = 0;
        // Se busca con el numero de actualizacion todos los promedios calculados de la tabla para el primer calculo de promedio
        List<PorcentajeFinalRecalculoDTO> listaPromediosTotales = promDao.obtenerPrimerPromedio(idPeriodoPadre, idPeriodo, numeroActualizacion, tipo);

        // Calculo primer porcentaje
        for (PorcentajeFinalRecalculoDTO promedioTercero : listaPromediosTotales) {
            promFinalDao.insertar(new VrtaPromFinales(new ConConceptoDAO(dataSource, auditoria())
                                .consultarPorAbreviatura("rcl_porce1").getUniConcepto().getUniIderegistro(), promedioTercero.getTerIdeRegistro(),
                                numeroActualizacion, tipo, promedioTercero.getVrtaPromValor(), idPeriodo, new Timestamp(new Date().getTime()), vrtaQaHistIderegistro));
        };
        
        
        // Calculo segundo porcentaje
        if(tipo == "C"){
        listaPromediosSegundoQA = promDao.obtenerSegundoPromedio(idPeriodoPadre, idPeriodo, numeroActualizacion, tipo);
        }else{    
        listaPromediosSegundoQA = promDao.obtenerSegundoPromedioRecalculo(idPeriodoPadre, idPeriodo, numeroActualizacion, tipo);
        }
        for (PorcentajeFinalRecalculoDTO promedioTerceroDos : listaPromediosSegundoQA) {
            promFinalDao.insertar(new VrtaPromFinales(new ConConceptoDAO(dataSource, auditoria())
                                .consultarPorAbreviatura("rcl_porce2").getUniConcepto().getUniIderegistro(), promedioTerceroDos.getTerIdeRegistro(),
                                numeroActualizacion, tipo, promedioTerceroDos.getVrtaPromValor(), idPeriodo, new Timestamp(new Date().getTime()), vrtaSegQaHistIderegistro));
        }
 
        // Calculo tercer porcentaje
        if (tipo == "R") {
            List<PorcentajeFinalRecalculoDTO> listaPromediosAjustados = promDao.obtenerValoresPromActualizacion(idPeriodoPadre, idPeriodo, numeroActualizacion, tipo);
            List<PorcentajeFinalRecalculoDTO> listaPromediosIniciales = promDao.obtenerValoresPromActualizacion(idPeriodoPadre, idPeriodo, numeroActualizacion - 1, tipo);
            
            List<PorcentajeFinalRecalculoDTO> listaPromediosFinalesAjuste = new ArrayList<PorcentajeFinalRecalculoDTO>();
            List<PorcentajeFinalRecalculoDTO> listaPromediosFinalesDevolucion = new ArrayList<PorcentajeFinalRecalculoDTO>();
            System.out.print("\nValidando-------\n");
            totalDiferenciasAjuste = 0.0;
            totalDiferenciasDevolucion = 0.0;
            totalPromedioToneladasAjuste = 0.0;
            totalPromedioToneladasDevolucion = 0.0;
            flagAjusteDevoluciones = 0;
            listaPromediosAjustados.stream().forEach(promedioAjustado -> {
                
                PorcentajeFinalRecalculoDTO findPromedio = listaPromediosIniciales.stream()
                    .filter(item -> promedioAjustado.getTerIdeRegistro().equals(item.getTerIdeRegistro()))
                    .findAny()
                    .orElse(null);
            System.out.print("\nEstadoflag->\n"+flagAjusteDevoluciones);    
                if (findPromedio != null) {
                    if (promedioAjustado.getVrtaPromValor().equals(findPromedio.getVrtaPromValor())) {
                    } else {
                        System.out.print("\n"+promedioAjustado.getTerIdeRegistro()+"->"+(promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor())+"\n");
                        if((promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor())>0){
                            
                            if(flagAjusteDevoluciones==3){
                                flagAjusteDevoluciones=2;//activa ajuste y devolucion
                            }else if(flagAjusteDevoluciones!=2){
                                flagAjusteDevoluciones=1;//activa ajuste
                            }
                            // Saca el total de las diferencias
                        totalDiferenciasAjuste = totalDiferenciasAjuste + (promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor());
                        // agrega la diferencia de cada asociacion
                        listaPromediosFinalesAjuste.add(new PorcentajeFinalRecalculoDTO(promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor(),
                                                    numeroActualizacion, promedioAjustado.getTerIdeRegistro()));
                        }else{
                            if(flagAjusteDevoluciones==1){
                                flagAjusteDevoluciones=2;//activa ajuste y devolucion
                            }else if(flagAjusteDevoluciones!=2){
                                flagAjusteDevoluciones=3;//activa devolucion
                            }
                            System.out.print("\nmayorque\n");
                        // Saca el total de las diferencias
                        totalDiferenciasDevolucion = totalDiferenciasDevolucion + (promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor());
                        // agrega la diferencia de cada asociacion
                        listaPromediosFinalesDevolucion.add(new PorcentajeFinalRecalculoDTO(promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor(),
                                                    numeroActualizacion, promedioAjustado.getTerIdeRegistro()));    
                        }
                    }
                } else {
                    if(flagAjusteDevoluciones==3){
                        flagAjusteDevoluciones=2;//activa ajuste y devolucion
                    }else if(flagAjusteDevoluciones!=2){
                        flagAjusteDevoluciones=1;//activa ajuste
                    }
                    // Si no encuentra el valor en los promedios anteriores se toma como si fuera 0 para los promedios
                    System.out.print("\nsolo ajuste\n");
                    totalDiferenciasAjuste = totalDiferenciasAjuste + (promedioAjustado.getVrtaPromValor());
                    // agrega el valor de promedios ya que no tiene un anterior
                    listaPromediosFinalesAjuste.add(new PorcentajeFinalRecalculoDTO(promedioAjustado.getVrtaPromValor(),
                                            numeroActualizacion, promedioAjustado.getTerIdeRegistro()));
                }
            });
            System.out.print("\ntotal toneladas ajuste->"+totalDiferenciasAjuste+"\n total toneladas dev->"+totalDiferenciasDevolucion+"\n");
            
            // recorre las diferencias para agregarle la division por el total
            listaPromediosFinalesAjuste.stream().forEach(calcPromedio -> {
                try {                            
                    promFinalDao.insertar(new VrtaPromFinales(new ConConceptoDAO(dataSource, auditoria())
                        .consultarPorAbreviatura("rcl_porce3").getUniConcepto().getUniIderegistro(), calcPromedio.getTerIdeRegistro(),
                        numeroActualizacion, tipo, (calcPromedio.getVrtaPromValor() / totalDiferenciasAjuste) * 100,
                        idPeriodo, new Timestamp(new Date().getTime()), vrtaQaHistIderegistro));
                } catch (PersistenciaExcepcion ex) {
                    System.out.print(ex.getMensaje());
                } catch (AplicacionExcepcion ex) {
                    System.out.print(ex.getMensaje());
                }
            });
            
            // recorre las diferencias para agregarle la division por el total por devolucion
            listaPromediosFinalesDevolucion.stream().forEach(calcPromedio -> {
                try {                            
                    promFinalDao.insertar(new VrtaPromFinales(new ConConceptoDAO(dataSource, auditoria())
                        .consultarPorAbreviatura("rcl_porce4").getUniConcepto().getUniIderegistro(), calcPromedio.getTerIdeRegistro(),
                        numeroActualizacion, tipo, (calcPromedio.getVrtaPromValor() / totalDiferenciasDevolucion) * 100,
                        idPeriodo, new Timestamp(new Date().getTime()), vrtaQaHistIderegistro));
                } catch (PersistenciaExcepcion ex) {
                    System.out.print(ex.getMensaje());
                } catch (AplicacionExcepcion ex) {
                    System.out.print(ex.getMensaje());
                }
            });
        }
    }
    
    @Transactional(readOnly = true)
    public Integer validarTA(Integer idPeriodo, String tipo)
            throws AplicacionExcepcion {
        VrtaTaRecalcAprvDAO taDao = new VrtaTaRecalcAprvDAO(dataSource, auditoria());
        return taDao.obtenerValorDeUltimaInsercion(idPeriodo, tipo);
    }

    @Transactional(rollbackFor = Throwable.class)
    public boolean recalcular(Integer idArea, Integer idPeriodoPadre, Integer idPeriodo)
            throws AplicacionExcepcion {
        // Cargo el numero de actualizacion que se procesara en todas las tablas
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        Integer numeroActualizacion = promDao.obtenerUltimoNumeroActualizacion(idPeriodoPadre, idPeriodo, "R");
        numeroActualizacion = numeroActualizacion == null ? 0 : numeroActualizacion + 1;
        System.out.print("recalculo numero de actualizacion->"+numeroActualizacion);

        // Validamos si el numero de actualizacion es 0 que almenos tenga un calculo tipo C
        List<PorcentajeFinalRecalculoDTO> listaPromediosAjustados = promDao.obtenerValoresPromActualizacion(idPeriodoPadre, idPeriodo, numeroActualizacion - 1, "R");
        System.out.println("listaPromediosAjustados->"+listaPromediosAjustados.size()+"\n");
        for (PorcentajeFinalRecalculoDTO listaPromediosAjustado : listaPromediosAjustados) {
            System.out.println("terceros->"+listaPromediosAjustado.getTerIdeRegistro()+"\n");
        }
        if (listaPromediosAjustados.size() == 0) {
            throw new NegocioExcepcion(EMensajeNegocio.ERROR_NO_CALCULO_NORMAL);
        }

        // validar por todas las asociaciones -- creadas hasta la fecha donde estoy recalculando        
        VrtaVarteraprDAO dao = new VrtaVarteraprDAO(dataSource, auditoria());
        VarprVarperregDAO variableDao = new VarprVarperregDAO(dataSource, auditoria());
        
        // Preparar conceptos necesarios
        // variableDao.activarVariablesAprovechamiento(idPeriodo);
        
        calcularPromedios(dao, numeroActualizacion, new ConConceptoDAO(dataSource, auditoria())
                .consultarPorAbreviatura("vb-TAXTM")
                .getUniConcepto().getUniIderegistro(), idPeriodo, idArea, idPeriodoPadre, "R");

        if (calculoQA(numeroActualizacion, idPeriodoPadre, idPeriodo, idArea, "R")) {
            liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_CALCULO_TA, numeroActualizacion);
            liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_CALCULO_PORCENTAJES_CON_DINC, numeroActualizacion);
            liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_CALCULO_PORCENTAJES_SIN_DINC, numeroActualizacion);
            System.out.print("flag->"+flagAjusteDevoluciones);
            if(flagAjusteDevoluciones==1){
                System.out.print("flag solo ajustes");
            
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENO_AJUSTES, numeroActualizacion);
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_AJUSTES_DINC, numeroActualizacion);
            }else if(flagAjusteDevoluciones==3){
                System.out.print("flag solo devoluciones");
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES, numeroActualizacion);
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES_DINC, numeroActualizacion); 
            }else if(flagAjusteDevoluciones==2){
                
               System.out.print("flag ambas");
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENO_AJUSTES, numeroActualizacion);
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_AJUSTES_DINC, numeroActualizacion);
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES, numeroActualizacion);
                liquidadorEditado(idArea, idPeriodoPadre, idPeriodo, EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES_DINC, numeroActualizacion); 
            }
            return true;
            // return true;
        } else {
            return false;
        }
    }

    public boolean liquidadorEditado(Integer idArea, Integer idPeriodoPadre, Integer idPeriodo, Integer idLiquidacionAGenerar, Integer numeroActualizacion)
            throws AplicacionExcepcion {
        LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
        PerPeriodo periodo = new PerPeriodoDAO(dataSource, auditoria()).consultar(idPeriodo.longValue());
        ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
        Integer idLiquidacion = area.getLiqIderegistro().getUniLiquidacion();
        VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());

        VrtaTaRecalcAprvDAO vrtaDao = new VrtaTaRecalcAprvDAO(dataSource, auditoria());

        List<VarprVarperreg> listaCertificadasPeriodoPadre = variableDAO.consultarVariablesCertificadasTA(idArea, idPeriodoPadre);
        if (listaCertificadasPeriodoPadre.isEmpty()) {
            throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO_PADRE);
        }
        delegado.ConceptosSemestralesLiquidados(listaCertificadasPeriodoPadre);
        List<VarprVarperreg> listaCertificadasPeriodoHijo = new ArrayList<>();
//        if (vrtaDao.validarSiHayTACalculado(idPeriodo).size() == 0) {
//            listaCertificadasPeriodoHijo = variableDAO.consultarVariablesCertificadasMensualTA(idArea, idPeriodo, true);
//        } else {
            listaCertificadasPeriodoHijo = variableDAO.consultarVariablesCertificadasMensualTA(idArea, idPeriodo, false);
//        }
        if (listaCertificadasPeriodoHijo.isEmpty() && idLiquidacionAGenerar == EParametro.LIQUIDACION_CALCULO_TA) {
            throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO);
        }
        delegado.ConceptosSemestralesLiquidados(listaCertificadasPeriodoHijo);

        List<ConConcepto> listaConceptos
                = new ConConceptoDAO(dataSource, auditoria()).consultarConceptosColi(idLiquidacionAGenerar);
        ArgumentoDTO argumentos = new ArgumentoDTO()
                .agregar("idEmpresa", auditoria().getIdEmpresa())
                .agregar("idArea", idArea)
                .agregar("idPeriodo", idPeriodo)
                .agregar("idUsuario", auditoria().getIdUsuario())
                .agregar("idConsecutivo",numeroActualizacion);

//        String ids = area.getArprLiquidaciones();
        List<String> idsLiquidacion = new ArrayList<>();
        idsLiquidacion.add(0, String.valueOf(EParametro.LIQUIDACION_CALCULO_TA));
        idsLiquidacion.add(1, String.valueOf(EParametro.LIQUIDACION_CALCULO_PORCENTAJES_SIN_DINC));
        idsLiquidacion.add(2, String.valueOf(EParametro.LIQUIDACION_CALCULO_PORCENTAJES_CON_DINC));
        idsLiquidacion.add(3, String.valueOf(EParametro.LIQUIDACION_APROVECHAMIENO_AJUSTES));
        idsLiquidacion.add(4, String.valueOf(EParametro.LIQUIDACION_APROVECHAMIENTO_AJUSTES_DINC));
        idsLiquidacion.add(5, String.valueOf(EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES));
        idsLiquidacion.add(6, String.valueOf(EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES_DINC));
        int i = 0;
        for (ConConcepto concepto : listaConceptos) {
            Log.info("concepto->" + i + " :" + concepto.getConAbreviatura());
            delegado.liquidarConcepto(concepto, idsLiquidacion, argumentos);
            i++;
        }
        guardarResultadosCalculoTA(delegado,
                idPeriodo,
                periodo,
                area,
                idLiquidacionAGenerar,
                vrtaDao,
                numeroActualizacion);
        return true;
    }

    public boolean guardarResultadosCalculoTA(LiquidarConceptoDelegado delegado,
            Integer idPeriodo,
            PerPeriodo periodo,
            ArprAreaprestacion area,
            Integer idLiquidacionAGenerar,
            VrtaTaRecalcAprvDAO vrtaDao,
            Integer numeroActualizacion)
            throws AplicacionExcepcion {
        boolean estDinc=true;
        VrtaHistoricoRecalculosDAO vrtaHistoricoRecalcDao = new VrtaHistoricoRecalculosDAO(dataSource, auditoria());
        List<ValorConceptoDTO> listaConceptosLiquidados = delegado.getListaConceptosLiquidados();
        // Integer numeroActualizacion = vrtaDao.obtenerValorDeUltimaInsercionTabla(idPeriodo);
        for (ValorConceptoDTO conceptoLiquidado : listaConceptosLiquidados) {
            // modificar para almacenar con otro estado
            Integer idVarperreg = calculoServicio.guardarVariablesRecalculoAprovechamiento(conceptoLiquidado, area, periodo);

            if (idLiquidacionAGenerar == EParametro.LIQUIDACION_CALCULO_TA) {
                if (conceptoLiquidado.getConcepto().getUniConcepto().getUniIderegistro() == TA_CON_DINC
                        || conceptoLiquidado.getConcepto().getUniConcepto().getUniIderegistro() == TA_SIN_DINC) {
                    vrtaDao.insertar(new VrtaTaRecalcAprv(numeroActualizacion, idVarperreg, idPeriodo,
                            conceptoLiquidado.getValorTotal(),
                            new Timestamp(new Date().getTime()),
                            auditoria().getIdUsuario(),
                            conceptoLiquidado.getConcepto().getUniConcepto().getUniIderegistro().equals(TA_CON_DINC)));
                }

            } else {
                if(idLiquidacionAGenerar == EParametro.LIQUIDACION_CALCULO_PORCENTAJES_CON_DINC
                        || idLiquidacionAGenerar == EParametro.LIQUIDACION_APROVECHAMIENTO_DEVOLUCIONES_DINC
                        || idLiquidacionAGenerar == EParametro.LIQUIDACION_APROVECHAMIENTO_AJUSTES_DINC){
                    estDinc=true;
                }else{
                    estDinc=false;
                }
                vrtaHistoricoRecalcDao.insertar(new VrtaHistoricoRecalculos(numeroActualizacion, idVarperreg, idPeriodo, estDinc));
            }
        }
        return true;

    }
    @Transactional(readOnly = true)
    public List<ConConcepto> obtenerConceptos()
            throws AplicacionExcepcion {
        return new VrtaVarteraprPromDAO(dataSource, auditoria()).obtenerConceptos();
    }
    
    @Transactional(readOnly = true)
    public List<ConceptosCalculadosDTO> obtenerConceptosCalculados(Integer numeroActualziacion, Integer idPeriodo)
            throws AplicacionExcepcion {
        if (numeroActualziacion == 0) {
            return new VrtaHistoricoRecalculosDAO(dataSource, auditoria())
                .obtenerConceptosCalculadosIniciales(numeroActualziacion, idPeriodo);
        } else {
            return new VrtaHistoricoRecalculosDAO(dataSource, auditoria())
                .obtenerConceptosCalculados(numeroActualziacion, idPeriodo);
        }
    }
    
    @Transactional(readOnly = true)
    public List<ListadoTotalPorcentajesRecalculoDTO> obtenerPorcentajesAsociacion(Integer idPeriodo, Integer numeroActualizacion)
            throws AplicacionExcepcion {
        VrtaPromFinalesDAO prfinalesDao = new VrtaPromFinalesDAO(dataSource, auditoria());
        List<PorcentajeFinalRecalculoDTO> listaPromediosFinales = new ArrayList<PorcentajeFinalRecalculoDTO>();
        List<ListadoTotalPorcentajesRecalculoDTO> listadoRespuesta = new ArrayList<>();
        
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        

        // Se verifica si hay promedios con diferencias en la tabla promedios finales
        listaPromediosFinales = prfinalesDao.obtenerTercerPorcentaje(idPeriodo, numeroActualizacion);
        
        if (listaPromediosFinales.size() > 0) {
            listaPromediosFinales.stream().forEach(promedioFinal -> {
                try {                            
                    List<PorcentajeRecalculoDTO> promedioAjustado = promDao.obtenerPromedioAjustado(idPeriodo, promedioFinal.getTerIdeRegistro(), numeroActualizacion);
                    List<PorcentajeRecalculoDTO> promedioSinAjuste = promDao.obtenerPromedioSinAjuste(idPeriodo, promedioFinal.getTerIdeRegistro(), numeroActualizacion-1);

                    Double ajustado = promedioAjustado.get(0).getVrtaPromValor();
                    Double sinAjuste = promedioSinAjuste.isEmpty()? 0.0 : promedioSinAjuste.get(0).getVrtaPromValor();
                    String nombreAsociacion = promedioAjustado.get(0).getTerNomcompleto();
                    
                    listadoRespuesta.add(new ListadoTotalPorcentajesRecalculoDTO(sinAjuste,
                                        ajustado, ajustado - sinAjuste, promedioFinal.getVrtaPromValor(), nombreAsociacion,promedioFinal.getConIderegistro()));
                } catch (PersistenciaExcepcion ex) {
                    System.out.print(ex.getMensaje());
                } catch (AplicacionExcepcion ex) {
                    System.out.print(ex.getMensaje());
                }
            });
        }

        return listadoRespuesta;
    } //registrar porcentajes 
    @Transactional(readOnly = true)
    public List<VrtaTaRecalcAprv> obtenerHistorico(Integer idPeriodo)
            throws AplicacionExcepcion {
        return new VrtaTaRecalcAprvDAO(dataSource, auditoria()).obtenerHistorico(idPeriodo);
    }

    //Registrar terceros porcentaje
    private boolean guardarPorcentajeTerceroTa(
            Long idTercero,
            Integer numeroActualizacionTercero,
            Integer periodoRecalculo)
            throws AplicacionExcepcion {
        
        VrtaTerceroPorcenTaDao vrtaTerceroPorcenTaDao = new VrtaTerceroPorcenTaDao(dataSource, auditoria());

        vrtaTerceroPorcenTaDao.insertar(new VrtaTerceroPorcenTa(numeroActualizacionTercero, 0, periodoRecalculo,
                idTercero, new Timestamp(new Date().getTime()), auditoria().getIdUsuario(), 0D));

        return true;
    }
    
    // Actualizar promedios porcentaje
    private boolean actualizarPromedioTerceroTa(
            Long idTercero,
            Integer numeroActualizacionTercero,
            Integer periodoRecalculo,
            Integer numeroActualizacionTa,
            Double promedioCalculado)
            throws AplicacionExcepcion {

        VrtaTerceroPorcenTaDao vrtaTerceroPorcenTaDao = new VrtaTerceroPorcenTaDao(dataSource, auditoria());

        vrtaTerceroPorcenTaDao.settearPromedioTerceroTa(new VrtaTerceroPorcenTa(numeroActualizacionTercero, numeroActualizacionTa, periodoRecalculo,
                idTercero, new Timestamp(new Date().getTime()), auditoria().getIdUsuario(), promedioCalculado));

        return true;
    }
    
    @Transactional(rollbackFor = Throwable.class)
    public void eliminarTonelada(Integer idTonelada)throws AplicacionExcepcion {

        VrtaVarteraprDAO vrtaVarteraprDAO = new VrtaVarteraprDAO(dataSource, auditoria());
        VrtaVarteraprHistDAO vrtaVarteraprHistDAO = new VrtaVarteraprHistDAO(dataSource, auditoria());
        
        vrtaVarteraprHistDAO.eliminarToneladaByVrta(idTonelada);
        vrtaVarteraprDAO.eliminarTonelada(idTonelada);
    }
}
