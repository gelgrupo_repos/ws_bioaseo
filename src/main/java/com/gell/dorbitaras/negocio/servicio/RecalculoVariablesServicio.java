/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.EParametro;
import com.gell.dorbitaras.negocio.delegado.LiquidarConceptoDelegado;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ArprAreaprestacionDAO;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.PerPeriodoDAO;
import com.gell.dorbitaras.persistencia.dao.RacoRanconceptDAO;
import com.gell.dorbitaras.persistencia.dao.VarprVarperregDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaHistoricoRecalculoDevolucionesDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaHistoricoRecalculosDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaQaTotalHistoricoDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaTaRecalcAprvDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprHistDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprPromDAO;
import com.gell.dorbitaras.persistencia.dao.crud.VrtaHistoricoRecalculoDevolucionesCRUD;
import com.gell.dorbitaras.persistencia.dto.ActualizacionesToneladasPeriodoDTO;
import com.gell.dorbitaras.persistencia.dto.ConConceptoCustomDTO;
import com.gell.dorbitaras.persistencia.dto.ConceptosDevolucionesDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.gell.dorbitaras.persistencia.dto.facturar.ArgumentoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoGestionVariables;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.RacoRanconcept;
import java.util.ArrayList;
import jline.internal.Log;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculoDevoluciones;
import com.gell.estandar.util.ValidarDato;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jrangel
 */
@Service
public class RecalculoVariablesServicio extends GenericoServicio {

    @Autowired
    private static final int LIQUIDADOR_SEMESTRAL = 2407;
    private static final int LIQUIDADOR_ENERGIA = 5568;
    private static final int LIQUIDADOR_GAS = 5569;
    private static final int LIQUIDADOR_TLU = 5674;
    private static final int LIQUIDADOR_TBL = 5675;
    private static final int LIQUIDADOR_TRT = 5676;
    private static final int LIQUIDADOR_TDF = 5677;
    private static final int LIQUIDADOR_TTL = 5678;
    private static final int LIQUIDADOR_TA = 5679;
    private static final int LIQUIDADOR_TFE = 5680;
    private static final int LIQUIDADOR_TTA = 5681;
    private static final int LIQUIDADOR_TAD = 5893;
    private static final int LIQUIDADOR_TFED = 5894;
    private static final int LIQUIDADOR_TTAD = 5895;
    
    
    /**
     * obtener conceptos tipo constante
     *
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public List<ConConceptoCustomDTO> obtenerConceptosTipoConstante() throws AplicacionExcepcion {
        ConConceptoDAO dao = new ConConceptoDAO(dataSource, auditoria());
        return dao.obtenerConceptosConstates();
    }
    
    /**
     * obtener conceptos tipo base
     * @param uniLiquidacion id de liquidacion asociada al concepto
     * @return Lista de las rutas dependiendo los parametros
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public List<ConConceptoGestionVariables> obtenerConceptosTipoBase(Integer uniLiquidacion) throws AplicacionExcepcion {
        ConConceptoDAO dao = new ConConceptoDAO(dataSource, auditoria());
        return dao.consultarConceptoYDescripcion(uniLiquidacion);
    }

 
     /**
     * Actualizar conceptos tipo constante
     *
     * @param idConcepto id de concepto para actualizar
     * @param valor valor para actualizar
     * @return booleano que indica resultado
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public boolean actualizarTipoConstante(
            Integer idConcepto,
            Double valor) throws AplicacionExcepcion {
        ConConceptoDAO dao = new ConConceptoDAO(dataSource, auditoria());
        boolean resultado = false;
        try{
            dao.cambiarValorConcepto(idConcepto, valor);
            dao.cambiarValorHistoricoConcepto(idConcepto, valor, auditoria().getIdUsuario().toString());
            resultado = true;
        }catch(PersistenciaExcepcion ex){
            ex.printStackTrace();
        }
        return resultado;
    }
    
     /**
     * Actualizar conceptos tipo Base
     *
     * @param idConcepto id de concepto para actualizar
     * @param valor valor para actualizar
     * @return booleano que indica resultado
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public boolean actualizarTipoBase(
            Integer idConcepto,
            Integer idPeriodo,
            Integer raco_ideregistr,            
            Double valor,
            String descripcion
    ) throws AplicacionExcepcion {
        ConConceptoDAO dao = new ConConceptoDAO(dataSource, auditoria());
        boolean resultado = false;
        try{
            dao.cambiarValorBase(idConcepto, idPeriodo, raco_ideregistr, valor, descripcion);
            resultado = true;
        }catch(PersistenciaExcepcion ex){
            ex.printStackTrace();
        }
        return resultado;
    }
    
     /**
     * Obtener tabla histrico recalculo devoluciones
     *
     * @param idConcepto id de concepto para actualizar
     * @param valor valor para actualizar
     * @return booleano que indica resultado
     * @throws PersistenciaExcepcion Error al ejecutar la sentencia
     */
    @Transactional(readOnly = true)
    public List<VrtaHistoricoRecalculoDevoluciones> obtenerHistoricoDevoluciones(
            Integer idPeriodo            
    ) throws AplicacionExcepcion {
        VrtaHistoricoRecalculoDevolucionesDAO dao = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        return dao.obtenerHistoricoDevoluciones(idPeriodo);
    }
    
    /**
    * Obtener numero de actualizacion Historico Devoluciones
    *
    * @param idArea id de concepto para actualizar
    * @param idPeriodo id de concepto para actualizar
    * @param idPeriodoPadre valor para actualizar
    * @return integer con el numero
    * @throws PersistenciaExcepcion Error al ejecutar la sentencia
    */
    @Transactional(readOnly = true)
    public Integer obtenerUltimaActualizacion(
            Integer idArea,
            Integer idPeriodo
    ) throws AplicacionExcepcion {
        VrtaHistoricoRecalculoDevolucionesCRUD vrtaHistoricoRecalcDevolucionDao = new VrtaHistoricoRecalculoDevolucionesCRUD(dataSource, auditoria());
        Integer numeroActualizacion = vrtaHistoricoRecalcDevolucionDao.obtenerValorDeUltimaInsercionTabla(idPeriodo);
        
        return numeroActualizacion;
    }
    
    /**
    * Obtener numero de actualizacion Historico Devoluciones
    *
    * @param idArea id de concepto para actualizar
    * @param idPeriodo id de concepto para actualizar
    * @param idPeriodoPadre valor para actualizar
    * @return integer con el numero
    * @throws PersistenciaExcepcion Error al ejecutar la sentencia
    */
    @Transactional(readOnly = true)
    public Integer obtenerUltimoRecalculoAprobado(
            Integer idArea,
            Integer idPeriodo
    ) throws AplicacionExcepcion {
        VrtaHistoricoRecalculoDevolucionesCRUD vrtaHistoricoRecalcDevolucionDao = new VrtaHistoricoRecalculoDevolucionesCRUD(dataSource, auditoria());
        Integer numeroActualizacion = vrtaHistoricoRecalcDevolucionDao.obtenerUltimoRecalculoAprobado(idPeriodo);
        
        return numeroActualizacion;
    }
    
    @Transactional(timeout = 1000)
    public boolean recalculoSemestral(
            Integer idArea,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion) 
    throws AplicacionExcepcion {
        List<String> idsLiquidacion = new ArrayList<String>();
        List<VarprVarperreg> listaCertificadasPeriodoPadre = new ArrayList<VarprVarperreg>();
        ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador de la liquidación es obligatorio")
            .agregar(idPeriodoPadre, "requerido", "El identificador del periodo es obligatorio")
            .validar();
        
        VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());
        listaCertificadasPeriodoPadre = variableDAO.consultarVariablesIniciales(idArea, idPeriodo);
        if (variableDAO.consultarVariablesIniciales(idArea, idPeriodo).size() == 0) {
            throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_INICIALES);
        }
        VrtaHistoricoRecalculoDevolucionesDAO vrtaHistoricoDevolucionDAO = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
        ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
        PerPeriodo periodo = new PerPeriodoDAO(dataSource, auditoria()).consultar(idPeriodoPadre.longValue());
        Integer idLiquidacion = area.getLiqIderegistro().getUniLiquidacion();
        
        System.out.print("idLiquidation->"+idLiquidacion.toString());
        idsLiquidacion.add(0, String.valueOf(LIQUIDADOR_SEMESTRAL));
        idLiquidacion = LIQUIDADOR_SEMESTRAL;
        
        List<ConConcepto> listaConceptos
            = new ConConceptoDAO(dataSource, auditoria()).consultarConceptosColi(idLiquidacion);
        ArgumentoDTO argumentos = new ArgumentoDTO()
            .agregar("idEmpresa", auditoria().getIdEmpresa())
            .agregar("idArea", idArea)
            .agregar("idPeriodo", idPeriodoPadre)
            .agregar("idUsuario", auditoria().getIdUsuario());
        int i=0;
        for (ConConcepto concepto : listaConceptos) {
            Log.info("concepto->"+i+" :"+concepto.getConAbreviatura());
            delegado.liquidarConcepto(concepto, idsLiquidacion, argumentos);
            i++;
        }
        
        List<ValorConceptoDTO> listaConceptosLiquidados = delegado.getListaConceptosLiquidados();
        for (ValorConceptoDTO conceptoLiquidado : listaConceptosLiquidados) {
            Integer idVarperreg = guardarConceptosLiduidacion(conceptoLiquidado, area, periodo);

            // if (idLiquidacionAGenerar == EParametro.LIQUIDACION_DEVOLUCIONES) {
                vrtaHistoricoDevolucionDAO.insertar(
                        new VrtaHistoricoRecalculoDevoluciones(
                                numeroActualizacion == null ? 0 : numeroActualizacion + 1,
                                idVarperreg,
                                idPeriodoPadre,
                                String.valueOf("I"),
                                String.valueOf("Calculo Inicial"),
                                new Timestamp(new Date().getTime()),
                                null
                            ));
            // }
        }
        return true;
    }
    
    @Transactional(timeout = 1000)
    public boolean recalculoMensual(
            Integer idArea,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion) 
    throws AplicacionExcepcion {
        List<String> idsLiquidacion = new ArrayList<String>();
        List<VarprVarperreg> listaCertificadasPeriodoPadre = null;
        ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador de la liquidación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
        
        VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());
        LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
        ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
        PerPeriodo periodo = new PerPeriodoDAO(dataSource, auditoria()).consultar(idPeriodo.longValue());
        Integer idLiquidacion = area.getLiqIderegistro().getUniLiquidacion();
        VrtaHistoricoRecalculoDevolucionesDAO vrtaHistoricoDevolucionDAO = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        
        listaCertificadasPeriodoPadre = variableDAO.consultarVariablesCertificadas(idArea, idPeriodoPadre);
        delegado.ConceptosSemestralesLiquidados(listaCertificadasPeriodoPadre);
        String ids = area.getArprLiquidaciones();
        System.out.print("ids->"+getValuesForGivenKey(ids,"liquidacion"));
        idsLiquidacion = getValuesForGivenKey(ids,"liquidacion");
        idLiquidacion = Integer.parseInt(idsLiquidacion.get(0));
        
        List<ConConcepto> listaConceptos
            = new ConConceptoDAO(dataSource, auditoria()).consultarConceptosColi(idLiquidacion);
        ArgumentoDTO argumentos = new ArgumentoDTO()
            .agregar("idEmpresa", auditoria().getIdEmpresa())
            .agregar("idArea", idArea)
            .agregar("idPeriodo", idPeriodo)
            .agregar("idUsuario", auditoria().getIdUsuario());
        
        int i=0;
        for (ConConcepto concepto : listaConceptos) {
            Log.info("concepto->"+i+" :"+concepto.getConAbreviatura());
            delegado.liquidarConcepto(concepto, idsLiquidacion, argumentos);
            i++;
        }
        
        guardarResultadosDevolucionesHistorico(
            delegado,
            idPeriodo,
            periodo,
            area,
            Integer.parseInt(idsLiquidacion.get(0)),
            vrtaHistoricoDevolucionDAO,
            numeroActualizacion
        );
        
        return true;
    }
    
    
    @Transactional(rollbackFor = Throwable.class)
    public boolean recalculoDiferencias(
            Integer idArea,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion) 
    throws AplicacionExcepcion {
        List<Integer> liquidadores = new ArrayList<Integer>();
        List<VarprVarperreg> listaCertificadasPeriodoPadre = null;
        ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador de la liquidación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
        
        VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());
        
        ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
        PerPeriodo periodo = new PerPeriodoDAO(dataSource, auditoria()).consultar(idPeriodo.longValue());
        VrtaHistoricoRecalculoDevolucionesDAO vrtaHistoricoDevolucionDAO = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        
        listaCertificadasPeriodoPadre = variableDAO.consultarVariablesCertificadasDevoluciones(idArea, idPeriodo);
        
        
        liquidadores.add(0, LIQUIDADOR_ENERGIA);
        liquidadores.add(1, LIQUIDADOR_GAS);
        liquidadores.add(2, LIQUIDADOR_TLU);
        liquidadores.add(3, LIQUIDADOR_TBL);
        liquidadores.add(4, LIQUIDADOR_TRT);
        liquidadores.add(5, LIQUIDADOR_TDF);
        liquidadores.add(6, LIQUIDADOR_TTL);
        liquidadores.add(7, LIQUIDADOR_TA);
        liquidadores.add(8, LIQUIDADOR_TFE);
        liquidadores.add(9, LIQUIDADOR_TTA);
        liquidadores.add(10, LIQUIDADOR_TAD);
        liquidadores.add(11, LIQUIDADOR_TFED);
        liquidadores.add(12, LIQUIDADOR_TTAD);
        
        // Recorrer liquidadores columnas
        for (Integer liquidacion: liquidadores) {
            System.out.print("idLiquidation->"+liquidacion.toString());
            LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
            
            delegado.ConceptosSemestralesLiquidados(listaCertificadasPeriodoPadre);
            
            List<String> idsLiquidacion = new ArrayList<String>();
            idsLiquidacion.add(0, String.valueOf(liquidacion));
            
            List<ConConcepto> listaConceptos
            = new ConConceptoDAO(dataSource, auditoria()).consultarConceptosColiOrder(liquidacion);
            ArgumentoDTO argumentos = new ArgumentoDTO()
                .agregar("idEmpresa", auditoria().getIdEmpresa())
                .agregar("idArea", idArea)
                .agregar("idPeriodo", idPeriodo)
                .agregar("idUsuario", auditoria().getIdUsuario());
            int i=0;
            for (ConConcepto concepto : listaConceptos) {
                Log.info("concepto->"+i+" :"+concepto.getConAbreviatura());
                delegado.liquidarConcepto(concepto, idsLiquidacion, argumentos);
                i++;
            }
            
            List<ValorConceptoDTO> listaConceptosLiquidados = delegado.getListaConceptosLiquidados();
            
            // Se extraen los conceptos filtrados solo del liquidador
            List<ValorConceptoDTO> conceptosLiquidador = new ArrayList<>();
            for (ConConcepto concepto : listaConceptos) {
                conceptosLiquidador.add(listaConceptosLiquidados.stream()
                                                .filter((valor) -> valor.getConcepto().getUniConcepto().getUniIderegistro() == concepto.getUniConcepto().getUniIderegistro())
                                                .findFirst()
                                                .orElse(null));;            
            }
            
            // Verificacion si hay diferencia o no para guardar por liquidador
            if (verificarCambios(conceptosLiquidador, liquidacion)) {
                guardarDiferenciasLiquidador(
                    conceptosLiquidador,
                    idPeriodo,
                    periodo,
                    area,
                    liquidacion,
                    vrtaHistoricoDevolucionDAO,
                    numeroActualizacion
                );
            }
        }

        return true;
    }
    
    /**
    * Método encargado validar si hay diferencias en valores dependiendo del liquidador
    *
    * @param List<ValorConceptoDTO> Listado de conceptos.
    * @param liquidacion Liquidacion a evaluar
    * @throws PersistenciaExcepcion
    */
    public boolean verificarCambios(List<ValorConceptoDTO> conceptosLiquidados,
            Integer liquidacion)
            throws AplicacionExcepcion {
    
        String filtro;
        List<String> filtros = new ArrayList<String>();
        ValorConceptoDTO concepto = new ValorConceptoDTO();
        boolean guardarConceptos = false;
        
        switch(liquidacion) {
            case LIQUIDADOR_ENERGIA:
                filtro = "ms_ENERGIA";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_GAS:
                filtro = "ms-GAS";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_TLU:
                filtro = "ms-TLU";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_TBL:
                filtro = "ms-TTBL";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_TRT:
                filtros.add(0, "ms-CTRTE1");
                filtros.add(1, "ms-CTRTE2");
                filtros.add(2, "ms-CTRTE3");
                filtros.add(3, "ms-CTRTE4");
                filtros.add(4, "ms-CTRTE5");
                filtros.add(5, "ms-CTRTE6");
                filtros.add(6, "ms-CTRTPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TDF:
                filtros.add(0, "ms-LTDFE1");
                filtros.add(1, "ms-LTDFE2");
                filtros.add(2, "ms-LTDFE3");
                filtros.add(3, "ms-LTDFE4");
                filtros.add(4, "ms-LTDFE5");
                filtros.add(5, "ms-LTDFE6");
                filtros.add(6, "ms-LTDFPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TTL:
                filtros.add(0, "ms-LTTLE1");
                filtros.add(1, "ms-LTTLE2");
                filtros.add(2, "ms-LTTLE3");
                filtros.add(3, "ms-LTTLE4");
                filtros.add(4, "ms-LTTLE5");
                filtros.add(5, "ms-LTTLE6");
                filtros.add(6, "ms-LTTLPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TA:
                filtro = "ms-LTA";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_TFE:
                filtros.add(0, "ms-TFEE1");
                filtros.add(1, "ms-TFEE2");
                filtros.add(2, "ms-TFEE3");
                filtros.add(3, "ms-TFEE4");
                filtros.add(4, "ms-TFEE5");
                filtros.add(5, "ms-TFEE6");
                filtros.add(6, "ms-TFEPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TTA:
                filtros.add(0, "ms-TTAE1");
                filtros.add(1, "ms-TTAE2");
                filtros.add(2, "ms-TTAE3");
                filtros.add(3, "ms-TTAE4");
                filtros.add(4, "ms-TTAE5");
                filtros.add(5, "ms-TTAE6");
                filtros.add(6, "ms-TTAPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TAD:
                filtro = "ms-LTAD";
                concepto = conceptosLiquidados.stream()
                    .filter(x -> filtro.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                guardarConceptos = concepto.getValorTotal() != 0;
                break;
            case LIQUIDADOR_TFED:
                filtros.add(0, "ms-TFEDE1");
                filtros.add(1, "ms-TFEDE2");
                filtros.add(2, "ms-TFEDE3");
                filtros.add(3, "ms-TFEDE4");
                filtros.add(4, "ms-TFEDE5");
                filtros.add(5, "ms-TFEDE6");
                filtros.add(6, "ms-TFEDPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
            case LIQUIDADOR_TTAD:
                filtros.add(0, "ms-TTADE1");
                filtros.add(1, "ms-TTADE2");
                filtros.add(2, "ms-TTADE3");
                filtros.add(3, "ms-TTADE4");
                filtros.add(4, "ms-TTADE5");
                filtros.add(5, "ms-TTADE6");
                filtros.add(6, "ms-TTADPP");
                
                for (String conceptoTRT: filtros) {
                    concepto = conceptosLiquidados.stream()
                    .filter(x -> conceptoTRT.equals(x.getConcepto().getConAlias()))
                    .findAny()
                    .orElse(null);
                
                    guardarConceptos = concepto.getValorTotal() != 0 ? true : false;
                    if (guardarConceptos) {
                        return true;
                    }
                }
                break;
        }
        
        return guardarConceptos;
    }

    /**
    * Método encargado de guardar las variables calculadas.
    *
    * @param conceptoLiq Datos de la variable a guardar.
    * @param area Datos del área de prestación.
    * @param periodo Datos del periodo.
    * @throws PersistenciaExcepcion
    */
    public boolean guardarResultadosDevolucionesHistorico(LiquidarConceptoDelegado delegado,
            Integer idPeriodo,
            PerPeriodo periodo,
            ArprAreaprestacion area,
            Integer idLiquidacionAGenerar,
            VrtaHistoricoRecalculoDevolucionesDAO vrtaHistoricoDevolucionDAO,
            Integer numeroActualizacion)
            throws AplicacionExcepcion {

        VrtaHistoricoRecalculoDevolucionesCRUD vrtaHistoricoRecalcDevolucionDao = new VrtaHistoricoRecalculoDevolucionesCRUD(dataSource, auditoria());
        List<ValorConceptoDTO> listaConceptosLiquidados = delegado.getListaConceptosLiquidados();
        for (ValorConceptoDTO conceptoLiquidado : listaConceptosLiquidados) {
            // modificar para almacenar con otro estado
            Integer idVarperreg = guardarConceptosLiduidacion(conceptoLiquidado, area, periodo);

            // if (idLiquidacionAGenerar == EParametro.LIQUIDACION_DEVOLUCIONES) {
                vrtaHistoricoDevolucionDAO.insertar(
                        new VrtaHistoricoRecalculoDevoluciones(
                                numeroActualizacion == null ? 0 : numeroActualizacion + 1,
                                idVarperreg,
                                idPeriodo,
                                String.valueOf("P"),
                                String.valueOf("Calculo Inicial"),
                                new Timestamp(new Date().getTime()),
                                null
                            ));
            // }
        }
        return true;
    }
    
    /**
    * Método encargado de guardar las variables calculadas.
    *
    * @param conceptoLiq Datos de la variable a guardar.
    * @param area Datos del área de prestación.
    * @param periodo Datos del periodo.
    * @throws PersistenciaExcepcion
    */
    public boolean guardarDiferenciasLiquidador(List<ValorConceptoDTO> listaConceptosLiquidados,
            Integer idPeriodo,
            PerPeriodo periodo,
            ArprAreaprestacion area,
            Integer idLiquidacionAGenerar,
            VrtaHistoricoRecalculoDevolucionesDAO vrtaHistoricoDevolucionDAO,
            Integer numeroActualizacion)
            throws AplicacionExcepcion {
        for (ValorConceptoDTO conceptoLiquidado : listaConceptosLiquidados) {
            // modificar para almacenar con otro estado
            Integer idVarperreg = guardarConceptosLiduidacion(conceptoLiquidado, area, periodo);

            // if (idLiquidacionAGenerar == EParametro.LIQUIDACION_DEVOLUCIONES) {
                vrtaHistoricoDevolucionDAO.insertar(
                        new VrtaHistoricoRecalculoDevoluciones(
                                numeroActualizacion == null ? 0 : numeroActualizacion + 1,
                                idVarperreg,
                                idPeriodo,
                                String.valueOf("P"),
                                String.valueOf("Calculo Inicial"),
                                new Timestamp(new Date().getTime()),
                                null
                            ));
            // }
        }
        return true;
    }
    
     /**
    * Método encargado de guardar las variables calculadas para recalculo de
    * aprovechamiento.
    *
    * @param conceptoLiq Datos de la variable a guardar.
    * @param area Datos del área de prestación.
    * @param periodo Datos del periodo.
    * @throws PersistenciaExcepcion
    */
    public Integer guardarConceptosLiduidacion(
        ValorConceptoDTO conceptoLiq,
        ArprAreaprestacion area,
        PerPeriodo periodo
    )
        throws PersistenciaExcepcion
    {
        ConConcepto conConcepto = conceptoLiq.getConcepto();
        VarprVarperregDAO variableDao = new VarprVarperregDAO(dataSource, auditoria());
        VarprVarperreg variableExistente = variableDao.consultarExistente(conConcepto, area, periodo);
        if (null != variableExistente) {
            variableDao.inactivarVariable(variableExistente.getVarprIderegistro());
        }
        RacoRanconcept rango = new RacoRanconceptDAO(dataSource, auditoria()).consultarIdRango(conceptoLiq);
        System.out.print("......................Guardar Variables........................\n");
        System.out.println("Concepto->"+conConcepto.getUniConcepto().getUniIderegistro()+" - valor->"+conceptoLiq.getValorTotal());
        VarprVarperreg variable = new VarprVarperreg()
            .setArprIderegistro(area)
            .setConIderegistro(conConcepto)
            .setRacoIderegistro(rango)
            .setPerIderegistro(periodo)
            .setUsuIderegistroGb(auditoria().getIdUsuario())
            .setEmpIderegistro(auditoria().getIdEmpresa())
            .setVarprFecgrabacion(new Date())
            .setVarprEstadoRegistro(EEstadoGenerico.ACTIVO)
            .setVarprEstado(EEstadoGenerico.CERTIFICADO)
            .setVarprValor(conceptoLiq.getValorTotal());
        return variableDao.insertarAprovechamiento(variable);
    }
    
    public List<ConceptosDevolucionesDTO> obtenerConceptosCalculados(Integer idArea, Integer numeroActualziacion, Integer idPeriodo)
            throws AplicacionExcepcion {
        VrtaHistoricoRecalculoDevolucionesDAO daoRecalculo = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        
        // Si es el calculo inicial, trae los calculos de la actualizacion y los ultimos de varpereg
        if (numeroActualziacion == 0) {
            return daoRecalculo.obtenerConceptosCalculadosIniciales(idArea, numeroActualziacion, idPeriodo);
        } else {
            return daoRecalculo.obtenerConceptosCalculados(idArea, numeroActualziacion, idPeriodo);
        }
    }
    
    public boolean actualizarHistorico(Integer idArea, Integer numeroActualziacion, Integer idPeriodoPadre, Integer idPeriodo, String accion)
            throws AplicacionExcepcion {
        VrtaHistoricoRecalculoDevolucionesDAO daoRecalculo = new VrtaHistoricoRecalculoDevolucionesDAO(dataSource, auditoria());
        
        daoRecalculo.actualizarHistorico(idArea, numeroActualziacion, idPeriodoPadre, idPeriodo, accion);
        if (accion == EEstadoGenerico.DENEGAR) {
            Integer numeroActualizacion = obtenerUltimoRecalculoAprobado(idArea, idPeriodo);
            
            VrtaHistoricoRecalculoDevolucionesCRUD vrtaHistoricoRecalcDevolucionDao = new VrtaHistoricoRecalculoDevolucionesCRUD(dataSource, auditoria());
            vrtaHistoricoRecalcDevolucionDao.denegarRecalculo(idArea, idPeriodoPadre, idPeriodo, numeroActualizacion);
        }
        
        return true;
    }
    
    public List<String> getValuesForGivenKey(String jsonArrayStr, String key) {
    JSONArray jsonArray = new JSONArray(jsonArrayStr);
    return IntStream.range(0, jsonArray.length())
      .mapToObj(index -> ((JSONObject)jsonArray.get(index)).optString(key))
      .collect(Collectors.toList());
} 
}
