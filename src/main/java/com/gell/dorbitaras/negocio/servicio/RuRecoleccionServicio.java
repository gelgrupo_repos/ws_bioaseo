/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.HrrHorrecoleccionDAO;
import com.gell.dorbitaras.persistencia.dao.RureRutrecoleccionDAO;
import com.gell.dorbitaras.persistencia.dao.RutRutaDAO;
import com.gell.dorbitaras.persistencia.entidades.HrrHorrecoleccion;
import com.gell.dorbitaras.persistencia.entidades.RureRutrecoleccion;
import com.gell.dorbitaras.persistencia.entidades.RutRuta;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.ValidarDato;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class RuRecoleccionServicio extends GenericoServicio
{

  /**
   * Método que controla la consulta de ruta macro o micro según parametro
   *
   * @param tipoRuta macro o micro
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<RutRuta> consultarMacro(String tipoRuta)
          throws PersistenciaExcepcion
  {
    return new RutRutaDAO(dataSource, auditoria())
            .consultarMacroMicro(tipoRuta);
  }

  /**
   * Método encargado de consultas las microrutas que no estan asociadas a una
   * macro ruta.
   *
   * @return Lista de micro rutas.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<RutRuta> consultarMicro()
          throws AplicacionExcepcion
  {
    return new RutRutaDAO(dataSource, auditoria())
            .consultarMicroRutas();
  }

  /**
   * Método que controla los datos al guardar
   *
   * @param rutrecoleccion Información del registro consulta
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardar(RureRutrecoleccion rutrecoleccion)
          throws AplicacionExcepcion
  {

    rutrecoleccion.validar();
    List<HrrHorrecoleccion> listaDias = rutrecoleccion.getListaDias();
    HrrHorrecoleccionDAO diaRecoleccionDAO = new HrrHorrecoleccionDAO(dataSource, auditoria());
    RureRutrecoleccionDAO rutRecoleccionDAO = new RureRutrecoleccionDAO(dataSource, auditoria());
    Integer saveIdeTemporal = rutrecoleccion.getRureIderegistro();
    HrrHorrecoleccion dias;

    if (saveIdeTemporal != null) {
      //Editar 
      rutrecoleccion.setUsuIderegistroAc(auditoria().getIdUsuario());
      rutRecoleccionDAO.guardarMacroRuta(rutrecoleccion);
    }
    if (saveIdeTemporal == null) {
      //Guardar 
      Integer id = rutRecoleccionDAO.consultarId(
              rutrecoleccion.getArprIderegistro().getArprIderegistro(),
              rutrecoleccion.getRutIdemacruta());
      Integer numero = rutRecoleccionDAO.consultarRuta(
              rutrecoleccion.getArprIderegistro().getArprIderegistro(),
              rutrecoleccion.getRutIdemacruta());
      if (numero != null) {
        rutrecoleccion.setRureIderegistro(id);
      }
      rutRecoleccionDAO.guardarMacroRuta(rutrecoleccion);
    }
    for (HrrHorrecoleccion valor : listaDias) {
      dias = new HrrHorrecoleccion()
              .setHrrIderegistro(valor.getHrrIderegistro())
              .setHrrDia(valor.getHrrDia())
              .setRureIderegistro(rutrecoleccion)
              .setHrrHorinicio(valor.getHrrHorinicio())
              .setHrrHorfin(valor.getHrrHorfin())
              .setHrrSwtact(valor.getHrrSwtact());
      dias.validar();
      diaRecoleccionDAO.insertar(dias);

    }

  }

  /**
   * Método que controla la consulta de ruta de recolección según parametro
   *
   * @param area Área de prestación
   * @param macro Macro de ruta
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<RureRutrecoleccion> consultarRecoleccion(Integer area, Integer macro)
          throws PersistenciaExcepcion, AplicacionExcepcion
  {

    ValidarDato.construir()
            .agregar(macro, "requerido", "El identificador la macro ruta es obligatorio")
            .agregar(area, "requerido", "El identificador del área de prestación es obligatorio")
            .validar();
    return new RureRutrecoleccionDAO(dataSource, auditoria())
            .consultarRecoleccion(area, macro);
  }

}
