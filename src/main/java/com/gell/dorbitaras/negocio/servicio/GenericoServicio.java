/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.estandar.util.FuncionesDatoUtil;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.dorbitaras.negocio.util.AuditoriaUtil;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 *
 * @author god
 */
public class GenericoServicio extends FuncionesDatoUtil {

  protected static final int NO_REGISTROS = 0;

  @Autowired
  protected DataSource dataSource;
  
  @Autowired
  @Qualifier("talend")
  protected DataSource dataSourceTalend;

  public AuditoriaDTO auditoria()
  {
    return AuditoriaUtil.auditoria();
  }

}
