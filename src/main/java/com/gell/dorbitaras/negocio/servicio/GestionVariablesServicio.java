/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ArprAreaprestacionDAO;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.CvlCtrverliquidacionDAO;
import com.gell.dorbitaras.persistencia.dao.LiqLiquidacionDAO;
import com.gell.dorbitaras.persistencia.dao.PerPeriodoDAO;
import com.gell.dorbitaras.persistencia.dao.RacoRanconceptDAO;
import com.gell.dorbitaras.persistencia.dao.RutRutaDAO;
import com.gell.dorbitaras.persistencia.dao.SmperSemperiodoDAO;
import com.gell.dorbitaras.persistencia.dao.VarcVarcalculoDAO;
import com.gell.dorbitaras.persistencia.dao.VarprVarperregDAO;
import com.gell.dorbitaras.persistencia.dao.VrmrVarmicrorutaDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaPromFinalesDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprPromDAO;
import com.gell.dorbitaras.persistencia.dto.ListadoTotalPorcentajesRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeFinalRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.VarcBaseRepuestaDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoBaseDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoDTO;
import com.gell.dorbitaras.persistencia.dto.VrtaVarteraprDTO;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.CvlCtrverliquidacion;
import com.gell.dorbitaras.persistencia.entidades.LiqLiquidacion;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.VarcVarcalculo;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.dorbitaras.persistencia.entidades.VrmrVarmicroruta;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarterapr;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.ValidarDato;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase encargada de la gestión de las variables por area de prestación,
 * periodo y concepto.
 *
 * @author Desarrollador
 */
@Service
public class GestionVariablesServicio extends GenericoServicio
{

  /**
   * Servicio encargado de consultar los conceptos.
   *
   * @param uniLiquidacion Identificador de la liquidación.
   * @return Lista de conceptos.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<ConConcepto> consultarConcepto(
          Integer uniLiquidacion
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(uniLiquidacion, "requerido", "El identificador de la liquidación es obligatorio")
            .validar();
    return new ConConceptoDAO(dataSource, auditoria())
            .consultarConcepto(uniLiquidacion);
  }

  /**
   * Servicio encargado de consultar los conceptos.
   *
   * @param uniLiquidacion Identificador de la liquidación.
   * @return Lista de conceptos.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<ConConcepto> consultarConceptoIndicadores()
          throws AplicacionExcepcion
  {
    return new ConConceptoDAO(dataSource, auditoria())
            .consultarConceptoIndicadores();
  }

  /**
   * Servicio encargado de consultar las variables según periodo, concepto,
   * tercero y área de prestación.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @param concepto Identificador de la variable.
   * @return Lista de variables.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<VrtaVarterapr> consultarVariablesAprovechamiento(
          Integer periodo,
          Integer areaPrestacion,
          Integer tercero,
          Integer concepto
  )
          throws AplicacionExcepcion
  {
    return new VrtaVarteraprDAO(dataSource, auditoria())
            .consultarVariablesAprovechamiento(periodo, areaPrestacion, tercero, concepto);
  }

  /**
   * Servicio encargado de consultar las variables segun periodo, concepto y
   * area de prestación
   *
   * @param periodo Identificador del periodo.
   * @param areaPrestacion Identificador del área de prestación
   * @param concepto Identificador del concepto.
   * @return
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<VarcVarcalculo> consultarVariablesPeriodos(
          Integer periodo,
          Integer areaPrestacion,
          Integer concepto
  )
          throws AplicacionExcepcion
  {
    return new VarcVarcalculoDAO(dataSource, auditoria())
            .consultarDatosVariable(periodo, concepto, areaPrestacion);
  }

  /**
   * Servicio encargado de consultar las variables según periodo, tercero y área
   * de prestación en estado pendiente.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Lista de variables.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<VrtaVarteraprDTO> consultarVariablesAprovechamientoPendientes(
          Integer periodo,
          Integer areaPrestacion,
          Integer tercero
  )
          throws AplicacionExcepcion
  {
    return new VrtaVarteraprDAO(dataSource, auditoria())
            .consultarVariablesAprovechamientoPendientes(periodo, areaPrestacion, tercero);
  }

  /**
   * Servicio encargado de consultar las variables en estado pendiente según el
   * área de prestación y periodo.
   *
   * @param periodo Identificador del periodo.
   * @param areaPrestacion Identificador de área de prestación.
   * @return Lista de variables en estado pendientes.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<VarcVarcalculoDTO> consultarVariablesPendientes(
          Integer periodo,
          Integer areaPrestacion
  )
          throws AplicacionExcepcion
  {
    return new VarcVarcalculoDAO(dataSource, auditoria())
            .consultarVariablesPendientes(periodo, areaPrestacion);
  }

  /**
   * Método encargado de guardar las variables en la varc_varcalculo.
   *
   * @param listaVariables Lista de datos de las variables.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarVariablesPeriodo(List<VarcVarcalculo> listaVariables)
          throws AplicacionExcepcion
  {
    VarcVarcalculoDAO varCalculoDao = new VarcVarcalculoDAO(dataSource, auditoria());
    RacoRanconceptDAO racoDao = new RacoRanconceptDAO(dataSource, auditoria());
    ConConceptoDAO conceptoDao = new ConConceptoDAO(dataSource, auditoria());
    Date fechaActual = new Date();
    for (VarcVarcalculo varcVarcalculo : listaVariables) {
        varcVarcalculo.validar();
        varcVarcalculo.setEmpIderegistro(auditoria().getIdEmpresa());
        if (varcVarcalculo.getVarcIderegistro() == null) 
        {
            varcVarcalculo.setVarcFecgrabacion(fechaActual);
            varcVarcalculo.setUsuIderegistroGb(auditoria().getIdUsuario());
        }    
        varcVarcalculo.setVarcEstadoregistro(EEstadoGenerico.ACTIVO);
        VarcVarcalculo registroPendiente = varCalculoDao.consultarVerificarRegistroPendiente(varcVarcalculo);
        VarcVarcalculo registroCertificado = varCalculoDao.consultarVerificarRegistro(varcVarcalculo);
        VarcVarcalculo registroCertificadoRaco = varCalculoDao.consultarVerificarRegistroRaco(varcVarcalculo);        
        if (registroPendiente == null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)
                && registroCertificado != null && varcVarcalculo.getVarcIderegistro() == null
                && registroCertificado.getVarcValor().doubleValue() == varcVarcalculo.getVarcValor().doubleValue()
                && (registroCertificado.getVarcDescripcion().equals(varcVarcalculo.getVarcDescripcion()) 
                || varcVarcalculo.getVarcDescripcion().length() == 0 )) {
            continue;            
        }
        if (registroPendiente == null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)
                && registroCertificadoRaco != null && varcVarcalculo.getVarcIderegistro() == null
                && registroCertificadoRaco.getVarcValor().doubleValue() == varcVarcalculo.getVarcValor().doubleValue() 
                && (registroCertificadoRaco.getVarcDescripcion().equals(varcVarcalculo.getVarcDescripcion())
                || varcVarcalculo.getVarcDescripcion().length() == 0 )) {
            continue;            
        }
        if (registroPendiente != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)) {
            registroPendiente
                .setVarcValor(varcVarcalculo.getVarcValor())
                .setVarcDescripcion(varcVarcalculo.getVarcDescripcion())
                .setVarcFecgrabacion(fechaActual)
                .setUsuIderegistroGb(auditoria().getIdUsuario());
            varCalculoDao.insertar(registroPendiente);
            continue;
        }
        if (registroCertificadoRaco != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) 
        {
            if (varcVarcalculo.getVarcIderegistro() != null                  
                    && varcVarcalculo.getVarcIderegistro().intValue() == registroCertificadoRaco.getVarcIderegistro().intValue()) {
                continue;
            }    
            varCalculoDao.editarVariable(registroCertificadoRaco.getVarcIderegistro());
        }
        if (registroCertificado != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) 
        {           
            if (varcVarcalculo.getVarcIderegistro() != null                    
                    && varcVarcalculo.getVarcIderegistro().intValue() == registroCertificado.getVarcIderegistro().intValue()) {
                continue;
            }
            varCalculoDao.editarVariable(registroCertificado.getVarcIderegistro());        
        }
        if (varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) {
            if(registroPendiente != null)
            {
                varcVarcalculo.setUsuIderegistroGb(registroPendiente.getUsuIderegistroGb());
                varcVarcalculo.setVarcFecgrabacion(registroPendiente.getVarcFecgrabacion());
            }
            varcVarcalculo.setUsuIderegistroCer(auditoria().getIdUsuario());
            varcVarcalculo.setVarcFeccerficicacion(new Date());
//            if (varcVarcalculo.getRacoIderegistro().getRacoIderegistr() != null) {
//                racoDao.cambiarValorConcepto(
//                    varcVarcalculo.getRacoIderegistro().getRacoIderegistr(),
//                    varcVarcalculo.getVarcValor().intValue()
//                );
//            } -- se comenta ya que este flujo es innecesario, ya que el valor que esta en el raco no se usa para nada.
//            if (varcVarcalculo.getRacoIderegistro().getRacoIderegistr() == null) {
//                conceptoDao.cambiarValorConcepto(
//                    varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro(),
//                    varcVarcalculo.getVarcValor().intValue()
//                );
//            }
            //controlHistorico(varcVarcalculo.getArprIderegistro().getArprIderegistro()); se remueve debido a que no esta funcionando
        }
        varCalculoDao.insertar(varcVarcalculo);
    }
  }
  
  
  
  /**
   * Método encargado de guardar las variables en la varc_varcalculo.
   *
   * @param listaVariables Lista de datos de las variables.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarVariablesPeriodoMicro(List<VrmrVarmicroruta> listaVariables)
          throws AplicacionExcepcion
  {
    VrmrVarmicrorutaDAO vrmrVarmicrorutaDAO = new VrmrVarmicrorutaDAO(dataSource, auditoria());
    RutRutaDAO rutRutaDAO = new RutRutaDAO(dataSource, auditoria());
    ConConceptoDAO conceptoDao = new ConConceptoDAO(dataSource, auditoria());
    Date fechaActual = new Date();
    for (VrmrVarmicroruta vrmrVarmicroruta : listaVariables) {
        //varcVarcalculo.validar();
        vrmrVarmicroruta.setEmpIderegistro(auditoria().getIdEmpresa());
        if (vrmrVarmicroruta.getVrmrIderegistro() == null) 
        {
            vrmrVarmicroruta.setVrmrFecgrabacion(fechaActual);
            vrmrVarmicroruta.setUsuIderegistrogb(auditoria().getIdUsuario());
        }    
        vrmrVarmicroruta.setVrmrEstado(EEstadoGenerico.CERTIFICADO);
        vrmrVarmicroruta.setVrmrFeccerficicacion(fechaActual);
        vrmrVarmicroruta.setUsuIderegistrocer(auditoria().getIdUsuario());
        vrmrVarmicroruta.setVrmrEstadoregistro(EEstadoGenerico.ACTIVO);
//        VarcVarcalculo registroPendiente = varCalculoDao.consultarVerificarRegistroPendiente(varcVarcalculo);
//        VarcVarcalculo registroCertificado = varCalculoDao.consultarVerificarRegistro(varcVarcalculo);
//        VarcVarcalculo registroCertificadoRaco = varCalculoDao.consultarVerificarRegistroRaco(varcVarcalculo);        
//        if (registroPendiente == null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)
//                && registroCertificado != null && varcVarcalculo.getVarcIderegistro() == null
//                && registroCertificado.getVarcValor().doubleValue() == varcVarcalculo.getVarcValor().doubleValue()
//                && (registroCertificado.getVarcDescripcion().equals(varcVarcalculo.getVarcDescripcion()) 
//                || varcVarcalculo.getVarcDescripcion().length() == 0 )) {
//            continue;            
//        }
//        if (registroPendiente == null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)
//                && registroCertificadoRaco != null && varcVarcalculo.getVarcIderegistro() == null
//                && registroCertificadoRaco.getVarcValor().doubleValue() == varcVarcalculo.getVarcValor().doubleValue() 
//                && (registroCertificadoRaco.getVarcDescripcion().equals(varcVarcalculo.getVarcDescripcion())
//                || varcVarcalculo.getVarcDescripcion().length() == 0 )) {
//            continue;            
//        }
//        if (registroPendiente != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.PENDIENTE)) {
//            registroPendiente
//                .setVarcValor(varcVarcalculo.getVarcValor())
//                .setVarcDescripcion(varcVarcalculo.getVarcDescripcion())
//                .setVarcFecgrabacion(fechaActual)
//                .setUsuIderegistroGb(auditoria().getIdUsuario());
//            varCalculoDao.insertar(registroPendiente);
//            continue;
//        }
//        if (registroCertificadoRaco != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) 
//        {
//            if (varcVarcalculo.getVarcIderegistro() != null                  
//                    && varcVarcalculo.getVarcIderegistro().intValue() == registroCertificadoRaco.getVarcIderegistro().intValue()) {
//                continue;
//            }    
//            varCalculoDao.editarVariable(registroCertificadoRaco.getVarcIderegistro());
//        }
//        if (registroCertificado != null && varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) 
//        {           
//            if (varcVarcalculo.getVarcIderegistro() != null                    
//                    && varcVarcalculo.getVarcIderegistro().intValue() == registroCertificado.getVarcIderegistro().intValue()) {
//                continue;
//            }
//            varCalculoDao.editarVariable(registroCertificado.getVarcIderegistro());        
//        }
//        if (varcVarcalculo.getVarcEstado().equals(EEstadoGenerico.CERTIFICADO)) {
//            if(registroPendiente != null)
//            {
//                varcVarcalculo.setUsuIderegistroGb(registroPendiente.getUsuIderegistroGb());
//                varcVarcalculo.setVarcFecgrabacion(registroPendiente.getVarcFecgrabacion());
//            }
//            varcVarcalculo.setUsuIderegistroCer(auditoria().getIdUsuario());
//            varcVarcalculo.setVarcFeccerficicacion(new Date());
//            if (varcVarcalculo.getRacoIderegistro().getRacoIderegistr() != null) {
//                racoDao.cambiarValorConcepto(
//                    varcVarcalculo.getRacoIderegistro().getRacoIderegistr(),
//                    varcVarcalculo.getVarcValor().intValue()
//                );
//            }
//            if (varcVarcalculo.getRacoIderegistro().getRacoIderegistr() == null) {
//                conceptoDao.cambiarValorConcepto(
//                    varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro(),
//                    varcVarcalculo.getVarcValor().intValue()
//                );
//            }
//            controlHistorico(varcVarcalculo.getArprIderegistro().getArprIderegistro());
//        }
        vrmrVarmicrorutaDAO.insertar(vrmrVarmicroruta);
    }
  }

  /**
   * Servicio encargado de guardar variables en la tabla vrta_vartearpr.
   *
   * @param listaEntidad Datos de las variables a guardar.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void guardarVariablesPeriodoAprovechamiento(List<VrtaVarterapr> listaEntidad)
          throws AplicacionExcepcion
  {
    VrtaVarteraprDAO vrtaDao = new VrtaVarteraprDAO(dataSource, auditoria());
    RacoRanconceptDAO racoDao = new RacoRanconceptDAO(dataSource, auditoria());
    ConConceptoDAO conceptoDao = new ConConceptoDAO(dataSource, auditoria());
    Date fechaActual = new Date();
    for (VrtaVarterapr vrtaVarterapr : listaEntidad) 
    { 
        if (vrtaVarterapr.getPerIderegistro().getPerIderegistro() == null && vrtaVarterapr.getPerIderegistro().getSmperIderegistro() != null)
        {
            vrtaVarterapr.getPerIderegistro().setPerIderegistro(vrtaVarterapr.getPerIderegistro().getSmperIderegistro());
        }      
        vrtaVarterapr.validar();      
        if (vrtaVarterapr.getVrtaIderegistro() == null) {
            vrtaVarterapr.setVrtaFecgrabacion(fechaActual);
            vrtaVarterapr.setEmpIderegistro(auditoria().getIdEmpresa());
            vrtaVarterapr.setUsuIderegistroGb(auditoria().getIdUsuario());
        }
        vrtaVarterapr.setVrtaEstadoRegistro(EEstadoGenerico.ACTIVO);
        VrtaVarterapr registroCertificado = vrtaDao.consultarVerificarRegistro(vrtaVarterapr);
        VrtaVarterapr registroPendiente = vrtaDao.consultarVerificarRegistroPendiente(vrtaVarterapr);
        VrtaVarterapr registroCertificadoRaco = vrtaDao.consultarVerificarRegistroRaco(vrtaVarterapr);
        if (registroPendiente == null && registroCertificado != null && vrtaVarterapr.getVrtaEstado().equals(EEstadoGenerico.PENDIENTE)
                && vrtaVarterapr.getVrtaValor().doubleValue() == registroCertificado.getVrtaValor().doubleValue()
                && (vrtaVarterapr.getVrtaDescripcion().equals(registroCertificado.getVrtaDescripcion())
                || vrtaVarterapr.getVrtaDescripcion().length() == 0 ))
        {
            continue ; 
        }
        if (registroPendiente == null && registroCertificadoRaco != null
                && vrtaVarterapr.getVrtaValor().doubleValue() == registroCertificadoRaco.getVrtaValor().doubleValue()
                && (vrtaVarterapr.getVrtaDescripcion().equals(registroCertificadoRaco.getVrtaDescripcion())
                || vrtaVarterapr.getVrtaDescripcion().length() == 0 ))
        {
            continue ; 
        }
        if (registroPendiente != null && vrtaVarterapr.getVrtaEstado().equals(EEstadoGenerico.PENDIENTE)) {
            registroPendiente.setVrtaValor(vrtaVarterapr.getVrtaValor())
                .setVrtaDescripcion(vrtaVarterapr.getVrtaDescripcion())
                .setUsuIderegistroGb(auditoria().getIdUsuario()) 
                .setVrtaFecgrabacion(fechaActual)  ;
            vrtaDao.insertar(registroPendiente);
            continue ;
        }
        if (registroCertificadoRaco != null && vrtaVarterapr.getVrtaEstado().equals(EEstadoGenerico.CERTIFICADO)) 
        {            
            if (vrtaVarterapr.getVrtaIderegistro() != null                
                && vrtaVarterapr.getVrtaIderegistro().intValue() == registroCertificadoRaco.getVrtaIderegistro().intValue()) 
            {
                continue ;
            }
            vrtaDao.editarVariable(registroCertificadoRaco.getVrtaIderegistro());
        }
        if (registroCertificado != null && vrtaVarterapr.getVrtaEstado().equals(EEstadoGenerico.CERTIFICADO)) 
        {
            if (vrtaVarterapr.getVrtaIderegistro() != null               
                && vrtaVarterapr.getVrtaIderegistro().intValue() == registroCertificado.getVrtaIderegistro().intValue()) 
            {
                continue ; 
            }
            vrtaDao.editarVariable(registroCertificado.getVrtaIderegistro());
        }
        if (vrtaVarterapr.getVrtaEstado().equals(EEstadoGenerico.CERTIFICADO)) 
        {
            if (registroPendiente != null )
            {
                vrtaVarterapr.setVrtaFecgrabacion(registroPendiente.getVrtaFecgrabacion());
                vrtaVarterapr.setEmpIderegistro(registroPendiente.getEmpIderegistro());
                vrtaVarterapr.setUsuIderegistroGb(registroPendiente.getUsuIderegistroGb());
            }
            vrtaVarterapr.setUsuIderegistroCer(auditoria().getIdUsuario());
            vrtaVarterapr.setVrtaFeccertificacion(new Date());
//            if (vrtaVarterapr.getRacoIderegistro().getRacoIderegistr() != null) {
//                racoDao.cambiarValorConcepto(
//                    vrtaVarterapr.getRacoIderegistro().getRacoIderegistr(),
//                    vrtaVarterapr.getVrtaValor().intValue()
//                );
//            }
//            if (vrtaVarterapr.getRacoIderegistro().getRacoIderegistr() == null) {
//                conceptoDao.cambiarValorConcepto(
//                    vrtaVarterapr.getConIderegistro().getUniConcepto().getUniIderegistro(),
//                    vrtaVarterapr.getVrtaValor().intValue()
//                );
//            }
            //controlHistorico(vrtaVarterapr.getArprIderegistro().getArprIderegistro());se remueve no esta funcionando
        }
        vrtaDao.insertar(vrtaVarterapr);
    }
  }

  /**
   * Método encargado de consultar las variables calculadas.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista de variables calculadas.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<VarprVarperreg> consultarVariablesCalculadas(
          Integer idArea,
          Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
//    SmperSemperiodoDAO per = new SmperSemperiodoDAO(dataSource, auditoria());
//    SmperSemperiodo periodo = per.consultar(idPeriodo);
//    periodo.get
    return new VarprVarperregDAO(dataSource, auditoria())
            .consultarVariablesCalculadas(idArea, idPeriodo);

  }

  /**
   * Método encargado de consultar las conceptos base calculo.
   *
   * @param idLiquidacion Identificador de la liquidación.
   * @param idPeriodo Identificador del periodo semestral que se esta consultando.
   * @param areaPrestacion Identificador del Area de Prestacion .
   * @return Lista de conceptos.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public VarcBaseRepuestaDTO consultarVariablesBaseCalculo(
          Integer idLiquidacion,
          Integer idPeriodo , 
          Integer areaPrestacion
  )
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idLiquidacion, "requerido", "El identificador de la liquidación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .agregar(areaPrestacion, "requerido", "El area de prestacion es obligatorio ")
            .validar();
//    List<PerPeriodo> periodosInconsistentens = new PerPeriodoDAO(dataSource, auditoria()).consultarPeriodosInconsistentes(idPeriodo);
//    if (!periodosInconsistentens.isEmpty()) {
//      throw new NegocioExcepcion(EMensajeNegocio.ERROR_PERIODOS_INCONSISTENTES, periodosInconsistentens);
//      
//    }
    VarcBaseRepuestaDTO respuestvarcBase = new VarcBaseRepuestaDTO() ;
    respuestvarcBase.setListaVariables(
            new VarcVarcalculoDAO(dataSource, auditoria())
            .consultarVariablesBaseCalcuclo(idLiquidacion , idPeriodo, areaPrestacion));
    respuestvarcBase.setPeriodos(
            new SmperSemperiodoDAO(dataSource, auditoria())
            .consultarPeriodosSemestre(idPeriodo, areaPrestacion));    
    respuestvarcBase.setListaVarApr(
            new VrtaVarteraprDAO(dataSource, auditoria())
            .consultarVariablesBaseAprCalcuclo(idLiquidacion , idPeriodo, areaPrestacion));    
    return respuestvarcBase ;
  }

  /**
   * Método encargado de certificar las variables calculadas por área de
   * prestación y periodo.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public void certificarVariablesCalculadas(Integer idArea, Integer idPeriodo)
          throws AplicacionExcepcion
  {
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
    VarprVarperregDAO varprDao = new VarprVarperregDAO(dataSource, auditoria());
    List<VarprVarperreg> listaVariables = varprDao.consultarVariablesCalculadas(idArea, idPeriodo);
    if (listaVariables.isEmpty()) {
      throw new PersistenciaExcepcion(EMensajeNegocio.ERROR_VARIABLES_CALCULADAS);
    }
    for (VarprVarperreg variable : listaVariables) {
      if (!variable.getListaRangos().isEmpty()) {
        procesarVariables(variable.getListaRangos());
        continue;
      }
      procesarVariables(variable.getListaValores());
    }
  }

  /**
   * Método encargado de procesar la lista de variables
   *
   * @param lista Lista de variables.
   * @throws AplicacionExcepcion
   */
  public void procesarVariables(List<VarprVarperreg> lista)
          throws AplicacionExcepcion
  {
    VarprVarperregDAO varprDao = new VarprVarperregDAO(dataSource, auditoria());
    for (VarprVarperreg rango : lista) {
      varprDao.certificarEstadoVariable(rango);
    }
  }

  /**
   * Método encargado de controlar el historico.
   *
   * @param idArea Identificador del área de prestación.
   * @throws PersistenciaExcepcion
   */
    public void controlHistorico(Integer idArea)
          throws PersistenciaExcepcion
    {
        ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea.longValue());
        Integer idLiquidacion = area.getLiqIderegistro().getUniLiquidacion();
        LiqLiquidacionDAO liquidacionDao = new LiqLiquidacionDAO(dataSource, auditoria());
        LiqLiquidacion liquidacion = liquidacionDao.consultar(idLiquidacion.longValue());
        CvlCtrverliquidacionDAO controlDao = new CvlCtrverliquidacionDAO(dataSource, auditoria());
        Integer numero = liquidacionDao.validarHistoricoLiquidacion(idLiquidacion);
        liquidacionDao.modificarVersionHistorico(idLiquidacion);
        if (numero > 0) {
            return;
        }
        CvlCtrverliquidacion control = new CvlCtrverliquidacion()
            .setHliqIderegistr(liquidacion.getHliqIderegistr())
            .setUniLiquidacion(idLiquidacion)
            .setUsuIderegistro(auditoria().getIdUsuario())
            .setCvlFecha(new Date());
        controlDao.insertar(control);
        liquidacionDao.ejecutarHistorico(idLiquidacion);
    }
    
    @Transactional(readOnly = true)
    public List<ListadoTotalPorcentajesRecalculoDTO> obtenerPorcentajesprimerysegundo(Integer idPeriodo, Integer numeroActualizacion,String tipo)
            throws AplicacionExcepcion {
        VrtaPromFinalesDAO prfinalesDao = new VrtaPromFinalesDAO(dataSource, auditoria());
        List<PorcentajeFinalRecalculoDTO> listaPromediosFinales = new ArrayList<PorcentajeFinalRecalculoDTO>();
        List<ListadoTotalPorcentajesRecalculoDTO> listadoRespuesta = new ArrayList<>();
        
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        

        // Se verifica si hay promedios con diferencias en la tabla promedios finales
        listaPromediosFinales = prfinalesDao.obtenerprimerysegundoporcentaje(idPeriodo, numeroActualizacion, tipo);
        
        if (listaPromediosFinales.size() > 0) {
            listaPromediosFinales.stream().forEach(promedioFinal -> {
                try {                            
                    List<PorcentajeRecalculoDTO> promedioSinAjuste = promDao.obtenerPromedioxporcentaje(idPeriodo, promedioFinal.getTerIdeRegistro(), numeroActualizacion,tipo);

                    Double sinAjuste = promedioSinAjuste.isEmpty()? 0.0 : promedioSinAjuste.get(0).getVrtaPromValor();
                    String nombreAsociacion = promedioSinAjuste.get(0).getTerNomcompleto();
                    
                    listadoRespuesta.add(new ListadoTotalPorcentajesRecalculoDTO(sinAjuste,
                                        sinAjuste,  0.0, promedioFinal.getVrtaPromValor(), nombreAsociacion,promedioFinal.getConIderegistro()));
                } catch (PersistenciaExcepcion ex) {
                    System.out.print(ex.getMessage());
                } catch (AplicacionExcepcion ex) {
                    System.out.print(ex.getMessage());
                }
            });
        }

        return listadoRespuesta;
    }

}
