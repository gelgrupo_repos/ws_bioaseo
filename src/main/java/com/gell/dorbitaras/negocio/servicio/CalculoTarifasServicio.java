/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EGlobal;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.delegado.LiquidarConceptoDelegado;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.dao.ArprAreaprestacionDAO;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.PerPeriodoDAO;
import com.gell.dorbitaras.persistencia.dao.RacoRanconceptDAO;
import com.gell.dorbitaras.persistencia.dao.VarprVarperregDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprPromDAO;
import com.gell.dorbitaras.persistencia.dto.facturar.ArgumentoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.RacoRanconcept;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.FuncionesDatoUtil;
import com.gell.estandar.util.ValidarDato;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import jline.internal.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Desarrollador
 */
@Service
public class CalculoTarifasServicio extends GenericoServicio
{
    @Autowired
    private CalculoTarifasServicio calculoServicio;
    @Autowired
    private RecalculoAprovechamientoServicio recalculoAprovechamientoServicio;

  /**
   * Método encargado de consultar las variables calculadas.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista de variables calculadas.
   * @throws AplicacionExcepcion
   */
  @Transactional(rollbackFor = Throwable.class)
  public List<ValorConceptoDTO> calcularBalanceMasas(
          Integer idArea,
          Integer idPeriodo
  )
          throws AplicacionExcepcion
  {
    VarprVarperregDAO varprDao = new VarprVarperregDAO(dataSource, auditoria());
    LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
    ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador del área de prestación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
    List<VarprVarperreg> listaVariables = varprDao.consultarVariablesCalculadas(idArea, idPeriodo);
    if (listaVariables.isEmpty()) {
      throw new PersistenciaExcepcion(EMensajeNegocio.ERROR_VARIABLES_CALCULADAS);
    }
    List<ConConcepto> listaConceptos = obtenerConceptos();
    ArgumentoDTO argumentos = new ArgumentoDTO()
            .agregar("idEmpresa", auditoria().getIdEmpresa())
            .agregar("idUsuario", auditoria().getIdUsuario());
    for (ConConcepto concepto : listaConceptos) {
//      delegado.liquidarConcepto(concepto, area.getLiqIderegistro().toString(), argumentos);
    }
    return delegado.getListaConceptosLiquidados();
  }

  /**
   * Método encargado de devolver la lista de variables para el calculo de masas
   *
   * @return @throws PersistenciaExcepcion
   */
  public List<ConConcepto> obtenerConceptos()
          throws PersistenciaExcepcion
  {
    ConConceptoDAO conceptoDao = new ConConceptoDAO(dataSource, auditoria());
    List<ConConcepto> listaConceptos = new ArrayList<>();
    ConConcepto qz = conceptoDao.consultarPorAbreviatura(EGlobal.Concepto.BalanceMasas.QZ);
    ConConcepto qbl = conceptoDao.consultarPorAbreviatura(EGlobal.Concepto.BalanceMasas.QBL);
    ConConcepto qlu = conceptoDao.consultarPorAbreviatura(EGlobal.Concepto.BalanceMasas.QLUz);
    ConConcepto qnaz = conceptoDao.consultarPorAbreviatura(EGlobal.Concepto.BalanceMasas.QNAz);
    ConConcepto qaz = conceptoDao.consultarPorAbreviatura(EGlobal.Concepto.BalanceMasas.QAz);
    listaConceptos.add(qz);
    listaConceptos.add(qbl);
    listaConceptos.add(qlu);
    listaConceptos.add(qnaz);
    listaConceptos.add(qaz);
    return listaConceptos;
  }
  
    private void calcularPromediosQA(Integer idPeriodo, Integer idArea, Integer idPeriodoPadre)
            throws AplicacionExcepcion {

        VrtaVarteraprDAO dao = new VrtaVarteraprDAO(dataSource, auditoria());
        //consulta para traer el concepto
        ConConcepto toneladasTercero = new ConConceptoDAO(dataSource, auditoria()).consultarPorAbreviatura("vb-TAXTM");
        
        // Cargo el numero de actualizacion que se procesara en todas las tablas
        VrtaVarteraprPromDAO promDao = new VrtaVarteraprPromDAO(dataSource, auditoria());
        Integer numeroActualizacion = promDao.obtenerUltimoNumeroActualizacion(idPeriodoPadre, idPeriodo, "C");
        numeroActualizacion = numeroActualizacion == null ? 0 : numeroActualizacion + 1;
        
        recalculoAprovechamientoServicio.calcularPromedios(dao, numeroActualizacion, toneladasTercero.getUniConcepto().getUniIderegistro(), idPeriodo, idArea, idPeriodoPadre, "C");
        recalculoAprovechamientoServicio.calculoQA(numeroActualizacion, idPeriodoPadre, idPeriodo,idArea, "C");
    }

  /**
   * Método encargado de realizar el calculo de las variables semestrales.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @param idPeriodoPadre Identificador del periodo padre.
   * @return Lista de conceptos liquidados.
   * @throws AplicacionExcepcion
   */
  //@Transactional(rollbackFor = Throwable.class)
  @Transactional(timeout = 600)
  public List<ValorConceptoDTO> calcularVariables(
          Integer idArea,
          Integer idPeriodo,
          Integer idPeriodoPadre
  )
          throws AplicacionExcepcion
  {
      
    List<String> idsLiquidacion = new ArrayList<String>();
    List<VarprVarperreg> listaCertificadasPeriodoPadre = null;
    List<ValorConceptoDTO> listaConceptosLiquidados = null;
//    Integer idLiquidacion= -1;
    ValidarDato.construir()
            .agregar(idArea, "requerido", "El identificador de la liquidación es obligatorio")
            .agregar(idPeriodo, "requerido", "El identificador del periodo es obligatorio")
            .validar();
    VarprVarperregDAO variableDAO = new VarprVarperregDAO(dataSource, auditoria());
    LiquidarConceptoDelegado delegado = new LiquidarConceptoDelegado(auditoria(), dataSource);
    ArprAreaprestacion area = new ArprAreaprestacionDAO(dataSource, auditoria()).consultar(idArea);
    PerPeriodo periodo = new PerPeriodoDAO(dataSource, auditoria()).consultar(idPeriodo.longValue());
    Integer idLiquidacion = area.getLiqIderegistro().getUniLiquidacion();
    if (null != idPeriodoPadre) {
      
      listaCertificadasPeriodoPadre = variableDAO.consultarVariablesCertificadas(idArea, idPeriodoPadre);
      if (listaCertificadasPeriodoPadre.isEmpty()) {
        throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO_PADRE);
      }
      // Calculo se mueve solo cuando se realiza el proceso mensual.
      calcularPromediosQA(idPeriodo,idArea,idPeriodoPadre);
      delegado.ConceptosSemestralesLiquidados(listaCertificadasPeriodoPadre);
      
//      try {
      String ids = area.getArprLiquidaciones();
//      System.out.print("ids->"+getValuesForGivenKey(ids,"liquidacion"));
      idsLiquidacion = getValuesForGivenKey(ids,"liquidacion");
      idLiquidacion = Integer.parseInt(idsLiquidacion.get(0));
    }else{
//        System.out.print("idLiquidation->"+idLiquidacion.toString());
      idsLiquidacion.add(idLiquidacion.toString());
    }
    List<PerPeriodo> periodosInconsistentens = new PerPeriodoDAO(dataSource, auditoria()).consultarPeriodosInconsistentes(idPeriodo);
    if (!periodosInconsistentens.isEmpty()) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_PERIODOS_INCONSISTENTES, periodosInconsistentens);
    }
    List<VarprVarperreg> variablesCertificadas = variableDAO.consultarVariablesCertificadas(idArea, idPeriodo);
    if (!variablesCertificadas.isEmpty()) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_CERTIFICADAS);
    }
    List<ConConcepto> listaConcSinCert
            = new ConConceptoDAO(dataSource, auditoria()).consultarVarBaseSinCert(idLiquidacion , idPeriodo, idArea); 
    if (!listaConcSinCert.isEmpty()) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_VARIABLES_SIN_CERTIFICAR);
    }
    List<ConConcepto> listaConceptos
            = new ConConceptoDAO(dataSource, auditoria()).consultarConceptosColi(idLiquidacion);
    ArgumentoDTO argumentos = new ArgumentoDTO()
            .agregar("idEmpresa", auditoria().getIdEmpresa())
            .agregar("idArea", idArea)
            .agregar("idPeriodo", idPeriodo)
            .agregar("idUsuario", auditoria().getIdUsuario());
    int i=0;
    for (ConConcepto concepto : listaConceptos) {
      Log.info("concepto->"+i+" :"+concepto.getConAbreviatura());
      delegado.liquidarConcepto(concepto, idsLiquidacion, argumentos);
      i++;
    }
        listaConceptosLiquidados = delegado.getListaConceptosLiquidados();
    for (ValorConceptoDTO conceptoLiquidado : listaConceptosLiquidados) {
        if(conceptoLiquidado.getFlagSemestrales()==0){
            guardarVariables(conceptoLiquidado, area, periodo);
        }
    }
    return listaConceptosLiquidados;
  }

  /**
   * Método encargado de guardar las variables calculadas.
   *
   * @param conceptoLiq Datos de la variable a guardar.
   * @param area Datos del área de prestación.
   * @param periodo Datos del periodo.
   * @throws PersistenciaExcepcion
   */
  @Transactional
  public void guardarVariables(
          ValorConceptoDTO conceptoLiq,
          ArprAreaprestacion area,
          PerPeriodo periodo
  )
          throws PersistenciaExcepcion
  {
    ConConcepto conConcepto = conceptoLiq.getConcepto();
    VarprVarperregDAO variableDao = new VarprVarperregDAO(dataSource, auditoria());
    VarprVarperreg variableExistente = variableDao.consultarExistente(conConcepto, area, periodo);
    if (null != variableExistente) {
      variableDao.inactivarVariable(variableExistente.getVarprIderegistro());
    }
    RacoRanconcept rango = new RacoRanconceptDAO(dataSource, auditoria()).consultarIdRango(conceptoLiq);
    System.out.print("......................Guardar Variables........................\n");
      System.out.println("Concepto->"+conConcepto.getUniConcepto().getUniIderegistro()+" - valor->"+conceptoLiq.getValorTotal());
    VarprVarperreg variable = new VarprVarperreg()
            .setArprIderegistro(area)
            .setConIderegistro(conConcepto)
            .setRacoIderegistro(rango)
            .setPerIderegistro(periodo)
            .setUsuIderegistroGb(auditoria().getIdUsuario())
            .setEmpIderegistro(auditoria().getIdEmpresa())
            .setVarprFecgrabacion(new Date())
            .setVarprEstadoRegistro(EEstadoGenerico.ACTIVO)
            .setVarprEstado(EEstadoGenerico.CALCULADO)
            .setVarprValor(conceptoLiq.getValorTotal());
    variableDao.insertar(variable);
  }
  
 /**
   * Método encargado de guardar las variables calculadas para recalculo de
   * aprovechamiento.
   *
   * @param conceptoLiq Datos de la variable a guardar.
   * @param area Datos del área de prestación.
   * @param periodo Datos del periodo.
   * @throws PersistenciaExcepcion
   */
  @Transactional
  public Integer guardarVariablesRecalculoAprovechamiento(
          ValorConceptoDTO conceptoLiq,
          ArprAreaprestacion area,
          PerPeriodo periodo
  )
          throws PersistenciaExcepcion
  {
    ConConcepto conConcepto = conceptoLiq.getConcepto();
    VarprVarperregDAO variableDao = new VarprVarperregDAO(dataSource, auditoria());
    VarprVarperreg variableExistente = variableDao.consultarExistente(conConcepto, area, periodo);
    if (null != variableExistente) {
      //variableDao.inactivarVariable(variableExistente.getVarprIderegistro());
    }
    RacoRanconcept rango = new RacoRanconceptDAO(dataSource, auditoria()).consultarIdRango(conceptoLiq);
    System.out.print("......................Guardar Variables aprovechamiento........................\n");
      System.out.println("Concepto->"+conConcepto.getUniConcepto().getUniIderegistro()+" - valor->"+conceptoLiq.getValorTotal());
    VarprVarperreg variable = new VarprVarperreg()
            .setArprIderegistro(area)
            .setConIderegistro(conConcepto)
            .setRacoIderegistro(rango)
            .setPerIderegistro(periodo)
            .setUsuIderegistroGb(auditoria().getIdUsuario())
            .setEmpIderegistro(auditoria().getIdEmpresa())
            .setVarprFecgrabacion(new Date())
            .setVarprEstadoRegistro(EEstadoGenerico.CALCULADO_TA)
            .setVarprEstado(EEstadoGenerico.CALCULADO_TA)
            .setVarprValor(conceptoLiq.getValorTotal());
    return variableDao.insertarAprovechamiento(variable);
  }
  
  public List<String> getValuesForGivenKey(String jsonArrayStr, String key) {
    JSONArray jsonArray = new JSONArray(jsonArrayStr);
    return IntStream.range(0, jsonArray.length())
      .mapToObj(index -> ((JSONObject)jsonArray.get(index)).optString(key))
      .collect(Collectors.toList());
}

}
