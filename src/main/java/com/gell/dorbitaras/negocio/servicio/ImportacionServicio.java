/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.Date;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Cristtian
 */
@Service
public class ImportacionServicio extends GenericoServicio {

  /**
   * Método que controla los datos al guardar
   *
   * @param importar Información del registro
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  @Transactional(rollbackFor = Throwable.class)
  public void insertar(ImpImportar importar)
          throws AplicacionExcepcion
  {
    importar.validar(); 
    ImpImportarDAO importarDAO = new ImpImportarDAO(dataSource, auditoria());
    importar.setUsuIderegistro(new Long(auditoria().getIdUsuario()))
            .setEmpIderegistsro(auditoria().getIdEmpresa())
            .setFechaRegistro(new Date());
    importarDAO.guardarImp(importar);
    List<DimpDetalleimportar> infoDestino = importar.getInfoDestino();
    DimpDetalleimportarDAO detalleDAO = new DimpDetalleimportarDAO(dataSource, auditoria());
    DimpDetalleimportar detalleImportar;
    for (DimpDetalleimportar valor : infoDestino) {
      detalleImportar = new DimpDetalleimportar()
              .setDimpIderegistro(valor.getDimpIderegistro())
              .setImpIderegistro(importar.getImpIderegistro())
              .setDimpTabladestino(valor.getDimpTabladestino())
              .setDimpDescripcion(valor.getDimpDescripcion())
              .setDimpOrdentransaccional(valor.getDimpOrdentransaccional())
              .setDimpDepedenciafuncional(valor.getDimpDepedenciafuncional())
              .setDimpCampodependenciafuncional(valor.getDimpCampodependenciafuncional())
              .setDimpDescripcioncampo(valor.getDimpDescripcioncampo())
              .setDimpOrden(valor.getDimpOrden())
              .setDimpInicio(valor.getDimpInicio())
              .setDimpFin(valor.getDimpFin())
              .setDimpValordefecto(valor.getDimpValordefecto())
              .setDimpCampodestino(valor.getDimpCampodestino())
              .setDimpExpresionregular(valor.getDimpExpresionregular())
              .setDimpLongitudmaxima(valor.getDimpLongitudmaxima())
              .setDimpRequerido(valor.getDimpRequerido());

      detalleImportar.validar();
      detalleDAO.guardarDetalle(detalleImportar);
    }
  }

  /**
   * Método que controla la consulta de InpImportar
   *
   * @return Lista de las rutas dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultar()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }

  /**
   * Método que controla la consulta de la tabla destino
   *
   * @return Lista de la tabla destino
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultarTablaDestino()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }
  
    /**
   * Método que controla la consulta de dependencia
   *
   * @return Lista de la tabla dependencia
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultarDependencia()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }

    /**
   * Método que controla la consulta de los campos
   *
   * @return Lista de la tabla destino
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultarCampos()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }

    /**
   * Método que controla la consulta de los atributos
   *
   * @return Lista de losn atributos
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultarAtributos()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }

    /**
   * Método que controla la consulta de la expresión
   *
   * @return Lista de las expresiónes
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Transactional(readOnly = true)
  public List<ImpImportar> consultarExpresion()
          throws PersistenciaExcepcion
  {
    return new ImpImportarDAO(dataSource, auditoria())
            .consultar();
  }


}
