/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.servicio;

import com.gell.dorbitaras.persistencia.dao.LiqLiquidacionDAO;
import com.gell.dorbitaras.persistencia.entidades.LiqLiquidacion;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Desarrollador
 */
@Service
public class LiquidacionServicio extends GenericoServicio
{

  /**
   * Servicio encargado de consultar las liquidaciones de taras.
   *
   * @return Lista de liquidaciones.
   * @throws AplicacionExcepcion
   */
  @Transactional(readOnly = true)
  public List<LiqLiquidacion> consultarLiquidacion()
          throws AplicacionExcepcion
  {
    return new LiqLiquidacionDAO(dataSource, auditoria()).consultarLiquidacion();
  }
}
