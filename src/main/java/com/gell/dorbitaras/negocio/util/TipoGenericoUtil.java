/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gell.dorbitaras.persistencia.dto.facturar.FragmentoDTO;
import java.util.List;
import java.util.Map;

/**
 *
 * @author billionaire
 */
public class TipoGenericoUtil
{

  public static TypeReference<List<Integer>> listaEnteros()
  {
    return new TypeReference<List<Integer>>()
    {
    };
  }

  public static TypeReference<List<FragmentoDTO>> listaFragmento()
  {
    return new TypeReference<List<FragmentoDTO>>()
    {
    };
  }

  public static TypeReference<Map<String, String>> mapa()
  {
    return new TypeReference<Map<String, String>>()
    {
    };
  }

}
