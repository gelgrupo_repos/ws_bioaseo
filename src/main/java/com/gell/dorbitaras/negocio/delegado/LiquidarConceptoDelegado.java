/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.delegado;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EGlobal;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.negocio.util.TipoGenericoUtil;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.dao.ConConceptoDAO;
import com.gell.dorbitaras.persistencia.dao.FunFuncionDAO;
import com.gell.dorbitaras.persistencia.dao.RacoRanconceptDAO;
import com.gell.dorbitaras.persistencia.dao.VrtaVarteraprPromDAO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeFinalRecalculoDTO;

import com.gell.dorbitaras.persistencia.dto.facturar.ArgumentoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.FragmentoDTO;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.CoreConrelacio;
import com.gell.dorbitaras.persistencia.entidades.FunFuncion;
import com.gell.dorbitaras.persistencia.entidades.RacoRanconcept;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import static com.gell.estandar.util.FuncionesDatoUtil.jsonObjeto;
import com.gell.estandar.util.LogUtil;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.sql.DataSource;
import org.jfree.util.Log;

/**
 * Clase encargada de ejecutar y calcular funciones, variables y fórmulas del
 * módulo de nominaciones
 *
 * @author billionaire
 */
public class LiquidarConceptoDelegado extends GenericoDelegado
{

  private final FunFuncionDAO funcionDAO;
  private final RacoRanconceptDAO rangoConceptoDAO;
  private final ConConceptoDAO conceptoDAO;
  private final List<ValorConceptoDTO> listaConceptosLiquidados;
  private final VrtaVarteraprPromDAO vrtaVarteraprPromDAO;
  private ArgumentoDTO argumentos;
  private List<ConConcepto> listaConceptoLiquidar;
  private Double valor=0.0;

  public LiquidarConceptoDelegado(AuditoriaDTO auditoria, DataSource datasource)
          throws AplicacionExcepcion
  {
    super(PostgresBD.getConexion(datasource), auditoria);
    this.funcionDAO = new FunFuncionDAO(cnn, auditoria);
    this.conceptoDAO = new ConConceptoDAO(cnn, auditoria);
    this.listaConceptosLiquidados = new ArrayList<>();
    this.rangoConceptoDAO = new RacoRanconceptDAO(cnn, auditoria);
    this.vrtaVarteraprPromDAO = new VrtaVarteraprPromDAO(cnn, auditoria);

  }

  public Double ejecutarFuncion(Integer idFuncion)
          throws AplicacionExcepcion
  {
    FunFuncion funcion = funcionDAO.consultar(idFuncion.longValue());
    return funcionDAO.ejecutarFuncion(funcion, argumentos);
  }

  /**
   * Ejecuta la información de una variable y devuelve el valor final y sus
   * variables relacionadas
   *
   * @param concepto Concepto a calcular
   * @param idsLiquidacion Identificador de la liquidación
   * @param argumentos Parámetros adicionales para liquidar el concepto
   *
   * @throws AplicacionExcepcion
   */
  public void liquidarConcepto(ConConcepto concepto, List<String> idsLiquidacion, ArgumentoDTO argumentos)

          throws AplicacionExcepcion
  {
    String listadoCon="";
    this.argumentos = argumentos;
    Integer idConcepto = concepto.getUniConcepto().getUniIderegistro();
    ValorConceptoDTO conceptoLiquidado = buscarConceptoLiquidado(idConcepto);
    if (conceptoLiquidado != null) {
      return;
    }

    List<Integer> listaConLiq= new ArrayList<Integer>();
    for (ValorConceptoDTO listaConceptosLiquidado : listaConceptosLiquidados) {
        listaConLiq.add(listaConceptosLiquidado.getConcepto().getUniConcepto().getUniIderegistro());
    }
    if(!listaConLiq.isEmpty()){
        listadoCon = Arrays.toString(listaConLiq.toArray()).replace("[", "(").replace("]", ")");
    }
    listaConceptoLiquidar = conceptoDAO.consultarConceptosRelacionados(concepto, idsLiquidacion,listadoCon);
    for (ConConcepto infoConcepto : listaConceptoLiquidar) {
//      if(infoConcepto.getListaRelacionados()!=null){
        if (!Objects.equals(concepto.getUniConcepto().getUniIderegistro(), infoConcepto.getUniConcepto().getUniIderegistro())) {
          liquidarConcepto(infoConcepto, idsLiquidacion, argumentos);
        }
//      }
    }
    Double valor = calcularValorConcepto(concepto);
    valor = calcularRangos(valor, concepto);
    calcularValorRealConcepto(valor, concepto,0);
  }

  /**
   * Ejecuta la funcion de traer y almacenar los conceptos semestrales ya liquidados
   * al momento de realizar los calculos semestrales.
   *
   * @param concepto Concepto a calcular
   * @param idsLiquidacion Identificador de la liquidación
   * @param argumentos Parámetros adicionales para liquidar el concepto
   *
   * @throws AplicacionExcepcion
   */
  public void ConceptosSemestralesLiquidados(List<VarprVarperreg> listaCertificadasPeriodoPadre)

          throws AplicacionExcepcion
  {
      for (VarprVarperreg varprVarperreg : listaCertificadasPeriodoPadre) {
          calcularValorRealConcepto(varprVarperreg.getVarprValor(), varprVarperreg.getConIderegistro(),1);
      }
  }
  /**
   * Calcuar el rango de acuerdo al valor del concepto liquidado
   *
   * @param valor Valor del concepto liquidado
   * @param infoConcepto información del concepto
   * @return Valor del rango
   * @throws AplicacionExcepcion Error al calcular el rango
   */
  private Double calcularRangos(Double valor, ConConcepto infoConcepto)
          throws AplicacionExcepcion
  {
    Integer idConcepto = infoConcepto.getUniConcepto().getUniIderegistro();
    if (!rangoConceptoDAO.tieneRangos(idConcepto)) {
      return valor;
    }
    List<RacoRanconcept> listaRangos = rangoConceptoDAO.consultarRango(valor, idConcepto);
    if (listaRangos.isEmpty() || listaRangos.size() > 1) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_RANGOS, infoConcepto.getConNombre());
    }
    RacoRanconcept rango = listaRangos.get(0);
    Double valorRango = rango.getRacoValor();
    if (nulo(rango.getRacoFormula()) && nulo(valorRango)) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_RANGOS_NULO, infoConcepto.getConNombre());
    }else{
    }
    if ((tieneValor(valorRango)) ||(rango.getRacoFormula()==null)) {
      return valorRango;
    }else{
        String formula = rango.getRacoFormula();
        return calcularConceptoFormula(infoConcepto, formula);
    }
  }

  /**
   * Valida si el concepto debe tener valor real o es un concepto informativo
   *
   * @param valor Valor del concepto liquidado
   * @param infoConcepto Información del concepto que se está liquidando
   */
  private void calcularValorRealConcepto(Double valor, ConConcepto infoConcepto,Integer flagSemestrales)
          throws NegocioExcepcion
  {
    if (EEstadoGenerico.NO.equalsIgnoreCase(infoConcepto.getConValnulo()) && nulo(valor)) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_NULO, infoConcepto.getConNombre());
    }
    valor = nulo(valor) ? 0D : valor;
    ValorConceptoDTO valorConcepto = new ValorConceptoDTO(infoConcepto)
            .setCantidad(1D)
            .setValorUnitario(valor)
            .setValorTotal(valor)
            .setValorReal(valor)
            .setFlagSemestrales(flagSemestrales);
    if (EGlobal.Concepto.INFORMATIVO.equalsIgnoreCase(infoConcepto.getConOperacion())) {
      valorConcepto.setValorReal(0D);
    }
//    System.out.print("valorrealdelconepto->"+infoConcepto.getConAbreviatura()+"\n valor->"+valorConcepto);
    listaConceptosLiquidados.add(valorConcepto.redondear());
  }

  /**
   * Devuelve la lista de los conceptos liquidados
   *
   * @return lista de conceptos
   */
  public List<ValorConceptoDTO> getListaConceptosLiquidados()
  {
    return listaConceptosLiquidados;
  }

  /**
   * Liquida un concepto
   *
   * @param infoConcepto Variable o concepto a liquidar
   * @throws AplicacionExcepcion
   */
  private Double calcularValorConcepto(ConConcepto infoConcepto)
          throws AplicacionExcepcion
  {
//      if (infoConcepto.getUniConcepto().getUniIderegistro() == 3230 )
//      {
//          System.out.println("estamos en el calculo del concepto");
//      }
    validarConceptoRelacionados(infoConcepto.getListaRelacionados());
//    System.out.print("infoformula->"+infoConcepto.getConFormula());
    if ((EGlobal.Concepto.TIPO_VALOR.equalsIgnoreCase(infoConcepto.getConTipcalculo())) ||(infoConcepto.getConFormula()==null)) {
      return calcularConceptoTipoValor(infoConcepto);
    }else{
       return calcularConceptoFormula(infoConcepto, infoConcepto.getConFormula()); 
    }
  }

  /**
   * Valida y calcula el valor del concepto cuando es tipo valor
   *
   * @param infoConcepto información del concepto
   * @return Valor del concepto calculado
   * @throws AplicacionExcepcion No se pudo calcular el valor del concepto
   */
  private Double calcularConceptoTipoValor(ConConcepto infoConcepto)
          throws AplicacionExcepcion
  {
    if(infoConcepto.getUniConcepto().getUniIderegistro().equals
        (conceptoDAO.consultarPorAbreviatura("ms-QAJUS").getUniConcepto().getUniIderegistro())){
        return calcularQaAjusteDevolucion(true);
    }else if(infoConcepto.getUniConcepto().getUniIderegistro().equals
        (conceptoDAO.consultarPorAbreviatura("ms-QADEV").getUniConcepto().getUniIderegistro())){
        return calcularQaAjusteDevolucion(false);
    }
    if (EGlobal.Concepto.NO_APLICA.equals(infoConcepto.getConTipregistro())) {
      return ejecutarFuncion(infoConcepto.getFunIderegistro());
    }

    if (tieneValor(infoConcepto.getConValor())) {
      return infoConcepto.getConValor();
    }

    if (tieneValor(infoConcepto.getFunIderegistro())) {
      return ejecutarFuncion(infoConcepto.getFunIderegistro());
    }
    if (EEstadoGenerico.SI.equalsIgnoreCase(infoConcepto.getConValnulo())) {
      return null;
    }
    throw new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_LIQUIDADO, infoConcepto.getConNombre());
  }
  
  private Double calcularQaAjusteDevolucion(Boolean estadoActual)
  throws AplicacionExcepcion {
            valor = 0.0;
            List<PorcentajeFinalRecalculoDTO> listaPromediosAjustados = vrtaVarteraprPromDAO.obtenerValoresPromActualizacion(1, argumentos.getEntero("idPeriodo"), argumentos.getEntero("idConsecutivo"), "R");
            List<PorcentajeFinalRecalculoDTO> listaPromediosIniciales = vrtaVarteraprPromDAO.obtenerValoresPromActualizacion(1, argumentos.getEntero("idPeriodo"), argumentos.getEntero("idConsecutivo") - 1,"R");
            
            List<PorcentajeFinalRecalculoDTO> listaPromediosFinalesAjuste = new ArrayList<PorcentajeFinalRecalculoDTO>();
            List<PorcentajeFinalRecalculoDTO> listaPromediosFinalesDevolucion = new ArrayList<PorcentajeFinalRecalculoDTO>();
            listaPromediosAjustados.stream().forEach(promedioAjustado -> {
                
                PorcentajeFinalRecalculoDTO findPromedio = listaPromediosIniciales.stream()
                    .filter(item -> promedioAjustado.getTerIdeRegistro().equals(item.getTerIdeRegistro()))
                    .findAny()
                    .orElse(null);
                
                if (findPromedio != null) {
                    if (promedioAjustado.getVrtaPromValor().equals(findPromedio.getVrtaPromValor())) {
                        valor = valor + findPromedio.getVrtaPromValor();
                    } else {
                        if((promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor())>0 && estadoActual){
                            valor = valor + (promedioAjustado.getVrtaPromValor());
                        }else if ((promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor())>0 && (!estadoActual)){
                            valor = valor + (findPromedio.getVrtaPromValor());
                        }else if((promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor())<0 && estadoActual){
                            valor = valor + (findPromedio.getVrtaPromValor());
                        }else{
                            valor = valor + (promedioAjustado.getVrtaPromValor());
                        }
//                        // agrega la diferencia de cada asociacion
//                        listaPromediosFinalesAjuste.add(new PorcentajeFinalRecalculoDTO(promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor(),
//                                                    numeroActualizacion, promedioAjustado.getTerIdeRegistro()));
//                        }else{
//                            Logger.getLogger(RecalculoAprovechamientoServicio.class.getName()).log(Level.INFO, 
//                                "\nmayorque\n");
//                        // Saca el total de las diferencias
//                        totalDiferenciasDevolucion = totalDiferenciasDevolucion + (promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor());
//                        // agrega la diferencia de cada asociacion
//                        listaPromediosFinalesDevolucion.add(new PorcentajeFinalRecalculoDTO(promedioAjustado.getVrtaPromValor() - findPromedio.getVrtaPromValor(),
//                                                    numeroActualizacion, promedioAjustado.getTerIdeRegistro()));    
//                        }
                    }
                } else {
                    if(estadoActual){
                        valor = valor + (promedioAjustado.getVrtaPromValor());
                    }
                }
            });
    return valor;                   
  } 


  /**
   * Ejecuta la fórmula de los conceptos
   *
   * @param infoConcepto Variablo o concepto a calcular
   * @throws AplicacionExcepcion Error al procesar la fórmula
   */
  private Double calcularConceptoFormula(ConConcepto infoConcepto, String formula)
          throws AplicacionExcepcion
  {
      Log.info("alcularConceptoFormula");
    List<FragmentoDTO> listaFragmentos = jsonObjeto(formula, TipoGenericoUtil.listaFragmento());
    StringBuilder infoFormula = new StringBuilder();
    for (FragmentoDTO fragmento : listaFragmentos) {
        LogUtil.info("esta es formula2->"+fragmento.getTipo()+" fin");
      switch (fragmento.getTipo()) {
        case EGlobal.Concepto.Formula.CONCEPTO:
          int idConcepto ;
          if (fragmento.getIdconcepto() == null)
          {
            idConcepto = Integer.parseInt(fragmento.getId());
          }
          else 
          {
            idConcepto = Integer.parseInt(fragmento.getIdconcepto());
          }          
          ValorConceptoDTO valorConcepto = buscarConceptoLiquidadoObligatorio(idConcepto);
          infoFormula.append(valorConcepto.getValorTotal());
          
          break;
        case EGlobal.Concepto.Formula.FUNCION:
          int idFuncion = Integer.parseInt(fragmento.getId());
          argumentos.agregar("idconcepto", infoConcepto.getUniConcepto().getUniIderegistro());
//          if(idFuncion==225){//valida cuando la funcion sea 225->fn_calcula_minimo
//              List<ConConcepto> listaConceptos = conceptoDAO.consultarConceptosCorrelacionados(infoConcepto.getUniConcepto().getUniIderegistro());
//              int i=1;
//              for(ConConcepto conceptos: listaConceptos){
//                ValorConceptoDTO fcrt = buscarConceptoLiquidadoObligatorio(conceptos.getUniConcepto().getUniIderegistro());
//                argumentos.agregar("f"+i, fcrt.getValorTotal());
//                i++;
//              }
              //System.out.print("f1->"+f1crt.getValorTotal()+"\n f2->"+f2crt.getValorTotal());
//          }
          Double valorFuncion = ejecutarFuncion(idFuncion);
          infoFormula.append(valorFuncion);
          break;
        default:
          infoFormula.append(fragmento.getValor());
      }
    }
    return evaluarFormula(infoFormula, infoConcepto);
  }

  /**
   * Ejecuta una expresión aritmética
   *
   * @param infoFormula Expresión aritmética a evaluar
   * @param infoConcepto Información de la variable a procesar
   * @return Valor evaluado
   * @throws AplicacionExcepcion Error al ejecutar la función
   */
  @SuppressWarnings("UseSpecificCatch")
  private Double evaluarFormula(StringBuilder infoFormula, ConConcepto infoConcepto)
          throws AplicacionExcepcion
  {
    try {
      ScriptEngineManager administrador = new ScriptEngineManager();
      ScriptEngine algoritmo = administrador.getEngineByName("js");
      Object valorFormula = algoritmo.eval(infoFormula.toString());
      return Double.valueOf(valorFormula.toString());
    } catch (Exception ex) {
      LogUtil.error(ex);
      throw new AplicacionExcepcion(EMensajeNegocio.ERROR_EVALUAR_FUNCION, infoConcepto.getConNombre() + " (fórmula: " + infoFormula + ")");
    }
  }

  /**
   * Se valida que las variables relacionadas estén liquidados de lo contrario
   * se procede a liquidar
   *
   * @param listaRelacionados Listado de los conceptos relacionados
   * @throws AplicacionExcepcion Error
   */
  private void validarConceptoRelacionados(List<CoreConrelacio> listaRelacionados)
          throws AplicacionExcepcion
  {
//    System.out.println("verificandopadres->"+(listaRelacionados));    
//    System.out.println("verificandopadresnulo->"+listaRelacionados.isEmpty());  
    if (listaRelacionados==null) {
//        System.out.println("verificandopadresnulo->");
      return;
    }
    for (CoreConrelacio relacionado : listaRelacionados) {
      Integer idConcepto = relacionado.getUniConrelacion().getUniConcepto().getUniIderegistro();
      ValorConceptoDTO valor = buscarConceptoLiquidado(idConcepto);
//      System.out.print("valordto->"+valor);
      if (valor != null) {
        continue;
      }
//      System.out.print("concepto relacionados.................\n"+"conceptorel->"+relacionado.getUniConcepto().getConAbreviatura());
      Integer idRelacionado = relacionado.getUniConrelacion().getUniConcepto().getUniIderegistro();
      ConConcepto concepto = listaConceptoLiquidar.stream()
              .filter((infoConcepto) -> Objects.equals(infoConcepto.getUniConcepto().getUniIderegistro(), idRelacionado))
              .findFirst()
              .orElseThrow(()
                      -> new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_RELACIONADO, "con|"+idRelacionado.toString()));
      calcularValorConcepto(concepto);
    }
  }

  /**
   * Valida si un concepto está liquidado
   *
   * @param idConcepto Identificador del concepto a buscar
   * @return NULL o Información del concepto
   */
  private ValorConceptoDTO buscarConceptoLiquidado(int idConcepto)
  {
//      if (idConcepto == 3233 )
//      {
//        System.out.println("estamos en el calculo del concepto");
//      }
    return listaConceptosLiquidados.stream()
            .filter((valor) -> valor.getConcepto().getUniConcepto().getUniIderegistro() == idConcepto)
            .findFirst()
            .orElse(null);
  }

  /**
   * Requiere que un concepto esté liquidado antes de calcular el siguiente
   *
   * @param idConcepto Identificador del concepto que se requiere que esté
   * liquidado
   * @return Infomación del concepto liquidado
   * @throws AplicacionExcepcion No se encontró el concepto
   */
  private ValorConceptoDTO buscarConceptoLiquidadoObligatorio(int idConcepto)
          throws AplicacionExcepcion
  {
    ValorConceptoDTO concepto = buscarConceptoLiquidado(idConcepto);
    if (tieneValor(concepto)) {
      return concepto;
    }
//    
//    if (idConcepto == 2679 )
//      {
//        System.out.println("estamos en el calculo del concepto");
//      }
    ConConcepto infoConcepto = conceptoDAO.consultar(idConcepto);
    if (EEstadoGenerico.SI.equalsIgnoreCase(infoConcepto.getConValnulo())) {
      return new ValorConceptoDTO(infoConcepto)
              .setCantidad(1D)
              .setValorUnitario(0D)
              .setValorReal(0D)
              .setValorTotal(0D);
    }
    throw new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_LIQUIDADO, infoConcepto.getConNombre());
  }

  /**
   * La variable no se encontró
   *
   * @param abreviatura identificador de la variable
   * @param listaConceptos Lista de variables liquidadas
   * @return
   * @throws NegocioExcepcion
   */
  public static ValorConceptoDTO buscarConcepto(String abreviatura, List<ValorConceptoDTO> listaConceptos)
          throws NegocioExcepcion
  {
    return listaConceptos.stream()
            .filter((valor) -> valor.getConcepto().getConAbreviatura().startsWith(abreviatura))
            .findAny()
            .orElseThrow(() -> new NegocioExcepcion(EMensajeNegocio.ERROR_CONCEPTO_NO_ENCONTRADO, abreviatura));
  }
}
