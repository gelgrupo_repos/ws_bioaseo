/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.negocio.delegado;

import com.gell.estandar.util.FuncionesDatoUtil;
import com.gell.estandar.dto.AuditoriaDTO;
import java.sql.Connection;
import javax.sql.DataSource;

/**
 * Se crean los delegados cuando la lógica de un serivicio es demasidao extensa
 *
 * @author god
 */
public abstract class GenericoDelegado extends FuncionesDatoUtil
{

  protected DataSource datasource;
  protected AuditoriaDTO auditoria;
  protected Connection cnn;

  /**
   * Constructor de la clase
   *
   * @param datasource conexión a la base de datos
   * @param auditoria Información del usuario logeado
   */
  public GenericoDelegado(DataSource datasource, AuditoriaDTO auditoria)
  {
    this.datasource = datasource;
    this.auditoria = auditoria;
  }

  public GenericoDelegado(Connection cnn, AuditoriaDTO auditoria)
  {
    this.cnn = cnn;
    this.auditoria = auditoria;
  }

}
