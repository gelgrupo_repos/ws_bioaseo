/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.servlet.MultipartConfigElement;
import javax.sql.DataSource;
import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 *
 * @author god
 */
@Configuration
@ComponentScan
@EnableAutoConfiguration
public class Aplicacion extends SpringBootServletInitializer
{
   @Value("${datasource.url}")
   private String url;
   @Value("${datasource.username}")
   private String user;
   @Value("${datasource.password}")
   private String pass;
   @Value("${datasource.url.talend}")
   private String urlTalend;
   @Value("${datasource.username.talend}")
   private String userTalend;
   @Value("${datasource.password.talend}")
   private String passTalend;
   

  public static void main(String[] args)
  {
    SpringApplication.run(Aplicacion.class, args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
  {
    return builder.sources(Aplicacion.class);
  }

//  @Bean
//  @Qualifier("prisma")
//  public DataSource getDataSource()
//          throws Exception
//  {
//    Context context = new InitialContext();
//    DataSource ds = (DataSource) context.lookup("java:/PoolAgauSures");
//    //DataSource ds = (DataSource) context.lookup("java:/Poolllanogas");
//    return ds;
//  }

    @Bean
    @Primary
    public DataSource getDataSource()
            throws Exception
    { 
      PGSimpleDataSource ds = new PGSimpleDataSource();
      System.out.println(" ----------- credenciales->\nurl->"+url+"\n user->"+user+"pass->"+pass+"\n");
      ds.setURL(url);
      ds.setUser(user);
      ds.setPassword(pass);
      return ds;
    }

    @Bean
    @Qualifier("talend")
    public DataSource getDataSourceTalend() throws Exception {
      PGSimpleDataSource ds = new PGSimpleDataSource();
      System.out.println(" ----------- credenciales ralend->\nurl->"+urlTalend+"\n user->"+userTalend+"pass->"+passTalend+"\n");
      ds.setURL(urlTalend);
      ds.setUser(userTalend);
      ds.setPassword(passTalend);
        return ds;
    }  
  /**
   * Método encargado de configurar el tamaño máximo de los archivos
   *
   * @return
   */
  @Bean
  public MultipartConfigElement multipartConfigElement()
  {
    MultipartConfigFactory factory = new MultipartConfigFactory();
    factory.setMaxFileSize("350MB");
    factory.setMaxRequestSize("500MB");
    return factory.createMultipartConfig();

  }

  @Bean
  public WebMvcConfigurer corsConfigurer()
  {
    return new WebMvcConfigurerAdapter()
    {
      @Override
      public void addCorsMappings(CorsRegistry registry)
      {
        registry.addMapping("/**")
                .allowedMethods("*")
                .allowedHeaders("*")
                .allowedOrigins("*");
      }
    };
  }
}
