/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.aplicacion.seguridad;

import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.comunicacion.ClienteToken;
import com.gell.estandar.constante.EAplicacion;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.constante.ERuta;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.negocio.servicio.SeguridadServicio;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

/**
 *
 * @author god
 */
@SuppressWarnings("UseSpecificCatch")
public class JwtFiltroTaras extends GenericFilterBean
{

  @Autowired
  private SeguridadServicio seguridadServicio;
  
   @Value("${url.auth}")
   private String urlAuth;

  @Override
  public void doFilter(ServletRequest request, ServletResponse respuesta, FilterChain filter)
          throws IOException, ServletException
  {
    HttpServletRequest peticion = (HttpServletRequest) request;
    HttpServletResponse servletResponse = (HttpServletResponse) respuesta;
    servletResponse.setHeader("Access-Control-Allow-Origin", "*");
    servletResponse.setHeader("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
    servletResponse.setHeader("Access-Control-Allow-Credentials", "true");
    servletResponse.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
    servletResponse.setHeader("Access-Control-Max-Age", "1209600");
    if (peticion.getMethod().equalsIgnoreCase("OPTIONS")) {
      servletResponse.setStatus(200);
      return;
    }
    AuditoriaDTO auditoria;
    try {
      String ruta = peticion.getServletPath();
      switch (ruta) {
        case ERuta.Global.ARCHIVO_CONSULTAR_GRANDE:
          String tokenParametro = request.getParameter("token");
          validarToken(tokenParametro);
        case ERuta.Global.INICIO_SESION_PRISMA:
          filter.doFilter(request, respuesta);
          return;
        default:
          String rutaInicial = peticion.getHeader("route_url_origin");
          String token = getToken((HttpServletRequest) request);
          auditoria = validarToken(token);
          validarURL(rutaInicial, auditoria);
          break;
      }
    } catch (Exception ex) {
      System.out.print(ex.getMessage());
      HttpServletResponse res = (HttpServletResponse) respuesta;
      res.sendError(401, EMensajeNegocio.ERROR_SESION_EXPIRADO.getMensaje());
      return;
    }
    servletResponse.setHeader("token", generarToken(auditoria));
    filter.doFilter(request, respuesta);
  }

  /**
   * Método que valida si la URL se encuentra asociada con algún registro en BD
   *
   * @param rutaInicial Identificadot que contiene la Url
   * @throws MalformedURLException
   * @throws AplicacionExcepcion
   */
  private void validarURL(String rutaInicial, AuditoriaDTO auditoria)
          throws MalformedURLException, AplicacionExcepcion
  {
    LogUtil.info("URL: " + rutaInicial);
    if (rutaInicial == null) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_PERMISO);
    }
    URL urlOrigin = new URL(rutaInicial);
    Integer idPrograma = seguridadServicio.existeURL(urlOrigin.getPath());
    auditoria.setParametro("idPrograma", idPrograma.toString());
  }

  /**
   * Realiza una petición al proyecto de autenticador para validar si el token
   * es correcto y tiene la información adecuada
   *
   * @param token String cifrado
   * @return Información del token descifrado
   * @throws IOException
   * @throws AplicacionExcepcion Error al descifrar el token o token vencido
   */
  private AuditoriaDTO validarToken(String token)
          throws IOException, AplicacionExcepcion
  {
    ClienteToken cliente = new ClienteToken(EAplicacion.DORBI,urlAuth);
    AuditoriaDTO auditoria = cliente.validarToken(token);
    auditoria.setToken(token);
    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(auditoria, null, new ArrayList<>()));
    return auditoria;
  }

  /**
   * Obtiene el token de las cabeceras de la petición
   *
   * @param request Información de la petición
   * @return Devuelve el token que trae la petición
   */
  private String getToken(HttpServletRequest request)
  {
    return request.getHeader("Authorization");
  }

  /**
   * Actualiza el token con la nueva información
   *
   * @param auditoria Información con la que se va a generar el token
   * @return Nuevo token
   */
  private String generarToken(AuditoriaDTO auditoria)
  {
    try {
      ClienteToken cliente = new ClienteToken(EAplicacion.DORBI,urlAuth);
      return cliente.renovar(auditoria);
    } catch (AplicacionExcepcion ex) {
      LogUtil.error(ex);
      return "";
    }
  }

}
