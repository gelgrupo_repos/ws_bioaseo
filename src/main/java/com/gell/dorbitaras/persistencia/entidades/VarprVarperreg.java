package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VarprVarperreg extends Entidad implements Serializable
{

  private Integer varprIderegistro;
  private PerPeriodo perIderegistro;
  private ArprAreaprestacion arprIderegistro;
  private ConConcepto conIderegistro;
  private Double varprValor;
  private String varprEstado;
  private Integer usuIderegistroGb;
  private Date varprFecgrabacion;
  private Integer usuIderegistroCer;
  private Date varprFeccertificacion;
  private Integer empIderegistro;
  private RacoRanconcept racoIderegistro;
  private String varprEstadoRegistro;
  private List<VarprVarperreg> listaRangos;
  private List<VarprVarperreg> listaValores;

  public VarprVarperreg()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getVarprIderegistro()
  {
    return varprIderegistro;
  }

  public VarprVarperreg setVarprIderegistro(Integer varprIderegistro)
  {
    this.varprIderegistro = varprIderegistro;
    return this;
  }

  public PerPeriodo getPerIderegistro()
  {
    return perIderegistro;
  }

  public VarprVarperreg setPerIderegistro(PerPeriodo perIderegistro)
  {
    this.perIderegistro = perIderegistro;
    return this;
  }

  public ArprAreaprestacion getArprIderegistro()
  {
    return arprIderegistro;
  }

  public VarprVarperreg setArprIderegistro(ArprAreaprestacion arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public ConConcepto getConIderegistro()
  {
    return conIderegistro;
  }

  public VarprVarperreg setConIderegistro(ConConcepto conIderegistro)
  {
    this.conIderegistro = conIderegistro;
    return this;
  }

  public Double getVarprValor()
  {
    return varprValor;
  }

  public VarprVarperreg setVarprValor(Double varprValor)
  {
    this.varprValor = varprValor;
    return this;
  }

  public String getVarprEstado()
  {
    return varprEstado;
  }

  public VarprVarperreg setVarprEstado(String varprEstado)
  {
    this.varprEstado = varprEstado;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public VarprVarperreg setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Date getVarprFecgrabacion()
  {
    return varprFecgrabacion;
  }

  public VarprVarperreg setVarprFecgrabacion(Date varprFecgrabacion)
  {
    this.varprFecgrabacion = varprFecgrabacion;
    return this;
  }

  public Integer getUsuIderegistroCer()
  {
    return usuIderegistroCer;
  }

  public VarprVarperreg setUsuIderegistroCer(Integer usuIderegistroCer)
  {
    this.usuIderegistroCer = usuIderegistroCer;
    return this;
  }

  public Date getVarprFeccertificacion()
  {
    return varprFeccertificacion;
  }

  public VarprVarperreg setVarprFeccertificacion(Date varprFeccertificacion)
  {
    this.varprFeccertificacion = varprFeccertificacion;
    return this;
  }

  public Integer getEmpIderegistro()
  {
    return empIderegistro;
  }

  public VarprVarperreg setEmpIderegistro(Integer empIderegistro)
  {
    this.empIderegistro = empIderegistro;
    return this;
  }

  public RacoRanconcept getRacoIderegistro()
  {
    return racoIderegistro;
  }

  public VarprVarperreg setRacoIderegistro(RacoRanconcept racoIderegistro)
  {
    this.racoIderegistro = racoIderegistro;
    return this;
  }

  public String getVarprEstadoRegistro()
  {
    return varprEstadoRegistro;
  }

  public VarprVarperreg setVarprEstadoRegistro(String varprEstadoRegistro)
  {
    this.varprEstadoRegistro = varprEstadoRegistro;
    return this;
  }

  public List<VarprVarperreg> getListaRangos()
  {
    return listaRangos;
  }

  public void setListaRangos(List<VarprVarperreg> listaRangos)
  {
    this.listaRangos = listaRangos;
  }

  public List<VarprVarperreg> getListaValores()
  {
    return listaValores;
  }

  public void setListaValores(List<VarprVarperreg> listaValores)
  {
    this.listaValores = listaValores;
  }

  // </editor-fold>
  @Override
  public VarprVarperreg validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
