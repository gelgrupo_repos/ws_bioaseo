package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.sql.Timestamp;

/**
 *
 * @author jhibarra
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaHistoricoRecalculos extends Entidad implements Serializable {

    private Integer vrtaHistoricoRecalculoIde;
    private Integer numeroActualizacion;
    private Integer varperregIde;
    private Integer periodoHijoIde;
    private Boolean conDinc;

    public VrtaHistoricoRecalculos() {
    }

    public VrtaHistoricoRecalculos(Integer numeroActualizacion, Integer varperregIde, Integer periodoHijoIde, Boolean conDinc) {
        this.numeroActualizacion = numeroActualizacion;
        this.varperregIde = varperregIde;
        this.periodoHijoIde = periodoHijoIde;
        this.conDinc = conDinc;
    }

    public Integer getVrtaHistoricoRecalculoIde() {
        return vrtaHistoricoRecalculoIde;
    }

    public void setVrtaHistoricoRecalculoIde(Integer vrtaHistoricoRecalculoIde) {
        this.vrtaHistoricoRecalculoIde = vrtaHistoricoRecalculoIde;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getVarperregIde() {
        return varperregIde;
    }

    public void setVarperregIde(Integer varperregIde) {
        this.varperregIde = varperregIde;
    }

    public Integer getPeriodoHijoIde() {
        return periodoHijoIde;
    }

    public void setPeriodoHijoIde(Integer periodoHijoIde) {
        this.periodoHijoIde = periodoHijoIde;
    }

    public Boolean getConDinc() {
        return conDinc;
    }

    public void setConDinc(Boolean conDinc) {
        this.conDinc = conDinc;
    }
    
    

    @Override
    public VrtaHistoricoRecalculos validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("vrtaHistoricoRecalculoIde", "requerido",
                        "El campo vrtaHistoricoRecalculoIde es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("varperregIde", "requerido",
                        "El campo varperregIde es obligatorio")
                .agregar("periodoHijoIde", "requerido",
                        "El campo periodoHijoIde fecha grabacion es obligatorio")
                .validar();
        return this;
    }
}
