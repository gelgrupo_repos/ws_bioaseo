package com.gell.dorbitaras.persistencia.entidades;

import com.gell.estandar.persistencia.abstracto.Entidad;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import java.io.Serializable;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EstEstructura extends Entidad implements Serializable {

  private Integer estIderegistro;
  private String estNombre;
  private Integer estNivel;
  private String estEstado;
  private String estTipordena;
  private Integer claIderegistro;
  private Integer usuIderegistro;
  private String estValida;

  public EstEstructura()
  {
  }

  public EstEstructura(Integer estIderegistro)
  {
    this.estIderegistro = estIderegistro;
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getEstIderegistro()
  {
    return estIderegistro;
  }

  public EstEstructura setEstIderegistro(Integer estIderegistro)
  {
    this.estIderegistro = estIderegistro;
    return this;
  }

  public String getEstNombre()
  {
    return estNombre;
  }

  public EstEstructura setEstNombre(String estNombre)
  {
    this.estNombre = estNombre;
    return this;
  }

  public Integer getEstNivel()
  {
    return estNivel;
  }

  public EstEstructura setEstNivel(Integer estNivel)
  {
    this.estNivel = estNivel;
    return this;
  }

  public String getEstEstado()
  {
    return estEstado;
  }

  public EstEstructura setEstEstado(String estEstado)
  {
    this.estEstado = estEstado;
    return this;
  }

  public String getEstTipordena()
  {
    return estTipordena;
  }

  public EstEstructura setEstTipordena(String estTipordena)
  {
    this.estTipordena = estTipordena;
    return this;
  }

  public Integer getClaIderegistro()
  {
    return claIderegistro;
  }

  public EstEstructura setClaIderegistro(Integer claIderegistro)
  {
    this.claIderegistro = claIderegistro;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public EstEstructura setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getEstValida()
  {
    return estValida;
  }

  public EstEstructura setEstValida(String estValida)
  {
    this.estValida = estValida;
    return this;
  }

  // </editor-fold>
  @Override
  public EstEstructura validar()
          throws NegocioExcepcion
  {
    return this;
  }
}
