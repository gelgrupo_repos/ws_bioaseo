package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocDocumento extends Entidad implements Serializable
{

  private Integer uniDocumento;
  private Integer estDocumento;
  private String docNombre;
  private String docAbreviatura;
  private String docFinanciable;
  private String docTipo;
  private String docContabiliza;
  private String docConsigna;
  private String docPresupuesto;
  private String docRecaudo;
  private Integer usuIderegistro;
  private String docDevolucion;
  private String docAnticipo;
  private String docRegistro;
  private String docNitcontabil;
  private Integer docMaximpresion;
  private Integer docPagpriori;
  private String docAplicafes;
  private String docAplicafelec;

  public DocDocumento()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getUniDocumento()
  {
    return uniDocumento;
  }

  public DocDocumento setUniDocumento(Integer uniDocumento)
  {
    this.uniDocumento = uniDocumento;
    return this;
  }

  public Integer getEstDocumento()
  {
    return estDocumento;
  }

  public DocDocumento setEstDocumento(Integer estDocumento)
  {
    this.estDocumento = estDocumento;
    return this;
  }

  public String getDocNombre()
  {
    return docNombre;
  }

  public DocDocumento setDocNombre(String docNombre)
  {
    this.docNombre = docNombre;
    return this;
  }

  public String getDocAbreviatura()
  {
    return docAbreviatura;
  }

  public DocDocumento setDocAbreviatura(String docAbreviatura)
  {
    this.docAbreviatura = docAbreviatura;
    return this;
  }

  public String getDocFinanciable()
  {
    return docFinanciable;
  }

  public DocDocumento setDocFinanciable(String docFinanciable)
  {
    this.docFinanciable = docFinanciable;
    return this;
  }

  public String getDocTipo()
  {
    return docTipo;
  }

  public DocDocumento setDocTipo(String docTipo)
  {
    this.docTipo = docTipo;
    return this;
  }

  public String getDocContabiliza()
  {
    return docContabiliza;
  }

  public DocDocumento setDocContabiliza(String docContabiliza)
  {
    this.docContabiliza = docContabiliza;
    return this;
  }

  public String getDocConsigna()
  {
    return docConsigna;
  }

  public DocDocumento setDocConsigna(String docConsigna)
  {
    this.docConsigna = docConsigna;
    return this;
  }

  public String getDocPresupuesto()
  {
    return docPresupuesto;
  }

  public DocDocumento setDocPresupuesto(String docPresupuesto)
  {
    this.docPresupuesto = docPresupuesto;
    return this;
  }

  public String getDocRecaudo()
  {
    return docRecaudo;
  }

  public DocDocumento setDocRecaudo(String docRecaudo)
  {
    this.docRecaudo = docRecaudo;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public DocDocumento setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getDocDevolucion()
  {
    return docDevolucion;
  }

  public DocDocumento setDocDevolucion(String docDevolucion)
  {
    this.docDevolucion = docDevolucion;
    return this;
  }

  public String getDocAnticipo()
  {
    return docAnticipo;
  }

  public DocDocumento setDocAnticipo(String docAnticipo)
  {
    this.docAnticipo = docAnticipo;
    return this;
  }

  public String getDocRegistro()
  {
    return docRegistro;
  }

  public DocDocumento setDocRegistro(String docRegistro)
  {
    this.docRegistro = docRegistro;
    return this;
  }

  public String getDocNitcontabil()
  {
    return docNitcontabil;
  }

  public DocDocumento setDocNitcontabil(String docNitcontabil)
  {
    this.docNitcontabil = docNitcontabil;
    return this;
  }

  public Integer getDocMaximpresion()
  {
    return docMaximpresion;
  }

  public DocDocumento setDocMaximpresion(Integer docMaximpresion)
  {
    this.docMaximpresion = docMaximpresion;
    return this;
  }

  public Integer getDocPagpriori()
  {
    return docPagpriori;
  }

  public DocDocumento setDocPagpriori(Integer docPagpriori)
  {
    this.docPagpriori = docPagpriori;
    return this;
  }

  public String getDocAplicafes()
  {
    return docAplicafes;
  }

  public DocDocumento setDocAplicafes(String docAplicafes)
  {
    this.docAplicafes = docAplicafes;
    return this;
  }

  public String getDocAplicafelec()
  {
    return docAplicafelec;
  }

  public DocDocumento setDocAplicafelec(String docAplicafelec)
  {
    this.docAplicafelec = docAplicafelec;
    return this;
  }

  // </editor-fold>
  @Override
  public DocDocumento validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
