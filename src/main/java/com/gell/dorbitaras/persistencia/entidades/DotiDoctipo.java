package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DotiDoctipo extends Entidad implements Serializable
{

  private DocDocumento uniDocumento;
  private TidoTipdocumen uniTipdocument;
  private Integer estDocumento;
  private Integer estTipdocument;
  private Integer dotiIderegistr;
  private Integer usuIderegistro;

  public DotiDoctipo()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public DocDocumento getUniDocumento()
  {
    return uniDocumento;
  }

  public DotiDoctipo setUniDocumento(DocDocumento uniDocumento)
  {
    this.uniDocumento = uniDocumento;
    return this;
  }

  public TidoTipdocumen getUniTipdocument()
  {
    return uniTipdocument;
  }

  public DotiDoctipo setUniTipdocument(TidoTipdocumen uniTipdocument)
  {
    this.uniTipdocument = uniTipdocument;
    return this;
  }

  public Integer getEstDocumento()
  {
    return estDocumento;
  }

  public DotiDoctipo setEstDocumento(Integer estDocumento)
  {
    this.estDocumento = estDocumento;
    return this;
  }

  public Integer getEstTipdocument()
  {
    return estTipdocument;
  }

  public DotiDoctipo setEstTipdocument(Integer estTipdocument)
  {
    this.estTipdocument = estTipdocument;
    return this;
  }

  public Integer getDotiIderegistr()
  {
    return dotiIderegistr;
  }

  public DotiDoctipo setDotiIderegistr(Integer dotiIderegistr)
  {
    this.dotiIderegistr = dotiIderegistr;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public DotiDoctipo setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public DotiDoctipo validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
