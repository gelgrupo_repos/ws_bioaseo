package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.sql.Timestamp;

/**
 *
 * @author jhibarra
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaVarteraprHist extends Entidad implements Serializable {

    private Integer vrtaHistIderegistro;
    private Integer numeroActualizacion;
    private Integer vrtaIderegistro;
    private Timestamp vrtaHistFecgrabacion;

    public VrtaVarteraprHist() {
    }

    public VrtaVarteraprHist(Integer numeroActualizacion, Integer vrtaIderegistro, Timestamp vrtaHistFecgrabacion) {
        this.numeroActualizacion = numeroActualizacion;
        this.vrtaIderegistro = vrtaIderegistro;
        this.vrtaHistFecgrabacion = vrtaHistFecgrabacion;
    }
    
    // <editor-fold defaultstate="collapsed" desc="GET-SET">
    public Integer getVrtaHistIderegistro() {
        return vrtaHistIderegistro;
    }

    public void setVrtaHistIderegistro(Integer vrtaHistIderegistro) {
        this.vrtaHistIderegistro = vrtaHistIderegistro;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getVrtaIderegistro() {
        return vrtaIderegistro;
    }

    public void setVrtaIderegistro(Integer vrtaIderegistro) {
        this.vrtaIderegistro = vrtaIderegistro;
    }

    public Timestamp getVrtaHistFecgrabacion() {
        return vrtaHistFecgrabacion;
    }

    public void setVrtaHistFecgrabacion(Timestamp vrtaHistFecgrabacion) {
        this.vrtaHistFecgrabacion = vrtaHistFecgrabacion;
    }
    // </editor-fold>
    
    @Override
    public VrtaVarteraprHist validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("vrtaHistIderegistro", "requerido",
                        "El campo vrtaHistIderegistro es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("vrtaIderegistro", "requerido",
                        "El campo Ideregistro es obligatorio")
                .agregar("vrtaHistFecgrabacion", "requerido",
                        "El campo Historico fecha grabacion es obligatorio")
                .validar();
        return this;
    }
}
