package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ImpImportar extends Entidad implements Serializable {

  private Long impIderegistro;
  private String impDescripcion;
  private String impRutaarchivo;
  private Boolean impNombreprimerfila;
  private Integer impDelimitacion;
  private String impSeperadorregistro;
  private String impSeparadorcampo;
  private Integer empIderegistsro;
  private Long usuIderegistro;
  private Date fechaRegistro;
  private List<DimpDetalleimportar> infoDestino;
  
  public ImpImportar()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Long getImpIderegistro()
  {
    return impIderegistro;
  }

  public ImpImportar setImpIderegistro(Long impIderegistro)
  {
    this.impIderegistro = impIderegistro;
    return this;
  }

  public String getImpDescripcion()
  {
    return impDescripcion;
  }

  public ImpImportar setImpDescripcion(String impDescripcion)
  {
    this.impDescripcion = impDescripcion;
    return this;
  }

  public String getImpRutaarchivo()
  {
    return impRutaarchivo;
  }

  public ImpImportar setImpRutaarchivo(String impRutaarchivo)
  {
    this.impRutaarchivo = impRutaarchivo;
    return this;
  }

  public Boolean getImpNombreprimerfila()
  {
    return impNombreprimerfila;
  }

  public ImpImportar setImpNombreprimerfila(Boolean impNombreprimerfila)
  {
    this.impNombreprimerfila = impNombreprimerfila;
    return this;
  }

  public Integer getImpDelimitacion()
  {
    return impDelimitacion;
  }

  public ImpImportar setImpDelimitacion(Integer impDelimitacion)
  {
    this.impDelimitacion = impDelimitacion;
    return this;
  }

  public String getImpSeperadorregistro()
  {
    return impSeperadorregistro;
  }

  public ImpImportar setImpSeperadorregistro(String impSeperadorregistro)
  {
    this.impSeperadorregistro = impSeperadorregistro;
    return this;
  }

  public String getImpSeparadorcampo()
  {
    return impSeparadorcampo;
  }

  public ImpImportar setImpSeparadorcampo(String impSeparadorcampo)
  {
    this.impSeparadorcampo = impSeparadorcampo;
    return this;
  }

  public Integer getEmpIderegistsro()
  {
    return empIderegistsro;
  }

  public ImpImportar setEmpIderegistsro(Integer empIderegistsro)
  {
    this.empIderegistsro = empIderegistsro;
    return this;
  }

  public Long getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public ImpImportar setUsuIderegistro(Long usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public Date getFechaRegistro()
  {
    return fechaRegistro;
  }

  public ImpImportar setFechaRegistro(Date fechaRegistro)
  {
    this.fechaRegistro = fechaRegistro;
    return this;
  }

  public List<DimpDetalleimportar> getInfoDestino()
  {
    return infoDestino;
  }

  public ImpImportar setInfoDestino(List<DimpDetalleimportar> infoDestino)
  {
    this.infoDestino = infoDestino;
    return this;
  }

  
  // </editor-fold>
@Override
  public ImpImportar validar()
          throws AplicacionExcepcion
  {

    ValidarEntidad.construir(this)
            .agregar("impDescripcion", "requerido",
                    "El campo Descripción no puede estar vacio")
            .agregar("impRutaarchivo", "requerido",
                    "El campo Ruta no puede estar vacio")
            .agregar("impDelimitacion", "requerido",
                    "El campo Delimitacion no puede estar vacio")
            .validar();
    return this;
  }
}
