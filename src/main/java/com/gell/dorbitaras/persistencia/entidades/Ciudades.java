package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Ciudades extends Entidad implements Serializable {

  private String ciudadCod;
  private String ciudadNom;
  private String ciudadCoddep;
  private String ciudadCodemp;

  public Ciudades()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public String getCiudadCod()
  {
    return ciudadCod;
  }

  public Ciudades setCiudadCod(String ciudadCod)
  {
    this.ciudadCod = ciudadCod;
    return this;
  }

  public String getCiudadNom()
  {
    return ciudadNom;
  }

  public Ciudades setCiudadNom(String ciudadNom)
  {
    this.ciudadNom = ciudadNom;
    return this;
  }

  public String getCiudadCoddep()
  {
    return ciudadCoddep;
  }

  public Ciudades setCiudadCoddep(String ciudadCoddep)
  {
    this.ciudadCoddep = ciudadCoddep;
    return this;
  }

  public String getCiudadCodemp()
  {
    return ciudadCodemp;
  }

  public Ciudades setCiudadCodemp(String ciudadCodemp)
  {
    this.ciudadCodemp = ciudadCodemp;
    return this;
  }

  // </editor-fold>
  @Override
  public Ciudades validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
