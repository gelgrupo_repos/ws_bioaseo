package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DimpDetalleimportar extends Entidad implements Serializable {

  private Long dimpIderegistro;
  private Long impIderegistro;
  private String dimpTabladestino;
  private String dimpDescripcion;
  private Integer dimpOrdentransaccional;
  private String dimpDepedenciafuncional;
  private String dimpCampodependenciafuncional;
  private String dimpDescripcioncampo;
  private Integer dimpOrden;
  private Integer dimpInicio;
  private Integer dimpFin;
  private Integer dimpValordefecto;
  private String dimpCampodestino;
  private String dimpExpresionregular;
  private Integer dimpLongitudmaxima;
  private Boolean dimpRequerido;

  public DimpDetalleimportar()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Long getDimpIderegistro()
  {
    return dimpIderegistro;
  }

  public DimpDetalleimportar setDimpIderegistro(Long dimpIderegistro)
  {
    this.dimpIderegistro = dimpIderegistro;
    return this;
  }

  public Long getImpIderegistro()
  {
    return impIderegistro;
  }

  public DimpDetalleimportar setImpIderegistro(Long impIderegistro)
  {
    this.impIderegistro = impIderegistro;
    return this;
  }

  public String getDimpTabladestino()
  {
    return dimpTabladestino;
  }

  public DimpDetalleimportar setDimpTabladestino(String dimpTabladestino)
  {
    this.dimpTabladestino = dimpTabladestino;
    return this;
  }

  public String getDimpDescripcion()
  {
    return dimpDescripcion;
  }

  public DimpDetalleimportar setDimpDescripcion(String dimpDescripcion)
  {
    this.dimpDescripcion = dimpDescripcion;
    return this;
  }

  public Integer getDimpOrdentransaccional()
  {
    return dimpOrdentransaccional;
  }

  public DimpDetalleimportar setDimpOrdentransaccional(Integer dimpOrdentransaccional)
  {
    this.dimpOrdentransaccional = dimpOrdentransaccional;
    return this;
  }

  public String getDimpDepedenciafuncional()
  {
    return dimpDepedenciafuncional;
  }

  public DimpDetalleimportar setDimpDepedenciafuncional(String dimpDepedenciafuncional)
  {
    this.dimpDepedenciafuncional = dimpDepedenciafuncional;
    return this;
  }

  public String getDimpCampodependenciafuncional()
  {
    return dimpCampodependenciafuncional;
  }

  public DimpDetalleimportar setDimpCampodependenciafuncional(String dimpCampodependenciafuncional)
  {
    this.dimpCampodependenciafuncional = dimpCampodependenciafuncional;
    return this;
  }

  public String getDimpDescripcioncampo()
  {
    return dimpDescripcioncampo;
  }

  public DimpDetalleimportar setDimpDescripcioncampo(String dimpDescripcioncampo)
  {
    this.dimpDescripcioncampo = dimpDescripcioncampo;
    return this;
  }

  public Integer getDimpOrden()
  {
    return dimpOrden;
  }

  public DimpDetalleimportar setDimpOrden(Integer dimpOrden)
  {
    this.dimpOrden = dimpOrden;
    return this;
  }

  public Integer getDimpInicio()
  {
    return dimpInicio;
  }

  public DimpDetalleimportar setDimpInicio(Integer dimpInicio)
  {
    this.dimpInicio = dimpInicio;
    return this;
  }

  public Integer getDimpFin()
  {
    return dimpFin;
  }

  public DimpDetalleimportar setDimpFin(Integer dimpFin)
  {
    this.dimpFin = dimpFin;
    return this;
  }

  public Integer getDimpValordefecto()
  {
    return dimpValordefecto;
  }

  public DimpDetalleimportar setDimpValordefecto(Integer dimpValordefecto)
  {
    this.dimpValordefecto = dimpValordefecto;
    return this;
  }

  public String getDimpCampodestino()
  {
    return dimpCampodestino;
  }

  public DimpDetalleimportar setDimpCampodestino(String dimpCampodestino)
  {
    this.dimpCampodestino = dimpCampodestino;
    return this;
  }

  public String getDimpExpresionregular()
  {
    return dimpExpresionregular;
  }

  public DimpDetalleimportar setDimpExpresionregular(String dimpExpresionregular)
  {
    this.dimpExpresionregular = dimpExpresionregular;
    return this;
  }

  public Integer getDimpLongitudmaxima()
  {
    return dimpLongitudmaxima;
  }

  public DimpDetalleimportar setDimpLongitudmaxima(Integer dimpLongitudmaxima)
  {
    this.dimpLongitudmaxima = dimpLongitudmaxima;
    return this;
  }

  public Boolean getDimpRequerido()
  {
    return dimpRequerido;
  }

  public DimpDetalleimportar setDimpRequerido(Boolean dimpRequerido)
  {
    this.dimpRequerido = dimpRequerido;
    return this;
  }

  // </editor-fold>
  @Override
  public DimpDetalleimportar validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
