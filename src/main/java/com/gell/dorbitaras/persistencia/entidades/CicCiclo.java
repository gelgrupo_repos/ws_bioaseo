package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CicCiclo extends Entidad implements Serializable {

  private Integer cicIderegistro;
  private String cicNombre;
  private Integer cicDiainicia;
  private Integer cicDiafinaliza;
  private String cicEstado;
  private Integer cicPeriodos;
  private Integer cicAnoactual;
  private Integer usuIderegistro;

  public CicCiclo()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getCicIderegistro()
  {
    return cicIderegistro;
  }

  public CicCiclo setCicIderegistro(Integer cicIderegistro)
  {
    this.cicIderegistro = cicIderegistro;
    return this;
  }

  public String getCicNombre()
  {
    return cicNombre;
  }

  public CicCiclo setCicNombre(String cicNombre)
  {
    this.cicNombre = cicNombre;
    return this;
  }

  public Integer getCicDiainicia()
  {
    return cicDiainicia;
  }

  public CicCiclo setCicDiainicia(Integer cicDiainicia)
  {
    this.cicDiainicia = cicDiainicia;
    return this;
  }

  public Integer getCicDiafinaliza()
  {
    return cicDiafinaliza;
  }

  public CicCiclo setCicDiafinaliza(Integer cicDiafinaliza)
  {
    this.cicDiafinaliza = cicDiafinaliza;
    return this;
  }

  public String getCicEstado()
  {
    return cicEstado;
  }

  public CicCiclo setCicEstado(String cicEstado)
  {
    this.cicEstado = cicEstado;
    return this;
  }

  public Integer getCicPeriodos()
  {
    return cicPeriodos;
  }

  public CicCiclo setCicPeriodos(Integer cicPeriodos)
  {
    this.cicPeriodos = cicPeriodos;
    return this;
  }

  public Integer getCicAnoactual()
  {
    return cicAnoactual;
  }

  public CicCiclo setCicAnoactual(Integer cicAnoactual)
  {
    this.cicAnoactual = cicAnoactual;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public CicCiclo setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public CicCiclo validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
