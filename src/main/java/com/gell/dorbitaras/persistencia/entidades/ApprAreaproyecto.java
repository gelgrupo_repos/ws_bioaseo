package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApprAreaproyecto extends Entidad implements Serializable
{

  private Integer apprIderegistro;
  private ArprAreaprestacion arprIderegistro;
  private String apprSwtestado;
  private Integer usuIderegistroGb;
  private Integer usuIderegistroAct;
  private Date apprFecgrabacion;
  private Date apprFecact;
  private Proyectos proyectoIderegistro;

  public ApprAreaproyecto()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getApprIderegistro()
  {
    return apprIderegistro;
  }

  public ApprAreaproyecto setApprIderegistro(Integer apprIderegistro)
  {
    this.apprIderegistro = apprIderegistro;
    return this;
  }

  public ArprAreaprestacion getArprIderegistro()
  {
    return arprIderegistro;
  }

  public ApprAreaproyecto setArprIderegistro(ArprAreaprestacion arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public String getApprSwtestado()
  {
    return apprSwtestado;
  }

  public ApprAreaproyecto setApprSwtestado(String apprSwtestado)
  {
    this.apprSwtestado = apprSwtestado;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public ApprAreaproyecto setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Integer getUsuIderegistroAct()
  {
    return usuIderegistroAct;
  }

  public ApprAreaproyecto setUsuIderegistroAct(Integer usuIderegistroAct)
  {
    this.usuIderegistroAct = usuIderegistroAct;
    return this;
  }

  public Date getApprFecgrabacion()
  {
    return apprFecgrabacion;
  }

  public ApprAreaproyecto setApprFecgrabacion(Date apprFecgrabacion)
  {
    this.apprFecgrabacion = apprFecgrabacion;
    return this;
  }

  public Date getApprFecact()
  {
    return apprFecact;
  }

  public ApprAreaproyecto setApprFecact(Date apprFecact)
  {
    this.apprFecact = apprFecact;
    return this;
  }

  public Proyectos getProyectoIderegistro()
  {
    return proyectoIderegistro;
  }

  public ApprAreaproyecto setProyectoIderegistro(Proyectos proyectoIderegistro)
  {
    this.proyectoIderegistro = proyectoIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public ApprAreaproyecto validar()
          throws AplicacionExcepcion
  {
    ValidarEntidad.construir(this)
            .agregar("proyectoIderegistro.proyectoIderegistro", "requerido",
                    "El identificador del proyecto es obligatorio")
            .validar();
    return this;
  }
}
