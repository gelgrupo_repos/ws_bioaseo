package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TerTercero extends Entidad implements Serializable {

  private String terDocumento;
  private String terNombre;
  private String terApellido;
  private String terNomcompleto;
  private String terSexo;
  private String terTelcelular;
  private String terTelfijo;
  private Long terIderegistro;
  private EstEstructura estTiptercero;
  private UniUnidad uniTiptercero;
  private String terCorreo;
  private Integer usuIderegistro;
  private String ciudadCod;
  private Date terDocexpedicion;
  private Integer uniTipidentifica;
  private Date terFecnacimiento;

  public TerTercero()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public String getTerDocumento()
  {
    return terDocumento;
  }

  public TerTercero setTerDocumento(String terDocumento)
  {
    this.terDocumento = terDocumento;
    return this;
  }

  public String getTerNombre()
  {
    return terNombre;
  }

  public TerTercero setTerNombre(String terNombre)
  {
    this.terNombre = terNombre;
    return this;
  }

  public String getTerApellido()
  {
    return terApellido;
  }

  public TerTercero setTerApellido(String terApellido)
  {
    this.terApellido = terApellido;
    return this;
  }

  public String getTerNomcompleto()
  {
    return terNomcompleto;
  }

  public TerTercero setTerNomcompleto(String terNomcompleto)
  {
    this.terNomcompleto = terNomcompleto;
    return this;
  }

  public String getTerSexo()
  {
    return terSexo;
  }

  public TerTercero setTerSexo(String terSexo)
  {
    this.terSexo = terSexo;
    return this;
  }

  public String getTerTelcelular()
  {
    return terTelcelular;
  }

  public TerTercero setTerTelcelular(String terTelcelular)
  {
    this.terTelcelular = terTelcelular;
    return this;
  }

  public String getTerTelfijo()
  {
    return terTelfijo;
  }

  public TerTercero setTerTelfijo(String terTelfijo)
  {
    this.terTelfijo = terTelfijo;
    return this;
  }

  public Long getTerIderegistro()
  {
    return terIderegistro;
  }

  public TerTercero setTerIderegistro(Long terIderegistro)
  {
    this.terIderegistro = terIderegistro;
    return this;
  }

  public EstEstructura getEstTiptercero()
  {
    return estTiptercero;
  }

  public TerTercero setEstTiptercero(EstEstructura estTiptercero)
  {
    this.estTiptercero = estTiptercero;
    return this;
  }

  public UniUnidad getUniTiptercero()
  {
    return uniTiptercero;
  }

  public TerTercero setUniTiptercero(UniUnidad uniTiptercero)
  {
    this.uniTiptercero = uniTiptercero;
    return this;
  }

  public String getTerCorreo()
  {
    return terCorreo;
  }

  public TerTercero setTerCorreo(String terCorreo)
  {
    this.terCorreo = terCorreo;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public TerTercero setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getCiudadCod()
  {
    return ciudadCod;
  }

  public TerTercero setCiudadCod(String ciudadCod)
  {
    this.ciudadCod = ciudadCod;
    return this;
  }

  public Date getTerDocexpedicion()
  {
    return terDocexpedicion;
  }

  public TerTercero setTerDocexpedicion(Date terDocexpedicion)
  {
    this.terDocexpedicion = terDocexpedicion;
    return this;
  }

  public Integer getUniTipidentifica()
  {
    return uniTipidentifica;
  }

  public TerTercero setUniTipidentifica(Integer uniTipidentifica)
  {
    this.uniTipidentifica = uniTipidentifica;
    return this;
  }

  public Date getTerFecnacimiento()
  {
    return terFecnacimiento;
  }

  public TerTercero setTerFecnacimiento(Date terFecnacimiento)
  {
    this.terFecnacimiento = terFecnacimiento;
    return this;
  }

  // </editor-fold>
  @Override
  public TerTercero validar()
          throws AplicacionExcepcion
  {
    ValidarEntidad.construir(this)
            .agregar("terDocumento",
                    "requerido",
                    "El documento del tercero es obligatoria")
            .agregar("terCorreo", "requerido|correo", "El correo es obligatorio")
            .agregar("terNomcompleto", "requerido", "El nombre es obligatorio")
            .agregar("estTiptercero.estIderegistro",
                    "requerido",
                    "La estructura del tercero es obligatoria")
            .agregar("uniTiptercero.uniIderegistro",
                    "requerido",
                    "Debe seleccionar un tipo de tercero")
            .validar();
    return this;
  }

}
