package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Sedes extends Entidad implements Serializable {

  private String sedeCod;
  private String sedeNom;
  private String sedeDir;
  private String sedeTel;
  private String sedeCiu;
  private String sedeTip;
  private String sedeCodemp;

  public Sedes()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public String getSedeCod()
  {
    return sedeCod;
  }

  public Sedes setSedeCod(String sedeCod)
  {
    this.sedeCod = sedeCod;
    return this;
  }

  public String getSedeNom()
  {
    return sedeNom;
  }

  public Sedes setSedeNom(String sedeNom)
  {
    this.sedeNom = sedeNom;
    return this;
  }

  public String getSedeDir()
  {
    return sedeDir;
  }

  public Sedes setSedeDir(String sedeDir)
  {
    this.sedeDir = sedeDir;
    return this;
  }

  public String getSedeTel()
  {
    return sedeTel;
  }

  public Sedes setSedeTel(String sedeTel)
  {
    this.sedeTel = sedeTel;
    return this;
  }

  public String getSedeCiu()
  {
    return sedeCiu;
  }

  public Sedes setSedeCiu(String sedeCiu)
  {
    this.sedeCiu = sedeCiu;
    return this;
  }

  public String getSedeTip()
  {
    return sedeTip;
  }

  public Sedes setSedeTip(String sedeTip)
  {
    this.sedeTip = sedeTip;
    return this;
  }

  public String getSedeCodemp()
  {
    return sedeCodemp;
  }

  public Sedes setSedeCodemp(String sedeCodemp)
  {
    this.sedeCodemp = sedeCodemp;
    return this;
  }

  // </editor-fold>
  @Override
  public Sedes validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
