/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author jcpacheco
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrmrVarmicroruta extends Entidad implements Serializable{
    private Integer vrmrIderegistro;
    private RutRuta rutIdemicroruta;
    private PerPeriodo perIderegistro;
    private ConConcepto conIderegistro;
    private ArprAreaprestacion arprIderegistro;
    private Double vrmrValor;
    private String vrmrDescripcion;
    private String vrmrEstado;
    private Integer usuIderegistrogb;
    private Date vrmrFeccerficicacion;
    private Date vrmrFecgrabacion;
    private Integer usuIderegistrocer;
    private Integer empIderegistro;
    private String vrmrEstadoregistro;

    public Integer getVrmrIderegistro() {
        return vrmrIderegistro;
    }

    public void setVrmrIderegistro(Integer vrmrIderegistro) {
        this.vrmrIderegistro = vrmrIderegistro;
    }

    public RutRuta getRutIdemicroruta() {
        return rutIdemicroruta;
    }

    public void setRutIdemicroruta(RutRuta rutIdemicroruta) {
        this.rutIdemicroruta = rutIdemicroruta;
    }

    public PerPeriodo getPerIderegistro() {
        return perIderegistro;
    }

    public void setPerIderegistro(PerPeriodo perIderegistro) {
        this.perIderegistro = perIderegistro;
    }

    public ConConcepto getConIderegistro() {
        return conIderegistro;
    }

    public void setConIderegistro(ConConcepto conIderegistro) {
        this.conIderegistro = conIderegistro;
    }

    public ArprAreaprestacion getArprIderegistro() {
        return arprIderegistro;
    }

    public void setArprIderegistro(ArprAreaprestacion arprIderegistro) {
        this.arprIderegistro = arprIderegistro;
    }

    public Double getVrmrValor() {
        return vrmrValor;
    }

    public void setVrmrValor(Double vrmrValor) {
        this.vrmrValor = vrmrValor;
    }

    public String getVrmrDescripcion() {
        return vrmrDescripcion;
    }

    public void setVrmrDescripcion(String vrmrDescripcion) {
        this.vrmrDescripcion = vrmrDescripcion;
    }

    public String getVrmrEstado() {
        return vrmrEstado;
    }

    public void setVrmrEstado(String vrmrEstado) {
        this.vrmrEstado = vrmrEstado;
    }

    public Integer getUsuIderegistrogb() {
        return usuIderegistrogb;
    }

    public void setUsuIderegistrogb(Integer usuIderegistrogb) {
        this.usuIderegistrogb = usuIderegistrogb;
    }

    public Date getVrmrFeccerficicacion() {
        return vrmrFeccerficicacion;
    }

    public void setVrmrFeccerficicacion(Date vrmrFeccerficicacion) {
        this.vrmrFeccerficicacion = vrmrFeccerficicacion;
    }

    public Date getVrmrFecgrabacion() {
        return vrmrFecgrabacion;
    }

    public void setVrmrFecgrabacion(Date vrmrFecgrabacion) {
        this.vrmrFecgrabacion = vrmrFecgrabacion;
    }

    public Integer getUsuIderegistrocer() {
        return usuIderegistrocer;
    }

    public void setUsuIderegistrocer(Integer usuIderegistrocer) {
        this.usuIderegistrocer = usuIderegistrocer;
    }

    public Integer getEmpIderegistro() {
        return empIderegistro;
    }

    public void setEmpIderegistro(Integer empIderegistro) {
        this.empIderegistro = empIderegistro;
    }

    public String getVrmrEstadoregistro() {
        return vrmrEstadoregistro;
    }

    public void setVrmrEstadoregistro(String vrmrEstadoregistro) {
        this.vrmrEstadoregistro = vrmrEstadoregistro;
    }

    
    @Override
    public <T extends Entidad> T validar() throws AplicacionExcepcion {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
