package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FunFuncion extends Entidad implements Serializable
{

  private String funNombre;
  private String funDescripcion;
  private String funUbicacion;
  private String funTipo;
  private Integer funIderegistro;
  private Integer funParametro;
  private Integer usuIderegistro;
  private String funSql;
  private String funClase;

  public FunFuncion()
  {
  }

  public FunFuncion(Integer funIderegistro)
  {
    this.funIderegistro = funIderegistro;
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public String getFunNombre()
  {
    return funNombre;
  }

  public FunFuncion setFunNombre(String funNombre)
  {
    this.funNombre = funNombre;
    return this;
  }

  public String getFunDescripcion()
  {
    return funDescripcion;
  }

  public FunFuncion setFunDescripcion(String funDescripcion)
  {
    this.funDescripcion = funDescripcion;
    return this;
  }

  public String getFunUbicacion()
  {
    return funUbicacion;
  }

  public FunFuncion setFunUbicacion(String funUbicacion)
  {
    this.funUbicacion = funUbicacion;
    return this;
  }

  public String getFunTipo()
  {
    return funTipo;
  }

  public FunFuncion setFunTipo(String funTipo)
  {
    this.funTipo = funTipo;
    return this;
  }

  public Integer getFunIderegistro()
  {
    return funIderegistro;
  }

  public FunFuncion setFunIderegistro(Integer funIderegistro)
  {
    this.funIderegistro = funIderegistro;
    return this;
  }

  public Integer getFunParametro()
  {
    return funParametro;
  }

  public FunFuncion setFunParametro(Integer funParametro)
  {
    this.funParametro = funParametro;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public FunFuncion setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getFunSql()
  {
    return funSql;
  }

  public FunFuncion setFunSql(String funSql)
  {
    this.funSql = funSql;
    return this;
  }

  public String getFunClase()
  {
    return funClase;
  }

  public FunFuncion setFunClase(String funClase)
  {
    this.funClase = funClase;
    return this;
  }

  // </editor-fold>
  @Override
  public FunFuncion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
