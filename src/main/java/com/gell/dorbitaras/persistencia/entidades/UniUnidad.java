package com.gell.dorbitaras.persistencia.entidades;

import com.gell.estandar.persistencia.abstracto.Entidad;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.util.Map;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UniUnidad extends Entidad implements Serializable {

  private Integer uniIderegistro;
  private EstEstructura estIderegistro;
  private String uniCodigo1;
  private String uniCodigo2;
  private String uniCodigo3;
  private String uniCodigo4;
  private String uniCodigo5;
  private String uniNombre1;
  private String uniNombre2;
  private String uniNombre3;
  private String uniNombre4;
  private String uniNombre5;
  private Double uniOrden;
  private Integer uniNivel;
  private String uniCodigo;
  private UniUnidad uniIdepadre;
  private Integer usuIderegistro;
  private String uniPropiedad;
  private Map<String, String> listaPropiedades;

  public UniUnidad()
  {
  }

  public UniUnidad(Integer uniIderegistro)
  {
    this.uniIderegistro = uniIderegistro;
  }

  public Integer getUniIderegistro()
  {
    return uniIderegistro;
  }

  public UniUnidad setUniIderegistro(Integer uniIderegistro)
  {
    this.uniIderegistro = uniIderegistro;
    return this;
  }

  public EstEstructura getEstIderegistro()
  {
    return estIderegistro;
  }

  public UniUnidad setEstIderegistro(EstEstructura estIderegistro)
  {
    this.estIderegistro = estIderegistro;
    return this;
  }

  public UniUnidad setEstIderegistroId(Integer estIderegistro)
  {
    this.estIderegistro = new EstEstructura().setEstIderegistro(estIderegistro);
    return this;
  }

  public String getUniCodigo1()
  {
    return uniCodigo1;
  }

  public UniUnidad setUniCodigo1(String uniCodigo1)
  {
    this.uniCodigo1 = uniCodigo1;
    return this;
  }

  public String getUniCodigo2()
  {
    return uniCodigo2;
  }

  public UniUnidad setUniCodigo2(String uniCodigo2)
  {
    this.uniCodigo2 = uniCodigo2;
    return this;
  }

  public String getUniCodigo3()
  {
    return uniCodigo3;
  }

  public UniUnidad setUniCodigo3(String uniCodigo3)
  {
    this.uniCodigo3 = uniCodigo3;
    return this;
  }

  public String getUniCodigo4()
  {
    return uniCodigo4;
  }

  public UniUnidad setUniCodigo4(String uniCodigo4)
  {
    this.uniCodigo4 = uniCodigo4;
    return this;
  }

  public String getUniCodigo5()
  {
    return uniCodigo5;
  }

  public UniUnidad setUniCodigo5(String uniCodigo5)
  {
    this.uniCodigo5 = uniCodigo5;
    return this;
  }

  public String getUniNombre1()
  {
    return uniNombre1;
  }

  public UniUnidad setUniNombre1(String uniNombre1)
  {
    this.uniNombre1 = uniNombre1;
    return this;
  }

  public String getUniNombre2()
  {
    return uniNombre2;
  }

  public UniUnidad setUniNombre2(String uniNombre2)
  {
    this.uniNombre2 = uniNombre2;
    return this;
  }

  public String getUniNombre3()
  {
    return uniNombre3;
  }

  public UniUnidad setUniNombre3(String uniNombre3)
  {
    this.uniNombre3 = uniNombre3;
    return this;
  }

  public String getUniNombre4()
  {
    return uniNombre4;
  }

  public UniUnidad setUniNombre4(String uniNombre4)
  {
    this.uniNombre4 = uniNombre4;
    return this;
  }

  public String getUniNombre5()
  {
    return uniNombre5;
  }

  public UniUnidad setUniNombre5(String uniNombre5)
  {
    this.uniNombre5 = uniNombre5;
    return this;
  }

  public Double getUniOrden()
  {
    return uniOrden;
  }

  public UniUnidad setUniOrden(Double uniOrden)
  {
    this.uniOrden = uniOrden;
    return this;
  }

  public Integer getUniNivel()
  {
    return uniNivel;
  }

  public UniUnidad setUniNivel(Integer uniNivel)
  {
    this.uniNivel = uniNivel;
    return this;
  }

  public String getUniCodigo()
  {
    return uniCodigo;
  }

  public UniUnidad setUniCodigo(String uniCodigo)
  {
    this.uniCodigo = uniCodigo;
    return this;
  }

  public UniUnidad getUniIdepadre()
  {
    return uniIdepadre;
  }

  public UniUnidad setUniIdepadre(UniUnidad uniIdepadre)
  {
    this.uniIdepadre = uniIdepadre;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public UniUnidad setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getUniPropiedad()
  {
    return uniPropiedad;
  }

  public UniUnidad setUniPropiedad(String uniPropiedad)
  {
    this.uniPropiedad = uniPropiedad;
    return this;
  }

  public Map<String, String> getListaPropiedades()
  {
    return listaPropiedades;
  }

  public UniUnidad setListaPropiedades(Map<String, String> listaPropiedades)
  {
    this.listaPropiedades = listaPropiedades;
    return this;
  }

  @Override
  public UniUnidad validar()
          throws NegocioExcepcion, AplicacionExcepcion
  {
    return this;

  }

  public UniUnidad validarTipoConfiguracion()
          throws NegocioExcepcion, AplicacionExcepcion
  {
    ValidarEntidad.construir(this).agregar("estIderegistro.estIderegistro",
            "requerido|numero", "Debe ingresar el id estructurado")
            .agregar("uniNombre1", "requerido", "Debe ingresar el nombre")
            .agregar("uniPropiedad", "requerido", "Debe ingresar la propiedad")
            .validar();
    return this;

  }

}
