package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.dorbitaras.negocio.constante.EParametro;
import com.gell.estandar.persistencia.abstracto.Entidad;
import java.time.LocalTime;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class HrrHorrecoleccion extends Entidad implements Serializable
{

  private Integer hrrIderegistro;
  private String hrrDia;
  private RureRutrecoleccion rureIderegistro;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EParametro.HORA, locale = EParametro.PAIS, timezone = EParametro.UBICACION)
  private LocalTime hrrHorinicio;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = EParametro.HORA, locale = EParametro.PAIS, timezone = EParametro.UBICACION)
  private LocalTime hrrHorfin;
  private String hrrSwtact;

  public HrrHorrecoleccion()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getHrrIderegistro()
  {
    return hrrIderegistro;
  }

  public HrrHorrecoleccion setHrrIderegistro(Integer hrrIderegistro)
  {
    this.hrrIderegistro = hrrIderegistro;
    return this;
  }

  public String getHrrDia()
  {
    return hrrDia;
  }

  public HrrHorrecoleccion setHrrDia(String hrrDia)
  {
    this.hrrDia = hrrDia;
    return this;
  }

  public RureRutrecoleccion getRureIderegistro()
  {
    return rureIderegistro;
  }

  public HrrHorrecoleccion setRureIderegistro(RureRutrecoleccion rureIderegistro)
  {
    this.rureIderegistro = rureIderegistro;
    return this;
  }

  public LocalTime getHrrHorinicio()
  {
    return hrrHorinicio;
  }

  public HrrHorrecoleccion setHrrHorinicio(LocalTime hrrHorinicio)
  {
    this.hrrHorinicio = hrrHorinicio;
    return this;
  }

  public LocalTime getHrrHorfin()
  {
    return hrrHorfin;
  }

  public HrrHorrecoleccion setHrrHorfin(LocalTime hrrHorfin)
  {
    this.hrrHorfin = hrrHorfin;
    return this;
  }

  public String getHrrSwtact()
  {
    return hrrSwtact;
  }

  public HrrHorrecoleccion setHrrSwtact(String hrrSwtact)
  {
    this.hrrSwtact = hrrSwtact;
    return this;
  }

  // </editor-fold>
  @Override
  public HrrHorrecoleccion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
