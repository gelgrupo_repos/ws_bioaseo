package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Proyectos extends Entidad implements Serializable {

  private String proyectoCod;
  private String proyectoNom;
  private String proyectoCodciu;
  private Empresas proyectoCodemp;
  private String proyectoLlacom;
  private Integer proyectoIderegistro;
  private Integer departamentoIderegistro;
  private Integer cueIderegistro;

  public Proyectos()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public String getProyectoCod()
  {
    return proyectoCod;
  }

  public Proyectos setProyectoCod(String proyectoCod)
  {
    this.proyectoCod = proyectoCod;
    return this;
  }

  public String getProyectoNom()
  {
    return proyectoNom;
  }

  public Proyectos setProyectoNom(String proyectoNom)
  {
    this.proyectoNom = proyectoNom;
    return this;
  }

  public String getProyectoCodciu()
  {
    return proyectoCodciu;
  }

  public Proyectos setProyectoCodciu(String proyectoCodciu)
  {
    this.proyectoCodciu = proyectoCodciu;
    return this;
  }

  public Empresas getProyectoCodemp()
  {
    return proyectoCodemp;
  }

  public Proyectos setProyectoCodemp(Empresas proyectoCodemp)
  {
    this.proyectoCodemp = proyectoCodemp;
    return this;
  }

  public String getProyectoLlacom()
  {
    return proyectoLlacom;
  }

  public Proyectos setProyectoLlacom(String proyectoLlacom)
  {
    this.proyectoLlacom = proyectoLlacom;
    return this;
  }

  public Integer getProyectoIderegistro()
  {
    return proyectoIderegistro;
  }

  public Proyectos setProyectoIderegistro(Integer proyectoIderegistro)
  {
    this.proyectoIderegistro = proyectoIderegistro;
    return this;
  }

  public Integer getDepartamentoIderegistro()
  {
    return departamentoIderegistro;
  }

  public Proyectos setDepartamentoIderegistro(Integer departamentoIderegistro)
  {
    this.departamentoIderegistro = departamentoIderegistro;
    return this;
  }

  public Integer getCueIderegistro()
  {
    return cueIderegistro;
  }

  public Proyectos setCueIderegistro(Integer cueIderegistro)
  {
    this.cueIderegistro = cueIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public Proyectos validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
