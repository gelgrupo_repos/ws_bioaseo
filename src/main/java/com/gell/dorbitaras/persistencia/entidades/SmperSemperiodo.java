package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmperSemperiodo extends Entidad implements Serializable
{

  private Integer smperIderegistro;
  private RgtaRgitarifario rgtaIderegistro;
  private PerPeriodo perIdepadre;
  private PerPeriodo perIderegistro;
  private String smperDescripcion;
  private Integer smperNumero;
  private String smperSwtact;
  private Integer usuIderegistroGb;
  private Integer usuIderegistroAct;
  private Date smperFecgrabacion;
  private Date smperFecact;
  /**
   * I=Inserta, M=Modifica, E = elimina, N = No Aplica
   */
  private String accion;

  public SmperSemperiodo()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getSmperIderegistro()
  {
    return smperIderegistro;
  }

  public SmperSemperiodo setSmperIderegistro(Integer smperIderegistro)
  {
    this.smperIderegistro = smperIderegistro;
    return this;
  }

  public RgtaRgitarifario getRgtaIderegistro()
  {
    return rgtaIderegistro;
  }

  public SmperSemperiodo setRgtaIderegistro(RgtaRgitarifario rgtaIderegistro)
  {
    this.rgtaIderegistro = rgtaIderegistro;
    return this;
  }

  public PerPeriodo getPerIdepadre()
  {
    return perIdepadre;
  }

  public SmperSemperiodo setPerIdepadre(PerPeriodo perIdepadre)
  {
    this.perIdepadre = perIdepadre;
    return this;
  }

  public PerPeriodo getPerIderegistro()
  {
    return perIderegistro;
  }

  public SmperSemperiodo setPerIderegistro(PerPeriodo perIderegistro)
  {
    this.perIderegistro = perIderegistro;
    return this;
  }

  public String getSmperDescripcion()
  {
    return smperDescripcion;
  }

  public SmperSemperiodo setSmperDescripcion(String smperDescripcion)
  {
    this.smperDescripcion = smperDescripcion;
    return this;
  }

  public Integer getSmperNumero()
  {
    return smperNumero;
  }

  public SmperSemperiodo setSmperNumero(Integer smperNumero)
  {
    this.smperNumero = smperNumero;
    return this;
  }

  public String getSmperSwtact()
  {
    return smperSwtact;
  }

  public SmperSemperiodo setSmperSwtact(String smperSwtact)
  {
    this.smperSwtact = smperSwtact;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public SmperSemperiodo setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Integer getUsuIderegistroAct()
  {
    return usuIderegistroAct;
  }

  public SmperSemperiodo setUsuIderegistroAct(Integer usuIderegistroAct)
  {
    this.usuIderegistroAct = usuIderegistroAct;
    return this;
  }

  public Date getSmperFecgrabacion()
  {
    return smperFecgrabacion;
  }

  public SmperSemperiodo setSmperFecgrabacion(Date smperFecgrabacion)
  {
    this.smperFecgrabacion = smperFecgrabacion;
    return this;
  }

  public Date getSmperFecact()
  {
    return smperFecact;
  }

  public SmperSemperiodo setSmperFecact(Date smperFecact)
  {
    this.smperFecact = smperFecact;
    return this;
  }

  public String getAccion()
  {
    return accion;
  }

  public SmperSemperiodo setAccion(String accion)
  {
    this.accion = accion;
    return this;
  }

  // </editor-fold>
  @Override
  public SmperSemperiodo validar()
          throws AplicacionExcepcion
  {

    ValidarEntidad.construir(this)
            .agregar("rgtaIderegistro.rgtaIderegistro", "requerido",
                    "El Régimen es obligatorio")
            .agregar("perIdepadre", "requerido",
                    "El Semestre es obligatorio")
            .agregar("perIderegistro", "requerido",
                    "El mes es obligatorio")
            .validar();
    return this;
  }
}
