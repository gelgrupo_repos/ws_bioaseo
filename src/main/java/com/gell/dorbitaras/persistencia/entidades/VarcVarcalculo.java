package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VarcVarcalculo extends Entidad implements Serializable {

    private Integer varcIderegistro;
    private PerPeriodo perIderegistro;
    private ConConcepto conIderegistro;
    private ArprAreaprestacion arprIderegistro;
    private Integer usuIderegistroGb;
    private Date varcFeccerficicacion;
    private Date varcFecgrabacion;
    private Integer usuIderegistroCer;
    private Double varcValor;
    private String varcDescripcion;
    private String varcEstado;
    private Integer empIderegistro;
    private String varcEstadoregistro;
    private RacoRanconcept racoIderegistro;
    private String varcEstadoVariacion;
    private Integer varcIdMes;
    private Integer varcIdConceptoAplicado;

    public VarcVarcalculo() {
    }

    // <editor-fold defaultstate="collapsed" desc="GET-SET">
    public Integer getVarcIderegistro() {
        return varcIderegistro;
    }

    public VarcVarcalculo setVarcIderegistro(Integer varcIderegistro) {
        this.varcIderegistro = varcIderegistro;
        return this;
    }

    public PerPeriodo getPerIderegistro() {
        return perIderegistro;
    }

    public void setPerIderegistro(PerPeriodo perIderegistro) {
        this.perIderegistro = perIderegistro;
    }

    public ConConcepto getConIderegistro() {
        return conIderegistro;
    }

    public VarcVarcalculo setConIderegistro(ConConcepto conIderegistro) {
        this.conIderegistro = conIderegistro;
        return this;
    }

    public ArprAreaprestacion getArprIderegistro() {
        return arprIderegistro;
    }

    public VarcVarcalculo setArprIderegistro(ArprAreaprestacion arprIderegistro) {
        this.arprIderegistro = arprIderegistro;
        return this;
    }

    public Integer getUsuIderegistroGb() {
        return usuIderegistroGb;
    }

    public VarcVarcalculo setUsuIderegistroGb(Integer usuIderegistroGb) {
        this.usuIderegistroGb = usuIderegistroGb;
        return this;
    }

    public Date getVarcFeccerficicacion() {
        return varcFeccerficicacion;
    }

    public VarcVarcalculo setVarcFeccerficicacion(Date varcFeccerficicacion) {
        this.varcFeccerficicacion = varcFeccerficicacion;
        return this;
    }

    public Date getVarcFecgrabacion() {
        return varcFecgrabacion;
    }

    public VarcVarcalculo setVarcFecgrabacion(Date varcFecgrabacion) {
        this.varcFecgrabacion = varcFecgrabacion;
        return this;
    }

    public Integer getUsuIderegistroCer() {
        return usuIderegistroCer;
    }

    public VarcVarcalculo setUsuIderegistroCer(Integer usuIderegistroCer) {
        this.usuIderegistroCer = usuIderegistroCer;
        return this;
    }

    public Double getVarcValor() {
        return varcValor;
    }

    public VarcVarcalculo setVarcValor(Double varcValor) {
        this.varcValor = varcValor;
        return this;
    }

    public String getVarcDescripcion() {
        return varcDescripcion;
    }

    public VarcVarcalculo setVarcDescripcion(String varcDescripcion) {
        this.varcDescripcion = varcDescripcion;
        return this;
    }

    public String getVarcEstado() {
        return varcEstado;
    }

    public VarcVarcalculo setVarcEstado(String varcEstado) {
        this.varcEstado = varcEstado;
        return this;
    }

    public Integer getEmpIderegistro() {
        return empIderegistro;
    }

    public VarcVarcalculo setEmpIderegistro(Integer empIderegistro) {
        this.empIderegistro = empIderegistro;
        return this;
    }

    public String getVarcEstadoregistro() {
        return varcEstadoregistro;
    }

    public VarcVarcalculo setVarcEstadoregistro(String varcEstadoregistro) {
        this.varcEstadoregistro = varcEstadoregistro;
        return this;
    }

    public RacoRanconcept getRacoIderegistro() {
        return racoIderegistro;
    }

    public VarcVarcalculo setRacoIderegistro(RacoRanconcept racoIderegistro) {
        this.racoIderegistro = racoIderegistro;
        return this;
    }

    public String getVarcEstadoVariacion() {
        return varcEstadoVariacion;
    }

    public void setVarcEstadoVariacion(String varcEstadoVariacion) {
        this.varcEstadoVariacion = varcEstadoVariacion;
    }

    public Integer getVarcIdMes() {
        return varcIdMes;
    }

    public void setVarcIdMes(Integer varcIdMes) {
        this.varcIdMes = varcIdMes;
    }
    
    public Integer getVarcIdConceptoAplicado() {
        return varcIdConceptoAplicado;
    }

    public void setVarcIdConceptoAplicado(Integer varcIdConceptoAplicado) {
        this.varcIdConceptoAplicado = varcIdConceptoAplicado;
    }
    

    // </editor-fold>
    @Override
    public VarcVarcalculo validar()
            throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("perIderegistro.perIderegistro", "requerido",
                        "El campo periodo es obligatorio")
                .agregar("conIderegistro.uniConcepto.uniIderegistro", "requerido",
                        "El campo concepto es obligatorio")
                .agregar("arprIderegistro.arprIderegistro", "requerido",
                        "El campo area de prestación es obligatorio")
                .agregar("varcValor", "requerido",
                        "El campo valor es obligatorio")
                .agregar("varcEstado", "requerido",
                        "El campo estado es obligatorio")
                .validar();
        return this;
    }
}
