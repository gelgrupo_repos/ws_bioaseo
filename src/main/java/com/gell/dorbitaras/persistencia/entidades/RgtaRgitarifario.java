package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.util.List;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RgtaRgitarifario extends Entidad implements Serializable {

  private Integer rgtaIderegistro;
  private String rgtaNombre;
  private String rgtaDescripcion;
  private Date rgtaFecinivigencia;
  private Date rgtaFecfinvigencia;
  private Integer empIderegistro;
  private DctaDctotarifas dctaDctotarifas;
  private List<DctaDctotarifas> listaValores;

  public RgtaRgitarifario()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getRgtaIderegistro()
  {
    return rgtaIderegistro;
  }

  public RgtaRgitarifario setRgtaIderegistro(Integer rgtaIderegistro)
  {
    this.rgtaIderegistro = rgtaIderegistro;
    return this;
  }

  public String getRgtaNombre()
  {
    return rgtaNombre;
  }

  public RgtaRgitarifario setRgtaNombre(String rgtaNombre)
  {
    this.rgtaNombre = rgtaNombre;
    return this;
  }

  public String getRgtaDescripcion()
  {
    return rgtaDescripcion;
  }

  public RgtaRgitarifario setRgtaDescripcion(String rgtaDescripcion)
  {
    this.rgtaDescripcion = rgtaDescripcion;
    return this;
  }

  public Date getRgtaFecinivigencia()
  {
    return rgtaFecinivigencia;
  }

  public RgtaRgitarifario setRgtaFecinivigencia(Date rgtaFecinivigencia)
  {
    this.rgtaFecinivigencia = rgtaFecinivigencia;
    return this;
  }

  public Date getRgtaFecfinvigencia()
  {
    return rgtaFecfinvigencia;
  }

  public RgtaRgitarifario setRgtaFecfinvigencia(Date rgtaFecfinvigencia)
  {
    this.rgtaFecfinvigencia = rgtaFecfinvigencia;
    return this;
  }

  public Integer getEmpIderegistro()
  {
    return empIderegistro;
  }

  public RgtaRgitarifario setEmpIderegistro(Integer empIderegistro)
  {
    this.empIderegistro = empIderegistro;
    return this;
  }

  public DctaDctotarifas getDctaDctotarifas()
  {
    return dctaDctotarifas;
  }

  public RgtaRgitarifario setDctaDctotarifas(DctaDctotarifas dctaDctotarifas)
  {
    this.dctaDctotarifas = dctaDctotarifas;
    return this;
  }

  public List<DctaDctotarifas> getListaValores()
  {
    return listaValores;
  }

  public void setListaValores(List<DctaDctotarifas> listaValores)
  {
    this.listaValores = listaValores;
  }

  // </editor-fold>
  @Override
  public RgtaRgitarifario validar()
          throws AplicacionExcepcion
  {

    ValidarEntidad.construir(this)
            .agregar("rgtaNombre", "requerido",
                    "El campo Nombre no puede estar vacio")
            .agregar("rgtaFecinivigencia", "requerido",
                    "La Fecha de inicio no puede estar vacia")
            .agregar("rgtaFecfinvigencia", "requerido",
                    "La Fecha final no puede estar vacia")
            .validar();
    return this;
  }
}
