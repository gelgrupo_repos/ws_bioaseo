package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CvlCtrverliquidacion extends Entidad implements Serializable
{

  private Long cvlIderegistro;
  private Long hliqIderegistr;
  private Integer uniLiquidacion;
  private Date cvlFecha;
  private Integer usuIderegistro;

  public CvlCtrverliquidacion()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Long getCvlIderegistro()
  {
    return cvlIderegistro;
  }

  public CvlCtrverliquidacion setCvlIderegistro(Long cvlIderegistro)
  {
    this.cvlIderegistro = cvlIderegistro;
    return this;
  }

  public Long getHliqIderegistr()
  {
    return hliqIderegistr;
  }

  public CvlCtrverliquidacion setHliqIderegistr(Long hliqIderegistr)
  {
    this.hliqIderegistr = hliqIderegistr;
    return this;
  }

  public Integer getUniLiquidacion()
  {
    return uniLiquidacion;
  }

  public CvlCtrverliquidacion setUniLiquidacion(Integer uniLiquidacion)
  {
    this.uniLiquidacion = uniLiquidacion;
    return this;
  }

  public Date getCvlFecha()
  {
    return cvlFecha;
  }

  public CvlCtrverliquidacion setCvlFecha(Date cvlFecha)
  {
    this.cvlFecha = cvlFecha;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public CvlCtrverliquidacion setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public CvlCtrverliquidacion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
