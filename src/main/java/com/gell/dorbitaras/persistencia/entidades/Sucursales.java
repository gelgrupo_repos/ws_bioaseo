package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Sucursales  extends Entidad implements Serializable {

    private String sucursalCod;
private String sucursalNom;
private Ciudades sucursalCodciu;
private String sucursalCodemp;


    public Sucursales() {
    }

    // <editor-fold defaultstate="collapsed" desc="GET-SET">
   public String getSucursalCod(){
 return sucursalCod;
}
public Sucursales setSucursalCod(String sucursalCod){
 this.sucursalCod=sucursalCod;
return this;}
public String getSucursalNom(){
 return sucursalNom;
}
public Sucursales setSucursalNom(String sucursalNom){
 this.sucursalNom=sucursalNom;
return this;}
public Ciudades getSucursalCodciu(){
 return sucursalCodciu;
}
public Sucursales setSucursalCodciu(Ciudades sucursalCodciu){
 this.sucursalCodciu=sucursalCodciu;
return this;}
public String getSucursalCodemp(){
 return sucursalCodemp;
}
public Sucursales setSucursalCodemp(String sucursalCodemp){
 this.sucursalCodemp=sucursalCodemp;
return this;}

    // </editor-fold>

    @Override
    public Sucursales validar() throws AplicacionExcepcion {
    return this;
    }
}

