/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author jonat
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaVarteraprProm extends Entidad implements Serializable {
    private Integer vrtaPromIderegistro;
    private Integer perIdePadre;
    private Integer numeroActualizacion;
    private String tipo;
    private Long terIdeRegistro;
    private Boolean periodoExacto;
    private Boolean taEjecutado;
    private Timestamp vrtaFecGrabacion;
    private Integer usuIdeRegistro;
    private Double vrtaPromValor;
    private Integer vrtaQaHistIderegistro;
    
    public VrtaVarteraprProm() {
    }

    public VrtaVarteraprProm(Integer vrtaPromIderegistro, Integer perIdePadre, Integer numeroActualizacion, String tipo, Long terIdeRegistro, Boolean periodoExacto, Boolean taEjecutado, Timestamp vrtaFecGrabacion, Integer usuIdeRegistro, Double vrtaPromValor, Integer vrtaQaHistIderegistro) {
        this.vrtaPromIderegistro = vrtaPromIderegistro;
        this.perIdePadre = perIdePadre;
        this.numeroActualizacion = numeroActualizacion;
        this.tipo = tipo;
        this.terIdeRegistro = terIdeRegistro;
        this.periodoExacto = periodoExacto;
        this.taEjecutado = taEjecutado;
        this.vrtaFecGrabacion = vrtaFecGrabacion;
        this.usuIdeRegistro = usuIdeRegistro;
        this.vrtaPromValor = vrtaPromValor;
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }
    
    

    public VrtaVarteraprProm(Integer perIdePadre, Integer numeroActualizacion, String tipo, Long terIdeRegistro, Boolean periodoExacto, Boolean taEjecutado, Timestamp vrtaFecGrabacion, Integer usuIdeRegistro, Double vrtaPromValor, Integer vrtaQaHistIderegistro) {
        this.perIdePadre = perIdePadre;
        this.numeroActualizacion = numeroActualizacion;
        this.tipo = tipo;
        this.terIdeRegistro = terIdeRegistro;
        this.periodoExacto = periodoExacto;
        this.taEjecutado = taEjecutado;
        this.vrtaFecGrabacion = vrtaFecGrabacion;
        this.usuIdeRegistro = usuIdeRegistro;
        this.vrtaPromValor = vrtaPromValor;
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }

    public Integer getVrtaPromIderegistro() {
        return vrtaPromIderegistro;
    }

    public void setVrtaPromIderegistro(Integer vrtaPromIderegistro) {
        this.vrtaPromIderegistro = vrtaPromIderegistro;
    }

    public Integer getPerIdePadre() {
        return perIdePadre;
    }

    public void setPerIdePadre(Integer perIdePadre) {
        this.perIdePadre = perIdePadre;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setNumeroActualizacion(String tipo) {
        this.tipo = tipo;
    }

    public Long getTerIdeRegistro() {
        return terIdeRegistro;
    }

    public void setTerIdeRegistro(Long terIdeRegistro) {
        this.terIdeRegistro = terIdeRegistro;
    }

    public Boolean getPeriodoExacto() {
        return periodoExacto;
    }

    public void setPeriodoExacto(Boolean periodoExacto) {
        this.periodoExacto = periodoExacto;
    }

    public Boolean getTaEjecutado() {
        return taEjecutado;
    }

    public void setTaEjecutado(Boolean taEjecutado) {
        this.taEjecutado = taEjecutado;
    }

    public Timestamp getVrtaFecGrabacion() {
        return vrtaFecGrabacion;
    }

    public void setVrtaFecGrabacion(Timestamp vrtaFecGrabacion) {
        this.vrtaFecGrabacion = vrtaFecGrabacion;
    }

    public Integer getUsuIdeRegistro() {
        return usuIdeRegistro;
    }

    public void setUsuIdeRegistro(Integer usuIdeRegistro) {
        this.usuIdeRegistro = usuIdeRegistro;
    }

    public Double getVrtaPromValor() {
        return vrtaPromValor;
    }

    public void setVrtaPromValor(Double vrtaPromValor) {
        this.vrtaPromValor = vrtaPromValor;
    }
    
    public Integer getVrtaQaHistIderegistro() {
        return vrtaQaHistIderegistro;
    }

    public void setVrtaQaHistIderegistro(Integer vrtaQaHistIderegistro) {
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }

    @Override
    public VrtaVarteraprProm validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("perIdePadre", "requerido",
                        "El campo perIdePadre es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("periodoExacto", "requerido",
                        "El campo periodoExacto fecha grabacion es obligatorio")
                .validar();
        return this;
    }

    public void setUsuIderegistro(Integer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
