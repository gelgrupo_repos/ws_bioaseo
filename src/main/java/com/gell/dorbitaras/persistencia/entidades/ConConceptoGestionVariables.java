package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.persistencia.entidades.PrgPrograma;
import java.util.List;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("OverridableMethodCallInConstructor")
public class ConConceptoGestionVariables extends Entidad implements Serializable {

    private UniUnidad uniConcepto;
    private Integer estConcepto;
    private String conNombre;
    private String conAlias;
    private String conAbreviatura;
    private String conTipcalculo;
    private Double conValor;
    private String conFormula;
    private String conOperacion;
    private String conNaturaleza;
    private String conPreliquidar;
    private String conAnticipo;
    private Integer conPagpriori;
    private String conFinanciable;
    private Date conInivigencia;
    private Date conFinvigencia;
    private String conEstado;
    private PrgPrograma prgIderegistro;
    private String conTipregistro;
    private String conCondonable;
    private String conValnulo;
    private Integer usuIderegistro;
    private Integer funIderegistro;
    private String conSuspende;
    private String conIntfinanciacion;
    private String conMetajuste;
    private Integer conPrecision;
    private String conContabiliza;
    private RacoRanconcept raco;
    private String listaRango;
    private List<CoreConrelacio> listaRelacionados;
    private String descripcion;

    public ConConceptoGestionVariables() {
    }

    public ConConceptoGestionVariables(Integer uniConcepto) {
        this.setUniConcepto(new UniUnidad(uniConcepto));
    }

    // <editor-fold defaultstate="collapsed" desc="GET-SET">
    public UniUnidad getUniConcepto() {
        return uniConcepto;
    }

    public ConConceptoGestionVariables setUniConcepto(UniUnidad uniConcepto) {
        this.uniConcepto = uniConcepto;
        return this;
    }

    public Integer getEstConcepto() {
        return estConcepto;
    }

    public ConConceptoGestionVariables setEstConcepto(Integer estConcepto) {
        this.estConcepto = estConcepto;
        return this;
    }

    public String getConNombre() {
        return conNombre;
    }

    public ConConceptoGestionVariables setConNombre(String conNombre) {
        this.conNombre = conNombre;
        return this;
    }

    public String getConAlias() {
        return conAlias;
    }

    public ConConceptoGestionVariables setConAlias(String conAlias) {
        this.conAlias = conAlias;
        return this;
    }

    public String getConAbreviatura() {
        return conAbreviatura;
    }

    public ConConceptoGestionVariables setConAbreviatura(String conAbreviatura) {
        this.conAbreviatura = conAbreviatura;
        return this;
    }

    public String getConTipcalculo() {
        return conTipcalculo;
    }

    public ConConceptoGestionVariables setConTipcalculo(String conTipcalculo) {
        this.conTipcalculo = conTipcalculo;
        return this;
    }

    public Double getConValor() {
        return conValor;
    }

    public ConConceptoGestionVariables setConValor(Double conValor) {
        this.conValor = conValor;
        return this;
    }

    public String getConFormula() {
        return conFormula;
    }

    public ConConceptoGestionVariables setConFormula(String conFormula) {
        this.conFormula = conFormula;
        return this;
    }

    public String getConOperacion() {
        return conOperacion;
    }

    public ConConceptoGestionVariables setConOperacion(String conOperacion) {
        this.conOperacion = conOperacion;
        return this;
    }

    public String getConNaturaleza() {
        return conNaturaleza;
    }

    public ConConceptoGestionVariables setConNaturaleza(String conNaturaleza) {
        this.conNaturaleza = conNaturaleza;
        return this;
    }

    public String getConPreliquidar() {
        return conPreliquidar;
    }

    public ConConceptoGestionVariables setConPreliquidar(String conPreliquidar) {
        this.conPreliquidar = conPreliquidar;
        return this;
    }

    public String getConAnticipo() {
        return conAnticipo;
    }

    public ConConceptoGestionVariables setConAnticipo(String conAnticipo) {
        this.conAnticipo = conAnticipo;
        return this;
    }

    public Integer getConPagpriori() {
        return conPagpriori;
    }

    public ConConceptoGestionVariables setConPagpriori(Integer conPagpriori) {
        this.conPagpriori = conPagpriori;
        return this;
    }

    public String getConFinanciable() {
        return conFinanciable;
    }

    public ConConceptoGestionVariables setConFinanciable(String conFinanciable) {
        this.conFinanciable = conFinanciable;
        return this;
    }

    public Date getConInivigencia() {
        return conInivigencia;
    }

    public ConConceptoGestionVariables setConInivigencia(Date conInivigencia) {
        this.conInivigencia = conInivigencia;
        return this;
    }

    public Date getConFinvigencia() {
        return conFinvigencia;
    }

    public ConConceptoGestionVariables setConFinvigencia(Date conFinvigencia) {
        this.conFinvigencia = conFinvigencia;
        return this;
    }

    public String getConEstado() {
        return conEstado;
    }

    public ConConceptoGestionVariables setConEstado(String conEstado) {
        this.conEstado = conEstado;
        return this;
    }

    public PrgPrograma getPrgIderegistro() {
        return prgIderegistro;
    }

    public ConConceptoGestionVariables setPrgIderegistro(PrgPrograma prgIderegistro) {
        this.prgIderegistro = prgIderegistro;
        return this;
    }

    public String getConTipregistro() {
        return conTipregistro;
    }

    public ConConceptoGestionVariables setConTipregistro(String conTipregistro) {
        this.conTipregistro = conTipregistro;
        return this;
    }

    public String getConCondonable() {
        return conCondonable;
    }

    public ConConceptoGestionVariables setConCondonable(String conCondonable) {
        this.conCondonable = conCondonable;
        return this;
    }

    public String getConValnulo() {
        return conValnulo;
    }

    public ConConceptoGestionVariables setConValnulo(String conValnulo) {
        this.conValnulo = conValnulo;
        return this;
    }

    public Integer getUsuIderegistro() {
        return usuIderegistro;
    }

    public ConConceptoGestionVariables setUsuIderegistro(Integer usuIderegistro) {
        this.usuIderegistro = usuIderegistro;
        return this;
    }

    public Integer getFunIderegistro() {
        return funIderegistro;
    }

    public ConConceptoGestionVariables setFunIderegistro(Integer funIderegistro) {
        this.funIderegistro = funIderegistro;
        return this;
    }

    public String getConSuspende() {
        return conSuspende;
    }

    public ConConceptoGestionVariables setConSuspende(String conSuspende) {
        this.conSuspende = conSuspende;
        return this;
    }

    public String getConIntfinanciacion() {
        return conIntfinanciacion;
    }

    public ConConceptoGestionVariables setConIntfinanciacion(String conIntfinanciacion) {
        this.conIntfinanciacion = conIntfinanciacion;
        return this;
    }

    public String getConMetajuste() {
        return conMetajuste;
    }

    public ConConceptoGestionVariables setConMetajuste(String conMetajuste) {
        this.conMetajuste = conMetajuste;
        return this;
    }

    public Integer getConPrecision() {
        return conPrecision;
    }

    public ConConceptoGestionVariables setConPrecision(Integer conPrecision) {
        this.conPrecision = conPrecision;
        return this;
    }

    public String getConContabiliza() {
        return conContabiliza;
    }

    public ConConceptoGestionVariables setConContabiliza(String conContabiliza) {
        this.conContabiliza = conContabiliza;
        return this;
    }

    public RacoRanconcept getRaco() {
        return raco;
    }

    public ConConceptoGestionVariables setRaco(RacoRanconcept raco) {
        this.raco = raco;
        return this;
    }

    public String getListaRango() {
        return listaRango;
    }

    public ConConceptoGestionVariables setListaRango(String listaRango) {
        this.listaRango = listaRango;
        return this;
    }

    public List<CoreConrelacio> getListaRelacionados() {
        return listaRelacionados;
    }

    public ConConceptoGestionVariables setListaRelacionados(List<CoreConrelacio> listaRelacionados) {
        this.listaRelacionados = listaRelacionados;
        return this;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    // </editor-fold>
    @Override
    public ConConceptoGestionVariables validar()
            throws AplicacionExcepcion {
        return this;
    }
}
