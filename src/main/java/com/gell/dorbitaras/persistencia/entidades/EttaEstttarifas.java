package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EttaEstttarifas extends Entidad implements Serializable {

  private Integer ettaIderegistro;
  private ArprAreaprestacion arprIderegistro;
  private String ettaNombre;
  private String ettaSwtestado;
  private Integer usuIderegistroGb;
  private Integer usuIderegistroAct;
  private Date ettaFecgrabacion;
  private Date ettaFecact;

  public EttaEstttarifas()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getEttaIderegistro()
  {
    return ettaIderegistro;
  }

  public EttaEstttarifas setEttaIderegistro(Integer ettaIderegistro)
  {
    this.ettaIderegistro = ettaIderegistro;
    return this;
  }

  public ArprAreaprestacion getArprIderegistro()
  {
    return arprIderegistro;
  }

  public EttaEstttarifas setArprIderegistro(ArprAreaprestacion arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public String getEttaNombre()
  {
    return ettaNombre;
  }

  public EttaEstttarifas setEttaNombre(String ettaNombre)
  {
    this.ettaNombre = ettaNombre;
    return this;
  }

  public String getEttaSwtestado()
  {
    return ettaSwtestado;
  }

  public EttaEstttarifas setEttaSwtestado(String ettaSwtestado)
  {
    this.ettaSwtestado = ettaSwtestado;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public EttaEstttarifas setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Integer getUsuIderegistroAct()
  {
    return usuIderegistroAct;
  }

  public EttaEstttarifas setUsuIderegistroAct(Integer usuIderegistroAct)
  {
    this.usuIderegistroAct = usuIderegistroAct;
    return this;
  }

  public Date getEttaFecgrabacion()
  {
    return ettaFecgrabacion;
  }

  public EttaEstttarifas setEttaFecgrabacion(Date ettaFecgrabacion)
  {
    this.ettaFecgrabacion = ettaFecgrabacion;
    return this;
  }

  public Date getEttaFecact()
  {
    return ettaFecact;
  }

  public EttaEstttarifas setEttaFecact(Date ettaFecact)
  {
    this.ettaFecact = ettaFecact;
    return this;
  }

  // </editor-fold>
  @Override
  public EttaEstttarifas validar()
          throws AplicacionExcepcion
  {
    ValidarEntidad.construir(this)
            .agregar("ettaNombre", "requerido",
                    "El nombre del documento es obligatorio")
            .validar();
    return this;
  }
}
