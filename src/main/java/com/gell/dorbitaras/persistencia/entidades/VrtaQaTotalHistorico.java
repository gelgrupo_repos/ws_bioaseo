/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author jonat
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaQaTotalHistorico extends Entidad implements Serializable {
    
    private Integer vrtaqahistideregistro;
    private Integer numeroactualizacion;
    private String tipo;
    private Integer periodoejecucion;
    private Integer periodopadrecalculo;
    private Timestamp fecharegistro;
    private Integer usuarioregistro;
    private Double valorqatotal;
    private Boolean primerQa;

    public VrtaQaTotalHistorico() {
    }

    public VrtaQaTotalHistorico(Integer vrtaqahistideregistro, Integer numeroactualizacion, String tipo, Integer periodoejecucion, Integer periodopadrecalculo, Timestamp fecharegistro, Integer usuarioregistro, Double valorqatotal) {
        this.vrtaqahistideregistro = vrtaqahistideregistro;
        this.numeroactualizacion = numeroactualizacion;
        this.tipo = tipo;
        this.periodoejecucion = periodoejecucion;
        this.periodopadrecalculo = periodopadrecalculo;
        this.fecharegistro = fecharegistro;
        this.usuarioregistro = usuarioregistro;
        this.valorqatotal = valorqatotal;
    }

    public VrtaQaTotalHistorico(Integer numeroactualizacion, Integer periodoejecucion, String tipo, Integer periodopadrecalculo, Timestamp fecharegistro, Integer usuarioregistro, Double valorqatotal, Boolean primerQa) {
        this.numeroactualizacion = numeroactualizacion;
        this.tipo = tipo;
        this.periodoejecucion = periodoejecucion;
        this.periodopadrecalculo = periodopadrecalculo;
        this.fecharegistro = fecharegistro;
        this.usuarioregistro = usuarioregistro;
        this.valorqatotal = valorqatotal;
        this.primerQa = primerQa;
    }
    
    

    public Integer getVrtaqahistideregistro() {
        return vrtaqahistideregistro;
    }

    public void setVrtaqahistideregistro(Integer vrtatacalcideregistro) {
        this.vrtaqahistideregistro = vrtatacalcideregistro;
    }

    public Integer getNumeroactualizacion() {
        return numeroactualizacion;
    }

    public void setNumeroactualizacion(Integer numeroactualizacion) {
        this.numeroactualizacion = numeroactualizacion;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPeriodoejecucion() {
        return periodoejecucion;
    }

    public void setPeriodoejecucion(Integer periodoejecucion) {
        this.periodoejecucion = periodoejecucion;
    }

    public Integer getPeriodopadrecalculo() {
        return periodopadrecalculo;
    }

    public void setPeriodopadrecalculo(Integer periodopadrecalculo) {
        this.periodopadrecalculo = periodopadrecalculo;
    }

    public Timestamp getFecharegistro() {
        return fecharegistro;
    }

    public void setFecharegistro(Timestamp fecharegistro) {
        this.fecharegistro = fecharegistro;
    }

    public Integer getUsuarioregistro() {
        return usuarioregistro;
    }

    public void setUsuarioregistro(Integer usuarioregistro) {
        this.usuarioregistro = usuarioregistro;
    }

    public Double getValorqatotal() {
        return valorqatotal;
    }

    public void setValorqatotal(Double valorta) {
        this.valorqatotal = valorta;
    }
    
    public Boolean getPrimerQa() {
        return primerQa;
    }
    
    public void setPrimerQa(Boolean primerQa) {
        this.primerQa = primerQa;
    }
    
    @Override
    public VrtaQaTotalHistorico validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("vrtapromideregistro", "requerido",
                        "El campo vrtapromideregistro es obligatorio")
                .agregar("smperideregistro", "requerido",
                        "El campo smperideregistro es obligatorio")
                .agregar("numeroactualizacion", "requerido",
                        "El campo numeroactualizacion es obligatorio")
                .agregar("periodoexacto", "requerido",
                        "El campo periodoexacto fecha grabacion es obligatorio")
                .validar();
        return this;
    }
}
