/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author JavierRangel
 */
public class VrtaTerceroPorcenTa extends Entidad implements Serializable {

    private Integer idTerceroPorcentajeTa;
    private Integer numeroActualizacionTercero;
    private Integer numeroActualizacionTa;
    private Integer periodoRecalculo;
    private Long idTercero;
    private Timestamp fechaRegistro;
    private Integer usuarioRegistro;
    private Double valorPorcentaje;

    public VrtaTerceroPorcenTa() {
    }

    public VrtaTerceroPorcenTa(
            Integer numeroActualizacionTercero, Integer numeroActualizacionTa,
            Integer periodoRecalculo, Long idTercero, Timestamp fechaRegistro,
            Integer usuarioRegistro, Double valorPorcentaje) {
        this.numeroActualizacionTercero = numeroActualizacionTercero;
        this.numeroActualizacionTa = numeroActualizacionTa;
        this.periodoRecalculo = periodoRecalculo;
        this.idTercero = idTercero;
        this.fechaRegistro = fechaRegistro;
        this.usuarioRegistro = usuarioRegistro;
        this.valorPorcentaje = valorPorcentaje;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Integer getIdTerceroPorcentajeTa() {
        return idTerceroPorcentajeTa;
    }

    public void setIdTerceroPorcentajeTa(Integer idTerceroPorcentajeTa) {
        this.idTerceroPorcentajeTa = idTerceroPorcentajeTa;
    }

    public Integer getNumeroActualizacionTercero() {
        return numeroActualizacionTercero;
    }

    public void setNumeroActualizacionTercero(Integer numeroActualizacionTercero) {
        this.numeroActualizacionTercero = numeroActualizacionTercero;
    }

    public Integer getNumeroActualizacionTa() {
        return numeroActualizacionTa;
    }

    public void setNumeroActualizacionTa(Integer numeroActualizacionTa) {
        this.numeroActualizacionTa = numeroActualizacionTa;
    }

    public Integer getPeriodoRecalculo() {
        return periodoRecalculo;
    }

    public void setPeriodoRecalculo(Integer periodoRecalculo) {
        this.periodoRecalculo = periodoRecalculo;
    }

    public Long getIdTercero() {
        return idTercero;
    }

    public void setIdTercero(Long idTercero) {
        this.idTercero = idTercero;
    }

    public Double getValorPorcentaje() {
        return valorPorcentaje;
    }

    public void setValorPorcentaje(Double valorPorcentaje) {
        this.valorPorcentaje = valorPorcentaje;
    }

    @Override
    public VrtaTerceroPorcenTa validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("numeroActualizacionTercero", "requerido",
                        "El campo numeroActualizacionTercero es obligatorio")
                .agregar("numeroActualizacionTa", "requerido",
                        "El campo numeroActualizacionTa es obligatorio")
                .agregar("periodoRecalculo", "requerido",
                        "El campo periodo recalculo es obligatorio")
                .agregar("idTercero", "requerido",
                        "El campo idTercero es obligatorio")
                .validar();
        return this;
    }
}
