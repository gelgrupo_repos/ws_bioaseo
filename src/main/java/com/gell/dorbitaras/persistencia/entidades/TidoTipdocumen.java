
package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TidoTipdocumen  extends Entidad implements Serializable {

    private Integer uniTipdocument;
private Integer estTipdocument;
private String tidoNombre;
private String tidoAbreviatur;
private String tidoMetregistr;
private String tidoGensuspend;
private Integer usuIderegistro;
private String tidoNitcontabil;
private Integer tidoMaxcuofinancia;
private Integer tidoMaxcuounifica;
private Integer tidoMaxcuoreestruc;
private Integer tidoMaxcuoabonok;
private String tidoFinvencido;
private Integer tidoPagpriori;


    public TidoTipdocumen() {
    }

    // <editor-fold defaultstate="collapsed" desc="GET-SET">
   public Integer getUniTipdocument(){
 return uniTipdocument;
}
public TidoTipdocumen setUniTipdocument(Integer uniTipdocument){
 this.uniTipdocument=uniTipdocument;
return this;}
public Integer getEstTipdocument(){
 return estTipdocument;
}
public TidoTipdocumen setEstTipdocument(Integer estTipdocument){
 this.estTipdocument=estTipdocument;
return this;}
public String getTidoNombre(){
 return tidoNombre;
}
public TidoTipdocumen setTidoNombre(String tidoNombre){
 this.tidoNombre=tidoNombre;
return this;}
public String getTidoAbreviatur(){
 return tidoAbreviatur;
}
public TidoTipdocumen setTidoAbreviatur(String tidoAbreviatur){
 this.tidoAbreviatur=tidoAbreviatur;
return this;}
public String getTidoMetregistr(){
 return tidoMetregistr;
}
public TidoTipdocumen setTidoMetregistr(String tidoMetregistr){
 this.tidoMetregistr=tidoMetregistr;
return this;}
public String getTidoGensuspend(){
 return tidoGensuspend;
}
public TidoTipdocumen setTidoGensuspend(String tidoGensuspend){
 this.tidoGensuspend=tidoGensuspend;
return this;}
public Integer getUsuIderegistro(){
 return usuIderegistro;
}
public TidoTipdocumen setUsuIderegistro(Integer usuIderegistro){
 this.usuIderegistro=usuIderegistro;
return this;}
public String getTidoNitcontabil(){
 return tidoNitcontabil;
}
public TidoTipdocumen setTidoNitcontabil(String tidoNitcontabil){
 this.tidoNitcontabil=tidoNitcontabil;
return this;}
public Integer getTidoMaxcuofinancia(){
 return tidoMaxcuofinancia;
}
public TidoTipdocumen setTidoMaxcuofinancia(Integer tidoMaxcuofinancia){
 this.tidoMaxcuofinancia=tidoMaxcuofinancia;
return this;}
public Integer getTidoMaxcuounifica(){
 return tidoMaxcuounifica;
}
public TidoTipdocumen setTidoMaxcuounifica(Integer tidoMaxcuounifica){
 this.tidoMaxcuounifica=tidoMaxcuounifica;
return this;}
public Integer getTidoMaxcuoreestruc(){
 return tidoMaxcuoreestruc;
}
public TidoTipdocumen setTidoMaxcuoreestruc(Integer tidoMaxcuoreestruc){
 this.tidoMaxcuoreestruc=tidoMaxcuoreestruc;
return this;}
public Integer getTidoMaxcuoabonok(){
 return tidoMaxcuoabonok;
}
public TidoTipdocumen setTidoMaxcuoabonok(Integer tidoMaxcuoabonok){
 this.tidoMaxcuoabonok=tidoMaxcuoabonok;
return this;}
public String getTidoFinvencido(){
 return tidoFinvencido;
}
public TidoTipdocumen setTidoFinvencido(String tidoFinvencido){
 this.tidoFinvencido=tidoFinvencido;
return this;}
public Integer getTidoPagpriori(){
 return tidoPagpriori;
}
public TidoTipdocumen setTidoPagpriori(Integer tidoPagpriori){
 this.tidoPagpriori=tidoPagpriori;
return this;}

    // </editor-fold>

    @Override
    public TidoTipdocumen validar() throws AplicacionExcepcion {
    return this;
    }
}

