/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author jonat
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaPromFinales extends Entidad implements Serializable {
    private Integer vrtaPromfinalesIderegistro;
    private Integer conIderegistro;
    private Integer terIderegistro;
    private Integer numeroActualizacion;
    private String tipo;
    private Double valorPromedio;
    private Integer perIderegistro;
    private Timestamp vrtaFecGrabacion;    
    private Integer vrtaQaHistIderegistro;

    public VrtaPromFinales(Integer vrtaPromfinalesIderegistro, Integer conIderegistro, Integer terIderegistro, Integer numeroActualizacion, String tipo, Double valorPromedio, Integer perIderegistro, Timestamp vrtaFecGrabacion, Integer vrtaQaHistIderegistro) {
        this.vrtaPromfinalesIderegistro = vrtaPromfinalesIderegistro;
        this.conIderegistro = conIderegistro;
        this.terIderegistro = terIderegistro;
        this.numeroActualizacion = numeroActualizacion;
        this.tipo = tipo;
        this.valorPromedio = valorPromedio;
        this.perIderegistro = perIderegistro;
        this.vrtaFecGrabacion = vrtaFecGrabacion;
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }
    
    public VrtaPromFinales(Integer conIderegistro, Integer terIderegistro, Integer numeroActualizacion, String tipo, Double valorPromedio, Integer perIderegistro, Timestamp vrtaFecGrabacion, Integer vrtaQaHistIderegistro) {
        this.conIderegistro = conIderegistro;
        this.terIderegistro = terIderegistro;
        this.numeroActualizacion = numeroActualizacion;
        this.tipo = tipo;
        this.valorPromedio = valorPromedio;
        this.perIderegistro = perIderegistro;
        this.vrtaFecGrabacion = vrtaFecGrabacion;
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }
    
   public VrtaPromFinales(){
       
   }

    public Integer getVrtaPromfinalesIderegistro() {
        return vrtaPromfinalesIderegistro;
    }

    public void setVrtaPromfinalesIderegistro(Integer vrtaPromfinalesIderegistro) {
        this.vrtaPromfinalesIderegistro = vrtaPromfinalesIderegistro;
    }

    public Integer getConIderegistro() {
        return conIderegistro;
    }

    public void setConIderegistro(Integer conIderegistro) {
        this.conIderegistro = conIderegistro;
    }

    public Double getValorPromedio() {
        return valorPromedio;
    }

    public void setValorPromedio(Double valorPromedio) {
        this.valorPromedio = valorPromedio;
    }

    public Integer getPerIderegistro() {
        return perIderegistro;
    }

    public void setPerIderegistro(Integer perIderegistro) {
        this.perIderegistro = perIderegistro;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }
    
    public Integer getTerIderegistro() {
        return terIderegistro;
    }
    
    public void setTerIderegistro(Integer terIderegistro) {
        this.terIderegistro = terIderegistro;
    }
    
    public String getTipo() {
        return tipo;
    }

    public void setNumeroActualizacion(String tipo) {
        this.tipo = tipo;
    }

    public Timestamp getVrtaFecGrabacion() {
        return vrtaFecGrabacion;
    }

    public void setVrtaFecGrabacion(Timestamp vrtaFecGrabacion) {
        this.vrtaFecGrabacion = vrtaFecGrabacion;
    }
    
    public Integer getVrtaQaHistIderegistro() {
        return vrtaQaHistIderegistro;
    }

    public void setVrtaQaHistIderegistro(Integer vrtaQaHistIderegistro) {
        this.vrtaQaHistIderegistro = vrtaQaHistIderegistro;
    }

    @Override
    public VrtaPromFinales validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("perIdePadre", "requerido",
                        "El campo perIdePadre es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("periodoExacto", "requerido",
                        "El campo periodoExacto fecha grabacion es obligatorio")
                .validar();
        return this;
    }

    public void setUsuIderegistro(Integer object) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
