package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaVarterapr extends Entidad implements Serializable
{

  private Integer vrtaIderegistro;
  private TerTercero terIderegistro;
  private PerPeriodo perIderegistro;
  private ConConcepto conIderegistro;
  private ArprAreaprestacion arprIderegistro;
  private Double vrtaValor;
  private String vrtaDescripcion;
  private String vrtaEstado;
  private Integer usuIderegistroGb;
  private Date vrtaFecgrabacion;
  private Integer usuIderegistroCer;
  private Date vrtaFeccertificacion;
  private Integer empIderegistro;
  private String vrtaEstadoRegistro;
  private RacoRanconcept racoIderegistro;
  private Boolean comercializacion;

  public VrtaVarterapr()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getVrtaIderegistro()
  {
    return vrtaIderegistro;
  }

  public VrtaVarterapr setVrtaIderegistro(Integer vrtaIderegistro)
  {
    this.vrtaIderegistro = vrtaIderegistro;
    return this;
  }

  public TerTercero getTerIderegistro()
  {
    return terIderegistro;
  }

  public VrtaVarterapr setTerIderegistro(TerTercero terIderegistro)
  {
    this.terIderegistro = terIderegistro;
    return this;
  }

  public PerPeriodo getPerIderegistro()
  {
    return perIderegistro;
  }

  public void setPerIderegistro(PerPeriodo perIderegistro)
  {
    this.perIderegistro = perIderegistro;
  }

  public ConConcepto getConIderegistro()
  {
    return conIderegistro;
  }

  public VrtaVarterapr setConIderegistro(ConConcepto conIderegistro)
  {
    this.conIderegistro = conIderegistro;
    return this;
  }

  public ArprAreaprestacion getArprIderegistro()
  {
    return arprIderegistro;
  }

  public VrtaVarterapr setArprIderegistro(ArprAreaprestacion arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public Double getVrtaValor()
  {
    return vrtaValor;
  }

  public VrtaVarterapr setVrtaValor(Double vrtaValor)
  {
    this.vrtaValor = vrtaValor;
    return this;
  }

  public String getVrtaDescripcion()
  {
    return vrtaDescripcion;
  }

  public VrtaVarterapr setVrtaDescripcion(String vrtaDescripcion)
  {
    this.vrtaDescripcion = vrtaDescripcion;
    return this;
  }

  public String getVrtaEstado()
  {
    return vrtaEstado;
  }

  public VrtaVarterapr setVrtaEstado(String vrtaEstado)
  {
    this.vrtaEstado = vrtaEstado;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public VrtaVarterapr setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Date getVrtaFecgrabacion()
  {
    return vrtaFecgrabacion;
  }

  public VrtaVarterapr setVrtaFecgrabacion(Date vrtaFecgrabacion)
  {
    this.vrtaFecgrabacion = vrtaFecgrabacion;
    return this;
  }

  public Integer getUsuIderegistroCer()
  {
    return usuIderegistroCer;
  }

  public VrtaVarterapr setUsuIderegistroCer(Integer usuIderegistroCer)
  {
    this.usuIderegistroCer = usuIderegistroCer;
    return this;
  }

  public Date getVrtaFeccertificacion()
  {
    return vrtaFeccertificacion;
  }

  public VrtaVarterapr setVrtaFeccertificacion(Date vrtaFeccertificacion)
  {
    this.vrtaFeccertificacion = vrtaFeccertificacion;
    return this;
  }

  public Integer getEmpIderegistro()
  {
    return empIderegistro;
  }

  public VrtaVarterapr setEmpIderegistro(Integer empIderegistro)
  {
    this.empIderegistro = empIderegistro;
    return this;
  }

  public String getVrtaEstadoRegistro()
  {
    return vrtaEstadoRegistro;
  }

  public VrtaVarterapr setVrtaEstadoRegistro(String vrtaEstadoRegistro)
  {
    this.vrtaEstadoRegistro = vrtaEstadoRegistro;
    return this;
  }

  public RacoRanconcept getRacoIderegistro()
  {
    return racoIderegistro;
  }

  public VrtaVarterapr setRacoIderegistro(RacoRanconcept racoIderegistro)
  {
    this.racoIderegistro = racoIderegistro;
    return this;
  }

    public Boolean getComercializacion() {
        return comercializacion;
    }

    public void setComercializacion(Boolean comercializacion) {
        this.comercializacion = comercializacion;
    }
  
  

  // </editor-fold>
  @Override
  public VrtaVarterapr validar()
          throws AplicacionExcepcion
  {
    ValidarEntidad.construir(this)
            .agregar("perIderegistro.perIderegistro", "requerido",
                    "El campo periodo es obligatorio")
            .agregar("terIderegistro.terIderegistro", "requerido",
                    "El campo tercero es obligatorio")
            .agregar("conIderegistro.uniConcepto.uniIderegistro", "requerido",
                    "El campo concepto es obligatorio")
            .agregar("arprIderegistro.arprIderegistro", "requerido",
                    "El campo area de prestación es obligatorio")
            .agregar("vrtaValor", "requerido",
                    "El campo valor es obligatorio")
            .agregar("vrtaEstado", "requerido",
                    "El campo estado es obligatorio")
            .validar();
    return this;
  }
}
