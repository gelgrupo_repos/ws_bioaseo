package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RuemRutempresa extends Entidad implements Serializable
{

  private Integer ruemIderegistr;
  private RutRuta rutIderegistro;
  private Empresas empIderegistro;
  private Integer usuIderegistro;
  /**
   * I=Inserta, M=Modifica, E = elimina, N = No Aplica
   */
  private String accion;

  public RuemRutempresa()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getRuemIderegistr()
  {
    return ruemIderegistr;
  }

  public RuemRutempresa setRuemIderegistr(Integer ruemIderegistr)
  {
    this.ruemIderegistr = ruemIderegistr;
    return this;
  }

  public RutRuta getRutIderegistro()
  {
    return rutIderegistro;
  }

  public RuemRutempresa setRutIderegistro(RutRuta rutIderegistro)
  {
    this.rutIderegistro = rutIderegistro;
    return this;
  }

  public Empresas getEmpIderegistro()
  {
    return empIderegistro;
  }

  public RuemRutempresa setEmpIderegistro(Empresas empIderegistro)
  {
    this.empIderegistro = empIderegistro;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public RuemRutempresa setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getAccion()
  {
    return accion;
  }

  public void setAccion(String accion)
  {
    this.accion = accion;
  }

  // </editor-fold>
  @Override
  public RuemRutempresa validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
