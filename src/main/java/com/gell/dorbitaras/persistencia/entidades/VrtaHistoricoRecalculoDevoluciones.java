package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author jonatan
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaHistoricoRecalculoDevoluciones extends Entidad implements Serializable {

    private Integer vrtaHistoricoRecalculoDevolucionesIde;
    private Integer numeroActualizacion;
    private Integer varperregIde;
    private Integer periodoHijoIde;
    private String estado;
    private String vrtaObservacion;
    private Date vrtaFecgrabacion;
    private Date vrtaFeccertificacion;

    public VrtaHistoricoRecalculoDevoluciones() {
    }

    public VrtaHistoricoRecalculoDevoluciones(Integer numeroActualizacion, Integer varperregIde, Integer periodoHijoIde, String estado, String vrtaObservacion, Date vrtaFecgrabacion, Date vrtaFeccertificacion) {
        this.numeroActualizacion = numeroActualizacion;
        this.varperregIde = varperregIde;
        this.periodoHijoIde = periodoHijoIde;
        this.estado = estado;
        this.vrtaObservacion = vrtaObservacion;
        this.vrtaFecgrabacion = vrtaFecgrabacion;
        this.vrtaFeccertificacion = vrtaFeccertificacion;
    }

    public Integer getVrtaHistoricoRecalculoDevolucionesIde() {
        return vrtaHistoricoRecalculoDevolucionesIde;
    }

    public void setVrtaHistoricoRecalculoDevolucionesIde(Integer vrtaHistoricoRecalculoDevolucionesIde) {
        this.vrtaHistoricoRecalculoDevolucionesIde = vrtaHistoricoRecalculoDevolucionesIde;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getVarperregIde() {
        return varperregIde;
    }

    public void setVarperregIde(Integer varperregIde) {
        this.varperregIde = varperregIde;
    }

    public Integer getPeriodoHijoIde() {
        return periodoHijoIde;
    }

    public void setPeriodoHijoIde(Integer periodoHijoIde) {
        this.periodoHijoIde = periodoHijoIde;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getVrtaObservacion() {
        return vrtaObservacion;
    }

    public void setVrtaObservacion(String vrtaObservacion) {
        this.vrtaObservacion = vrtaObservacion;
    }

    public Date getVrtaFecgrabacion() {
        return vrtaFecgrabacion;
    }

    public void setVrtaFecgrabacion(Date vrtaFecgrabacion) {
        this.vrtaFecgrabacion = vrtaFecgrabacion;
    }

    public Date getVrtaFeccertificacion() {
        return vrtaFeccertificacion;
    }

    public void setVrtaFeccertificacion(Date vrtaFeccertificacion) {
        this.vrtaFeccertificacion = vrtaFeccertificacion;
    }

    @Override
    public VrtaHistoricoRecalculoDevoluciones validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("vrtaHistoricoRecalculoDevolucionesIde", "requerido",
                        "El campo vrtaHistoricoRecalculoDevolucionesIde es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("varperregIde", "requerido",
                        "El campo varperregIde es obligatorio")
                .agregar("periodoHijoIde", "requerido",
                        "El campo periodoHijoIde fecha grabacion es obligatorio")
                .validar();
        return this;
    }
}
