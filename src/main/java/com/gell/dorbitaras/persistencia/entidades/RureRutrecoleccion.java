package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author cristtian
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RureRutrecoleccion extends Entidad implements Serializable
{

  private Integer rureIderegistro;
  private ArprAreaprestacion arprIderegistro;
  private Integer usuIderegistroGb;
  private Date rureFecgrabacion;
  private Integer usuIderegistroAc;
  private Date rureFecact;
  private Integer rutIdemacruta;
  private String rutMicroruta;
  private List<HrrHorrecoleccion> listaDias;

  public RureRutrecoleccion()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getRureIderegistro()
  {
    return rureIderegistro;
  }

  public RureRutrecoleccion setRureIderegistro(Integer rureIderegistro)
  {
    this.rureIderegistro = rureIderegistro;
    return this;
  }

  public ArprAreaprestacion getArprIderegistro()
  {
    return arprIderegistro;
  }

  public RureRutrecoleccion setArprIderegistro(ArprAreaprestacion arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public Integer getUsuIderegistroGb()
  {
    return usuIderegistroGb;
  }

  public RureRutrecoleccion setUsuIderegistroGb(Integer usuIderegistroGb)
  {
    this.usuIderegistroGb = usuIderegistroGb;
    return this;
  }

  public Date getRureFecgrabacion()
  {
    return rureFecgrabacion;
  }

  public RureRutrecoleccion setRureFecgrabacion(Date rureFecgrabacion)
  {
    this.rureFecgrabacion = rureFecgrabacion;
    return this;
  }

  public Integer getUsuIderegistroAc()
  {
    return usuIderegistroAc;
  }

  public RureRutrecoleccion setUsuIderegistroAc(Integer usuIderegistroAc)
  {
    this.usuIderegistroAc = usuIderegistroAc;
    return this;
  }

  public Date getRureFecact()
  {
    return rureFecact;
  }

  public RureRutrecoleccion setRureFecact(Date rureFecact)
  {
    this.rureFecact = rureFecact;
    return this;
  }

  public Integer getRutIdemacruta()
  {
    return rutIdemacruta;
  }

  public RureRutrecoleccion setRutIdemacruta(Integer rutIdemacruta)
  {
    this.rutIdemacruta = rutIdemacruta;
    return this;
  }

  public String getRutMicroruta()
  {
    return rutMicroruta;
  }

  public RureRutrecoleccion setRutMicroruta(String rutMicroruta)
  {
    this.rutMicroruta = rutMicroruta;
    return this;
  }

  public List<HrrHorrecoleccion> getListaDias()
  {
    return listaDias;
  }

  public RureRutrecoleccion setListaDias(List<HrrHorrecoleccion> listaDias)
  {
    this.listaDias = listaDias;
    return this;
  }

  // </editor-fold>
  @Override
  public RureRutrecoleccion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
