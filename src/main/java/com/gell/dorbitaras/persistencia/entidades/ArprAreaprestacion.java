package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import java.util.List;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArprAreaprestacion extends Entidad implements Serializable
{

  private Integer arprIderegistro;
  private DctaDctotarifas dctaIderegistro;
  private String arprNombre;
  private String arprDescripcion;
  private String arprNuap;
  private String arprNusd;
  private String arprNomdisfinal;
  private Integer empIderegistro;
  private RgtaRgitarifario rgtaIderegistro;
  private LiqLiquidacion liqIderegistro;
  private List<EttaEstttarifas> listaEstratos;
  private List<ApprAreaproyecto> listaProyectos;
  private String proyectos;
  private String estratos;
  private String arprLiquidaciones;

  public ArprAreaprestacion()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getArprIderegistro()
  {
    return arprIderegistro;
  }

  public ArprAreaprestacion setArprIderegistro(Integer arprIderegistro)
  {
    this.arprIderegistro = arprIderegistro;
    return this;
  }

  public DctaDctotarifas getDctaIderegistro()
  {
    return dctaIderegistro;
  }

  public ArprAreaprestacion setDctaIderegistro(DctaDctotarifas dctaIderegistro)
  {
    this.dctaIderegistro = dctaIderegistro;
    return this;
  }

  public String getArprNombre()
  {
    return arprNombre;
  }

  public ArprAreaprestacion setArprNombre(String arprNombre)
  {
    this.arprNombre = arprNombre;
    return this;
  }

  public String getArprDescripcion()
  {
    return arprDescripcion;
  }

  public ArprAreaprestacion setArprDescripcion(String arprDescripcion)
  {
    this.arprDescripcion = arprDescripcion;
    return this;
  }

  public String getArprNuap()
  {
    return arprNuap;
  }

  public ArprAreaprestacion setArprNuap(String arprNuap)
  {
    this.arprNuap = arprNuap;
    return this;
  }

  public String getArprNusd()
  {
    return arprNusd;
  }

  public ArprAreaprestacion setArprNusd(String arprNusd)
  {
    this.arprNusd = arprNusd;
    return this;
  }

  public String getArprNomdisfinal()
  {
    return arprNomdisfinal;
  }

  public ArprAreaprestacion setArprNomdisfinal(String arprNomdisfinal)
  {
    this.arprNomdisfinal = arprNomdisfinal;
    return this;
  }

  public Integer getEmpIderegistro()
  {
    return empIderegistro;
  }

  public ArprAreaprestacion setEmpIderegistro(Integer empIderegistro)
  {
    this.empIderegistro = empIderegistro;
    return this;
  }

  public RgtaRgitarifario getRgtaIderegistro()
  {
    return rgtaIderegistro;
  }

  public ArprAreaprestacion setRgtaIderegistro(RgtaRgitarifario rgtaIderegistro)
  {
    this.rgtaIderegistro = rgtaIderegistro;
    return this;
  }

  public LiqLiquidacion getLiqIderegistro()
  {
    return liqIderegistro;
  }

  public ArprAreaprestacion setLiqIderegistro(LiqLiquidacion liqIderegistro)
  {
    this.liqIderegistro = liqIderegistro;
    return this;
  }

  public List<EttaEstttarifas> getListaEstratos()
  {
    return listaEstratos;
  }

  public void setListaEstratos(List<EttaEstttarifas> listaEstratos)
  {
    this.listaEstratos = listaEstratos;
  }

  public List<ApprAreaproyecto> getListaProyectos()
  {
    return listaProyectos;
  }

  public void setListaProyectos(List<ApprAreaproyecto> listaProyectos)
  {
    this.listaProyectos = listaProyectos;
  }

  public String getProyectos()
  {
    return proyectos;
  }

  public void setProyectos(String proyectos)
  {
    this.proyectos = proyectos;
  }

  public String getEstratos()
  {
    return estratos;
  }

  public void setEstratos(String estratos)
  {
    this.estratos = estratos;
  }

    public String getArprLiquidaciones() {
        return arprLiquidaciones;
    }

    public void setArprLiquidaciones(String arprLiquidaciones) {
        this.arprLiquidaciones = arprLiquidaciones;
    }
  

  // </editor-fold>
  @Override
  public ArprAreaprestacion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
