package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.LogUtil;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CoreConrelacio extends Entidad implements Serializable, Cloneable
{

  private ConConcepto uniConcepto;
  private ConConcepto uniConrelacion;
  private Integer coreIderegistr;
  private Integer usuIderegistro;
  private FunFuncion funIderegistro;

  public CoreConrelacio()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public ConConcepto getUniConcepto()
  {
    return uniConcepto;
  }

  public CoreConrelacio setUniConcepto(ConConcepto uniConcepto)
  {
    this.uniConcepto = uniConcepto;
    return this;
  }

  public ConConcepto getUniConrelacion()
  {
    return uniConrelacion;
  }

  public CoreConrelacio setUniConrelacion(ConConcepto uniConrelacion)
  {
    this.uniConrelacion = uniConrelacion;
    return this;
  }

  public Integer getCoreIderegistr()
  {
    return coreIderegistr;
  }

  public CoreConrelacio setCoreIderegistr(Integer coreIderegistr)
  {
    this.coreIderegistr = coreIderegistr;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public CoreConrelacio setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public FunFuncion getFunIderegistro()
  {
    return funIderegistro;
  }

  public CoreConrelacio setFunIderegistro(FunFuncion funIderegistro)
  {
    this.funIderegistro = funIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public CoreConrelacio validar()
          throws AplicacionExcepcion
  {
    return this;
  }

  @Override
  public CoreConrelacio clone()
  {
    try {
      return (CoreConrelacio) super.clone();
    } catch (CloneNotSupportedException ex) {
      LogUtil.error(ex);
      throw new RuntimeException();
    }
  }
}
