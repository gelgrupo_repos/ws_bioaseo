package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.persistencia.entidades.PrgPrograma;
import java.util.List;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@SuppressWarnings("OverridableMethodCallInConstructor")
public class ConConceptoIndicadorProductividad extends Entidad implements Serializable
{

  private Integer uniConcepto;
  private Double conValor;
  private String conNombre;
  private String conAlias;

  public ConConceptoIndicadorProductividad()
  {
  }

  public ConConceptoIndicadorProductividad(Integer uniConcepto)
  {
    this.setUniConcepto(uniConcepto);
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">

    public Integer getUniConcepto() {
        return uniConcepto;
    }

    public void setUniConcepto(Integer uniConcepto) {
        this.uniConcepto = uniConcepto;
    }

    public Double getConValor() {
        return conValor;
    }

    public void setConValor(Double conValor) {
        this.conValor = conValor;
    }

    public String getConNombre() {
        return conNombre;
    }

    public void setConNombre(String conNombre) {
        this.conNombre = conNombre;
    }

    public String getConAlias() {
        return conAlias;
    }

    public void setConAlias(String conAlias) {
        this.conAlias = conAlias;
    }
  

  // </editor-fold>
  
  @Override
  public ConConceptoIndicadorProductividad validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
