package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ClaClase extends Entidad implements Serializable {

  private Integer claIderegistro;
  private String claNombre;
  private String claTipo;
  private Integer usuIderegistro;

  public ClaClase()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getClaIderegistro()
  {
    return claIderegistro;
  }

  public ClaClase setClaIderegistro(Integer claIderegistro)
  {
    this.claIderegistro = claIderegistro;
    return this;
  }

  public String getClaNombre()
  {
    return claNombre;
  }

  public ClaClase setClaNombre(String claNombre)
  {
    this.claNombre = claNombre;
    return this;
  }

  public String getClaTipo()
  {
    return claTipo;
  }

  public ClaClase setClaTipo(String claTipo)
  {
    this.claTipo = claTipo;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public ClaClase setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public ClaClase validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
