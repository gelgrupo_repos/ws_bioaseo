package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.List;
import java.sql.Array;
import java.sql.Time;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DctaDctotarifas extends Entidad implements Serializable
{

  private Integer dctaIderegistro;
  private TerTercero terIderegistro;
  private RgtaRgitarifario rgtaIderegistro;
  private String dctaNombre;
  private String dctaDescripcion;
  private Date dctaFecinivigencia;
  private Date dctaFecfinvigencia;
  private UniUnidad uniIdetipodocumento;
  private String dctaNumero;
  private Date dctaFecemision;
  private Date dctaFecpublicacion;
  private String dctaUrldocumento;
  private String dctaEstado;

  public DctaDctotarifas()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getDctaIderegistro()
  {
    return dctaIderegistro;
  }

  public DctaDctotarifas setDctaIderegistro(Integer dctaIderegistro)
  {
    this.dctaIderegistro = dctaIderegistro;
    return this;
  }

  public TerTercero getTerIderegistro()
  {
    return terIderegistro;
  }

  public DctaDctotarifas setTerIderegistro(TerTercero terIderegistro)
  {
    this.terIderegistro = terIderegistro;
    return this;
  }

  public RgtaRgitarifario getRgtaIderegistro()
  {
    return rgtaIderegistro;
  }

  public DctaDctotarifas setRgtaIderegistro(RgtaRgitarifario rgtaIderegistro)
  {
    this.rgtaIderegistro = rgtaIderegistro;
    return this;
  }

  public String getDctaNombre()
  {
    return dctaNombre;
  }

  public DctaDctotarifas setDctaNombre(String dctaNombre)
  {
    this.dctaNombre = dctaNombre;
    return this;
  }

  public String getDctaDescripcion()
  {
    return dctaDescripcion;
  }

  public DctaDctotarifas setDctaDescripcion(String dctaDescripcion)
  {
    this.dctaDescripcion = dctaDescripcion;
    return this;
  }

  public Date getDctaFecinivigencia()
  {
    return dctaFecinivigencia;
  }

  public DctaDctotarifas setDctaFecinivigencia(Date dctaFecinivigencia)
  {
    this.dctaFecinivigencia = dctaFecinivigencia;
    return this;
  }

  public Date getDctaFecfinvigencia()
  {
    return dctaFecfinvigencia;
  }

  public DctaDctotarifas setDctaFecfinvigencia(Date dctaFecfinvigencia)
  {
    this.dctaFecfinvigencia = dctaFecfinvigencia;
    return this;
  }

  public UniUnidad getUniIdetipodocumento()
  {
    return uniIdetipodocumento;
  }

  public DctaDctotarifas setUniIdetipodocumento(UniUnidad uniIdetipodocumento)
  {
    this.uniIdetipodocumento = uniIdetipodocumento;
    return this;
  }

  public String getDctaNumero()
  {
    return dctaNumero;
  }

  public DctaDctotarifas setDctaNumero(String dctaNumero)
  {
    this.dctaNumero = dctaNumero;
    return this;
  }

  public Date getDctaFecemision()
  {
    return dctaFecemision;
  }

  public DctaDctotarifas setDctaFecemision(Date dctaFecemision)
  {
    this.dctaFecemision = dctaFecemision;
    return this;
  }

  public Date getDctaFecpublicacion()
  {
    return dctaFecpublicacion;
  }

  public DctaDctotarifas setDctaFecpublicacion(Date dctaFecpublicacion)
  {
    this.dctaFecpublicacion = dctaFecpublicacion;
    return this;
  }

  public String getDctaUrldocumento()
  {
    return dctaUrldocumento;
  }

  public DctaDctotarifas setDctaUrldocumento(String dctaUrldocumento)
  {
    this.dctaUrldocumento = dctaUrldocumento;
    return this;
  }

  public String getDctaEstado()
  {
    return dctaEstado;
  }

  public DctaDctotarifas setDctaEstado(String dctaEstado)
  {
    this.dctaEstado = dctaEstado;
    return this;
  }

  // </editor-fold>
  @Override
  public DctaDctotarifas validar()
          throws AplicacionExcepcion
  {
    ValidarEntidad.construir(this)
            .agregar("uniIdetipodocumento.uniIderegistro", "requerido", "El tipo de documento es obligatorio")
            .validar();
    return this;
  }
}
