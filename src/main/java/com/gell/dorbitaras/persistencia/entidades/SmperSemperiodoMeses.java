package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;

/**
 *
 * @author maestro
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SmperSemperiodoMeses extends Entidad implements Serializable {

    private Integer perIdEPadre;
    private Integer rgtaIdERegistro;
    private String smperDescripcion;
    private Integer perIdeRegistro;
    private Integer perFecInicial;
    private Integer smperNumero;
    private String nombrePeriodo;

    public SmperSemperiodoMeses() {
    }
    // <editor-fold defaultstate="collapsed" desc="GET-SET">
    public Integer getPerIdEPadre() {
        return perIdEPadre;
    }

    public void setPerIdEPadre(Integer perIdEPadre) {
        this.perIdEPadre = perIdEPadre;
    }

    public Integer getRgtaIdERegistro() {
        return rgtaIdERegistro;
    }

    public void setRgtaIdERegistro(Integer rgtaIdERegistro) {
        this.rgtaIdERegistro = rgtaIdERegistro;
    }

    public String getSmperDescripcion() {
        return smperDescripcion;
    }

    public void setSmperDescripcion(String smperDescripcion) {
        this.smperDescripcion = smperDescripcion;
    }

    public Integer getPerIdeRegistro() {
        return perIdeRegistro;
    }

    public void setPerIdeRegistro(Integer perIdeRegistro) {
        this.perIdeRegistro = perIdeRegistro;
    }

    public String getNombrePeriodo() {
        return nombrePeriodo;
    }

    public void setNombrePeriodo(String nombrePeriodo) {
        this.nombrePeriodo = nombrePeriodo;
    }

    public Integer getPerFecInicial() {
        return perFecInicial;
    }

    public void setPerFecInicial(Integer perFecInicial) {
        this.perFecInicial = perFecInicial;
    }

    public Integer getSmperNumero() {
        return smperNumero;
    }

    public void setSmperNumero(Integer smperNumero) {
        this.smperNumero = smperNumero;
    }
    
  // </editor-fold>
    @Override
    public SmperSemperiodoMeses validar()
            throws AplicacionExcepcion {

        ValidarEntidad.construir(this)
                .agregar("perIdEPadre", "requerido",
                        "El Régimen es obligatorio")
                .agregar("rgtaIdERegistro", "requerido",
                        "El Semestre es obligatorio")
                .agregar("smperDescripcion", "requerido",
                        "El mes es obligatorio")
                .agregar("smperIdRegistro", "requerido",
                        "El Semestre es obligatorio")
                .agregar("perFecInicial", "requerido",
                        "El mes es obligatorio")
                .validar();
        return this;
    }
}
