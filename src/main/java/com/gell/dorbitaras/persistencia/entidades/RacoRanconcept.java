package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RacoRanconcept extends Entidad implements Serializable
{

  private Integer racoIderegistr;
  private Integer uniConcepto;
  private Double racoRaninicial;
  private Double racoRanfinal;
  private Double racoValor;
  private String racoFormula;
  private Integer usuIderegistro;

  public RacoRanconcept()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getRacoIderegistr()
  {
    return racoIderegistr;
  }

  public RacoRanconcept setRacoIderegistr(Integer racoIderegistr)
  {
    this.racoIderegistr = racoIderegistr;
    return this;
  }

  public Integer getUniConcepto()
  {
    return uniConcepto;
  }

  public RacoRanconcept setUniConcepto(Integer uniConcepto)
  {
    this.uniConcepto = uniConcepto;
    return this;
  }

  public Double getRacoRaninicial()
  {
    return racoRaninicial;
  }

  public RacoRanconcept setRacoRaninicial(Double racoRaninicial)
  {
    this.racoRaninicial = racoRaninicial;
    return this;
  }

  public Double getRacoRanfinal()
  {
    return racoRanfinal;
  }

  public RacoRanconcept setRacoRanfinal(Double racoRanfinal)
  {
    this.racoRanfinal = racoRanfinal;
    return this;
  }

  public Double getRacoValor()
  {
    return racoValor;
  }

  public RacoRanconcept setRacoValor(Double racoValor)
  {
    this.racoValor = racoValor;
    return this;
  }

  public String getRacoFormula()
  {
    return racoFormula;
  }

  public RacoRanconcept setRacoFormula(String racoFormula)
  {
    this.racoFormula = racoFormula;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public RacoRanconcept setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  // </editor-fold>
  @Override
  public RacoRanconcept validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
