package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;
import java.util.List;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RutRuta extends Entidad implements Serializable
{

  private Integer rutIderegistro;
  private String rutNombre;
  private String rutTipo;
  private String rutCodigo;
  private CicCiclo cicIderegistro;
  private Integer usuIderegistro;
  private Integer uniTiporuta;
  private List<RuemRutempresa> listaValores;

  public RutRuta()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getRutIderegistro()
  {
    return rutIderegistro;
  }

  public RutRuta setRutIderegistro(Integer rutIderegistro)
  {
    this.rutIderegistro = rutIderegistro;
    return this;
  }

  public String getRutNombre()
  {
    return rutNombre;
  }

  public RutRuta setRutNombre(String rutNombre)
  {
    this.rutNombre = rutNombre;
    return this;
  }

  public String getRutTipo()
  {
    return rutTipo;
  }

  public RutRuta setRutTipo(String rutTipo)
  {
    this.rutTipo = rutTipo;
    return this;
  }

  public CicCiclo getCicIderegistro()
  {
    return cicIderegistro;
  }

  public RutRuta setCicIderegistro(CicCiclo cicIderegistro)
  {
    this.cicIderegistro = cicIderegistro;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public RutRuta setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public Integer getUniTiporuta()
  {
    return uniTiporuta;
  }

  public RutRuta setUniTiporuta(Integer uniTiporuta)
  {
    this.uniTiporuta = uniTiporuta;
    return this;
  }

  public List<RuemRutempresa> getListaValores()
  {
    return listaValores;
  }

  public RutRuta setListaValores(List<RuemRutempresa> listaValores)
  {
    this.listaValores = listaValores;
    return this;
  }
    public String getRutCodigo() {
        return rutCodigo;
    }

    public void setRutCodigo(String rutCodigo) {
        this.rutCodigo = rutCodigo;
    }
  
  // </editor-fold>
  @Override
  public RutRuta validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
