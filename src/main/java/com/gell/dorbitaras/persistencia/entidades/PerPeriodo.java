package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PerPeriodo extends Entidad implements Serializable
{

  private Integer perIderegistro;
  private Integer smperIderegistro;
  private Integer perIdeorden;
  private CicCiclo cicIderegistro;
  private String perNombre;
  private String perEstado;
  private String perBlofecha;
  private Date perFecinicial;
  private Date perFecfinal;
  private Date perFecvence;
  private Date perFecsuspens;
  private Integer usuIderegistro;

  public PerPeriodo()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getPerIderegistro()
  {
    return perIderegistro;
  }

  public PerPeriodo setPerIderegistro(Integer perIderegistro)
  {
    this.perIderegistro = perIderegistro;
    return this;
  }

  public Integer getPerIdeorden()
  {
    return perIdeorden;
  }

  public PerPeriodo setPerIdeorden(Integer perIdeorden)
  {
    this.perIdeorden = perIdeorden;
    return this;
  }

  public CicCiclo getCicIderegistro()
  {
    return cicIderegistro;
  }

  public PerPeriodo setCicIderegistro(CicCiclo cicIderegistro)
  {
    this.cicIderegistro = cicIderegistro;
    return this;
  }

  public String getPerNombre()
  {
    return perNombre;
  }

  public PerPeriodo setPerNombre(String perNombre)
  {
    this.perNombre = perNombre;
    return this;
  }

  public String getPerEstado()
  {
    return perEstado;
  }

  public PerPeriodo setPerEstado(String perEstado)
  {
    this.perEstado = perEstado;
    return this;
  }

  public String getPerBlofecha()
  {
    return perBlofecha;
  }

  public PerPeriodo setPerBlofecha(String perBlofecha)
  {
    this.perBlofecha = perBlofecha;
    return this;
  }

  public Date getPerFecinicial()
  {
    return perFecinicial;
  }

  public PerPeriodo setPerFecinicial(Date perFecinicial)
  {
    this.perFecinicial = perFecinicial;
    return this;
  }

  public Date getPerFecfinal()
  {
    return perFecfinal;
  }

  public PerPeriodo setPerFecfinal(Date perFecfinal)
  {
    this.perFecfinal = perFecfinal;
    return this;
  }

  public Date getPerFecvence()
  {
    return perFecvence;
  }

  public PerPeriodo setPerFecvence(Date perFecvence)
  {
    this.perFecvence = perFecvence;
    return this;
  }

  public Date getPerFecsuspens()
  {
    return perFecsuspens;
  }

  public PerPeriodo setPerFecsuspens(Date perFecsuspens)
  {
    this.perFecsuspens = perFecsuspens;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public PerPeriodo setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

    public Integer getSmperIderegistro() {
        return smperIderegistro;
    }

    public void setSmperIderegistro(Integer smperIderegistro) {
        this.smperIderegistro = smperIderegistro;
    }  

  // </editor-fold>
  @Override
  public PerPeriodo validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
