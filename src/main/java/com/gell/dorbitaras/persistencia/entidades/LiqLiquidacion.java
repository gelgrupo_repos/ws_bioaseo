package com.gell.dorbitaras.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author hrey
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LiqLiquidacion extends Entidad implements Serializable
{

  private Integer uniLiquidacion;
  private UniUnidad estLiquidacion;
  private String liqNombre;
  private DotiDoctipo uniDocumento;
  private DotiDoctipo uniTipdocument;
  private Date liqInivigencia;
  private Date liqFinvigencia;
  private String liqVenclasific;
  private String liqEstado;
  private String liqHistorico;
  private Integer liqDiavencim;
  private Integer liqDiasuspens;
  private Integer usuIderegistro;
  private String liqTipcuota;
  private String liqCtrventas;
  private Long hliqIderegistr;

  public LiqLiquidacion()
  {
  }

  // <editor-fold defaultstate="collapsed" desc="GET-SET">
  public Integer getUniLiquidacion()
  {
    return uniLiquidacion;
  }

  public LiqLiquidacion setUniLiquidacion(Integer uniLiquidacion)
  {
    this.uniLiquidacion = uniLiquidacion;
    return this;
  }

  public UniUnidad getEstLiquidacion()
  {
    return estLiquidacion;
  }

  public LiqLiquidacion setEstLiquidacion(UniUnidad estLiquidacion)
  {
    this.estLiquidacion = estLiquidacion;
    return this;
  }

  public String getLiqNombre()
  {
    return liqNombre;
  }

  public LiqLiquidacion setLiqNombre(String liqNombre)
  {
    this.liqNombre = liqNombre;
    return this;
  }

  public DotiDoctipo getUniDocumento()
  {
    return uniDocumento;
  }

  public LiqLiquidacion setUniDocumento(DotiDoctipo uniDocumento)
  {
    this.uniDocumento = uniDocumento;
    return this;
  }

  public DotiDoctipo getUniTipdocument()
  {
    return uniTipdocument;
  }

  public LiqLiquidacion setUniTipdocument(DotiDoctipo uniTipdocument)
  {
    this.uniTipdocument = uniTipdocument;
    return this;
  }

  public Date getLiqInivigencia()
  {
    return liqInivigencia;
  }

  public LiqLiquidacion setLiqInivigencia(Date liqInivigencia)
  {
    this.liqInivigencia = liqInivigencia;
    return this;
  }

  public Date getLiqFinvigencia()
  {
    return liqFinvigencia;
  }

  public LiqLiquidacion setLiqFinvigencia(Date liqFinvigencia)
  {
    this.liqFinvigencia = liqFinvigencia;
    return this;
  }

  public String getLiqVenclasific()
  {
    return liqVenclasific;
  }

  public LiqLiquidacion setLiqVenclasific(String liqVenclasific)
  {
    this.liqVenclasific = liqVenclasific;
    return this;
  }

  public String getLiqEstado()
  {
    return liqEstado;
  }

  public LiqLiquidacion setLiqEstado(String liqEstado)
  {
    this.liqEstado = liqEstado;
    return this;
  }

  public String getLiqHistorico()
  {
    return liqHistorico;
  }

  public LiqLiquidacion setLiqHistorico(String liqHistorico)
  {
    this.liqHistorico = liqHistorico;
    return this;
  }

  public Integer getLiqDiavencim()
  {
    return liqDiavencim;
  }

  public LiqLiquidacion setLiqDiavencim(Integer liqDiavencim)
  {
    this.liqDiavencim = liqDiavencim;
    return this;
  }

  public Integer getLiqDiasuspens()
  {
    return liqDiasuspens;
  }

  public LiqLiquidacion setLiqDiasuspens(Integer liqDiasuspens)
  {
    this.liqDiasuspens = liqDiasuspens;
    return this;
  }

  public Integer getUsuIderegistro()
  {
    return usuIderegistro;
  }

  public LiqLiquidacion setUsuIderegistro(Integer usuIderegistro)
  {
    this.usuIderegistro = usuIderegistro;
    return this;
  }

  public String getLiqTipcuota()
  {
    return liqTipcuota;
  }

  public LiqLiquidacion setLiqTipcuota(String liqTipcuota)
  {
    this.liqTipcuota = liqTipcuota;
    return this;
  }

  public String getLiqCtrventas()
  {
    return liqCtrventas;
  }

  public LiqLiquidacion setLiqCtrventas(String liqCtrventas)
  {
    this.liqCtrventas = liqCtrventas;
    return this;
  }

  public Long getHliqIderegistr()
  {
    return hliqIderegistr;
  }

  public LiqLiquidacion setHliqIderegistr(Long hliqIderegistr)
  {
    this.hliqIderegistr = hliqIderegistr;
    return this;
  }

  // </editor-fold>
  @Override
  public LiqLiquidacion validar()
          throws AplicacionExcepcion
  {
    return this;
  }
}
