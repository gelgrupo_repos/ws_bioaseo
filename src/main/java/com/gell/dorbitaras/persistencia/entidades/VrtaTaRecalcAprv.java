/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.entidades;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import com.gell.estandar.util.ValidarEntidad;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author jonat
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VrtaTaRecalcAprv extends Entidad implements Serializable {
    
    private Integer vrtaTaRecalcIdeRegistro;
    private Integer varprIderegistro;
    private Integer numeroActualizacion;
    private Integer periodoRecalculo;
    private Double valor;
    private Timestamp fechaRegistro;
    private Integer usuarioRegistro;
    private Boolean conDinc;

    public VrtaTaRecalcAprv() {
    }
    
    public VrtaTaRecalcAprv(Integer vrtaTaRecalcIdeRegistro, Integer VarprIderegistro, Integer numeroActualizacion, Integer periodoRecalculo, Double valor, Timestamp fechaRegistro, Integer usuarioRegistro, Boolean  conDinc) {
        this.vrtaTaRecalcIdeRegistro = vrtaTaRecalcIdeRegistro;
        this.varprIderegistro = VarprIderegistro;
        this.numeroActualizacion = numeroActualizacion;
        this.periodoRecalculo = periodoRecalculo;
        this.valor = valor;
        this.fechaRegistro = fechaRegistro;
        this.usuarioRegistro = usuarioRegistro;
        this.conDinc = conDinc;
    }

    public VrtaTaRecalcAprv(Integer numeroActualizacion, Integer VarprIderegistro, Integer periodoRecalculo, Double valor, Timestamp fechaRegistro, Integer usuarioRegistro, Boolean conDinc) {
        this.numeroActualizacion = numeroActualizacion;
        this.varprIderegistro = VarprIderegistro;
        this.periodoRecalculo = periodoRecalculo;
        this.valor = valor;
        this.fechaRegistro = fechaRegistro;
        this.usuarioRegistro = usuarioRegistro;
        this.conDinc = conDinc;
    }
    
    

    public Integer getVrtaTaRecalcIdeRegistro() {
        return vrtaTaRecalcIdeRegistro;
    }

    public void setVrtaTaRecalcIdeRegistro(Integer vrtaTaRecalcIdeRegistro) {
        this.vrtaTaRecalcIdeRegistro = vrtaTaRecalcIdeRegistro;
    }
    
    public Integer getvarprIderegistro() {
        return varprIderegistro;
    }

    public void setVarprIderegistro(Integer VarprIderegistro) {
        this.varprIderegistro = VarprIderegistro;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getPeriodoRecalculo() {
        return periodoRecalculo;
    }

    public void setPeriodoRecalculo(Integer periodoRecalculo) {
        this.periodoRecalculo = periodoRecalculo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Timestamp getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Timestamp fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getUsuarioRegistro() {
        return usuarioRegistro;
    }

    public void setUsuarioRegistro(Integer usuarioRegistro) {
        this.usuarioRegistro = usuarioRegistro;
    }

    public Boolean isConDinc() {
        return conDinc;
    }

    public void setConDinc(Boolean conDinc) {
        this.conDinc = conDinc;
    }
    
    
    
    @Override
    public VrtaTaRecalcAprv validar() throws AplicacionExcepcion {
        ValidarEntidad.construir(this)
                .agregar("vrtaTaRecalcIdeRegistro", "requerido",
                        "El campo Recalculo IdRegistro es obligatorio")
                .agregar("numeroActualizacion", "requerido",
                        "El campo numeroActualizacion es obligatorio")
                .agregar("periodoRecalculo", "requerido",
                        "El campo periodo recalculo es obligatorio")
                .agregar("valor", "requerido",
                        "El campo valor aplicado recalculo es obligatorio")
                .agregar("fechaRegistro", "requerido",
                        "El campo fecha registro recalculo es obligatorio")
                .agregar("usuarioRegistro", "requerido",
                        "El campo usuario registro es obligatorio")
                .validar();
        return this;
    }
}
