/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class ConceptosDevolucionesDTO extends Entidad {
    
    private Double valor;
    private String segmento;
    private String conAlias;
   
    public ConceptosDevolucionesDTO() {
    }

    public ConceptosDevolucionesDTO(Double valor, String segmento, String conAlias) {
        this.valor = valor;
        this.segmento = segmento;
        this.conAlias = conAlias;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getSegmento() {
        return segmento;
    }

    public void setConDinc(String segmento) {
        this.segmento = segmento;
    }

    public String getConAlias() {
        return conAlias;
    }

    public void setConAlias(String conAlias) {
        this.conAlias = conAlias;
    }

    @Override
    public ConceptosDevolucionesDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
