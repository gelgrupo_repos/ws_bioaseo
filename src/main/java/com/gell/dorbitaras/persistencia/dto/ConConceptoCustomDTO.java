/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class ConConceptoCustomDTO extends Entidad {

    private Integer uniConcepto;
    private String nombreConcepto;
    private String aliasConcepto;
    private String abreviatura;
    private Double valor;
    private Double valorAnterior;

    public ConConceptoCustomDTO() {
    }

    public ConConceptoCustomDTO(Integer uniConcepto, String nombreConcepto, String aliasConcepto, String abreviatura, Double valor, Double valorAnterior) {
        this.uniConcepto = uniConcepto;
        this.nombreConcepto = nombreConcepto;
        this.aliasConcepto = aliasConcepto;
        this.abreviatura = abreviatura;
        this.valor = valor;
        this.valorAnterior = valorAnterior;
    }

    public Integer getUniConcepto() {
        return uniConcepto;
    }

    public void setUniConcepto(Integer uniConcepto) {
        this.uniConcepto = uniConcepto;
    }

    public String getNombreConcepto() {
        return nombreConcepto;
    }

    public void setNombreConcepto(String nombreConcepto) {
        this.nombreConcepto = nombreConcepto;
    }

    public String getAliasConcepto() {
        return aliasConcepto;
    }

    public void setAliasConcepto(String aliasConcepto) {
        this.aliasConcepto = aliasConcepto;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Double getValorAnterior() {
        return valorAnterior;
    }

    public void setValorAnterior(Double valorAnterior) {
        this.valorAnterior = valorAnterior;
    }

    @Override
    public ConConceptoCustomDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
