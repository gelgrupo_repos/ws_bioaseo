/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class VrtaVarteraprDTO extends Entidad
{

  private String listaRangos;
  private String listaValores;
  private String nombreConcepto;
  private Integer idConcepto;
  private String observacion;

  public String getListaRangos()
  {
    return listaRangos;
  }

  public void setListaRangos(String listaRangos)
  {
    this.listaRangos = listaRangos;
  }

  public String getListaValores()
  {
    return listaValores;
  }

  public void setListaValores(String listaValores)
  {
    this.listaValores = listaValores;
  }

  public String getNombreConcepto()
  {
    return nombreConcepto;
  }

  public void setNombreConcepto(String nombreConcepto)
  {
    this.nombreConcepto = nombreConcepto;
  }

  public Integer getIdConcepto()
  {
    return idConcepto;
  }

  public void setIdConcepto(Integer idConcepto)
  {
    this.idConcepto = idConcepto;
  }

  public String getObservacion()
  {
    return observacion;
  }

  public void setObservacion(String observacion)
  {
    this.observacion = observacion;
  }

  @Override
  public VrtaVarteraprDTO validar()
          throws AplicacionExcepcion
  {
    return null;
  }

}
