/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class VarcVarcalculoBaseDTO extends Entidad
{
  private Integer idConcepto ;
  private Integer idRango ;
  private String nombreConcepto ;
  private String ranInical ;
  private String ranFinal ;
  private String valorPeriodos ;

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public String getRanInical() {
        return ranInical;
    }

    public void setRanInical(String ranInical) {
        this.ranInical = ranInical;
    }

    public String getRanFinal() {
        return ranFinal;
    }

    public void setRanFinal(String ranFinal) {
        this.ranFinal = ranFinal;
    }
    
    public String getNombreConcepto()
    {
      return nombreConcepto;
    }

    public void setNombreConcepto(String nombreConcepto)
    {
      this.nombreConcepto = nombreConcepto;
    }

    public Integer getIdConcepto()
    {
      return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto)
    {
      this.idConcepto = idConcepto;
    }

    public String getValorPeriodos() {
        return valorPeriodos;
    }

    public void setValorPeriodos(String valorPeriodos) {
        this.valorPeriodos = valorPeriodos;
    }

    @Override
    public VrtaVarteraprDTO validar()
            throws AplicacionExcepcion
    {
      return null;
    }
}
