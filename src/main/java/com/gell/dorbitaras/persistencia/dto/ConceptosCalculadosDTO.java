/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class ConceptosCalculadosDTO extends Entidad {
    
    private Double valor;
    private Boolean conDinc;
    private String conAlias;
   
    public ConceptosCalculadosDTO() {
    }

    public ConceptosCalculadosDTO(Double valor, Boolean conDinc, String conAlias) {
        this.valor = valor;
        this.conDinc = conDinc;
        this.conAlias = conAlias;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Boolean getConDinc() {
        return conDinc;
    }

    public void setConDinc(Boolean conDinc) {
        this.conDinc = conDinc;
    }

    public String getConAlias() {
        return conAlias;
    }

    public void setConAlias(String conAlias) {
        this.conAlias = conAlias;
    }

    @Override
    public ConceptosCalculadosDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
