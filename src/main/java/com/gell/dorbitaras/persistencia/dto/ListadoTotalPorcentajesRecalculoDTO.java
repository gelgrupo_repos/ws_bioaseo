 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */


public class ListadoTotalPorcentajesRecalculoDTO extends Entidad {

    private Double sinAjuste;
    private Double ajustado;
    private Double Diferencia;
    private Double Participacion;
    private String nombreAsociacion;
    private Integer conIderegistro;

    public ListadoTotalPorcentajesRecalculoDTO() {
    }

    public ListadoTotalPorcentajesRecalculoDTO(Double sinAjuste, Double ajustado, Double Diferencia, Double Participacion, String nombreAsociacion) {
        this.sinAjuste = sinAjuste;
        this.ajustado = ajustado;
        this.Diferencia = Diferencia;
        this.Participacion = Participacion;
        this.nombreAsociacion = nombreAsociacion;
    }
    
    public ListadoTotalPorcentajesRecalculoDTO(Double sinAjuste, Double ajustado, Double Diferencia, Double Participacion, String nombreAsociacion, Integer conIderegistro) {
        this.sinAjuste = sinAjuste;
        this.ajustado = ajustado;
        this.Diferencia = Diferencia;
        this.Participacion = Participacion;
        this.nombreAsociacion = nombreAsociacion;
        this.conIderegistro = conIderegistro;
    }

    public Double getSinAjuste() {
        return sinAjuste;
    }

    public void setSinAjuste(Double sinAjuste) {
        this.sinAjuste = sinAjuste;
    }

    public Double getAjustado() {
        return ajustado;
    }

    public void setAjustado(Double ajustado) {
        this.ajustado = ajustado;
    }

    public Double getDiferencia() {
        return Diferencia;
    }

    public void setDiferencia(Double Diferencia) {
        this.Diferencia = Diferencia;
    }

    public Double getParticipacion() {
        return Participacion;
    }

    public void setParticipacion(Double Participacion) {
        this.Participacion = Participacion;
    }

    public String getNombreAsociacion() {
        return nombreAsociacion;
    }

    public void setNombreAsociacion(String nombreAsociacion) {
        this.nombreAsociacion = nombreAsociacion;
    }

    public Integer getConIderegistro() {
        return conIderegistro;
    }

    public void setConIderegistro(Integer conIderegistro) {
        this.conIderegistro = conIderegistro;
    }
    
    

    @Override
    public ListadoTotalPorcentajesRecalculoDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
