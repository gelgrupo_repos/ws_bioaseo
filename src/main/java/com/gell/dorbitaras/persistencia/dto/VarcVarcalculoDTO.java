/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class VarcVarcalculoDTO extends Entidad
{

  private Integer idVcalculo ;
  private Integer idConcepto ;
  private Integer idRango ;
  private String nombreConcepto ;
  private String fecGrabacion ;
  private String valor ;
  private String estado ;
  private String observacion ;
  private String ranInical ;
  private String ranFinal ;
  private String valorAnt ;

    public Integer getIdVcalculo() {
        return idVcalculo;
    }

    public void setIdVcalculo(Integer idVcalculo) {
        this.idVcalculo = idVcalculo;
    }

    public Integer getIdRango() {
        return idRango;
    }

    public void setIdRango(Integer idRango) {
        this.idRango = idRango;
    }

    public String getFecGrabacion() {
        return fecGrabacion;
    }

    public void setFecGrabacion(String fecGrabacion) {
        this.fecGrabacion = fecGrabacion;
    }    

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getValorAnt() {
        return valorAnt;
    }

    public void setValorAnt(String valorAnt) {
        this.valorAnt = valorAnt;
    }
    
    

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getRanInical() {
        return ranInical;
    }

    public void setRanInical(String ranInical) {
        this.ranInical = ranInical;
    }

    public String getRanFinal() {
        return ranFinal;
    }

    public void setRanFinal(String ranFinal) {
        this.ranFinal = ranFinal;
    }
    
    public String getNombreConcepto()
    {
      return nombreConcepto;
    }

    public void setNombreConcepto(String nombreConcepto)
    {
      this.nombreConcepto = nombreConcepto;
    }

    public Integer getIdConcepto()
    {
      return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto)
    {
      this.idConcepto = idConcepto;
    }

    public String getObservacion()
    {
      return observacion;
    }

    public void setObservacion(String observacion)
    {
      this.observacion = observacion;
    }

    @Override
    public VrtaVarteraprDTO validar()
            throws AplicacionExcepcion
    {
      return null;
    }
}
