/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto.facturar;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author billionaire
 */
public class FragmentoDTO {

    private String tipo;
    private String valor;
    private String idconcepto;
    private String id;

    public String getIdconcepto() {
        return idconcepto;
    }

    public void setIdconcepto(String idconcepto) {
        this.idconcepto = idconcepto;
    }

    public String getTipo() {
        return tipo;
    }

    public FragmentoDTO setTipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public String getValor() {
        return valor;
    }

    public FragmentoDTO setValor(String valor) {
        this.valor = valor;
        return this;
    }

    public String getId() {
        return id;
    }

    public FragmentoDTO setId(String id) {
        this.id = id;
        return this;
    }

}
