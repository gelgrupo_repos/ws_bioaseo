/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto.facturar;

import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.estandar.util.FuncionesDatoUtil;

/**
 *
 * @author billionaire
 */
public class ValorConceptoDTO
{

  private ConConcepto concepto;
  private Double valorTotal;
  private Double cantidad;
  private Double valorUnitario;
  private Double valorReal;
  private Integer flagSemestrales;

  public ValorConceptoDTO()
  {
  }

  public ValorConceptoDTO(ConConcepto concepto)
  {
    this.concepto = concepto;

  }

  public ConConcepto getConcepto()
  {
    return concepto;
  }

  public ValorConceptoDTO setConcepto(ConConcepto concepto)
  {
    this.concepto = concepto;
    return this;
  }

  public Double getValorTotal()
  {
    return valorTotal;
  }

  public ValorConceptoDTO setValorTotal(Double valor)
  {
    this.valorTotal = valor;
    return this;
  }

  public ValorConceptoDTO redondear()
  {
    valorTotal = redondearValor(valorTotal);
    valorReal = redondearValor(valorReal);
    return this;
  }

  public Double getCantidad()
  {
    return cantidad;
  }

  public ValorConceptoDTO setCantidad(Double cantidad)
  {
    this.cantidad = cantidad;
    return this;
  }

  public Double getValorUnitario()
  {
    return valorUnitario;
  }

  public ValorConceptoDTO setValorUnitario(Double valorUnitario)
  {
    this.valorUnitario = valorUnitario;
    return this;
  }

  public Double getValorReal()
  {
    return valorReal;
  }

  public ValorConceptoDTO setValorReal(Double valorReal)
  {
    this.valorReal = valorReal;
    return this;
  }
  
    public Integer getFlagSemestrales() {
        return flagSemestrales;
    }

    public ValorConceptoDTO setFlagSemestrales(Integer flagSemestrales) {
        this.flagSemestrales = flagSemestrales;
        return this;
    }

  private Double redondearValor(Double numero)
  {
    if (concepto.getConMetajuste() == null || numero == null) {
      return numero;
    }

    switch (concepto.getConMetajuste()) {
      case "R":
        return FuncionesDatoUtil.round(numero, concepto.getConPrecision());
      case "T":
        return Integer.valueOf(numero.intValue()).doubleValue();
      default:
        return numero;

    }
  }

}
