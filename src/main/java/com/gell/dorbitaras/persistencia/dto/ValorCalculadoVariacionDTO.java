/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author jhibarra
 */
public class ValorCalculadoVariacionDTO {
    private Integer ideRegistro;
    private String bandera;
    private Float valor;
    private Integer idConcepto;
    private Integer idPeriodo;

    public ValorCalculadoVariacionDTO() {
    }


    public Integer getIdeRegistro() {
        return ideRegistro;
    }

    public void setIdeRegistro(Integer ideRegistro) {
        this.ideRegistro = ideRegistro;
    }
    
    

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public String getBandera() {
        return bandera;
    }

    public void setBandera(String bandera) {
        this.bandera = bandera;
    }

    public Integer getIdConcepto() {
        return idConcepto;
    }

    public void setIdConcepto(Integer idConcepto) {
        this.idConcepto = idConcepto;
    }

    public Integer getIdPeriodo() {
        return idPeriodo;
    }

    public void setIdPeriodo(Integer idPeriodo) {
        this.idPeriodo = idPeriodo;
    }
    
    
    
}
