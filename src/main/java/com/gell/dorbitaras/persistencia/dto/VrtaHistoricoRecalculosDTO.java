/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class VrtaHistoricoRecalculosDTO extends Entidad {

    private Integer vrtaHistoricoRecalculoIde;
    private Integer numeroActualizacion;
    private Integer varperregIde;
    private Integer periodoHijoIde;

    public VrtaHistoricoRecalculosDTO() {
    }

    public VrtaHistoricoRecalculosDTO(Integer numeroActualizacion, Integer varperregIde, Integer periodoHijoIde) {
        this.numeroActualizacion = numeroActualizacion;
        this.varperregIde = varperregIde;
        this.periodoHijoIde = periodoHijoIde;
    }

    public Integer getVrtaHistoricoRecalculoIde() {
        return vrtaHistoricoRecalculoIde;
    }

    public void setVrtaHistoricoRecalculoIde(Integer vrtaHistoricoRecalculoIde) {
        this.vrtaHistoricoRecalculoIde = vrtaHistoricoRecalculoIde;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getVarperregIde() {
        return varperregIde;
    }

    public void setVarperregIde(Integer varperregIde) {
        this.varperregIde = varperregIde;
    }

    public Integer getPeriodoHijoIde() {
        return periodoHijoIde;
    }

    public void setPeriodoHijoIde(Integer periodoHijoIde) {
        this.periodoHijoIde = periodoHijoIde;
    }

    @Override
    public VrtaHistoricoRecalculosDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
