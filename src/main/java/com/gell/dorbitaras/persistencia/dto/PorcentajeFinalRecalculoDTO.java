/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class PorcentajeFinalRecalculoDTO extends Entidad {

    private Double vrtaPromValor;
    private Integer numeroActualizacion;
    private Integer terIdeRegistro;
    private Integer conIderegistro;

    public PorcentajeFinalRecalculoDTO() {
    }

    public PorcentajeFinalRecalculoDTO(Double vrtaPromValor, Integer numeroActualizacion, Integer terIdeRegistro) {
        this.vrtaPromValor = vrtaPromValor;
        this.numeroActualizacion = numeroActualizacion;
        this.terIdeRegistro = terIdeRegistro;
    }
    
    public PorcentajeFinalRecalculoDTO(Double vrtaPromValor, Integer numeroActualizacion, Integer terIdeRegistro, Integer conIderegistro) {
        this.vrtaPromValor = vrtaPromValor;
        this.numeroActualizacion = numeroActualizacion;
        this.terIdeRegistro = terIdeRegistro;
        this.conIderegistro = conIderegistro;
    }

    public Double getVrtaPromValor() {
        return vrtaPromValor;
    }

    public void setVrtaPromValor(Double vrtaPromValor) {
        this.vrtaPromValor = vrtaPromValor;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Integer getTerIdeRegistro() {
        return terIdeRegistro;
    }

    public void setTerIdeRegistro(Integer terIdeRegistro) {
        this.terIdeRegistro = terIdeRegistro;
    }

    public Integer getConIderegistro() {
        return conIderegistro;
    }

    public void setConIderegistro(Integer conIderegistro) {
        this.conIderegistro = conIderegistro;
    }

    @Override
    public PorcentajeFinalRecalculoDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
