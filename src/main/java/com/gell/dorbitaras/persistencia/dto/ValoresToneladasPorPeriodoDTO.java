/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author Jhon Jairo Ibarra
 */
public class ValoresToneladasPorPeriodoDTO {
    private Double valor;
    private Integer idPeriodoPadre;

    public ValoresToneladasPorPeriodoDTO() {
    }

    public ValoresToneladasPorPeriodoDTO(Double valor, Integer idPeriodoPadre) {
        this.valor = valor;
        this.idPeriodoPadre = idPeriodoPadre;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Integer getIdPeriodoPadre() {
        return idPeriodoPadre;
    }

    public void setIdPeriodoPadre(Integer idPeriodoPadre) {
        this.idPeriodoPadre = idPeriodoPadre;
    }
    
    
}
