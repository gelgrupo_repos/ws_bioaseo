/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto.facturar;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author billionaire
 */
public class ArgumentoDTO
{

  private final Map<String, Object> parametros = new HashMap<>();

  public ArgumentoDTO()
  {
  }

  public ArgumentoDTO(String nombre, Object valor)
  {
    parametros.put(nombre, valor);

  }

  public ArgumentoDTO agregar(String nombre, Object valor)
  {
    parametros.put(nombre, valor);
    return this;
  }

  public ArgumentoDTO limpiar()
  {
    parametros.clear();
    return this;
  }

  public Map<String, Object> getParametros()
  {
    return parametros;
  }

  public Object get(String nombre)
  {
    return parametros.get(nombre);
  }

  public Integer getEntero(String nombre)
  {
    return (Integer) parametros.get(nombre);
  }

  public Long getLargo(String nombre)
  {
    return (Long) parametros.get(nombre);
  }

  public Double getDoble(String nombre)
  {
    return (Double) parametros.get(nombre);
  }

  public String getTexto(String nombre)
  {
    return String.valueOf(parametros.get(nombre));
  }

}
