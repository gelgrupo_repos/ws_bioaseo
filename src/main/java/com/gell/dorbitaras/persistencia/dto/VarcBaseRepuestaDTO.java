/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;
import java.util.List;

/**
 *
 * @author Desarrollador
 */
public class VarcBaseRepuestaDTO extends Entidad
{


  private List<VarcVarcalculoBaseDTO> listaVariables ;
  private List<VarcVarcalculoBaseDTO> listaVarApr ;
  private String periodos ;

    public List<VarcVarcalculoBaseDTO> getListaVariables() {
        return listaVariables;
    }

    public void setListaVariables(List<VarcVarcalculoBaseDTO> listaVariables) {
        this.listaVariables = listaVariables;
    }  

    public List<VarcVarcalculoBaseDTO> getListaVarApr() {
        return listaVarApr;
    }

    public void setListaVarApr(List<VarcVarcalculoBaseDTO> listaVarApr) {
        this.listaVarApr = listaVarApr;
    }
    
    public String getPeriodos() {
        return periodos;
    }

    public void setPeriodos(String periodos) {
        this.periodos = periodos;
    }

  
    @Override
    public VrtaVarteraprDTO validar()
            throws AplicacionExcepcion
    {
      return null;
    }
}
