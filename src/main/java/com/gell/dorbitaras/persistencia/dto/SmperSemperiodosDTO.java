/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class SmperSemperiodosDTO extends Entidad
{

  Integer idSemestre;
  Integer idRegimen;

  public Integer getIdSemestre()
  {
    return idSemestre;
  }

  public void setIdSemestre(Integer idSemestre)
  {
    this.idSemestre = idSemestre;
  }

  public Integer getIdRegimen()
  {
    return idRegimen;
  }

  public void setIdRegimen(Integer idRegimen)
  {
    this.idRegimen = idRegimen;
  }

  @Override
  public VrtaVarteraprDTO validar()
          throws AplicacionExcepcion
  {
    return null;
  }
}
