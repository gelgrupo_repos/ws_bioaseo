/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto.facturar;

/**
 *
 * @author billionaire
 */
public class ValorFuncionDTO
{

  private Double valorTotal;
  private Double valorUnitario;
  private String tablaOrigen;
  private Integer uniConcepto;
  private Boolean liquidaServicio;

  public ValorFuncionDTO()
  {
  }

  public Double getValorTotal()
  {
    return valorTotal;
  }

  public ValorFuncionDTO setValorTotal(Double valorTotal)
  {
    this.valorTotal = valorTotal;
    return this;
  }

  public Double getValorUnitario()
  {
    return valorUnitario;
  }

  public ValorFuncionDTO setValorUnitario(Double valorUnitario)
  {
    this.valorUnitario = valorUnitario;
    return this;
  }

  public String getTablaOrigen()
  {
    return tablaOrigen;
  }

  public ValorFuncionDTO setTablaOrigen(String tablaOrigen)
  {
    this.tablaOrigen = tablaOrigen;
    return this;
  }

  public Integer getUniConcepto()
  {
    return uniConcepto;
  }

  public ValorFuncionDTO setUniConcepto(Integer uniConcepto)
  {
    this.uniConcepto = uniConcepto;
    return this;
  }

  public Boolean getLiquidaServicio()
  {
    return liquidaServicio;
  }

  public ValorFuncionDTO setLiquidaServicio(Boolean liquidaServicio)
  {
    this.liquidaServicio = liquidaServicio;
    return this;
  }

}
