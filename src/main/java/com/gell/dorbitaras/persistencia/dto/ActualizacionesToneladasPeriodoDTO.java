/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author Jhon Jairo Ibarra
 */
public class ActualizacionesToneladasPeriodoDTO {
    
    private Integer numeroActualizacion;
    private Double vrtaValor;
    private Integer vrtaIdeRegistro;
    private Integer perIdeRegistro;
    private Integer smperNumero;
    private Long idAsociacion;

    public ActualizacionesToneladasPeriodoDTO() {
    }

        
    public ActualizacionesToneladasPeriodoDTO(Integer numeroActualizacion, Double vrtaValor, Integer vrtaIdeRegistro, Integer perIdeRegistro, Integer smperNumero, Long idAsociacion) {
        this.numeroActualizacion = numeroActualizacion;
        this.vrtaValor = vrtaValor;
        this.vrtaIdeRegistro = vrtaIdeRegistro;
        this.perIdeRegistro = perIdeRegistro;
        this.smperNumero = smperNumero;
        this.idAsociacion = idAsociacion;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public Double getVrtaValor() {
        return vrtaValor;
    }

    public void setVrtaValor(Double vrtaValor) {
        this.vrtaValor = vrtaValor;
    }

    public Integer getVrtaIdeRegistro() {
        return vrtaIdeRegistro;
    }

    public void setVrtaIdeRegistro(Integer vrtaIdeRegistro) {
        this.vrtaIdeRegistro = vrtaIdeRegistro;
    }

    public Integer getPerIdeRegistro() {
        return perIdeRegistro;
    }

    public void setPerIdeRegistro(Integer perIdeRegistro) {
        this.perIdeRegistro = perIdeRegistro;
    }

    public Integer getSmperNumero() {
        return smperNumero;
    }

    public void setSmperNumero(Integer smperNumero) {
        this.smperNumero = smperNumero;
    }

    public Long getIdAsociacion() {
        return idAsociacion;
    }

    public void setIdAsociacion(Long idAsociacion) {
        this.idAsociacion = idAsociacion;
    }
    
    
    

}
