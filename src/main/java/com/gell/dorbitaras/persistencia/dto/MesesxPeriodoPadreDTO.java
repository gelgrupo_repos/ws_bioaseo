/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author Jhon Jairo Ibarra
 */
public class MesesxPeriodoPadreDTO {
    private Integer perIderegistro;
    private Integer perIdepadre;
    private Integer smperNumero;

    public MesesxPeriodoPadreDTO() {
    }

    public MesesxPeriodoPadreDTO(Integer perIderegistro, Integer perIdepadre, Integer smperNumero) {
        this.perIderegistro = perIderegistro;
        this.perIdepadre = perIdepadre;
        this.smperNumero = smperNumero;
    }

    public Integer getPerIderegistro() {
        return perIderegistro;
    }

    public void setPerIderegistro(Integer smperIderegistro) {
        this.perIderegistro = smperIderegistro;
    }

    public Integer getPerIdepadre() {
        return perIdepadre;
    }

    public void setPerIdepadre(Integer perIdepadre) {
        this.perIdepadre = perIdepadre;
    }

    public Integer getPerNumero() {
        return smperNumero;
    }

    public void setPerNumero(Integer smperNumero) {
        this.smperNumero = smperNumero;
    }
    
    
}
