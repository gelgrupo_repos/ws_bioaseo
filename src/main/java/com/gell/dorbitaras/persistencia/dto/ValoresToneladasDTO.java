/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author Jhon Jairo Ibarra
 */
public class ValoresToneladasDTO {

    private Integer idVrtaIdRegistro;
    private Double valorToneladas;
    private String observacion;
    private String fechaCertificacion;
    private String estado;
    private Integer numeroActualizacion;
    private String comercializacion;

    public ValoresToneladasDTO() {
    }

    public Integer getIdVrtaIdRegistro() {
        return idVrtaIdRegistro;
    }

    public void setIdVrtaIdRegistro(Integer idVrtaIdRegistro) {
        this.idVrtaIdRegistro = idVrtaIdRegistro;
    }

    public Double getValorToneladas() {
        return valorToneladas;
    }

    public void setValorToneladas(Double valorToneladas) {
        this.valorToneladas = valorToneladas;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getFechaCertificacion() {
        return fechaCertificacion;
    }

    public void setFechaCertificacion(String fechaCertificacion) {
        this.fechaCertificacion = fechaCertificacion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public String getComercializacion() {
        return comercializacion;
    }

    public void setComercializacion(String comercializacion) {
        this.comercializacion = comercializacion;
    }
    
}
