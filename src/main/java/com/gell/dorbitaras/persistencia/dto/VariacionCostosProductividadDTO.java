/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

/**
 *
 * @author jhibarra
 */
public class VariacionCostosProductividadDTO {
    private Integer idRegistro;
    private Float valor;
    private Integer perIdRegistro;
    private String accessor;
    private String header;
    private String bandera;
    private Integer varceIdConceptoAplicado;

    public VariacionCostosProductividadDTO() {
    }

    public VariacionCostosProductividadDTO(Integer idRegistro, Float valor, Integer smperIdRegistro, String accessor, String header, String bandera,Integer varceIdConceptoAplicado) {
        this.idRegistro = idRegistro;
        this.valor = valor;
        this.perIdRegistro = perIdRegistro;
        this.accessor = accessor;
        this.header = header;
        this.bandera = bandera;
        this.varceIdConceptoAplicado = varceIdConceptoAplicado;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public Integer getPerIdRegistro() {
        return perIdRegistro;
    }

    public void setPerIdRegistro(Integer perIdRegistro) {
        this.perIdRegistro = perIdRegistro;
    }


    public String getAccessor() {
        return accessor;
    }

    public void setAccessor(String accessor) {
        this.accessor = accessor;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBandera() {
        return bandera;
    }

    public void setBandera(String bandera) {
        this.bandera = bandera;
    }

    public Integer getVarceIdConceptoAplicado() {
        return varceIdConceptoAplicado;
    }

    public void setVarceIdConceptoAplicado(Integer varceIdConceptoAplicado) {
        this.varceIdConceptoAplicado = varceIdConceptoAplicado;
    }
    
    
}
