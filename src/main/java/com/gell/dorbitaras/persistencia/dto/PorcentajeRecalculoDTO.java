/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto;

import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.Entidad;

/**
 *
 * @author Desarrollador
 */
public class PorcentajeRecalculoDTO extends Entidad {

    private Double vrtaPromValor;
    private Integer numeroActualizacion;
    private String terNomcompleto;

    public PorcentajeRecalculoDTO() {
    }

    public PorcentajeRecalculoDTO(Double vrtaPromValor, Integer numeroActualizacion, String terNomcompleto) {
        this.vrtaPromValor = vrtaPromValor;
        this.numeroActualizacion = numeroActualizacion;
        this.terNomcompleto = terNomcompleto;
    }

    public Double getVrtaPromValor() {
        return vrtaPromValor;
    }

    public void setVrtaPromValor(Double vrtaPromValor) {
        this.vrtaPromValor = vrtaPromValor;
    }

    public Integer getNumeroActualizacion() {
        return numeroActualizacion;
    }

    public void setNumeroActualizacion(Integer numeroActualizacion) {
        this.numeroActualizacion = numeroActualizacion;
    }

    public String getTerNomcompleto() {
        return terNomcompleto;
    }

    public void setTerNomcompleto(String terNomcompleto) {
        this.terNomcompleto = terNomcompleto;
    }

    @Override
    public PorcentajeRecalculoDTO validar()
            throws AplicacionExcepcion {
        return null;
    }

}
