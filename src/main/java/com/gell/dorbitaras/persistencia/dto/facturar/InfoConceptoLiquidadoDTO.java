/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dto.facturar;

/**
 *
 * @author billionaire
 */
public class InfoConceptoLiquidadoDTO
{

  private Integer uniConcepto;
  private Double valorTotal;
  private Double valorUnitario;
  private String tablaOrigen;
  private Boolean liquidaServicio;

  public Integer getUniConcepto()
  {
    return uniConcepto;
  }

  public InfoConceptoLiquidadoDTO setUniConcepto(Integer uniConcepto)
  {
    this.uniConcepto = uniConcepto;
    return this;
  }

  public Double getValorTotal()
  {
    return valorTotal;
  }

  public InfoConceptoLiquidadoDTO setValorTotal(Double valorTotal)
  {
    this.valorTotal = valorTotal;
    return this;
  }

  public Double getValorUnitario()
  {
    return valorUnitario;
  }

  public InfoConceptoLiquidadoDTO setValorUnitario(Double valorUnitario)
  {
    this.valorUnitario = valorUnitario;
    return this;
  }

  public String getTablaOrigen()
  {
    return tablaOrigen;
  }

  public InfoConceptoLiquidadoDTO setTablaOrigen(String tablaOrigen)
  {
    this.tablaOrigen = tablaOrigen;
    return this;
  }

  public Boolean getLiquidaServicio()
  {
    return liquidaServicio;
  }

  public InfoConceptoLiquidadoDTO setLiquidaServicio(Boolean liquidaServicio)
  {
    this.liquidaServicio = liquidaServicio;
    return this;
  }

}
