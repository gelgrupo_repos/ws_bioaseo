/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.constante;

import com.gell.estandar.plantilla.IGenericoMensaje;

/**
 *
 * @author god
 */
public enum EMensajePersistencia implements IGenericoMensaje
{

  OK(1, "Se ejecutó correctamente"),
  EDICION_OK(1, "Se hizo la edición"),
  NO_RESULTADOS(0, "No se encontraron  registros"),
  ERROR_CONEXION_BD(-1, "No hay conexión con la base de datos"),
  ERROR_CONSULTAR(-2, "Error al consultar el registro"),
  ERROR_MODIFICAR(-3, "Error al modificar el registro"),
  ERROR_COLUMNA_NO_ENCONTRADA(-4, "No se encontró la columna"),
  ERROR_INSERTAR(-4, "Error al insertar el registro"),
  ERROR_CONSULTAR_TERCERO(-4, "Error al consultar los terceros"),
  ERROR_EDITAR(-5, "Error al editar"),
  ERROR_NO_ENCONTRADA_ESTRUCTURA(-6, "Error la estructura no fue encontrada o no está parametrizada para la empresa "),
  ERROR_UNIDAD_NOMBRE(-10, "Error el nombre ya existe"),
  ERROR_REGISTRO_VERSION(-12, "El registro fue modificado por otro proceso"),
  ERROR_CONFIRMAR(-13, "Error al confirmar los cambios en la base de datos"),
  ERROR_CONSULTAR_UNIDAD(-15, "No hay datos de la unidad ingresado"),
  ERROR_CONSULTAR_PUNTO_CONSUMO(-12, "No hay datos del código gestor ingresado"),
  ERROR_INGRESO_FECHA(-22, "La fecha ingresada no debe pasar la fecha actual"),
  ERROR_FORMATO_FECHA(-23, "La fecha ingresada debe esta en el formato Año-Mes"),
  ERROR_FECHA_FINAL(-24, "La fecha final ingresada no debe ser menor a la fecha inicial"),
  ERROR_ESTADO_NO_ENCONTRADO(-25, "Error el estado no se encontró "),
  ERROR_UNIDAD(-31, "Error al consultar la unidad "),
  ERROR_CONCEPTO_RELACIONADO(-32, "No se encontró información relacionada con la variable __COMPLEMENTO__"),
  ERROR_FUNCION(-33, "Error al ejecutar la función __COMPLEMENTO__ "),
  ERROR_VARIABLE_EMPRESA(-34, "Error no se encontré la variable con abreviatura __COMPLEMENTO__"),
  ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO(-35, "Se encontraron variables sin certificar en el periodo __COMPLEMENTO__"),
  NO_RESULTADOS_PERIODOS_SEMESTRE_SIGUIENTE(-36, "No se encontraron meses parametrizados para el siguiente semestre"),
  ERROR_PERMISO(-45, "No tiene permiso al programa solicitado");

  /**
   * Código de respuesta
   */
  private final int codigo;

  /**
   * Descripción del mensaje o el evento que sucedió
   */
  private final String mensaje;

  private EMensajePersistencia(int codigo, String mensaje)
  {
    this.codigo = codigo;
    this.mensaje = mensaje;
  }

  @Override
  public int getCodigo()
  {
    return codigo;
  }

  @Override
  public String getMensaje()
  {
    return mensaje;
  }

}
