package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import java.util.Date;

public class VrtaVarteraprHistCRUD extends GenericoCRUD
{

  public VrtaVarteraprHistCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(VrtaVarteraprHist vrtaVarteraprHist)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.vrta_varterapr_hist(numero_actualizacion,vrta_ideregistro,vrta_hist_fecgrabacion) VALUES (?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object numeroActualizacion = (vrtaVarteraprHist.getNumeroActualizacion() == null) ? null : vrtaVarteraprHist.getNumeroActualizacion();
      sentencia.setObject(i++, numeroActualizacion);
      Object vrtaIderegistro = (vrtaVarteraprHist.getVrtaIderegistro() == null) ? null : vrtaVarteraprHist.getVrtaIderegistro();
      sentencia.setObject(i++, vrtaIderegistro);
      Object vrtaHistFecgrabacion = (vrtaVarteraprHist.getVrtaHistFecgrabacion() == null) ? null : vrtaVarteraprHist.getVrtaHistFecgrabacion();
      sentencia.setObject(i++, vrtaHistFecgrabacion);
      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(VrtaVarteraprHist vrtaVarteraprHist)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.vrta_varterapr_hist SET vrta_hist_ideregistro=?,numero_actualizacion=?,vrta_ideregistro=?,vrta_hist_fecgrabacion=?";
           sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object vrtaHistIderegistro = (vrtaVarteraprHist.getVrtaHistIderegistro() == null) ? null : vrtaVarteraprHist.getVrtaHistIderegistro();
      sentencia.setObject(i++, vrtaHistIderegistro);
      Object numeroActualizacion = (vrtaVarteraprHist.getNumeroActualizacion() == null) ? null : vrtaVarteraprHist.getNumeroActualizacion();
      sentencia.setObject(i++, numeroActualizacion);
      Object vrtaIderegistro = (vrtaVarteraprHist.getVrtaIderegistro() == null) ? null : vrtaVarteraprHist.getVrtaIderegistro();
      sentencia.setObject(i++, vrtaIderegistro);
      Object vrtaHistFecgrabacion = (vrtaVarteraprHist.getVrtaHistFecgrabacion() == null) ? null : vrtaVarteraprHist.getVrtaHistFecgrabacion();
      sentencia.setObject(i++, vrtaHistFecgrabacion);

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<VrtaVarteraprHist> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<VrtaVarteraprHist> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.vrta_varterapr_hist";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getVrtaVarteraprHist(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public VrtaVarteraprHist consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    VrtaVarteraprHist obj = null;
    try {

      String sql = "SELECT * FROM aseo.vrta_varterapr_hist WHERE vrta_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getVrtaVarteraprHist(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static VrtaVarteraprHist getVrtaVarteraprHist(ResultSet rs)
          throws PersistenciaExcepcion
  {
    VrtaVarteraprHist vrtaVarteraprHist = new VrtaVarteraprHist();
    vrtaVarteraprHist.setVrtaIderegistro(getObject("vrta_ideregistro", Integer.class, rs));
    vrtaVarteraprHist.setNumeroActualizacion(getObject("numero_actualizacion", Integer.class, rs));
    vrtaVarteraprHist.setVrtaHistIderegistro(getObject("vrta_hist_ideregistro", Integer.class, rs));
    vrtaVarteraprHist.setVrtaHistFecgrabacion(getObject("vrta_hist_fecgrabacion", Timestamp.class, rs));
    return vrtaVarteraprHist;
  }

  public static VrtaVarteraprHist getVrtaVarteraprHist(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    VrtaVarteraprHist vrtaVarteraprHist = new VrtaVarteraprHist();
    Integer columna = columnas.get(alias + "_vrta_hist_ideregistro");
    if (columna != null) {
      vrtaVarteraprHist.setVrtaHistIderegistro(getObject(columna, Integer.class, rs));
    }
    
    columna = columnas.get(alias + "_numero_actualizacion");
    if (columna != null) {
      vrtaVarteraprHist.setNumeroActualizacion(getObject(columna, Integer.class, rs));
    }

    columna = columnas.get(alias + "_vrta_ideregistro");
    if (columna != null) {
      vrtaVarteraprHist.setVrtaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_vrta_hist_fecgrabacion");
    if (columna != null) {
      vrtaVarteraprHist.setVrtaHistFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    
    return vrtaVarteraprHist;
  }

}
