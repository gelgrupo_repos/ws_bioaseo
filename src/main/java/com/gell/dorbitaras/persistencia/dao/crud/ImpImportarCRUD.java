package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class ImpImportarCRUD extends GenericoCRUD {


    public ImpImportarCRUD(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    
    public void insertar(ImpImportar impImportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO aseo.imp_importar(imp_descripcion,imp_rutaarchivo,imp_nombreprimerfila,imp_delimitacion,imp_seperadorregistro,imp_separadorcampo,emp_ideregistsro,usu_ideregistro,fecha_registro) VALUES (?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++,impImportar.getImpDescripcion()); 
sentencia.setObject(i++,impImportar.getImpRutaarchivo()); 
sentencia.setObject(i++,impImportar.getImpNombreprimerfila()); 
sentencia.setObject(i++,impImportar.getImpDelimitacion()); 
sentencia.setObject(i++,impImportar.getImpSeperadorregistro()); 
sentencia.setObject(i++,impImportar.getImpSeparadorcampo()); 
sentencia.setObject(i++,impImportar.getEmpIderegistsro()); 
sentencia.setObject(i++,impImportar.getUsuIderegistro()); 
sentencia.setObject(i++,DateUtil.parseTimestamp(impImportar.getFechaRegistro())); 
 
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                impImportar.setImpIderegistro(rs.getLong("imp_ideregistro"));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void insertarTodos(ImpImportar impImportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO aseo.imp_importar(imp_ideregistro,imp_descripcion,imp_rutaarchivo,imp_nombreprimerfila,imp_delimitacion,imp_seperadorregistro,imp_separadorcampo,emp_ideregistsro,usu_ideregistro,fecha_registro) VALUES (?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,impImportar.getImpIderegistro()); 
sentencia.setObject(i++,impImportar.getImpDescripcion()); 
sentencia.setObject(i++,impImportar.getImpRutaarchivo()); 
sentencia.setObject(i++,impImportar.getImpNombreprimerfila()); 
sentencia.setObject(i++,impImportar.getImpDelimitacion()); 
sentencia.setObject(i++,impImportar.getImpSeperadorregistro()); 
sentencia.setObject(i++,impImportar.getImpSeparadorcampo()); 
sentencia.setObject(i++,impImportar.getEmpIderegistsro()); 
sentencia.setObject(i++,impImportar.getUsuIderegistro()); 
sentencia.setObject(i++,DateUtil.parseTimestamp(impImportar.getFechaRegistro())); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
          LogUtil.error(e);
          throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void editar(ImpImportar impImportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "UPDATE aseo.imp_importar SET imp_descripcion=?,imp_rutaarchivo=?,imp_nombreprimerfila=?,imp_delimitacion=?,imp_seperadorregistro=?,imp_separadorcampo=?,emp_ideregistsro=?,usu_ideregistro=?,fecha_registro=? where imp_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,impImportar.getImpDescripcion()); 
sentencia.setObject(i++,impImportar.getImpRutaarchivo()); 
sentencia.setObject(i++,impImportar.getImpNombreprimerfila()); 
sentencia.setObject(i++,impImportar.getImpDelimitacion()); 
sentencia.setObject(i++,impImportar.getImpSeperadorregistro()); 
sentencia.setObject(i++,impImportar.getImpSeparadorcampo()); 
sentencia.setObject(i++,impImportar.getEmpIderegistsro()); 
sentencia.setObject(i++,impImportar.getUsuIderegistro()); 
sentencia.setObject(i++,DateUtil.parseTimestamp(impImportar.getFechaRegistro())); 
sentencia.setObject(i++,impImportar.getImpIderegistro()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
             desconectar(sentencia);
        }
    }

    
    public List<ImpImportar> consultar() throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<ImpImportar> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.imp_importar";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getImpImportar(rs));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
             desconectar(sentencia);
        }
        return lista;

    }

    
    public ImpImportar consultar(long id) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        ImpImportar obj = null;
        try {

            String sql = "SELECT * FROM aseo.imp_importar WHERE imp_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getImpImportar(rs);
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
              desconectar(sentencia);
        }
        return obj;
    }

    public static ImpImportar getImpImportar(ResultSet rs) throws PersistenciaExcepcion{
      ImpImportar impImportar = new ImpImportar();
impImportar.setImpIderegistro( getObject("imp_ideregistro",Long.class,rs));
impImportar.setImpDescripcion( getObject("imp_descripcion",String.class,rs));
impImportar.setImpRutaarchivo( getObject("imp_rutaarchivo",String.class,rs));
impImportar.setImpNombreprimerfila( getObject("imp_nombreprimerfila",Boolean.class,rs));
impImportar.setImpDelimitacion( getObject("imp_delimitacion",Integer.class,rs));
impImportar.setImpSeperadorregistro( getObject("imp_seperadorregistro",String.class,rs));
impImportar.setImpSeparadorcampo( getObject("imp_separadorcampo",String.class,rs));
impImportar.setEmpIderegistsro( getObject("emp_ideregistsro",Integer.class,rs));
impImportar.setUsuIderegistro( getObject("usu_ideregistro",Long.class,rs));
impImportar.setFechaRegistro( getObject("fecha_registro",Timestamp.class,rs));

      return impImportar;
    }

    public static void getImpImportar(ResultSet rs,Map<String, Integer> columnas,ImpImportar impImportar) throws PersistenciaExcepcion{
      Integer columna = columnas.get("imp_importar_imp_ideregistro");if( columna != null ){impImportar.setImpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get("imp_importar_imp_descripcion");if( columna != null ){impImportar.setImpDescripcion( getObject(columna,String.class,rs));
} columna = columnas.get("imp_importar_imp_rutaarchivo");if( columna != null ){impImportar.setImpRutaarchivo( getObject(columna,String.class,rs));
} columna = columnas.get("imp_importar_imp_nombreprimerfila");if( columna != null ){impImportar.setImpNombreprimerfila( getObject(columna,Boolean.class,rs));
} columna = columnas.get("imp_importar_imp_delimitacion");if( columna != null ){impImportar.setImpDelimitacion( getObject(columna,Integer.class,rs));
} columna = columnas.get("imp_importar_imp_seperadorregistro");if( columna != null ){impImportar.setImpSeperadorregistro( getObject(columna,String.class,rs));
} columna = columnas.get("imp_importar_imp_separadorcampo");if( columna != null ){impImportar.setImpSeparadorcampo( getObject(columna,String.class,rs));
} columna = columnas.get("imp_importar_emp_ideregistsro");if( columna != null ){impImportar.setEmpIderegistsro( getObject(columna,Integer.class,rs));
} columna = columnas.get("imp_importar_usu_ideregistro");if( columna != null ){impImportar.setUsuIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get("imp_importar_fecha_registro");if( columna != null ){impImportar.setFechaRegistro( getObject(columna,Timestamp.class,rs));
}
    }
    
    public static void getImpImportar(ResultSet rs,Map<String, Integer> columnas,ImpImportar impImportar,String alias) throws PersistenciaExcepcion{
      Integer columna = columnas.get(alias+"_imp_ideregistro");if( columna != null ){impImportar.setImpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get(alias+"_imp_descripcion");if( columna != null ){impImportar.setImpDescripcion( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_imp_rutaarchivo");if( columna != null ){impImportar.setImpRutaarchivo( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_imp_nombreprimerfila");if( columna != null ){impImportar.setImpNombreprimerfila( getObject(columna,Boolean.class,rs));
} columna = columnas.get(alias+"_imp_delimitacion");if( columna != null ){impImportar.setImpDelimitacion( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_imp_seperadorregistro");if( columna != null ){impImportar.setImpSeperadorregistro( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_imp_separadorcampo");if( columna != null ){impImportar.setImpSeparadorcampo( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_emp_ideregistsro");if( columna != null ){impImportar.setEmpIderegistsro( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_usu_ideregistro");if( columna != null ){impImportar.setUsuIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get(alias+"_fecha_registro");if( columna != null ){impImportar.setFechaRegistro( getObject(columna,Timestamp.class,rs));
}
    }

    public static ImpImportar getImpImportar(ResultSet rs,Map<String, Integer> columnas) throws PersistenciaExcepcion{
      ImpImportar impImportar = new  ImpImportar();
      getImpImportar( rs, columnas,impImportar);
      return impImportar;
    }
   
 public static ImpImportar getImpImportar(ResultSet rs,Map<String, Integer> columnas,String alias) throws PersistenciaExcepcion{
      ImpImportar impImportar = new  ImpImportar();
      getImpImportar( rs,columnas, impImportar, alias);
      return impImportar;
    }
    
}
