package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.FuncionesDatoUtil;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class ArprAreaprestacionDAO extends ArprAreaprestacionCRUD
{

  public ArprAreaprestacionDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Método que expone el método de guardarRegimen y si se envía el
   * identificador del régimen se edita
   *
   * @param areaPresentacion Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarArpr(ArprAreaprestacion areaPresentacion)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer arprIderegistro = areaPresentacion.getArprIderegistro();
    arprIderegistro = arprIderegistro == null ? -1 : arprIderegistro;
    ArprAreaprestacion regimenGuardada = consultar(arprIderegistro.longValue());
    if (regimenGuardada != null) {
      this.editar(areaPresentacion);
      return;
    }
    super.insertar(areaPresentacion);
  }

  /**
   * Consulta si el código NUAP ya existe en la base de datos
   *
   * @param nuap Número de identificación
   * @param idRegistro Identificador del registro
   * @return Cantidad de registros
   * @throws PersistenciaExcepcion
   */
  public Integer validaNuap(String nuap, Integer idRegistro)
          throws PersistenciaExcepcion
  {
    idRegistro = idRegistro == null ? -1 : idRegistro;
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT count(*) numero  ")
            .append("         FROM aseo.arpr_areaprestacion arpr ")
            .append("         WHERE arpr.arpr_nuap ILIKE :nuap ")
            .append("AND arpr.emp_ideregistro = :idempresa ")
            .append("AND arpr.arpr_ideregistro <> :idregistro; ");
    parametros.put("nuap", nuap);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("idregistro", idRegistro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Integer cantidad = getObject("numero", Integer.class, rs);
              return cantidad;
            });
  }

  /**
   * Consulta si el código NUSD ya existe en la base de datos
   *
   * @param nusd Número de identificación
   * @param idRegistro Identificador del registro
   * @return Cantidad de registros
   * @throws PersistenciaExcepcion
   */
  public Integer validaNusd(String nusd, Integer idRegistro)
          throws PersistenciaExcepcion
  {
    idRegistro = idRegistro == null ? -1 : idRegistro;
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT count(*) numero  ")
            .append("         FROM aseo.arpr_areaprestacion arpr ")
            .append("         WHERE arpr.arpr_nusd ILIKE :nuap ")
            .append("AND arpr.emp_ideregistro = :idempresa ")
            .append("AND arpr.arpr_ideregistro <> :idregistro; ");
    parametros.put("nuap", nusd);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("idregistro", idRegistro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Integer cantidad = getObject("numero", Integer.class, rs);
              return cantidad;
            });
  }

  /**
   * Consulta si el nombre ya existe en la base de datos
   *
   * @param nombre Nombre del área de prestación
   * @param idRegistro Identificador del registro
   * @return Cantidad de registros
   * @throws PersistenciaExcepcion
   */
  public Integer validarNombre(String nombre, Integer idRegistro)
          throws PersistenciaExcepcion
  {
    idRegistro = idRegistro == null ? -1 : idRegistro;
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT count(*) existente ")
            .append("FROM aseo.arpr_areaprestacion arp ")
            .append("WHERE arp.arpr_nombre ILIKE :nombre ")
            .append("AND arp.emp_ideregistro = :idempresa ")
            .append("AND arp.arpr_ideregistro <> :idregistro; ");
    parametros.put("nombre", nombre);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("idregistro", idRegistro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Integer existente = getObject("existente", Integer.class, rs);
              return existente;
            });
  }

  /**
   * Consulta todas las área de prestación dependiendo el parametro de busqueda
   *
   * @param nombre parametro de búsqueda
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<ArprAreaprestacion> consultar(String nombre)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT arpr.*,dcta.*,rgta.*,  ")
            .append("       to_json((  ")
            .append("         SELECT ARRAY_AGG(estts.*)  ")
            .append("            FROM (SELECT *  ")
            .append("                FROM aseo.etta_estttarifas etta  ")
            .append("            WHERE arpr.arpr_ideregistro = etta.arpr_ideregistro  ")
            .append("                AND etta.etta_swtestado = :estado) AS estts  ")
            .append("         )) estratos,  ")
            .append("       to_json((  ")
            .append("         SELECT ARRAY_AGG(areapro.*)  ")
            .append("            FROM (SELECT appr.*,  ")
            .append("                  to_json((  ")
            .append("                      SELECT ARRAY_AGG(prodatos.*)  ")
            .append("                          FROM (SELECT *  ")
            .append("                             FROM proyectos p  ")
            .append("                        WHERE  appr.proyecto_ideregistro = p.proyecto_ideregistro ) AS prodatos  ")
            .append("                      )) infoproyecto  ")
            .append("                FROM aseo.appr_areaproyecto appr  ")
            .append("            WHERE arpr.arpr_ideregistro = appr.arpr_ideregistro  ")
            .append("                AND appr.appr_swtestado = :estado) AS areapro  ")
            .append("         )) proyectos  ")
            .append("       FROM aseo.arpr_areaprestacion arpr  ")
            .append("          INNER JOIN aseo.dcta_dctotarifas dcta  ")
            .append("            ON dcta.dcta_ideregistro = arpr.dcta_ideregistro  ")
            .append("          INNER JOIN aseo.rgta_rgitarifario rgta  ")
            .append("            ON rgta.rgta_ideregistro = arpr.rgta_ideregistro  ")
            .append("        WHERE arpr.arpr_nombre ILIKE :nombre; ");
    parametros.put("nombre", '%' + nombre + '%');
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ArprAreaprestacion>()
    {
      @Override
      public ArprAreaprestacion siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {

        ArprAreaprestacion area = getArprAreaprestacion(rs, columns, "arpr_areaprestacion1");
        DctaDctotarifas documentos = DctaDctotarifasDAO.getDctaDctotarifas(rs, columns, "dcta_dctotarifas1");
        RgtaRgitarifario regimen = RgtaRgitarifarioDAO.getRgtaRgitarifario(rs, columns, "rgta_rgitarifario1");
        String estratos = getObject("estratos", String.class, rs);
        String proyectos = getObject("proyectos", String.class, rs);
        if (estratos != null) {
          estratos = FuncionesDatoUtil.deSeparadoPorGuionesACamelCase(estratos);
        }
        if (proyectos != null) {
          proyectos = FuncionesDatoUtil.deSeparadoPorGuionesACamelCase(proyectos);
        }
        area.setProyectos(proyectos);
        area.setEstratos(estratos);
        return area.setRgtaIderegistro(regimen).setDctaIderegistro(documentos);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Consulta el área de prestación para de un régimen tarifario.
   *
   * @param idRegimen Identificador del régimen tarifario.
   * @return Área de prestación
   * @throws PersistenciaExcepcion
   */
  public ArprAreaprestacion obtenerArea(Integer idRegimen)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.arpr_areaprestacion ")
            .append("WHERE rgta_ideregistro = :regimen;");
    parametros.put("regimen", idRegimen);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getArprAreaprestacion(rs));
  }
}
