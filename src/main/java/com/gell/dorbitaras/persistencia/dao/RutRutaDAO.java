package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class RutRutaDAO extends RutRutaCRUD
{

  private static final String MACRO_RUTA = "macro";
  private static final String MICRO_RUTA = "micro";

  public RutRutaDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Expone el método de guardarRuta y si se envía el identificador de ruta se
   * edita
   *
   * @param rutRuta Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarRuta(RutRuta rutRuta)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer rutIderegistro = rutRuta.getRutIderegistro();
    rutIderegistro = rutIderegistro == null ? -1 : rutIderegistro;
    RutRuta rutaGuardada = consultar(rutIderegistro.longValue());
    if (rutaGuardada != null) {
      this.editar(rutRuta);
      return;
    }
    super.insertar(rutRuta);
  }

  /**
   * Consulta todas las área de prestación dependiendo el parametro de busqueda
   *
   * @param nombre parametro de búsqueda
   * @param tipo parametro de búsqueda
   * @param ciclo parametro de búsqueda
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<RutRuta> consultar(String nombre, Integer tipo, Integer ciclo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT  rut.*,cc.*,uni.* ,  ")
            .append("       to_json((  ")
            .append("               SELECT DISTINCT ARRAY_AGG(empresa.*)  ")
            .append("                  FROM (SELECT DISTINCT *  ")
            .append("                      FROM empresas emp  ")
            .append("                  INNER JOIN ruem_rutempresa ruem  ")
            .append("                ON rut.rut_ideregistro = ruem.rut_ideregistro  ")
            .append("                  WHERE ruem.emp_ideregistro = emp.empresa_sevemp  ")
            .append("                   ) AS empresa ")
            .append("               )) empresas  ")
            .append("            FROM rut_ruta rut  ")
            .append("              INNER JOIN cic_ciclo cc  ")
            .append("                ON rut.cic_ideregistro = cc.cic_ideregistro  ")
            .append("              INNER JOIN uni_unidad uni  ")
            .append("                ON rut.uni_tiporuta = uni.uni_ideregistro  ")
            .append(" WHERE uni.uni_nombre1 ILIKE :macro ")
            .append(" OR uni.uni_nombre1 ILIKE :micro ");
    if (nombre != null && !tipo.equals(0) && ciclo.equals(0)) {
      sql.append("            AND rut.rut_nombre ILIKE :nombre  ")
              .append("                AND cc.cic_estado = :estado  ")
              .append("                AND rut.uni_tiporuta = :tipo  ")
              .append("                AND rut.cic_ideregistro = :ciclo;");
      parametros.put("nombre", '%' + nombre + '%');
      parametros.put("tipo", tipo);
      parametros.put("ciclo", ciclo);
    }
    parametros.put("macro", '%' + MACRO_RUTA + '%');
    parametros.put("micro", '%' + MICRO_RUTA + '%');
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<RutRuta>()
    {
      @Override
      public RutRuta siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        RutRuta rutRuta = getRutRuta(rs, columns, "rut_ruta1");
        CicCiclo cicCiclo = CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1");
        UniUnidad unidad = UniUnidadDAO.getUniUnidad(rs, columns, "uni_unidad1");
        String empresas = getObject("empresas", String.class, rs);
        rutRuta.setInfo(empresas);
        return rutRuta.setCicIderegistro(cicCiclo).setUniTiporuta(unidad.getUniIderegistro())
                      .setRutTipo(unidad.getUniNombre1());
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Método encargado de consultas la micro rutas que no tengan asociada una
   * macroruta.
   *
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<RutRuta> consultarMicroRutas()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT rut.* ")
            .append("FROM rut_ruta rut ")
            .append("         INNER JOIN cic_ciclo cic ")
            .append("                    ON cic.cic_ideregistro = rut.cic_ideregistro ")
            .append("         INNER JOIN uni_unidad uni ")
            .append("                    ON rut.uni_tiporuta = uni.uni_ideregistro ")
            .append("WHERE uni.uni_nombre1 ILIKE :tipoRuta ")
            .append("  AND cic.cic_estado = :estado ")
            .append("  AND rut.rut_ideregistro NOT IN ( ")
            .append("    SELECT ((jsonb_array_elements(rure.rut_microruta)) ->> 'microRuta')::INTEGER idmicro ")
            .append("    FROM aseo.rure_rutrecoleccion rure ")
            .append(")");
    parametros.put("tipoRuta", '%' + MICRO_RUTA + '%');
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getRutRuta(rs));
  }

  /**
   * Consulta todas las rutas micro y macro segun parametro
   *
   * @param tipoRuta macro o micro
   * @return Lista de las rutas
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<RutRuta> consultarMacroMicro(String tipoRuta)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT DISTINCT * FROM rut_ruta rut  ")
            .append("    INNER JOIN cic_ciclo cic  ")
            .append("          ON cic.cic_ideregistro = rut.cic_ideregistro  ")
            .append("    INNER JOIN uni_unidad uni ")
            .append("          ON rut.uni_tiporuta = uni.uni_ideregistro  ")
            .append("    WHERE uni.uni_nombre1 ILIKE :tipoRuta  ")
            .append("      AND cic.cic_estado = :estado ")
            .append(" AND rut.rut_ideregistro IN ") 
            .append(" (SELECT ((jsonb_array_elements(rure.rut_microruta)) ->> 'microRuta')::INTEGER idmicro ")
            .append(" FROM aseo.rure_rutrecoleccion rure where ")
            .append(" rure.rure_swtact is null or rure.rure_swtact in ('A')) ");
    parametros.put("tipoRuta", '%' + tipoRuta + '%');
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<RutRuta>()
    {
      @Override
      public RutRuta siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        RutRuta rutRuta = RutRutaDAO.getRutRuta(rs, columns, "rut_ruta1");
        return rutRuta;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

}
