package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class ArprAreaprestacionCRUD extends GenericoCRUD
{

  public ArprAreaprestacionCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(ArprAreaprestacion arprAreaprestacion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.arpr_areaprestacion(dcta_ideregistro,arpr_nombre,arpr_descripcion,arpr_nuap,arpr_nusd,arpr_nomdisfinal,emp_ideregistro,rgta_ideregistro,liq_ideregistro) VALUES (?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object dctaIderegistro = (arprAreaprestacion.getDctaIderegistro() == null) ? null : arprAreaprestacion.getDctaIderegistro().getDctaIderegistro();
      sentencia.setObject(i++, dctaIderegistro);
      sentencia.setObject(i++, arprAreaprestacion.getArprNombre());
      sentencia.setObject(i++, arprAreaprestacion.getArprDescripcion());
      sentencia.setObject(i++, arprAreaprestacion.getArprNuap());
      sentencia.setObject(i++, arprAreaprestacion.getArprNusd());
      sentencia.setObject(i++, arprAreaprestacion.getArprNomdisfinal());
      sentencia.setObject(i++, arprAreaprestacion.getEmpIderegistro());
      Object rgtaIderegistro = (arprAreaprestacion.getRgtaIderegistro() == null) ? null : arprAreaprestacion.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject(i++, rgtaIderegistro);
      Object liqIderegistro = (arprAreaprestacion.getLiqIderegistro() == null) ? null : arprAreaprestacion.getLiqIderegistro().getUniLiquidacion();
      sentencia.setObject(i++, liqIderegistro);

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        arprAreaprestacion.setArprIderegistro(rs.getInt("arpr_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(ArprAreaprestacion arprAreaprestacion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.arpr_areaprestacion(arpr_ideregistro,dcta_ideregistro,arpr_nombre,arpr_descripcion,arpr_nuap,arpr_nusd,arpr_nomdisfinal,emp_ideregistro,rgta_ideregistro,liq_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, arprAreaprestacion.getArprIderegistro());
      Object dctaIderegistro = (arprAreaprestacion.getDctaIderegistro() == null) ? null : arprAreaprestacion.getDctaIderegistro().getDctaIderegistro();
      sentencia.setObject(i++, dctaIderegistro);
      sentencia.setObject(i++, arprAreaprestacion.getArprNombre());
      sentencia.setObject(i++, arprAreaprestacion.getArprDescripcion());
      sentencia.setObject(i++, arprAreaprestacion.getArprNuap());
      sentencia.setObject(i++, arprAreaprestacion.getArprNusd());
      sentencia.setObject(i++, arprAreaprestacion.getArprNomdisfinal());
      sentencia.setObject(i++, arprAreaprestacion.getEmpIderegistro());
      Object rgtaIderegistro = (arprAreaprestacion.getRgtaIderegistro() == null) ? null : arprAreaprestacion.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject(i++, rgtaIderegistro);
      Object liqIderegistro = (arprAreaprestacion.getLiqIderegistro() == null) ? null : arprAreaprestacion.getLiqIderegistro().getUniLiquidacion();
      sentencia.setObject(i++, liqIderegistro);

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(ArprAreaprestacion arprAreaprestacion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.arpr_areaprestacion SET dcta_ideregistro=?,arpr_nombre=?,arpr_descripcion=?,arpr_nuap=?,arpr_nusd=?,arpr_nomdisfinal=?,emp_ideregistro=?,rgta_ideregistro=?,liq_ideregistro=? where arpr_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      Object dctaIderegistro = (arprAreaprestacion.getDctaIderegistro() == null) ? null : arprAreaprestacion.getDctaIderegistro().getDctaIderegistro();
      sentencia.setObject(i++, dctaIderegistro);
      sentencia.setObject(i++, arprAreaprestacion.getArprNombre());
      sentencia.setObject(i++, arprAreaprestacion.getArprDescripcion());
      sentencia.setObject(i++, arprAreaprestacion.getArprNuap());
      sentencia.setObject(i++, arprAreaprestacion.getArprNusd());
      sentencia.setObject(i++, arprAreaprestacion.getArprNomdisfinal());
      sentencia.setObject(i++, arprAreaprestacion.getEmpIderegistro());
      Object rgtaIderegistro = (arprAreaprestacion.getRgtaIderegistro() == null) ? null : arprAreaprestacion.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject(i++, rgtaIderegistro);
      Object liqIderegistro = (arprAreaprestacion.getLiqIderegistro() == null) ? null : arprAreaprestacion.getLiqIderegistro().getUniLiquidacion();
      sentencia.setObject(i++, liqIderegistro);
      sentencia.setObject(i++, arprAreaprestacion.getArprIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<ArprAreaprestacion> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<ArprAreaprestacion> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.arpr_areaprestacion";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getArprAreaprestacion(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public ArprAreaprestacion consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    ArprAreaprestacion obj = null;
    try {

      String sql = "SELECT * FROM aseo.arpr_areaprestacion WHERE arpr_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getArprAreaprestacion(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static ArprAreaprestacion getArprAreaprestacion(ResultSet rs)
          throws PersistenciaExcepcion
  {
    ArprAreaprestacion arprAreaprestacion = new ArprAreaprestacion();
    arprAreaprestacion.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    DctaDctotarifas dcta_ideregistro = new DctaDctotarifas();
    dcta_ideregistro.setDctaIderegistro(getObject("dcta_ideregistro", Integer.class, rs));
    arprAreaprestacion.setDctaIderegistro(dcta_ideregistro);
    arprAreaprestacion.setArprNombre(getObject("arpr_nombre", String.class, rs));
    arprAreaprestacion.setArprDescripcion(getObject("arpr_descripcion", String.class, rs));
    arprAreaprestacion.setArprNuap(getObject("arpr_nuap", String.class, rs));
    arprAreaprestacion.setArprNusd(getObject("arpr_nusd", String.class, rs));
    arprAreaprestacion.setArprNomdisfinal(getObject("arpr_nomdisfinal", String.class, rs));
    arprAreaprestacion.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
    RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
    rgta_ideregistro.setRgtaIderegistro(getObject("rgta_ideregistro", Integer.class, rs));
    arprAreaprestacion.setRgtaIderegistro(rgta_ideregistro);
    LiqLiquidacion liq_ideregistro = new LiqLiquidacion();
    liq_ideregistro.setUniLiquidacion(getObject("liq_ideregistro", Integer.class, rs));
    arprAreaprestacion.setLiqIderegistro(liq_ideregistro);
    arprAreaprestacion.setArprLiquidaciones(getObject("arpr_liquidaciones", String.class, rs));
    return arprAreaprestacion;
  }

  public static ArprAreaprestacion getArprAreaprestacion(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ArprAreaprestacion arprAreaprestacion = new ArprAreaprestacion();
    Integer columna = columnas.get(alias + "_arpr_ideregistro");
    if (columna != null) {
      arprAreaprestacion.setArprIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_dcta_ideregistro");
    if (columna != null) {
      DctaDctotarifas dcta_ideregistro = new DctaDctotarifas();
      dcta_ideregistro.setDctaIderegistro(getObject(columna, Integer.class, rs));
      arprAreaprestacion.setDctaIderegistro(dcta_ideregistro);
    }
    columna = columnas.get(alias + "_arpr_nombre");
    if (columna != null) {
      arprAreaprestacion.setArprNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_arpr_descripcion");
    if (columna != null) {
      arprAreaprestacion.setArprDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_arpr_nuap");
    if (columna != null) {
      arprAreaprestacion.setArprNuap(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_arpr_nusd");
    if (columna != null) {
      arprAreaprestacion.setArprNusd(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_arpr_nomdisfinal");
    if (columna != null) {
      arprAreaprestacion.setArprNomdisfinal(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_emp_ideregistro");
    if (columna != null) {
      arprAreaprestacion.setEmpIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_rgta_ideregistro");
    if (columna != null) {
      RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
      rgta_ideregistro.setRgtaIderegistro(getObject(columna, Integer.class, rs));
      arprAreaprestacion.setRgtaIderegistro(rgta_ideregistro);
    }
    columna = columnas.get(alias + "_liq_ideregistro");
    if (columna != null) {
      LiqLiquidacion liq_ideregistro = new LiqLiquidacion();
      liq_ideregistro.setUniLiquidacion(getObject(columna, Integer.class, rs));
      arprAreaprestacion.setLiqIderegistro(liq_ideregistro);
    }
    columna = columnas.get(alias + "_arpr_liquidaciones");    
    if (columna != null) {
      arprAreaprestacion.setArprLiquidaciones(getObject(columna, String.class, rs));
    }
    return arprAreaprestacion;
  }

}
