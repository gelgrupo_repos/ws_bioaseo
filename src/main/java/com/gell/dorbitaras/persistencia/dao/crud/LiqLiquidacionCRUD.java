package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class LiqLiquidacionCRUD extends GenericoCRUD
{

  public LiqLiquidacionCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(LiqLiquidacion liqLiquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO public.liq_liquidacion(est_liquidacion,liq_nombre,uni_documento,uni_tipdocument,liq_inivigencia,liq_finvigencia,liq_venclasific,liq_estado,liq_historico,liq_diavencim,liq_diasuspens,usu_ideregistro,liq_tipcuota,liq_ctrventas,hliq_ideregistr) VALUES (:est_liquidacion,:liq_nombre,:uni_documento,:uni_tipdocument,:liq_inivigencia,:liq_finvigencia,:liq_venclasific,:liq_estado,:liq_historico,:liq_diavencim,:liq_diasuspens,:usu_ideregistro,:liq_tipcuota,:liq_ctrventas,:hliq_ideregistr)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object estLiquidacion = (liqLiquidacion.getEstLiquidacion() == null) ? null : liqLiquidacion.getEstLiquidacion().getUniIderegistro();
      sentencia.setObject("est_liquidacion", estLiquidacion);
      sentencia.setObject("liq_nombre", liqLiquidacion.getLiqNombre());
      Object uniDocumento = (liqLiquidacion.getUniDocumento() == null) ? null : liqLiquidacion.getUniDocumento().getDotiIderegistr();
      sentencia.setObject("uni_documento", uniDocumento);
      Object uniTipdocument = (liqLiquidacion.getUniTipdocument() == null) ? null : liqLiquidacion.getUniTipdocument().getDotiIderegistr();
      sentencia.setObject("uni_tipdocument", uniTipdocument);
      sentencia.setObject("liq_inivigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqInivigencia()));
      sentencia.setObject("liq_finvigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqFinvigencia()));
      sentencia.setObject("liq_venclasific", liqLiquidacion.getLiqVenclasific());
      sentencia.setObject("liq_estado", liqLiquidacion.getLiqEstado());
      sentencia.setObject("liq_historico", liqLiquidacion.getLiqHistorico());
      sentencia.setObject("liq_diavencim", liqLiquidacion.getLiqDiavencim());
      sentencia.setObject("liq_diasuspens", liqLiquidacion.getLiqDiasuspens());
      sentencia.setObject("usu_ideregistro", liqLiquidacion.getUsuIderegistro());
      sentencia.setObject("liq_tipcuota", liqLiquidacion.getLiqTipcuota());
      sentencia.setObject("liq_ctrventas", liqLiquidacion.getLiqCtrventas());
      sentencia.setObject("hliq_ideregistr", liqLiquidacion.getHliqIderegistr());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        liqLiquidacion.setUniLiquidacion(rs.getInt("uni_liquidacion"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(LiqLiquidacion liqLiquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.liq_liquidacion(uni_liquidacion,est_liquidacion,liq_nombre,uni_documento,uni_tipdocument,liq_inivigencia,liq_finvigencia,liq_venclasific,liq_estado,liq_historico,liq_diavencim,liq_diasuspens,usu_ideregistro,liq_tipcuota,liq_ctrventas,hliq_ideregistr) VALUES (:uni_liquidacion,:est_liquidacion,:liq_nombre,:uni_documento,:uni_tipdocument,:liq_inivigencia,:liq_finvigencia,:liq_venclasific,:liq_estado,:liq_historico,:liq_diavencim,:liq_diasuspens,:usu_ideregistro,:liq_tipcuota,:liq_ctrventas,:hliq_ideregistr)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("uni_liquidacion", liqLiquidacion.getUniLiquidacion());
      Object estLiquidacion = (liqLiquidacion.getEstLiquidacion() == null) ? null : liqLiquidacion.getEstLiquidacion().getUniIderegistro();
      sentencia.setObject("est_liquidacion", estLiquidacion);
      sentencia.setObject("liq_nombre", liqLiquidacion.getLiqNombre());
      Object uniDocumento = (liqLiquidacion.getUniDocumento() == null) ? null : liqLiquidacion.getUniDocumento().getDotiIderegistr();
      sentencia.setObject("uni_documento", uniDocumento);
      Object uniTipdocument = (liqLiquidacion.getUniTipdocument() == null) ? null : liqLiquidacion.getUniTipdocument().getDotiIderegistr();
      sentencia.setObject("uni_tipdocument", uniTipdocument);
      sentencia.setObject("liq_inivigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqInivigencia()));
      sentencia.setObject("liq_finvigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqFinvigencia()));
      sentencia.setObject("liq_venclasific", liqLiquidacion.getLiqVenclasific());
      sentencia.setObject("liq_estado", liqLiquidacion.getLiqEstado());
      sentencia.setObject("liq_historico", liqLiquidacion.getLiqHistorico());
      sentencia.setObject("liq_diavencim", liqLiquidacion.getLiqDiavencim());
      sentencia.setObject("liq_diasuspens", liqLiquidacion.getLiqDiasuspens());
      sentencia.setObject("usu_ideregistro", liqLiquidacion.getUsuIderegistro());
      sentencia.setObject("liq_tipcuota", liqLiquidacion.getLiqTipcuota());
      sentencia.setObject("liq_ctrventas", liqLiquidacion.getLiqCtrventas());
      sentencia.setObject("hliq_ideregistr", liqLiquidacion.getHliqIderegistr());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(LiqLiquidacion liqLiquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.liq_liquidacion SET est_liquidacion=:est_liquidacion,liq_nombre=:liq_nombre,uni_documento=:uni_documento,uni_tipdocument=:uni_tipdocument,liq_inivigencia=:liq_inivigencia,liq_finvigencia=:liq_finvigencia,liq_venclasific=:liq_venclasific,liq_estado=:liq_estado,liq_historico=:liq_historico,liq_diavencim=:liq_diavencim,liq_diasuspens=:liq_diasuspens,usu_ideregistro=:usu_ideregistro,liq_tipcuota=:liq_tipcuota,liq_ctrventas=:liq_ctrventas,hliq_ideregistr=:hliq_ideregistr where uni_liquidacion = :uni_liquidacion ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object estLiquidacion = (liqLiquidacion.getEstLiquidacion() == null) ? null : liqLiquidacion.getEstLiquidacion().getUniIderegistro();
      sentencia.setObject("est_liquidacion", estLiquidacion);
      sentencia.setObject("liq_nombre", liqLiquidacion.getLiqNombre());
      Object uniDocumento = (liqLiquidacion.getUniDocumento() == null) ? null : liqLiquidacion.getUniDocumento().getDotiIderegistr();
      sentencia.setObject("uni_documento", uniDocumento);
      Object uniTipdocument = (liqLiquidacion.getUniTipdocument() == null) ? null : liqLiquidacion.getUniTipdocument().getDotiIderegistr();
      sentencia.setObject("uni_tipdocument", uniTipdocument);
      sentencia.setObject("liq_inivigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqInivigencia()));
      sentencia.setObject("liq_finvigencia", DateUtil.parseTimestamp(liqLiquidacion.getLiqFinvigencia()));
      sentencia.setObject("liq_venclasific", liqLiquidacion.getLiqVenclasific());
      sentencia.setObject("liq_estado", liqLiquidacion.getLiqEstado());
      sentencia.setObject("liq_historico", liqLiquidacion.getLiqHistorico());
      sentencia.setObject("liq_diavencim", liqLiquidacion.getLiqDiavencim());
      sentencia.setObject("liq_diasuspens", liqLiquidacion.getLiqDiasuspens());
      sentencia.setObject("usu_ideregistro", liqLiquidacion.getUsuIderegistro());
      sentencia.setObject("liq_tipcuota", liqLiquidacion.getLiqTipcuota());
      sentencia.setObject("liq_ctrventas", liqLiquidacion.getLiqCtrventas());
      sentencia.setObject("hliq_ideregistr", liqLiquidacion.getHliqIderegistr());
      sentencia.setObject("uni_liquidacion", liqLiquidacion.getUniLiquidacion());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<LiqLiquidacion> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<LiqLiquidacion> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.liq_liquidacion";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getLiqLiquidacion(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public LiqLiquidacion consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    LiqLiquidacion obj = null;
    try {

      String sql = "SELECT * FROM public.liq_liquidacion WHERE uni_liquidacion= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getLiqLiquidacion(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static LiqLiquidacion getLiqLiquidacion(ResultSet rs)
          throws PersistenciaExcepcion
  {
    LiqLiquidacion liqLiquidacion = new LiqLiquidacion();
    liqLiquidacion.setUniLiquidacion(getObject("uni_liquidacion", Integer.class, rs));
    UniUnidad est_liquidacion = new UniUnidad();
    est_liquidacion.setUniIderegistro(getObject("est_liquidacion", Integer.class, rs));
    liqLiquidacion.setEstLiquidacion(est_liquidacion);
    liqLiquidacion.setLiqNombre(getObject("liq_nombre", String.class, rs));
    DotiDoctipo uni_documento = new DotiDoctipo();
    uni_documento.setDotiIderegistr(getObject("uni_documento", Integer.class, rs));
    liqLiquidacion.setUniDocumento(uni_documento);
    DotiDoctipo uni_tipdocument = new DotiDoctipo();
    uni_tipdocument.setDotiIderegistr(getObject("uni_tipdocument", Integer.class, rs));
    liqLiquidacion.setUniTipdocument(uni_tipdocument);
    liqLiquidacion.setLiqInivigencia(getObject("liq_inivigencia", Timestamp.class, rs));
    liqLiquidacion.setLiqFinvigencia(getObject("liq_finvigencia", Timestamp.class, rs));
    liqLiquidacion.setLiqVenclasific(getObject("liq_venclasific", String.class, rs));
    liqLiquidacion.setLiqEstado(getObject("liq_estado", String.class, rs));
    liqLiquidacion.setLiqHistorico(getObject("liq_historico", String.class, rs));
    liqLiquidacion.setLiqDiavencim(getObject("liq_diavencim", Integer.class, rs));
    liqLiquidacion.setLiqDiasuspens(getObject("liq_diasuspens", Integer.class, rs));
    liqLiquidacion.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    liqLiquidacion.setLiqTipcuota(getObject("liq_tipcuota", String.class, rs));
    liqLiquidacion.setLiqCtrventas(getObject("liq_ctrventas", String.class, rs));
    liqLiquidacion.setHliqIderegistr(getObject("hliq_ideregistr", Long.class, rs));

    return liqLiquidacion;
  }

  public static LiqLiquidacion getLiqLiquidacion(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    LiqLiquidacion liqLiquidacion = new LiqLiquidacion();
    Integer columna = columnas.get(alias + "_uni_liquidacion");
    if (columna != null) {
      liqLiquidacion.setUniLiquidacion(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_est_liquidacion");
    if (columna != null) {
      UniUnidad est_liquidacion = new UniUnidad();
      est_liquidacion.setUniIderegistro(getObject(columna, Integer.class, rs));
      liqLiquidacion.setEstLiquidacion(est_liquidacion);
    }
    columna = columnas.get(alias + "_liq_nombre");
    if (columna != null) {
      liqLiquidacion.setLiqNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_documento");
    if (columna != null) {
      DotiDoctipo uni_documento = new DotiDoctipo();
      uni_documento.setDotiIderegistr(getObject(columna, Integer.class, rs));
      liqLiquidacion.setUniDocumento(uni_documento);
    }
    columna = columnas.get(alias + "_uni_tipdocument");
    if (columna != null) {
      DotiDoctipo uni_tipdocument = new DotiDoctipo();
      uni_tipdocument.setDotiIderegistr(getObject(columna, Integer.class, rs));
      liqLiquidacion.setUniTipdocument(uni_tipdocument);
    }
    columna = columnas.get(alias + "_liq_inivigencia");
    if (columna != null) {
      liqLiquidacion.setLiqInivigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_liq_finvigencia");
    if (columna != null) {
      liqLiquidacion.setLiqFinvigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_liq_venclasific");
    if (columna != null) {
      liqLiquidacion.setLiqVenclasific(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_liq_estado");
    if (columna != null) {
      liqLiquidacion.setLiqEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_liq_historico");
    if (columna != null) {
      liqLiquidacion.setLiqHistorico(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_liq_diavencim");
    if (columna != null) {
      liqLiquidacion.setLiqDiavencim(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_liq_diasuspens");
    if (columna != null) {
      liqLiquidacion.setLiqDiasuspens(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      liqLiquidacion.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_liq_tipcuota");
    if (columna != null) {
      liqLiquidacion.setLiqTipcuota(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_liq_ctrventas");
    if (columna != null) {
      liqLiquidacion.setLiqCtrventas(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_hliq_ideregistr");
    if (columna != null) {
      liqLiquidacion.setHliqIderegistr(getObject(columna, Long.class, rs));
    }
    return liqLiquidacion;
  }

}
