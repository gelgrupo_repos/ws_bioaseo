package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import java.sql.Connection;

public class RacoRanconceptCRUD extends GenericoCRUD
{

  public RacoRanconceptCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public RacoRanconceptCRUD(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

  public void insertar(RacoRanconcept racoRanconcept)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.raco_ranconcept(uni_concepto,raco_raninicial,raco_ranfinal,raco_valor,raco_formula,usu_ideregistro) VALUES (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, racoRanconcept.getUniConcepto());
      sentencia.setObject(i++, racoRanconcept.getRacoRaninicial());
      sentencia.setObject(i++, racoRanconcept.getRacoRanfinal());
      sentencia.setObject(i++, racoRanconcept.getRacoValor());
      sentencia.setObject(i++, racoRanconcept.getRacoFormula());
      sentencia.setObject(i++, racoRanconcept.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        racoRanconcept.setRacoIderegistr(rs.getInt("raco_ideregistr"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(RacoRanconcept racoRanconcept)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.raco_ranconcept(raco_ideregistr,uni_concepto,raco_raninicial,raco_ranfinal,raco_valor,raco_formula,usu_ideregistro) VALUES (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, racoRanconcept.getRacoIderegistr());
      sentencia.setObject(i++, racoRanconcept.getUniConcepto());
      sentencia.setObject(i++, racoRanconcept.getRacoRaninicial());
      sentencia.setObject(i++, racoRanconcept.getRacoRanfinal());
      sentencia.setObject(i++, racoRanconcept.getRacoValor());
      sentencia.setObject(i++, racoRanconcept.getRacoFormula());
      sentencia.setObject(i++, racoRanconcept.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(RacoRanconcept racoRanconcept)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.raco_ranconcept SET uni_concepto=?,raco_raninicial=?,raco_ranfinal=?,raco_valor=?,raco_formula=?,usu_ideregistro=? where raco_ideregistr=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, racoRanconcept.getUniConcepto());
      sentencia.setObject(i++, racoRanconcept.getRacoRaninicial());
      sentencia.setObject(i++, racoRanconcept.getRacoRanfinal());
      sentencia.setObject(i++, racoRanconcept.getRacoValor());
      sentencia.setObject(i++, racoRanconcept.getRacoFormula());
      sentencia.setObject(i++, racoRanconcept.getUsuIderegistro());
      sentencia.setObject(i++, racoRanconcept.getRacoIderegistr());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<RacoRanconcept> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<RacoRanconcept> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.raco_ranconcept";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getRacoRanconcept(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public RacoRanconcept consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    RacoRanconcept obj = null;
    try {

      String sql = "SELECT * FROM public.raco_ranconcept WHERE raco_ideregistr=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getRacoRanconcept(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static RacoRanconcept getRacoRanconcept(ResultSet rs)
          throws PersistenciaExcepcion
  {
    RacoRanconcept racoRanconcept = new RacoRanconcept();
    racoRanconcept.setRacoIderegistr(getObject("raco_ideregistr", Integer.class, rs));
    racoRanconcept.setUniConcepto(getObject("uni_concepto", Integer.class, rs));
    racoRanconcept.setRacoRaninicial(getObject("raco_raninicial", Double.class, rs));
    racoRanconcept.setRacoRanfinal(getObject("raco_ranfinal", Double.class, rs));
    racoRanconcept.setRacoValor(getObject("raco_valor", Double.class, rs));
    racoRanconcept.setRacoFormula(getObject("raco_formula", String.class, rs));
    racoRanconcept.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return racoRanconcept;
  }

  public static RacoRanconcept getRacoRanconcept(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    RacoRanconcept racoRanconcept = new RacoRanconcept();
    Integer columna = columnas.get(alias + "_raco_ideregistr");
    if (columna != null) {
      racoRanconcept.setRacoIderegistr(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_uni_concepto");
    if (columna != null) {
      racoRanconcept.setUniConcepto(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_raco_raninicial");
    if (columna != null) {
      racoRanconcept.setRacoRaninicial(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_raco_ranfinal");
    if (columna != null) {
      racoRanconcept.setRacoRanfinal(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_raco_valor");
    if (columna != null) {
      racoRanconcept.setRacoValor(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_raco_formula");
    if (columna != null) {
      racoRanconcept.setRacoFormula(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      racoRanconcept.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    return racoRanconcept;
  }

}
