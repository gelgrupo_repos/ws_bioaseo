package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.util.TipoGenericoUtil;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.ConConceptoCRUD;
import static com.gell.dorbitaras.persistencia.dao.crud.ConConceptoCRUD.getConConcepto;
import com.gell.dorbitaras.persistencia.dto.ConConceptoCustomDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoGestionVariables;
import com.gell.dorbitaras.persistencia.entidades.CoreConrelacio;
import com.gell.dorbitaras.persistencia.entidades.RacoRanconcept;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import static com.gell.estandar.util.FuncionesDatoUtil.jsonObjetoPorDefecto;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * los conceptos.
 *
 * @author Desarrollador
 */
public class ConConceptoDAO extends ConConceptoCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public ConConceptoDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Super clase.
   *
   * @param cnn Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public ConConceptoDAO(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

  /**
   * Método encargado de consultar los conceptos por liquidación.
   *
   * @param uniLiquidacion Identificador de la liquidación.
   * @return Lista con los conceptos consultados.
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarConcepto(
          Integer uniLiquidacion
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT con.*, rc.* ")
            .append("FROM public.con_concepto con ")
            .append("         INNER JOIN public.uni_unidad unn ")
            .append("                    ON unn.uni_ideregistro = con.uni_concepto ")
            .append("         AND unn.uni_propiedad -> 'tipoVariable' = '\"B\"'") 
            .append("         INNER JOIN public.est_estructura est ")
            .append("                    ON con.est_concepto = est.est_ideregistro ")
            .append("         INNER JOIN public.esem_estempresa ee ")
            .append("                    ON est.est_ideregistro = ee.est_ideregistro ")            
            .append("         LEFT JOIN public.core_conrelacio crr")
            .append("                    ON crr.uni_conrelacion = con.uni_concepto ") 
            .append("         LEFT JOIN public.coli_conliquida cll ")
            .append("                    ON cll.uni_concepto = crr.uni_concepto ")
            .append("         LEFT JOIN public.liq_liquidacion liq ")
            .append("                    ON cll.uni_liquidacion = liq.uni_liquidacion ")
            .append("         INNER JOIN public.prun_prgunidad pp on pp.uni_ideregistro = unn.uni_ideregistro ")//se agrega validacion para filtrar concepto por usuarios
            .append("         INNER JOIN public.uspu_usuprgunid uu on pp.prun_ideregistr = uu.prun_ideregistr ")//se agrega validacion para filtrar concepto por usuarios
            .append("         LEFT JOIN public.raco_ranconcept rc ")
            .append("                   ON con.uni_concepto = rc.uni_concepto ")
       //     .append("WHERE liq.uni_liquidacion = :idLiquidacion ")//Se elimina filtro de liquidacion para que retorne tanto de variables mensuales como semestrales
            .append("Where  con.uni_concepto not in (2896,2902) ")//se elimina validacion 2907 (toneladas rechazo) para ingresarla por este modulo.
            .append(" and ee.emp_ideregistro = :idEmpresa ")
            .append(" and uu.usu_ideregistro = :usuario ")
            .append("ORDER BY con.con_abreviatura,con.con_nombre , rc.raco_raninicial ");
   // parametros.put("idLiquidacion", uniLiquidacion);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("usuario", auditoria.getIdUsuario());
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ConConcepto>()
    {
      @Override
      public ConConcepto siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        ConConcepto concepto = getConConcepto(rs, columns, "con_concepto1");
        concepto.setConNombre("("+concepto.getConAbreviatura()+")-"+concepto.getConNombre());
        RacoRanconcept raco = RacoRanconceptDAO.getRacoRanconcept(rs, columns, "raco_ranconcept1");
        return concepto.setRaco(raco);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }
  
  /**
   * Consulta los conceptos relacionados a indicadores de calidad
   *
   * @param idClase Identificador de la clase
   * @param idPrograma Identificador del programa
   * @param criterio nombre o parte del nombre de la unidad
   * @return Listado de las unidades
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  
    public List<ConConcepto> consultarConceptoIndicadores()throws PersistenciaExcepcion{
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT con.*, rc.* ")
            .append("FROM public.con_concepto con ")
            .append("         INNER JOIN public.uni_unidad unn ")
            .append("                    ON unn.uni_ideregistro = con.uni_concepto ")
            .append("         INNER JOIN public.est_estructura est ")
            .append("                    ON con.est_concepto = est.est_ideregistro ")
            .append("         INNER JOIN public.esem_estempresa ee ")
            .append("                    ON est.est_ideregistro = ee.est_ideregistro ")            
            .append("         LEFT JOIN public.raco_ranconcept rc ")
            .append("                   ON con.uni_concepto = rc.uni_concepto ")
            .append(" WHERE con.con_abreviatura = 'vb-IC_IRCF' or ")
            .append(" con.con_abreviatura = 'vb-IC_IRCF' or ")
            .append(" con.con_abreviatura = 'vb-IC_CRS' or ")
            .append(" con.con_abreviatura = 'vb-ICTR_NA' ")
            .append(" AND ee.emp_ideregistro = :idEmpresa ")
            .append(" ORDER BY con.con_nombre , rc.raco_raninicial ");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ConConcepto>()
    {
      @Override
      public ConConcepto siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        ConConcepto concepto = getConConcepto(rs, columns, "con_concepto1");
        RacoRanconcept raco = RacoRanconceptDAO.getRacoRanconcept(rs, columns, "raco_ranconcept1");
        return concepto.setRaco(raco);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }
    
/**
     * Método encargado de consultar los conceptos por liquidación y adiciona 
     * descripción del concepto
     *
     * @param uniLiquidacion Identificador de la liquidación.
     * @return Lista con los conceptos personalizados consultados.
     * @throws PersistenciaExcepcion
     */
    public List<ConConceptoGestionVariables> consultarConceptoYDescripcion(
            Integer uniLiquidacion
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT DISTINCT con.*, rc.*, vv.varc_descripcion  ")
                .append(" FROM public.con_concepto con ")
                .append("          INNER JOIN public.uni_unidad unn ")
                .append("                     ON unn.uni_ideregistro = con.uni_concepto ")
                .append(" 	      AND unn.uni_propiedad ->> 'tipoVariable' = 'B' ")
                .append(" 	     inner join aseo.varc_varcalculo vv  ")
                .append(" 	     	on con.uni_concepto = vv.con_ideregistro  ")
                .append("          INNER JOIN public.est_estructura est ")
                .append("                     ON con.est_concepto = est.est_ideregistro ")
                .append("          INNER JOIN public.esem_estempresa ee ")
                .append("                     ON est.est_ideregistro = ee.est_ideregistro  ")
                .append("          INNER JOIN public.core_conrelacio crr ")
                .append("                     ON crr.uni_conrelacion = con.uni_concepto  ")
                .append("          INNER JOIN public.coli_conliquida cll ")
                .append("                     ON cll.uni_concepto = crr.uni_concepto ")
                .append("          INNER JOIN public.liq_liquidacion liq ")
                .append("                     ON cll.uni_liquidacion = liq.uni_liquidacion  ")
                .append("          LEFT JOIN public.raco_ranconcept rc ")
                .append("                    ON con.uni_concepto = rc.uni_concepto ")
                .append(" WHERE liq.uni_liquidacion = :idLiquidacion  ")
                .append(" AND con.uni_concepto not in (2896,2902,2907) ")
                .append(" AND ee.emp_ideregistro = :idEmpresa  ")
                .append(" ORDER BY con.con_nombre , rc.raco_raninicial ");
        parametros.put("idLiquidacion", uniLiquidacion);
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ConConceptoGestionVariables>() {
            @Override
            public ConConceptoGestionVariables siguiente(ResultSet rs, Map<String, Integer> columns)
                    throws PersistenciaExcepcion {
                ConConceptoGestionVariables concepto = getConConceptoGestionVariables(rs, columns, "con_concepto1");
                RacoRanconcept raco = RacoRanconceptDAO.getRacoRanconcept(rs, columns, "raco_ranconcept1");
                concepto.setDescripcion(getObject("varc_descripcion", String.class, rs));
                return concepto.setRaco(raco);
            }

            @Override
            public void sinResultados()
                    throws PersistenciaExcepcion {
                throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
            }
        });
    }

  /**
   * Consulta todas las unidades de la empresa que pertenecen un programa y a
   * una clase en específico
   *
   * @param idClase Identificador de la clase
   * @param idPrograma Identificador del programa
   * @param criterio nombre o parte del nombre de la unidad
   * @return Listado de las unidades
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<ConConcepto> consultarUnidadProgramaClase(Integer idClase,
          Integer idPrograma, String criterio)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT uni.*, rr.*, con.* ")
            .append("FROM uni_unidad uni ")
            .append("  INNER JOIN est_estructura est ON uni.est_ideregistro = est.est_ideregistro ")
            .append("  INNER JOIN esem_estempresa esem ON est.est_ideregistro = esem.est_ideregistro ")
            .append("  INNER JOIN prun_prgunidad prun ON uni.uni_ideregistro = prun.uni_ideregistro ")
            .append("  INNER JOIN con_concepto con ON uni.uni_ideregistro = con.uni_concepto ")
            .append("  LEFT JOIN raco_ranconcept rr on con.uni_concepto = rr.uni_concepto ")
            .append("WHERE est.cla_ideregistro = :idclase ")
            .append("      AND esem.emp_ideregistro = :idempresa ")
            .append("      AND prun.prg_ideregistro = :idprograma ")
            .append("      AND ( uni.uni_nombre1 ILIKE :criterio)");
    parametros.put("idprograma", idPrograma);
    parametros.put("criterio", "%" + criterio + "%");
    parametros.put("idclase", idClase);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              ConConcepto concepto = getConConcepto(rs, columns, "con_concepto1");
              RacoRanconcept raco = RacoRanconceptDAO.getRacoRanconcept(rs, columns, "raco_ranconcept1");
              return concepto.setRaco(raco);
            });
  }

  /**
   * Método encargado de cambiar el valor del concepto al momento de certificar
   * una variable.
   *
   * @param idConcepto Identificador del concepto.
   * @param nuevoValor Valor certificado para el concepto.
   * @throws PersistenciaExcepcion
   */
  public void cambiarValorConcepto(Integer idConcepto, Double nuevoValor)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE con_concepto ")
            .append("SET con_valor = :valorNuevo ")
            .append("WHERE uni_concepto = :idConcepto ");
    parametros.put("valorNuevo", nuevoValor);
    parametros.put("idConcepto", idConcepto);
    ejecutarEdicion(sql, parametros);
  }
  
    /**
   * Método encargado de cambiar el valor del concepto al momento de certificar
   * una variable.
   *
   * @param idConcepto Identificador del concepto.
   * @param idPeriodo Identificador del periodo.
   * @param raco_ideregistr  Identificador del periodo padre.
   * @param valor  valor a actualizar.
   * @param descripcion  descripcion del Raco.
   * @throws PersistenciaExcepcion
   */
  public void cambiarValorBase(Integer idConcepto, Integer idPeriodo, Integer raco_ideregistr, Double valor, String descripcion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("update raco_ranconcept set raco_valor :valorNuevo ")
        .append("where raco_ideregistr = :raco_ideregistr; ")
        .append("update aseo.varc_varcalculo set varc_valor = :valorNuevo, ")
        .append("varc_descripcion = :descripcion  ")
        .append("where con_ideregistro = :idConcepto ")
        .append("and per_ideregistro = :idPeriodo ");
    parametros.put("valorNuevo", valor);
    parametros.put("raco_ideregistr", raco_ideregistr);
    parametros.put("idConcepto", idConcepto);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("descripcion", descripcion);
    ejecutarEdicion(sql, parametros);
  }
  
  
    /**
   * Método encargado de cambiar el valor del concepto al momento de certificar
   * una variable.
   *
   * @param idConcepto Identificador del concepto.
   * @param nuevoValor Valor certificado para el concepto.
   * @param usuario Usuario activo en el token.
   * @throws PersistenciaExcepcion
   */
  public void cambiarValorHistoricoConcepto(Integer idConcepto, Double nuevoValor, String usuario)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("insert into con_concepto_hist ( ")
        .append("	uni_concepto, est_concepto, con_nombre, con_alias, con_abreviatura, ")
        .append("	con_tipcalculo, con_valor, con_formula, con_operacion, con_naturaleza,  ")
        .append("	con_preliquidar, con_anticipo, con_pagpriori, con_financiable, con_inivigencia, ") 
        .append("	con_finvigencia, con_estado, prg_ideregistro, con_tipregistro, con_condonable,  ")
        .append("	con_valnulo, usu_ideregistro, fun_ideregistro, con_suspende, con_intfinanciacion, con_metajuste, ")
        .append("	con_precision, con_contabiliza, con_liquidaservicio, con_propiedad, fecha_modificacion, usu_modificacion ")
        .append(")  ")
        .append("select uni_concepto, est_concepto, con_nombre, con_alias, con_abreviatura, con_tipcalculo, :nuevoValor,  ")
        .append("	con_formula, con_operacion, con_naturaleza, con_preliquidar, con_anticipo, con_pagpriori,  ")
        .append("	con_financiable, con_inivigencia, con_finvigencia, con_estado, prg_ideregistro, con_tipregistro,  ")
        .append("	con_condonable, con_valnulo, usu_ideregistro, fun_ideregistro, con_suspende, con_intfinanciacion, con_metajuste, ")
        .append("	con_precision, con_contabiliza, con_liquidaservicio, con_propiedad, now(), :usuario ")
        .append("from con_concepto cc where uni_concepto = :idConcepto; ");
    parametros.put("valorNuevo", nuevoValor);
    parametros.put("idConcepto", idConcepto);
    parametros.put("usuario", usuario);
    ejecutarEdicion(sql, parametros);
  }

  /**
   * Método encargado de consultar los conceptos para el base calculo.
   *
   * @param idLiquidacion
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarVariablesBaseCalcuclo(Integer idLiquidacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT *, ")
            .append("       (SELECT ARRAY_TO_JSON(ARRAY_AGG(rangos.*)) ")
            .append("        FROM (SELECT raco.raco_raninicial, ")
            .append("                     raco.raco_ranfinal, ")
            .append("                     raco.raco_valor, ")
            .append("                     raco.raco_ideregistr ")
            .append("              FROM con_concepto varr ")
            .append("                       INNER JOIN raco_ranconcept raco ")
            .append("                                  ON varr.uni_concepto = raco.uni_concepto ")
            .append("              WHERE varr.uni_concepto = variables.idConcepto ")
            .append("              ORDER BY varr.uni_concepto) rangos) lista_rangos ")
            .append("FROM (SELECT DISTINCT varc.uni_concepto idConcepto, ")
            .append("                      varc.con_nombre   nombreconcepto, ")
            .append("                      varc.con_valor valorConcepto ")
            .append("      FROM con_concepto varc ")
            .append("               INNER JOIN est_estructura est ")
            .append("                          ON varc.est_concepto = est_ideregistro ")
            .append("               INNER JOIN esem_estempresa ee ")
            .append("                          ON est.est_ideregistro = ee.est_ideregistro ")
            .append("               INNER JOIN coli_conliquida coli ")
            .append("                          ON varc.uni_concepto = coli.uni_concepto ")
            .append("               INNER JOIN aseo.varc_varcalculo var ")
            .append("                          ON varc.uni_concepto = var.con_ideregistro ")
            .append("               INNER JOIN aseo.vrta_varterapr vrta ")
            .append("                          ON varc.uni_concepto = vrta.con_ideregistro ")
            .append("      WHERE ee.emp_ideregistro = :idempresa ")
            .append("        AND coli.uni_liquidacion = :idLiquidacion ")
            .append("        AND var.varc_estado = :certificado ")
            .append("        AND var.varc_estadoregistro = :activo ")
            .append("        AND vrta.vrta_estado = :certificado ")
            .append("        AND vrta.vrta_estado_registro = :activo ")
            .append("     ) variables;");
    parametros.put("idLiquidacion", idLiquidacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      ConConcepto concepto = new ConConcepto()
              .setConNombre(getObject("nombreconcepto", String.class, rs))
              .setUniConcepto(new UniUnidad(getObject("idConcepto", Integer.class, rs)))
              .setConValor(getObject("valorConcepto", Double.class, rs))
              .setListaRango(getObject("lista_rangos", String.class, rs));
      return concepto;
    });
  }

  /**
   * Consulta los conceptos o variables relacionadas a otra.
   *
   * @param concepto Información del concepto que se quiere consultar los
   * conceptos que depende
   * @param idsLiquidacion Identificador de la liquidación que se quiere
   * procesar
   * @return Listado de los conceptos que depende incluyendo el consultado
   * @throws PersistenciaExcepcion Error al consultar los registros
   */
  public List<ConConcepto> consultarConceptosRelacionados(ConConcepto concepto, List<String> idsLiquidacion,String conLiquidados)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    String listaFinal="-1";
    if((conLiquidados==null) || (conLiquidados.equals(""))){
      conLiquidados="(-1)";
    }
    if(!idsLiquidacion.get(0).isEmpty() || !idsLiquidacion.get(0).equals("")){
        listaFinal =idsLiquidacion.toString().replace("[", "").replace("]", "");
    }
//    idsLiquidacion..add(0,"-2");
    sql.append("WITH RECURSIVE infoconcepto AS ( ")
            .append("    SELECT con.*, ")
            .append("           to_json((SELECT array_agg(coe.uni_conrelacion) ")
            .append("                    FROM core_conrelacio coe ")
            .append("                    WHERE coe.uni_concepto = con.uni_concepto))::TEXT padres ")
            .append("    FROM con_concepto con ")
            .append("             INNER JOIN coli_conliquida coli ON con.uni_concepto = coli.uni_concepto ")
            .append("    WHERE con.uni_concepto = :idconcepto ")
            .append("      AND con.con_estado = :estado ")
            .append("    AND coli.uni_liquidacion IN (").append(listaFinal).append(" )")
            .append("      AND now()::DATE BETWEEN con.con_inivigencia AND (CASE ")
            .append("                                                           WHEN con.con_finvigencia IS NULL THEN now()::DATE ")
            .append("                                                           ELSE con.con_finvigencia END) ")
            .append("    UNION ")
            .append("    SELECT cor.*, ")
            .append("           to_json((SELECT array_agg(coe.uni_conrelacion) ")
            .append("                    FROM core_conrelacio coe ")
            .append("                    WHERE coe.uni_concepto = cor.uni_concepto))::TEXT padres ")
            .append("    FROM con_concepto cor ")
            .append("             INNER JOIN core_conrelacio core ON cor.uni_concepto = core.uni_conrelacion ")
            .append("             INNER JOIN infoconcepto inc ON core.uni_concepto = inc.uni_concepto ")
            .append("             INNER JOIN coli_conliquida coli ON cor.uni_concepto = coli.uni_concepto ")
            .append("    WHERE cor.con_estado = :estado ")
            .append("    AND coli.uni_liquidacion IN ( ").append(listaFinal).append(" )")
            .append("      AND now()::DATE BETWEEN cor.con_inivigencia AND (CASE ")
            .append("                                                           WHEN cor.con_finvigencia IS NULL THEN now()::DATE ")
            .append("                                                           ELSE cor.con_finvigencia END) ")
            .append(") ")
            .append("SELECT row_number() over () orden, *, padres IS NOT NULL tienePadre ")
            .append("FROM infoconcepto ")
            .append("WHERE infoconcepto.uni_concepto NOT IN ").append(conLiquidados)
            .append("ORDER BY tienePadre NULLS FIRST, orden DESC");
    parametros.put("idconcepto", concepto.getUniConcepto().getUniIderegistro());
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ConConcepto>()
    {
      @Override
      public ConConcepto siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        String padres = getObject("padres", String.class, rs);
        ConConcepto infoConcepto = getConConcepto(rs);
        if((padres!=null) ||(padres!="")){
            List<Integer> listaRelacionados = jsonObjetoPorDefecto(padres, TipoGenericoUtil.listaEnteros(), new ArrayList<>());
            List<CoreConrelacio> relacionados = listaRelacionados.stream()
                .map(id -> new CoreConrelacio().setUniConrelacion(new ConConcepto(id)))
                .collect(Collectors.toList());
            infoConcepto.setListaRelacionados(relacionados);
        }else{
            infoConcepto.setListaRelacionados(null);
        }
        return infoConcepto;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONCEPTO_RELACIONADO, "sin|"+concepto.getConNombre());
      }

    });
  }

  /**
   * Método encargado de consultar los conceptos relacionados a la liquidación.
   *
   * @param idLiquidacion Identificador de la liquidación.
   * @return Lista de conceptos
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarConceptosColi(
          Integer idLiquidacion
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT con.* ")
            .append("FROM con_concepto con ")
            .append("         INNER JOIN coli_conliquida cc ON con.uni_concepto = cc.uni_concepto ")
           // .append("         INNER JOIN aseo.vrta_varterapr ON con.uni_concepto = vrta_varterapr.con_ideregistro ")
           // .append("         INNER JOIN aseo.varc_varcalculo ON con.uni_concepto = varc_varcalculo.con_ideregistro ")
            .append("WHERE cc.uni_liquidacion = :uniLiquidacion  ")
//            .append("and cc.uni_concepto in (4210) ; ");
            //.append("  AND varc_estado = :certificado ")
            .append("order by con_tipcalculo desc,length(con_formula) asc; ");
    parametros.put("uniLiquidacion", idLiquidacion);
   // parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getConConcepto(rs));
  }
  
public List<ConConcepto> conceptosExcluidos(
          Integer idLiquidacion
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT con.* ")
            .append("FROM con_concepto con ")
            .append("         INNER JOIN coli_conliquida cc ON con.uni_concepto = cc.uni_concepto ")           
            .append(" WHERE cc.uni_liquidacion = :uniLiquidacion  ")
            .append(" and con. con_alias in ('N', 'TAR-VBAM', 'TAR-TA', 'TAMD', 'VBAMD') ")
            .append("order by con_tipcalculo desc,length(con_formula) asc; ");
    parametros.put("uniLiquidacion", idLiquidacion);
   // parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getConConcepto(rs));
  }
  
  /**
   * Método encargado de consultar los conceptos relacionados a la liquidación por orden.
   *
   * @param idLiquidacion Identificador de la liquidación.
   * @return Lista de conceptos
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarConceptosColiOrder(
          Integer idLiquidacion
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT con.*  ")
          .append("FROM con_concepto con  ")
          .append("  INNER JOIN coli_conliquida cc ON con.uni_concepto = cc.uni_concepto  ")
          .append("WHERE cc.uni_liquidacion = :uniLiquidacion  ")
          .append("  order by con_tipcalculo desc, cc.coli_ideregistr asc;  ");
    parametros.put("uniLiquidacion", idLiquidacion);
   // parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getConConcepto(rs));
  }

  /**
   * Consulta un concepto por la abreviatura
   *
   * @param abreviatura parte inicial de la abreviatura
   * @return Variable con toda la información
   * @throws PersistenciaExcepcion
   */
  public ConConcepto consultarPorAbreviatura(String abreviatura)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT con.* ")
            .append("FROM con_concepto con ")
            .append("         INNER JOIN esem_estempresa esem ")
            .append("                    ON esem.est_ideregistro = con.est_concepto ")
            .append("WHERE esem.emp_ideregistro = :idempresa ")
            .append("  AND con.con_abreviatura ILIKE :abreviatura ")
            .append("  AND con.con_estado = :estado ");
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("abreviatura", abreviatura + "%");
    return ejecutarConsultaSimple(sql, parametros, new ConsultaAdaptador<ConConcepto>()
    {
      @Override
      public ConConcepto siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getConConcepto(rs);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_VARIABLE_EMPRESA, abreviatura);
      }
    });
  }
  
  /**
   * Método encargado de consultar los conceptos para el base calculo, pero que estan pendientes de Certificar.
   * @param idLiquidacion  
   * @param idPeriodo  
   * @param areaPrestacion  
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarVarBaseSinCert(
          Integer idLiquidacion , 
          Integer idPeriodo,
          Integer areaPrestacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT   ")
        .append("	vcc.con_ideregistro AS id_concepto,  ")
        .append("	smm.smper_descripcion, ")
        .append("	vcc.vrta_valor AS val_concepto")
        .append(" FROM aseo.smper_semperiodo smm   ")
        .append("	INNER JOIN per_periodo prr ON prr.per_ideregistro = smm.per_ideregistro   ")
        .append("	INNER JOIN per_periodo pant ON smm.per_idepadre = pant.per_ideregistro    ")
        .append("	INNER JOIN per_periodo pact ON pact.cic_ideregistro = pant.cic_ideregistro")
        .append("		AND pact.per_ideregistro = :idperiodo ")
        .append("		AND pact.per_fecinicial::DATE = CAST ( pant.per_fecfinal AS DATE ) + CAST ( '1 days' AS INTERVAL )")
        .append("	INNER JOIN aseo.vrta_varterapr vcc ON vcc.per_ideregistro = smm.per_ideregistro  ")
        .append("	INNER JOIN public.core_conrelacio crr ON crr.uni_conrelacion = vcc.con_ideregistro ")
        .append("	INNER JOIN public.coli_conliquida cll ON crr.uni_concepto = cll.uni_concepto     ")
        .append("		AND uni_liquidacion = :idLiquidacion ")
        .append("		AND vrta_estado != :certificado  ")
        .append("		AND vrta_estado_registro = :activo   ")
        .append("		AND arpr_ideregistro = :idArea  ")
        .append(" WHERE smm.smper_swtact = :activo     ")
        .append(" UNION	ALL")
        .append(" SELECT    ")
        .append("	vcc.con_ideregistro AS id_concepto,  ")
        .append("	smm.smper_descripcion, ")
        .append("	vcc.varc_valor  AS val_concepto")
        .append(" FROM aseo.smper_semperiodo smm   ")
        .append("	INNER JOIN per_periodo prr ON prr.per_ideregistro = smm.per_ideregistro   ")
        .append("	INNER JOIN per_periodo pant ON smm.per_idepadre = pant.per_ideregistro    ")
        .append("	INNER JOIN per_periodo pact ON pact.cic_ideregistro = pant.cic_ideregistro")
        .append("		AND pact.per_ideregistro = :idperiodo  ")
        .append("		AND pact.per_fecinicial::DATE = CAST ( pant.per_fecfinal AS DATE ) + CAST ( '1 days' AS INTERVAL )")
        .append("	INNER JOIN aseo.varc_varcalculo vcc ON vcc.per_ideregistro = smm.per_ideregistro   ")
        .append("	INNER JOIN public.core_conrelacio crr ON crr.uni_conrelacion = vcc.con_ideregistro ")
        .append("	INNER JOIN public.coli_conliquida cll ON crr.uni_concepto = cll.uni_concepto     ")
        .append("		AND uni_liquidacion = :idLiquidacion")
        .append("		AND varc_estado != :certificado ")
        .append("		AND varc_estadoregistro = :activo ")
        .append("		AND arpr_ideregistro = :idArea  ") 
        .append(" WHERE smm.smper_swtact = :activo ");
    parametros.put("idLiquidacion", idLiquidacion);
    parametros.put("idArea", areaPrestacion);
    parametros.put("idperiodo", idPeriodo);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      ConConcepto concepto = new ConConcepto()
              .setUniConcepto(new UniUnidad(getObject("id_concepto", Integer.class, rs)))
              .setConValor(getObject("val_concepto", Double.class, rs));
      return concepto;
    });
  }
  
  /**
   * Método encargado de relacionar los conceptos relacionados de primer nivel.
   *
   * @param conConcepto Identificador del concepto.
   * @return Lista de conceptos
   * @throws PersistenciaExcepcion
   */
  public List<ConConcepto> consultarConceptosCorrelacionados(
          Integer conConcepto
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("select cc2.* from core_conrelacio cc ")
        .append("inner join con_concepto cc2 on cc2.uni_concepto = cc.uni_conrelacion " )
        .append("where cc.uni_concepto = :conConcepto ; ");
    parametros.put("conConcepto", conConcepto);
   // parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getConConcepto(rs));
  }
  
  public ConConcepto consultarConceptoFProductividad() throws PersistenciaExcepcion{
    StringBuilder sql = new StringBuilder();
    sql.append("select * ")
        .append("from con_concepto cc  " )
        .append("where con_nombre = 'Fact_prod'; ");
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getConConcepto(rs)).get(0);
  }
  
    public List<ConConceptoCustomDTO> obtenerConceptosConstates() throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("select 	cc.uni_concepto, cc.con_nombre, cc.con_alias, cc.con_abreviatura, cc.con_valor, cch.con_valor as valor_anterior ")
            .append("from con_concepto cc ")
            .append("inner join uni_unidad uu  ")
            .append("	on cc.uni_concepto = uu.uni_ideregistro ")
            .append("inner join ( ")
            .append("				select tb1.con_hist_idregistr, tb1.uni_concepto from ( ")
            .append("			  															select ROW_NUMBER() OVER (ORDER BY uni_concepto asc) AS orden, con_hist_idregistr, uni_concepto ")
            .append("																		from (select con_hist_idregistr, uni_concepto from con_concepto_hist ")
            .append("																		order by uni_concepto desc, fecha_modificacion desc ")
            .append("																	  ) tb1 ")
            .append("				order by tb1.uni_concepto desc ")
            .append("			) tb1 ")
            .append("inner join (select max(orden - 1) as orden, tb1.uni_concepto  from ( ")
            .append("			  															select ROW_NUMBER() OVER (ORDER BY uni_concepto asc) AS orden, con_hist_idregistr, uni_concepto ")
            .append("																		from (select con_hist_idregistr, uni_concepto from con_concepto_hist ")
            .append("																		order by uni_concepto desc, fecha_modificacion desc ")
            .append("																				) tb1 ")
            .append("																		order by tb1.uni_concepto desc) tb1 ")
            .append("			  group by uni_concepto) tbl2 ")
            .append("	on tb1.orden = tbl2.orden and tb1.uni_concepto = tbl2.uni_concepto ")
            .append(" 		   ) tb1 ")
            .append(" 	on cc.uni_concepto = tb1.uni_concepto ")
            .append(" inner join con_concepto_hist cch ")
            .append(" 	on tb1.con_hist_idregistr = cch.con_hist_idregistr and tb1.uni_concepto = cch.uni_concepto ")
            .append(" where cc.est_concepto = '111' ")
            .append(" and uu.uni_propiedad  ->> 'tipoVariable' = 'N' order by uu.uni_fecha; ");
        return ejecutarConsulta(sql, parametros, (rs, columns) -> {
            ConConceptoCustomDTO conConceptoCustomDTO = new ConConceptoCustomDTO(
                    getObject("uni_concepto", Integer.class, rs),
                    getObject("con_nombre", String.class, rs),
                    getObject("con_alias", String.class, rs),
                    getObject("con_abreviatura", String.class, rs),
                    getObject("con_valor", Double.class, rs),
                    getObject("valor_anterior", Double.class, rs)
            );
            return conConceptoCustomDTO;
        });
    }
    
    public List<ConConceptoCustomDTO> obtenerConceptosBase() throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("select 	cc.uni_concepto, cc.con_nombre, cc.con_alias, cc.con_abreviatura, cc.con_valor, cch.con_valor as valor_anterior ")
                .append(" from con_concepto cc ")
                .append(" inner join uni_unidad uu  ")
                .append(" 	on cc.uni_concepto = uu.uni_ideregistro ")
                .append(" inner join ( ")
                .append(" 				select max(con_hist_idregistr - 1) as con_hist_idregistr, cch.uni_concepto  ")
                .append(" 				from con_concepto_hist cch	 ")
                .append(" 				group by uni_concepto  ")
                .append(" 		   ) tb1 ")
                .append(" 	on cc.uni_concepto = tb1.uni_concepto ")
                .append(" inner join con_concepto_hist cch ")
                .append(" 	on tb1.con_hist_idregistr = cch.con_hist_idregistr ")
                .append(" where cc.est_concepto = '111' ")
                .append(" and uu.uni_propiedad  ->> 'tipoVariable' = 'N' ");
        return ejecutarConsulta(sql, parametros, (rs, columns) -> {
            ConConceptoCustomDTO conConceptoCustomDTO = new ConConceptoCustomDTO(
                    getObject("uni_concepto", Integer.class, rs),
                    getObject("con_nombre", String.class, rs),
                    getObject("con_alias", String.class, rs),
                    getObject("con_abreviatura", String.class, rs),
                    getObject("con_valor", Double.class, rs),
                    getObject("valor_anterior", Double.class, rs)
            );
            return conConceptoCustomDTO;
        });
    }

}
