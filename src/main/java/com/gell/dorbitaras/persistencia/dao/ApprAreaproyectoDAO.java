package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import static com.gell.dorbitaras.negocio.util.AuditoriaUtil.auditoria;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.ApprAreaproyecto;
import com.gell.dorbitaras.persistencia.entidades.EttaEstttarifas;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.Date;

public class ApprAreaproyectoDAO extends ApprAreaproyectoCRUD {

  public ApprAreaproyectoDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   *
   * Método para la inserción y edición de datos
   *
   * @param proyecto Informacíon de proyecto para área presentación
   * @throws PersistenciaExcepcion
   */
  @Override
  public void insertar(ApprAreaproyecto proyecto)
          throws PersistenciaExcepcion
  {
    Integer ideRegistro = proyecto.getApprIderegistro();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    ApprAreaproyecto documentoGuardada = consultar(ideRegistro.longValue());
    if (documentoGuardada != null) {
      proyecto.setUsuIderegistroAct(auditoria().getIdUsuario())
              .setApprFecact(new Date());

      this.editar(proyecto);
      return;
    }
    proyecto.setApprSwtestado(EEstadoGenerico.ACTIVO);
    proyecto.setApprFecgrabacion(new Date());
    super.insertar(proyecto);
  }
}
