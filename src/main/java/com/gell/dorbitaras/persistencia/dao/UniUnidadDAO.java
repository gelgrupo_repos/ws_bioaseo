package com.gell.dorbitaras.persistencia.dao;

import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.util.LogUtil;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EEstructura;
import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import static com.gell.dorbitaras.persistencia.dao.crud.UniUnidadCRUD.getUniUnidad;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.constante.EMensajeEstandar;
import static com.gell.estandar.util.FuncionesDatoUtil.json;
import static com.gell.estandar.util.FuncionesDatoUtil.jsonMap;
import static com.gell.estandar.util.FuncionesDatoUtil.trim;
import static com.gell.estandar.util.FuncionesDatoUtil.vacio;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.sql.DataSource;

public class UniUnidadDAO extends UniUnidadCRUD
{

  public UniUnidadDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Inserta un registro en la tabla de uni_unidad si el campo uni_ideregistro
   * está nulo de lo contrario lo actualiza
   *
   * @param uniUnidad
   * @throws PersistenciaExcepcion Error al actualizar el registro
   */
  @Override
  public void insertar(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    String nombre = trim(uniUnidad.getUniNombre1());
    uniUnidad.setUsuIderegistro(auditoria.getIdUsuario())
            .setUniNombre1(nombre);
    if (uniUnidad.getUniIderegistro() != null) {
      this.editar(uniUnidad);
      return;
    }
    super.insertar(uniUnidad);
  }

  @Override
  public void editar(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    UniUnidad unidadGuardada = consultar(uniUnidad.getUniIderegistro().longValue());
    try {
      Map<String, String> info = uniUnidad.getUniPropiedad() == null
              ? new HashMap<>()
              : jsonMap(uniUnidad.getUniPropiedad());
      Map<String, String> infoGuardada = unidadGuardada.getUniPropiedad() == null
              ? new HashMap<>()
              : jsonMap(unidadGuardada.getUniPropiedad());
      infoGuardada.putAll(info);
      uniUnidad.setUniPropiedad(json(infoGuardada));
    } catch (AplicacionExcepcion e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_EDITAR);
    }
    super.editar(uniUnidad);
  }

  @Override
  public void insertarTodos(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    UniUnidad unidadGuardada
            = consultar(uniUnidad.getUniIderegistro().longValue());
    if (unidadGuardada == null) {
      super.insertarTodos(uniUnidad);
      return;
    }
    unidadGuardada.setUniNombre1(uniUnidad.getUniNombre1());
    this.editar(unidadGuardada);
  }

  /**
   * Método que expone el método de guardarUnidadBasica y si se envía el
   * identificador de la unidad se edita
   *
   * @param unidad Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * Error al ejecutar la consulta
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion Si el código
   * de usuario existe
   */
  public void guardarUnidad(UniUnidad unidad)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer uniIderegistro = unidad.getUniIderegistro();
    uniIderegistro = uniIderegistro == null ? -1 : uniIderegistro;
    validarCodigo(unidad, unidad.getUniCodigo());
    unidad.setUsuIderegistro(auditoria.getIdUsuario())
            .setUniOrden(1D)
            .setUniNivel(1);
    UniUnidad unidadGuardada = consultar(uniIderegistro.longValue());
    if (unidadGuardada != null) {
      unidad.setEstIderegistro(unidadGuardada.getEstIderegistro())
              .setUniCodigo(unidadGuardada.getUniCodigo())
              .setUniCodigo1(unidadGuardada.getUniCodigo1());
      this.editar(unidad);
      return;
    }
    super.insertar(unidad);
  }

  /**
   * Valida que el código de la uniad no exista en la base de datos para la
   * estructura que está en la entidad de unidades
   *
   * @param unidad información de la unidad
   * @param codigo
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void validarCodigo(UniUnidad unidad, String codigo)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer idUnidad = unidad.getUniIderegistro();
    idUnidad = idUnidad == null ? -1 : idUnidad;
    Integer idEstructura = unidad.getEstIderegistro().getEstIderegistro();
    Integer numero = consultarUnidad(codigo, idEstructura, idUnidad);
    if (numero > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_UNIDAD_CODIGO);
    }
  }

  /**
   * Consulta si el código ya existe en la base de datos
   *
   * @param codigo Código de la unidad
   * @param idEstructura Identificador de la estructura
   * @param idUnidad Identificador de la unidad
   * @return El número de veces que se encuentra registrado el códgio en la base
   * de datos
   * @throws PersistenciaExcepcion
   */
  public Integer consultarUnidad(String codigo, Integer idEstructura,
          Integer idUnidad)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT count(*) numero ")
            .append("FROM uni_unidad uni ")
            .append("WHERE (uni.uni_propiedad ->> 'codigo') ILIKE :codigo ")
            .append("      AND uni.uni_ideregistro <> :idunidad ")
            .append("      AND uni.est_ideregistro = :idestructura ");
    parametros.put("codigo", codigo);
    parametros.put("idunidad", idUnidad);
    parametros.put("idestructura", idEstructura);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getObject("numero", Integer.class, rs));
  }

  /**
   * Consulta si el código ya existe en la base de datos
   *
   * @param nombre Nombre de la unidad
   * @param idEstructura Identificador de la estructura
   * @param idUnidad Identificador de la unidad
   * @throws PersistenciaExcepcion
   */
  public void validaNombre(String nombre, Integer idEstructura,
          Integer idUnidad)
          throws PersistenciaExcepcion
  {
    idUnidad = (idUnidad == null) ? -1 : idUnidad;
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT count(*) numero ")
            .append("FROM uni_unidad uni ")
            .append("WHERE uni.uni_nombre1 ILIKE :nombre ")
            .append("      AND uni.uni_ideregistro <> :idunidad ")
            .append("      AND uni.est_ideregistro = :idestructura ");
    parametros.put("nombre", nombre);
    parametros.put("idunidad", idUnidad);
    parametros.put("idestructura", idEstructura);
    ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Integer cantidad = getObject("numero", Integer.class, rs);
              if (cantidad > 0) {
                throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_UNIDAD_NOMBRE);
              }
              return cantidad;
            });
  }

  /**
   * Consulta todas las unidades dependiendo del tipo de estructura
   *
   * @param criterio nombre o código de la unidad
   * @param estIderegistro Identificador de la estructura
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<UniUnidad> consultar(String criterio, Long estIderegistro)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT DISTINCT uni.* ")
            .append("FROM uni_unidad uni ")
            .append("  INNER JOIN esem_estempresa esem ON esem.est_ideregistro = uni.est_ideregistro ")
            .append("WHERE uni.est_ideregistro = :estIderegistro ")
            .append("      AND esem.emp_ideregistro = :idempresa ")
            .append("      AND (uni.uni_nombre1 ILIKE :criterio OR uni.uni_codigo ILIKE :criterio) ");
    parametros.put("criterio", '%' + criterio + '%');
    parametros.put("estIderegistro", estIderegistro);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              UniUnidad unidad = getUniUnidad(rs);
              unidad.setUniCodigo(unidad.getUniCodigo1());
              return unidad;
            });
  }

  /**
   * Método encargado de obtener el siguiente identificador de las unidades, ya
   * que en las unidades es obligatorio el código por tal motivo se va a generar
   * un código automático para las unidades de fuente de distribución y que sean
   * de tipo Transporte ya que en el caso de uso no es obligatorio, entonces en
   * el campo código 2 se va a almacenar el último número generado automático
   * para generar el siguiente
   *
   * @return Siguiente identificador de la tabla de unidades
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   */
  public Integer consultarSiguienteId()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT nextval('sq_uni_ideregistro') id");
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns)
            -> getObject("id", Integer.class, rs));
  }

  /**
   * Genera un código para la nueva unidad que se quiere ingresar
   *
   * @param uniUnidad
   * @param sigla Para poder genera el código de la unidad
   * @throws PersistenciaExcepcion
   * @throws NegocioExcepcion
   */
  public void generarCodigoUnidad(UniUnidad uniUnidad, String sigla)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    if (uniUnidad.getUniIderegistro() != null) {
      return;
    }
    if (uniUnidad.getUniCodigo() != null) {
      return;
    }
    Integer id = consultarSiguienteId();
    uniUnidad.setUniCodigo(sigla + id)
            .setUniIderegistro(id);
  }

  /**
   * Consulta todas las unidades dependiendo de la clase y la empresa donde se
   * ha iniciado sesión
   *
   * @param criterio nombre o código de la unidad
   * @param idClase Identificador de la clase
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<UniUnidad> consultarPorClase(String criterio, Integer idClase)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT DISTINCT uni.* ")
            .append("FROM uni_unidad uni ")
            .append("  INNER JOIN est_estructura est on uni.est_ideregistro = est.est_ideregistro ")
            .append("  INNER JOIN esem_estempresa esem on est.est_ideregistro = esem.est_ideregistro ")
            .append("WHERE uni.uni_nombre1 ILIKE :criterio AND est.cla_ideregistro = :idClase ")
            .append("      AND esem.emp_ideregistro = :idempresa ");
    parametros.put("criterio", '%' + criterio + '%');
    parametros.put("idClase", idClase);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<UniUnidad>()
    {
      @Override
      public UniUnidad siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        UniUnidad unidad = getUniUnidad(rs);
        unidad.setUniCodigo(unidad.getUniCodigo1());
        return unidad;
      }
    });
  }

  /**
   * Consulta todas las unidades de la empresa que pertenecen un programa y a
   * una clase en específico
   *
   * @param idClase Identificador de la clase
   * @param idPrograma Identificador del programa
   * @param criterio nombre o parte del nombre de la unidad
   * @return Listado de las unidades
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<UniUnidad> consultarUnidadProgramaClase(Integer idClase,
          Integer idPrograma, String criterio)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT uni.*, rr.*, con.* ")
            .append("FROM uni_unidad uni ")
            .append("  INNER JOIN est_estructura est ON uni.est_ideregistro = est.est_ideregistro ")
            .append("  INNER JOIN esem_estempresa esem ON est.est_ideregistro = esem.est_ideregistro ")
            .append("  INNER JOIN prun_prgunidad prun ON uni.uni_ideregistro = prun.uni_ideregistro ")
            .append("  INNER JOIN con_concepto con ON uni.uni_ideregistro = con.uni_concepto ")
            .append("  LEFT JOIN raco_ranconcept rr on con.uni_concepto = rr.uni_concepto ")
            .append("WHERE est.cla_ideregistro = :idclase ")
            .append("      AND esem.emp_ideregistro = :idempresa ")
            .append("      AND prun.prg_ideregistro = :idprograma ")
            .append("      AND ( uni.uni_nombre1 ILIKE :criterio)");
    parametros.put("idprograma", idPrograma);
    parametros.put("criterio", "%" + criterio + "%");
    parametros.put("idclase", idClase);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getUniUnidad(rs));
  }

  /**
   * Valida una propiedad de una unidad y este método no distingue entre
   * minúsculas y mayçusculas
   *
   * @param unidad propiedad que se quiere validar
   * @param atributo nombre del atributo que se encuentra en la columna
   * uniPropiedad
   * @param valor Valor que se quiere verificar si la unidad tiene esa propiedad
   * y esta tiene el valor que se quiere
   * @return TRUE si coincide el valor de la propiedad con el valor que se pasa
   * por parámetro
   * @throws PersistenciaExcepcion Error al consultar
   * @throws NegocioExcepcion Error al realizar la validación
   */
  public boolean validarPropiedad(UniUnidad unidad, String atributo, String valor)
          throws AplicacionExcepcion
  {
    UniUnidad unidadTipo = consultar(unidad.getUniIderegistro().longValue());
    String propiedad = unidadTipo.getUniPropiedad();
    if (vacio(propiedad) || vacio(valor) || vacio(atributo)) {
      return false;
    }
    atributo = atributo.toLowerCase();
    propiedad = propiedad.toLowerCase();
    valor = valor.toLowerCase();
    Map<String, String> infoPropiedad = jsonMap(propiedad);
    String valorAtributo = infoPropiedad.get(atributo);
    return Objects.equals(valor, valorAtributo);
  }

  /**
   * Método encargado de consultar una unidad por el identificador y la clase
   *
   * @param idUnidad identificador de la unidad
   * @param idClase identificador de la clase
   * @return Información de la unidad
   * @throws PersistenciaExcepcion No se encontró la unidad
   */
  public UniUnidad consultarUnidad(Integer idUnidad, Integer idClase)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT uni.*  ")
            .append("FROM uni_unidad uni  ")
            .append("       INNER JOIN est_estructura est ON uni.est_ideregistro = est.est_ideregistro  ")
            .append("       INNER JOIN esem_estempresa esem ON est.est_ideregistro = esem.est_ideregistro  ")
            .append("WHERE est.cla_ideregistro = :idclase  ")
            .append("  AND uni.uni_ideregistro = :idunidad  ")
            .append("  AND esem.emp_ideregistro = :idempresa");
    parametros.put("idclase", idClase);
    parametros.put("idunidad", idUnidad);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsultaSimple(sql, parametros, new ConsultaAdaptador<UniUnidad>()
    {
      @Override
      public UniUnidad siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getUniUnidad(rs);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_UNIDAD);
      }

    });
  }
}
