package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class PerPeriodoCRUD extends GenericoCRUD
{

  public PerPeriodoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(PerPeriodo perPeriodo)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.per_periodo(per_ideorden,cic_ideregistro,per_nombre,per_estado,per_blofecha,per_fecinicial,per_fecfinal,per_fecvence,per_fecsuspens,usu_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, perPeriodo.getPerIdeorden());
      Object cicIderegistro = (perPeriodo.getCicIderegistro() == null) ? null : perPeriodo.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
      sentencia.setObject(i++, perPeriodo.getPerNombre());
      sentencia.setObject(i++, perPeriodo.getPerEstado());
      sentencia.setObject(i++, perPeriodo.getPerBlofecha());
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecinicial()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecfinal()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecvence()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecsuspens()));
      sentencia.setObject(i++, perPeriodo.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        perPeriodo.setPerIderegistro(rs.getInt("per_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(PerPeriodo perPeriodo)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.per_periodo(per_ideregistro,per_ideorden,cic_ideregistro,per_nombre,per_estado,per_blofecha,per_fecinicial,per_fecfinal,per_fecvence,per_fecsuspens,usu_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, perPeriodo.getPerIderegistro());
      sentencia.setObject(i++, perPeriodo.getPerIdeorden());
      Object cicIderegistro = (perPeriodo.getCicIderegistro() == null) ? null : perPeriodo.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
      sentencia.setObject(i++, perPeriodo.getPerNombre());
      sentencia.setObject(i++, perPeriodo.getPerEstado());
      sentencia.setObject(i++, perPeriodo.getPerBlofecha());
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecinicial()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecfinal()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecvence()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecsuspens()));
      sentencia.setObject(i++, perPeriodo.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(PerPeriodo perPeriodo)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.per_periodo SET per_ideorden=?,cic_ideregistro=?,per_nombre=?,per_estado=?,per_blofecha=?,per_fecinicial=?,per_fecfinal=?,per_fecvence=?,per_fecsuspens=?,usu_ideregistro=? where per_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, perPeriodo.getPerIdeorden());
      Object cicIderegistro = (perPeriodo.getCicIderegistro() == null) ? null : perPeriodo.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
      sentencia.setObject(i++, perPeriodo.getPerNombre());
      sentencia.setObject(i++, perPeriodo.getPerEstado());
      sentencia.setObject(i++, perPeriodo.getPerBlofecha());
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecinicial()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecfinal()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecvence()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(perPeriodo.getPerFecsuspens()));
      sentencia.setObject(i++, perPeriodo.getUsuIderegistro());
      sentencia.setObject(i++, perPeriodo.getPerIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<PerPeriodo> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<PerPeriodo> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.per_periodo";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getPerPeriodo(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public PerPeriodo consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    PerPeriodo obj = null;
    try {

      String sql = "SELECT * FROM public.per_periodo WHERE per_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getPerPeriodo(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static PerPeriodo getPerPeriodo(ResultSet rs)
          throws PersistenciaExcepcion
  {
    PerPeriodo perPeriodo = new PerPeriodo();
    perPeriodo.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
    perPeriodo.setPerIdeorden(getObject("per_ideorden", Integer.class, rs));
    CicCiclo cic_ideregistro = new CicCiclo();
    cic_ideregistro.setCicIderegistro(getObject("cic_ideregistro", Integer.class, rs));
    perPeriodo.setCicIderegistro(cic_ideregistro);
    perPeriodo.setPerNombre(getObject("per_nombre", String.class, rs));
    perPeriodo.setPerEstado(getObject("per_estado", String.class, rs));
    perPeriodo.setPerBlofecha(getObject("per_blofecha", String.class, rs));
    perPeriodo.setPerFecinicial(getObject("per_fecinicial", Timestamp.class, rs));
    perPeriodo.setPerFecfinal(getObject("per_fecfinal", Timestamp.class, rs));
    perPeriodo.setPerFecvence(getObject("per_fecvence", Timestamp.class, rs));
    perPeriodo.setPerFecsuspens(getObject("per_fecsuspens", Timestamp.class, rs));
    perPeriodo.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return perPeriodo;
  }

  public static PerPeriodo getPerPeriodo(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    PerPeriodo perPeriodo = new PerPeriodo();
    Integer columna = columnas.get(alias + "_per_ideregistro");
    if (columna != null) {
      perPeriodo.setPerIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_per_ideorden");
    if (columna != null) {
      perPeriodo.setPerIdeorden(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_cic_ideregistro");
    if (columna != null) {
      CicCiclo cic_ideregistro = new CicCiclo();
      cic_ideregistro.setCicIderegistro(getObject(columna, Integer.class, rs));
      perPeriodo.setCicIderegistro(cic_ideregistro);
    }
    columna = columnas.get(alias + "_per_nombre");
    if (columna != null) {
      perPeriodo.setPerNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_per_estado");
    if (columna != null) {
      perPeriodo.setPerEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_per_blofecha");
    if (columna != null) {
      perPeriodo.setPerBlofecha(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_per_fecinicial");
    if (columna != null) {
      perPeriodo.setPerFecinicial(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_per_fecfinal");
    if (columna != null) {
      perPeriodo.setPerFecfinal(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_per_fecvence");
    if (columna != null) {
      perPeriodo.setPerFecvence(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_per_fecsuspens");
    if (columna != null) {
      perPeriodo.setPerFecsuspens(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      perPeriodo.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    return perPeriodo;
  }

}
