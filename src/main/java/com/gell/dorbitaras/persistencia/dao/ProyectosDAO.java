package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class ProyectosDAO extends ProyectosCRUD {

  public ProyectosDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta todos los proyectos
   *
   * @return Lista de proyectos dependiendo de la empresa
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<Proyectos> consultarProyectos()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT DISTINCT pro.* FROM proyectos pro ")
            .append("    INNER JOIN empresas emp ")
            .append("      ON emp.empresa_cod = pro.proyecto_codemp ")
            .append("    INNER JOIN aseo.rgta_rgitarifario rgta ")
            .append("      ON emp.empresa_sevemp = :ideEmpresa ");
    parametros.put("ideEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> getProyectos(rs));
  }

}
