/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaTaRecalcAprvCRUD;
import com.gell.dorbitaras.persistencia.entidades.VrtaTaRecalcAprv;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaTaRecalcAprvDAO extends VrtaTaRecalcAprvCRUD {

    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaTaRecalcAprvDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaVariableHist Datos de la variable a insertar.
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public void insertar(VrtaTaRecalcAprv vrtaTaRecalcAprv)
            throws PersistenciaExcepcion {
        super.insertar(vrtaTaRecalcAprv);
    }

    public Integer obtenerValorDeUltimaInsercion(
            Integer idPeriodo, String tipo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select	max(vtch.numero_actualizacion) as numero_actualizacion ")
                .append(" from 	aseo.vrta_qa_total_historico vtch  ")
                .append(" where vtch.periodo_ejecucion = :idPeriodo ")
                .append(" and vtch.tipo = :tipo; ");
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("numero_actualizacion", Integer.class, rs);
        }).get(0);
    }
    
    public List<Integer> validarSiHayTACalculado(
            Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select numero_actualizacion  ")
                .append(" from aseo.vrta_ta_recalc_aprv aa ")
                .append(" where aa.periodo_recalculo = :idPeriodo ");
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("numero_actualizacion", Integer.class, rs);
        });
    }
    
    
    public Integer obtenerValorDeUltimaInsercionTabla(
            Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select	max(vtch.numero_actualizacion) as numero_actualizacion ")
                .append(" from 	aseo.vrta_ta_recalc_aprv vtch  ")
                .append(" where vtch.periodo_recalculo = :idPeriodo ");
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("numero_actualizacion", Integer.class, rs);
        }).get(0);
    }
    
        
    public List<VrtaTaRecalcAprv> obtenerHistorico(Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select numero_actualizacion, fecha_registro ")
                .append(" from aseo.vrta_qa_total_historico vqth  ")
                .append(" where periodo_ejecucion = :idPeriodo ")
                .append(" and primer_qa  = true ")
                .append(" and tipo = 'R' ")
                .append(" order by numero_actualizacion; ");
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaTaRecalcAprv(getObject("numero_actualizacion", Integer.class, rs),
                    null,
                    null,
                    null,
                    getObject("fecha_registro", Timestamp.class, rs),
                    null,
                    null);
        });
    }
}
