package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class CicCicloCRUD extends GenericoCRUD {


    public CicCicloCRUD(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    
    public void insertar(CicCiclo cicCiclo) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO public.cic_ciclo(cic_nombre,cic_diainicia,cic_diafinaliza,cic_estado,cic_periodos,cic_anoactual,usu_ideregistro) VALUES (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++,cicCiclo.getCicNombre()); 
sentencia.setObject(i++,cicCiclo.getCicDiainicia()); 
sentencia.setObject(i++,cicCiclo.getCicDiafinaliza()); 
sentencia.setObject(i++,cicCiclo.getCicEstado()); 
sentencia.setObject(i++,cicCiclo.getCicPeriodos()); 
sentencia.setObject(i++,cicCiclo.getCicAnoactual()); 
sentencia.setObject(i++,cicCiclo.getUsuIderegistro()); 
 
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                cicCiclo.setCicIderegistro(rs.getInt("cic_ideregistro"));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void insertarTodos(CicCiclo cicCiclo) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO public.cic_ciclo(cic_ideregistro,cic_nombre,cic_diainicia,cic_diafinaliza,cic_estado,cic_periodos,cic_anoactual,usu_ideregistro) VALUES (?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,cicCiclo.getCicIderegistro()); 
sentencia.setObject(i++,cicCiclo.getCicNombre()); 
sentencia.setObject(i++,cicCiclo.getCicDiainicia()); 
sentencia.setObject(i++,cicCiclo.getCicDiafinaliza()); 
sentencia.setObject(i++,cicCiclo.getCicEstado()); 
sentencia.setObject(i++,cicCiclo.getCicPeriodos()); 
sentencia.setObject(i++,cicCiclo.getCicAnoactual()); 
sentencia.setObject(i++,cicCiclo.getUsuIderegistro()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
          LogUtil.error(e);
          throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void editar(CicCiclo cicCiclo) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "UPDATE public.cic_ciclo SET cic_nombre=?,cic_diainicia=?,cic_diafinaliza=?,cic_estado=?,cic_periodos=?,cic_anoactual=?,usu_ideregistro=? where cic_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,cicCiclo.getCicNombre()); 
sentencia.setObject(i++,cicCiclo.getCicDiainicia()); 
sentencia.setObject(i++,cicCiclo.getCicDiafinaliza()); 
sentencia.setObject(i++,cicCiclo.getCicEstado()); 
sentencia.setObject(i++,cicCiclo.getCicPeriodos()); 
sentencia.setObject(i++,cicCiclo.getCicAnoactual()); 
sentencia.setObject(i++,cicCiclo.getUsuIderegistro()); 
sentencia.setObject(i++,cicCiclo.getCicIderegistro()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
             desconectar(sentencia);
        }
    }

    
    public List<CicCiclo> consultar() throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<CicCiclo> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM public.cic_ciclo";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getCicCiclo(rs));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
             desconectar(sentencia);
        }
        return lista;

    }

    
    public CicCiclo consultar(long id) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        CicCiclo obj = null;
        try {

            String sql = "SELECT * FROM public.cic_ciclo WHERE cic_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getCicCiclo(rs);
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
              desconectar(sentencia);
        }
        return obj;
    }

    public static CicCiclo getCicCiclo(ResultSet rs) throws PersistenciaExcepcion{
      CicCiclo cicCiclo = new CicCiclo();
cicCiclo.setCicIderegistro( getObject("cic_ideregistro",Integer.class,rs));
cicCiclo.setCicNombre( getObject("cic_nombre",String.class,rs));
cicCiclo.setCicDiainicia( getObject("cic_diainicia",Integer.class,rs));
cicCiclo.setCicDiafinaliza( getObject("cic_diafinaliza",Integer.class,rs));
cicCiclo.setCicEstado( getObject("cic_estado",String.class,rs));
cicCiclo.setCicPeriodos( getObject("cic_periodos",Integer.class,rs));
cicCiclo.setCicAnoactual( getObject("cic_anoactual",Integer.class,rs));
cicCiclo.setUsuIderegistro( getObject("usu_ideregistro",Integer.class,rs));

      return cicCiclo;
    }

    public static void getCicCiclo(ResultSet rs,Map<String, Integer> columnas,CicCiclo cicCiclo) throws PersistenciaExcepcion{
      Integer columna = columnas.get("cic_ciclo_cic_ideregistro");if( columna != null ){cicCiclo.setCicIderegistro( getObject(columna,Integer.class,rs));
} columna = columnas.get("cic_ciclo_cic_nombre");if( columna != null ){cicCiclo.setCicNombre( getObject(columna,String.class,rs));
} columna = columnas.get("cic_ciclo_cic_diainicia");if( columna != null ){cicCiclo.setCicDiainicia( getObject(columna,Integer.class,rs));
} columna = columnas.get("cic_ciclo_cic_diafinaliza");if( columna != null ){cicCiclo.setCicDiafinaliza( getObject(columna,Integer.class,rs));
} columna = columnas.get("cic_ciclo_cic_estado");if( columna != null ){cicCiclo.setCicEstado( getObject(columna,String.class,rs));
} columna = columnas.get("cic_ciclo_cic_periodos");if( columna != null ){cicCiclo.setCicPeriodos( getObject(columna,Integer.class,rs));
} columna = columnas.get("cic_ciclo_cic_anoactual");if( columna != null ){cicCiclo.setCicAnoactual( getObject(columna,Integer.class,rs));
} columna = columnas.get("cic_ciclo_usu_ideregistro");if( columna != null ){cicCiclo.setUsuIderegistro( getObject(columna,Integer.class,rs));
}
    }
    
    public static void getCicCiclo(ResultSet rs,Map<String, Integer> columnas,CicCiclo cicCiclo,String alias) throws PersistenciaExcepcion{
      Integer columna = columnas.get(alias+"_cic_ideregistro");if( columna != null ){cicCiclo.setCicIderegistro( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_cic_nombre");if( columna != null ){cicCiclo.setCicNombre( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_cic_diainicia");if( columna != null ){cicCiclo.setCicDiainicia( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_cic_diafinaliza");if( columna != null ){cicCiclo.setCicDiafinaliza( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_cic_estado");if( columna != null ){cicCiclo.setCicEstado( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_cic_periodos");if( columna != null ){cicCiclo.setCicPeriodos( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_cic_anoactual");if( columna != null ){cicCiclo.setCicAnoactual( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_usu_ideregistro");if( columna != null ){cicCiclo.setUsuIderegistro( getObject(columna,Integer.class,rs));
}
    }

    public static CicCiclo getCicCiclo(ResultSet rs,Map<String, Integer> columnas) throws PersistenciaExcepcion{
      CicCiclo cicCiclo = new  CicCiclo();
      getCicCiclo( rs, columnas,cicCiclo);
      return cicCiclo;
    }
   
 public static CicCiclo getCicCiclo(ResultSet rs,Map<String, Integer> columnas,String alias) throws PersistenciaExcepcion{
      CicCiclo cicCiclo = new  CicCiclo();
      getCicCiclo( rs,columnas, cicCiclo, alias);
      return cicCiclo;
    }
    
}
