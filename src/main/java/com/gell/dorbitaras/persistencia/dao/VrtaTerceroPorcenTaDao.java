/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaTerceroPorcenTaCRUD;
import com.gell.dorbitaras.persistencia.entidades.VrtaTerceroPorcenTa;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import javax.sql.DataSource;

/**
 *
 * @author JavierRangel
 */
public class VrtaTerceroPorcenTaDao extends VrtaTerceroPorcenTaCRUD {

    public VrtaTerceroPorcenTaDao(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(dataSource, auditoria);
    }

    @Override
    public void insertar(VrtaTerceroPorcenTa vrtaTerceroPorcenTa)
            throws PersistenciaExcepcion {
        super.insertar(vrtaTerceroPorcenTa);
    }

    public void settearPromedioTerceroTa(
            VrtaTerceroPorcenTa variable
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE 	aseo.vrta_tercero_porcentaje_ta ")
                .append(" SET   valor_porcentaje = :valorPorcentaje , ")
                .append("       numero_actualizacion_ta = :numeroActualizacionTa  ")
                .append(" WHERE periodo_recalculo = :periodoRecalculo ")
                .append(" AND   ter_ideregistro = :idTercero  ")
                .append(" AND   numero_actualizacion_tercero = (  ")    
      		.append(" 	select 	max(numero_actualizacion_tercero)")
		.append(" 	from    aseo.vrta_tercero_porcentaje_ta vtpt ")
      		.append(" 	where   vtpt.periodo_recalculo = :periodoRecalculo")
      		.append(" 	and     vtpt.ter_ideregistro = :idTercero ")	
      		.append(" 	);");
        parametros.put("valorPorcentaje", variable.getValorPorcentaje());
        parametros.put("numeroActualizacionTa", variable.getNumeroActualizacionTa());
        parametros.put("periodoRecalculo", variable.getPeriodoRecalculo());
        parametros.put("idTercero", variable.getIdTercero());
        parametros.put("numeroActualizacionTercero", variable.getNumeroActualizacionTercero());
        ejecutarEdicion(sql, parametros);
    }

}
