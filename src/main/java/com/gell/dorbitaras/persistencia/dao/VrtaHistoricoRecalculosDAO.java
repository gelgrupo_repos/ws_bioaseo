/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaHistoricoRecalculosCRUD;
import com.gell.dorbitaras.persistencia.dto.ConceptosCalculadosDTO;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculos;
import com.gell.dorbitaras.persistencia.entidades.VrtaQaTotalHistorico;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaHistoricoRecalculosDAO extends VrtaHistoricoRecalculosCRUD {

    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaHistoricoRecalculosDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaHistoricoRecalculos Datos de la variable a insertar.
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public void insertar(VrtaHistoricoRecalculos vrtaHistoricoRecalculos)
            throws PersistenciaExcepcion {
        if (vrtaHistoricoRecalculos.getVrtaHistoricoRecalculoIde() != null) {
            editar(vrtaHistoricoRecalculos);
            return;
        }
        super.insertar(vrtaHistoricoRecalculos);
    }

    public List<ConceptosCalculadosDTO> obtenerConceptosCalculados(
            Integer numeroActualizacion,
            Integer idPeriodo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * ")
                .append(" from ")
                .append(" ( ")
                .append("  select ")
                .append(" 		vv.varpr_valor as valor, ")
                .append(" 		vhr.con_dinc as con_dinc, ")
                .append(" 		cc.con_alias as con_alias ")
                .append(" from	aseo.vrta_historico_recalculos vhr  ")
                .append(" inner join ")
                .append(" 		aseo.varpr_varperreg vv  ")
                .append(" on		vv.varpr_ideregistro = vhr.varperreg_ide ")
                .append(" inner join  ")
                .append(" 		con_concepto cc  ")
                .append(" on		cc.uni_concepto = vv.con_ideregistro  ")
                .append(" where 	vhr.numero_actualizacion = :numeroActualizacion ")
                .append(" and 	periodo_hijo_ide = :idPeriodo ")
                .append(" union   ")
                .append(" select  ")
                .append(" 		vtra.valor  	as valor, ")
                .append(" 		vtra.con_dinc 	as con_dinc, ")
                .append(" 		'TA_CALCULADO' 	as con_alias ")
                .append(" from	aseo.vrta_ta_recalc_aprv vtra  ")
                .append(" where 	vtra.numero_actualizacion = :numeroActualizacion ")
                .append(" and 	vtra.periodo_recalculo = :idPeriodo ")
                .append(" union  ")
                .append(" select  ")
                .append(" 		vtra.valor  	as valor, ")
                .append(" 		vtra.con_dinc 	as con_dinc, ")
                .append(" 		'TA_ANT_CALCULADO' 	as con_alias ")
                .append(" from	aseo.vrta_ta_recalc_aprv vtra  ")
                .append(" where 	vtra.numero_actualizacion = (:numeroActualizacion - 1) ")
                .append(" and 	vtra.periodo_recalculo = :idPeriodo ")
                .append(" ) as query_1 ")
                .append(" order by con_dinc asc ");
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ConceptosCalculadosDTO(
                    getObject("valor", Double.class, rs),
                    getObject("con_dinc", Boolean.class, rs),
                    getObject("con_alias", String.class, rs));
        });
    }
    
    public List<ConceptosCalculadosDTO> obtenerConceptosCalculadosIniciales(
            Integer numeroActualizacion,
            Integer idPeriodo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * ")
                .append(" from ")
                .append(" ( ")
                .append("  select ")
                .append(" 		vv.varpr_valor as valor, ")
                .append(" 		vhr.con_dinc as con_dinc, ")
                .append(" 		cc.con_alias as con_alias ")
                .append(" from	aseo.vrta_historico_recalculos vhr  ")
                .append(" inner join ")
                .append(" 		aseo.varpr_varperreg vv  ")
                .append(" on		vv.varpr_ideregistro = vhr.varperreg_ide ")
                .append(" inner join  ")
                .append(" 		con_concepto cc  ")
                .append(" on		cc.uni_concepto = vv.con_ideregistro  ")
                .append(" where 	vhr.numero_actualizacion = :numeroActualizacion ")
                .append(" and 	periodo_hijo_ide = :idPeriodo ")
                .append(" union   ")
                .append(" select  ")
                .append(" 		vtra.valor  	as valor, ")
                .append(" 		vtra.con_dinc 	as con_dinc, ")
                .append(" 		'TA_CALCULADO' 	as con_alias ")
                .append(" from	aseo.vrta_ta_recalc_aprv vtra  ")
                .append(" where 	vtra.numero_actualizacion = :numeroActualizacion ")
                .append(" and 	vtra.periodo_recalculo = :idPeriodo ")
                .append(" union  ")
                .append("(select  ")
                .append("   vv.varpr_valor 	as valor, ")
                .append("   false 	as con_dinc, ")
                .append("   'TA_ANT_CALCULADO' 	as con_alias ")
                .append("   from aseo.varpr_varperreg vv  ")
                .append(" where vv.per_ideregistro = :idPeriodo ")
                .append("    and varpr_estado = 'CE' ")
                .append("    and varpr_estado_registro = 'A' ")
                .append("    and con_ideregistro = 2899 ")
                .append("    order by varpr_ideregistro desc ")
                .append("    limit 1) ")
                .append("union ")
                .append("( select  ")
                .append("   vv.varpr_valor 	as valor, ")
                .append("   true 	as con_dinc, ")
                .append("   'TA_ANT_CALCULADO' 	as con_alias ")
                .append("   from aseo.varpr_varperreg vv  ")
                .append("    where vv.per_ideregistro = :idPeriodo ")
                .append("    and varpr_estado = 'CE' ")
                .append("    and varpr_estado_registro = 'A' ")
                .append("    and con_ideregistro = 3897 ")
                .append("    order by varpr_ideregistro desc ")
                .append("    limit 1)    ")
                .append(" ) as query_1 ")
                .append(" order by con_dinc asc ");
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ConceptosCalculadosDTO(
                    getObject("valor", Double.class, rs),
                    getObject("con_dinc", Boolean.class, rs),
                    getObject("con_alias", String.class, rs));
        });
    }

}
