/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.GestionActualizacionCostosCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoIndicadorProductividad;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jhibarra
 */
public class GestionActualizacionCostosDAO extends GestionActualizacionCostosCRUD{
    
    public GestionActualizacionCostosDAO (DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }
    
      /**
   * Método encargado de consultar los meses por periodo
   *
   * @param idArea Identificador del área de prestación
   * @param idPeriodo Identificador del periodo de prestación
   * @param idConcepto Identificador del concepto de prestación
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<VariacionCostosProductividadDTO> consultarVariaciones(Integer idArea,
          Integer idPeriodo, Integer idConcepto)
              throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("select v.varc_ideregistro as id_registro, ")
            .append("   v.varc_valor as valor, ")
            .append("   smper.smper_ideregistro as smper_id_registro, ")
            .append("   'valor' as accessor, ")
            .append("   smper_descripcion as header,  ")
            .append("   v.varc_estado as bandera ")
            .append("  from aseo.varc_varcalculo v ")
            .append("         inner join aseo.smper_semperiodo smper on smper.per_ideregistro = v.per_ideregistro ")
            .append("where v.per_ideregistro = :idPeriodo ")
            .append("  and v.arpr_ideregistro = :idArea ")
            .append("  and v.con_ideregistro = :idConcepto ")
            .append("  and v.emp_ideregistro = :idEmpresa ");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idArea", idArea);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idConcepto", idConcepto);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      VariacionCostosProductividadDTO variacionCostosProductividadDTO = 
              getVariacionesCostos(rs, columns, "");
      return variacionCostosProductividadDTO;
    });
  } 
  
  
  /**
   * Método encargado de consultar los conceptos por liquidación.
   *
   * @return Lista con los conceptos consultados.
   * @throws PersistenciaExcepcion
   */
  public List<ConConceptoIndicadorProductividad> consultarConceptoIndicadorProductividad(
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" select uni_concepto, con_nombre, con_valor, con_alias  ")
            .append(" from con_concepto cc  ")
            .append("  where con_alias  in('SMLMV', 'IPC', 'IPCc', 'IOAMB','VAR_SMLMV','VAR_IPCc','VAR_IOAMB','VAR_IPC')  ");
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ConConceptoIndicadorProductividad>()
    {
      @Override
      public ConConceptoIndicadorProductividad siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        ConConceptoIndicadorProductividad concepto = getConConcepto(rs, columns, "con_concepto1");
        return concepto;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

}
