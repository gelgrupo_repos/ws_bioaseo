package com.gell.dorbitaras.persistencia.dao;

import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.dto.facturar.ValorConceptoDTO;
import com.gell.dorbitaras.persistencia.entidades.RacoRanconcept;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;
import java.sql.Connection;
import java.util.List;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * los conceptos con rango.
 *
 * @author Desarrollador
 */
public class RacoRanconceptDAO extends RacoRanconceptCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public RacoRanconceptDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Super clase.
   *
   * @param cnn Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public RacoRanconceptDAO(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

  /**
   * Método encargado de cambiar el valor del concepto raco al momento de
   * certificar una variable.
   *
   * @param idRegistro Identificador del registro.
   * @param nuevoValor Valor certificado para el concepto.
   * @throws PersistenciaExcepcion
   */
//  public void cambiarValorConcepto(Integer idRegistro, Integer nuevoValor)
//          throws PersistenciaExcepcion
//  {
//    StringBuilder sql = new StringBuilder();
//    sql.append("UPDATE raco_ranconcept ")
//            .append("SET raco_valor = :valorNuevo ")
//            .append("WHERE raco_ideregistr = :idRegistro ");
//    parametros.put("valorNuevo", nuevoValor);
//    parametros.put("idRegistro", idRegistro);
//    ejecutarEdicion(sql, parametros);
//  }

  /**
   * Método encargado de verificar si el concepto tiene rangos.
   *
   * @param idConcepto Identificador del concepto.
   * @return número de registros.
   * @throws PersistenciaExcepcion
   */
  public boolean tieneRangos(Integer idConcepto)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("select count(*) numero from raco_ranconcept raco where raco.uni_concepto=:idconcepto");
    parametros.put("idconcepto", idConcepto);
    return ejecutarConsultaSimple(sql, parametros,
            (rs, columns) -> getObject("numero", Integer.class, rs) > 1);
  }

  /**
   * Consulta los rangos de un concepto dependiendo del valor
   *
   * @param valor Valor del concepto
   * @param IdConcepto Identificador del concepto
   * @return Lista de rangos del concepto
   * @throws PersistenciaExcepcion Error al ejecutar el concepto
   */
  public List<RacoRanconcept> consultarRango(Double valor, Integer IdConcepto)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT raco.* ")
            .append("FROM raco_ranconcept raco ")
            .append("WHERE raco.uni_concepto = :idconcepto  ")
            .append("  AND :valortotal BETWEEN raco.raco_raninicial AND raco.raco_ranfinal");
    parametros.put("idconcepto", IdConcepto);
    parametros.put("valortotal", valor);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getRacoRanconcept(rs));

  }

  /**
   * Consulta los rangos de un concepto dependiendo del valor
   *
   * @param concepto Datos del concepto del concepto
   * @return Lista de rangos del concepto
   * @throws PersistenciaExcepcion
   */
  public RacoRanconcept consultarIdRango(ValorConceptoDTO concepto)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT raco.* ")
            .append("FROM raco_ranconcept raco ")
            .append("WHERE raco.uni_concepto = :idconcepto  ")
            .append("  AND raco.raco_valor = :valortotal");
    parametros.put("idconcepto", concepto.getConcepto().getUniConcepto().getUniIderegistro());
    parametros.put("valortotal", concepto.getValorTotal());
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> getRacoRanconcept(rs));
  }
}
