/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.VrtaQaTotalHistorico;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaQaTotalHistoricoCRUD extends GenericoCRUD {
    public VrtaQaTotalHistoricoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }
    
    public Integer insertar(VrtaQaTotalHistorico vrtaTaCalcHistorico)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_qa_total_historico(numero_actualizacion,periodo_ejecucion, periodo_padre_calculo, fecha_registro, usuario_registro, valor_qa_total, tipo, primer_qa) VALUES (?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object numeroactualizacion = (vrtaTaCalcHistorico.getNumeroactualizacion() == null) ? null : vrtaTaCalcHistorico.getNumeroactualizacion();
            sentencia.setObject(i++, numeroactualizacion);
            Object periodoejecucion = (vrtaTaCalcHistorico.getPeriodoejecucion() == null) ? null : vrtaTaCalcHistorico.getPeriodoejecucion();
            sentencia.setObject(i++, periodoejecucion);
            Object periodopadrecalculo = (vrtaTaCalcHistorico.getPeriodopadrecalculo() == null) ? null : vrtaTaCalcHistorico.getPeriodopadrecalculo();
            sentencia.setObject(i++, periodopadrecalculo);
            Object fecharegistro = (vrtaTaCalcHistorico.getFecharegistro() == null) ? null : vrtaTaCalcHistorico.getFecharegistro();
            sentencia.setObject(i++, fecharegistro);
            Object usuarioregistro = (vrtaTaCalcHistorico.getUsuarioregistro() == null) ? null : vrtaTaCalcHistorico.getUsuarioregistro();
            sentencia.setObject(i++, usuarioregistro);
            Object valorqa = (vrtaTaCalcHistorico.getValorqatotal() == null) ? null : vrtaTaCalcHistorico.getValorqatotal();
            sentencia.setObject(i++, valorqa);
            Object tipo = (vrtaTaCalcHistorico.getTipo() == null) ? null : vrtaTaCalcHistorico.getTipo();
            sentencia.setObject(i++, tipo);
            Object primerQa = (vrtaTaCalcHistorico.getPrimerQa() == null) ? null : vrtaTaCalcHistorico.getPrimerQa();
            sentencia.setObject(i++, primerQa);
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                vrtaTaCalcHistorico.setVrtaqahistideregistro(rs.getInt("vrta_qa_hist_ideregistro"));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
            return vrtaTaCalcHistorico.getVrtaqahistideregistro();
        }
    }
    
    public void editar(VrtaQaTotalHistorico vrtaTaCalcHistorico)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.vrta_qa_total_historico SET vrta_hist_ideregistro=?,numero_actualizacion=?,vrta_ideregistro=?,vrta_hist_fecgrabacion=?";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object vrtatacalcideregistro = (vrtaTaCalcHistorico.getVrtaqahistideregistro() == null) ? null : vrtaTaCalcHistorico.getVrtaqahistideregistro();
            sentencia.setObject(i++, vrtatacalcideregistro);
            Object numeroactualizacion = (vrtaTaCalcHistorico.getNumeroactualizacion() == null) ? null : vrtaTaCalcHistorico.getNumeroactualizacion();
            sentencia.setObject(i++, numeroactualizacion);
            Object periodoejecucion = (vrtaTaCalcHistorico.getPeriodoejecucion() == null) ? null : vrtaTaCalcHistorico.getPeriodoejecucion();
            sentencia.setObject(i++, periodoejecucion);
            Object periodopadrecalculo = (vrtaTaCalcHistorico.getPeriodopadrecalculo() == null) ? null : vrtaTaCalcHistorico.getPeriodopadrecalculo();
            sentencia.setObject(i++, periodopadrecalculo);
            Object fecharegistro = (vrtaTaCalcHistorico.getFecharegistro() == null) ? null : vrtaTaCalcHistorico.getFecharegistro();
            sentencia.setObject(i++, fecharegistro);
            Object usuarioregistro = (vrtaTaCalcHistorico.getUsuarioregistro() == null) ? null : vrtaTaCalcHistorico.getUsuarioregistro();
            sentencia.setObject(i++, usuarioregistro);
            Object valorta = (vrtaTaCalcHistorico.getValorqatotal() == null) ? null : vrtaTaCalcHistorico.getValorqatotal();
            sentencia.setObject(i++, valorta);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public List<VrtaQaTotalHistorico> consultar()
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<VrtaQaTotalHistorico> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.vrta_qa_total_historico";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVrtaTaCalcHistorico(rs));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return lista;
    }

    public VrtaQaTotalHistorico consultar(long id)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        VrtaQaTotalHistorico obj = null;
        try {

            String sql = "SELECT * FROM aseo.vrta_qa_total_historico WHERE vrta_qa_hist_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVrtaTaCalcHistorico(rs);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return obj;
    }

    public static VrtaQaTotalHistorico getVrtaTaCalcHistorico(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaQaTotalHistorico vrtaTaCalcHistorico = new VrtaQaTotalHistorico();
        vrtaTaCalcHistorico.setVrtaqahistideregistro(getObject("vrta_qa_hist_ideregistro", Integer.class, rs));
        vrtaTaCalcHistorico.setNumeroactualizacion(getObject("numero_actualizacion", Integer.class, rs));
        vrtaTaCalcHistorico.setPeriodoejecucion(getObject("periodo_ejecucion", Integer.class, rs));
        vrtaTaCalcHistorico.setPeriodopadrecalculo(getObject("periodo_padre_calculo", Integer.class, rs));
        vrtaTaCalcHistorico.setFecharegistro(getObject("fecha_registro", Timestamp.class, rs));
        vrtaTaCalcHistorico.setUsuarioregistro(getObject("usuario_registro", Integer.class, rs));
        vrtaTaCalcHistorico.setValorqatotal(getObject("valor_qa_total", Double.class, rs));
        return vrtaTaCalcHistorico;
    }
    
}
