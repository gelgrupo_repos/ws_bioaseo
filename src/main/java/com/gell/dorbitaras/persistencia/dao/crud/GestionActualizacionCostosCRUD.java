/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConceptoIndicadorProductividad;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jhibarra
 */
public class GestionActualizacionCostosCRUD extends GenericoCRUD {

    public GestionActualizacionCostosCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public static VariacionCostosProductividadDTO getVariacionesCostos(ResultSet rs, Map<String, Integer> columnas, String alias)
            throws PersistenciaExcepcion {
        VariacionCostosProductividadDTO variacionCostosProductividadDTO = new VariacionCostosProductividadDTO();
        Integer columna = columnas.get("smper_semperiodo1_smper_id_registro");
        if (columna != null) {
            variacionCostosProductividadDTO.setPerIdRegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_id_registro");
        if (columna != null) {
            variacionCostosProductividadDTO.setIdRegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_valor");
        if (columna != null) {
            variacionCostosProductividadDTO.setValor(getObject(columna, Float.class, rs));
        }
        columna = columnas.get("1_accessor");
        if (columna != null) {
            variacionCostosProductividadDTO.setAccessor(getObject(columna, String.class, rs));
        }
        columna = columnas.get("smper_semperiodo1_header");
        if (columna != null) {
            variacionCostosProductividadDTO.setHeader(getObject(columna, String.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_bandera");
        if (columna != null) {
            variacionCostosProductividadDTO.setBandera(getObject(columna, String.class, rs));
        }
        return variacionCostosProductividadDTO;
    }
    
      
  public static ConConceptoIndicadorProductividad getConConcepto(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ConConceptoIndicadorProductividad conConcepto = new ConConceptoIndicadorProductividad();
    Integer columna = columnas.get(alias + "_uni_concepto");
    if (columna != null) {
      conConcepto.setUniConcepto(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_nombre");
    if (columna != null) {
      conConcepto.setConNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_alias");
    if (columna != null) {
      conConcepto.setConAlias(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_valor");
    if (columna != null) {
      conConcepto.setConValor(getObject(columna, Double.class, rs));
    }
    return conConcepto;
  }
  
}
