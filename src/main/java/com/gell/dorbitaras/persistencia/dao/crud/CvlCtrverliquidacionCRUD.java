package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class CvlCtrverliquidacionCRUD extends GenericoCRUD
{

  public CvlCtrverliquidacionCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(CvlCtrverliquidacion cvlCtrverliquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO public.cvl_ctrverliquidacion(hliq_ideregistr,uni_liquidacion,cvl_fecha,usu_ideregistro) VALUES (:hliq_ideregistr,:uni_liquidacion,:cvl_fecha,:usu_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      sentencia.setObject("hliq_ideregistr", cvlCtrverliquidacion.getHliqIderegistr());
      sentencia.setObject("uni_liquidacion", cvlCtrverliquidacion.getUniLiquidacion());
      sentencia.setObject("cvl_fecha", DateUtil.parseTimestamp(cvlCtrverliquidacion.getCvlFecha()));
      sentencia.setObject("usu_ideregistro", cvlCtrverliquidacion.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        cvlCtrverliquidacion.setCvlIderegistro(rs.getLong("cvl_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(CvlCtrverliquidacion cvlCtrverliquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.cvl_ctrverliquidacion(cvl_ideregistro,hliq_ideregistr,uni_liquidacion,cvl_fecha,usu_ideregistro) VALUES (:cvl_ideregistro,:hliq_ideregistr,:uni_liquidacion,:cvl_fecha,:usu_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("cvl_ideregistro", cvlCtrverliquidacion.getCvlIderegistro());
      sentencia.setObject("hliq_ideregistr", cvlCtrverliquidacion.getHliqIderegistr());
      sentencia.setObject("uni_liquidacion", cvlCtrverliquidacion.getUniLiquidacion());
      sentencia.setObject("cvl_fecha", DateUtil.parseTimestamp(cvlCtrverliquidacion.getCvlFecha()));
      sentencia.setObject("usu_ideregistro", cvlCtrverliquidacion.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(CvlCtrverliquidacion cvlCtrverliquidacion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.cvl_ctrverliquidacion SET hliq_ideregistr=:hliq_ideregistr,uni_liquidacion=:uni_liquidacion,cvl_fecha=:cvl_fecha,usu_ideregistro=:usu_ideregistro where cvl_ideregistro = :cvl_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("hliq_ideregistr", cvlCtrverliquidacion.getHliqIderegistr());
      sentencia.setObject("uni_liquidacion", cvlCtrverliquidacion.getUniLiquidacion());
      sentencia.setObject("cvl_fecha", DateUtil.parseTimestamp(cvlCtrverliquidacion.getCvlFecha()));
      sentencia.setObject("usu_ideregistro", cvlCtrverliquidacion.getUsuIderegistro());
      sentencia.setObject("cvl_ideregistro", cvlCtrverliquidacion.getCvlIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<CvlCtrverliquidacion> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<CvlCtrverliquidacion> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.cvl_ctrverliquidacion";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getCvlCtrverliquidacion(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public CvlCtrverliquidacion consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    CvlCtrverliquidacion obj = null;
    try {

      String sql = "SELECT * FROM public.cvl_ctrverliquidacion WHERE cvl_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getCvlCtrverliquidacion(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static CvlCtrverliquidacion getCvlCtrverliquidacion(ResultSet rs)
          throws PersistenciaExcepcion
  {
    CvlCtrverliquidacion cvlCtrverliquidacion = new CvlCtrverliquidacion();
    cvlCtrverliquidacion.setCvlIderegistro(getObject("cvl_ideregistro", Long.class, rs));
    cvlCtrverliquidacion.setHliqIderegistr(getObject("hliq_ideregistr", Long.class, rs));
    cvlCtrverliquidacion.setUniLiquidacion(getObject("uni_liquidacion", Integer.class, rs));
    cvlCtrverliquidacion.setCvlFecha(getObject("cvl_fecha", Timestamp.class, rs));
    cvlCtrverliquidacion.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return cvlCtrverliquidacion;
  }

  public static CvlCtrverliquidacion getCvlCtrverliquidacion(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    CvlCtrverliquidacion cvlCtrverliquidacion = new CvlCtrverliquidacion();
    Integer columna = columnas.get(alias + "_cvl_ideregistro");
    if (columna != null) {
      cvlCtrverliquidacion.setCvlIderegistro(getObject(columna, Long.class, rs));
    }
    columna = columnas.get(alias + "_hliq_ideregistr");
    if (columna != null) {
      cvlCtrverliquidacion.setHliqIderegistr(getObject(columna, Long.class, rs));
    }
    columna = columnas.get(alias + "_uni_liquidacion");
    if (columna != null) {
      cvlCtrverliquidacion.setUniLiquidacion(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_cvl_fecha");
    if (columna != null) {
      cvlCtrverliquidacion.setCvlFecha(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      cvlCtrverliquidacion.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    return cvlCtrverliquidacion;
  }

}
