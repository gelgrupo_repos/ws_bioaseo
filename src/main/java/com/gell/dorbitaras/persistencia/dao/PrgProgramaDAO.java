package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EPrograma;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.PrgProgramaCRUD;
import java.sql.ResultSet;
import java.util.List;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.entidades.PrgPrograma;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.Map;

public class PrgProgramaDAO extends PrgProgramaCRUD
{

  public PrgProgramaDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Verifica que la ruta del menú exista dentro del perfil del usuario
   *
   * @param ruta Url a validar
   * @return 0 No existe 1 Existe
   * @throws PersistenciaExcepcion Error al ejecutar la consulta
   */
  public Integer exiteURL(String ruta)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT prg.prg_ideregistro idprograma ")
            .append("FROM public.prg_programa prg ")
            .append("       INNER JOIN public.oppf_opcperfil oppf ")
            .append("                  ON oppf.prg_ideregistro = prg.prg_ideregistro ")
            .append("       INNER JOIN usem_usuempresa usem ")
            .append("                  ON usem.pfi_ideregistro = oppf.pfi_ideregistro ")
            .append("WHERE prg.prg_localiza ILIKE :url ")
            .append("  AND usem.usu_ideregistro = :idusuario ")
            .append("  AND usem.emp_ideregistro = :idempresa ");
    parametros.put("url", ruta);
    parametros.put("idusuario", auditoria.getIdUsuario());
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsultaSimple(sql, parametros, new ConsultaAdaptador<Integer>()
    {
      @Override
      public Integer siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getObject("idprograma", Integer.class, rs);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_PERMISO);
      }

    });
  }

}
