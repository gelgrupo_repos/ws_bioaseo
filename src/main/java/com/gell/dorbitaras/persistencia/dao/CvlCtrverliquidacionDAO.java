package com.gell.dorbitaras.persistencia.dao;

import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;

public class CvlCtrverliquidacionDAO extends CvlCtrverliquidacionCRUD {

  public CvlCtrverliquidacionDAO(DataSource datasource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
     super(datasource, auditoria);
  }
}
