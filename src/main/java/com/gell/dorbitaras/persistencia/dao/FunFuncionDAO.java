package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.FunFuncionCRUD;
import com.gell.dorbitaras.persistencia.dto.facturar.ArgumentoDTO;
import com.gell.dorbitaras.persistencia.entidades.FunFuncion;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import static com.gell.estandar.persistencia.abstracto.GenericoCRUD.getObject;
import javax.sql.DataSource;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public class FunFuncionDAO extends FunFuncionCRUD
{

  public FunFuncionDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  public FunFuncionDAO(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

  /**
   * Ejecuta una función y le asigna los parámetros
   *
   * @param funcion Información de la función a ejecutar
   * @param argumentos Parámetros de la función a ejectar
   * @return Lista de los registros
   * @throws PersistenciaExcepcion Error al ejecutar la consulta
   */
  public Double ejecutarFuncion(FunFuncion funcion, ArgumentoDTO argumentos)
          throws PersistenciaExcepcion
  {
    parametros.putAll(argumentos.getParametros());
    return ejecutarConsultaSimple(new StringBuilder(funcion.getFunSql()),
            parametros, new ConsultaAdaptador<Double>()
    {
      @Override
      public Double siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getObject("valor", Double.class, rs);
      }

      @Override
      public void error(SQLException ex)
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_FUNCION, funcion.getFunNombre() + " Mensaje: " + ex.getMessage());
      }
    });
  }
}
