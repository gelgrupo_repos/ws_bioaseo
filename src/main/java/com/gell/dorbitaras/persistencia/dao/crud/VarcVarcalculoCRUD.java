package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.dto.ValorCalculadoVariacionDTO;
import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;

public class VarcVarcalculoCRUD extends GenericoCRUD {

    public VarcVarcalculoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public void insertar(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        ResultSet rs=null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.varc_varcalculo(per_ideregistro,con_ideregistro,arpr_ideregistro,usu_ideregistro_gb,varc_feccerficicacion,varc_fecgrabacion,usu_ideregistro_cer,varc_valor,varc_descripcion,varc_estado,emp_ideregistro,varc_estadoregistro,raco_ideregistro, varc_estado_variacion, varc_id_mes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object perIderegistro = (varcVarcalculo.getPerIderegistro() == null) ? null : varcVarcalculo.getPerIderegistro().getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object conIderegistro = (varcVarcalculo.getConIderegistro() == null) ? null : varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object arprIderegistro = (varcVarcalculo.getArprIderegistro() == null) ? null : varcVarcalculo.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFeccerficicacion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFecgrabacion()));
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroCer());
            sentencia.setObject(i++, varcVarcalculo.getVarcValor());
            sentencia.setObject(i++, varcVarcalculo.getVarcDescripcion());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstado());
            sentencia.setObject(i++, varcVarcalculo.getEmpIderegistro());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstadoregistro());
            Object racoIderegistro = (varcVarcalculo.getRacoIderegistro() == null) ? null : varcVarcalculo.getRacoIderegistro().getRacoIderegistr();
            sentencia.setObject(i++, racoIderegistro);
            sentencia.setObject(i++, (varcVarcalculo.getVarcEstadoVariacion() == null) ? null : varcVarcalculo.getVarcEstadoVariacion());
            sentencia.setObject(i++, (varcVarcalculo.getVarcIdMes() == null) ? null : varcVarcalculo.getVarcIdMes());

            sentencia.executeUpdate();
            rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                varcVarcalculo.setVarcIderegistro(rs.getInt("varc_ideregistro"));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia,rs);
        }
    }
    
    public Integer agregarRegistro(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        Integer id = -1;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.varc_varcalculo(per_ideregistro,con_ideregistro,arpr_ideregistro,usu_ideregistro_gb,varc_feccerficicacion,varc_fecgrabacion,usu_ideregistro_cer,varc_valor,varc_descripcion,varc_estado,emp_ideregistro,varc_estadoregistro,raco_ideregistro, varc_estado_variacion, varc_id_mes) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object perIderegistro = (varcVarcalculo.getPerIderegistro() == null) ? null : varcVarcalculo.getPerIderegistro().getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object conIderegistro = (varcVarcalculo.getConIderegistro() == null) ? null : varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object arprIderegistro = (varcVarcalculo.getArprIderegistro() == null) ? null : varcVarcalculo.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFeccerficicacion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFecgrabacion()));
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroCer());
            sentencia.setObject(i++, varcVarcalculo.getVarcValor());
            sentencia.setObject(i++, varcVarcalculo.getVarcDescripcion());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstado());
            sentencia.setObject(i++, varcVarcalculo.getEmpIderegistro());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstadoregistro());
            Object racoIderegistro = (varcVarcalculo.getRacoIderegistro() == null) ? null : varcVarcalculo.getRacoIderegistro().getRacoIderegistr();
            sentencia.setObject(i++, racoIderegistro);
            sentencia.setObject(i++, (varcVarcalculo.getVarcEstadoVariacion() == null) ? null : varcVarcalculo.getVarcEstadoVariacion());
            sentencia.setObject(i++, (varcVarcalculo.getVarcIdMes() == null) ? null : varcVarcalculo.getVarcIdMes());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                varcVarcalculo.setVarcIderegistro(rs.getInt("varc_ideregistro"));
                id =  rs.getInt("varc_ideregistro");
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
            return id;
        }
    }

    public void insertarTodos(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.varc_varcalculo(varc_ideregistro,per_ideregistro,con_ideregistro,arpr_ideregistro,usu_ideregistro_gb,varc_feccerficicacion,varc_fecgrabacion,usu_ideregistro_cer,varc_valor,varc_descripcion,varc_estado,emp_ideregistro,varc_estadoregistro,raco_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, varcVarcalculo.getVarcIderegistro());
            Object perIderegistro = (varcVarcalculo.getPerIderegistro() == null) ? null : varcVarcalculo.getPerIderegistro().getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object conIderegistro = (varcVarcalculo.getConIderegistro() == null) ? null : varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object arprIderegistro = (varcVarcalculo.getArprIderegistro() == null) ? null : varcVarcalculo.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFeccerficicacion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFecgrabacion()));
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroCer());
            sentencia.setObject(i++, varcVarcalculo.getVarcValor());
            sentencia.setObject(i++, varcVarcalculo.getVarcDescripcion());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstado());
            sentencia.setObject(i++, varcVarcalculo.getEmpIderegistro());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstadoregistro());
            Object racoIderegistro = (varcVarcalculo.getRacoIderegistro() == null) ? null : varcVarcalculo.getRacoIderegistro().getRacoIderegistr();
            sentencia.setObject(i++, racoIderegistro);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public void editar(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.varc_varcalculo SET per_ideregistro=?,con_ideregistro=?,arpr_ideregistro=?,usu_ideregistro_gb=?,varc_feccerficicacion=?,varc_fecgrabacion=?,usu_ideregistro_cer=?,varc_valor=?,varc_descripcion=?,varc_estado=?,emp_ideregistro=?,varc_estadoregistro=?,raco_ideregistro=? where varc_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
            Object perIderegistro = (varcVarcalculo.getPerIderegistro() == null) ? null : varcVarcalculo.getPerIderegistro().getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object conIderegistro = (varcVarcalculo.getConIderegistro() == null) ? null : varcVarcalculo.getConIderegistro().getUniConcepto().getUniIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object arprIderegistro = (varcVarcalculo.getArprIderegistro() == null) ? null : varcVarcalculo.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFeccerficicacion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(varcVarcalculo.getVarcFecgrabacion()));
            sentencia.setObject(i++, varcVarcalculo.getUsuIderegistroCer());
            sentencia.setObject(i++, varcVarcalculo.getVarcValor());
            sentencia.setObject(i++, varcVarcalculo.getVarcDescripcion());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstado());
            sentencia.setObject(i++, varcVarcalculo.getEmpIderegistro());
            sentencia.setObject(i++, varcVarcalculo.getVarcEstadoregistro());
            Object racoIderegistro = (varcVarcalculo.getRacoIderegistro() == null) ? null : varcVarcalculo.getRacoIderegistro().getRacoIderegistr();
            sentencia.setObject(i++, racoIderegistro);
            sentencia.setObject(i++, varcVarcalculo.getVarcIderegistro());

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public void editarFProductividadIndicadoresAdyacentes(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.varc_varcalculo SET varc_id_concepto_aplicado =? where varc_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
//            sentencia.setObject(i++, (double)varcVarcalculo.getVarcValor());
            sentencia.setObject(i++, varcVarcalculo.getVarcIdConceptoAplicado());
            sentencia.setObject(i++, varcVarcalculo.getVarcIderegistro());
            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public List<VarcVarcalculo> consultar()
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<VarcVarcalculo> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.varc_varcalculo";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVarcVarcalculo(rs));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return lista;

    }

    public VarcVarcalculo consultar(long id)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        VarcVarcalculo obj = null;
        try {

            String sql = "SELECT * FROM aseo.varc_varcalculo WHERE varc_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVarcVarcalculo(rs);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return obj;
    }

    public static VarcVarcalculo getVarcVarcalculo(ResultSet rs)
            throws PersistenciaExcepcion {
        VarcVarcalculo varcVarcalculo = new VarcVarcalculo();
        varcVarcalculo.setVarcIderegistro(getObject("varc_ideregistro", Integer.class, rs));
        PerPeriodo per_ideregistro = new PerPeriodo();
        per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
        varcVarcalculo.setPerIderegistro(per_ideregistro);

        ConConcepto con_ideregistro = new ConConcepto();
        con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
        varcVarcalculo.setConIderegistro(con_ideregistro);
        ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
        arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
        varcVarcalculo.setArprIderegistro(arpr_ideregistro);
        varcVarcalculo.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
        varcVarcalculo.setVarcFeccerficicacion(getObject("varc_feccerficicacion", Timestamp.class, rs));
        varcVarcalculo.setVarcFecgrabacion(getObject("varc_fecgrabacion", Timestamp.class, rs));
        varcVarcalculo.setUsuIderegistroCer(getObject("usu_ideregistro_cer", Integer.class, rs));
        varcVarcalculo.setVarcValor(getObject("varc_valor", Double.class, rs));
        varcVarcalculo.setVarcDescripcion(getObject("varc_descripcion", String.class, rs));
        varcVarcalculo.setVarcEstado(getObject("varc_estado", String.class, rs));
        varcVarcalculo.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
        varcVarcalculo.setVarcEstadoregistro(getObject("varc_estadoregistro", String.class, rs));
        RacoRanconcept raco_ideregistro = new RacoRanconcept();
        raco_ideregistro.setRacoIderegistr(getObject("raco_ideregistro", Integer.class, rs));
        varcVarcalculo.setRacoIderegistro(raco_ideregistro);

        return varcVarcalculo;
    }

    public static VarcVarcalculo getVarcVarcalRaco(ResultSet rs)
            throws PersistenciaExcepcion {
        VarcVarcalculo varcVarcalculo = new VarcVarcalculo();
        varcVarcalculo.setVarcIderegistro(getObject("varc_ideregistro", Integer.class, rs));
        PerPeriodo per_ideregistro = new PerPeriodo();
        per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
        varcVarcalculo.setPerIderegistro(per_ideregistro);

        ConConcepto con_ideregistro = new ConConcepto();
        con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
        varcVarcalculo.setConIderegistro(con_ideregistro);
        ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
        arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
        varcVarcalculo.setArprIderegistro(arpr_ideregistro);
        varcVarcalculo.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
        varcVarcalculo.setVarcFeccerficicacion(getObject("varc_feccerficicacion", Timestamp.class, rs));
        varcVarcalculo.setVarcFecgrabacion(getObject("varc_fecgrabacion", Timestamp.class, rs));
        varcVarcalculo.setUsuIderegistroCer(getObject("usu_ideregistro_cer", Integer.class, rs));
        varcVarcalculo.setVarcValor(getObject("varc_valor", Double.class, rs));
        varcVarcalculo.setVarcDescripcion(getObject("varc_descripcion", String.class, rs));
        varcVarcalculo.setVarcEstado(getObject("varc_estado", String.class, rs));
        varcVarcalculo.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
        varcVarcalculo.setVarcEstadoregistro(getObject("varc_estadoregistro", String.class, rs));
        RacoRanconcept raco_ideregistro = new RacoRanconcept();
        raco_ideregistro.setRacoIderegistr(getObject("raco_ideregistro", Integer.class, rs));
        raco_ideregistro.setRacoRaninicial(getObject("raco_raninicial", Double.class, rs));
        raco_ideregistro.setRacoRanfinal(getObject("raco_ranfinal", Double.class, rs));
        varcVarcalculo.setRacoIderegistro(raco_ideregistro);

        return varcVarcalculo;
    }

    public static VarcVarcalculo getVarcVarcalculo(ResultSet rs, Map<String, Integer> columnas, String alias)
            throws PersistenciaExcepcion {
        VarcVarcalculo varcVarcalculo = new VarcVarcalculo();
        Integer columna = columnas.get(alias + "_varc_ideregistro");
        if (columna != null) {
            varcVarcalculo.setVarcIderegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_per_ideregistro");
        if (columna != null) {
            PerPeriodo per_ideregistro = new PerPeriodo();
            per_ideregistro.setPerIderegistro(getObject(columna, Integer.class, rs));
            varcVarcalculo.setPerIderegistro(per_ideregistro);
        }
        columna = columnas.get(alias + "_con_ideregistro");
        if (columna != null) {
            ConConcepto con_ideregistro = new ConConcepto();
            con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
            varcVarcalculo.setConIderegistro(con_ideregistro);
        }
        columna = columnas.get(alias + "_arpr_ideregistro");
        if (columna != null) {
            ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
            arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
            varcVarcalculo.setArprIderegistro(arpr_ideregistro);
        }
        columna = columnas.get(alias + "_usu_ideregistro_gb");
        if (columna != null) {
            varcVarcalculo.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_varc_feccerficicacion");
        if (columna != null) {
            varcVarcalculo.setVarcFeccerficicacion(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get(alias + "_varc_fecgrabacion");
        if (columna != null) {
            varcVarcalculo.setVarcFecgrabacion(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get(alias + "_usu_ideregistro_cer");
        if (columna != null) {
            varcVarcalculo.setUsuIderegistroCer(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_varc_valor");
        if (columna != null) {
            varcVarcalculo.setVarcValor(getObject(columna, Double.class, rs));
        }
        columna = columnas.get(alias + "_varc_descripcion");
        if (columna != null) {
            varcVarcalculo.setVarcDescripcion(getObject(columna, String.class, rs));
        }
        columna = columnas.get(alias + "_varc_estado");
        if (columna != null) {
            varcVarcalculo.setVarcEstado(getObject(columna, String.class, rs));
        }
        columna = columnas.get(alias + "_emp_ideregistro");
        if (columna != null) {
            varcVarcalculo.setEmpIderegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_varc_estadoregistro");
        if (columna != null) {
            varcVarcalculo.setVarcEstadoregistro(getObject(columna, String.class, rs));
        }
        columna = columnas.get(alias + "_raco_ideregistro");
        if (columna != null) {
            RacoRanconcept raco_ideregistro = new RacoRanconcept();
            raco_ideregistro.setRacoIderegistr(getObject(columna, Integer.class, rs));
            varcVarcalculo.setRacoIderegistro(raco_ideregistro);
        }
        return varcVarcalculo;
    }

    public static ValorCalculadoVariacionDTO valorInicialCalculado(ResultSet rs, Map<String, Integer> columnas, String alias)
            throws PersistenciaExcepcion {
        ValorCalculadoVariacionDTO valorCalculadoVariacion = new ValorCalculadoVariacionDTO();
        Integer columna;
        columna = columnas.get(alias + "1_varc_ideregistro");
        if (columna != null) {
            valorCalculadoVariacion.setIdeRegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "1_valor");
        if (columna != null) {
            valorCalculadoVariacion.setValor(getObject(columna, Float.class, rs));
        }

        columna = columnas.get(alias + "1_bandera");
        if (columna != null) {
            valorCalculadoVariacion.setBandera(getObject(columna, String.class, rs));
        }
        
        columna = columnas.get(alias + "1_con_ideregistro");
        if (columna != null) {
            valorCalculadoVariacion.setIdConcepto(getObject(columna, Integer.class, rs));
        }
        
        columna = columnas.get(alias + "1_per_ideregistro");
        if (columna != null) {
            valorCalculadoVariacion.setIdPeriodo(getObject(columna, Integer.class, rs));
        }

        return valorCalculadoVariacion;
    }

    public static VariacionCostosProductividadDTO getVariacionesCostos(ResultSet rs, Map<String, Integer> columnas, String alias)
            throws PersistenciaExcepcion {
        VariacionCostosProductividadDTO variacionCostosProductividadDTO = new VariacionCostosProductividadDTO();
        Integer columna = columnas.get("smper_semperiodo1_per_id_registro");
        if (columna != null) {
            variacionCostosProductividadDTO.setPerIdRegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_varc_ideregistro");
        if (columna != null) {
            variacionCostosProductividadDTO.setIdRegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_valor");
        if (columna != null) {
            variacionCostosProductividadDTO.setValor(getObject(columna, Float.class, rs));
        }
        columna = columnas.get("1_accessor");
        if (columna != null) {
            variacionCostosProductividadDTO.setAccessor(getObject(columna, String.class, rs));
        }
        columna = columnas.get("1_header");
        if (columna != null) {
            variacionCostosProductividadDTO.setHeader(getObject(columna, String.class, rs));
        }
        columna = columnas.get("varc_varcalculo1_bandera");
        if (columna != null) {
            variacionCostosProductividadDTO.setBandera(getObject(columna, String.class, rs));
        }
        
        columna = columnas.get("varc_varcalculo1_varc_id_concepto_aplicado");
        if (columna != null) {
            variacionCostosProductividadDTO.setVarceIdConceptoAplicado(getObject(columna, Integer.class, rs));
        }
        
        return variacionCostosProductividadDTO;
    }

}
