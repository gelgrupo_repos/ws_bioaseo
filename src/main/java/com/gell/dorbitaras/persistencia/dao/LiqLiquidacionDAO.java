package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EEstructura;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.LiqLiquidacion;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;
import java.util.List;

public class LiqLiquidacionDAO extends LiqLiquidacionCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public LiqLiquidacionDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta las liquidaciones tipo taras.
   *
   * @return Lista con las liquidaciones.
   * @throws PersistenciaExcepcion
   */
  public List<LiqLiquidacion> consultarLiquidacion()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT liq.* ")
            .append("FROM liq_liquidacion liq ")
            .append("         INNER JOIN uni_unidad uu ")
            .append("                    ON liq.est_liquidacion = uu.est_ideregistro and liq.uni_liquidacion = uu.uni_ideregistro ")
            .append("         INNER JOIN est_estructura ee ")
            .append("                    ON uu.est_ideregistro = ee.est_ideregistro ")
            .append("         INNER JOIN esem_estempresa e ")
            .append("                    ON ee.est_ideregistro = e.est_ideregistro ")
            .append("WHERE liq_estado = :estado ")
            .append("  AND est_estado = :estado ")
            .append("  AND ee.est_ideregistro = :taras ")
            .append("  AND e.emp_ideregistro = :idempresa ");
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("taras", EEstructura.ESTRUCTURA_LIQUIDACIONES);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getLiqLiquidacion(rs));
  }

  /**
   * Consulta si existen historicos para los conceptos de la liquidación
   *
   * @param idLiquidacion Identificador de la liquidación
   * @return Número de columnas consultadas
   * @throws PersistenciaExcepcion
   */
  public Integer validarHistoricoLiquidacion(Integer idLiquidacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT count(*) numero ")
            .append("FROM public.hliq_liquidacion hliq ")
            .append("         INNER JOIN public.hcoli_conliquida hcoli ON hliq.uni_liquidacion = hcoli.uni_liquidacion ")
            .append("         INNER JOIN public.hcon_concepto hcon ON hcon.uni_concepto = hcoli.uni_concepto ")
            .append("         INNER JOIN public.hcore_conrelacio hcore ON hcore.uni_concepto = hcon.uni_concepto ")
            .append("         LEFT JOIN public.hraco_ranconcept hraco ON hraco.uni_concepto = hcon.uni_concepto ")
            .append("WHERE hliq.uni_liquidacion = :idliquidacion;");
    parametros.put("idliquidacion", idLiquidacion);
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getObject("numero", Integer.class, rs);
    });
  }

  /**
   * Consulta si existen historicos para los conceptos de la liquidación
   *
   * @param idLiquidacion Identificador de la liquidación
   * @throws PersistenciaExcepcion
   */
  public void ejecutarHistorico(Integer idLiquidacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT fn_cargar_historico(:idliquidacion, :idempresa)");
    parametros.put("idliquidacion", idLiquidacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return true;
    });
  }

  /**
   * Consulta si existen historicos para los conceptos de la liquidación
   *
   * @param idLiquidacion Identificador de la liquidación
   * @throws PersistenciaExcepcion
   */
  public void modificarVersionHistorico(Integer idLiquidacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE public.liq_liquidacion ")
            .append("SET hliq_ideregistr = (SELECT nextval('public.sq_hliq_ideregistr'::regclass)) ")
            .append("WHERE uni_liquidacion = :idLiquidacion;");
    parametros.put("idLiquidacion", idLiquidacion);
    ejecutarEdicion(sql, parametros);
  }
}
