package com.gell.dorbitaras.persistencia.dao;

import com.fasterxml.jackson.core.type.TypeReference;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.VarprVarperregCRUD;
import static com.gell.dorbitaras.persistencia.dao.crud.VarprVarperregCRUD.getVarprVarperreg;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.dorbitaras.persistencia.entidades.VarprVarperreg;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import javax.sql.DataSource;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import static com.gell.estandar.util.FuncionesDatoUtil.deSeparadoPorGuionesACamelCase;
import static com.gell.estandar.util.FuncionesDatoUtil.jsonObjetoPorDefecto;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * las variables.
 *
 * @author Desarrollador
 */
public class VarprVarperregDAO extends VarprVarperregCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public VarprVarperregDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Registra una variable en la base de datos.
   *
   * @param varpvarperreg Datos de la variable a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public void insertar(VarprVarperreg varpvarperreg)
          throws PersistenciaExcepcion
  {
    if (varpvarperreg.getVarprIderegistro() != null) {
      editar(varpvarperreg);
      return;
    }
    super.insertar(varpvarperreg);
  }

    /**
   * Registra una variable en la base de datos.
   *
   * @param varpvarperreg Datos de la variable a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public Integer insertarAprovechamiento(VarprVarperreg varpvarperreg)
          throws PersistenciaExcepcion
  {
    return super.insertarAprovechamiento(varpvarperreg);
  }

  /**
   * Método Consulta las variables calculadas.
   *
   * @param periodo Identificador del periodo.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Lista de variables calculadas.
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCalculadas(
          Integer areaPrestacion,
          Integer periodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT *, ")
            .append("       (SELECT ARRAY_TO_JSON(ARRAY_AGG(rangos.*)) ")
            .append("        FROM (SELECT varr.varpr_ideregistro, ")
            .append("                     varr.varpr_valor, ")
            .append("                     varr.varpr_estado, ")
            .append("                     (SELECT to_json(ranco.*) ")
            .append("                      FROM (SELECT raco.raco_ideregistr, ")
            .append("                                   raco.raco_raninicial, ")
            .append("                                   raco.raco_ranfinal ")
            .append("                            FROM raco_ranconcept raco ")
            .append("                            WHERE varr.raco_ideregistro = raco.raco_ideregistr) ranco) AS raco_ideregistro ")
            .append("              FROM aseo.varpr_varperreg varr ")
            .append("                       INNER JOIN raco_ranconcept raco ")
            .append("                                  ON varr.raco_ideregistro = raco.raco_ideregistr ")
            .append("              WHERE varr.con_ideregistro = variables.idConcepto ")
            .append("                AND varr.con_ideregistro IS NOT NULL ")
            .append("                AND varr.varpr_fecgrabacion = variables.varpr_fecgrabacion ")
            .append("                AND varr.varpr_estado_registro = :activo ")
            .append("              ORDER BY varr.varpr_fecgrabacion) rangos) lista_rangos, ")
            .append("       (SELECT to_json(array_agg(valores.*)) ")
            .append("        FROM (SELECT varv.varpr_ideregistro, ")
            .append("                     varv.varpr_valor, ")
            .append("                     varv.varpr_estado ")
            .append("              FROM aseo.varpr_varperreg varv ")
            .append("              WHERE varv.raco_ideregistro IS NULL ")
            .append("                AND varv.varpr_fecgrabacion = variables.varpr_fecgrabacion ")
            .append("                AND varv.con_ideregistro = variables.idConcepto ")
            .append("                AND varv.per_ideregistro = :periodo ")
            .append("                AND varv.arpr_ideregistro = :areaprestacion ")
            .append("                AND varv.varpr_estado_registro = :activo ")
            .append("              LIMIT 1) AS valores)                      valor ")
            .append("FROM (SELECT DISTINCT varc.con_ideregistro idConcepto, ")
            .append("                      con.con_nombre       nombreconcepto, ")
            .append("                      con.con_abreviatura      abreviatura, ")
            .append("                      varc.varpr_fecgrabacion, ")
            .append("                      varc.varpr_feccertificacion, ")
            .append("                      varc.varpr_estado, ")
            .append("                      varc.varpr_valor ")
            .append("      FROM aseo.varpr_varperreg varc ")
            .append("               INNER JOIN con_concepto con ON varc.con_ideregistro = con.uni_concepto ")
            .append("      WHERE varc.emp_ideregistro = :idempresa ")
            .append("        AND varc.varpr_estado = :calculado ")
            .append("        AND varc.varpr_estado_registro = :activo ")
            .append("        AND varc.per_ideregistro = :periodo ")
            .append("        AND varc.arpr_ideregistro = :areaprestacion ")
            .append("        AND varc.con_ideregistro not in (select vv.con_ideregistro from aseo.varc_varcalculo vv) ")
            .append("     ) variables order by 2,3;");
    parametros.put("periodo", periodo);
    parametros.put("areaprestacion", areaPrestacion);
    parametros.put("calculado", EEstadoGenerico.CALCULADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      ConConcepto concepto = new ConConcepto()
              .setConNombre(getObject("nombreconcepto", String.class, rs))
              .setConAbreviatura(getObject("abreviatura", String.class, rs))
              .setUniConcepto(new UniUnidad(getObject("idConcepto", Integer.class, rs)));
              
      VarprVarperreg variables = getVarprVarperreg(rs, columns, "varpr_varperreg1")
              .setConIderegistro(concepto);
      String valor = getObject("valor", String.class, rs);
      String rangos = getObject("lista_rangos", String.class, rs);
      List<VarprVarperreg> entidad = new ArrayList<>();
      variables.setListaValores(procesarVariable(valor, entidad));
      variables.setListaRangos(procesarVariable(rangos, entidad));
      return variables;
    });
  }

  /**
   * Método encargado de procesar los datos de las variables dependiendo si
   * tienen rango o no.
   *
   * @param datos Datos consultados.
   * @param resultado Lista de variables.
   * @return Lista de la entidad.
   */
  public List<VarprVarperreg> procesarVariable(String datos, List<VarprVarperreg> resultado)
  {
    if (null != datos) {
      datos = deSeparadoPorGuionesACamelCase(datos);
      resultado = jsonObjetoPorDefecto(datos, new TypeReference<List<VarprVarperreg>>()
      {
      }, null);
    }
    return resultado;
  }

  /**
   * Método encargado de cambiar el estado de la variable.
   *
   * @param variable Datos de la variable.
   * @throws PersistenciaExcepcion
   */
  public void certificarEstadoVariable(
          VarprVarperreg variable
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.varpr_varperreg ")
            .append("SET varpr_feccertificacion = :fecha, ")
            .append(" usu_ideregistro_cer    = :usuario, ")
            .append(" varpr_estado = :certificado ")
            .append("WHERE varpr_ideregistro = :idRegistro ")
            .append("  AND varpr_estado = :calculado;");
    parametros.put("fecha", new Date());
    parametros.put("usuario", auditoria.getIdUsuario());
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("idRegistro", variable.getVarprIderegistro());
    parametros.put("calculado", EEstadoGenerico.CALCULADO);
    ejecutarEdicion(sql, parametros);
  }

  /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCertificadas(
          Integer idArea,
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado;");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }
  
  /**
   * Método encargado de consultar las variables en estado certificado necesarias para los liquidadores de devolucion.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCertificadasDevoluciones(
          Integer idArea,
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado")
            .append("  and con_ideregistro in (select uni_concepto  from con_concepto cc where con_alias in ('FCSE1', 'FCSE2', 'FCSE3', 'FCSE4', 'FCSE5', 'FCSE6', 'FCSPP1', 'FCSPP2', 'FCSGP1', 'FCSGP2') order by uni_concepto);");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }
  
  /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesRecalculo(
          Integer idArea,
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            // .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado;");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }
  
    /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCertificadasTA(
          Integer idArea,
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado")
            .append("  and con_ideregistro in (select uni_concepto  from con_concepto cc where con_alias in ('N', 'ND', 'NA'));");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }
  
      /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @param traerTA bandera para saber si debo ir a traer primer TA.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCertificadasMensualTA(
          Integer idArea,
          Integer idPeriodo,
          boolean traerTA
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    String condicionFinal;
    if(traerTA){
        condicionFinal = "  and con_ideregistro in (select uni_concepto  from con_concepto cc where con_alias in ('TAR-VBAM','TAR-TA','TAMD','VBAMD'));";
    }else{
        condicionFinal = "  and con_ideregistro in (select uni_concepto  from con_concepto cc where con_alias in ('TAR-VBAM','VBAMD'));";
    }
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado")
            .append(condicionFinal);                 
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }


  /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param periodo Datos del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesCertificadas(
          PerPeriodo periodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND varpr_estado_registro = :activo ")
            .append("  AND varpr_estado = :certificado;");
    parametros.put("idPeriodo", periodo.getPerIderegistro());
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<VarprVarperreg>()
    {
      @Override
      public VarprVarperreg siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getVarprVarperreg(rs);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_VARIABLES_SIN_CERTIFICAR_PERIODO, periodo.getPerNombre());
      }
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto, área de prestación identificador del raco.
   *
   * @param concepto Datos de la variable.
   * @param area Datos del área de prestación.
   * @param periodo Datos del periodo.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VarprVarperreg consultarExistente(
          ConConcepto concepto,
          ArprAreaprestacion area,
          PerPeriodo periodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT var.* ")
            .append("FROM aseo.varpr_varperreg var ")
            .append("WHERE var.arpr_ideregistro = :idArea ")
            .append("  AND var.per_ideregistro = :idPeriodo ")
            .append("  AND var.con_ideregistro = :idConcepto ")
            .append("  AND var.varpr_estado_registro = :activo ")
            .append("ORDER BY var.varpr_ideregistro DESC ")
            .append("LIMIT 1;");
    parametros.put("idConcepto", concepto.getUniConcepto().getUniIderegistro());
    parametros.put("idArea", area.getArprIderegistro());
    parametros.put("idPeriodo", periodo.getPerIderegistro());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVarprVarperreg(rs);
    });
  }

  /**
   * Método encargado de inactivar los registros al momento de guardar.
   *
   * @param idRegistro Identificador de la variable.
   * @throws PersistenciaExcepcion
   */
  public void inactivarVariable(
          Integer idRegistro
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.varpr_varperreg ")
            .append("SET varpr_estado_registro = :estadoInactivo ")
            .append("WHERE varpr_estado_registro = :estado ")
            .append("  AND varpr_ideregistro = :idRegistro; ");
    parametros.put("estadoInactivo", EEstadoGenerico.INACTIVAR);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idRegistro", idRegistro);
    ejecutarEdicion(sql, parametros);
  }
  
  /**
   * Método encargado de consultar las variables en estado certificado.
   *
   * @param idArea Identificador del área de prestación.
   * @param idPeriodo Identificador del periodo.
   * @return Lista variables certificadas
   * @throws PersistenciaExcepcion
   */
  public List<VarprVarperreg> consultarVariablesIniciales(
          Integer idArea,
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM aseo.varpr_varperreg ")
            .append("WHERE per_ideregistro = :idPeriodo ")
            .append("  AND arpr_ideregistro = :idArea ")
            .append("  AND varpr_estado = :certificado;");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarprVarperreg(rs));
  }

}
