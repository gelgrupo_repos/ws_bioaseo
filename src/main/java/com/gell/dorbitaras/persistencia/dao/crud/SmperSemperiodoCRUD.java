package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class SmperSemperiodoCRUD extends GenericoCRUD
{

  public SmperSemperiodoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(SmperSemperiodo smperSemperiodo)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO aseo.smper_semperiodo(rgta_ideregistro,per_idepadre,per_ideregistro,smper_descripcion,smper_numero,smper_swtact,usu_ideregistro_gb,usu_ideregistro_act,smper_fecgrabacion,smper_fecact) VALUES (:rgta_ideregistro,:per_idepadre,:per_ideregistro,:smper_descripcion,:smper_numero,:smper_swtact,:usu_ideregistro_gb,:usu_ideregistro_act,:smper_fecgrabacion,:smper_fecact)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object rgtaIderegistro = (smperSemperiodo.getRgtaIderegistro() == null) ? null : smperSemperiodo.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      Object perIdepadre = (smperSemperiodo.getPerIdepadre() == null) ? null : smperSemperiodo.getPerIdepadre().getPerIderegistro();
      sentencia.setObject("per_idepadre", perIdepadre);
      Object perIderegistro = (smperSemperiodo.getPerIderegistro() == null) ? null : smperSemperiodo.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      sentencia.setObject("smper_descripcion", smperSemperiodo.getSmperDescripcion());
      sentencia.setObject("smper_numero", smperSemperiodo.getSmperNumero());
      sentencia.setObject("smper_swtact", smperSemperiodo.getSmperSwtact());
      sentencia.setObject("usu_ideregistro_gb", smperSemperiodo.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", smperSemperiodo.getUsuIderegistroAct());
      sentencia.setObject("smper_fecgrabacion", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecgrabacion()));
      sentencia.setObject("smper_fecact", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecact()));

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        smperSemperiodo.setSmperIderegistro(rs.getInt("smper_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(SmperSemperiodo smperSemperiodo)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.smper_semperiodo(smper_ideregistro,rgta_ideregistro,per_idepadre,per_ideregistro,smper_descripcion,smper_numero,smper_swtact,usu_ideregistro_gb,usu_ideregistro_act,smper_fecgrabacion,smper_fecact) VALUES (:smper_ideregistro,:rgta_ideregistro,:per_idepadre,:per_ideregistro,:smper_descripcion,:smper_numero,:smper_swtact,:usu_ideregistro_gb,:usu_ideregistro_act,:smper_fecgrabacion,:smper_fecact)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("smper_ideregistro", smperSemperiodo.getSmperIderegistro());
      Object rgtaIderegistro = (smperSemperiodo.getRgtaIderegistro() == null) ? null : smperSemperiodo.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      Object perIdepadre = (smperSemperiodo.getPerIdepadre() == null) ? null : smperSemperiodo.getPerIdepadre().getPerIderegistro();
      sentencia.setObject("per_idepadre", perIdepadre);
      Object perIderegistro = (smperSemperiodo.getPerIderegistro() == null) ? null : smperSemperiodo.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      sentencia.setObject("smper_descripcion", smperSemperiodo.getSmperDescripcion());
      sentencia.setObject("smper_numero", smperSemperiodo.getSmperNumero());
      sentencia.setObject("smper_swtact", smperSemperiodo.getSmperSwtact());
      sentencia.setObject("usu_ideregistro_gb", smperSemperiodo.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", smperSemperiodo.getUsuIderegistroAct());
      sentencia.setObject("smper_fecgrabacion", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecgrabacion()));
      sentencia.setObject("smper_fecact", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecact()));

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(SmperSemperiodo smperSemperiodo)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.smper_semperiodo SET rgta_ideregistro=:rgta_ideregistro,per_idepadre=:per_idepadre,per_ideregistro=:per_ideregistro,smper_descripcion=:smper_descripcion,smper_numero=:smper_numero,smper_swtact=:smper_swtact,usu_ideregistro_gb=:usu_ideregistro_gb,usu_ideregistro_act=:usu_ideregistro_act,smper_fecgrabacion=:smper_fecgrabacion,smper_fecact=:smper_fecact where smper_ideregistro = :smper_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object rgtaIderegistro = (smperSemperiodo.getRgtaIderegistro() == null) ? null : smperSemperiodo.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      Object perIdepadre = (smperSemperiodo.getPerIdepadre() == null) ? null : smperSemperiodo.getPerIdepadre().getPerIderegistro();
      sentencia.setObject("per_idepadre", perIdepadre);
      Object perIderegistro = (smperSemperiodo.getPerIderegistro() == null) ? null : smperSemperiodo.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      sentencia.setObject("smper_descripcion", smperSemperiodo.getSmperDescripcion());
      sentencia.setObject("smper_numero", smperSemperiodo.getSmperNumero());
      sentencia.setObject("smper_swtact", smperSemperiodo.getSmperSwtact());
      sentencia.setObject("usu_ideregistro_gb", smperSemperiodo.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", smperSemperiodo.getUsuIderegistroAct());
      sentencia.setObject("smper_fecgrabacion", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecgrabacion()));
      sentencia.setObject("smper_fecact", DateUtil.parseTimestamp(smperSemperiodo.getSmperFecact()));
      sentencia.setObject("smper_ideregistro", smperSemperiodo.getSmperIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<SmperSemperiodo> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<SmperSemperiodo> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.smper_semperiodo";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getSmperSemperiodo(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public SmperSemperiodo consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    SmperSemperiodo obj = null;
    try {

      String sql = "SELECT * FROM aseo.smper_semperiodo WHERE smper_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getSmperSemperiodo(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static SmperSemperiodo getSmperSemperiodo(ResultSet rs)
          throws PersistenciaExcepcion
  {
    SmperSemperiodo smperSemperiodo = new SmperSemperiodo();
    smperSemperiodo.setSmperIderegistro(getObject("smper_ideregistro", Integer.class, rs));
    RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
    rgta_ideregistro.setRgtaIderegistro(getObject("rgta_ideregistro", Integer.class, rs));
    smperSemperiodo.setRgtaIderegistro(rgta_ideregistro);
    PerPeriodo per_idepadre = new PerPeriodo();
    per_idepadre.setPerIderegistro(getObject("per_idepadre", Integer.class, rs));
    smperSemperiodo.setPerIdepadre(per_idepadre);
    PerPeriodo per_ideregistro = new PerPeriodo();
    per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
    smperSemperiodo.setPerIderegistro(per_ideregistro);
    smperSemperiodo.setSmperDescripcion(getObject("smper_descripcion", String.class, rs));
    smperSemperiodo.setSmperNumero(getObject("smper_numero", Integer.class, rs));
    smperSemperiodo.setSmperSwtact(getObject("smper_swtact", String.class, rs));
    smperSemperiodo.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
    smperSemperiodo.setUsuIderegistroAct(getObject("usu_ideregistro_act", Integer.class, rs));
    smperSemperiodo.setSmperFecgrabacion(getObject("smper_fecgrabacion", Timestamp.class, rs));
    smperSemperiodo.setSmperFecact(getObject("smper_fecact", Timestamp.class, rs));

    return smperSemperiodo;
  }

  public static SmperSemperiodo getSmperSemperiodo(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    SmperSemperiodo smperSemperiodo = new SmperSemperiodo();
    Integer columna = columnas.get(alias + "_smper_ideregistro");
    if (columna != null) {
      smperSemperiodo.setSmperIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_rgta_ideregistro");
    if (columna != null) {
      RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
      rgta_ideregistro.setRgtaIderegistro(getObject(columna, Integer.class, rs));
      smperSemperiodo.setRgtaIderegistro(rgta_ideregistro);
    }
    columna = columnas.get(alias + "_per_idepadre");
    if (columna != null) {
      PerPeriodo per_idepadre = new PerPeriodo();
      per_idepadre.setPerIderegistro(getObject(columna, Integer.class, rs));
      smperSemperiodo.setPerIdepadre(per_idepadre);
    }
    columna = columnas.get(alias + "_per_ideregistro");
    if (columna != null) {
      PerPeriodo per_ideregistro = new PerPeriodo();
      per_ideregistro.setPerIderegistro(getObject(columna, Integer.class, rs));
      smperSemperiodo.setPerIderegistro(per_ideregistro);
    }
    columna = columnas.get(alias + "_smper_descripcion");
    if (columna != null) {
      smperSemperiodo.setSmperDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_smper_numero");
    if (columna != null) {
      smperSemperiodo.setSmperNumero(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_smper_swtact");
    if (columna != null) {
      smperSemperiodo.setSmperSwtact(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_gb");
    if (columna != null) {
      smperSemperiodo.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_act");
    if (columna != null) {
      smperSemperiodo.setUsuIderegistroAct(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_smper_fecgrabacion");
    if (columna != null) {
      smperSemperiodo.setSmperFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_smper_fecact");
    if (columna != null) {
      smperSemperiodo.setSmperFecact(getObject(columna, Timestamp.class, rs));
    }
    return smperSemperiodo;
  }
  
public static SmperSemperiodoMeses getSmperSemperiodoMeses(ResultSet rs, Map<String, Integer> columnas, String alias)
            throws PersistenciaExcepcion {
        SmperSemperiodoMeses smperSemperiodoMeses = new SmperSemperiodoMeses();
        Integer columna = columnas.get("smper_semperiodo1_per_idepadre");
        if (columna != null) {
            smperSemperiodoMeses.setPerIdEPadre(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("smper_semperiodo1_rgta_ideregistro");
        if (columna != null) {
            smperSemperiodoMeses.setRgtaIdERegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("smper_semperiodo1_smper_descripcion");
        if (columna != null) {
            smperSemperiodoMeses.setSmperDescripcion(getObject(columna, String.class, rs));
        }
        columna = columnas.get("smper_semperiodo1_smper_numero");
        if (columna != null) {
            smperSemperiodoMeses.setSmperNumero(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("per_periodo1_per_ideregistro");
        if (columna != null) {
            smperSemperiodoMeses.setPerIdeRegistro(getObject(columna, Integer.class, rs));
        }
       columna = columnas.get("per_periodo1_per_nombre");
        if (columna != null) {
            smperSemperiodoMeses.setNombrePeriodo(getObject(columna, String.class, rs));
        }
        columna = columnas.get("1_per_fecinicial");
        if (columna != null) {
            smperSemperiodoMeses.setPerFecInicial(getObject(columna, Integer.class, rs));
        }
        return smperSemperiodoMeses;
    }

}
