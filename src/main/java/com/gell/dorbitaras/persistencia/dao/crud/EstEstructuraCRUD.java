package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.constante.EMensajeEstandar;

public class EstEstructuraCRUD extends GenericoCRUD {

  public EstEstructuraCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(EstEstructura estEstructura)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.est_estructura(est_nombre,est_nivel,est_estado,est_tipordena,cla_ideregistro,usu_ideregistro,est_valida) VALUES (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, estEstructura.getEstNombre());
      sentencia.setObject(i++, estEstructura.getEstNivel());
      sentencia.setObject(i++, estEstructura.getEstEstado());
      sentencia.setObject(i++, estEstructura.getEstTipordena());
      sentencia.setObject(i++, estEstructura.getClaIderegistro());
      sentencia.setObject(i++, estEstructura.getUsuIderegistro());
      sentencia.setObject(i++, estEstructura.getEstValida());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        estEstructura.setEstIderegistro(rs.getInt("est_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(EstEstructura estEstructura)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.est_estructura(est_ideregistro,est_nombre,est_nivel,est_estado,est_tipordena,cla_ideregistro,usu_ideregistro,est_valida) VALUES (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, estEstructura.getEstIderegistro());
      sentencia.setObject(i++, estEstructura.getEstNombre());
      sentencia.setObject(i++, estEstructura.getEstNivel());
      sentencia.setObject(i++, estEstructura.getEstEstado());
      sentencia.setObject(i++, estEstructura.getEstTipordena());
      sentencia.setObject(i++, estEstructura.getClaIderegistro());
      sentencia.setObject(i++, estEstructura.getUsuIderegistro());
      sentencia.setObject(i++, estEstructura.getEstValida());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(EstEstructura estEstructura)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.est_estructura SET est_nombre=?,est_nivel=?,est_estado=?,est_tipordena=?,cla_ideregistro=?,usu_ideregistro=?,est_valida=? where est_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, estEstructura.getEstNombre());
      sentencia.setObject(i++, estEstructura.getEstNivel());
      sentencia.setObject(i++, estEstructura.getEstEstado());
      sentencia.setObject(i++, estEstructura.getEstTipordena());
      sentencia.setObject(i++, estEstructura.getClaIderegistro());
      sentencia.setObject(i++, estEstructura.getUsuIderegistro());
      sentencia.setObject(i++, estEstructura.getEstValida());
      sentencia.setObject(i++, estEstructura.getEstIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<EstEstructura> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<EstEstructura> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.est_estructura";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getEstEstructura(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public EstEstructura consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    EstEstructura obj = null;
    try {

      String sql = "SELECT * FROM public.est_estructura WHERE est_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getEstEstructura(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static EstEstructura getEstEstructura(ResultSet rs)
          throws PersistenciaExcepcion
  {
    EstEstructura estEstructura = new EstEstructura();
    estEstructura.setEstIderegistro(getObject("est_ideregistro", Integer.class, rs));
    estEstructura.setEstNombre(getObject("est_nombre", String.class, rs));
    estEstructura.setEstNivel(getObject("est_nivel", Integer.class, rs));
    estEstructura.setEstEstado(getObject("est_estado", String.class, rs));
    estEstructura.setEstTipordena(getObject("est_tipordena", String.class, rs));
    estEstructura.setClaIderegistro(getObject("cla_ideregistro", Integer.class, rs));
    estEstructura.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    estEstructura.setEstValida(getObject("est_valida", String.class, rs));

    return estEstructura;
  }

  public static void getEstEstructura(ResultSet rs, Map<String, Integer> columnas, EstEstructura estEstructura)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("est_estructura_est_ideregistro");
    if (columna != null) {
      estEstructura.setEstIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("est_estructura_est_nombre");
    if (columna != null) {
      estEstructura.setEstNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get("est_estructura_est_nivel");
    if (columna != null) {
      estEstructura.setEstNivel(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("est_estructura_est_estado");
    if (columna != null) {
      estEstructura.setEstEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get("est_estructura_est_tipordena");
    if (columna != null) {
      estEstructura.setEstTipordena(getObject(columna, String.class, rs));
    }
    columna = columnas.get("est_estructura_cla_ideregistro");
    if (columna != null) {
      estEstructura.setClaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("est_estructura_usu_ideregistro");
    if (columna != null) {
      estEstructura.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("est_estructura_est_valida");
    if (columna != null) {
      estEstructura.setEstValida(getObject(columna, String.class, rs));
    }
  }

  public static void getEstEstructura(ResultSet rs, Map<String, Integer> columnas, EstEstructura estEstructura, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_est_ideregistro");
    if (columna != null) {
      estEstructura.setEstIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_est_nombre");
    if (columna != null) {
      estEstructura.setEstNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_est_nivel");
    if (columna != null) {
      estEstructura.setEstNivel(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_est_estado");
    if (columna != null) {
      estEstructura.setEstEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_est_tipordena");
    if (columna != null) {
      estEstructura.setEstTipordena(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_cla_ideregistro");
    if (columna != null) {
      estEstructura.setClaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      estEstructura.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_est_valida");
    if (columna != null) {
      estEstructura.setEstValida(getObject(columna, String.class, rs));
    }
  }

  public static EstEstructura getEstEstructura(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    EstEstructura estEstructura = new EstEstructura();
    getEstEstructura(rs, columnas, estEstructura);
    return estEstructura;
  }

  public static EstEstructura getEstEstructura(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    EstEstructura estEstructura = new EstEstructura();
    getEstEstructura(rs, columnas, estEstructura, alias);
    return estEstructura;
  }

}
