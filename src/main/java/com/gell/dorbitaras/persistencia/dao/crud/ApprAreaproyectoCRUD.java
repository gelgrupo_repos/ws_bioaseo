package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class ApprAreaproyectoCRUD extends GenericoCRUD
{

  public ApprAreaproyectoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(ApprAreaproyecto apprAreaproyecto)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO aseo.appr_areaproyecto(arpr_ideregistro,appr_swtestado,usu_ideregistro_gb,usu_ideregistro_act,appr_fecgrabacion,appr_fecact,proyecto_ideregistro) VALUES (:arpr_ideregistro,:appr_swtestado,:usu_ideregistro_gb,:usu_ideregistro_act,:appr_fecgrabacion,:appr_fecact,:proyecto_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object arprIderegistro = (apprAreaproyecto.getArprIderegistro() == null) ? null : apprAreaproyecto.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      sentencia.setObject("appr_swtestado", apprAreaproyecto.getApprSwtestado());
      sentencia.setObject("usu_ideregistro_gb", apprAreaproyecto.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", apprAreaproyecto.getUsuIderegistroAct());
      sentencia.setObject("appr_fecgrabacion", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecgrabacion()));
      sentencia.setObject("appr_fecact", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecact()));
      Object proyectoIderegistro = (apprAreaproyecto.getProyectoIderegistro() == null) ? null : apprAreaproyecto.getProyectoIderegistro().getProyectoIderegistro();
      sentencia.setObject("proyecto_ideregistro", proyectoIderegistro);

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        apprAreaproyecto.setApprIderegistro(rs.getInt("appr_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(ApprAreaproyecto apprAreaproyecto)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.appr_areaproyecto(appr_ideregistro,arpr_ideregistro,appr_swtestado,usu_ideregistro_gb,usu_ideregistro_act,appr_fecgrabacion,appr_fecact,proyecto_ideregistro) VALUES (:appr_ideregistro,:arpr_ideregistro,:appr_swtestado,:usu_ideregistro_gb,:usu_ideregistro_act,:appr_fecgrabacion,:appr_fecact,:proyecto_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("appr_ideregistro", apprAreaproyecto.getApprIderegistro());
      Object arprIderegistro = (apprAreaproyecto.getArprIderegistro() == null) ? null : apprAreaproyecto.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      sentencia.setObject("appr_swtestado", apprAreaproyecto.getApprSwtestado());
      sentencia.setObject("usu_ideregistro_gb", apprAreaproyecto.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", apprAreaproyecto.getUsuIderegistroAct());
      sentencia.setObject("appr_fecgrabacion", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecgrabacion()));
      sentencia.setObject("appr_fecact", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecact()));
      Object proyectoIderegistro = (apprAreaproyecto.getProyectoIderegistro() == null) ? null : apprAreaproyecto.getProyectoIderegistro().getProyectoIderegistro();
      sentencia.setObject("proyecto_ideregistro", proyectoIderegistro);

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(ApprAreaproyecto apprAreaproyecto)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.appr_areaproyecto SET arpr_ideregistro=:arpr_ideregistro,appr_swtestado=:appr_swtestado,usu_ideregistro_gb=:usu_ideregistro_gb,usu_ideregistro_act=:usu_ideregistro_act,appr_fecgrabacion=:appr_fecgrabacion,appr_fecact=:appr_fecact,proyecto_ideregistro=:proyecto_ideregistro where appr_ideregistro = :appr_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object arprIderegistro = (apprAreaproyecto.getArprIderegistro() == null) ? null : apprAreaproyecto.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      sentencia.setObject("appr_swtestado", apprAreaproyecto.getApprSwtestado());
      sentencia.setObject("usu_ideregistro_gb", apprAreaproyecto.getUsuIderegistroGb());
      sentencia.setObject("usu_ideregistro_act", apprAreaproyecto.getUsuIderegistroAct());
      sentencia.setObject("appr_fecgrabacion", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecgrabacion()));
      sentencia.setObject("appr_fecact", DateUtil.parseTimestamp(apprAreaproyecto.getApprFecact()));
      Object proyectoIderegistro = (apprAreaproyecto.getProyectoIderegistro() == null) ? null : apprAreaproyecto.getProyectoIderegistro().getProyectoIderegistro();
      sentencia.setObject("proyecto_ideregistro", proyectoIderegistro);
      sentencia.setObject("appr_ideregistro", apprAreaproyecto.getApprIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<ApprAreaproyecto> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<ApprAreaproyecto> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.appr_areaproyecto";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getApprAreaproyecto(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public ApprAreaproyecto consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    ApprAreaproyecto obj = null;
    try {

      String sql = "SELECT * FROM aseo.appr_areaproyecto WHERE appr_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getApprAreaproyecto(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static ApprAreaproyecto getApprAreaproyecto(ResultSet rs)
          throws PersistenciaExcepcion
  {
    ApprAreaproyecto apprAreaproyecto = new ApprAreaproyecto();
    apprAreaproyecto.setApprIderegistro(getObject("appr_ideregistro", Integer.class, rs));
    ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
    arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    apprAreaproyecto.setArprIderegistro(arpr_ideregistro);
    apprAreaproyecto.setApprSwtestado(getObject("appr_swtestado", String.class, rs));
    apprAreaproyecto.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
    apprAreaproyecto.setUsuIderegistroAct(getObject("usu_ideregistro_act", Integer.class, rs));
    apprAreaproyecto.setApprFecgrabacion(getObject("appr_fecgrabacion", Timestamp.class, rs));
    apprAreaproyecto.setApprFecact(getObject("appr_fecact", Timestamp.class, rs));
    Proyectos proyecto_ideregistro = new Proyectos();
    proyecto_ideregistro.setProyectoIderegistro(getObject("proyecto_ideregistro", Integer.class, rs));
    apprAreaproyecto.setProyectoIderegistro(proyecto_ideregistro);

    return apprAreaproyecto;
  }

  public static ApprAreaproyecto getApprAreaproyecto(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ApprAreaproyecto apprAreaproyecto = new ApprAreaproyecto();
    Integer columna = columnas.get(alias + "_appr_ideregistro");
    if (columna != null) {
      apprAreaproyecto.setApprIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_arpr_ideregistro");
    if (columna != null) {
      ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
      arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
      apprAreaproyecto.setArprIderegistro(arpr_ideregistro);
    }
    columna = columnas.get(alias + "_appr_swtestado");
    if (columna != null) {
      apprAreaproyecto.setApprSwtestado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_gb");
    if (columna != null) {
      apprAreaproyecto.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_act");
    if (columna != null) {
      apprAreaproyecto.setUsuIderegistroAct(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_appr_fecgrabacion");
    if (columna != null) {
      apprAreaproyecto.setApprFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_appr_fecact");
    if (columna != null) {
      apprAreaproyecto.setApprFecact(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_proyecto_ideregistro");
    if (columna != null) {
      Proyectos proyecto_ideregistro = new Proyectos();
      proyecto_ideregistro.setProyectoIderegistro(getObject("proyecto_ideregistro", Integer.class, rs));
      apprAreaproyecto.setProyectoIderegistro(proyecto_ideregistro);
    }
    return apprAreaproyecto;
  }

}
