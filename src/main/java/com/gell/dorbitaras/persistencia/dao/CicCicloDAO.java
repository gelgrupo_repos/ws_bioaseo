package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.CicCicloCRUD;
import com.gell.dorbitaras.persistencia.dao.crud.PerPeriodoCRUD;
import com.gell.dorbitaras.persistencia.entidades.CicCiclo;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author Cristtian
 */
public class CicCicloDAO extends CicCicloCRUD
{

  public CicCicloDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * consultarAnios Consulta todas las área de prestación dependiendo el
   * parametro de busqueda
   *
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  @Override
  public List<CicCiclo> consultar()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT cic.* ")
            .append("    FROM cic_ciclo cic ")
            .append("        INNER JOIN ciem_cicempresa ciem ")
            .append("            ON cic.cic_ideregistro = ciem.cic_ideregistro ")
            .append("        INNER JOIN empresas emp ")
            .append("            ON emp.empresa_sevemp = ciem.emp_ideregistro ")
            .append("    WHERE cic.cic_estado = :estado ");
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<CicCiclo>()
    {
      @Override
      public CicCiclo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        CicCiclo cicCiclo = getCicCiclo(rs, columns, "cic_ciclo1");

        return cicCiclo;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

}
