package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class EttaEstttarifasCRUD extends GenericoCRUD {

  public EttaEstttarifasCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(EttaEstttarifas ettaEstttarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.etta_estttarifas(arpr_ideregistro,etta_nombre,etta_swtestado,usu_ideregistro_gb,usu_ideregistro_act,etta_fecgrabacion,etta_fecact) VALUES (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object arprIderegistro = (ettaEstttarifas.getArprIderegistro() == null) ? null : ettaEstttarifas.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, ettaEstttarifas.getEttaNombre());
      sentencia.setObject(i++, ettaEstttarifas.getEttaSwtestado());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroGb());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroAct());
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecgrabacion()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecact()));

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        ettaEstttarifas.setEttaIderegistro(rs.getInt("etta_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(EttaEstttarifas ettaEstttarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.etta_estttarifas(etta_ideregistro,arpr_ideregistro,etta_nombre,etta_swtestado,usu_ideregistro_gb,usu_ideregistro_act,etta_fecgrabacion,etta_fecact) VALUES (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, ettaEstttarifas.getEttaIderegistro());
      Object arprIderegistro = (ettaEstttarifas.getArprIderegistro() == null) ? null : ettaEstttarifas.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, ettaEstttarifas.getEttaNombre());
      sentencia.setObject(i++, ettaEstttarifas.getEttaSwtestado());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroGb());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroAct());
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecgrabacion()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecact()));

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(EttaEstttarifas ettaEstttarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.etta_estttarifas SET arpr_ideregistro=?,etta_nombre=?,etta_swtestado=?,usu_ideregistro_gb=?,usu_ideregistro_act=?,etta_fecgrabacion=?,etta_fecact=? where etta_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      Object arprIderegistro = (ettaEstttarifas.getArprIderegistro() == null) ? null : ettaEstttarifas.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, ettaEstttarifas.getEttaNombre());
      sentencia.setObject(i++, ettaEstttarifas.getEttaSwtestado());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroGb());
      sentencia.setObject(i++, ettaEstttarifas.getUsuIderegistroAct());
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecgrabacion()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(ettaEstttarifas.getEttaFecact()));
      sentencia.setObject(i++, ettaEstttarifas.getEttaIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<EttaEstttarifas> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<EttaEstttarifas> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.etta_estttarifas";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getEttaEstttarifas(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public EttaEstttarifas consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    EttaEstttarifas obj = null;
    try {

      String sql = "SELECT * FROM aseo.etta_estttarifas WHERE etta_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getEttaEstttarifas(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static EttaEstttarifas getEttaEstttarifas(ResultSet rs)
          throws PersistenciaExcepcion
  {
    EttaEstttarifas ettaEstttarifas = new EttaEstttarifas();
    ettaEstttarifas.setEttaIderegistro(getObject("etta_ideregistro", Integer.class, rs));
    ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
    arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    ettaEstttarifas.setArprIderegistro(arpr_ideregistro);
    ettaEstttarifas.setEttaNombre(getObject("etta_nombre", String.class, rs));
    ettaEstttarifas.setEttaSwtestado(getObject("etta_swtestado", String.class, rs));
    ettaEstttarifas.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
    ettaEstttarifas.setUsuIderegistroAct(getObject("usu_ideregistro_act", Integer.class, rs));
    ettaEstttarifas.setEttaFecgrabacion(getObject("etta_fecgrabacion", Timestamp.class, rs));
    ettaEstttarifas.setEttaFecact(getObject("etta_fecact", Timestamp.class, rs));

    return ettaEstttarifas;
  }

  public static void getEttaEstttarifas(ResultSet rs, Map<String, Integer> columnas, EttaEstttarifas ettaEstttarifas)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("etta_estttarifas_etta_ideregistro");
    if (columna != null) {
      ettaEstttarifas.setEttaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("etta_estttarifas_arpr_ideregistro");
    if (columna != null) {
      ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
      arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
      ettaEstttarifas.setArprIderegistro(arpr_ideregistro);
    }
    columna = columnas.get("etta_estttarifas_etta_nombre");
    if (columna != null) {
      ettaEstttarifas.setEttaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get("etta_estttarifas_etta_swtestado");
    if (columna != null) {
      ettaEstttarifas.setEttaSwtestado(getObject(columna, String.class, rs));
    }
    columna = columnas.get("etta_estttarifas_usu_ideregistro_gb");
    if (columna != null) {
      ettaEstttarifas.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("etta_estttarifas_usu_ideregistro_act");
    if (columna != null) {
      ettaEstttarifas.setUsuIderegistroAct(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("etta_estttarifas_etta_fecgrabacion");
    if (columna != null) {
      ettaEstttarifas.setEttaFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get("etta_estttarifas_etta_fecact");
    if (columna != null) {
      ettaEstttarifas.setEttaFecact(getObject(columna, Timestamp.class, rs));
    }
  }

  public static void getEttaEstttarifas(ResultSet rs, Map<String, Integer> columnas, EttaEstttarifas ettaEstttarifas, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_etta_ideregistro");
    if (columna != null) {
      ettaEstttarifas.setEttaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_etta_nombre");
    if (columna != null) {
      ettaEstttarifas.setEttaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_etta_swtestado");
    if (columna != null) {
      ettaEstttarifas.setEttaSwtestado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_gb");
    if (columna != null) {
      ettaEstttarifas.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_act");
    if (columna != null) {
      ettaEstttarifas.setUsuIderegistroAct(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_etta_fecgrabacion");
    if (columna != null) {
      ettaEstttarifas.setEttaFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_etta_fecact");
    if (columna != null) {
      ettaEstttarifas.setEttaFecact(getObject(columna, Timestamp.class, rs));
    }
  }

  public static EttaEstttarifas getEttaEstttarifas(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    EttaEstttarifas ettaEstttarifas = new EttaEstttarifas();
    getEttaEstttarifas(rs, columnas, ettaEstttarifas);
    return ettaEstttarifas;
  }

  public static EttaEstttarifas getEttaEstttarifas(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    EttaEstttarifas ettaEstttarifas = new EttaEstttarifas();
    getEttaEstttarifas(rs, columnas, ettaEstttarifas, alias);
    return ettaEstttarifas;
  }

}
