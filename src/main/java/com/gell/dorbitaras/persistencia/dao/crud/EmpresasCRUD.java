package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class EmpresasCRUD extends GenericoCRUD {


    public EmpresasCRUD(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    
    public void insertar(Empresas empresas) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO public.empresas(empresa_nom,empresa_slo,empresa_img,empresa_codsed,empresa_codsuc,empresa_indemp,empresa_idefac,empresa_sevemp,ter_idegenerico,empresa_codfssri) VALUES (?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++,empresas.getEmpresaNom()); 
sentencia.setObject(i++,empresas.getEmpresaSlo()); 
sentencia.setObject(i++,empresas.getEmpresaImg()); 
Object empresaCodsed = (empresas.getEmpresaCodsed()==null)?null:empresas.getEmpresaCodsed().getSedeCod();
sentencia.setObject(i++,empresaCodsed); 
Object empresaCodsuc = (empresas.getEmpresaCodsuc()==null)?null:empresas.getEmpresaCodsuc().getSucursalCod();
sentencia.setObject(i++,empresaCodsuc); 
sentencia.setObject(i++,empresas.getEmpresaIndemp()); 
sentencia.setObject(i++,empresas.getEmpresaIdefac()); 
sentencia.setObject(i++,empresas.getEmpresaSevemp()); 
sentencia.setObject(i++,empresas.getTerIdegenerico()); 
sentencia.setObject(i++,empresas.getEmpresaCodfssri()); 
 
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                empresas.setEmpresaCod(rs.getString("empresa_cod"));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void insertarTodos(Empresas empresas) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO public.empresas(empresa_cod,empresa_nom,empresa_slo,empresa_img,empresa_codsed,empresa_codsuc,empresa_indemp,empresa_idefac,empresa_sevemp,ter_idegenerico,empresa_codfssri) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,empresas.getEmpresaCod()); 
sentencia.setObject(i++,empresas.getEmpresaNom()); 
sentencia.setObject(i++,empresas.getEmpresaSlo()); 
sentencia.setObject(i++,empresas.getEmpresaImg()); 
Object empresaCodsed = (empresas.getEmpresaCodsed()==null)?null:empresas.getEmpresaCodsed().getSedeCod();
sentencia.setObject(i++,empresaCodsed); 
Object empresaCodsuc = (empresas.getEmpresaCodsuc()==null)?null:empresas.getEmpresaCodsuc().getSucursalCod();
sentencia.setObject(i++,empresaCodsuc); 
sentencia.setObject(i++,empresas.getEmpresaIndemp()); 
sentencia.setObject(i++,empresas.getEmpresaIdefac()); 
sentencia.setObject(i++,empresas.getEmpresaSevemp()); 
sentencia.setObject(i++,empresas.getTerIdegenerico()); 
sentencia.setObject(i++,empresas.getEmpresaCodfssri()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
          LogUtil.error(e);
          throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void editar(Empresas empresas) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "UPDATE public.empresas SET empresa_nom=?,empresa_slo=?,empresa_img=?,empresa_codsed=?,empresa_codsuc=?,empresa_indemp=?,empresa_idefac=?,empresa_sevemp=?,ter_idegenerico=?,empresa_codfssri=? where empresa_cod=? ";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,empresas.getEmpresaNom()); 
sentencia.setObject(i++,empresas.getEmpresaSlo()); 
sentencia.setObject(i++,empresas.getEmpresaImg()); 
Object empresaCodsed = (empresas.getEmpresaCodsed()==null)?null:empresas.getEmpresaCodsed().getSedeCod();
sentencia.setObject(i++,empresaCodsed); 
Object empresaCodsuc = (empresas.getEmpresaCodsuc()==null)?null:empresas.getEmpresaCodsuc().getSucursalCod();
sentencia.setObject(i++,empresaCodsuc); 
sentencia.setObject(i++,empresas.getEmpresaIndemp()); 
sentencia.setObject(i++,empresas.getEmpresaIdefac()); 
sentencia.setObject(i++,empresas.getEmpresaSevemp()); 
sentencia.setObject(i++,empresas.getTerIdegenerico()); 
sentencia.setObject(i++,empresas.getEmpresaCodfssri()); 
sentencia.setObject(i++,empresas.getEmpresaCod()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
             desconectar(sentencia);
        }
    }

    
    public List<Empresas> consultar() throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<Empresas> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM public.empresas";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getEmpresas(rs));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
             desconectar(sentencia);
        }
        return lista;

    }

    
    public Empresas consultar(long id) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        Empresas obj = null;
        try {

            String sql = "SELECT * FROM public.empresas WHERE empresa_cod=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getEmpresas(rs);
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
              desconectar(sentencia);
        }
        return obj;
    }

    public static Empresas getEmpresas(ResultSet rs) throws PersistenciaExcepcion{
      Empresas empresas = new Empresas();
empresas.setEmpresaCod( getObject("empresa_cod",String.class,rs));
empresas.setEmpresaNom( getObject("empresa_nom",String.class,rs));
empresas.setEmpresaSlo( getObject("empresa_slo",String.class,rs));
empresas.setEmpresaImg( getObject("empresa_img",String.class,rs));
Sedes empresa_codsed =new Sedes();
empresa_codsed.setSedeCod( getObject("empresa_codsed",String.class,rs));
empresas.setEmpresaCodsed(empresa_codsed);
Sucursales empresa_codsuc =new Sucursales();
empresa_codsuc.setSucursalCod( getObject("empresa_codsuc",String.class,rs));
empresas.setEmpresaCodsuc(empresa_codsuc);
empresas.setEmpresaIndemp( getObject("empresa_indemp",String.class,rs));
empresas.setEmpresaIdefac( getObject("empresa_idefac",String.class,rs));
empresas.setEmpresaSevemp( getObject("empresa_sevemp",Integer.class,rs));
empresas.setTerIdegenerico( getObject("ter_idegenerico",Long.class,rs));
empresas.setEmpresaCodfssri( getObject("empresa_codfssri",String.class,rs));

      return empresas;
    }

    public static void getEmpresas(ResultSet rs,Map<String, Integer> columnas,Empresas empresas) throws PersistenciaExcepcion{
      Integer columna = columnas.get("empresas_empresa_cod");if( columna != null ){empresas.setEmpresaCod( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_nom");if( columna != null ){empresas.setEmpresaNom( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_slo");if( columna != null ){empresas.setEmpresaSlo( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_img");if( columna != null ){empresas.setEmpresaImg( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_codsed");if( columna != null ){Sedes empresa_codsed =new Sedes();
empresa_codsed.setSedeCod( getObject(columna,String.class,rs));
empresas.setEmpresaCodsed(empresa_codsed);
} columna = columnas.get("empresas_empresa_codsuc");if( columna != null ){Sucursales empresa_codsuc =new Sucursales();
empresa_codsuc.setSucursalCod( getObject(columna,String.class,rs));
empresas.setEmpresaCodsuc(empresa_codsuc);
} columna = columnas.get("empresas_empresa_indemp");if( columna != null ){empresas.setEmpresaIndemp( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_idefac");if( columna != null ){empresas.setEmpresaIdefac( getObject(columna,String.class,rs));
} columna = columnas.get("empresas_empresa_sevemp");if( columna != null ){empresas.setEmpresaSevemp( getObject(columna,Integer.class,rs));
} columna = columnas.get("empresas_ter_idegenerico");if( columna != null ){empresas.setTerIdegenerico( getObject(columna,Long.class,rs));
} columna = columnas.get("empresas_empresa_codfssri");if( columna != null ){empresas.setEmpresaCodfssri( getObject(columna,String.class,rs));
}
    }
    
    public static void getEmpresas(ResultSet rs,Map<String, Integer> columnas,Empresas empresas,String alias) throws PersistenciaExcepcion{
      Integer columna = columnas.get(alias+"_empresa_cod");if( columna != null ){empresas.setEmpresaCod( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_nom");if( columna != null ){empresas.setEmpresaNom( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_slo");if( columna != null ){empresas.setEmpresaSlo( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_img");if( columna != null ){empresas.setEmpresaImg( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_indemp");if( columna != null ){empresas.setEmpresaIndemp( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_idefac");if( columna != null ){empresas.setEmpresaIdefac( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_empresa_sevemp");if( columna != null ){empresas.setEmpresaSevemp( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_ter_idegenerico");if( columna != null ){empresas.setTerIdegenerico( getObject(columna,Long.class,rs));
} columna = columnas.get(alias+"_empresa_codfssri");if( columna != null ){empresas.setEmpresaCodfssri( getObject(columna,String.class,rs));
}
    }

    public static Empresas getEmpresas(ResultSet rs,Map<String, Integer> columnas) throws PersistenciaExcepcion{
      Empresas empresas = new  Empresas();
      getEmpresas( rs, columnas,empresas);
      return empresas;
    }
   
 public static Empresas getEmpresas(ResultSet rs,Map<String, Integer> columnas,String alias) throws PersistenciaExcepcion{
      Empresas empresas = new  Empresas();
      getEmpresas( rs,columnas, empresas, alias);
      return empresas;
    }
    
}
