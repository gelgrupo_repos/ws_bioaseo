/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculos;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaHistoricoRecalculosCRUD extends GenericoCRUD {
    public VrtaHistoricoRecalculosCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }
    
    public void insertar(VrtaHistoricoRecalculos vrtaHistoricoRecalculos)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_historico_recalculos (numero_actualizacion,"
                    + " varperreg_ide, periodo_hijo_ide, con_dinc)"
                    + " VALUES (?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
           /* Object vrtaHistoricoRecalculoIde = (vrtaHistoricoRecalculos.getVrtaHistoricoRecalculoIde() == null) ? null : vrtaHistoricoRecalculos.getVrtaHistoricoRecalculoIde();
            sentencia.setObject(i++, vrtaHistoricoRecalculoIde);*/
            Object numeroActualizacion = (vrtaHistoricoRecalculos.getNumeroActualizacion() == null) ? null : vrtaHistoricoRecalculos.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object varperregIde = (vrtaHistoricoRecalculos.getVarperregIde() == null) ? null : vrtaHistoricoRecalculos.getVarperregIde();
            sentencia.setObject(i++, varperregIde);
            Object periodoHijoIde = (vrtaHistoricoRecalculos.getPeriodoHijoIde() == null) ? null : vrtaHistoricoRecalculos.getPeriodoHijoIde();
            sentencia.setObject(i++, periodoHijoIde);
            Object conDinc = (vrtaHistoricoRecalculos.getConDinc() == null) ? null : vrtaHistoricoRecalculos.getConDinc();
            sentencia.setObject(i++, conDinc);
            sentencia.executeUpdate();
            /*ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
              vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }
    
    public void editar(VrtaHistoricoRecalculos vrtaHistoricoRecalculos)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.vrta_historico_recalculos SET numero_actualizacion=?,varperreg_ide=?,periodo_hijo_ide=?";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                       Object numeroActualizacion = (vrtaHistoricoRecalculos.getNumeroActualizacion() == null) ? null : vrtaHistoricoRecalculos.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object varperregIde = (vrtaHistoricoRecalculos.getVarperregIde() == null) ? null : vrtaHistoricoRecalculos.getVarperregIde();
            sentencia.setObject(i++, varperregIde);
            Object periodoHijoIde = (vrtaHistoricoRecalculos.getPeriodoHijoIde() == null) ? null : vrtaHistoricoRecalculos.getPeriodoHijoIde();
            sentencia.setObject(i++, periodoHijoIde);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public List<VrtaHistoricoRecalculos> consultar()
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<VrtaHistoricoRecalculos> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.vrta_historico_recalculos";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVrtaHistoricoRecalculos(rs));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return lista;
    }

    public VrtaHistoricoRecalculos consultar(long id)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        VrtaHistoricoRecalculos obj = null;
        try {

            String sql = "SELECT * FROM aseo.vrta_historico_recalculos WHERE vrta_historico_recalculoide=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVrtaHistoricoRecalculos(rs);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return obj;
    }

    public static VrtaHistoricoRecalculos getVrtaHistoricoRecalculos(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaHistoricoRecalculos historicoRecalculos = new VrtaHistoricoRecalculos();
        historicoRecalculos.setNumeroActualizacion(getObject("numero_actualizacion", Integer.class, rs));
        historicoRecalculos.setVarperregIde(getObject("periodo_ejecucion", Integer.class, rs));
        historicoRecalculos.setPeriodoHijoIde(getObject("periodo_padre_calculo", Integer.class, rs));

        return historicoRecalculos;
    }
    
}
