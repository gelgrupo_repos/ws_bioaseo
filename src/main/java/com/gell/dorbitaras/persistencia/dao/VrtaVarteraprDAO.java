package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EClase;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.VrtaVarteraprCRUD;
import com.gell.dorbitaras.persistencia.dto.ActualizacionesToneladasPeriodoDTO;
import com.gell.dorbitaras.persistencia.dto.MesesxPeriodoPadreDTO;
import com.gell.dorbitaras.persistencia.dto.ValoresToneladasDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoBaseDTO;
import com.gell.dorbitaras.persistencia.dto.VrtaVarteraprDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarterapr;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * las variables.
 *
 * @author Desarrollador
 */
public class VrtaVarteraprDAO extends VrtaVarteraprCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public VrtaVarteraprDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta las variables según periodo, área de prestación, tercero y
   * concepto.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @param concepto Identificador de la variable.
   * @return Lista de variables.
   * @throws PersistenciaExcepcion
   */
  public List<VrtaVarterapr> consultarVariablesAprovechamiento(
          Integer periodo,
          Integer areaPrestacion,
          Integer tercero,
          Integer concepto
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT vrta.* ")
            .append("FROM aseo.vrta_varterapr vrta ")
            .append("WHERE vrta.arpr_ideregistro = :areaPrestacion ")
            .append("  AND vrta.con_ideregistro = :concepto ")
            .append("  AND vrta.per_ideregistro = :periodo ")
            .append("  AND vrta.vrta_estado_registro = :estado ")
            .append("  AND vrta.ter_ideregistro = :tercero ")
            .append("  AND vrta.emp_ideregistro = :idEmpresa");
    parametros.put("areaPrestacion", areaPrestacion);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("concepto", concepto);
    parametros.put("periodo", periodo);
    parametros.put("tercero", tercero);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (rs, columns) -> {
      return getVrtaVarterapr(rs);
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto, tercero y área de prestación.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VrtaVarterapr consultarVerificarRegistro(VrtaVarterapr datosVariable)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT vrta.* ")
            .append("FROM aseo.vrta_varterapr vrta ")
            .append("WHERE vrta.arpr_ideregistro = :areaPrestacion ")
            .append("  AND vrta.con_ideregistro = :concepto ")
            .append("  AND vrta.per_ideregistro = :periodo ")
            .append("  AND vrta.vrta_estado_registro = :estadoRegistro ")
            .append("  AND vrta.ter_ideregistro = :tercero ")
            .append("  AND vrta.emp_ideregistro = :idEmpresa ")
            .append("  AND vrta.vrta_estado = :estado ")
            .append("  AND vrta.raco_ideregistro ISNULL ")
            .append("  ORDER BY vrta.vrta_ideregistro DESC ")
            .append("  LIMIT 1");
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("estado", EEstadoGenerico.CERTIFICADO);
    parametros.put("tercero", datosVariable.getTerIderegistro().getTerIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVrtaVarterapr(rs);
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto, tercero y área de prestación.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VrtaVarterapr consultarVerificarRegistroPendiente(VrtaVarterapr datosVariable)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT vrta.* ")
            .append("FROM aseo.vrta_varterapr vrta ")
            .append("WHERE vrta.arpr_ideregistro = :areaPrestacion ")
            .append("  AND vrta.con_ideregistro = :concepto ")
            .append("  AND vrta.per_ideregistro = :periodo ")
            .append("  AND vrta.vrta_estado_registro = :estadoRegistro ")
            .append("  AND vrta.ter_ideregistro = :tercero ")
            .append("  AND vrta.emp_ideregistro = :idEmpresa ")
            .append("  AND vrta.vrta_estado = :estado ")
            .append("  ORDER BY vrta.vrta_ideregistro DESC ")
            .append("  LIMIT 1");
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("estado", EEstadoGenerico.PENDIENTE);
    parametros.put("tercero", datosVariable.getTerIderegistro().getTerIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVrtaVarterapr(rs);
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto, tercero, área de prestación e identificador del raco.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VrtaVarterapr consultarVerificarRegistroRaco(VrtaVarterapr datosVariable)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT vrta.* ")
            .append("FROM aseo.vrta_varterapr vrta ")
            .append("WHERE vrta.arpr_ideregistro = :areaPrestacion ")
            .append("  AND vrta.con_ideregistro = :concepto ")
            .append("  AND vrta.per_ideregistro = :periodo ")
            .append("  AND vrta.vrta_estado_registro = :estadoRegistro ")
            .append("  AND vrta.ter_ideregistro = :tercero ")
            .append("  AND vrta.emp_ideregistro = :idEmpresa ")
            .append("  AND vrta.vrta_estado = :estado ")
            .append("  AND vrta.raco_ideregistro = :idRaco ")
            .append("  ORDER BY vrta.vrta_ideregistro DESC ")
            .append("LIMIT 1 ");
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("tercero", datosVariable.getTerIderegistro().getTerIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("estado", EEstadoGenerico.CERTIFICADO);
    parametros.put("idRaco", datosVariable.getRacoIderegistro().getRacoIderegistr());
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVrtaVarterapr(rs);
    });
  }

  /**
   * Método encargado de inactivar los registros al momento de guardar.
   *
   * @param idRegistro Identificador de la variable.
   * @throws PersistenciaExcepcion
   */
  public void editarVariable(
          Integer idRegistro
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.vrta_varterapr ")
            .append("SET vrta_estado_registro = :estadoInactivo ")
            .append("WHERE vrta_estado_registro = :estado ")
            .append("  AND vrta_ideregistro = :idRegistro ");
    parametros.put("estadoInactivo", EEstadoGenerico.INACTIVAR);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idRegistro", idRegistro);
    ejecutarEdicion(sql, parametros);
  }
  
/**
     * Método encargado de inactivar los registros al momento de guardar por recalculo de aprovechamiento.
     *
     * @param idRegistro Identificador de la variable.
     * @throws PersistenciaExcepcion
     */
    public void InactivarVariablesTercero(
            Integer ConIderegistro,
            Long TerIderegistro,
            Integer PerIderegistro
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE aseo.vrta_varterapr ")
                .append("SET vrta_estado_registro = :estadoInactivo ")
                .append("WHERE vrta_estado_registro = :estado ")
                .append("  AND con_ideregistro = :idConcepto ")
                .append("  AND ter_ideregistro = :idTerceroRegistro ")
                .append("  AND per_ideregistro = :idPeriodo ");
        parametros.put("estadoInactivo", EEstadoGenerico.INACTIVAR);
        parametros.put("estado", EEstadoGenerico.ACTIVO);
        parametros.put("idConcepto",ConIderegistro );
        parametros.put("idTerceroRegistro", TerIderegistro);
        parametros.put("idPeriodo", PerIderegistro);
        ejecutarEdicion(sql, parametros);
    }

  /**
   * Consulta las variables en estado pendiente.
   *
   * @param periodo Identificador del periodo.
   * @param tercero Identificador del tercero.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Lista de variables pendientes.
   * @throws PersistenciaExcepcion
   */
  public List<VrtaVarteraprDTO> consultarVariablesAprovechamientoPendientes(
          Integer periodo,
          Integer areaPrestacion,
          Integer tercero
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT *, ")
            .append("       (SELECT ARRAY_TO_JSON(ARRAY_AGG(rangos.*)) ")
            .append("        FROM (SELECT raco.raco_raninicial, ")
            .append("                     raco.raco_ranfinal, ")
            .append("                     varr.vrta_ideregistro, ")
            .append("                     ant.vrta_valor  valoranterior, ")
            .append("                     varr.vrta_valor nuevovalor, ")
            .append("                     varr.vrta_estado ")
            .append("              FROM aseo.vrta_varterapr varr ")
            .append("                       INNER JOIN raco_ranconcept raco ")
            .append("                                  ON varr.raco_ideregistro = raco.raco_ideregistr ")
            .append("                       LEFT JOIN LATERAL (SELECT * ")
            .append("                                          FROM aseo.vrta_varterapr vara ")
            .append("                                          WHERE vara.raco_ideregistro = varr.raco_ideregistro ")
            .append("                                            AND vara.vrta_estado = :certificado ")
            .append("                                            AND vara.vrta_estado_registro = :activo ")
            .append("                                            AND vara.per_ideregistro = :periodo ")
            .append("                                          ORDER BY vara.vrta_ideregistro DESC ")
            .append("                                          LIMIT 1) AS ant ")
            .append("                                 ON varr.raco_ideregistro = ant.raco_ideregistro ")
            .append("              WHERE varr.con_ideregistro = variables.idConcepto ")
            .append("                AND varr.con_ideregistro IS NOT NULL ")
            .append("                AND varr.vrta_fecgrabacion = variables.vrta_fecgrabacion ")
            .append("              ORDER BY varr.vrta_fecgrabacion) rangos) lista_rangos, ")
            .append("       (SELECT to_json(valores.*) ")
            .append("        FROM (SELECT varv.vrta_ideregistro, ")
            .append("                     ant.vrta_valor  valoranterior, ")
            .append("                     varv.vrta_valor nuevovalor, ")
            .append("                     varv.vrta_estado ")
            .append("              FROM aseo.vrta_varterapr varv ")
            .append("                       LEFT JOIN LATERAL (SELECT * ")
            .append("                                          FROM aseo.vrta_varterapr vari ")
            .append("                                          WHERE vari.con_ideregistro = varv.con_ideregistro ")
            .append("                                            AND vari.vrta_estado = :certificado ")
            .append("                                            AND vari.vrta_estado_registro = :activo ")
            .append("                                          ORDER BY vari.vrta_ideregistro DESC ")
            .append("                                          LIMIT 1) as ant ON varv.con_ideregistro = ant.con_ideregistro ")
            .append("              WHERE varv.raco_ideregistro IS NULL ")
            .append("                AND varv.vrta_fecgrabacion = variables.vrta_fecgrabacion ")
            .append("                AND varv.con_ideregistro = variables.idConcepto ")
            .append("                AND varv.per_ideregistro = :periodo ")
            .append("                AND varv.arpr_ideregistro = :areaprestacion ")
            .append("                AND varv.ter_ideregistro = :tercero ")
            .append("              LIMIT 1) as valores)                     valor ")
            .append("FROM (SELECT DISTINCT varc.con_ideregistro idConcepto, ")
            .append("                      con.con_nombre       nombreconcepto, ")
            .append("                      varc.vrta_fecgrabacion, ")
            .append("                      varc.vrta_descripcion observacion ")
            .append("      FROM aseo.vrta_varterapr varc ")
            .append("               INNER JOIN con_concepto con on varc.con_ideregistro = con.uni_concepto ")
            .append("      WHERE varc.emp_ideregistro = :idempresa ")
            .append("        AND varc.vrta_estado = :pendiente ")
            .append("        AND varc.vrta_estado_registro = :activo ")
            .append("        AND varc.per_ideregistro = :periodo ")
            .append("        AND varc.arpr_ideregistro = :areaprestacion ")
            .append("        AND varc.ter_ideregistro = :tercero ")
            .append("     ) variables;");
    parametros.put("periodo", periodo);
    parametros.put("tercero", tercero);
    parametros.put("areaprestacion", areaPrestacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("pendiente", EEstadoGenerico.PENDIENTE);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> {
      VrtaVarteraprDTO variableDTO = new VrtaVarteraprDTO();
      String valorAnterior = getObject("valor", String.class, rs);
      String rangos = getObject("lista_rangos", String.class, rs);
      String nombre = getObject("nombreconcepto", String.class, rs);
      Integer idConcepto = getObject("idconcepto", Integer.class, rs);
      String observacion = getObject("observacion", String.class, rs);
      variableDTO.setIdConcepto(idConcepto);
      variableDTO.setListaRangos(rangos);
      variableDTO.setNombreConcepto(nombre);
      variableDTO.setListaValores(valorAnterior);
      variableDTO.setObservacion(observacion);
      return variableDTO;
    });
  }

  /**
   * Método encargado de consultar los conceptos para el base de aprovechamiento para el calculo.
   *
   * @param idLiquidacion  
   * @param idPeriodo  
   * @param areaPrestacion  
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<VarcVarcalculoBaseDTO> consultarVariablesBaseAprCalcuclo(
          Integer idLiquidacion , 
          Integer idPeriodo,
          Integer areaPrestacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT   ")
        .append("	variables.*,  ")
        .append("	( ")
        .append("		SELECT ARRAY_TO_JSON(ARRAY_AGG(datos_terceros.*)) as valores_meses  ")
        .append("		FROM (    ")
        .append("				SELECT    ")
        .append("					terceros.* ,  ")
        .append("					( ")
        .append("						SELECT ARRAY_TO_JSON(ARRAY_AGG(datos.*))  ")
        .append("						FROM (    ")
        .append("								SELECT    ")
        .append("									smm.smper_descripcion ,  vcc.vrta_valor   ")
        .append("								FROM aseo.smper_semperiodo smm    ")
        .append("									INNER JOIN per_periodo prr    ")
        .append("										ON prr.per_ideregistro = smm.per_ideregistro  ")
        .append("									INNER JOIN per_periodo pant   ")
        .append("										ON smm.per_idepadre = pant.per_ideregistro    ")
        .append("									INNER JOIN per_periodo pact ON pact.cic_ideregistro =  pant.cic_ideregistro   ")
        .append("										AND pact.per_ideregistro = :idPeriodo   ")
        .append("										AND pact.per_fecinicial::DATE = CAST(pant.per_fecfinal  AS DATE) + CAST('1 days' AS INTERVAL) ")
        .append("									LEFT JOIN aseo.vrta_varterapr vcc ")
        .append("										ON vcc.per_ideregistro = smm.per_ideregistro  ")
        .append("										AND con_ideregistro =  variables.uni_concepto ")
        .append("										AND vrta_estado = :certificado    ")
        .append("										AND vrta_estado_registro = :activo   ")
        .append("										AND (raco_ideregistro = variables.raco_ideregistr ")
        .append("											OR vcc.raco_ideregistro is NULL ) ")
        .append("										AND vcc.ter_ideregistro =  terceros.ter_ideregistro   ")
        .append("										AND arpr_ideregistro = :areaPrestacion  ")
        .append("								WHERE smm.smper_swtact = :activo  ")
        .append("								ORDER BY prr.per_ideorden ")
        .append("							) datos   ")
        .append("					) as valor_per_apr   ")
        .append("				FROM( ")
        .append("						SELECT    ")
        .append("							DISTINCT vcc.ter_ideregistro , ter_nomcompleto    ")
        .append("						FROM aseo.smper_semperiodo smm    ")
        .append("							INNER JOIN per_periodo pant   ")
        .append("								ON smm.per_idepadre = pant.per_ideregistro    ")
        .append("							INNER JOIN per_periodo pact ON pact.cic_ideregistro =  pant.cic_ideregistro   ")
        .append("								AND pact.per_ideregistro = :idPeriodo   ")
        .append("								AND pact.per_fecinicial::DATE = CAST(pant.per_fecfinal  AS DATE) + CAST('1 days' AS INTERVAL) ")
        .append("							INNER JOIN aseo.vrta_varterapr vcc    ")
        .append("								ON vcc.per_ideregistro = smm.per_ideregistro  ")
        .append("								AND vrta_estado = :certificado    ")
        .append("								AND vrta_estado_registro = :activo    ")
        .append("								AND arpr_ideregistro = :areaPrestacion  ")
        .append("							INNER JOIN ter_tercero trr    ")
        .append("								ON trr.ter_ideregistro =  vcc.ter_ideregistro	  ")
        .append("						WHERE smm.smper_swtact = :activo  ")
        .append("					) terceros    ")
        .append("			) datos_terceros  ")
        .append("	) 			  ")
        .append("FROM (  		  ")
        .append("		SELECT DISTINCT   ")
        .append("			con.con_nombre ,  ")
        .append("			con.uni_concepto  ,   ")
        .append("			rc.raco_ideregistr ,  ")
        .append("			rc.raco_raninicial ,  ")
        .append("			rc.raco_ranfinal  ")
        .append("		FROM public.con_concepto con  ")
        .append("			INNER JOIN public.uni_unidad unn  ")
        .append("				ON unn.uni_ideregistro = con.uni_concepto ")
        .append("				AND unn.uni_propiedad -> 'tipoVariable' = '\"B\"'   ")
        .append("			INNER JOIN public.est_estructura est  ")
        .append("				ON con.est_concepto = est.est_ideregistro ")
        .append("			INNER JOIN public.esem_estempresa ee  ")
        .append("				ON est.est_ideregistro = ee.est_ideregistro   ")
        .append("			INNER JOIN public.core_conrelacio crr ")
        .append("				ON crr.uni_conrelacion = con.uni_concepto ")
        .append("			INNER JOIN public.coli_conliquida cll ")
        .append("				ON cll.uni_concepto = crr.uni_concepto    ")
        .append("			INNER JOIN public.liq_liquidacion liq ")
        .append("				ON cll.uni_liquidacion = liq.uni_liquidacion  ")
        .append("			LEFT JOIN public.raco_ranconcept rc   ")
        .append("				ON con.uni_concepto = rc.uni_concepto ")
        .append("		WHERE liq.uni_liquidacion = :idLiquidacion  ")
        .append("			AND ee.emp_ideregistro = :idempresa  ")
        .append("			AND con.uni_concepto IN   ")
        .append("			(	  ")
        .append("				SELECT uni_ideregistro    ")
        .append("				FROM prun_prgunidad   ")
        .append("				WHERE prg_ideregistro = :prg_apr   ")
        .append("			) ")
        .append("		ORDER BY con.con_nombre , rc.raco_raninicial ")
        .append("	) variables ;  ") ;
    parametros.put("idLiquidacion", idLiquidacion);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("areaPrestacion", areaPrestacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("prg_apr", 513 );
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
        VarcVarcalculoBaseDTO varibleBase = new VarcVarcalculoBaseDTO() ;
        varibleBase.setIdConcepto(getObject("uni_concepto", Integer.class, rs));
        varibleBase.setIdRango(getObject("raco_ideregistr", Integer.class, rs));
        varibleBase.setNombreConcepto(getObject("con_nombre", String.class, rs));
        varibleBase.setRanInical(getObject("raco_raninicial", String.class, rs));
        varibleBase.setRanFinal(getObject("raco_ranfinal", String.class, rs));
        varibleBase.setValorPeriodos(getObject("valores_meses", String.class, rs));
        return varibleBase;
    });
  }


    public List<ValoresToneladasDTO> obtenerHistoricoToneladas(
            Integer idArea,
            Integer idPeriodo,
            Long idAsociacion,
            Integer idConcepto)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select  ")
                .append(" vv.vrta_ideregistro as idVrtaIdRegistro, ")
                .append(" vv.per_ideregistro as idPeriodo,  ")
                .append(" vv.vrta_valor as valorToneladas,  ")
                .append(" vv.vrta_descripcion as observacion,  ")
                .append(" vv.vrta_feccertificacion as fechaCertificacion,  ")
                .append(" vv.vrta_estado as estado,  ")
                //.append(" case when vvh.numero_actualizacion is null then 0 else vvh.numero_actualizacion+1 end as numeroActualizacion,  ")
                .append(" ROW_NUMBER() OVER (ORDER BY vv.vrta_ideregistro ASC) AS numeroActualizacion,  ")
                .append(" case when vv.comercializacion = true then 'aplica' else 'no aplica' end as comercializacion ") 
                .append(" from aseo.vrta_varterapr vv  ")
                .append(" inner join per_periodo per  ")
                .append(" on vv.per_ideregistro = per.per_ideregistro  ")
                .append(" left join aseo.vrta_varterapr_hist vvh  ")
                .append(" on vvh.vrta_ideregistro  = vv.vrta_ideregistro  ")
                .append(" where per.per_ideregistro  = :idPeriodo  ")
                .append(" and vv.ter_ideregistro = :idAsociacion  ")
                .append(" and vv.arpr_ideregistro = :idArea  ")
                .append(" and vv.con_ideregistro = :idConcepto  ")
                .append(" and vv.emp_ideregistro = :idEmpresa  ")
                .append(" order by vv.vrta_ideregistro asc ");
        parametros.put("idArea", idArea);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("idAsociacion", idAsociacion);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            ValoresToneladasDTO variableToneladasDTO = new ValoresToneladasDTO();
            variableToneladasDTO.setIdVrtaIdRegistro(getObject("idVrtaIdRegistro", Integer.class, rs));
            variableToneladasDTO.setValorToneladas(getObject("valorToneladas", Double.class, rs));
            variableToneladasDTO.setObservacion(getObject("observacion", String.class, rs));
            variableToneladasDTO.setFechaCertificacion(getObject("fechaCertificacion", String.class, rs));
            variableToneladasDTO.setEstado(getObject("estado", String.class, rs));
            variableToneladasDTO.setNumeroActualizacion(getObject("numeroActualizacion", Integer.class, rs));
            variableToneladasDTO.setComercializacion(getObject("comercializacion", String.class, rs));
            return variableToneladasDTO;
        });
    }

    public Integer obtenerCuentaActualizaciones(
            Integer idRegistro)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select ROW_NUMBER () OVER (ORDER BY numero_actualizacion  desc) as conteo ")
                .append(" from aseo.vrta_varterapr_hist where vrta_ideregistro = :id_registro ")
                .append(" order by conteo desc ")
                .append(" limit 1");
        parametros.put("id_registro", idRegistro);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("conteo", Integer.class, rs);
        }).get(0);
    }
    
    public List<ActualizacionesToneladasPeriodoDTO> obtenerActualizacionesToneladasXPeriodoPadre(
            Integer idConcepto,
            Integer idPeriodo,
            Integer idArea
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("select vvh.numero_actualizacion, ")
                .append("        vv.vrta_ideregistro, ")
                .append("        vv.vrta_valor, ")
                .append("        vv.ter_ideregistro, ")
                .append("        per_ideregistro ")
                .append(" from aseo.vrta_varterapr vv ")
                .append(" inner join aseo.vrta_varterapr_hist vvh ")
                .append(" 	on vv.vrta_ideregistro = vvh.vrta_ideregistro  ")
                .append(" where vv.vrta_ideregistro in ( ")
                .append(" 	select max(vrta_ideregistro) ")
                .append(" 	from aseo.vrta_varterapr vv ")
                .append(" 	where per_ideregistro = ( ")
                .append(" 	select pp1.per_ideregistro  ")
                .append("	from 	per_periodo pp1 ")
                .append("	where 	pp1.per_nombre like 'Semestre%' ")
                .append("	and 	pp1.per_fecfinal < (select 	pp.per_fecinicial  ")
                .append("	from 	per_periodo pp  ")
                .append("	where 	per_ideregistro = :idPeriodo ")
                .append("	) ")
                .append("	order by pp1.per_fecfinal desc  ")
                .append("	limit 1 ")
                .append(" 	) ")
                .append(" 	and vv.arpr_ideregistro = :idArea ")
                .append(" 	and vv.con_ideregistro = :idConcepto ")
                .append(" 	and vv.emp_ideregistro = :idEmpresa ")
                .append(" 	group by per_ideregistro, con_ideregistro ");
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("vrta_valor", Double.class, rs),
                    getObject("vrta_ideregistro", Integer.class, rs),
                    getObject("per_ideregistro", Integer.class, rs),
                    null,
                    getObject("ter_ideregistro", Long.class, rs)
            );
        });
    }
    
    public List<ActualizacionesToneladasPeriodoDTO> obtenerTercerosVarterapr(
            Integer idPeriodo, Integer idArea)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select distinct ter_ideregistro  from aseo.vrta_varterapr ")
                .append(" where arpr_ideregistro = :idArea order by ter_ideregistro;  ");
        parametros.put("idArea", idArea);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    null,
                    null,
                    null,
                    null,
                    null,
                    getObject("ter_ideregistro", Long.class, rs)
            );
        });
    }
    
    /**
     * Consulta los tercero registrados a la fecha
     *
     * @return Lista tipo ActualizacionToneladasPeriodoDTO
     * @throws PersistenciaExcepcion
     */
    public List<ActualizacionesToneladasPeriodoDTO> obtenerTercerosTotal()
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
            sql.append("SELECT ter.* ")
            .append("FROM ter_tercero ter ")
            .append("         INNER JOIN clte_clatercero clt ON clt.ter_ideregistro = ter.ter_ideregistro ")
            .append("         INNER JOIN uni_unidad uu on clt.uni_clatercero = uu.uni_ideregistro ")
            .append("         INNER JOIN est_estructura ee on uu.est_ideregistro = ee.est_ideregistro ")
            .append("         INNER JOIN esem_estempresa e on ee.est_ideregistro = e.est_ideregistro ")
            .append("WHERE e.emp_ideregistro = :idEmpresa ")
            .append("  AND uu.uni_propiedad ->> 'uniestado' = :estado ")
            .append("  AND clt.uni_clatercero = :idClase ");
    parametros.put("idClase", EClase.APROVECHADOR);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    null,
                    null,
                    null,
                    null,
                    null,
                    getObject("ter_ideregistro", Long.class, rs)
            );
        });
    }
    
    /**
     * Consulta los tercero registrados en el ultimo calculo mensual, filtra los de la fecha
     *
     * @return Lista tipo ActualizacionToneladasPeriodoDTO
     * @throws PersistenciaExcepcion
     */
    public List<ActualizacionesToneladasPeriodoDTO> obtenerTercerosUltimoCalculoNormal(
    Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
            sql.append("select tt.* from ASEO.vrta_varterapr_prom vvp ")
                .append("inner join ter_tercero tt on tt.ter_ideregistro = vvp.ter_ideregistro ")
                .append("where vvp.per_idepadre = :idPeriodo ")
                .append(" and vvp.tipo = 'C' ")
                .append(" and vvp.numero_actualizacion = ")
                .append(" (select max(vp.numero_actualizacion) ")
                .append(" from aseo.vrta_prom_finales vp ")
                .append(" where per_ideregistro = :idPeriodo and vp.tipo='C') ") 
//                .append(" UNION select tt2.* ")
//                .append(" from aseo.vrta_varterapr o ")
//                .append(" inner join ter_tercero tt2 on tt2.ter_ideregistro = o.ter_ideregistro ")
//                .append(" where per_ideregistro = :idPeriodo ")  
//                .append(" and vrta_estado_registro = :estado ")  
//                .append(" and vrta_estado = :estadoCertificado ")  
//                .append(" and con_ideregistro=:idConcepto ")
//                .append(" and o.ter_ideregistro not in (select distinct vvp2.ter_ideregistro from aseo.vrta_varterapr_prom vvp2 where vvp2.vrta_prom_valor>0); ");
                .append(" UNION select distinct tt2.* ") 
                .append(" from aseo.vrta_varterapr o  ")
                .append(" inner join ter_tercero tt2 on tt2.ter_ideregistro = o.ter_ideregistro  ")
                .append(" inner join per_periodo pp on pp.per_ideregistro = o.per_ideregistro ")
                .append(" inner join ( ")
                .append(" select ter_ideregistro,min(pp.per_fecinicial) as fecha ")
                .append(" from aseo.vrta_varterapr_prom vvp2 ") 
                .append(" inner join per_periodo pp on pp.per_ideregistro = vvp2.per_idepadre  ")
                .append(" where vvp2.vrta_prom_valor>0 ")
                .append(" and vvp2.tipo = 'C' ")
                .append(" group by ter_ideregistro ")
                .append(" ) sub on sub.ter_ideregistro = tt2.ter_ideregistro and pp.per_fecinicial::date < sub.fecha::date ")
                .append(" where pp.per_ideregistro = :idPeriodo  ")  
                .append(" and vrta_estado_registro = :estado ")   
                .append(" and vrta_estado = :estadoCertificado ")  
                .append(" and con_ideregistro=:idConcepto;"); 
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("estadoCertificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("idConcepto",2902);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    null,
                    null,
                    null,
                    null,
                    null,
                    getObject("ter_ideregistro", Long.class, rs)
            );
        });
    }
    
    public List<ActualizacionesToneladasPeriodoDTO> obtenerActualizacionesToneladasXPeriodoPadreAnterior(
            Integer idConcepto,
            Integer idPeriodo,
            Integer idArea,
            Long terTercero
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select vvh.numero_actualizacion,  ")
                .append("        vv.vrta_ideregistro, ")
                .append("        vv.vrta_valor, ")
                .append("        vv.ter_ideregistro, ")
                .append("        per_ideregistro ")
                .append(" from aseo.vrta_varterapr vv ")
                .append(" left join aseo.vrta_varterapr_hist vvh  ")
                .append(" 	on vv.vrta_ideregistro = vvh.vrta_ideregistro  ")
                .append(" where vv.vrta_ideregistro in ( ")
                .append(" 	select max(vrta_ideregistro) ")
                .append(" 	from aseo.vrta_varterapr vv ")
                .append(" 	where per_ideregistro = :idPeriodo ")
                .append(" 	and vv.arpr_ideregistro = :idArea ")
                .append(" 	and vv.con_ideregistro = :idConcepto ")
                .append(" 	and vv.emp_ideregistro = :idEmpresa ")
                .append(" 	group by per_ideregistro, con_ideregistro, ter_ideregistro")
                .append(" ) and vv.ter_ideregistro = :terTercero and vv.con_ideregistro=:idConceptoAprovechamiento ; ");
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("terTercero", terTercero);
        parametros.put("idConceptoAprovechamiento",2902);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("vrta_valor", Double.class, rs),
                    getObject("vrta_ideregistro", Integer.class, rs),
                    getObject("per_ideregistro", Integer.class, rs),
                    null,
                    getObject("ter_ideregistro", Long.class, rs)
            );
        });
    }
    
        public List<ActualizacionesToneladasPeriodoDTO> obtenerActualizacionesToneladasMesAnterior(
            Long idAsociacion,
            Integer idConcepto,
            Integer idPeriodo,
            Integer idArea
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select	  ")
                .append(" 		vv.vrta_ideregistro,  ")
                .append(" 		vv.vrta_valor,  ")
                .append(" 		vv.vrta_descripcion,  ")
                .append(" 		vv.per_ideregistro,  ")
                .append(" 		vv.vrta_estado  ")
                .append(" 		  ")
                .append(" from 	aseo.vrta_varterapr vv   ")
                .append(" where 	vv.per_ideregistro in (  ")
                .append(" 		select per_ideregistro   ")
                .append(" 		from 	aseo.smper_semperiodo ss   ")
                .append(" 		where per_idepadre = (  ")
                .append(" 			  ")
                .append(" 			select pp1.per_ideregistro   ")
                .append(" 			from 	per_periodo pp1  ")
                .append(" 			where 	pp1.per_nombre like 'Semestre%'  ")
                .append(" 			and 	pp1.per_fecfinal < (select 	pp.per_fecinicial   ")
                .append(" 			from 	per_periodo pp   ")
                .append(" 			where 	per_ideregistro = (select 	per_idepadre  ")
                .append(" 			from 	aseo.smper_semperiodo ss   ")
                .append(" 			where 	ss.per_ideregistro = :idPeriodo)  ")
                .append(" 			)  ")
                .append(" 			order by pp1.per_fecfinal desc   ")
                .append(" 			limit 1  ")
                .append(" 		)  ")
                .append(" )  ");
        parametros.put("idAsociacion", idAsociacion);
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ActualizacionesToneladasPeriodoDTO(
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("vrta_valor", Double.class, rs),
                    getObject("vrta_ideregistro", Integer.class, rs),
                    getObject("per_ideregistro", Integer.class, rs),
                    getObject("smper_numero", Integer.class, rs),
                    null
            );
        });
    }
    
    public List<MesesxPeriodoPadreDTO> obtenerListadoMesesPeriodoAnterior(
            Integer idPeriodo, Integer idArea, Long terTercero)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select distinct ss.per_ideregistro, ")
                .append("                             		ss.per_idepadre, ")
                .append("                             		ss.smper_numero ")
                .append("                  from 	aseo.smper_semperiodo ss ")
                .append("                  inner join aseo.vrta_varterapr vv  ")
                .append("                      on ss.per_ideregistro = vv.per_ideregistro  ")
                .append("                  where vv.ter_ideregistro = :terTercero and vv.con_ideregistro = :idConceptoAprovechamiento ")
                .append("                  and	ss.per_idepadre  IN ( ")
                .append("                  select per_idepadre from (select per_idepadre ")
                .append("                  from aseo.smper_semperiodo ss  ")
                .append("                  inner join ")
                .append(" 							aseo.arpr_areaprestacion aa  ")
                .append(" 				 on		aa.rgta_ideregistro  = ss.rgta_ideregistro  ")
                .append(" 				 inner join  ")
                .append(" 				 			per_periodo pp2  ")
                .append(" 				 on ")
                .append(" 				 			pp2.per_ideregistro = ss.per_ideregistro  ")
                .append("                  where ss.per_idepadre  in   ")
                .append("                  ( ")
                .append("                  select pp1.per_ideregistro  ")
                .append("                  from 	per_periodo pp1 ")
                .append("                  where 	pp1.per_nombre like 'Semestre%'  ")
                .append("                  and 	pp1.cic_ideregistro = 5 ")
                .append("                  and 	pp1.per_fecfinal  < (select 	pp.per_fecinicial  ")
                .append("                   from 	per_periodo pp  ")
                .append("                  where 	per_ideregistro = :idPeriodo ")
                .append("                  ) ")
                .append("                  order by pp1.per_fecinicial  desc ")
                .append("                             									)  ")
                .append("                  and 	aa.arpr_ideregistro = :idArea ")
                .append("                  order by pp2.per_fecinicial  desc ")
                .append("                   ")
                .append("                  ) query_1 ")
                .append("                  limit 1 ")
                .append("                 ) order by smper_numero asc; ");
        parametros.put("terTercero", terTercero);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("idArea", idArea);
        parametros.put("idConceptoAprovechamiento",2902);//temporal
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new MesesxPeriodoPadreDTO(
                    getObject("per_ideregistro", Integer.class, rs),
                    getObject("per_idepadre", Integer.class, rs),
                    getObject("smper_numero", Integer.class, rs)
            );
        });
    }

    public List<MesesxPeriodoPadreDTO> obtenerListadoMesesPeriodoActual(
            Integer idPeriodo, Long terTercero)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select 	ss.per_ideregistro , ")
                .append(" 		ss.per_idepadre, ")
                .append(" 		ss.smper_numero ")
                .append(" from 	aseo.smper_semperiodo ss  ")
                .append(" inner join aseo.vrta_varterapr vv  ")
                .append(" on ss.per_ideregistro  = vv.per_ideregistro  ")
                .append(" inner join per_periodo pp  ")
 	        .append(" on ss.per_ideregistro = pp.per_ideregistro  ")
                .append(" where vv.ter_ideregistro = :terTercero and vv.con_ideregistro = :idConceptoAprovechamiento ")
                .append(" and	ss.per_idepadre  = (select per_idepadre from aseo.smper_semperiodo ss where per_ideregistro = :idPeriodo and rgta_ideregistro = 1 limit 1) ")
                .append(" and pp.per_fecfinal <= (select per_fecfinal  from per_periodo pp where per_ideregistro = :idPeriodo limit 1) ")
                .append(" and (select count(vv.*) ")
                .append(" from 	aseo.smper_semperiodo ss ")  
                .append(" inner join aseo.vrta_varterapr vv ")  
                .append(" on ss.per_ideregistro  = vv.per_ideregistro ")  
                .append(" inner join per_periodo pp ")  
                .append(" on ss.per_ideregistro = pp.per_ideregistro ")  
                .append(" where vv.ter_ideregistro = :terTercero and vv.con_ideregistro = :idConceptoAprovechamiento and vv.vrta_estado_registro = 'A' ")
                .append(" and	ss.per_idepadre <> (select per_idepadre from aseo.smper_semperiodo ss where per_ideregistro = :idPeriodo and rgta_ideregistro = 1 limit 1))=0 ")
                .append(" union ") 
                .append(" select ss.per_ideregistro , ") 
                .append(" ss.per_idepadre, ") 
                .append(" ss.smper_numero ") 
                .append(" from 	aseo.smper_semperiodo ss ")  
                .append(" inner join aseo.vrta_varterapr vv on ss.per_ideregistro  = vv.per_ideregistro ")  
                .append(" inner join per_periodo pp on ss.per_ideregistro = pp.per_ideregistro ")  
                .append(" inner join ( ")
                .append(" select ter_ideregistro,min(pp.per_fecinicial) as fecha ")
                .append(" from aseo.vrta_varterapr_prom vvp2 ") 
                .append(" inner join per_periodo pp on pp.per_ideregistro = vvp2.per_idepadre  ")
                .append(" where vvp2.vrta_prom_valor>0 ")
                .append(" and vvp2.tipo = 'C' ")
                .append(" group by ter_ideregistro ")
                .append(" ) sub on sub.ter_ideregistro = vv.ter_ideregistro and pp.per_fecinicial::date < sub.fecha::date ")
                .append(" where vv.ter_ideregistro = :terTercero and vv.con_ideregistro = :idConceptoAprovechamiento ") 
                .append(" and	ss.per_idepadre  = (select per_idepadre from aseo.smper_semperiodo ss where per_ideregistro = :idPeriodo and rgta_ideregistro = 1 limit 1) ") 
                .append(" and pp.per_fecfinal <= (select per_fecfinal  from per_periodo pp where per_ideregistro = :idPeriodo limit 1) ");
                //.append(" and vv.ter_ideregistro not in (select distinct vvp2.ter_ideregistro from aseo.vrta_varterapr_prom vvp2 where vvp2.tipo = 'C') ");
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("terTercero", terTercero);
        parametros.put("idConceptoAprovechamiento", 2902);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new MesesxPeriodoPadreDTO(
                    getObject("per_ideregistro", Integer.class, rs),
                    getObject("per_idepadre", Integer.class, rs),
                    getObject("smper_numero", Integer.class, rs)
            );
        });
    }
    
  /**
   * Registra una variable en la base de datos.
   *
   * @param vrtaVariable Datos de la variable a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public void insertar(VrtaVarterapr vrtaVariable)
          throws PersistenciaExcepcion
  {
    if (vrtaVariable.getVrtaIderegistro() != null) {
      editar(vrtaVariable);
      return;
    }
    super.insertar(vrtaVariable);
  }
  
    /**
     * Registra una variable en la base de datos y devuelve el id.
     *
     * @param vrtaVariable Datos de la variable a insertar.
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public Integer insertarRetornaId(VrtaVarterapr vrtaVariable)
            throws PersistenciaExcepcion {
        if (vrtaVariable.getVrtaIderegistro() != null) {
            editar(vrtaVariable);
            return vrtaVariable.getVrtaIderegistro();
        }
        return super.insertarRetornaId(vrtaVariable);
    }

    /**
       * Método encargado de eliminar la tonelada
       *
       * @param idVrtaIderegistro Identificador del registro.
       * @throws PersistenciaExcepcion
       */
      public void eliminarTonelada(Integer idVrtaIderegistro)
              throws PersistenciaExcepcion
      {
        StringBuilder sql = new StringBuilder();
        sql.append("DELETE ")
                .append(" from aseo.vrta_varterapr ")
                .append("where vrta_ideregistro = :idVrtaIderegistro ");
        parametros.put("idVrtaIderegistro", idVrtaIderegistro);
        ejecutarEdicion(sql, parametros);
      }
}
