package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import static com.gell.dorbitaras.persistencia.dao.crud.RutRutaCRUD.getRutRuta;
import com.gell.dorbitaras.persistencia.entidades.CicCiclo;
import com.gell.dorbitaras.persistencia.entidades.DimpDetalleimportar;
import com.gell.dorbitaras.persistencia.entidades.ImpImportar;
import com.gell.dorbitaras.persistencia.entidades.RutRuta;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

public class ImpImportarDAO extends ImpImportarCRUD {

  public ImpImportarDAO(DataSource datasource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
     super(datasource, auditoria);
  }
  
    /**
   * Expone el método de guardarImp y si se envía el identificador de ImpImportar se
   * edita
   *
   * @param importar Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarImp(ImpImportar importar)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Long Ideregistro = (importar.getImpIderegistro());
    Ideregistro = Ideregistro == null ? -1 : Ideregistro;
    ImpImportar importarGuardada = consultar(Ideregistro);
    if (importarGuardada != null) {
      this.editar(importar);
      return;
    }
    super.insertar(importar);
  }
  
    /**
   * Consulta imp_importar
   *
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<ImpImportar> consultar()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM aseo.imp_importar imp, ")
          .append("             to_json((   ")
          .append("              SELECT ARRAY_AGG(destino.*) ")
          .append("                 FROM (SELECT *   ")
          .append("                     FROM aseo.dimp_detalleimportar dimp ")
          .append("                 WHERE imp.imp_ideregistro = dimp.imp_ideregistro ) AS destino ")
          .append("              )) infodestino  ");

    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<ImpImportar>() {
      @Override
      public ImpImportar siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        ImpImportar impImportar = getImpImportar(rs, columns, "imp_importar1");
        String infodestino = getObject("infodestino", String.class, rs);
        impImportar.setInfo(infodestino);
        return impImportar;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }
}
