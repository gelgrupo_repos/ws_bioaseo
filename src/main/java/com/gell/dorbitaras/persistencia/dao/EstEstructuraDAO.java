package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.dorbitaras.persistencia.entidades.EstEstructura;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class EstEstructuraDAO extends EstEstructuraCRUD {

  public EstEstructuraDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta las estructuras disponibles para poder configurar las unidades
   *
   * @return Lista con todas las Estructuras permitidas para el registro de
   * unidades
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion Si
   * ocurre un error al momento de ejecutar
   *
   */
  public List<EstEstructura> consultarEstructurasConfiguracion()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT  est.* ")
            .append("FROM est_estructura est ")
            .append("  INNER JOIN cla_clase cla ON est.cla_ideregistro = cla.cla_ideregistro ")
            .append("  INNER JOIN esem_estempresa esem on est.est_ideregistro = esem.est_ideregistro ")
            .append("WHERE cla.cla_tipo = :taras ")
            .append("      AND est.est_estado = :activo ")
            .append("      AND esem.emp_ideregistro = :idempresa ")
            .append("      ORDER BY est.est_nombre ASC");
    parametros.put("taras", EEstadoGenerico.TARAS);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getEstEstructura(rs, columns, "est_estructura1"));
  }

  /**
   * Consulta una estructura dependiendo de la clase y la empresa
   *
   * @param idClase identificador de la clase (cla_clase)
   * @param idEmpresa Identificador de la empresa que se quiere consultar
   * @return Información de la estructura
   * @throws PersistenciaExcepcion Error al consultar la estructura
   */
  public EstEstructura consultar(int idClase, int idEmpresa)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT est.* ")
            .append("FROM est_estructura est ")
            .append("  INNER JOIN esem_estempresa esem on est.est_ideregistro = esem.est_ideregistro ")
            .append("WHERE est.cla_ideregistro = :idclase AND esem.emp_ideregistro = :idempresa");
    parametros.put("idclase", idClase);
    parametros.put("idempresa", idEmpresa);
    return ejecutarConsultaSimple(sql, parametros, new ConsultaAdaptador<EstEstructura>() {
      @Override
      public EstEstructura siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getEstEstructura(rs);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_NO_ENCONTRADA_ESTRUCTURA);
      }
    });

  }
}
