package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class RuemRutempresaCRUD extends GenericoCRUD
{

  public RuemRutempresaCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(RuemRutempresa ruemRutempresa)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO public.ruem_rutempresa(rut_ideregistro,emp_ideregistro,usu_ideregistro) VALUES (:rut_ideregistro,:emp_ideregistro,:usu_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object rutIderegistro = (ruemRutempresa.getRutIderegistro() == null) ? null : ruemRutempresa.getRutIderegistro().getRutIderegistro();
      sentencia.setObject("rut_ideregistro", rutIderegistro);
      Object empIderegistro = (ruemRutempresa.getEmpIderegistro() == null) ? null : ruemRutempresa.getEmpIderegistro().getEmpresaSevemp();
      sentencia.setObject("emp_ideregistro", empIderegistro);
      sentencia.setObject("usu_ideregistro", ruemRutempresa.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        ruemRutempresa.setRuemIderegistr(rs.getInt("ruem_ideregistr"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(RuemRutempresa ruemRutempresa)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.ruem_rutempresa(ruem_ideregistr,rut_ideregistro,emp_ideregistro,usu_ideregistro) VALUES (:ruem_ideregistr,:rut_ideregistro,:emp_ideregistro,:usu_ideregistro)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("ruem_ideregistr", ruemRutempresa.getRuemIderegistr());
      Object rutIderegistro = (ruemRutempresa.getRutIderegistro() == null) ? null : ruemRutempresa.getRutIderegistro().getRutIderegistro();
      sentencia.setObject("rut_ideregistro", rutIderegistro);
      Object empIderegistro = (ruemRutempresa.getEmpIderegistro() == null) ? null : ruemRutempresa.getEmpIderegistro().getEmpresaSevemp();
      sentencia.setObject("emp_ideregistro", empIderegistro);
      sentencia.setObject("usu_ideregistro", ruemRutempresa.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(RuemRutempresa ruemRutempresa)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.ruem_rutempresa SET rut_ideregistro=:rut_ideregistro,emp_ideregistro=:emp_ideregistro,usu_ideregistro=:usu_ideregistro where ruem_ideregistr = :ruem_ideregistr ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object rutIderegistro = (ruemRutempresa.getRutIderegistro() == null) ? null : ruemRutempresa.getRutIderegistro().getRutIderegistro();
      sentencia.setObject("rut_ideregistro", rutIderegistro);
      Object empIderegistro = (ruemRutempresa.getEmpIderegistro() == null) ? null : ruemRutempresa.getEmpIderegistro().getEmpresaSevemp();
      sentencia.setObject("emp_ideregistro", empIderegistro);
      sentencia.setObject("usu_ideregistro", ruemRutempresa.getUsuIderegistro());
      sentencia.setObject("ruem_ideregistr", ruemRutempresa.getRuemIderegistr());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<RuemRutempresa> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<RuemRutempresa> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.ruem_rutempresa";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getRuemRutempresa(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public RuemRutempresa consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    RuemRutempresa obj = null;
    try {

      String sql = "SELECT * FROM public.ruem_rutempresa WHERE ruem_ideregistr= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getRuemRutempresa(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static RuemRutempresa getRuemRutempresa(ResultSet rs)
          throws PersistenciaExcepcion
  {
    RuemRutempresa ruemRutempresa = new RuemRutempresa();
    ruemRutempresa.setRuemIderegistr(getObject("ruem_ideregistr", Integer.class, rs));
    RutRuta rut_ideregistro = new RutRuta();
    rut_ideregistro.setRutIderegistro(getObject("rut_ideregistro", Integer.class, rs));
    ruemRutempresa.setRutIderegistro(rut_ideregistro);
    Empresas emp_ideregistro = new Empresas();
    emp_ideregistro.setEmpresaSevemp(getObject("emp_ideregistro", Integer.class, rs));
    ruemRutempresa.setEmpIderegistro(emp_ideregistro);
    ruemRutempresa.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return ruemRutempresa;
  }

  public static RuemRutempresa getRuemRutempresa(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    RuemRutempresa ruemRutempresa = new RuemRutempresa();
    Integer columna = columnas.get(alias + "_ruem_ideregistr");
    if (columna != null) {
      ruemRutempresa.setRuemIderegistr(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_rut_ideregistro");
    if (columna != null) {
      RutRuta rut_ideregistro = new RutRuta();
      rut_ideregistro.setRutIderegistro(getObject("rut_ideregistro", Integer.class, rs));
      ruemRutempresa.setRutIderegistro(rut_ideregistro);
    }
    columna = columnas.get(alias + "_emp_ideregistro");
    if (columna != null) {
      Empresas emp_ideregistro = new Empresas();
      emp_ideregistro.setEmpresaSevemp(getObject(columna, Integer.class, rs));
      ruemRutempresa.setEmpIderegistro(emp_ideregistro);
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      ruemRutempresa.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    return ruemRutempresa;
  }

}
