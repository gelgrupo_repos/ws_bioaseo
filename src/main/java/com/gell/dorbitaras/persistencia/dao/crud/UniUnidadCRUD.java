package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.util.LogUtil;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.constante.EMensajeEstandar;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class UniUnidadCRUD extends GenericoCRUD {

  private final int ID = 1;

  public UniUnidadCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO uni_unidad(est_ideregistro,uni_codigo1,uni_codigo2,uni_codigo3,uni_codigo4,uni_codigo5,uni_nombre1,uni_nombre2,uni_nombre3,uni_nombre4,uni_nombre5,uni_orden,uni_nivel,uni_codigo,uni_idepadre,usu_ideregistro,uni_propiedad) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?::jsonb)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, (uniUnidad.getEstIderegistro() == null) ? null : uniUnidad.getEstIderegistro().getEstIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniCodigo1());
      sentencia.setObject(i++, uniUnidad.getUniCodigo2());
      sentencia.setObject(i++, uniUnidad.getUniCodigo3());
      sentencia.setObject(i++, uniUnidad.getUniCodigo4());
      sentencia.setObject(i++, uniUnidad.getUniCodigo5());
      sentencia.setObject(i++, uniUnidad.getUniNombre1());
      sentencia.setObject(i++, uniUnidad.getUniNombre2());
      sentencia.setObject(i++, uniUnidad.getUniNombre3());
      sentencia.setObject(i++, uniUnidad.getUniNombre4());
      sentencia.setObject(i++, uniUnidad.getUniNombre5());
      sentencia.setObject(i++, uniUnidad.getUniOrden());
      sentencia.setObject(i++, uniUnidad.getUniNivel());
      sentencia.setObject(i++, uniUnidad.getUniCodigo());
      sentencia.setObject(i++, (uniUnidad.getUniIdepadre() == null) ? null : uniUnidad.getUniIdepadre().getUniIderegistro());
      sentencia.setObject(i++, uniUnidad.getUsuIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniPropiedad());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        uniUnidad.setUniIderegistro(rs.getInt(ID));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO uni_unidad(uni_ideregistro,est_ideregistro,uni_codigo1,uni_codigo2,uni_codigo3,uni_codigo4,uni_codigo5,uni_nombre1,uni_nombre2,uni_nombre3,uni_nombre4,uni_nombre5,uni_orden,uni_nivel,uni_codigo,uni_idepadre,usu_ideregistro,uni_propiedad) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?::jsonb)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, uniUnidad.getUniIderegistro());
      sentencia.setObject(i++, (uniUnidad.getEstIderegistro() == null) ? null : uniUnidad.getEstIderegistro().getEstIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniCodigo1());
      sentencia.setObject(i++, uniUnidad.getUniCodigo2());
      sentencia.setObject(i++, uniUnidad.getUniCodigo3());
      sentencia.setObject(i++, uniUnidad.getUniCodigo4());
      sentencia.setObject(i++, uniUnidad.getUniCodigo5());
      sentencia.setObject(i++, uniUnidad.getUniNombre1());
      sentencia.setObject(i++, uniUnidad.getUniNombre2());
      sentencia.setObject(i++, uniUnidad.getUniNombre3());
      sentencia.setObject(i++, uniUnidad.getUniNombre4());
      sentencia.setObject(i++, uniUnidad.getUniNombre5());
      sentencia.setObject(i++, uniUnidad.getUniOrden());
      sentencia.setObject(i++, uniUnidad.getUniNivel());
      sentencia.setObject(i++, uniUnidad.getUniCodigo());
      sentencia.setObject(i++, (uniUnidad.getUniIdepadre() == null) ? null : uniUnidad.getUniIdepadre().getUniIderegistro());
      sentencia.setObject(i++, uniUnidad.getUsuIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniPropiedad());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE uni_unidad SET est_ideregistro=?,uni_codigo1=?,uni_codigo2=?,uni_codigo3=?,uni_codigo4=?,uni_codigo5=?,uni_nombre1=?,uni_nombre2=?,uni_nombre3=?,uni_nombre4=?,uni_nombre5=?,uni_orden=?,uni_nivel=?,uni_codigo=?,uni_idepadre=?,usu_ideregistro=?,uni_propiedad=?::jsonb where uni_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, (uniUnidad.getEstIderegistro() == null) ? null : uniUnidad.getEstIderegistro().getEstIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniCodigo1());
      sentencia.setObject(i++, uniUnidad.getUniCodigo2());
      sentencia.setObject(i++, uniUnidad.getUniCodigo3());
      sentencia.setObject(i++, uniUnidad.getUniCodigo4());
      sentencia.setObject(i++, uniUnidad.getUniCodigo5());
      sentencia.setObject(i++, uniUnidad.getUniNombre1());
      sentencia.setObject(i++, uniUnidad.getUniNombre2());
      sentencia.setObject(i++, uniUnidad.getUniNombre3());
      sentencia.setObject(i++, uniUnidad.getUniNombre4());
      sentencia.setObject(i++, uniUnidad.getUniNombre5());
      sentencia.setObject(i++, uniUnidad.getUniOrden());
      sentencia.setObject(i++, uniUnidad.getUniNivel());
      sentencia.setObject(i++, uniUnidad.getUniCodigo());
      sentencia.setObject(i++, (uniUnidad.getUniIdepadre() == null) ? null : uniUnidad.getUniIdepadre().getUniIderegistro());
      sentencia.setObject(i++, uniUnidad.getUsuIderegistro());
      sentencia.setObject(i++, uniUnidad.getUniPropiedad());
      sentencia.setObject(i++, uniUnidad.getUniIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<UniUnidad> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<UniUnidad> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM uni_unidad";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getUniUnidad(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public UniUnidad consultar(Long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    UniUnidad obj = null;
    try {

      String sql = "SELECT * FROM uni_unidad WHERE uni_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getUniUnidad(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static UniUnidad getUniUnidad(ResultSet rs)
          throws PersistenciaExcepcion
  {
    UniUnidad uniUnidad = new UniUnidad();
    uniUnidad.setUniIderegistro(getObject("uni_ideregistro", Integer.class, rs));
    EstEstructura est_ideregistro = new EstEstructura();
    est_ideregistro.setEstIderegistro(getObject("est_ideregistro", Integer.class, rs));
    uniUnidad.setEstIderegistro(est_ideregistro);
    uniUnidad.setUniCodigo1(getObject("uni_codigo1", String.class, rs));
    uniUnidad.setUniCodigo2(getObject("uni_codigo2", String.class, rs));
    uniUnidad.setUniCodigo3(getObject("uni_codigo3", String.class, rs));
    uniUnidad.setUniCodigo4(getObject("uni_codigo4", String.class, rs));
    uniUnidad.setUniCodigo5(getObject("uni_codigo5", String.class, rs));
    uniUnidad.setUniNombre1(getObject("uni_nombre1", String.class, rs));
    uniUnidad.setUniNombre2(getObject("uni_nombre2", String.class, rs));
    uniUnidad.setUniNombre3(getObject("uni_nombre3", String.class, rs));
    uniUnidad.setUniNombre4(getObject("uni_nombre4", String.class, rs));
    uniUnidad.setUniNombre5(getObject("uni_nombre5", String.class, rs));
    uniUnidad.setUniOrden(getObject("uni_orden", Double.class, rs));
    uniUnidad.setUniNivel(getObject("uni_nivel", Integer.class, rs));
    uniUnidad.setUniCodigo(getObject("uni_codigo", String.class, rs));
    UniUnidad uni_idepadre = new UniUnidad();
    uni_idepadre.setUniIderegistro(getObject("uni_idepadre", Integer.class, rs));
    uniUnidad.setUniIdepadre(uni_idepadre);
    uniUnidad.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    uniUnidad.setUniPropiedad(getObject("uni_propiedad", String.class, rs));

    return uniUnidad;
  }

  public static void getUniUnidad(ResultSet rs, Map<String, Integer> columnas, UniUnidad uniUnidad)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("uni_unidad_uni_ideregistro");
    if (columna != null) {
      uniUnidad.setUniIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("uni_unidad_est_ideregistro");
    if (columna != null) {
      EstEstructura est_ideregistro = new EstEstructura();
      est_ideregistro.setEstIderegistro(getObject(columna, Integer.class, rs));
      uniUnidad.setEstIderegistro(est_ideregistro);
    }
    columna = columnas.get("uni_unidad_uni_codigo1");
    if (columna != null) {
      uniUnidad.setUniCodigo1(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_codigo2");
    if (columna != null) {
      uniUnidad.setUniCodigo2(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_codigo3");
    if (columna != null) {
      uniUnidad.setUniCodigo3(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_codigo4");
    if (columna != null) {
      uniUnidad.setUniCodigo4(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_codigo5");
    if (columna != null) {
      uniUnidad.setUniCodigo5(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nombre1");
    if (columna != null) {
      uniUnidad.setUniNombre1(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nombre2");
    if (columna != null) {
      uniUnidad.setUniNombre2(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nombre3");
    if (columna != null) {
      uniUnidad.setUniNombre3(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nombre4");
    if (columna != null) {
      uniUnidad.setUniNombre4(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nombre5");
    if (columna != null) {
      uniUnidad.setUniNombre5(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_orden");
    if (columna != null) {
      uniUnidad.setUniOrden(getObject(columna, Double.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_nivel");
    if (columna != null) {
      uniUnidad.setUniNivel(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_codigo");
    if (columna != null) {
      uniUnidad.setUniCodigo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_idepadre");
    if (columna != null) {
      UniUnidad uni_idepadre = new UniUnidad();
      uni_idepadre.setUniIderegistro(getObject(columna, Integer.class, rs));
      uniUnidad.setUniIdepadre(uni_idepadre);
    }
    columna = columnas.get("uni_unidad_usu_ideregistro");
    if (columna != null) {
      uniUnidad.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("uni_unidad_uni_propiedad");
    if (columna != null) {
      uniUnidad.setUniPropiedad(getObject(columna, String.class, rs));
    }
  }

  public static void getUniUnidad(ResultSet rs, Map<String, Integer> columnas, UniUnidad uniUnidad, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_uni_ideregistro");
    if (columna != null) {
      uniUnidad.setUniIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo1");
    if (columna != null) {
      uniUnidad.setUniCodigo1(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo2");
    if (columna != null) {
      uniUnidad.setUniCodigo2(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo3");
    if (columna != null) {
      uniUnidad.setUniCodigo3(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo4");
    if (columna != null) {
      uniUnidad.setUniCodigo4(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo5");
    if (columna != null) {
      uniUnidad.setUniCodigo5(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_nombre1");
    if (columna != null) {
      uniUnidad.setUniNombre1(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_nombre2");
    if (columna != null) {
      uniUnidad.setUniNombre2(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_nombre3");
    if (columna != null) {
      uniUnidad.setUniNombre3(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_nombre4");
    if (columna != null) {
      uniUnidad.setUniNombre4(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_nombre5");
    if (columna != null) {
      uniUnidad.setUniNombre5(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_uni_orden");
    if (columna != null) {
      uniUnidad.setUniOrden(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_uni_nivel");
    if (columna != null) {
      uniUnidad.setUniNivel(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_uni_codigo");
    if (columna != null) {
      uniUnidad.setUniCodigo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      uniUnidad.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_uni_propiedad");
    if (columna != null) {
      uniUnidad.setUniPropiedad(getObject(columna, String.class, rs));
    }
  }

  public static UniUnidad getUniUnidad(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    UniUnidad uniUnidad = new UniUnidad();
    getUniUnidad(rs, columnas, uniUnidad);
    return uniUnidad;
  }

  public static UniUnidad getUniUnidad(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    UniUnidad uniUnidad = new UniUnidad();
    getUniUnidad(rs, columnas, uniUnidad, alias);
    return uniUnidad;
  }

}
