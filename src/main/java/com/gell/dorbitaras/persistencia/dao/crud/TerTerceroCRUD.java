package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.entidades.EstEstructura;
import com.gell.dorbitaras.persistencia.entidades.TerTercero;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.constante.EMensajeEstandar;

public class TerTerceroCRUD extends GenericoCRUD {

  private final int ID = 1;

  public TerTerceroCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(TerTercero terTercero)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.ter_tercero(ter_documento,ter_nombre,ter_apellido,ter_nomcompleto,ter_sexo,ter_telcelular,ter_telfijo,est_tiptercero,uni_tiptercero,ter_correo,usu_ideregistro,ciudad_cod,ter_docexpedicion,uni_tipidentifica,ter_fecnacimiento,ter_infoadicional) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?::jsonb)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, terTercero.getTerDocumento());
      sentencia.setObject(i++, terTercero.getTerNombre());
      sentencia.setObject(i++, terTercero.getTerApellido());
      sentencia.setObject(i++, terTercero.getTerNomcompleto());
      sentencia.setObject(i++, terTercero.getTerSexo());
      sentencia.setObject(i++, terTercero.getTerTelcelular());
      sentencia.setObject(i++, terTercero.getTerTelfijo());
      sentencia.setObject(i++, (terTercero.getEstTiptercero() == null) ? null : terTercero.getEstTiptercero().getEstIderegistro());
      sentencia.setObject(i++, (terTercero.getUniTiptercero() == null) ? null : terTercero.getUniTiptercero().getUniIderegistro());
      sentencia.setObject(i++, terTercero.getTerCorreo());
      sentencia.setObject(i++, terTercero.getUsuIderegistro());
      sentencia.setObject(i++, terTercero.getCiudadCod());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerDocexpedicion()));
      sentencia.setObject(i++, terTercero.getUniTipidentifica());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerFecnacimiento()));

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        terTercero.setTerIderegistro(rs.getLong(ID));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(TerTercero terTercero)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.ter_tercero(ter_documento,ter_nombre,ter_apellido,ter_nomcompleto,ter_sexo,ter_telcelular,ter_telfijo,ter_ideregistro,est_tiptercero,uni_tiptercero,ter_correo,usu_ideregistro,ciudad_cod,ter_docexpedicion,uni_tipidentifica,ter_fecnacimiento,ter_infoadicional) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?::jsonb)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, terTercero.getTerDocumento());
      sentencia.setObject(i++, terTercero.getTerNombre());
      sentencia.setObject(i++, terTercero.getTerApellido());
      sentencia.setObject(i++, terTercero.getTerNomcompleto());
      sentencia.setObject(i++, terTercero.getTerSexo());
      sentencia.setObject(i++, terTercero.getTerTelcelular());
      sentencia.setObject(i++, terTercero.getTerTelfijo());
      sentencia.setObject(i++, terTercero.getTerIderegistro());
      sentencia.setObject(i++, (terTercero.getEstTiptercero() == null) ? null : terTercero.getEstTiptercero().getEstIderegistro());
      sentencia.setObject(i++, (terTercero.getUniTiptercero() == null) ? null : terTercero.getUniTiptercero().getUniIderegistro());
      sentencia.setObject(i++, terTercero.getTerCorreo());
      sentencia.setObject(i++, terTercero.getUsuIderegistro());
      sentencia.setObject(i++, terTercero.getCiudadCod());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerDocexpedicion()));
      sentencia.setObject(i++, terTercero.getUniTipidentifica());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerFecnacimiento()));

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(TerTercero terTercero)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.ter_tercero SET ter_documento=?,ter_nombre=?,ter_apellido=?,ter_nomcompleto=?,ter_sexo=?,ter_telcelular=?,ter_telfijo=?,est_tiptercero=?,uni_tiptercero=?,ter_correo=?,usu_ideregistro=?,ciudad_cod=?,ter_docexpedicion=?,uni_tipidentifica=?,ter_fecnacimiento=?,ter_infoadicional=?::jsonb where ter_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, terTercero.getTerDocumento());
      sentencia.setObject(i++, terTercero.getTerNombre());
      sentencia.setObject(i++, terTercero.getTerApellido());
      sentencia.setObject(i++, terTercero.getTerNomcompleto());
      sentencia.setObject(i++, terTercero.getTerSexo());
      sentencia.setObject(i++, terTercero.getTerTelcelular());
      sentencia.setObject(i++, terTercero.getTerTelfijo());
      sentencia.setObject(i++, (terTercero.getEstTiptercero() == null) ? null : terTercero.getEstTiptercero().getEstIderegistro());
      sentencia.setObject(i++, (terTercero.getUniTiptercero() == null) ? null : terTercero.getUniTiptercero().getUniIderegistro());
      sentencia.setObject(i++, terTercero.getTerCorreo());
      sentencia.setObject(i++, terTercero.getUsuIderegistro());
      sentencia.setObject(i++, terTercero.getCiudadCod());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerDocexpedicion()));
      sentencia.setObject(i++, terTercero.getUniTipidentifica());
      sentencia.setObject(i++, DateUtil.parseTimestamp(terTercero.getTerFecnacimiento()));
      sentencia.setObject(i++, terTercero.getTerIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<TerTercero> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<TerTercero> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.ter_tercero";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getTerTercero(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public TerTercero consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    TerTercero obj = null;
    try {

      String sql = "SELECT * FROM public.ter_tercero WHERE ter_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getTerTercero(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static TerTercero getTerTercero(ResultSet rs)
          throws PersistenciaExcepcion
  {
    TerTercero terTercero = new TerTercero();
    terTercero.setTerDocumento(getObject("ter_documento", String.class, rs));
    terTercero.setTerNombre(getObject("ter_nombre", String.class, rs));
    terTercero.setTerApellido(getObject("ter_apellido", String.class, rs));
    terTercero.setTerNomcompleto(getObject("ter_nomcompleto", String.class, rs));
    terTercero.setTerSexo(getObject("ter_sexo", String.class, rs));
    terTercero.setTerTelcelular(getObject("ter_telcelular", String.class, rs));
    terTercero.setTerTelfijo(getObject("ter_telfijo", String.class, rs));
    terTercero.setTerIderegistro(getObject("ter_ideregistro", Long.class, rs));
    EstEstructura est_tiptercero = new EstEstructura();
    est_tiptercero.setEstIderegistro(getObject("est_tiptercero", Integer.class, rs));
    terTercero.setEstTiptercero(est_tiptercero);
    UniUnidad uni_tiptercero = new UniUnidad();
    uni_tiptercero.setUniIderegistro(getObject("uni_tiptercero", Integer.class, rs));
    terTercero.setUniTiptercero(uni_tiptercero);
    terTercero.setTerCorreo(getObject("ter_correo", String.class, rs));
    terTercero.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    terTercero.setCiudadCod(getObject("ciudad_cod", String.class, rs));
    terTercero.setTerDocexpedicion(getObject("ter_docexpedicion", Timestamp.class, rs));
    terTercero.setUniTipidentifica(getObject("uni_tipidentifica", Integer.class, rs));
    terTercero.setTerFecnacimiento(getObject("ter_fecnacimiento", Timestamp.class, rs));

    return terTercero;
  }

  public static void getTerTercero(ResultSet rs, Map<String, Integer> columnas, TerTercero terTercero)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("ter_tercero_ter_documento");
    if (columna != null) {
      terTercero.setTerDocumento(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_nombre");
    if (columna != null) {
      terTercero.setTerNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_apellido");
    if (columna != null) {
      terTercero.setTerApellido(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_nomcompleto");
    if (columna != null) {
      terTercero.setTerNomcompleto(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_sexo");
    if (columna != null) {
      terTercero.setTerSexo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_telcelular");
    if (columna != null) {
      terTercero.setTerTelcelular(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_telfijo");
    if (columna != null) {
      terTercero.setTerTelfijo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_ideregistro");
    if (columna != null) {
      terTercero.setTerIderegistro(getObject(columna, Long.class, rs));
    }
    columna = columnas.get("ter_tercero_est_tiptercero");
    if (columna != null) {
      EstEstructura est_tiptercero = new EstEstructura();
      est_tiptercero.setEstIderegistro(getObject(columna, Integer.class, rs));
      terTercero.setEstTiptercero(est_tiptercero);
    }
    columna = columnas.get("ter_tercero_uni_tiptercero");
    if (columna != null) {
      UniUnidad uni_tiptercero = new UniUnidad();
      uni_tiptercero.setUniIderegistro(getObject(columna, Integer.class, rs));
      terTercero.setUniTiptercero(uni_tiptercero);
    }
    columna = columnas.get("ter_tercero_ter_correo");
    if (columna != null) {
      terTercero.setTerCorreo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_usu_ideregistro");
    if (columna != null) {
      terTercero.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("ter_tercero_ciudad_cod");
    if (columna != null) {
      terTercero.setCiudadCod(getObject(columna, String.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_docexpedicion");
    if (columna != null) {
      terTercero.setTerDocexpedicion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get("ter_tercero_uni_tipidentifica");
    if (columna != null) {
      terTercero.setUniTipidentifica(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("ter_tercero_ter_fecnacimiento");
    if (columna != null) {
      terTercero.setTerFecnacimiento(getObject(columna, Timestamp.class, rs));
    }
  }

  public static void getTerTercero(ResultSet rs, Map<String, Integer> columnas, TerTercero terTercero, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_ter_documento");
    if (columna != null) {
      terTercero.setTerDocumento(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_nombre");
    if (columna != null) {
      terTercero.setTerNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_apellido");
    if (columna != null) {
      terTercero.setTerApellido(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_nomcompleto");
    if (columna != null) {
      terTercero.setTerNomcompleto(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_sexo");
    if (columna != null) {
      terTercero.setTerSexo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_telcelular");
    if (columna != null) {
      terTercero.setTerTelcelular(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_telfijo");
    if (columna != null) {
      terTercero.setTerTelfijo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_ideregistro");
    if (columna != null) {
      terTercero.setTerIderegistro(getObject(columna, Long.class, rs));
    }
    columna = columnas.get(alias + "_ter_correo");
    if (columna != null) {
      terTercero.setTerCorreo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      terTercero.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_ciudad_cod");
    if (columna != null) {
      terTercero.setCiudadCod(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_ter_docexpedicion");
    if (columna != null) {
      terTercero.setTerDocexpedicion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_uni_tipidentifica");
    if (columna != null) {
      terTercero.setUniTipidentifica(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_ter_fecnacimiento");
    if (columna != null) {
      terTercero.setTerFecnacimiento(getObject(columna, Timestamp.class, rs));
    }
  }

  public static TerTercero getTerTercero(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    TerTercero terTercero = new TerTercero();
    getTerTercero(rs, columnas, terTercero);
    return terTercero;
  }

  public static TerTercero getTerTercero(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    TerTercero terTercero = new TerTercero();
    getTerTercero(rs, columnas, terTercero, alias);
    return terTercero;
  }

}
