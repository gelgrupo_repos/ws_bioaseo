/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaQaTotalHistoricoCRUD;
import com.gell.dorbitaras.persistencia.entidades.VrtaQaTotalHistorico;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaQaTotalHistoricoDAO extends VrtaQaTotalHistoricoCRUD {
    
    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaQaTotalHistoricoDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaVariableHist Datos de la variable a insertar.
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public Integer insertar(VrtaQaTotalHistorico vrtaTaCalcHistorico)
            throws PersistenciaExcepcion {
       /* if (vrtaTaCalcHistorico.getVrtaqahistideregistro() != null) {
            editar(vrtaTaCalcHistorico);
            return;
        }*/
        return super.insertar(vrtaTaCalcHistorico);
    }
    
    public List<VrtaQaTotalHistorico> obtenerUltimosTA(
            Integer idPeriodoPadre)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * ")
                .append(" from aseo.vrta_qa_total_historico ")
                .append(" where periodo_padre_calculo = :idPeriodo ")
                .append(" order by numero_actualizacion desc, periodo_ejecucion desc ")
                .append(" limit 2 ");
        parametros.put("idPeriodo", idPeriodoPadre);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaQaTotalHistorico(
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("periodo_ejecucion", Integer.class, rs),
                    getObject("tipo", String.class, rs),
                    getObject("periodo_padre_calculo", Integer.class, rs),
                    getObject("fecha_registro", Timestamp.class, rs),
                    getObject("usuario_registro", Integer.class, rs),
                    getObject("valor_qa_total", Double.class, rs),
                    getObject("primer_ta", Boolean.class, rs)
                    
            );
        });
    }
    
}
