package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class VrtaVarteraprCRUD extends GenericoCRUD
{

  public VrtaVarteraprCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(VrtaVarterapr vrtaVarterapr)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.vrta_varterapr(ter_ideregistro,per_ideregistro,con_ideregistro,arpr_ideregistro,vrta_valor,vrta_descripcion,vrta_estado,usu_ideregistro_gb,vrta_fecgrabacion,usu_ideregistro_cer,vrta_feccertificacion,emp_ideregistro,vrta_estado_registro,raco_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object terIderegistro = (vrtaVarterapr.getTerIderegistro() == null) ? null : vrtaVarterapr.getTerIderegistro().getTerIderegistro();
      sentencia.setObject(i++, terIderegistro);
      Object perIderegistro = (vrtaVarterapr.getPerIderegistro() == null) ? null : vrtaVarterapr.getPerIderegistro().getPerIderegistro();
      sentencia.setObject(i++, perIderegistro);
      Object conIderegistro = (vrtaVarterapr.getConIderegistro() == null) ? null : vrtaVarterapr.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject(i++, conIderegistro);
      Object arprIderegistro = (vrtaVarterapr.getArprIderegistro() == null) ? null : vrtaVarterapr.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaValor());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaDescripcion());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstado());
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroGb());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFecgrabacion()));
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroCer());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFeccertificacion()));
      sentencia.setObject(i++, vrtaVarterapr.getEmpIderegistro());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstadoRegistro());
      Object racoIderegistro = (vrtaVarterapr.getRacoIderegistro() == null) ? null : vrtaVarterapr.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject(i++, racoIderegistro);

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }
  
    public Integer insertarRetornaId(VrtaVarterapr vrtaVarterapr)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    Integer id = 0;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.vrta_varterapr(ter_ideregistro,per_ideregistro,con_ideregistro,arpr_ideregistro,vrta_valor,vrta_descripcion,vrta_estado,usu_ideregistro_gb,vrta_fecgrabacion,usu_ideregistro_cer,vrta_feccertificacion,emp_ideregistro,vrta_estado_registro,raco_ideregistro,comercializacion) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      Object terIderegistro = (vrtaVarterapr.getTerIderegistro() == null) ? null : vrtaVarterapr.getTerIderegistro().getTerIderegistro();
      sentencia.setObject(i++, terIderegistro);
      Object perIderegistro = (vrtaVarterapr.getPerIderegistro() == null) ? null : vrtaVarterapr.getPerIderegistro().getPerIderegistro();
      sentencia.setObject(i++, perIderegistro);
      Object conIderegistro = (vrtaVarterapr.getConIderegistro() == null) ? null : vrtaVarterapr.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject(i++, conIderegistro);
      Object arprIderegistro = (vrtaVarterapr.getArprIderegistro() == null) ? null : vrtaVarterapr.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaValor());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaDescripcion());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstado());
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroGb());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFecgrabacion()));
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroCer());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFeccertificacion()));
      sentencia.setObject(i++, vrtaVarterapr.getEmpIderegistro());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstadoRegistro());
      Object racoIderegistro = (vrtaVarterapr.getRacoIderegistro() == null) ? null : vrtaVarterapr.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject(i++, racoIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getComercializacion());
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
        id = rs.getInt("vrta_ideregistro");
      }

    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
        
      desconectar(sentencia);
      return id;
    }
  }

  public void insertarTodos(VrtaVarterapr vrtaVarterapr)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.vrta_varterapr(vrta_ideregistro,ter_ideregistro,per_ideregistro,con_ideregistro,arpr_ideregistro,vrta_valor,vrta_descripcion,vrta_estado,usu_ideregistro_gb,vrta_fecgrabacion,usu_ideregistro_cer,vrta_feccertificacion,emp_ideregistro,vrta_estado_registro,raco_ideregistro) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaIderegistro());
      Object terIderegistro = (vrtaVarterapr.getTerIderegistro() == null) ? null : vrtaVarterapr.getTerIderegistro().getTerIderegistro();
      sentencia.setObject(i++, terIderegistro);
      Object perIderegistro = (vrtaVarterapr.getPerIderegistro() == null) ? null : vrtaVarterapr.getPerIderegistro().getPerIderegistro();
      sentencia.setObject(i++, perIderegistro);
      Object conIderegistro = (vrtaVarterapr.getConIderegistro() == null) ? null : vrtaVarterapr.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject(i++, conIderegistro);
      Object arprIderegistro = (vrtaVarterapr.getArprIderegistro() == null) ? null : vrtaVarterapr.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaValor());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaDescripcion());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstado());
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroGb());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFecgrabacion()));
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroCer());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFeccertificacion()));
      sentencia.setObject(i++, vrtaVarterapr.getEmpIderegistro());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstadoRegistro());
      Object racoIderegistro = (vrtaVarterapr.getRacoIderegistro() == null) ? null : vrtaVarterapr.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject(i++, racoIderegistro);

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(VrtaVarterapr vrtaVarterapr)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.vrta_varterapr SET ter_ideregistro=?,per_ideregistro=?,con_ideregistro=?,arpr_ideregistro=?,vrta_valor=?,vrta_descripcion=?,vrta_estado=?,usu_ideregistro_gb=?,vrta_fecgrabacion=?,usu_ideregistro_cer=?,vrta_feccertificacion=?,emp_ideregistro=?,vrta_estado_registro=?,raco_ideregistro=? where vrta_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      Object terIderegistro = (vrtaVarterapr.getTerIderegistro() == null) ? null : vrtaVarterapr.getTerIderegistro().getTerIderegistro();
      sentencia.setObject(i++, terIderegistro);
      Object perIderegistro = (vrtaVarterapr.getPerIderegistro() == null) ? null : vrtaVarterapr.getPerIderegistro().getPerIderegistro();
      sentencia.setObject(i++, perIderegistro);
      Object conIderegistro = (vrtaVarterapr.getConIderegistro() == null) ? null : vrtaVarterapr.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject(i++, conIderegistro);
      Object arprIderegistro = (vrtaVarterapr.getArprIderegistro() == null) ? null : vrtaVarterapr.getArprIderegistro().getArprIderegistro();
      sentencia.setObject(i++, arprIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaValor());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaDescripcion());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstado());
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroGb());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFecgrabacion()));
      sentencia.setObject(i++, vrtaVarterapr.getUsuIderegistroCer());
      sentencia.setObject(i++, DateUtil.parseTimestamp(vrtaVarterapr.getVrtaFeccertificacion()));
      sentencia.setObject(i++, vrtaVarterapr.getEmpIderegistro());
      sentencia.setObject(i++, vrtaVarterapr.getVrtaEstadoRegistro());
      Object racoIderegistro = (vrtaVarterapr.getRacoIderegistro() == null) ? null : vrtaVarterapr.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject(i++, racoIderegistro);
      sentencia.setObject(i++, vrtaVarterapr.getVrtaIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<VrtaVarterapr> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<VrtaVarterapr> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.vrta_varterapr";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getVrtaVarterapr(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public VrtaVarterapr consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    VrtaVarterapr obj = null;
    try {

      String sql = "SELECT * FROM aseo.vrta_varterapr WHERE vrta_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getVrtaVarterapr(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static VrtaVarterapr getVrtaVarterapr(ResultSet rs)
          throws PersistenciaExcepcion
  {
    VrtaVarterapr vrtaVarterapr = new VrtaVarterapr();
    vrtaVarterapr.setVrtaIderegistro(getObject("vrta_ideregistro", Integer.class, rs));
    TerTercero ter_ideregistro = new TerTercero();
    ter_ideregistro.setTerIderegistro(getObject("ter_ideregistro", Long.class, rs));
    vrtaVarterapr.setTerIderegistro(ter_ideregistro);
    PerPeriodo per_ideregistro = new PerPeriodo();
    per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
    vrtaVarterapr.setPerIderegistro(per_ideregistro);
    ConConcepto con_ideregistro = new ConConcepto();
    con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
    vrtaVarterapr.setConIderegistro(con_ideregistro);
    ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
    arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    vrtaVarterapr.setArprIderegistro(arpr_ideregistro);
    vrtaVarterapr.setVrtaValor(getObject("vrta_valor", Double.class, rs));
    vrtaVarterapr.setVrtaDescripcion(getObject("vrta_descripcion", String.class, rs));
    vrtaVarterapr.setVrtaEstado(getObject("vrta_estado", String.class, rs));
    vrtaVarterapr.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
    vrtaVarterapr.setVrtaFecgrabacion(getObject("vrta_fecgrabacion", Timestamp.class, rs));
    vrtaVarterapr.setUsuIderegistroCer(getObject("usu_ideregistro_cer", Integer.class, rs));
    vrtaVarterapr.setVrtaFeccertificacion(getObject("vrta_feccertificacion", Timestamp.class, rs));
    vrtaVarterapr.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
    vrtaVarterapr.setVrtaEstadoRegistro(getObject("vrta_estado_registro", String.class, rs));
    RacoRanconcept raco_ideregistro = new RacoRanconcept();
    raco_ideregistro.setRacoIderegistr(getObject("raco_ideregistro", Integer.class, rs));
    vrtaVarterapr.setRacoIderegistro(raco_ideregistro);
    vrtaVarterapr.setComercializacion(getObject("comercializacion", boolean.class, rs));

    return vrtaVarterapr;
  }

  public static VrtaVarterapr getVrtaVarterapr(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    VrtaVarterapr vrtaVarterapr = new VrtaVarterapr();
    Integer columna = columnas.get(alias + "_vrta_ideregistro");
    if (columna != null) {
      vrtaVarterapr.setVrtaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_ter_ideregistro");
    if (columna != null) {
      TerTercero ter_ideregistro = new TerTercero();
      ter_ideregistro.setTerIderegistro(getObject(columna, Long.class, rs));
      vrtaVarterapr.setTerIderegistro(ter_ideregistro);
    }
    columna = columnas.get(alias + "_per_ideregistro");
    if (columna != null) {
      PerPeriodo per_ideregistro = new PerPeriodo();
      per_ideregistro.setPerIderegistro(getObject(columna, Integer.class, rs));
      vrtaVarterapr.setPerIderegistro(per_ideregistro);
    }
    columna = columnas.get(alias + "_con_ideregistro");
    if (columna != null) {
      ConConcepto con_ideregistro = new ConConcepto();
      con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
      vrtaVarterapr.setConIderegistro(con_ideregistro);
    }
    columna = columnas.get(alias + "_arpr_ideregistro");
    if (columna != null) {
      ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
      arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
      vrtaVarterapr.setArprIderegistro(arpr_ideregistro);
    }
    columna = columnas.get(alias + "_vrta_valor");
    if (columna != null) {
      vrtaVarterapr.setVrtaValor(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_vrta_descripcion");
    if (columna != null) {
      vrtaVarterapr.setVrtaDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_vrta_estado");
    if (columna != null) {
      vrtaVarterapr.setVrtaEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_gb");
    if (columna != null) {
      vrtaVarterapr.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_vrta_fecgrabacion");
    if (columna != null) {
      vrtaVarterapr.setVrtaFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_cer");
    if (columna != null) {
      vrtaVarterapr.setUsuIderegistroCer(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_vrta_feccertificacion");
    if (columna != null) {
      vrtaVarterapr.setVrtaFeccertificacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_emp_ideregistro");
    if (columna != null) {
      vrtaVarterapr.setEmpIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_vrta_estado_registro");
    if (columna != null) {
      vrtaVarterapr.setVrtaEstadoRegistro(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_raco_ideregistro");
    if (columna != null) {
      RacoRanconcept raco_ideregistro = new RacoRanconcept();
      raco_ideregistro.setRacoIderegistr(getObject(columna, Integer.class, rs));
      vrtaVarterapr.setRacoIderegistro(raco_ideregistro);
    }
    columna = columnas.get(alias + "_comercializacion");
    if (columna != null) {
      vrtaVarterapr.setComercializacion(getObject(columna, Boolean.class, rs));
    }
    return vrtaVarterapr;
  }

}
