/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaPromFinalesCRUD;
import com.gell.dorbitaras.persistencia.dto.PorcentajeFinalRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeRecalculoDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.dorbitaras.persistencia.entidades.VrtaPromFinales;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaPromFinalesDAO extends VrtaPromFinalesCRUD {

    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaPromFinalesDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaPromFinales
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public void insertar(VrtaPromFinales vrtaPromFinales)
            throws PersistenciaExcepcion {
        super.insertar(vrtaPromFinales);
    }

    public List<PorcentajeFinalRecalculoDTO> obtenerTercerPorcentaje(
            Integer idPeriodo,
            Integer numeroActualizacion
    )
    throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select round(valor_promedio,5) as valor_promedio, numero_actualizacion, ter_ideregistro, cc.uni_concepto ")
            .append("    from aseo.vrta_prom_finales vpf ")
            .append(" inner join con_concepto cc ")
            .append(" on cc.uni_concepto = vpf.con_ideregistro ")
            .append("       where per_ideregistro = :idPeriodo ")
            .append("       and numero_actualizacion = :numeroActualizacion ")
            .append("       and con_ideregistro IN (5981,6444) ")
            .append("       and tipo = 'R'  ");
            parametros.put("idPeriodo", idPeriodo);
            parametros.put("numeroActualizacion", numeroActualizacion);
        return ejecutarConsulta(sql, parametros, (rs,columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                getObject("valor_promedio", Double.class, rs),
                getObject("numero_actualizacion", Integer.class, rs),
                getObject("ter_ideregistro", Integer.class, rs),
                getObject("uni_concepto", Integer.class, rs));
            });      
    }
    
    public List<PorcentajeFinalRecalculoDTO> obtenerprimerysegundoporcentaje(
            Integer idPeriodo,
            Integer numeroActualizacion,
            String TipoCalculo
    )
    throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        if(TipoCalculo.equals("C")){
            sql.append("	select round(valor_promedio,5) as valor_promedio, 	")
            .append("	numero_actualizacion, ter_ideregistro, cc.uni_concepto 	")
            .append("   from aseo.vrta_prom_finales vpf 	")
            .append("       inner join con_concepto cc 	")
            .append("	             on cc.uni_concepto = vpf.con_ideregistro 	")
            .append("        where per_ideregistro = :idPeriodo 	")
            .append("        and numero_actualizacion = 	")
            .append("       (select max(vp.numero_actualizacion) from aseo.vrta_prom_finales vp where per_ideregistro = :idPeriodo and vp.tipo='C')	")
            .append(" and con_ideregistro IN (5980,5979) 	")
            .append(" and tipo = 'C';	");            
        }else{
            sql.append("	select round(valor_promedio,5) as valor_promedio, 	")
                .append("	numero_actualizacion, ter_ideregistro, cc.uni_concepto 	")
                .append("	                from aseo.vrta_prom_finales vpf 	")
                .append("	             inner join con_concepto cc 	")
                .append("	             on cc.uni_concepto = vpf.con_ideregistro 	")
                .append("	                   where per_ideregistro = :idPeriodo 	")
                .append("	                   and numero_actualizacion = :numeroActualizacion	")
                .append("	                   and con_ideregistro IN (5980,5979) 	")
                .append("	                   and tipo = 'R';	");

        }

            parametros.put("idPeriodo", idPeriodo);
            parametros.put("numeroActualizacion", numeroActualizacion);
        return ejecutarConsulta(sql, parametros, (rs,columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                getObject("valor_promedio", Double.class, rs),
                getObject("numero_actualizacion", Integer.class, rs),
                getObject("ter_ideregistro", Integer.class, rs),
                getObject("uni_concepto", Integer.class, rs));
            });      
    }
}
