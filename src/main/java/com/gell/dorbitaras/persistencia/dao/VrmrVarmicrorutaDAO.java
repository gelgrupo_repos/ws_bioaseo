/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.VrmrVarmicrorutaCRUD;
import com.gell.dorbitaras.persistencia.entidades.VrmrVarmicroruta;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author jcpacheco
 */
public class VrmrVarmicrorutaDAO extends VrmrVarmicrorutaCRUD{
    
    public VrmrVarmicrorutaDAO(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(dataSource, auditoria);
    }
    
  /**
   * Método encargado de consultar los datos de la variable según el periodo,
   * concepto y área de prestación.
   *
   * @param periodo Identificador del periodo.
   * @param concepto Idenfificador del concepto.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Variable consultada.
   * @throws PersistenciaExcepcion
   */
  public List<VrmrVarmicroruta> consultarIndicadoresCertificados(
          Integer periodo,
          Integer areaPrestacion,
          String tipoRuta
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append(" FROM ")
            .append("ASEO.VRMR_VARMICRORUTA VV ")
            .append("INNER JOIN RUT_RUTA RUT ON ")
            .append("VV.RUT_IDEMICRORUTA = RUT.RUT_IDEREGISTRO ")
            .append("INNER JOIN CIC_CICLO CIC ON ")
            .append("CIC.CIC_IDEREGISTRO = RUT.CIC_IDEREGISTRO ")
            .append("INNER JOIN UNI_UNIDAD UNI ON ")
            .append("RUT.UNI_TIPORUTA = UNI.UNI_IDEREGISTRO ")
            .append("WHERE ")
            .append("UNI.UNI_NOMBRE1 ILIKE :tipoRuta ") 
            .append("AND CIC.CIC_ESTADO = :estado ")
            .append("and VV.PER_IDEREGISTRO = :periodo ")
            .append("AND VV.VRMR_ESTADO = :estadoCertificado ")
            .append("and VV.arpr_ideregistro = :area ")
            .append("and VV.emp_ideregistro = :empresa ");
    parametros.put("tipoRuta", '%' + tipoRuta + '%');
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("periodo", periodo);
    parametros.put("estadoCertificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("area", areaPrestacion);
    parametros.put("empresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVrmrVarmicroruta(rs));
  }
  
  /**
   * Método encargado de consultar los datos de la variable según el periodo,
   * concepto y área de prestación.
   *
   * @param periodo Identificador del periodo.
   * @param concepto Idenfificador del concepto.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Variable consultada.
   * @throws PersistenciaExcepcion
   */
//  public List<VrmrVarmicroruta> consultarIndicadoresCertificados(
//          Integer periodo,
//          Integer concepto,
//          Integer areaPrestacion
//  )
//          throws PersistenciaExcepcion
//  {
//    StringBuilder sql = new StringBuilder();
//    sql.append("SELECT rcc.raco_raninicial , rcc.raco_ranfinal, var.* ")
//            .append("FROM aseo.varc_varcalculo var ")
//            .append("LEFT JOIN public.raco_ranconcept rcc ")
//            .append("ON rcc.raco_ideregistr = var.raco_ideregistro ")
//            .append("WHERE var.per_ideregistro = :periodo ")
//            .append("  AND var.con_ideregistro = :concepto ")
//            .append("  AND var.arpr_ideregistro = :areaPrestacion ")
//            .append("  AND var.varc_estadoregistro = :estado ")
//            .append("  AND var.emp_ideregistro = :idEmpresa")
//            .append("  ORDER BY var.varc_estado , var.raco_ideregistro ");
//    parametros.put("periodo", periodo);
//    parametros.put("concepto", concepto);
//    parametros.put("areaPrestacion", areaPrestacion);
//    parametros.put("estado", EEstadoGenerico.ACTIVO);
//    parametros.put("idEmpresa", auditoria.getIdEmpresa());
//    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVrmrVarmicroruta(rs));
//  }
   
}
