package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.HrrHorrecoleccionCRUD;
import com.gell.dorbitaras.persistencia.entidades.HrrHorrecoleccion;
import javax.sql.DataSource;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;

public class HrrHorrecoleccionDAO extends HrrHorrecoleccionCRUD
{

  public HrrHorrecoleccionDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   *
   * Método para la inserción y edición de datos
   *
   * @param diaRecoleccion Informacíon de (días - horario) Macro Rutas
   * @throws PersistenciaExcepcion
   */
  @Override
  public void insertar(HrrHorrecoleccion diaRecoleccion)
          throws PersistenciaExcepcion
  {
    Integer ideRegistro = diaRecoleccion.getHrrIderegistro();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    HrrHorrecoleccion documentoGuardada = consultar(ideRegistro.longValue());
    if (documentoGuardada != null) {
      this.editar(diaRecoleccion);
      return;
    }
    diaRecoleccion.setHrrSwtact(EEstadoGenerico.ACTIVO);
    super.insertar(diaRecoleccion);
  }
}
