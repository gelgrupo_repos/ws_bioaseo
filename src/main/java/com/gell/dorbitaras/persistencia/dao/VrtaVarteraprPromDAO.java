/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.VrtaVarteraprPromCRUD;
import com.gell.dorbitaras.persistencia.dto.PorcentajeRecalculoDTO;
import com.gell.dorbitaras.persistencia.dto.PorcentajeFinalRecalculoDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarteraprProm;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaVarteraprPromDAO extends VrtaVarteraprPromCRUD {

    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaVarteraprPromDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }
    
    public VrtaVarteraprPromDAO(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaVarteraprProm
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public void insertar(VrtaVarteraprProm vrtaVarteraprProm)
            throws PersistenciaExcepcion {
        super.insertar(vrtaVarteraprProm);
    }
    
    public Integer obtenerUltimoNumeroActualizacion(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            String tipo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select max(numero_actualizacion) numero_actualizacion ")
                .append(" from aseo.vrta_qa_total_historico ")
                .append(" where periodo_ejecucion = :idPeriodo ")
                .append("  and tipo = :tipo; ");
                parametros.put("idPeriodo", idPeriodo);
                parametros.put("tipo", tipo);
                return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
                    return getObject("numero_actualizacion", Integer.class, rs);
                }).get(0);
    }

    public VrtaVarteraprProm obtenerValorDeUltimaInsercion(
            Integer idPeriodoPadre,
            Long idAsociacion,
            String tipo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select	max(vvp.numero_actualizacion) numero_actualizacion, ")
                .append(" 		vvp.tipo as tipo, ")
                .append(" 		vvp.ta_ejecutado as ta_ejecutado, ")
                .append(" 		vvp.vrta_prom_ideregistro ")
                .append(" from 	aseo.vrta_varterapr_prom vvp ")
                .append(" where 	vvp.per_idepadre = :idPeriodoPadre ")
                .append(" and 	vvp.ter_ideregistro  = :idAsociacion ")
                .append(" and 	vvp.tipo  = :tipo ")
                .append(" group by vvp.ta_ejecutado, vvp.vrta_prom_ideregistro ")
                .append(" order by vvp.numero_actualizacion desc  ")
                .append(" limit 1 ");
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idAsociacion", idAsociacion);
        parametros.put("tipo", tipo);
        List<VrtaVarteraprProm> elementos = ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaVarteraprProm(
                    getObject("vrta_prom_ideregistro", Integer.class, rs),
                    null,
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("tipo", String.class, rs),
                    null,
                    null,
                    getObject("ta_ejecutado", Boolean.class, rs),
                    null,
                    null,
                    null,
                    null);
        });
        return elementos.size() > 0 ? elementos.get(0) : new VrtaVarteraprProm();
    }

    public List<VrtaVarteraprProm> obtieneUltimoPeriodoconQA(Integer idPeriodo, Integer idArea, String tipo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select 	vrta_prom_ideregistro,  ")
                .append("                         per_idepadre, ")
                .append("                         vvp.numero_actualizacion,  ")
                .append("                         vvp.tipo,  ")
                .append("                         vvp.ter_ideregistro,  ")
                .append("                         vrta_prom_valor  ")
                .append("                 from aseo.vrta_varterapr_prom vvp   ")
                .append("                 where vrta_prom_ideregistro in (  ")
                .append("                 select max(vrta_prom_ideregistro)  ")
                .append("                 from aseo.vrta_varterapr_prom vvp  ")
                .append("                 where per_idepadre = :idPeriodoHijo and vvp.tipo = :tipo  ")
                .append("                 group by per_idepadre, ter_ideregistro  ")
                .append("                 )  ")
                .append("                 order by per_idepadre, ter_ideregistro ; ");
        parametros.put("idPeriodoHijo", idPeriodo);
        parametros.put("idArea", idArea);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaVarteraprProm(
                    getObject("vrta_prom_ideregistro", Integer.class, rs),
                    getObject("per_idepadre", Integer.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("tipo", String.class, rs),
                    getObject("ter_ideregistro", Long.class, rs),
                    null,
                    null,
                    null,
                    null,
                    getObject("vrta_prom_valor", Double.class, rs),
                    null);
        });
    }
    
    public List<VrtaVarteraprProm> obtenerSegundoQa(Integer idPeriodo, Integer idArea, String tipo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select 	vrta_prom_ideregistro,  ")
                .append("                         per_idepadre, ")
                .append("                         vvp.numero_actualizacion,  ")
                .append("                         vvp.tipo,  ")
                .append("                         vvp.ter_ideregistro,  ")
                .append("                         case when ( ccc.vrta_valor = 0 or var.vrta_valor = 0 ) then 0 else vrta_prom_valor end as vrta_prom_valor  ")
                .append(" from aseo.vrta_varterapr_prom vvp    ")
                .append(" left  join (select ter_ideregistro, vrta_valor ")
                .append(" from aseo.vrta_varterapr   ")
                .append(" where per_ideregistro = :idPeriodoHijo ")
                .append(" and vrta_estado_registro = :estado ")
                .append(" and vrta_estado = :estadoCertificado and con_ideregistro=:idConcepto ")
                .append(" ) ccc ")
                .append(" on ccc.ter_ideregistro = vvp.ter_ideregistro ")
                .append(" left join ") 
                .append(" (select * ")
                .append("	from aseo.vrta_varterapr vv2 ")  
                .append("	where vv2.per_ideregistro = :idPeriodoHijo ")   
                .append("	and vv2.vrta_estado = 'CE' ")
                .append("	and vv2.vrta_valor = 0 and con_ideregistro=:idConcepto) var ") 
                .append(" on var.ter_ideregistro = vvp.ter_ideregistro ")
                .append(" where vrta_prom_ideregistro in (   ")
                .append(" select max(vrta_prom_ideregistro)   ")
                .append(" from aseo.vrta_varterapr_prom vvp   ")
                .append(" where per_idepadre = :idPeriodoHijo   ")
                .append(" and vvp.tipo = :tipo   ")
                .append(" group by per_idepadre, ter_ideregistro   ")
                .append(" ) ")
                .append(" order by per_idepadre, ter_ideregistro ; ");
    
        parametros.put("idPeriodoHijo", idPeriodo);
        parametros.put("idArea", idArea);
        parametros.put("tipo", tipo);
        parametros.put("estado", EEstadoGenerico.ACTIVO);
        parametros.put("estadoCertificado", EEstadoGenerico.CERTIFICADO);
        parametros.put("idConcepto",2902);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaVarteraprProm(
                    getObject("vrta_prom_ideregistro", Integer.class, rs),
                    getObject("per_idepadre", Integer.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("tipo", String.class, rs),
                    getObject("ter_ideregistro", Long.class, rs),
                    null,
                    null,
                    null,
                    null,
                    getObject("vrta_prom_valor", Double.class, rs),
                    null);
        });
    }
    
 public List<VrtaVarteraprProm> obtenerSegundoRecalculoQa(Integer idPeriodo, Integer idArea, String tipo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select 	vrta_prom_ideregistro,  ")
                .append("                         per_idepadre, ")
                .append("                         vvp.numero_actualizacion,  ")
                .append("                         vvp.tipo,  ")
                .append("                         vvp.ter_ideregistro,  ")
                .append(" case ")
                .append("    when (coalesce(ccs.valor_promedio,0)>0) then vrta_prom_valor else ")
                .append("    case when (ccc.vrta_valor is null) then vrta_prom_valor else")
                .append(" 	case when ( ccc.vrta_valor is not null ")
                .append(" 	and (coalesce(ckccs.comercializacion,false)=true) ")
                .append(" 	) then vrta_prom_valor ")
                .append(" 	else 0 end")
                .append(" 	end ")
                .append(" end as vrta_prom_valor ")  
                .append(" from aseo.vrta_varterapr_prom vvp ")  
                .append("  left  join (select ter_ideregistro, vrta_valor ") 
                .append("  from aseo.vrta_varterapr ")   
                .append("  where per_ideregistro = :idPeriodoHijo ") 
                .append("  and vrta_estado_registro = :estado ") 
                .append("  and vrta_estado = :estadoCertificado ") 
                .append("  and con_ideregistro=:idConcepto ")
//                .append("  limit 1 ")
                .append("  ) ccc on ccc.ter_ideregistro = vvp.ter_ideregistro ") 
                .append("  left join ( ")
                .append("  select tt.ter_nomcompleto ,v.ter_ideregistro,v.valor_promedio as valor_promedio ") 
                .append(" 	from aseo.vrta_prom_finales v ")
                .append(" 	inner join ter_tercero tt on tt.ter_ideregistro = v.ter_ideregistro ")
                .append(" 	where v.per_ideregistro = :idPeriodoHijo ")  
                .append(" 	and v.con_ideregistro = 5980 ")
                .append(" 	and v.tipo = 'C' ") 
                .append(" 	and v.numero_actualizacion = ( ")
                .append(" 	select max(v.numero_actualizacion) ") 
                .append(" 	from aseo.vrta_prom_finales v ") 
                .append(" 	where v.per_ideregistro = :idPeriodoHijo ")   
                .append(" 	and v.con_ideregistro = 5980 ")
                .append(" 	and v.tipo = 'C') ")
                .append("  ) ccs on ccs.ter_ideregistro = vvp.ter_ideregistro ") 
                .append("  left  join (select ter_ideregistro, comercializacion ")
                .append("  from aseo.vrta_varterapr ")   
                .append("  where per_ideregistro = :idPeriodoHijo  ")
                .append("  and comercializacion = true ")
                .append("  and con_ideregistro=2902 and vrta_estado_registro = 'A'") // limit 1 ")
                .append("  ) ckccs on ckccs.ter_ideregistro = vvp.ter_ideregistro ") 
                .append("  where vrta_prom_ideregistro in ( ")   
                .append("  select max(vrta_prom_ideregistro) ")    
                .append("  from aseo.vrta_varterapr_prom vvp ")   
                .append("  where per_idepadre = :idPeriodoHijo ")   
                .append("  and vvp.tipo = :tipo ")   
                .append("  group by per_idepadre, ter_ideregistro) ") 
                .append("  order by per_idepadre, ter_ideregistro; ");
        parametros.put("idPeriodoHijo", idPeriodo);
        parametros.put("idArea", idArea);
        parametros.put("tipo", tipo);
        parametros.put("estado", EEstadoGenerico.ACTIVO);
        parametros.put("estadoCertificado", EEstadoGenerico.CERTIFICADO);
        parametros.put("idConcepto",2902);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaVarteraprProm(
                    getObject("vrta_prom_ideregistro", Integer.class, rs),
                    getObject("per_idepadre", Integer.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("tipo", String.class, rs),
                    getObject("ter_ideregistro", Long.class, rs),
                    null,
                    null,
                    null,
                    null,
                    getObject("vrta_prom_valor", Double.class, rs),
                    null);
        });
    }

    /**
     * Método encargado de inactivar los registros al momento de guardar.
     *
     * @param idRegistro Identificador de la variable.
     * @param valor Identificador de la variable.
     * @throws PersistenciaExcepcion
     */
    public void editarProm(
            Integer idRegistro,
            Double valor
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" update aseo.vrta_varterapr_prom ")
                .append(" set vrta_prom_valor = :valor  ")
                .append(" where vrta_prom_ideregistro = :idRegistro ");
        parametros.put("valor", valor);
        parametros.put("idRegistro", idRegistro);
        ejecutarEdicion(sql, parametros);
    }
    
    /**
     * Método encargado de asignarle el id creado del qa a la asociacion de promedios.
     *
     * @param idRegistro Identificador de la variable.
     * @param valor Identificador de la variable.
     * @throws PersistenciaExcepcion
     */
    public void asignarIdQaPromedios(
            Integer vrtaQaHistIderegistro,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" update aseo.vrta_varterapr_prom ")
                .append(" set vrta_qa_hist_ideregistro = :vrtaQaHistIderegistro  ")
                .append(" where per_idepadre = :idPeriodo ")
                .append(" and numero_actualizacion = :numeroActualizacion  ")
                .append(" and tipo = :tipo; ");
        parametros.put("vrtaQaHistIderegistro", vrtaQaHistIderegistro);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        ejecutarEdicion(sql, parametros);
    }

    public void actualizarTaEjecutado(
            Integer idRegistro
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" update aseo.vrta_varterapr_prom ")
                .append(" set ta_ejecutado = true  ")
                .append(" where vrta_prom_ideregistro = :idRegistro ");
        parametros.put("idRegistro", idRegistro);
        ejecutarEdicion(sql, parametros);
    }

    
    
    public List<PorcentajeFinalRecalculoDTO> obtenerPrimerPromedio(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select (pr.vrta_prom_valor / qah.valor_qa_total) * 100 as vrtaPromValor, ")
	        .append(" qah.numero_actualizacion, ")
	        .append(" pr.ter_ideregistro ")
                .append(" from aseo.vrta_varterapr_prom pr ")
                .append(" inner join aseo.vrta_qa_total_historico qah  ")
                .append(" on pr.vrta_qa_hist_ideregistro  = qah.vrta_qa_hist_ideregistro and pr.per_idepadre = qah.periodo_ejecucion ")
                .append(" where pr.per_idepadre = :idPeriodo ")
                .append(" and pr.tipo = :tipo ")
                .append(" and pr.numero_actualizacion = :numeroActualizacion ")
                .append(" and qah.primer_qa = true ");
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                    getObject("vrtaPromValor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_ideregistro", Integer.class, rs));
        });
    }
    
    public List<PorcentajeFinalRecalculoDTO> obtenerSegundoPromedio(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select case when (var.vrta_valor=0 or pr.vrta_prom_valor = 0 or qah.valor_qa_total = 0 or vv.vrta_valor= 0) then 0 else (pr.vrta_prom_valor / qah.valor_qa_total) * 100 end as vrtaPromValor, ")
	        .append(" qah.numero_actualizacion, ")
	        .append(" pr.ter_ideregistro ")
                .append(" from aseo.vrta_varterapr_prom pr ")
                .append(" inner join aseo.vrta_qa_total_historico qah  ")
                .append(" on pr.vrta_qa_hist_ideregistro  = qah.vrta_qa_hist_ideregistro - 1  ")
                .append(" and pr.per_idepadre = qah.periodo_ejecucion  ")
                .append(" left join aseo.vrta_varterapr vv on vv.ter_ideregistro = pr.ter_ideregistro ") 
                .append(" and vv.per_ideregistro = :idPeriodo ") 
                .append(" and vv.vrta_estado = 'CE' ")
                .append(" and vv.vrta_estado_registro = 'A'")
                .append(" left join ")
                .append("	(select * ")
                .append("	from aseo.vrta_varterapr vv2 ") 
                .append("	where vv2.per_ideregistro = :idPeriodo ")  
                .append("	and vv2.vrta_estado = 'CE' ")
                .append("	and vv2.vrta_valor = 0) var ")
                .append(" on var.ter_ideregistro = vv.ter_ideregistro ") 
                .append(" where pr.per_idepadre = :idPeriodo ")
                .append(" and pr.tipo = :tipo ")
                .append(" and pr.numero_actualizacion = :numeroActualizacion ")
                .append(" and qah.primer_qa = false ");            
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                    getObject("vrtaPromValor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_ideregistro", Integer.class, rs));
        });
    }
    
    public List<PorcentajeFinalRecalculoDTO> obtenerSegundoPromedioRecalculo(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("   select   ")
            .append("   case  ")
            .append("   	when (coalesce(terv.valor_promedio,0)>0 and coalesce(qah.valor_qa_total,0)>0)  ")
            .append("   		then (coalesce(pr.vrta_prom_valor,0) / qah.valor_qa_total) * 100   ")
            .append("   	else  ")
            .append("   	case when ( ccc.vrta_valor is null and coalesce(qah.valor_qa_total,0)>0)  ")
            .append("   	then (coalesce(pr.vrta_prom_valor,0) / qah.valor_qa_total) * 100 else ")
            .append("   	case  ")
            .append("   		when ( ccc.vrta_valor is not null and (coalesce(ckccs.comercializacion,false)=true) and coalesce(qah.valor_qa_total,0)>0)  ")
            .append("  			then (coalesce(pr.vrta_prom_valor,0) / qah.valor_qa_total) * 100  ")
            .append("  	        else 0 end ")
            .append("  		end ")
            .append("   end as vrtaPromValor, ")
            .append("   qah.numero_actualizacion, ")
            .append("   pr.ter_ideregistro,   ")
            .append("   tt.ter_nomcompleto,   ")
            .append("   tt.ter_documento,   ")
            .append("   pr.vrta_prom_valor,   ")
            .append("   qah.valor_qa_total  ")
            .append("   from aseo.vrta_varterapr_prom pr   ")
            .append("   inner join ter_tercero tt on tt.ter_ideregistro = pr.ter_ideregistro   ")
            .append("   inner join aseo.vrta_qa_total_historico qah  ")
            .append("   on pr.vrta_qa_hist_ideregistro  = qah.vrta_qa_hist_ideregistro - 1    ")
            .append("   and pr.per_idepadre = qah.periodo_ejecucion    ")
            .append("   left  join (select ter_ideregistro, vrta_valor   ")
            .append("   from aseo.vrta_varterapr     ")
            .append("   where per_ideregistro = :idPeriodo  ")
            .append("   and vrta_estado_registro = 'A'  ")
            .append("   and vrta_estado = 'CE' ")
            .append("   and con_ideregistro=2902  ")
            //.append("   limit 1  ")
            .append("   ) ccc on ccc.ter_ideregistro = pr.ter_ideregistro   ")
            .append("   left join (select v.ter_ideregistro,v.valor_promedio as valor_promedio   ")
            .append("   from aseo.vrta_prom_finales v   ")
            .append("   where v.per_ideregistro = :idPeriodo   ")
            .append("   and v.con_ideregistro = 5980   ")
            .append("   and v.tipo = 'C'   ")
            .append("   and v.numero_actualizacion = (  ")
            .append("   select max(v.numero_actualizacion)   ")
            .append("   from aseo.vrta_prom_finales v   ")
            .append("   where v.per_ideregistro = :idPeriodo   ")
            .append("   and v.con_ideregistro = 5980   ")
            .append("   and v.tipo = 'C')) terv on terv.ter_ideregistro=pr.ter_ideregistro   ")
            .append("   left  join (select ter_ideregistro, comercializacion  ")
            .append("   from aseo.vrta_varterapr     ")
            .append("   where per_ideregistro = :idPeriodo   ")
            .append("   and comercializacion = true  ")
            .append("   and con_ideregistro=2902  ")
            .append("   and vrta_estado_registro = 'A') ckccs on ckccs.ter_ideregistro = pr.ter_ideregistro ")
            .append("   where pr.per_idepadre = :idPeriodo   ")
            .append("   and pr.tipo = :tipo   ")
            .append("   and pr.numero_actualizacion = :numeroActualizacion  "); 
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                    getObject("vrtaPromValor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_ideregistro", Integer.class, rs));
        });
    }
    
    public List<PorcentajeFinalRecalculoDTO> obtenerValoresPromActualizacion(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        System.out.print("obtenerValoresPromActualizacion periodo->"+idPeriodo+" y numactualizacion->"+numeroActualizacion+" y tipo->"+tipo);
        if (numeroActualizacion >= 0) {
            sql.append(" select pr.vrta_prom_valor as vrtaPromValor, ")
	        .append(" qah.numero_actualizacion, ")
	        .append(" pr.ter_ideregistro ")
                .append(" from aseo.vrta_varterapr_prom pr ")
                .append(" inner join aseo.vrta_qa_total_historico qah  ")
                .append(" on pr.vrta_qa_hist_ideregistro  = qah.vrta_qa_hist_ideregistro and pr.per_idepadre = qah.periodo_ejecucion  ")
                .append(" where qah.periodo_ejecucion = :idPeriodo ")
                .append(" and pr.tipo = :tipo ")
                .append(" and pr.numero_actualizacion = :numeroActualizacion ")
                .append(" and qah.primer_qa = true ");
        } else {
            sql.append(" select pr.vrta_prom_valor as vrtaPromValor, ")
	        .append(" qah.numero_actualizacion, ")
	        .append(" pr.ter_ideregistro ")
                .append(" from aseo.vrta_varterapr_prom pr ")
                .append(" inner join aseo.vrta_qa_total_historico qah  ")
                .append(" on pr.vrta_qa_hist_ideregistro  = qah.vrta_qa_hist_ideregistro and pr.per_idepadre = qah.periodo_ejecucion  ")
                .append(" where qah.periodo_ejecucion = :idPeriodo ")
                .append(" and pr.tipo = 'C' ")
                .append(" and pr.numero_actualizacion = (select MAX(numero_actualizacion)  from aseo.vrta_qa_total_historico where tipo = 'C' and periodo_ejecucion = :idPeriodo) ")
                .append(" and qah.primer_qa = true ");
        }
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                    getObject("vrtaPromValor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_ideregistro", Integer.class, rs));
        });
    }
    
    public List<PorcentajeFinalRecalculoDTO> obtenerPromediosTerceros(
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select vvp.vrta_prom_valor vrtaPromValor, ")
                .append(" numero_actualizacion, ")
                .append(" ter_ideregistro ")
                .append(" from aseo.vrta_varterapr_prom vvp  ")
                .append(" where numero_actualizacion = :numeroActualizacion  ")
                .append(" and tipo = :tipo ");
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("tipo", tipo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new PorcentajeFinalRecalculoDTO(
                    getObject("vrtaPromValor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_ideregistro", Integer.class, rs));
        });
    }

    public List<PorcentajeRecalculoDTO> obtenerPromedioAjustado(
            Integer idPeriodo,
            Integer terIderegistro,
            Integer numeroActualizacion
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select vrta_prom_valor valor, numero_actualizacion, tt.ter_apellido as ter_nombre   ")
                .append("    from aseo.vrta_varterapr_prom vvp ")
                .append("    inner join ter_tercero tt  ")
                .append("    on vvp.ter_ideregistro = tt.ter_ideregistro  ")
                .append("    where vvp.ter_ideregistro = :terIderegistro ")
                .append("    and per_idepadre = :idPeriodo ")
                .append("    and numero_actualizacion = :numeroActualizacion ")
                .append("    and tipo = 'R' limit 1 ");
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("terIderegistro", terIderegistro);
        parametros.put("numeroActualizacion", numeroActualizacion);
        return ejecutarConsulta(sql, parametros, (rs, columns) -> {
            return new PorcentajeRecalculoDTO(
                    getObject("valor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_nombre", String.class, rs));
        });
    }
    
    public List<PorcentajeRecalculoDTO> obtenerPromedioSinAjuste(
            Integer idPeriodo,
            Integer terIderegistro,
            Integer numeroActualizacion
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        if (numeroActualizacion >= 0) {
            sql.append(" select vrta_prom_valor valor, numero_actualizacion, tt.ter_apellido as ter_nombre  ")
                .append("    from aseo.vrta_varterapr_prom vvp ")
                .append("    inner join ter_tercero tt  ")
                .append("    on vvp.ter_ideregistro = tt.ter_ideregistro  ")
                .append("    where vvp.ter_ideregistro = :terIderegistro ")
                .append("    and per_idepadre = :idPeriodo ")
                .append("    and numero_actualizacion = :numeroActualizacion ")
                .append("    and tipo = 'R' limit 1 ");
        } else {
            sql.append(" select vrta_prom_valor valor, numero_actualizacion, tt.ter_apellido as ter_nombre  ")
                .append("    from aseo.vrta_varterapr_prom vvp ")
                .append("    inner join ter_tercero tt  ")
                .append("    on vvp.ter_ideregistro = tt.ter_ideregistro  ")
                .append("    where vvp.ter_ideregistro = :terIderegistro ")
                .append("    and per_idepadre = :idPeriodo ")
                .append("    and numero_actualizacion = (select max(vpf.numero_actualizacion) from aseo.vrta_prom_finales vpf where per_ideregistro = :idPeriodo and tipo = 'C' and vpf.ter_ideregistro = :terIderegistro) ")
                .append("    and tipo = 'C' limit 1 ");
        }
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("terIderegistro", terIderegistro);
        parametros.put("numeroActualizacion", numeroActualizacion);
        return ejecutarConsulta(sql, parametros, (rs, columns) -> {
            return new PorcentajeRecalculoDTO(
                    getObject("valor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_nombre", String.class, rs));
        });
    }

    public List<PorcentajeRecalculoDTO> obtenerPromedioxporcentaje(
            Integer idPeriodo,
            Integer terIderegistro,
            Integer numeroActualizacion,
            String tipo
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        if (tipo.equals("R")) {
            sql.append(" select vrta_prom_valor valor, numero_actualizacion, tt.ter_nomcompleto as ter_nombre  ")
                .append("    from aseo.vrta_varterapr_prom vvp ")
                .append("    inner join ter_tercero tt  ")
                .append("    on vvp.ter_ideregistro = tt.ter_ideregistro  ")
                .append("    where vvp.ter_ideregistro = :terIderegistro ")
                .append("    and per_idepadre = :idPeriodo ")
                .append("    and numero_actualizacion = :numeroActualizacion ")
                .append("    and tipo = 'R' limit 1 ");
        } else {
            sql.append(" select vrta_prom_valor valor, numero_actualizacion, tt.ter_nomcompleto as ter_nombre  ")
                .append("    from aseo.vrta_varterapr_prom vvp ")
                .append("    inner join ter_tercero tt  ")
                .append("    on vvp.ter_ideregistro = tt.ter_ideregistro  ")
                .append("    where vvp.ter_ideregistro = :terIderegistro ")
                .append("    and per_idepadre = :idPeriodo ")
                .append("    and numero_actualizacion = (select max(vpf.numero_actualizacion) from aseo.vrta_prom_finales vpf where per_ideregistro = :idPeriodo and tipo = 'C' and vpf.ter_ideregistro = :terIderegistro) ")
                .append("    and tipo = 'C' limit 1 ");
        }
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("terIderegistro", terIderegistro);
        parametros.put("numeroActualizacion", numeroActualizacion);
        return ejecutarConsulta(sql, parametros, (rs, columns) -> {
            return new PorcentajeRecalculoDTO(
                    getObject("valor", Double.class, rs),
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("ter_nombre", String.class, rs));
        });
    }
    
    
    public List<ConConcepto> obtenerConceptos()
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura IN ('c-FCSE1', 'c-FCSE2', 'c-FCSE3', 'c-FCSE4', 'c-FCSE5', 'c-FCSE6') ")
                .append(" order by con_abreviatura) ")
                .append(" union all ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSPPC') ")
                .append(" union all ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSPPC') ")
                .append(" union all ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSPPC') ")
                .append(" union all ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSPPC') ")
                .append(" union all  ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSGPC') ")
                .append(" union all  ")
                .append(" (select uni_concepto, con_nombre, con_abreviatura, con_valor ")
                .append(" from con_concepto cc  ")
                .append(" where con_abreviatura = 'c-FCSGPO') ");
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            ConConcepto cConcepto = new ConConcepto();
            try {
                cConcepto.setUniConcepto(new UniUnidad().setUniIderegistro(getObject("uni_concepto", Integer.class, rs)));
                cConcepto.setConNombre(getObject("con_nombre", String.class, rs));
                cConcepto.setConAbreviatura(getObject("con_abreviatura", String.class, rs));
                cConcepto.setConValor(getObject("con_valor", Double.class, rs));
            } catch (PersistenciaExcepcion ex) {
                System.out.print(ex);
            }
            return cConcepto;
        });
    }

}
