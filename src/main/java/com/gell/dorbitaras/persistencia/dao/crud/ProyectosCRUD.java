package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.entidades.Empresas;
import com.gell.dorbitaras.persistencia.entidades.Proyectos;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.constante.EMensajeEstandar;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ProyectosCRUD extends GenericoCRUD {

  private final int ID = 1;

  public ProyectosCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(Proyectos proyectos)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.proyectos(proyecto_cod,proyecto_nom,proyecto_codciu,proyecto_codemp,proyecto_ideregistro,departamento_ideregistro,cue_ideregistro) VALUES (?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, proyectos.getProyectoCod());
      sentencia.setObject(i++, proyectos.getProyectoNom());
      sentencia.setObject(i++, proyectos.getProyectoCodciu());
      sentencia.setObject(i++, (proyectos.getProyectoCodemp() == null) ? null : proyectos.getProyectoCodemp().getEmpresaCod());
      sentencia.setObject(i++, proyectos.getProyectoIderegistro());
      sentencia.setObject(i++, proyectos.getDepartamentoIderegistro());
      sentencia.setObject(i++, proyectos.getCueIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        proyectos.setProyectoLlacom(rs.getString(ID));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(Proyectos proyectos)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.proyectos(proyecto_cod,proyecto_nom,proyecto_codciu,proyecto_codemp,proyecto_llacom,proyecto_ideregistro,departamento_ideregistro,cue_ideregistro) VALUES (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, proyectos.getProyectoCod());
      sentencia.setObject(i++, proyectos.getProyectoNom());
      sentencia.setObject(i++, proyectos.getProyectoCodciu());
      sentencia.setObject(i++, (proyectos.getProyectoCodemp() == null) ? null : proyectos.getProyectoCodemp().getEmpresaCod());
      sentencia.setObject(i++, proyectos.getProyectoLlacom());
      sentencia.setObject(i++, proyectos.getProyectoIderegistro());
      sentencia.setObject(i++, proyectos.getDepartamentoIderegistro());
      sentencia.setObject(i++, proyectos.getCueIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(Proyectos proyectos)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.proyectos SET proyecto_cod=?,proyecto_nom=?,proyecto_codciu=?,proyecto_codemp=?,proyecto_ideregistro=?,departamento_ideregistro=?,cue_ideregistro=? where proyecto_llacom=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, proyectos.getProyectoCod());
      sentencia.setObject(i++, proyectos.getProyectoNom());
      sentencia.setObject(i++, proyectos.getProyectoCodciu());
      sentencia.setObject(i++, (proyectos.getProyectoCodemp() == null) ? null : proyectos.getProyectoCodemp().getEmpresaCod());
      sentencia.setObject(i++, proyectos.getProyectoIderegistro());
      sentencia.setObject(i++, proyectos.getDepartamentoIderegistro());
      sentencia.setObject(i++, proyectos.getCueIderegistro());
      sentencia.setObject(i++, proyectos.getProyectoLlacom());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<Proyectos> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<Proyectos> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.proyectos";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getProyectos(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public Proyectos consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    Proyectos obj = null;
    try {

      String sql = "SELECT * FROM public.proyectos WHERE proyecto_llacom=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getProyectos(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajeEstandar.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static Proyectos getProyectos(ResultSet rs)
          throws PersistenciaExcepcion
  {
    Proyectos proyectos = new Proyectos();
    proyectos.setProyectoCod(getObject("proyecto_cod", String.class, rs));
    proyectos.setProyectoNom(getObject("proyecto_nom", String.class, rs));
    proyectos.setProyectoCodciu(getObject("proyecto_codciu", String.class, rs));
    Empresas proyecto_codemp = new Empresas();
    proyecto_codemp.setEmpresaCod(getObject("proyecto_codemp", String.class, rs));
    proyectos.setProyectoCodemp(proyecto_codemp);
    proyectos.setProyectoLlacom(getObject("proyecto_llacom", String.class, rs));
    proyectos.setProyectoIderegistro(getObject("proyecto_ideregistro", Integer.class, rs));
    proyectos.setDepartamentoIderegistro(getObject("departamento_ideregistro", Integer.class, rs));
    proyectos.setCueIderegistro(getObject("cue_ideregistro", Integer.class, rs));

    return proyectos;
  }

  public static void getProyectos(ResultSet rs, Map<String, Integer> columnas, Proyectos proyectos)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("proyectos_proyecto_cod");
    if (columna != null) {
      proyectos.setProyectoCod(getObject(columna, String.class, rs));
    }
    columna = columnas.get("proyectos_proyecto_nom");
    if (columna != null) {
      proyectos.setProyectoNom(getObject(columna, String.class, rs));
    }
    columna = columnas.get("proyectos_proyecto_codciu");
    if (columna != null) {
      proyectos.setProyectoCodciu(getObject(columna, String.class, rs));
    }
    columna = columnas.get("proyectos_proyecto_codemp");
    if (columna != null) {
      Empresas proyecto_codemp = new Empresas();
      proyecto_codemp.setEmpresaCod(getObject(columna, String.class, rs));
      proyectos.setProyectoCodemp(proyecto_codemp);
    }
    columna = columnas.get("proyectos_proyecto_llacom");
    if (columna != null) {
      proyectos.setProyectoLlacom(getObject(columna, String.class, rs));
    }
    columna = columnas.get("proyectos_proyecto_ideregistro");
    if (columna != null) {
      proyectos.setProyectoIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("proyectos_departamento_ideregistro");
    if (columna != null) {
      proyectos.setDepartamentoIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("proyectos_cue_ideregistro");
    if (columna != null) {
      proyectos.setCueIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static void getProyectos(ResultSet rs, Map<String, Integer> columnas, Proyectos proyectos, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_proyecto_cod");
    if (columna != null) {
      proyectos.setProyectoCod(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_proyecto_nom");
    if (columna != null) {
      proyectos.setProyectoNom(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_proyecto_codciu");
    if (columna != null) {
      proyectos.setProyectoCodciu(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_proyecto_llacom");
    if (columna != null) {
      proyectos.setProyectoLlacom(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_proyecto_ideregistro");
    if (columna != null) {
      proyectos.setProyectoIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_departamento_ideregistro");
    if (columna != null) {
      proyectos.setDepartamentoIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_cue_ideregistro");
    if (columna != null) {
      proyectos.setCueIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static Proyectos getProyectos(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    Proyectos proyectos = new Proyectos();
    getProyectos(rs, columnas, proyectos);
    return proyectos;
  }

  public static Proyectos getProyectos(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    Proyectos proyectos = new Proyectos();
    getProyectos(rs, columnas, proyectos, alias);
    return proyectos;
  }

}
