/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaPromFinalesCRUD extends GenericoCRUD {

    public VrtaPromFinalesCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public void insertar(VrtaPromFinales vrtaPromFinales)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        Integer id = -1;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_prom_finales(con_ideregistro,ter_ideregistro,numero_actualizacion,tipo,valor_promedio,per_ideregistro,vrta_qa_hist_ideregistro,vrtafecgrabacion) VALUES (?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object conIderegistro = (vrtaPromFinales.getConIderegistro() == null) ? null : vrtaPromFinales.getConIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object terIderegistro = (vrtaPromFinales.getTerIderegistro() == null) ? null : vrtaPromFinales.getTerIderegistro();
            sentencia.setObject(i++, terIderegistro);
            Object numeroActualizacion = (vrtaPromFinales.getNumeroActualizacion() == null) ? null : vrtaPromFinales.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object tipo = (vrtaPromFinales.getTipo()== null) ? null : vrtaPromFinales.getTipo();
            sentencia.setObject(i++, tipo);
            Object valorPromedio = (vrtaPromFinales.getValorPromedio() == null) ? null : vrtaPromFinales.getValorPromedio();
            sentencia.setObject(i++, valorPromedio);
            Object perIderegistro = (vrtaPromFinales.getPerIderegistro() == null) ? null : vrtaPromFinales.getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object vrtaQaHistIderegistro = (vrtaPromFinales.getVrtaQaHistIderegistro() == null) ? null : vrtaPromFinales.getVrtaQaHistIderegistro();
            sentencia.setObject(i++, vrtaQaHistIderegistro);
            Object vrtafecgrabacion = (vrtaPromFinales.getVrtaFecGrabacion() == null) ? null : vrtaPromFinales.getVrtaFecGrabacion();
            sentencia.setObject(i++, vrtafecgrabacion);
            
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            /*if (rs.next()) {
              vrtaVarteraprProm.setVrtaPromIderegistro(rs.getInt("vrta_prom_ideregistro"));
              id =  rs.getInt("vrta_prom_ideregistro");
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
            // return id;
        }
    }

}
