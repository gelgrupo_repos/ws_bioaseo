package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Time;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import java.time.LocalTime;

public class HrrHorrecoleccionCRUD extends GenericoCRUD
{

  public HrrHorrecoleccionCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(HrrHorrecoleccion hrrHorrecoleccion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO aseo.hrr_horrecoleccion(hrr_dia,rure_ideregistro,hrr_horinicio,hrr_horfin,hrr_swtact) VALUES (:hrr_dia,:rure_ideregistro,:hrr_horinicio,:hrr_horfin,:hrr_swtact)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      sentencia.setObject("hrr_dia", hrrHorrecoleccion.getHrrDia());
      Object rureIderegistro = (hrrHorrecoleccion.getRureIderegistro() == null) ? null : hrrHorrecoleccion.getRureIderegistro().getRureIderegistro();
      sentencia.setObject("rure_ideregistro", rureIderegistro);
      sentencia.setObject("hrr_horinicio", hrrHorrecoleccion.getHrrHorinicio());
      sentencia.setObject("hrr_horfin", hrrHorrecoleccion.getHrrHorfin());
      sentencia.setObject("hrr_swtact", hrrHorrecoleccion.getHrrSwtact());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        hrrHorrecoleccion.setHrrIderegistro(rs.getInt("hrr_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(HrrHorrecoleccion hrrHorrecoleccion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.hrr_horrecoleccion(hrr_ideregistro,hrr_dia,rure_ideregistro,hrr_horinicio,hrr_horfin,hrr_swtact) VALUES (:hrr_ideregistro,:hrr_dia,:rure_ideregistro,:hrr_horinicio,:hrr_horfin,:hrr_swtact)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("hrr_ideregistro", hrrHorrecoleccion.getHrrIderegistro());
      sentencia.setObject("hrr_dia", hrrHorrecoleccion.getHrrDia());
      Object rureIderegistro = (hrrHorrecoleccion.getRureIderegistro() == null) ? null : hrrHorrecoleccion.getRureIderegistro().getRureIderegistro();
      sentencia.setObject("rure_ideregistro", rureIderegistro);
      sentencia.setObject("hrr_horinicio", hrrHorrecoleccion.getHrrHorinicio());
      sentencia.setObject("hrr_horfin", hrrHorrecoleccion.getHrrHorfin());
      sentencia.setObject("hrr_swtact", hrrHorrecoleccion.getHrrSwtact());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(HrrHorrecoleccion hrrHorrecoleccion)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.hrr_horrecoleccion SET hrr_dia=:hrr_dia,rure_ideregistro=:rure_ideregistro,hrr_horinicio=:hrr_horinicio,hrr_horfin=:hrr_horfin,hrr_swtact=:hrr_swtact where hrr_ideregistro = :hrr_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("hrr_dia", hrrHorrecoleccion.getHrrDia());
      Object rureIderegistro = (hrrHorrecoleccion.getRureIderegistro() == null) ? null : hrrHorrecoleccion.getRureIderegistro().getRureIderegistro();
      sentencia.setObject("rure_ideregistro", rureIderegistro);
      sentencia.setObject("hrr_horinicio", hrrHorrecoleccion.getHrrHorinicio());
      sentencia.setObject("hrr_horfin", hrrHorrecoleccion.getHrrHorfin());
      sentencia.setObject("hrr_swtact", hrrHorrecoleccion.getHrrSwtact());
      sentencia.setObject("hrr_ideregistro", hrrHorrecoleccion.getHrrIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<HrrHorrecoleccion> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<HrrHorrecoleccion> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.hrr_horrecoleccion";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getHrrHorrecoleccion(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public HrrHorrecoleccion consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    HrrHorrecoleccion obj = null;
    try {

      String sql = "SELECT * FROM aseo.hrr_horrecoleccion WHERE hrr_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getHrrHorrecoleccion(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static HrrHorrecoleccion getHrrHorrecoleccion(ResultSet rs)
          throws PersistenciaExcepcion
  {
    HrrHorrecoleccion hrrHorrecoleccion = new HrrHorrecoleccion();
    hrrHorrecoleccion.setHrrIderegistro(getObject("hrr_ideregistro", Integer.class, rs));
    hrrHorrecoleccion.setHrrDia(getObject("hrr_dia", String.class, rs));
    RureRutrecoleccion rure_ideregistro = new RureRutrecoleccion();
    rure_ideregistro.setRureIderegistro(getObject("rure_ideregistro", Integer.class, rs));
    hrrHorrecoleccion.setRureIderegistro(rure_ideregistro);
    hrrHorrecoleccion.setHrrHorinicio(getObject("hrr_horinicio", LocalTime.class, rs));
    hrrHorrecoleccion.setHrrHorfin(getObject("hrr_horfin", LocalTime.class, rs));
    hrrHorrecoleccion.setHrrSwtact(getObject("hrr_swtact", String.class, rs));

    return hrrHorrecoleccion;
  }

  public static HrrHorrecoleccion getHrrHorrecoleccion(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    HrrHorrecoleccion hrrHorrecoleccion = new HrrHorrecoleccion();
    Integer columna = columnas.get(alias + "_hrr_ideregistro");
    if (columna != null) {
      hrrHorrecoleccion.setHrrIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_hrr_dia");
    if (columna != null) {
      hrrHorrecoleccion.setHrrDia(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_rure_ideregistro");
    if (columna != null) {
      RureRutrecoleccion rure_ideregistro = new RureRutrecoleccion();
      rure_ideregistro.setRureIderegistro(getObject(columna, Integer.class, rs));
      hrrHorrecoleccion.setRureIderegistro(rure_ideregistro);
    }
    columna = columnas.get(alias + "_hrr_horinicio");
    if (columna != null) {
      hrrHorrecoleccion.setHrrHorinicio(getObject(columna, LocalTime.class, rs));
    }
    columna = columnas.get(alias + "_hrr_horfin");
    if (columna != null) {
      hrrHorrecoleccion.setHrrHorfin(getObject(columna, LocalTime.class, rs));
    }
    columna = columnas.get(alias + "_hrr_swtact");
    if (columna != null) {
      hrrHorrecoleccion.setHrrSwtact(getObject(columna, String.class, rs));
    }
    return hrrHorrecoleccion;
  }

}
