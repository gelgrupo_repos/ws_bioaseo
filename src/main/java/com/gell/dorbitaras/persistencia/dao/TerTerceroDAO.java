package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EClase;
import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EEstructura;
import com.gell.dorbitaras.persistencia.dao.crud.TerTerceroCRUD;
import static com.gell.dorbitaras.persistencia.dao.crud.TerTerceroCRUD.getTerTercero;
import com.gell.dorbitaras.persistencia.entidades.TerTercero;
import com.gell.estandar.dto.AuditoriaDTO;
import static com.gell.estandar.persistencia.abstracto.GenericoCRUD.getObject;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * los terceros.
 *
 * @author Desarrollador
 */
public class TerTerceroDAO extends TerTerceroCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public TerTerceroDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta todos los terceros que tengan asociados alguna unidad de la
   * estructura que se está pasando
   *
   * @param criterio nombre o documento del tercero a buscar
   * @return Lista de todos los terceros que cumplen con las condiciones
   * @throws PersistenciaExcepcion
   */
  public List<TerTercero> consultarPorClasificacion(String criterio)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT ter.*, ")
            .append("                uni.uni_nombre1 tipo ")
            .append("FROM ter_tercero ter ")
            .append("         INNER JOIN clte_clatercero clte on ter.ter_ideregistro = clte.ter_ideregistro ")
            .append("         INNER JOIN uni_unidad uni on clte.uni_clatercero = uni.uni_ideregistro ")
            .append("WHERE uni.est_ideregistro = :idEstructura ")
            .append("  AND (ter.ter_nomcompleto ILIKE :criterio OR ter.ter_documento ILIKE :criterio); ");
    parametros.put("criterio", "%" + criterio + "%");
    parametros.put("idEstructura", EEstructura.TARAS_ENTIDADES_EMITEN);
    return ejecutarConsulta(sql, parametros, (rs, columns) -> {
      String tipo = getObject("tipo", String.class, rs);
      TerTercero terTercero = getTerTercero(rs);
      Properties propiedades = new Properties();
      propiedades.put("tipo", tipo);
      terTercero.setInfo(propiedades);
      return terTercero;
    });
  }

  /**
   * Consulta todas las rutas que tengan asociados alguna unidad de la
   * estructura que se está pasando
   *
   * @param criterio nombre de la ruta a buscar
   * @return Lista de todas las rutas que cumplen con las condiciones
   * @throws PersistenciaExcepcion
   */
  public List<TerTercero> consultarTipoRuta(String criterio)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("  SELECT DISTINCT ter.*,uni.uni_nombre1 tipo, uni.uni_ideregistro uniIderegistro")
            .append(" FROM ter_tercero ter ")
            .append("  INNER JOIN clte_clatercero clte on ter.ter_ideregistro = clte.ter_ideregistro ")
            .append("  INNER JOIN uni_unidad uni on clte.uni_clatercero = uni.uni_ideregistro ")
            .append(" WHERE uni.est_ideregistro = :idEstructura ")
            .append("      AND (ter.ter_nomcompleto ILIKE :criterio OR ter.ter_documento ILIKE :criterio)");
    parametros.put("criterio", "%" + criterio + "%");
    parametros.put("idEstructura", EEstructura.ESTRUCTURA_TIPO_RUTA);
    return ejecutarConsulta(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              String tipo = getObject("tipo", String.class, rs);
              String uniIderegistro = getObject("uniIderegistro", String.class, rs);
              TerTercero terTercero = getTerTercero(rs);
              Properties propiedades = new Properties();
              propiedades.put("tipo", tipo);
              propiedades.put("uniIderegistro", uniIderegistro);
              terTercero.setInfo(propiedades);
              return terTercero;
            });
  }

  /**
   * Consulta todos los terceros por clase.
   *
   * @return Lista con los terceros por clase.
   * @throws PersistenciaExcepcion
   */
  public List<TerTercero> consultarTerceroAprovechador()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT ter.* ")
            .append("FROM ter_tercero ter ")
            .append("         INNER JOIN clte_clatercero clt ON clt.ter_ideregistro = ter.ter_ideregistro ")
            .append("         INNER JOIN uni_unidad uu on clt.uni_clatercero = uu.uni_ideregistro ")
            .append("         INNER JOIN est_estructura ee on uu.est_ideregistro = ee.est_ideregistro ")
            .append("         INNER JOIN esem_estempresa e on ee.est_ideregistro = e.est_ideregistro ")
            .append("WHERE e.emp_ideregistro = :idEmpresa ")
            .append("  AND uu.uni_propiedad ->> 'uniestado' = :estado ")
            .append("  AND clt.uni_clatercero = :idClase ");
    parametros.put("idClase", EClase.APROVECHADOR);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getTerTercero(rs));
  }

}
