package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class RureRutrecoleccionCRUD extends GenericoCRUD {

    public RureRutrecoleccionCRUD(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public void insertar(RureRutrecoleccion rureRutrecoleccion) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.rure_rutrecoleccion(arpr_ideregistro,usu_ideregistro_gb,rure_fecgrabacion,usu_ideregistro_ac,rure_fecact,rut_idemacruta,rut_microruta) VALUES (?,?,?,?,?,?,?::jsonb)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object arprIderegistro = (rureRutrecoleccion.getArprIderegistro() == null) ? null : rureRutrecoleccion.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecgrabacion()));
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroAc());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecact()));
            sentencia.setObject(i++, rureRutrecoleccion.getRutIdemacruta());
            sentencia.setObject(i++, rureRutrecoleccion.getRutMicroruta());

            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                rureRutrecoleccion.setRureIderegistro(rs.getInt("rure_ideregistro"));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public void insertarTodos(RureRutrecoleccion rureRutrecoleccion) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.rure_rutrecoleccion(rure_ideregistro,arpr_ideregistro,usu_ideregistro_gb,rure_fecgrabacion,usu_ideregistro_ac,rure_fecact,rut_idemacruta,rut_microruta) VALUES (?,?,?,?,?,?,?,?::jsonb)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++, rureRutrecoleccion.getRureIderegistro());
            Object arprIderegistro = (rureRutrecoleccion.getArprIderegistro() == null) ? null : rureRutrecoleccion.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecgrabacion()));
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroAc());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecact()));
            sentencia.setObject(i++, rureRutrecoleccion.getRutIdemacruta());
            sentencia.setObject(i++, rureRutrecoleccion.getRutMicroruta());

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public void editar(RureRutrecoleccion rureRutrecoleccion) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.rure_rutrecoleccion SET arpr_ideregistro=?,usu_ideregistro_gb=?,rure_fecgrabacion=?,usu_ideregistro_ac=?,rure_fecact=?,rut_idemacruta=?,rut_microruta=?::jsonb where rure_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
            Object arprIderegistro = (rureRutrecoleccion.getArprIderegistro() == null) ? null : rureRutrecoleccion.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroGb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecgrabacion()));
            sentencia.setObject(i++, rureRutrecoleccion.getUsuIderegistroAc());
            sentencia.setObject(i++, DateUtil.parseTimestamp(rureRutrecoleccion.getRureFecact()));
            sentencia.setObject(i++, rureRutrecoleccion.getRutIdemacruta());
            sentencia.setObject(i++, rureRutrecoleccion.getRutMicroruta());
            sentencia.setObject(i++, rureRutrecoleccion.getRureIderegistro());

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public List<RureRutrecoleccion> consultar() throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<RureRutrecoleccion> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.rure_rutrecoleccion";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getRureRutrecoleccion(rs));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return lista;

    }

    public RureRutrecoleccion consultar(long id) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        RureRutrecoleccion obj = null;
        try {

            String sql = "SELECT * FROM aseo.rure_rutrecoleccion WHERE rure_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getRureRutrecoleccion(rs);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return obj;
    }

    public static RureRutrecoleccion getRureRutrecoleccion(ResultSet rs) throws PersistenciaExcepcion {
        RureRutrecoleccion rureRutrecoleccion = new RureRutrecoleccion();
        rureRutrecoleccion.setRureIderegistro(getObject("rure_ideregistro", Integer.class, rs));
        ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
        arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
        rureRutrecoleccion.setArprIderegistro(arpr_ideregistro);
        rureRutrecoleccion.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
        rureRutrecoleccion.setRureFecgrabacion(getObject("rure_fecgrabacion", Timestamp.class, rs));
        rureRutrecoleccion.setUsuIderegistroAc(getObject("usu_ideregistro_ac", Integer.class, rs));
        rureRutrecoleccion.setRureFecact(getObject("rure_fecact", Timestamp.class, rs));
        rureRutrecoleccion.setRutIdemacruta(getObject("rut_idemacruta", Integer.class, rs));
        rureRutrecoleccion.setRutMicroruta(getObject("rut_microruta", String.class, rs));

        return rureRutrecoleccion;
    }

    public static void getRureRutrecoleccion(ResultSet rs, Map<String, Integer> columnas, RureRutrecoleccion rureRutrecoleccion) throws PersistenciaExcepcion {
        Integer columna = columnas.get("rure_rutrecoleccion_rure_ideregistro");
        if (columna != null) {
            rureRutrecoleccion.setRureIderegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_arpr_ideregistro");
        if (columna != null) {
            ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
            arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
            rureRutrecoleccion.setArprIderegistro(arpr_ideregistro);
        }
        columna = columnas.get("rure_rutrecoleccion_usu_ideregistro_gb");
        if (columna != null) {
            rureRutrecoleccion.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_rure_fecgrabacion");
        if (columna != null) {
            rureRutrecoleccion.setRureFecgrabacion(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_usu_ideregistro_ac");
        if (columna != null) {
            rureRutrecoleccion.setUsuIderegistroAc(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_rure_fecact");
        if (columna != null) {
            rureRutrecoleccion.setRureFecact(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_rut_idemacruta");
        if (columna != null) {
            rureRutrecoleccion.setRutIdemacruta(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get("rure_rutrecoleccion_rut_microruta");
        if (columna != null) {
            rureRutrecoleccion.setRutMicroruta(getObject(columna, String.class, rs));
        }
    }

    public static void getRureRutrecoleccion(ResultSet rs, Map<String, Integer> columnas, RureRutrecoleccion rureRutrecoleccion, String alias) throws PersistenciaExcepcion {
        Integer columna = columnas.get(alias + "_rure_ideregistro");
        if (columna != null) {
            rureRutrecoleccion.setRureIderegistro(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_usu_ideregistro_gb");
        if (columna != null) {
            rureRutrecoleccion.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_rure_fecgrabacion");
        if (columna != null) {
            rureRutrecoleccion.setRureFecgrabacion(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get(alias + "_usu_ideregistro_ac");
        if (columna != null) {
            rureRutrecoleccion.setUsuIderegistroAc(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_rure_fecact");
        if (columna != null) {
            rureRutrecoleccion.setRureFecact(getObject(columna, Timestamp.class, rs));
        }
        columna = columnas.get(alias + "_rut_idemacruta");
        if (columna != null) {
            rureRutrecoleccion.setRutIdemacruta(getObject(columna, Integer.class, rs));
        }
        columna = columnas.get(alias + "_rut_microruta");
        if (columna != null) {
            rureRutrecoleccion.setRutMicroruta(getObject(columna, String.class, rs));
        }
    }

    public static RureRutrecoleccion getRureRutrecoleccion(ResultSet rs, Map<String, Integer> columnas) throws PersistenciaExcepcion {
        RureRutrecoleccion rureRutrecoleccion = new RureRutrecoleccion();
        getRureRutrecoleccion(rs, columnas, rureRutrecoleccion);
        return rureRutrecoleccion;
    }

    public static RureRutrecoleccion getRureRutrecoleccion(ResultSet rs, Map<String, Integer> columnas, String alias) throws PersistenciaExcepcion {
        RureRutrecoleccion rureRutrecoleccion = new RureRutrecoleccion();
        getRureRutrecoleccion(rs, columnas, rureRutrecoleccion, alias);
        return rureRutrecoleccion;
    }

}
