package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class VarprVarperregCRUD extends GenericoCRUD
{

  public VarprVarperregCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(VarprVarperreg varprVarperreg)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO aseo.varpr_varperreg(per_ideregistro,arpr_ideregistro,con_ideregistro,varpr_valor,varpr_estado,usu_ideregistro_gb,varpr_fecgrabacion,usu_ideregistro_cer,varpr_feccertificacion,emp_ideregistro,raco_ideregistro,varpr_estado_registro) VALUES (:per_ideregistro,:arpr_ideregistro,:con_ideregistro,:varpr_valor,:varpr_estado,:usu_ideregistro_gb,:varpr_fecgrabacion,:usu_ideregistro_cer,:varpr_feccertificacion,:emp_ideregistro,:raco_ideregistro,:varpr_estado_registro)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object perIderegistro = (varprVarperreg.getPerIderegistro() == null) ? null : varprVarperreg.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      Object arprIderegistro = (varprVarperreg.getArprIderegistro() == null) ? null : varprVarperreg.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      Object conIderegistro = (varprVarperreg.getConIderegistro() == null) ? null : varprVarperreg.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject("con_ideregistro", conIderegistro);
      sentencia.setObject("varpr_valor", varprVarperreg.getVarprValor());
      sentencia.setObject("varpr_estado", varprVarperreg.getVarprEstado());
      sentencia.setObject("usu_ideregistro_gb", varprVarperreg.getUsuIderegistroGb());
      sentencia.setObject("varpr_fecgrabacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFecgrabacion()));
      sentencia.setObject("usu_ideregistro_cer", varprVarperreg.getUsuIderegistroCer());
      sentencia.setObject("varpr_feccertificacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFeccertificacion()));
      sentencia.setObject("emp_ideregistro", varprVarperreg.getEmpIderegistro());
      Object racoIderegistro = (varprVarperreg.getRacoIderegistro() == null) ? null : varprVarperreg.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject("raco_ideregistro", racoIderegistro);
      sentencia.setObject("varpr_estado_registro", varprVarperreg.getVarprEstadoRegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        varprVarperreg.setVarprIderegistro(rs.getInt("varpr_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }
  
    public Integer insertarAprovechamiento(VarprVarperreg varprVarperreg)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    Integer id = -1;
    try {
      String sql = "INSERT INTO aseo.varpr_varperreg(per_ideregistro,arpr_ideregistro,con_ideregistro,varpr_valor,varpr_estado,usu_ideregistro_gb,varpr_fecgrabacion,usu_ideregistro_cer,varpr_feccertificacion,emp_ideregistro,raco_ideregistro,varpr_estado_registro) VALUES (:per_ideregistro,:arpr_ideregistro,:con_ideregistro,:varpr_valor,:varpr_estado,:usu_ideregistro_gb,:varpr_fecgrabacion,:usu_ideregistro_cer,:varpr_feccertificacion,:emp_ideregistro,:raco_ideregistro,:varpr_estado_registro)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object perIderegistro = (varprVarperreg.getPerIderegistro() == null) ? null : varprVarperreg.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      Object arprIderegistro = (varprVarperreg.getArprIderegistro() == null) ? null : varprVarperreg.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      Object conIderegistro = (varprVarperreg.getConIderegistro() == null) ? null : varprVarperreg.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject("con_ideregistro", conIderegistro);
      sentencia.setObject("varpr_valor", varprVarperreg.getVarprValor());
      sentencia.setObject("varpr_estado", varprVarperreg.getVarprEstado());
      sentencia.setObject("usu_ideregistro_gb", varprVarperreg.getUsuIderegistroGb());
      sentencia.setObject("varpr_fecgrabacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFecgrabacion()));
      sentencia.setObject("usu_ideregistro_cer", varprVarperreg.getUsuIderegistroCer());
      sentencia.setObject("varpr_feccertificacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFeccertificacion()));
      sentencia.setObject("emp_ideregistro", varprVarperreg.getEmpIderegistro());
      Object racoIderegistro = (varprVarperreg.getRacoIderegistro() == null) ? null : varprVarperreg.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject("raco_ideregistro", racoIderegistro);
      sentencia.setObject("varpr_estado_registro", varprVarperreg.getVarprEstadoRegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        varprVarperreg.setVarprIderegistro(rs.getInt("varpr_ideregistro"));
        id =  rs.getInt("varpr_ideregistro");
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
      return id;
    }
  }

  public void insertarTodos(VarprVarperreg varprVarperreg)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.varpr_varperreg(varpr_ideregistro,per_ideregistro,arpr_ideregistro,con_ideregistro,varpr_valor,varpr_estado,usu_ideregistro_gb,varpr_fecgrabacion,usu_ideregistro_cer,varpr_feccertificacion,emp_ideregistro,raco_ideregistro,varpr_estado_registro) VALUES (:varpr_ideregistro,:per_ideregistro,:arpr_ideregistro,:con_ideregistro,:varpr_valor,:varpr_estado,:usu_ideregistro_gb,:varpr_fecgrabacion,:usu_ideregistro_cer,:varpr_feccertificacion,:emp_ideregistro,:raco_ideregistro,:varpr_estado_registro)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("varpr_ideregistro", varprVarperreg.getVarprIderegistro());
      Object perIderegistro = (varprVarperreg.getPerIderegistro() == null) ? null : varprVarperreg.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      Object arprIderegistro = (varprVarperreg.getArprIderegistro() == null) ? null : varprVarperreg.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      Object conIderegistro = (varprVarperreg.getConIderegistro() == null) ? null : varprVarperreg.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject("con_ideregistro", conIderegistro);
      sentencia.setObject("varpr_valor", varprVarperreg.getVarprValor());
      sentencia.setObject("varpr_estado", varprVarperreg.getVarprEstado());
      sentencia.setObject("usu_ideregistro_gb", varprVarperreg.getUsuIderegistroGb());
      sentencia.setObject("varpr_fecgrabacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFecgrabacion()));
      sentencia.setObject("usu_ideregistro_cer", varprVarperreg.getUsuIderegistroCer());
      sentencia.setObject("varpr_feccertificacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFeccertificacion()));
      sentencia.setObject("emp_ideregistro", varprVarperreg.getEmpIderegistro());
      Object racoIderegistro = (varprVarperreg.getRacoIderegistro() == null) ? null : varprVarperreg.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject("raco_ideregistro", racoIderegistro);
      sentencia.setObject("varpr_estado_registro", varprVarperreg.getVarprEstadoRegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(VarprVarperreg varprVarperreg)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.varpr_varperreg SET per_ideregistro=:per_ideregistro,arpr_ideregistro=:arpr_ideregistro,con_ideregistro=:con_ideregistro,varpr_valor=:varpr_valor,varpr_estado=:varpr_estado,usu_ideregistro_gb=:usu_ideregistro_gb,varpr_fecgrabacion=:varpr_fecgrabacion,usu_ideregistro_cer=:usu_ideregistro_cer,varpr_feccertificacion=:varpr_feccertificacion,emp_ideregistro=:emp_ideregistro,raco_ideregistro=:raco_ideregistro,varpr_estado_registro=:varpr_estado_registro where varpr_ideregistro = :varpr_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object perIderegistro = (varprVarperreg.getPerIderegistro() == null) ? null : varprVarperreg.getPerIderegistro().getPerIderegistro();
      sentencia.setObject("per_ideregistro", perIderegistro);
      Object arprIderegistro = (varprVarperreg.getArprIderegistro() == null) ? null : varprVarperreg.getArprIderegistro().getArprIderegistro();
      sentencia.setObject("arpr_ideregistro", arprIderegistro);
      Object conIderegistro = (varprVarperreg.getConIderegistro() == null) ? null : varprVarperreg.getConIderegistro().getUniConcepto().getUniIderegistro();
      sentencia.setObject("con_ideregistro", conIderegistro);
      sentencia.setObject("varpr_valor", varprVarperreg.getVarprValor());
      sentencia.setObject("varpr_estado", varprVarperreg.getVarprEstado());
      sentencia.setObject("usu_ideregistro_gb", varprVarperreg.getUsuIderegistroGb());
      sentencia.setObject("varpr_fecgrabacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFecgrabacion()));
      sentencia.setObject("usu_ideregistro_cer", varprVarperreg.getUsuIderegistroCer());
      sentencia.setObject("varpr_feccertificacion", DateUtil.parseTimestamp(varprVarperreg.getVarprFeccertificacion()));
      sentencia.setObject("emp_ideregistro", varprVarperreg.getEmpIderegistro());
      Object racoIderegistro = (varprVarperreg.getRacoIderegistro() == null) ? null : varprVarperreg.getRacoIderegistro().getRacoIderegistr();
      sentencia.setObject("raco_ideregistro", racoIderegistro);
      sentencia.setObject("varpr_estado_registro", varprVarperreg.getVarprEstadoRegistro());
      sentencia.setObject("varpr_ideregistro", varprVarperreg.getVarprIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<VarprVarperreg> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<VarprVarperreg> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.varpr_varperreg";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getVarprVarperreg(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public VarprVarperreg consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    VarprVarperreg obj = null;
    try {

      String sql = "SELECT * FROM aseo.varpr_varperreg WHERE varpr_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getVarprVarperreg(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static VarprVarperreg getVarprVarperreg(ResultSet rs)
          throws PersistenciaExcepcion
  {
    VarprVarperreg varprVarperreg = new VarprVarperreg();
    varprVarperreg.setVarprIderegistro(getObject("varpr_ideregistro", Integer.class, rs));
    PerPeriodo per_ideregistro = new PerPeriodo();
    per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
    varprVarperreg.setPerIderegistro(per_ideregistro);
    ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
    arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    varprVarperreg.setArprIderegistro(arpr_ideregistro);
    ConConcepto con_ideregistro = new ConConcepto();
    con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
    varprVarperreg.setConIderegistro(con_ideregistro);
    varprVarperreg.setVarprValor(getObject("varpr_valor", Double.class, rs));
    varprVarperreg.setVarprEstado(getObject("varpr_estado", String.class, rs));
    varprVarperreg.setUsuIderegistroGb(getObject("usu_ideregistro_gb", Integer.class, rs));
    varprVarperreg.setVarprFecgrabacion(getObject("varpr_fecgrabacion", Timestamp.class, rs));
    varprVarperreg.setUsuIderegistroCer(getObject("usu_ideregistro_cer", Integer.class, rs));
    varprVarperreg.setVarprFeccertificacion(getObject("varpr_feccertificacion", Timestamp.class, rs));
    varprVarperreg.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
    RacoRanconcept raco_ideregistro = new RacoRanconcept();
    raco_ideregistro.setRacoIderegistr(getObject("raco_ideregistro", Integer.class, rs));
    varprVarperreg.setRacoIderegistro(raco_ideregistro);
    varprVarperreg.setVarprEstadoRegistro(getObject("varpr_estado_registro", String.class, rs));

    return varprVarperreg;
  }

  public static VarprVarperreg getVarprVarperreg(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    VarprVarperreg varprVarperreg = new VarprVarperreg();
    Integer columna = columnas.get(alias + "_varpr_ideregistro");
    if (columna != null) {
      varprVarperreg.setVarprIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_per_ideregistro");
    if (columna != null) {
      PerPeriodo per_ideregistro = new PerPeriodo();
      per_ideregistro.setPerIderegistro(getObject(columna, Integer.class, rs));
      varprVarperreg.setPerIderegistro(per_ideregistro);
    }
    columna = columnas.get(alias + "_arpr_ideregistro");
    if (columna != null) {
      ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
      arpr_ideregistro.setArprIderegistro(getObject(columna, Integer.class, rs));
      varprVarperreg.setArprIderegistro(arpr_ideregistro);
    }
    columna = columnas.get(alias + "_con_ideregistro");
    if (columna != null) {
      ConConcepto con_ideregistro = new ConConcepto();
      con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
      varprVarperreg.setConIderegistro(con_ideregistro);
    }
    columna = columnas.get(alias + "_varpr_valor");
    if (columna != null) {
      varprVarperreg.setVarprValor(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_varpr_estado");
    if (columna != null) {
      varprVarperreg.setVarprEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_gb");
    if (columna != null) {
      varprVarperreg.setUsuIderegistroGb(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_varpr_fecgrabacion");
    if (columna != null) {
      varprVarperreg.setVarprFecgrabacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro_cer");
    if (columna != null) {
      varprVarperreg.setUsuIderegistroCer(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_varpr_feccertificacion");
    if (columna != null) {
      varprVarperreg.setVarprFeccertificacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_emp_ideregistro");
    if (columna != null) {
      varprVarperreg.setEmpIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_raco_ideregistro");
    if (columna != null) {
      RacoRanconcept raco_ideregistro = new RacoRanconcept();
      raco_ideregistro.setRacoIderegistr(getObject(columna, Integer.class, rs));
      varprVarperreg.setRacoIderegistro(raco_ideregistro);
    }
    columna = columnas.get(alias + "_varpr_estado_registro");
    if (columna != null) {
      varprVarperreg.setVarprEstadoRegistro(getObject(columna, String.class, rs));
    }
    return varprVarperreg;
  }

}
