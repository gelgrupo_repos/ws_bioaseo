/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import javax.sql.DataSource;

/**
 *
 * @author JavierRangel
 */
public class VrtaTerceroPorcenTaCRUD  extends GenericoCRUD {

    public VrtaTerceroPorcenTaCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public void insertar(VrtaTerceroPorcenTa vrtaTerceroPorcenTa)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_tercero_porcentaje_ta (numero_actualizacion_tercero, periodo_recalculo, numero_actualizacion_ta, ter_ideregistro, fecha_registro, usuario_registro, valor_porcentaje) VALUES (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object numeroActualizacionTercero = (vrtaTerceroPorcenTa.getNumeroActualizacionTercero()== null) ? null : vrtaTerceroPorcenTa.getNumeroActualizacionTercero();
            sentencia.setObject(i++, numeroActualizacionTercero);
            Object periodoRecalculo = (vrtaTerceroPorcenTa.getPeriodoRecalculo() == null) ? null : vrtaTerceroPorcenTa.getPeriodoRecalculo();
            sentencia.setObject(i++, periodoRecalculo);
            Object numeroActualizacionTa = (vrtaTerceroPorcenTa.getNumeroActualizacionTa()== null) ? null : vrtaTerceroPorcenTa.getNumeroActualizacionTa();
            sentencia.setObject(i++, numeroActualizacionTa);   
            Object idTercero = (vrtaTerceroPorcenTa.getIdTercero()== null) ? null : vrtaTerceroPorcenTa.getIdTercero();
            sentencia.setObject(i++, idTercero);
            Object fechaRegistro = (vrtaTerceroPorcenTa.getFechaRegistro() == null) ? null : vrtaTerceroPorcenTa.getFechaRegistro();
            sentencia.setObject(i++, fechaRegistro);
            Object usuarioRegistro = (vrtaTerceroPorcenTa.getUsuarioRegistro() == null) ? null : vrtaTerceroPorcenTa.getUsuarioRegistro();
            sentencia.setObject(i++, usuarioRegistro);
            Object valorPorcentaje = (vrtaTerceroPorcenTa.getValorPorcentaje() == null) ? null : vrtaTerceroPorcenTa.getValorPorcentaje();
            sentencia.setObject(i++, valorPorcentaje);
            sentencia.executeUpdate();
            /*ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
              vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }
    
    public static VrtaTerceroPorcenTa getVrtaTerceroPorcenTa(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaTerceroPorcenTa vrtaTerceroPorcenTa = new VrtaTerceroPorcenTa();
        vrtaTerceroPorcenTa.setIdTerceroPorcentajeTa(getObject("vrta_tercero_porcent_ta_ideregistro", Integer.class, rs));
        vrtaTerceroPorcenTa.setNumeroActualizacionTercero(getObject("numero_actualizacion_tercero", Integer.class, rs));
        vrtaTerceroPorcenTa.setPeriodoRecalculo(getObject("periodo_recalculo", Integer.class, rs));
        vrtaTerceroPorcenTa.setNumeroActualizacionTa(getObject("numero_actualizacion_ta", Integer.class, rs));
        vrtaTerceroPorcenTa.setIdTercero(getObject("ter_ideregistro", Long.class, rs));
        vrtaTerceroPorcenTa.setFechaRegistro(getObject("fecha_registro", Timestamp.class, rs));
        vrtaTerceroPorcenTa.setUsuarioRegistro(getObject("usuario_registro", Integer.class, rs));
        vrtaTerceroPorcenTa.setValorPorcentaje(getObject("valor_porcentaje", Double.class, rs));
        return vrtaTerceroPorcenTa;
    }
}