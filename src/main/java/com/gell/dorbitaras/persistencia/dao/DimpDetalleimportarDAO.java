package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.DimpDetalleimportar;
import com.gell.dorbitaras.persistencia.entidades.ImpImportar;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.dto.AuditoriaDTO;

public class DimpDetalleimportarDAO extends DimpDetalleimportarCRUD {

  public DimpDetalleimportarDAO(DataSource datasource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
     super(datasource, auditoria);
  }
  
      /**
   * Expone el método de guardarImp y si se envía el identificador de DimpDetalleimportar se
   * edita
   *
   * @param detalleImportar Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarDetalle(DimpDetalleimportar detalleImportar)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Long Ideregistro = (detalleImportar.getDimpIderegistro());
    Ideregistro = Ideregistro == null ? -1 : Ideregistro;
    DimpDetalleimportar detalleGuardada = consultar(Ideregistro);
    if (detalleGuardada != null) {
      this.editar(detalleImportar);
      return;
    }
    super.insertar(detalleImportar);
  }
}
