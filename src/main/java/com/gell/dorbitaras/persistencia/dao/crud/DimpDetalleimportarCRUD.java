package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class DimpDetalleimportarCRUD extends GenericoCRUD {


    public DimpDetalleimportarCRUD(DataSource dataSource, AuditoriaDTO auditoria) throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    
    public void insertar(DimpDetalleimportar dimpDetalleimportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO aseo.dimp_detalleimportar(imp_ideregistro,dimp_tabladestino,dimp_descripcion,dimp_ordentransaccional,dimp_depedenciafuncional,dimp_campodependenciafuncional,dimp_descripcioncampo,dimp_orden,dimp_inicio,dimp_fin,dimp_valordefecto,dimp_campodestino,dimp_expresionregular,dimp_longitudmaxima,dimp_requerido) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            sentencia.setObject(i++,dimpDetalleimportar.getImpIderegistro()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpTabladestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcion()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrdentransaccional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDepedenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodependenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcioncampo()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrden()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpInicio()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpFin()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpValordefecto()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpExpresionregular()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpLongitudmaxima()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpRequerido()); 
 
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                dimpDetalleimportar.setDimpIderegistro(rs.getLong("dimp_ideregistro"));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void insertarTodos(DimpDetalleimportar dimpDetalleimportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "INSERT INTO aseo.dimp_detalleimportar(dimp_ideregistro,imp_ideregistro,dimp_tabladestino,dimp_descripcion,dimp_ordentransaccional,dimp_depedenciafuncional,dimp_campodependenciafuncional,dimp_descripcioncampo,dimp_orden,dimp_inicio,dimp_fin,dimp_valordefecto,dimp_campodestino,dimp_expresionregular,dimp_longitudmaxima,dimp_requerido) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,dimpDetalleimportar.getDimpIderegistro()); 
sentencia.setObject(i++,dimpDetalleimportar.getImpIderegistro()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpTabladestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcion()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrdentransaccional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDepedenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodependenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcioncampo()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrden()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpInicio()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpFin()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpValordefecto()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpExpresionregular()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpLongitudmaxima()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpRequerido()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
          LogUtil.error(e);
          throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    
    public void editar(DimpDetalleimportar dimpDetalleimportar) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i=1;
            String sql = "UPDATE aseo.dimp_detalleimportar SET imp_ideregistro=?,dimp_tabladestino=?,dimp_descripcion=?,dimp_ordentransaccional=?,dimp_depedenciafuncional=?,dimp_campodependenciafuncional=?,dimp_descripcioncampo=?,dimp_orden=?,dimp_inicio=?,dimp_fin=?,dimp_valordefecto=?,dimp_campodestino=?,dimp_expresionregular=?,dimp_longitudmaxima=?,dimp_requerido=? where dimp_ideregistro=? ";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setObject(i++,dimpDetalleimportar.getImpIderegistro()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpTabladestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcion()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrdentransaccional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDepedenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodependenciafuncional()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpDescripcioncampo()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpOrden()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpInicio()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpFin()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpValordefecto()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpCampodestino()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpExpresionregular()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpLongitudmaxima()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpRequerido()); 
sentencia.setObject(i++,dimpDetalleimportar.getDimpIderegistro()); 
 
            sentencia.executeUpdate();
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
             desconectar(sentencia);
        }
    }

    
    public List<DimpDetalleimportar> consultar() throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<DimpDetalleimportar> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.dimp_detalleimportar";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getDimpDetalleimportar(rs));
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
             desconectar(sentencia);
        }
        return lista;

    }

    
    public DimpDetalleimportar consultar(long id) throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        DimpDetalleimportar obj = null;
        try {

            String sql = "SELECT * FROM aseo.dimp_detalleimportar WHERE dimp_ideregistro=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getDimpDetalleimportar(rs);
            }
        }catch(SQLException e){
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
              desconectar(sentencia);
        }
        return obj;
    }

    public static DimpDetalleimportar getDimpDetalleimportar(ResultSet rs) throws PersistenciaExcepcion{
      DimpDetalleimportar dimpDetalleimportar = new DimpDetalleimportar();
dimpDetalleimportar.setDimpIderegistro( getObject("dimp_ideregistro",Long.class,rs));
dimpDetalleimportar.setImpIderegistro( getObject("imp_ideregistro",Long.class,rs));
dimpDetalleimportar.setDimpTabladestino( getObject("dimp_tabladestino",String.class,rs));
dimpDetalleimportar.setDimpDescripcion( getObject("dimp_descripcion",String.class,rs));
dimpDetalleimportar.setDimpOrdentransaccional( getObject("dimp_ordentransaccional",Integer.class,rs));
dimpDetalleimportar.setDimpDepedenciafuncional( getObject("dimp_depedenciafuncional",String.class,rs));
dimpDetalleimportar.setDimpCampodependenciafuncional( getObject("dimp_campodependenciafuncional",String.class,rs));
dimpDetalleimportar.setDimpDescripcioncampo( getObject("dimp_descripcioncampo",String.class,rs));
dimpDetalleimportar.setDimpOrden( getObject("dimp_orden",Integer.class,rs));
dimpDetalleimportar.setDimpInicio( getObject("dimp_inicio",Integer.class,rs));
dimpDetalleimportar.setDimpFin( getObject("dimp_fin",Integer.class,rs));
dimpDetalleimportar.setDimpValordefecto( getObject("dimp_valordefecto",Integer.class,rs));
dimpDetalleimportar.setDimpCampodestino( getObject("dimp_campodestino",String.class,rs));
dimpDetalleimportar.setDimpExpresionregular( getObject("dimp_expresionregular",String.class,rs));
dimpDetalleimportar.setDimpLongitudmaxima( getObject("dimp_longitudmaxima",Integer.class,rs));
dimpDetalleimportar.setDimpRequerido( getObject("dimp_requerido",Boolean.class,rs));

      return dimpDetalleimportar;
    }

    public static void getDimpDetalleimportar(ResultSet rs,Map<String, Integer> columnas,DimpDetalleimportar dimpDetalleimportar) throws PersistenciaExcepcion{
      Integer columna = columnas.get("dimp_detalleimportar_dimp_ideregistro");if( columna != null ){dimpDetalleimportar.setDimpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get("dimp_detalleimportar_imp_ideregistro");if( columna != null ){dimpDetalleimportar.setImpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_tabladestino");if( columna != null ){dimpDetalleimportar.setDimpTabladestino( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_descripcion");if( columna != null ){dimpDetalleimportar.setDimpDescripcion( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_ordentransaccional");if( columna != null ){dimpDetalleimportar.setDimpOrdentransaccional( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_depedenciafuncional");if( columna != null ){dimpDetalleimportar.setDimpDepedenciafuncional( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_campodependenciafuncional");if( columna != null ){dimpDetalleimportar.setDimpCampodependenciafuncional( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_descripcioncampo");if( columna != null ){dimpDetalleimportar.setDimpDescripcioncampo( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_orden");if( columna != null ){dimpDetalleimportar.setDimpOrden( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_inicio");if( columna != null ){dimpDetalleimportar.setDimpInicio( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_fin");if( columna != null ){dimpDetalleimportar.setDimpFin( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_valordefecto");if( columna != null ){dimpDetalleimportar.setDimpValordefecto( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_campodestino");if( columna != null ){dimpDetalleimportar.setDimpCampodestino( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_expresionregular");if( columna != null ){dimpDetalleimportar.setDimpExpresionregular( getObject(columna,String.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_longitudmaxima");if( columna != null ){dimpDetalleimportar.setDimpLongitudmaxima( getObject(columna,Integer.class,rs));
} columna = columnas.get("dimp_detalleimportar_dimp_requerido");if( columna != null ){dimpDetalleimportar.setDimpRequerido( getObject(columna,Boolean.class,rs));
}
    }
    
    public static void getDimpDetalleimportar(ResultSet rs,Map<String, Integer> columnas,DimpDetalleimportar dimpDetalleimportar,String alias) throws PersistenciaExcepcion{
      Integer columna = columnas.get(alias+"_dimp_ideregistro");if( columna != null ){dimpDetalleimportar.setDimpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get(alias+"_imp_ideregistro");if( columna != null ){dimpDetalleimportar.setImpIderegistro( getObject(columna,Long.class,rs));
} columna = columnas.get(alias+"_dimp_tabladestino");if( columna != null ){dimpDetalleimportar.setDimpTabladestino( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_descripcion");if( columna != null ){dimpDetalleimportar.setDimpDescripcion( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_ordentransaccional");if( columna != null ){dimpDetalleimportar.setDimpOrdentransaccional( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_depedenciafuncional");if( columna != null ){dimpDetalleimportar.setDimpDepedenciafuncional( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_campodependenciafuncional");if( columna != null ){dimpDetalleimportar.setDimpCampodependenciafuncional( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_descripcioncampo");if( columna != null ){dimpDetalleimportar.setDimpDescripcioncampo( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_orden");if( columna != null ){dimpDetalleimportar.setDimpOrden( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_inicio");if( columna != null ){dimpDetalleimportar.setDimpInicio( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_fin");if( columna != null ){dimpDetalleimportar.setDimpFin( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_valordefecto");if( columna != null ){dimpDetalleimportar.setDimpValordefecto( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_campodestino");if( columna != null ){dimpDetalleimportar.setDimpCampodestino( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_expresionregular");if( columna != null ){dimpDetalleimportar.setDimpExpresionregular( getObject(columna,String.class,rs));
} columna = columnas.get(alias+"_dimp_longitudmaxima");if( columna != null ){dimpDetalleimportar.setDimpLongitudmaxima( getObject(columna,Integer.class,rs));
} columna = columnas.get(alias+"_dimp_requerido");if( columna != null ){dimpDetalleimportar.setDimpRequerido( getObject(columna,Boolean.class,rs));
}
    }

    public static DimpDetalleimportar getDimpDetalleimportar(ResultSet rs,Map<String, Integer> columnas) throws PersistenciaExcepcion{
      DimpDetalleimportar dimpDetalleimportar = new  DimpDetalleimportar();
      getDimpDetalleimportar( rs, columnas,dimpDetalleimportar);
      return dimpDetalleimportar;
    }
   
 public static DimpDetalleimportar getDimpDetalleimportar(ResultSet rs,Map<String, Integer> columnas,String alias) throws PersistenciaExcepcion{
      DimpDetalleimportar dimpDetalleimportar = new  DimpDetalleimportar();
      getDimpDetalleimportar( rs,columnas, dimpDetalleimportar, alias);
      return dimpDetalleimportar;
    }
    
}
