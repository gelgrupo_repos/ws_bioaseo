/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaVarteraprPromCRUD extends GenericoCRUD {

    public VrtaVarteraprPromCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }
    
     public VrtaVarteraprPromCRUD(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

    public void insertar(VrtaVarteraprProm vrtaVarteraprProm)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        Integer id = -1;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_varterapr_prom(per_idepadre,numero_actualizacion,tipo,ter_ideregistro,periodo_exacto,ta_ejecutado,vrta_fecgrabacion,usu_ideregistro_gb, vrta_prom_valor) VALUES (?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object perIdepadre = (vrtaVarteraprProm.getPerIdePadre() == null) ? null : vrtaVarteraprProm.getPerIdePadre();
            sentencia.setObject(i++, perIdepadre);
            Object numeroActualizacion = (vrtaVarteraprProm.getNumeroActualizacion() == null) ? null : vrtaVarteraprProm.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object tipo = (vrtaVarteraprProm.getTipo()== null) ? null : vrtaVarteraprProm.getTipo();
            sentencia.setObject(i++, tipo);
            Object terIderegistr = (vrtaVarteraprProm.getTerIdeRegistro() == null) ? null : vrtaVarteraprProm.getTerIdeRegistro();
            sentencia.setObject(i++, terIderegistr);
            Object periodoexacto = (vrtaVarteraprProm.getPeriodoExacto() == null) ? null : vrtaVarteraprProm.getPeriodoExacto();
            sentencia.setObject(i++, periodoexacto);
            Object taejecutado = (vrtaVarteraprProm.getTaEjecutado() == null) ? null : vrtaVarteraprProm.getTaEjecutado();
            sentencia.setObject(i++, taejecutado);
            Object vrtafecgrabacion = (vrtaVarteraprProm.getVrtaFecGrabacion() == null) ? null : vrtaVarteraprProm.getVrtaFecGrabacion();
            sentencia.setObject(i++, vrtafecgrabacion);
            Object usuideregistro = (vrtaVarteraprProm.getUsuIdeRegistro() == null) ? null : vrtaVarteraprProm.getUsuIdeRegistro();
            sentencia.setObject(i++, usuideregistro);
            Object vrtapromvalor = (vrtaVarteraprProm.getVrtaPromValor() == null) ? null : vrtaVarteraprProm.getVrtaPromValor();
            sentencia.setObject(i++, vrtapromvalor);
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            /*if (rs.next()) {
              vrtaVarteraprProm.setVrtaPromIderegistro(rs.getInt("vrta_prom_ideregistro"));
              id =  rs.getInt("vrta_prom_ideregistro");
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
            // return id;
        }
    }

    

    public static VrtaVarteraprProm getVrtaVarteraprProm(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaVarteraprProm vrtaVarteraprProm = new VrtaVarteraprProm();
        vrtaVarteraprProm.setPerIdePadre(getObject("per_idepadre", Integer.class, rs));
        vrtaVarteraprProm.setNumeroActualizacion(getObject("numero_actualizacion", Integer.class, rs));
        vrtaVarteraprProm.setTerIdeRegistro(getObject("ter_ideregistro", Long.class, rs));
        vrtaVarteraprProm.setPeriodoExacto(getObject("periodo_exacto", Boolean.class, rs));
        vrtaVarteraprProm.setTaEjecutado(getObject("ta_ejecutado", Boolean.class, rs));
        vrtaVarteraprProm.setVrtaFecGrabacion(getObject("vrta_fecgrabacion", Timestamp.class, rs));
        vrtaVarteraprProm.setUsuIderegistro(getObject("usu_ideregistro_gb", Integer.class, rs));
        vrtaVarteraprProm.setVrtaPromValor(getObject("vrta_prom_valor", Double.class, rs));
        return vrtaVarteraprProm;
    }
    
 
}
