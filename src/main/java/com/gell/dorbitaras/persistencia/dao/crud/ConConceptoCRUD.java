package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.estandar.persistencia.entidades.PrgPrograma;
import java.sql.Connection;

public class ConConceptoCRUD extends GenericoCRUD
{
  
  public ConConceptoCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }
  public ConConceptoCRUD(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }
  
  public void insertar(ConConcepto conConcepto)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.con_concepto(est_concepto,con_nombre,con_alias,con_abreviatura,con_tipcalculo,con_valor,con_formula,con_operacion,con_naturaleza,con_preliquidar,con_anticipo,con_pagpriori,con_financiable,con_inivigencia,con_finvigencia,con_estado,prg_ideregistro,con_tipregistro,con_condonable,con_valnulo,usu_ideregistro,fun_ideregistro,con_suspende,con_intfinanciacion,con_metajuste,con_precision,con_contabiliza) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, conConcepto.getEstConcepto());
      sentencia.setObject(i++, conConcepto.getConNombre());
      sentencia.setObject(i++, conConcepto.getConAlias());
      sentencia.setObject(i++, conConcepto.getConAbreviatura());
      sentencia.setObject(i++, conConcepto.getConTipcalculo());
      sentencia.setObject(i++, conConcepto.getConValor());
      sentencia.setObject(i++, conConcepto.getConFormula());
      sentencia.setObject(i++, conConcepto.getConOperacion());
      sentencia.setObject(i++, conConcepto.getConNaturaleza());
      sentencia.setObject(i++, conConcepto.getConPreliquidar());
      sentencia.setObject(i++, conConcepto.getConAnticipo());
      sentencia.setObject(i++, conConcepto.getConPagpriori());
      sentencia.setObject(i++, conConcepto.getConFinanciable());
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConInivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConFinvigencia()));
      sentencia.setObject(i++, conConcepto.getConEstado());
      Object prgIderegistro = (conConcepto.getPrgIderegistro() == null) ? null : conConcepto.getPrgIderegistro().getPrgIderegistro();
      sentencia.setObject(i++, prgIderegistro);
      sentencia.setObject(i++, conConcepto.getConTipregistro());
      sentencia.setObject(i++, conConcepto.getConCondonable());
      sentencia.setObject(i++, conConcepto.getConValnulo());
      sentencia.setObject(i++, conConcepto.getUsuIderegistro());
      sentencia.setObject(i++, conConcepto.getFunIderegistro());
      sentencia.setObject(i++, conConcepto.getConSuspende());
      sentencia.setObject(i++, conConcepto.getConIntfinanciacion());
      sentencia.setObject(i++, conConcepto.getConMetajuste());
      sentencia.setObject(i++, conConcepto.getConPrecision());
      sentencia.setObject(i++, conConcepto.getConContabiliza());
      
      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        conConcepto.setUniConcepto(new UniUnidad(rs.getInt("uni_concepto")));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }
  
  public void insertarTodos(ConConcepto conConcepto)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.con_concepto(uni_concepto,est_concepto,con_nombre,con_alias,con_abreviatura,con_tipcalculo,con_valor,con_formula,con_operacion,con_naturaleza,con_preliquidar,con_anticipo,con_pagpriori,con_financiable,con_inivigencia,con_finvigencia,con_estado,prg_ideregistro,con_tipregistro,con_condonable,con_valnulo,usu_ideregistro,fun_ideregistro,con_suspende,con_intfinanciacion,con_metajuste,con_precision,con_contabiliza) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, conConcepto.getUniConcepto().getUniIderegistro());
      sentencia.setObject(i++, conConcepto.getEstConcepto());
      sentencia.setObject(i++, conConcepto.getConNombre());
      sentencia.setObject(i++, conConcepto.getConAlias());
      sentencia.setObject(i++, conConcepto.getConAbreviatura());
      sentencia.setObject(i++, conConcepto.getConTipcalculo());
      sentencia.setObject(i++, conConcepto.getConValor());
      sentencia.setObject(i++, conConcepto.getConFormula());
      sentencia.setObject(i++, conConcepto.getConOperacion());
      sentencia.setObject(i++, conConcepto.getConNaturaleza());
      sentencia.setObject(i++, conConcepto.getConPreliquidar());
      sentencia.setObject(i++, conConcepto.getConAnticipo());
      sentencia.setObject(i++, conConcepto.getConPagpriori());
      sentencia.setObject(i++, conConcepto.getConFinanciable());
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConInivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConFinvigencia()));
      sentencia.setObject(i++, conConcepto.getConEstado());
      Object prgIderegistro = (conConcepto.getPrgIderegistro() == null) ? null : conConcepto.getPrgIderegistro().getPrgIderegistro();
      sentencia.setObject(i++, prgIderegistro);
      sentencia.setObject(i++, conConcepto.getConTipregistro());
      sentencia.setObject(i++, conConcepto.getConCondonable());
      sentencia.setObject(i++, conConcepto.getConValnulo());
      sentencia.setObject(i++, conConcepto.getUsuIderegistro());
      sentencia.setObject(i++, conConcepto.getFunIderegistro());
      sentencia.setObject(i++, conConcepto.getConSuspende());
      sentencia.setObject(i++, conConcepto.getConIntfinanciacion());
      sentencia.setObject(i++, conConcepto.getConMetajuste());
      sentencia.setObject(i++, conConcepto.getConPrecision());
      sentencia.setObject(i++, conConcepto.getConContabiliza());
      
      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }
  
  public void editar(ConConcepto conConcepto)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.con_concepto SET est_concepto=?,con_nombre=?,con_alias=?,con_abreviatura=?,con_tipcalculo=?,con_valor=?,con_formula=?,con_operacion=?,con_naturaleza=?,con_preliquidar=?,con_anticipo=?,con_pagpriori=?,con_financiable=?,con_inivigencia=?,con_finvigencia=?,con_estado=?,prg_ideregistro=?,con_tipregistro=?,con_condonable=?,con_valnulo=?,usu_ideregistro=?,fun_ideregistro=?,con_suspende=?,con_intfinanciacion=?,con_metajuste=?,con_precision=?,con_contabiliza=? where uni_concepto=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, conConcepto.getEstConcepto());
      sentencia.setObject(i++, conConcepto.getConNombre());
      sentencia.setObject(i++, conConcepto.getConAlias());
      sentencia.setObject(i++, conConcepto.getConAbreviatura());
      sentencia.setObject(i++, conConcepto.getConTipcalculo());
      sentencia.setObject(i++, conConcepto.getConValor());
      sentencia.setObject(i++, conConcepto.getConFormula());
      sentencia.setObject(i++, conConcepto.getConOperacion());
      sentencia.setObject(i++, conConcepto.getConNaturaleza());
      sentencia.setObject(i++, conConcepto.getConPreliquidar());
      sentencia.setObject(i++, conConcepto.getConAnticipo());
      sentencia.setObject(i++, conConcepto.getConPagpriori());
      sentencia.setObject(i++, conConcepto.getConFinanciable());
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConInivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(conConcepto.getConFinvigencia()));
      sentencia.setObject(i++, conConcepto.getConEstado());
      Object prgIderegistro = (conConcepto.getPrgIderegistro() == null) ? null : conConcepto.getPrgIderegistro().getPrgIderegistro();
      sentencia.setObject(i++, prgIderegistro);
      sentencia.setObject(i++, conConcepto.getConTipregistro());
      sentencia.setObject(i++, conConcepto.getConCondonable());
      sentencia.setObject(i++, conConcepto.getConValnulo());
      sentencia.setObject(i++, conConcepto.getUsuIderegistro());
      sentencia.setObject(i++, conConcepto.getFunIderegistro());
      sentencia.setObject(i++, conConcepto.getConSuspende());
      sentencia.setObject(i++, conConcepto.getConIntfinanciacion());
      sentencia.setObject(i++, conConcepto.getConMetajuste());
      sentencia.setObject(i++, conConcepto.getConPrecision());
      sentencia.setObject(i++, conConcepto.getConContabiliza());
      sentencia.setObject(i++, conConcepto.getUniConcepto().getUniIderegistro());
      
      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }
  
  public List<ConConcepto> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<ConConcepto> lista = new ArrayList<>();
    try {
      
      String sql = "SELECT * FROM public.con_concepto";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getConConcepto(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;
    
  }
  
  public ConConcepto consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    ConConcepto obj = null;
    try {
      
      String sql = "SELECT * FROM public.con_concepto WHERE uni_concepto=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getConConcepto(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }
  
  public static ConConcepto getConConcepto(ResultSet rs)
          throws PersistenciaExcepcion
  {
    ConConcepto conConcepto = new ConConcepto();
    conConcepto.setUniConcepto(new UniUnidad(getObject("uni_concepto", Integer.class, rs)));
    conConcepto.setEstConcepto(getObject("est_concepto", Integer.class, rs));
    conConcepto.setConNombre(getObject("con_nombre", String.class, rs));
    conConcepto.setConAlias(getObject("con_alias", String.class, rs));
    conConcepto.setConAbreviatura(getObject("con_abreviatura", String.class, rs));
    conConcepto.setConTipcalculo(getObject("con_tipcalculo", String.class, rs));
    conConcepto.setConValor(getObject("con_valor", Double.class, rs));
    conConcepto.setConFormula(getObject("con_formula", String.class, rs));
    conConcepto.setConOperacion(getObject("con_operacion", String.class, rs));
    conConcepto.setConNaturaleza(getObject("con_naturaleza", String.class, rs));
    conConcepto.setConPreliquidar(getObject("con_preliquidar", String.class, rs));
    conConcepto.setConAnticipo(getObject("con_anticipo", String.class, rs));
    conConcepto.setConPagpriori(getObject("con_pagpriori", Integer.class, rs));
    conConcepto.setConFinanciable(getObject("con_financiable", String.class, rs));
    conConcepto.setConInivigencia(getObject("con_inivigencia", Timestamp.class, rs));
    conConcepto.setConFinvigencia(getObject("con_finvigencia", Timestamp.class, rs));
    conConcepto.setConEstado(getObject("con_estado", String.class, rs));
    PrgPrograma prg_ideregistro = new PrgPrograma();
    prg_ideregistro.setPrgIderegistro(getObject("prg_ideregistro", Integer.class, rs));
    conConcepto.setPrgIderegistro(prg_ideregistro);
    conConcepto.setConTipregistro(getObject("con_tipregistro", String.class, rs));
    conConcepto.setConCondonable(getObject("con_condonable", String.class, rs));
    conConcepto.setConValnulo(getObject("con_valnulo", String.class, rs));
    conConcepto.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    conConcepto.setFunIderegistro(getObject("fun_ideregistro", Integer.class, rs));
    conConcepto.setConSuspende(getObject("con_suspende", String.class, rs));
    conConcepto.setConIntfinanciacion(getObject("con_intfinanciacion", String.class, rs));
    conConcepto.setConMetajuste(getObject("con_metajuste", String.class, rs));
    conConcepto.setConPrecision(getObject("con_precision", Integer.class, rs));
    conConcepto.setConContabiliza(getObject("con_contabiliza", String.class, rs));
    
    return conConcepto;
  }
  
  public static ConConcepto getConConcepto(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ConConcepto conConcepto = new ConConcepto();
    Integer columna = columnas.get(alias + "_uni_concepto");
    if (columna != null) {
      conConcepto.setUniConcepto(new UniUnidad(getObject(columna, Integer.class, rs)));
    }
    columna = columnas.get(alias + "_est_concepto");
    if (columna != null) {
      conConcepto.setEstConcepto(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_nombre");
    if (columna != null) {
      conConcepto.setConNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_alias");
    if (columna != null) {
      conConcepto.setConAlias(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_abreviatura");
    if (columna != null) {
      conConcepto.setConAbreviatura(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_tipcalculo");
    if (columna != null) {
      conConcepto.setConTipcalculo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_valor");
    if (columna != null) {
      conConcepto.setConValor(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_con_formula");
    if (columna != null) {
      conConcepto.setConFormula(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_operacion");
    if (columna != null) {
      conConcepto.setConOperacion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_naturaleza");
    if (columna != null) {
      conConcepto.setConNaturaleza(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_preliquidar");
    if (columna != null) {
      conConcepto.setConPreliquidar(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_anticipo");
    if (columna != null) {
      conConcepto.setConAnticipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_pagpriori");
    if (columna != null) {
      conConcepto.setConPagpriori(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_financiable");
    if (columna != null) {
      conConcepto.setConFinanciable(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_inivigencia");
    if (columna != null) {
      conConcepto.setConInivigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_con_finvigencia");
    if (columna != null) {
      conConcepto.setConFinvigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_con_estado");
    if (columna != null) {
      conConcepto.setConEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_prg_ideregistro");
    if (columna != null) {
      PrgPrograma prg_ideregistro = new PrgPrograma();
      prg_ideregistro.setPrgIderegistro(getObject(columna, Integer.class, rs));
      conConcepto.setPrgIderegistro(prg_ideregistro);
    }
    columna = columnas.get(alias + "_con_tipregistro");
    if (columna != null) {
      conConcepto.setConTipregistro(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_condonable");
    if (columna != null) {
      conConcepto.setConCondonable(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_valnulo");
    if (columna != null) {
      conConcepto.setConValnulo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      conConcepto.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_fun_ideregistro");
    if (columna != null) {
      conConcepto.setFunIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_suspende");
    if (columna != null) {
      conConcepto.setConSuspende(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_intfinanciacion");
    if (columna != null) {
      conConcepto.setConIntfinanciacion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_metajuste");
    if (columna != null) {
      conConcepto.setConMetajuste(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_precision");
    if (columna != null) {
      conConcepto.setConPrecision(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_contabiliza");
    if (columna != null) {
      conConcepto.setConContabiliza(getObject(columna, String.class, rs));
    }
    return conConcepto;
  }
  
    public static ConConceptoGestionVariables getConConceptoGestionVariables(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ConConceptoGestionVariables conConcepto = new ConConceptoGestionVariables();
    Integer columna = columnas.get(alias + "_uni_concepto");
    if (columna != null) {
      conConcepto.setUniConcepto(new UniUnidad(getObject(columna, Integer.class, rs)));
    }
    columna = columnas.get(alias + "_est_concepto");
    if (columna != null) {
      conConcepto.setEstConcepto(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_nombre");
    if (columna != null) {
      conConcepto.setConNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_alias");
    if (columna != null) {
      conConcepto.setConAlias(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_abreviatura");
    if (columna != null) {
      conConcepto.setConAbreviatura(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_tipcalculo");
    if (columna != null) {
      conConcepto.setConTipcalculo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_valor");
    if (columna != null) {
      conConcepto.setConValor(getObject(columna, Double.class, rs));
    }
    columna = columnas.get(alias + "_con_formula");
    if (columna != null) {
      conConcepto.setConFormula(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_operacion");
    if (columna != null) {
      conConcepto.setConOperacion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_naturaleza");
    if (columna != null) {
      conConcepto.setConNaturaleza(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_preliquidar");
    if (columna != null) {
      conConcepto.setConPreliquidar(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_anticipo");
    if (columna != null) {
      conConcepto.setConAnticipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_pagpriori");
    if (columna != null) {
      conConcepto.setConPagpriori(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_financiable");
    if (columna != null) {
      conConcepto.setConFinanciable(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_inivigencia");
    if (columna != null) {
      conConcepto.setConInivigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_con_finvigencia");
    if (columna != null) {
      conConcepto.setConFinvigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_con_estado");
    if (columna != null) {
      conConcepto.setConEstado(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_prg_ideregistro");
    if (columna != null) {
      PrgPrograma prg_ideregistro = new PrgPrograma();
      prg_ideregistro.setPrgIderegistro(getObject(columna, Integer.class, rs));
      conConcepto.setPrgIderegistro(prg_ideregistro);
    }
    columna = columnas.get(alias + "_con_tipregistro");
    if (columna != null) {
      conConcepto.setConTipregistro(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_condonable");
    if (columna != null) {
      conConcepto.setConCondonable(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_valnulo");
    if (columna != null) {
      conConcepto.setConValnulo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      conConcepto.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_fun_ideregistro");
    if (columna != null) {
      conConcepto.setFunIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_suspende");
    if (columna != null) {
      conConcepto.setConSuspende(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_intfinanciacion");
    if (columna != null) {
      conConcepto.setConIntfinanciacion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_metajuste");
    if (columna != null) {
      conConcepto.setConMetajuste(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_con_precision");
    if (columna != null) {
      conConcepto.setConPrecision(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_con_contabiliza");
    if (columna != null) {
      conConcepto.setConContabiliza(getObject(columna, String.class, rs));
    }
    return conConcepto;
  }
}
