package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class ClaClaseCRUD extends GenericoCRUD {

  public ClaClaseCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(ClaClase claClase)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.cla_clase(cla_nombre,cla_tipo,usu_ideregistro) VALUES (?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, claClase.getClaNombre());
      sentencia.setObject(i++, claClase.getClaTipo());
      sentencia.setObject(i++, claClase.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        claClase.setClaIderegistro(rs.getInt("cla_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(ClaClase claClase)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO public.cla_clase(cla_ideregistro,cla_nombre,cla_tipo,usu_ideregistro) VALUES (?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, claClase.getClaIderegistro());
      sentencia.setObject(i++, claClase.getClaNombre());
      sentencia.setObject(i++, claClase.getClaTipo());
      sentencia.setObject(i++, claClase.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(ClaClase claClase)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE public.cla_clase SET cla_nombre=?,cla_tipo=?,usu_ideregistro=? where cla_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, claClase.getClaNombre());
      sentencia.setObject(i++, claClase.getClaTipo());
      sentencia.setObject(i++, claClase.getUsuIderegistro());
      sentencia.setObject(i++, claClase.getClaIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<ClaClase> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<ClaClase> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM public.cla_clase";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getClaClase(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public ClaClase consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    ClaClase obj = null;
    try {

      String sql = "SELECT * FROM public.cla_clase WHERE cla_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getClaClase(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static ClaClase getClaClase(ResultSet rs)
          throws PersistenciaExcepcion
  {
    ClaClase claClase = new ClaClase();
    claClase.setClaIderegistro(getObject("cla_ideregistro", Integer.class, rs));
    claClase.setClaNombre(getObject("cla_nombre", String.class, rs));
    claClase.setClaTipo(getObject("cla_tipo", String.class, rs));
    claClase.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return claClase;
  }

  public static void getClaClase(ResultSet rs, Map<String, Integer> columnas, ClaClase claClase)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("cla_clase_cla_ideregistro");
    if (columna != null) {
      claClase.setClaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("cla_clase_cla_nombre");
    if (columna != null) {
      claClase.setClaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get("cla_clase_cla_tipo");
    if (columna != null) {
      claClase.setClaTipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("cla_clase_usu_ideregistro");
    if (columna != null) {
      claClase.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static void getClaClase(ResultSet rs, Map<String, Integer> columnas, ClaClase claClase, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_cla_ideregistro");
    if (columna != null) {
      claClase.setClaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_cla_nombre");
    if (columna != null) {
      claClase.setClaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_cla_tipo");
    if (columna != null) {
      claClase.setClaTipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      claClase.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static ClaClase getClaClase(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    ClaClase claClase = new ClaClase();
    getClaClase(rs, columnas, claClase);
    return claClase;
  }

  public static ClaClase getClaClase(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    ClaClase claClase = new ClaClase();
    getClaClase(rs, columnas, claClase, alias);
    return claClase;
  }

}
