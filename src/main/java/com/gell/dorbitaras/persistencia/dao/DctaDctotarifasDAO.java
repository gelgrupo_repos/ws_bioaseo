package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.DctaDctotarifas;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;

public class DctaDctotarifasDAO extends DctaDctotarifasCRUD {

  public DctaDctotarifasDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   *DAO (Data Access Objetc) DTO (Data Transfer Object)
   * Método para la inserción y edición de datos
   *
   * @param dctaDctotarifas Informacíon de regimen tarifario
   * @throws PersistenciaExcepcion
   */
  @Override
  public void insertar(DctaDctotarifas dctaDctotarifas)
          throws PersistenciaExcepcion
  {
    Integer ideRegistro = dctaDctotarifas.getDctaIderegistro();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    DctaDctotarifas documentoGuardada = consultar(ideRegistro.longValue());
    if (documentoGuardada != null) {
      this.editar(dctaDctotarifas);
      return;
    }
    dctaDctotarifas.setDctaEstado(EEstadoGenerico.ACTIVO);
    super.insertar(dctaDctotarifas);
  }

}
