package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EParametro;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.dto.SmperSemperiodosDTO;
import com.gell.dorbitaras.persistencia.dto.VarcBaseRepuestaDTO;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.SmperSemperiodo;
import com.gell.dorbitaras.persistencia.entidades.SmperSemperiodoMeses;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public class SmperSemperiodoDAO extends SmperSemperiodoCRUD
{

  public SmperSemperiodoDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Registra una variable en la base de datos.
   *
   * @param periodos Datos de el periodo a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public void insertar(SmperSemperiodo periodos)
          throws PersistenciaExcepcion
  {
    if (periodos.getSmperIderegistro() != null) {
      SmperSemperiodo smperPeriodo = consultar(periodos.getSmperIderegistro());
      periodos.setSmperFecgrabacion(smperPeriodo.getSmperFecgrabacion());
      periodos.setUsuIderegistroGb(smperPeriodo.getUsuIderegistroGb());
      editar(periodos);
      return;
    }
    super.insertar(periodos);
  }

  /**
   * Consulta que trae el ide sin el usuario no busca primero
   *
   * @param regimen Regimen tarifario
   * @param semestre Semestre
   * @param anio Año
   * @return El número de veces que se encuentra registrado el códgio en la base
   * de datos
   * @throws PersistenciaExcepcion
   */
  public Integer consultarSinBuscar(Integer regimen, Integer semestre, Integer anio)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT smper_ideregistro id FROM aseo.smper_semperiodos  ")
            .append("    WHERE rgta_ideregistro = :regimen  ")
            .append("      AND per_padre = :semestre  ")
            .append("        AND per_anio = :anio ")
            .append("          AND swt_act = :estado  ");
    parametros.put("regimen", regimen);
    parametros.put("semestre", semestre);
    parametros.put("anio", anio);
    parametros.put("estado", EEstadoGenerico.ACTIVO);

    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getObject("id", Integer.class, rs));
  }

  /**
   * Consulta todas las área de prestación dependiendo el parametro de busqueda
   *
   * @param regimen Identificador del régimen tarifario.
   * @param semestre Identificador del periodo semestral.
   * @param anio Identificador del año del periodo semestral.
   * @return Lista de los periodos dependiendo los parametros
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<SmperSemperiodosDTO> consultar(
          Integer regimen,
          Integer semestre,
          Integer anio
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT *, ")
            .append("       (SELECT ARRAY_TO_JSON(ARRAY_AGG(periodos.*)) ")
            .append("        FROM (SELECT varr.per_ideregistro, ")
            .append("                     varr.smper_descripcion, ")
            .append("                     per.per_nombre, ")
            .append("                     varr.smper_numero, ")
            .append("                     varr.smper_swtact, ")
            .append("                     comprobarEditarVarc.varc_ideregistro, ")
            .append("                     comprobarEditarVarpr.varpr_ideregistro, ")
            .append("                     comprobarEditarVrta.vrta_ideregistro, ")
            .append("                     varr.smper_ideregistro ")
            .append("              FROM aseo.smper_semperiodo varr ")
            .append("                       INNER JOIN per_periodo per ")
            .append("                                  ON varr.per_ideregistro = per.per_ideregistro ")
            .append("                       INNER JOIN aseo.rgta_rgitarifario rg")
            .append("                                  ON varr.rgta_ideregistro = rg.rgta_ideregistro ")
            .append("                       LEFT JOIN LATERAL (SELECT * ")
            .append("                                          FROM aseo.varc_varcalculo vaca ")
            .append("                                          WHERE vaca.per_ideregistro = varr.smper_ideregistro ")
            .append("                                            AND vaca.varc_estado = :activo ")
            .append("                                          LIMIT 1) AS comprobarEditarVarc ")
            .append("                                 ON comprobarEditarVarc.per_ideregistro = varr.smper_ideregistro ")
            .append("                       LEFT JOIN LATERAL (SELECT * ")
            .append("                                          FROM aseo.varpr_varperreg varp ")
            .append("                                          WHERE varp.per_ideregistro = varr.smper_ideregistro ")
            .append("                                            AND varp.varpr_estado = :activo ")
            .append("                                          LIMIT 1) AS comprobarEditarVarpr ")
            .append("                                 ON comprobarEditarVarpr.per_ideregistro = varr.smper_ideregistro ")
            .append("                       LEFT JOIN LATERAL (SELECT * ")
            .append("                                          FROM aseo.vrta_varterapr vrta ")
            .append("                                          WHERE vrta.per_ideregistro = varr.smper_ideregistro ")
            .append("                                            AND vrta.vrta_estado = :activo ")
            .append("                                          LIMIT 1) AS comprobarEditarVrta ")
            .append("                                 ON comprobarEditarVrta.per_ideregistro = varr.smper_ideregistro ")
            .append("              WHERE varr.smper_swtact = :activo ")
            .append("                AND varr.rgta_ideregistro = :regimen ")
            .append("                AND varr.per_idepadre = :semestre ")
            .append("              ORDER BY varr.smper_ideregistro) periodos) lista_periodos ")
            .append("FROM (SELECT DISTINCT varc.per_idepadre     idSemestre, ")
            .append("                      varc.rgta_ideregistro regimen ")
            .append("      FROM aseo.smper_semperiodo varc ")
            .append("               INNER JOIN aseo.rgta_rgitarifario ")
            .append("                          ON varc.rgta_ideregistro = rgta_rgitarifario.rgta_ideregistro ")
            .append("               INNER JOIN per_periodo pp ")
            .append("                          ON varc.per_idepadre = pp.per_ideregistro")
            .append("      WHERE smper_swtact = :activo ")
            .append("        AND varc.rgta_ideregistro = :regimen ")
            .append("        AND varc.per_idepadre = :semestre ")
            .append("        AND (to_char(pp.per_fecinicial, 'yyyy')) = :anio::CHARACTER VARYING ")
            .append("     ) periodos;");
    parametros.put("regimen", regimen);
    parametros.put("semestre", semestre);
    parametros.put("anio", anio);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<SmperSemperiodosDTO>()
    {
      @Override
      public SmperSemperiodosDTO siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        SmperSemperiodosDTO periodos = new SmperSemperiodosDTO();
        Integer idSemestre = getObject("idSemestre", Integer.class, rs);
        Integer idRegimen = getObject("regimen", Integer.class, rs);
        String lista = getObject("lista_periodos", String.class, rs);
        periodos.setInfo(lista);
        periodos.setIdRegimen(idRegimen);
        periodos.setIdSemestre(idSemestre);
        return periodos;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Consulta los periodos semestrales por área de prestación.
   *
   * @param idArea Identificador del área de prestación.
   * @return Lista de periodos semestrales.
   * @throws PersistenciaExcepcion
   */
  public List<SmperSemperiodo> consultarPeriodosSemestralesArea(
          Integer idArea
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT smper.*, perm.* ")
            .append(" FROM aseo.smper_semperiodo smper ")
            .append("       INNER JOIN public.per_periodo per ")
            .append("                  ON smper.per_idepadre = per.per_ideregistro ")
            .append("       INNER JOIN public.per_periodo perm ")
            .append("                  ON smper.per_ideregistro = perm.per_ideregistro ")
            .append("       INNER JOIN aseo.rgta_rgitarifario rgta ")
            .append("                  ON smper.rgta_ideregistro = rgta.rgta_ideregistro ")
            .append("       INNER JOIN aseo.arpr_areaprestacion arpr ")
            .append("                  ON rgta.rgta_ideregistro = arpr.rgta_ideregistro ")
            .append(" WHERE per.per_estado = :estadoActivo ")
            .append("   AND rgta.emp_ideregistro = :idEmpresa ")
            .append("   AND arpr.arpr_ideregistro = :idArea ")
            .append(" ORDER BY smper.smper_numero");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    //parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + " %");
    parametros.put("idArea", idArea);
    parametros.put("estadoActivo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      SmperSemperiodo periodoSemestral = getSmperSemperiodo(rs, columns, "smper_semperiodo1");
      PerPeriodo periodo = PerPeriodoDAO.getPerPeriodo(rs, columns, "per_periodo1");
      periodo.setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      return periodoSemestral.setPerIderegistro(periodo);
    });
  }

  /**
   * Método encargado de consultar los periodos semestrales por área de
   * prestación
   *
   * @param idArea Identificador del área de prestación
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<SmperSemperiodo> consultarSemestresArea(
          Integer idArea
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT smper.per_idepadre, ")
            .append("               smper.rgta_ideregistro, ")
            .append("               per.* ")
            .append("FROM aseo.smper_semperiodo smper ")
            .append("         INNER JOIN aseo.rgta_rgitarifario rgta ON smper.rgta_ideregistro = rgta.rgta_ideregistro ")
            .append("         INNER JOIN esem_estempresa ee ON ee.emp_ideregistro = rgta.emp_ideregistro ")
            .append("         INNER JOIN aseo.arpr_areaprestacion arpr ON rgta.rgta_ideregistro = arpr.rgta_ideregistro ")
            .append("         INNER JOIN per_periodo per on smper.per_idepadre = per.per_ideregistro ")
            .append("WHERE rgta.emp_ideregistro = :idEmpresa ")
//            .append("  AND arpr.arpr_ideregistro = :idArea ")
//            .append("  AND smper.smper_swtact = :estadoActivo ")
            .append("order by per.per_fecinicial,per.per_ideorden ");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idArea", idArea);
    parametros.put("estadoActivo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      SmperSemperiodo periodoSemestral = getSmperSemperiodo(rs, columns, "smper_semperiodo1");
      PerPeriodo periodo = PerPeriodoDAO.getPerPeriodo(rs, columns, "per_periodo1");
      periodo.setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      return periodoSemestral.setPerIdepadre(periodo);
    });
  }
  
  

  /**
   * Método encargado de consultar los periodos semestrales por régimen
   * tarifario
   *
   * @param idRegimen Identificador del régimen tarifario
   * @return Lista de periodos semestrales
   * @throws PersistenciaExcepcion
   */
  public List<SmperSemperiodo> consultarSemestresRegimen(
          Integer idRegimen
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT smper.per_idepadre, ")
            .append("               smper.rgta_ideregistro, ")
            .append("               per.* ")
            .append("FROM aseo.smper_semperiodo smper ")
            .append("        INNER JOIN aseo.rgta_rgitarifario rgta ON smper.rgta_ideregistro = rgta.rgta_ideregistro ")
            .append("        INNER JOIN esem_estempresa ee ON ee.emp_ideregistro = rgta.emp_ideregistro ")
            .append("        INNER JOIN aseo.arpr_areaprestacion arpr ON rgta.rgta_ideregistro = arpr.rgta_ideregistro ")
            .append("        INNER JOIN per_periodo per on smper.per_idepadre = per.per_ideregistro ")
            .append("WHERE rgta.emp_ideregistro = :idEmpresa ")
            .append(" AND rgta.rgta_ideregistro = :idRegimen ")
            .append(" AND smper.smper_swtact = :estadoActivo ")
            .append(" AND per.per_estado = :estadoActivo;");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idRegimen", idRegimen);
    parametros.put("estadoActivo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      SmperSemperiodo periodoSemestral = getSmperSemperiodo(rs, columns, "smper_semperiodo1");
      PerPeriodo periodo = PerPeriodoDAO.getPerPeriodo(rs, columns, "per_periodo1");
      periodo.setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      return periodoSemestral.setPerIdepadre(periodo);
    });
  }

  /**
   * Método encargado de inactivar el un periodo.
   *
   * @param idPeriodo Identificador del periodo semestral.
   * @throws PersistenciaExcepcion
   */
  public void inactivarPeriodoSemestral(Integer idPeriodo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.smper_semperiodo ")
            .append("SET smper_fecact        = :fecha, ")
            .append("    usu_ideregistro_act = :usuario, ")
            .append("    smper_swtact        = :cerrado ")
            .append("WHERE per_idepadre = :periodo ")
            .append("  AND smper_swtact = :activo;");
    parametros.put("fecha", LocalDate.now());
    parametros.put("usuario", auditoria.getIdUsuario());
    parametros.put("cerrado", EEstadoGenerico.CERRADO);
    parametros.put("periodo", idPeriodo);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    ejecutarEdicion(sql, parametros);
  }

  /**
   * Método encargado de inactivar el un periodo.
   *
   * @param periodo Datos del periodo a inactivar.
   * @throws PersistenciaExcepcion
   */
  public void inactivarPeriodo(SmperSemperiodo periodo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.smper_semperiodo ")
            .append("SET smper_fecact        = :fecha, ")
            .append("    usu_ideregistro_act = :usuario, ")
            .append("    smper_swtact        = :inactivo ")
            .append("WHERE smper_ideregistro = :idSemestrePeriodo ")
            .append("  AND smper_swtact = :activo;");
    parametros.put("fecha", LocalDate.now());
    parametros.put("usuario", auditoria.getIdUsuario());
    parametros.put("inactivo", EEstadoGenerico.INACTIVAR);
    parametros.put("idSemestrePeriodo", periodo.getSmperIderegistro());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    ejecutarEdicion(sql, parametros);
  }
   /**
   * Consulta los periodos de un semestrales.
   *
   * @param idPeriodo Identificador del periodo semestral.
   * @param idArea Identificador del Area de Prestacion.
   * @return Json con los nombres de los periodops.
   * @throws PersistenciaExcepcion
   */
  public String consultarPeriodosSemestre(
          Integer idPeriodo,
          Integer idArea 
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT   ") 
        .append("	ARRAY_TO_JSON(ARRAY_AGG(datos.*)) as periodos ") 
        .append(" FROM (   ") 
        .append("		SELECT prr.per_nombre ") 
        .append("		FROM aseo.smper_semperiodo smm    ") 
        .append("			INNER JOIN per_periodo prr    ") 
        .append("				ON prr.per_ideregistro = smm.per_ideregistro  ") 
        .append("			INNER JOIN per_periodo pant   ") 
        .append("				ON smm.per_idepadre = pant.per_ideregistro    ") 
        .append("			INNER JOIN per_periodo pact   ") 
        .append("				ON pact.cic_ideregistro =  pant.cic_ideregistro   ") 
        .append("				AND pact.per_ideregistro = :idPeriodo   ") 
        .append("				AND pact.per_fecinicial::DATE = CAST(pant.per_fecfinal  AS DATE) + CAST('1 days' AS INTERVAL) ") 
        .append("       INNER JOIN aseo.rgta_rgitarifario rgta ")
        .append("                  ON smm.rgta_ideregistro = rgta.rgta_ideregistro ")
        .append("       INNER JOIN aseo.arpr_areaprestacion arpr ")
        .append("                  ON rgta.rgta_ideregistro = arpr.rgta_ideregistro ")
        .append("		WHERE smm.smper_swtact = :estadoActivo  ") 
        .append("   		AND rgta.emp_ideregistro = :idEmpresa ")
        .append("   		AND arpr.arpr_ideregistro = :idArea ")
        .append("		ORDER BY prr.per_ideorden ") 
        .append(") datos  ") ;
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idPeriodo",idPeriodo);
    parametros.put("idArea", idArea);
    parametros.put("estadoActivo", EEstadoGenerico.ACTIVO);
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getObject("periodos", String.class, rs);
    });
  }
  
    /**
   * Método encargado de consultar los meses por periodo
   *
   * @param idArea Identificador del área de prestación
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<SmperSemperiodoMeses> consultarMesesPeriodo(Integer idArea)
              throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT  smper.per_idepadre, ")
            .append("   smper.rgta_ideregistro, ")
            .append("   smper_descripcion, ")
            .append("   per2.per_ideregistro, ")
            .append("   smper_numero, ")
            .append("   per.per_nombre, ")
            .append("   date_part('year', per.per_fecinicial) per_fecinicial  ")
            .append("FROM aseo.smper_semperiodo smper ")
            .append("         INNER JOIN aseo.rgta_rgitarifario rgta ON smper.rgta_ideregistro = rgta.rgta_ideregistro ")
            .append("         INNER JOIN aseo.arpr_areaprestacion arpr ON rgta.rgta_ideregistro = arpr.rgta_ideregistro ")
            .append("         INNER JOIN per_periodo per on smper.per_idepadre = per.per_ideregistro ")
            .append("         INNER JOIN per_periodo per2 on smper.per_ideregistro = per2.per_ideregistro ")
            .append("WHERE rgta.emp_ideregistro = :idEmpresa ")
            .append("  AND arpr.arpr_ideregistro = :idArea ")
         //   .append("  AND per.per_estado = :estadoActivo ")
            .append("  order by date_part('year', per.per_fecinicial) asc, ")
            .append("  per.per_nombre asc ");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idArea", idArea);
    parametros.put("estadoActivo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
      SmperSemperiodoMeses periodoMeses = getSmperSemperiodoMeses(rs, columns, "");
      //PerPeriodo periodo = PerPeriodoDAO.getPerPeriodo(rs, columns, "per_periodo1");
      //periodo.setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      return periodoMeses;
    });
  }

}
