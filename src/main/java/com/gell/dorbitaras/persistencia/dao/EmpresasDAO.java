package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class EmpresasDAO extends EmpresasCRUD {

  public EmpresasDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Consulta todas las empresas dependiendo el parametro de busqueda
   *
   * @param codigo Código de la empresa
   * @return Lista de las empresas
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<Empresas> consultarEmpresa(String codigo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM empresas  ")
             .append("  WHERE empresa_cod ILIKE :codigo ")
             .append("    AND empresa_sevemp IS NOT NULL");
    parametros.put("codigo", '%' + codigo + '%');
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<Empresas>() {
      @Override
      public Empresas siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        Empresas emp = getEmpresas(rs, columns, "empresas1");

        return emp;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }

    });
  }
}
