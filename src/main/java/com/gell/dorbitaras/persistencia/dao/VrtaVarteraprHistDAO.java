package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.dao.crud.VrtaVarteraprHistCRUD;
import com.gell.dorbitaras.persistencia.dto.ValoresToneladasDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoBaseDTO;
import com.gell.dorbitaras.persistencia.dto.VrtaVarteraprDTO;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarterapr;
import com.gell.dorbitaras.persistencia.entidades.VrtaVarteraprHist;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * las variables.
 *
 * @author Desarrollador
 */
public class VrtaVarteraprHistDAO extends VrtaVarteraprHistCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public VrtaVarteraprHistDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Registra una variable en la base de datos.
   *
   * @param vrtaVariableHist Datos de la variable a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public void insertar(VrtaVarteraprHist vrtaVariableHist)
          throws PersistenciaExcepcion
  {
    if (vrtaVariableHist.getVrtaHistIderegistro() != null) {
      editar(vrtaVariableHist);
      return;
    }
    super.insertar(vrtaVariableHist);
  }
  
    /**
   * Método encargado de eliminar la empresa de la ruta
   *
   * @param idRutaEmpresa Identificador del registro.
   * @throws PersistenciaExcepcion
   */
  public void eliminarToneladaByVrta(Integer idVrtaIderegistro)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("DELETE ")
            .append(" from aseo.vrta_varterapr_hist ")
            .append("where vrta_ideregistro = :idVrtaIderegistro ");
    parametros.put("idVrtaIderegistro", idVrtaIderegistro);
    ejecutarEdicion(sql, parametros);
  }
}
