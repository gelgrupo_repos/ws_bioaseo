package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EMensajeNegocio;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.RureRutrecoleccionCRUD;
import com.gell.dorbitaras.persistencia.entidades.RureRutrecoleccion;
import com.gell.estandar.constante.EMensajeEstandar;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.excepcion.AplicacionExcepcion;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import static com.gell.estandar.util.FuncionesDatoUtil.json;
import static com.gell.estandar.util.FuncionesDatoUtil.jsonMap;
import com.gell.estandar.util.LogUtil;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class RureRutrecoleccionDAO extends RureRutrecoleccionCRUD {

  public RureRutrecoleccionDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Método que expone el método de guardarMacroRuta y si se envía el
   * identificador de la ruta se edita
   *
   * @param rutrecoleccion Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarMacroRuta(RureRutrecoleccion rutrecoleccion)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer ideRegistro = rutrecoleccion.getRureIderegistro();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    RureRutrecoleccion rutaGuardada = consultar(ideRegistro.longValue());
    
    if (rutaGuardada != null) {
      rutrecoleccion.setRureFecact(new Date());
      this.editar(rutrecoleccion);
      return;
    }
    rutrecoleccion.setUsuIderegistroGb(auditoria.getIdUsuario())
            .setRureFecgrabacion(new Date());
    super.insertar(rutrecoleccion);
  }

  /**
   * Método que controla la consulta de ruta de recolección según parametro
   *
   * @param area Área de prestación
   * @param macro Macro de ruta
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<RureRutrecoleccion> consultarRecoleccion(Integer area, Integer macro)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM aseo.rure_rutrecoleccion rure,  ")
            .append("                  to_json((  ")
            .append("                   SELECT ARRAY_AGG(dias.*)  ")
            .append("                      FROM (SELECT *  ")
            .append("                          FROM aseo.hrr_horrecoleccion dia  ")
            .append("                      WHERE rure.rure_ideregistro = dia.rure_ideregistro ) AS dias  ")
            .append("                   )) horario  ")
            .append("      WHERE rure.arpr_ideregistro = :area AND rure.rut_idemacruta = :macro ");
    parametros.put("area", area);
    parametros.put("macro", macro);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<RureRutrecoleccion>() {
      @Override
      public RureRutrecoleccion siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        String horario = getObject("horario", String.class, rs);
        RureRutrecoleccion rutRecoleccion
                = RureRutrecoleccionDAO.getRureRutrecoleccion(rs, columns, "rure_rutrecoleccion1");
        rutRecoleccion.setInfo(horario);
        return rutRecoleccion;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Valida que el area y la macroruta  no exista en la base de datos
   *
   * @param area
   * @param macro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void validarRuta(Integer area, Integer macro)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
   
    Integer numero = consultarRuta(area, macro);
    if (numero > 0) {
      throw new NegocioExcepcion(EMensajeNegocio.ERROR_UNIDAD_CODIGO);
    }
  }
    /**
     * Consulta si el area y la macroruta ya existe en la base de datos
     *
     * @param area area de prestación
     * @param macro macroruta
     * @return El número de veces que se encuentra registrado el códgio en la
     * base de datos
     * @throws PersistenciaExcepcion
     */

  public Integer consultarRuta(Integer area, Integer macro)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT count(*) numero FROM aseo.rure_rutrecoleccion rure,  ")
            .append("                  to_json((  ")
            .append("                   SELECT ARRAY_AGG(dias.*)  ")
            .append("                      FROM (SELECT *  ")
            .append("                          FROM aseo.hrr_horrecoleccion dia  ")
            .append("                      WHERE rure.rure_ideregistro = dia.rure_ideregistro ) AS dias  ")
            .append("                   )) horario  ")
            .append("      WHERE rure.arpr_ideregistro = :area AND rure.rut_idemacruta = :macro ");
    parametros.put("area", area);
    parametros.put("macro", macro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getObject("numero", Integer.class, rs));
  }

    /**
     * Consulta del id del registro si ya existe en la base de datos
     *
     * @param area area de prestación
     * @param macro macroruta
     * @return El id
     * @throws PersistenciaExcepcion
     */

  public Integer consultarId(Integer area, Integer macro)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT rure_ideregistro id FROM aseo.rure_rutrecoleccion rure,  ")
            .append("                  to_json((  ")
            .append("                   SELECT ARRAY_AGG(dias.*)  ")
            .append("                      FROM (SELECT *  ")
            .append("                          FROM aseo.hrr_horrecoleccion dia  ")
            .append("                      WHERE rure.rure_ideregistro = dia.rure_ideregistro ) AS dias  ")
            .append("                   )) horario  ")
            .append("      WHERE rure.arpr_ideregistro = :area AND rure.rut_idemacruta = :macro ");
    parametros.put("area", area);
    parametros.put("macro", macro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getObject("id", Integer.class, rs));
  }

}
