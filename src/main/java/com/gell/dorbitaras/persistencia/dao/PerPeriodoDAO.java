package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EGlobal;
import com.gell.dorbitaras.negocio.constante.EParametro;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.CicCiclo;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * los periodos.
 *
 * @author Desarrollador
 */
public class PerPeriodoDAO extends PerPeriodoCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public PerPeriodoDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  private static final String[] MESES_SEMESTRE_1 = {
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio"
  };
  private static final String[] MESES_SEMESTRE_2 = {
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
  };

  /**
   * Consulta los periodos por empresa y que esten activos.
   *
   * @param idSemestre Identificador del semestre.
   * @return Lista de periodos.
   * @throws PersistenciaExcepcion
   */
  public List<PerPeriodo> consultarMesesSemestre(Integer idSemestre)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    StringBuilder condicion = new StringBuilder();
    String[] mesesSemestre = idSemestre == 1 ? MESES_SEMESTRE_1 : MESES_SEMESTRE_2;
    condicion.append(" AND per.per_nombre = ANY (:mesesSemestre::TEXT[])");
    parametros.put("mesesSemestre", crearArregloTexto(mesesSemestre));
    sql.append("SELECT DISTINCT per.per_nombre, ")
            .append("                per.per_ideregistro, ")
            .append("                cic.cic_nombre, ")
            .append("                cic.cic_ideregistro, ")
            .append("                per.per_fecinicial, ")
            .append("                per.per_ideorden, ")
            .append("                (to_char(per.per_fecinicial, 'yyyy')) anio ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN cic_ciclo cic ")
            .append("                    ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("         INNER JOIN ciem_cicempresa ciem ")
            .append("                    ON cic.cic_ideregistro = ciem.cic_ideregistro ")
            .append("WHERE per.per_estado IN (:activo, :bloqueado)  ")
            .append("  AND ciem.emp_ideregistro = :idEmpresa ")
            .append("  AND cic.cic_nombre ILIKE :nombreCiclo ")
            .append("  AND cic.cic_periodos = :periodos ")
            .append(condicion);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("bloqueado", EEstadoGenerico.BLOQUEADO);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + "%");
    parametros.put("periodos", EGlobal.PERIODOS_MENSUALES);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<PerPeriodo>()
    {
      @Override
      public PerPeriodo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        PerPeriodo periodo = getPerPeriodo(rs, columns, "per_periodo1");
        CicCiclo ciclo = CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1");
        String anio = getObject("anio", String.class, rs);
        periodo.setInfo(anio);
        return periodo.setCicIderegistro(ciclo);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Consulta los años enlazados a per_periodo y cic_ciclo.
   *
   * @param idCiclo Parametro opcional identificador del ciclo.
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<PerPeriodo> consultarAnios(Integer idCiclo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    StringBuilder condicion = new StringBuilder();
    if (null != idCiclo) {
      condicion.append(" AND cc.cic_ideregistro = :idCiclo ");
      parametros.put("idCiclo", idCiclo);
    }
    sql.append("SELECT DISTINCT per.per_ideregistro, ")
            .append("                cc.cic_nombre, ")
            .append("                cc.cic_ideregistro, ")
            .append("                per.per_fecinicial, ")
            .append("                per.per_nombre, ")
            .append("                per.per_ideorden, ")
            .append("                (to_char(per.per_fecinicial, 'yyyy')) anio ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN cic_ciclo cc on per.cic_ideregistro = cc.cic_ideregistro ")
            .append("         INNER JOIN ciem_cicempresa c on cc.cic_ideregistro = c.cic_ideregistro ")
            .append("WHERE emp_ideregistro = :idEmpresa ")
            .append("  AND cc.cic_nombre ILIKE :nombreCiclo ")
            .append("  AND per.per_estado = :activo ")
            .append("  AND cc.cic_estado = :activo ")
            .append(condicion)
            .append("ORDER BY per.per_ideregistro;");
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + "%");
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<PerPeriodo>()
    {
      @Override
      public PerPeriodo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        PerPeriodo periodo = getPerPeriodo(rs, columns, "per_periodo1");
        CicCiclo ciclo = CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1");
        String anio = getObject("anio", String.class, rs);
        periodo.setInfo(anio);
        return periodo.setCicIderegistro(ciclo);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Consulta los semestres
   *
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<PerPeriodo> consultarSemestres()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT DISTINCT per.per_nombre, ")
            .append("                per.per_ideregistro, ")
            .append("                cic.cic_nombre, ")
            .append("                cic.cic_ideregistro, ")
            .append("                per.per_fecinicial, ")
            .append("                per.per_ideorden, ")
            .append("                (to_char(per.per_fecinicial, 'yyyy')) anio ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN cic_ciclo cic ")
            .append("                    ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("         INNER JOIN ciem_cicempresa ciem ")
            .append("                    ON cic.cic_ideregistro = ciem.cic_ideregistro ")
            .append("WHERE per.per_estado IN (:activo, :bloqueado) ")
            .append("  AND ciem.emp_ideregistro = :idEmpresa ")
            .append("  AND cic.cic_nombre ILIKE :nombreCiclo ")
            .append("  AND cic.cic_periodos = :periodos; ");
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("bloqueado", EEstadoGenerico.BLOQUEADO);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + "%");
    parametros.put("periodos", EGlobal.PERIODOS_SEMESTRALES);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<PerPeriodo>()
    {
      @Override
      public PerPeriodo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        PerPeriodo periodo = getPerPeriodo(rs, columns, "per_periodo1");
        CicCiclo ciclo = CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1");
        String anio = getObject("anio", String.class, rs);
        periodo.setInfo(anio);
        return periodo.setCicIderegistro(ciclo);
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Método encargado de consultar los periodos inconsistentes, es decir periodos del
   * semestre anterior que aun no esten cerrados.
   *
   * @param idPeriodo Identificador del periodo.
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<PerPeriodo> consultarPeriodosInconsistentes(
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("    SELECT per.per_ideregistro, ")
            .append("       per.per_nombre, ")
            .append("       per.per_estado, ")
            .append("       cic.cic_nombre ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN aseo.smper_semperiodo smp ")
            .append("                    ON per.per_ideregistro = smp.per_ideregistro ")
            .append("         INNER JOIN public.cic_ciclo cic ")
            .append("                    ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("WHERE smp.smper_swtact = :activo ")
            .append(" AND  per.per_estado NOT IN (:cerrado)  ")
            .append("  AND smp.per_idepadre IN ( ")
            .append("    SELECT pp.per_ideregistro ")
            .append("    FROM aseo.smper_semperiodo smper ")
            .append("             INNER JOIN per_periodo pp ON smper.per_idepadre = pp.per_ideregistro ")
            .append("    WHERE pp.per_fecinicial::DATE IN ( ")
            .append("        SELECT (per.per_fecinicial - INTERVAL '6 MONTH')::DATE fechainicialperiodoanterior ")
            .append("        FROM per_periodo per ")
            .append("        WHERE per.per_ideregistro = :idPeriodo ")
            .append("          AND per.per_estado = :activo) ")
            .append(")")
            .append(" UNION ")
            .append("SELECT DISTINCT per.per_ideregistro, ")
            .append("   per.per_nombre, ")
            .append("   per.per_estado , ")
            .append("   cic.cic_nombre ")
            .append(" FROM per_periodo per ")
            .append("   INNER JOIN public.cic_ciclo cic ")
            .append("       ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("   INNER JOIN aseo.smper_semperiodo smmp ")
            .append("       ON smmp.per_idepadre = per.per_ideregistro ")
            .append("       AND smmp.smper_swtact = :activo ")
            .append("   INNER JOIN per_periodo psact ")  
            .append("       ON psact.cic_ideregistro = per.cic_ideregistro ")  
            .append("       AND psact.per_ideregistro = :idPeriodo ")  
            .append("WHERE per.per_fecfinal::DATE = (psact.per_fecinicial::DATE - CAST('1 days' AS INTERVAL))::DATE ")  
            .append("   AND per.per_estado NOT IN (:cerrado) ; ")  ;
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("cerrado", EEstadoGenerico.CERRADO);
    parametros.put("estado", EEstadoGenerico.INACTIVAR);

    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<PerPeriodo>()
    {
      @Override
      public PerPeriodo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getPerPeriodo(rs, columns, "per_periodo1")
                .setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      }
    });
  }

  /**
   * Método encargado de consultar meses del semestre siguiente al seleccionado.
   *
   * @param idPeriodo Identificador del periodo.
   * @return Lista de periodos
   * @throws PersistenciaExcepcion
   */
  public List<PerPeriodo> consultarMesesSemestreSiguiente(
          Integer idPeriodo
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT per.* ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN public.cic_ciclo cic ")
            .append("                    ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("WHERE per.per_estado NOT IN (:cerrado, :estado) ")
            .append("  AND cic.cic_nombre ILIKE :nombreCiclo ")
            .append("  AND per.per_nombre NOT ILIKE :semestre ")
            .append("  AND per.per_fecfinal BETWEEN (SELECT per.per_fecfinal ")
            .append("                                FROM per_periodo per ")
            .append("                                WHERE per.per_ideregistro = :idPeriodo ")
            .append("                                  AND per.per_estado = :activo) ")
            .append("    AND (SELECT (per.per_fecfinal + INTERVAL '6 MONTH')::DATE as fechainicialperiodosiguiente ")
            .append("         FROM per_periodo per ")
            .append("         WHERE per.per_ideregistro = :idPeriodo ")
            .append("           AND per.per_estado = :activo);");
    parametros.put("cerrado", EEstadoGenerico.CERRADO);
    parametros.put("estado", EEstadoGenerico.INACTIVAR);
    parametros.put("semestre", "%" + EParametro.SEMESTRE + "%");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + "%");

    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<PerPeriodo>()
    {
      @Override
      public PerPeriodo siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        return getPerPeriodo(rs, columns, "per_periodo1")
                .setCicIderegistro(CicCicloDAO.getCicCiclo(rs, columns, "cic_ciclo1"));
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS_PERIODOS_SEMESTRE_SIGUIENTE);
      }
    });
  }

  /**
   * Consulta que trae el semestre siguiente al seleccionado
   *
   * @param idPeriodo Identificador del periodo semestral.
   * @return El número de veces que se encuentra registrado el códgio en la base
   * de datos
   * @throws PersistenciaExcepcion
   */
  public PerPeriodo consultarSemestreSiguiente(Integer idPeriodo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT per.* ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN public.cic_ciclo cic ")
            .append("                    ON per.cic_ideregistro = cic.cic_ideregistro ")
            .append("WHERE per_estado NOT IN (:cerrado, :estado) ")
            .append("  AND cic.cic_nombre ILIKE :nombreCiclo ")
            .append("  AND per_ideregistro IN ( ")
            .append("    SELECT smper.per_ideregistro ")
            .append("    FROM per_periodo smper ")
            .append("    WHERE smper.per_fecfinal::DATE IN ( ")
            .append("        SELECT (per.per_fecfinal + INTERVAL '6 MONTH')::DATE as fechainicialperiodoanterior ")
            .append("        FROM per_periodo per ")
            .append("        WHERE per.per_ideregistro = :idPeriodo ")
            .append("          AND per.per_estado = :activo) ")
            .append(");");
    parametros.put("cerrado", EEstadoGenerico.CERRADO);
    parametros.put("estado", EEstadoGenerico.INACTIVAR);
    parametros.put("nombreCiclo", "%" + EParametro.CICLO_TARAS + "%");
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> getPerPeriodo(rs));
  }

  /**
   * Método encargado de consultar los meses de un semestre.
   *
   * @param idPeriodoPadre Identificador del periodo semestral.F
   * @return Lista de periodos mensuales
   * @throws PersistenciaExcepcion
   */
  public List<PerPeriodo> consultarMesesSemestrePeriodo(
          Integer idPeriodoPadre
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT * ")
            .append("FROM per_periodo per ")
            .append("         INNER JOIN aseo.smper_semperiodo smper ")
            .append("                    ON per.per_ideregistro = smper.per_ideregistro ")
            .append("WHERE smper.per_idepadre = :periodoSemestral ")
            .append("  AND smper.smper_swtact = :activo;");
    parametros.put("periodoSemestral", idPeriodoPadre);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, (rs, columns)
            -> getPerPeriodo(rs));
  }
  
  /**
   * Método encargado de inactivar los periodos semestral y mensuales del periodo
   * padre semestral.
   *
   * @param idPeriodo Identificador del periodo semestral.
   * @throws PersistenciaExcepcion
   */
  public void inactivarPeriodos(Integer idPeriodo)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("update per_periodo ")
        .append("set per_estado = :cerrado ")
        .append("from   ")
        .append("(select distinct pp.* from aseo.smper_semperiodo ss  ")
        .append("inner join per_periodo pp on pp.per_ideregistro = ss.per_idepadre  ")
        .append("where ss.per_idepadre = :periodo and pp.per_estado = :activo ")
        .append("union  ")
        .append("select distinct pp.* from aseo.smper_semperiodo ss  ")
        .append("inner join per_periodo pp on pp.per_ideregistro = ss.per_ideregistro  ")
        .append("where ss.per_idepadre = :periodo and pp.per_estado = :activo) pp2  ")
        .append("where per_periodo.per_ideregistro = pp2.per_ideregistro; ");
    parametros.put("cerrado", EEstadoGenerico.CERRADO);
    parametros.put("periodo", idPeriodo);
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    ejecutarEdicion(sql, parametros);
  }

}
