package com.gell.dorbitaras.persistencia.dao;

import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;

public class RuemRutempresaDAO extends RuemRutempresaCRUD
{

  public RuemRutempresaDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   *
   * Método para la inserción y edición de datos
   *
   * @param rutaEmpresa Informacíon de ruta para ruta empresa
   * @throws PersistenciaExcepcion
   */
  @Override
  public void insertar(RuemRutempresa rutaEmpresa)
          throws PersistenciaExcepcion
  {
    Integer ideRegistro = rutaEmpresa.getRuemIderegistr();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    RuemRutempresa documentoGuardada = consultar(ideRegistro.longValue());
    if (documentoGuardada != null) {
      this.editar(rutaEmpresa);
      return;
    }
    super.insertar(rutaEmpresa);
  }

  /**
   * Método encargado de eliminar la empresa de la ruta
   *
   * @param idRutaEmpresa Identificador del registro.
   * @throws PersistenciaExcepcion
   */
  public void eliminarEmpresa(Integer idRutaEmpresa)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("DELETE ")
            .append("FROM ruem_rutempresa ")
            .append("WHERE ruem_ideregistr = :idRutaEmpresa ");
    parametros.put("idRutaEmpresa", idRutaEmpresa);
    ejecutarEdicion(sql, parametros);
  }
}
