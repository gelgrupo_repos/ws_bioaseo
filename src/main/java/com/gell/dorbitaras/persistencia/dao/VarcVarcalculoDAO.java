package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.constante.EPrograma;
import com.gell.dorbitaras.persistencia.entidades.VarcVarcalculo;
import com.gell.estandar.dto.AuditoriaDTO;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.dto.ValorCalculadoVariacionDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoBaseDTO;
import com.gell.dorbitaras.persistencia.dto.VarcVarcalculoDTO;
import com.gell.dorbitaras.persistencia.dto.VariacionCostosProductividadDTO;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;

/**
 * Clase encargada de enviar la información a la base de datos con referencia a
 * las variables.
 *
 * @author Desarrollador
 */
public class VarcVarcalculoDAO extends VarcVarcalculoCRUD
{

  /**
   * Super clase.
   *
   * @param datasource Objeto de la conexión a la base de datos.
   * @param auditoria Datos prioritarios.
   * @throws PersistenciaExcepcion
   */
  public VarcVarcalculoDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Método encargado de consultar los datos de la variable según el periodo,
   * concepto y área de prestación.
   *
   * @param periodo Identificador del periodo.
   * @param concepto Idenfificador del concepto.
   * @param areaPrestacion Identificador del área de prestación.
   * @return Variable consultada.
   * @throws PersistenciaExcepcion
   */
  public List<VarcVarcalculo> consultarDatosVariable(
          Integer periodo,
          Integer concepto,
          Integer areaPrestacion
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT rcc.raco_raninicial , rcc.raco_ranfinal, var.* ")
            .append("FROM aseo.varc_varcalculo var ")
            .append("LEFT JOIN public.raco_ranconcept rcc ")
            .append("ON rcc.raco_ideregistr = var.raco_ideregistro ")
            .append("WHERE var.per_ideregistro = :periodo ")
            .append("  AND var.con_ideregistro = :concepto ")
            .append("  AND var.arpr_ideregistro = :areaPrestacion ")
            .append("  AND var.varc_estadoregistro = :estado ")
            .append("  AND var.emp_ideregistro = :idEmpresa")
            .append("  ORDER BY var.varc_estado , var.raco_ideregistro ");
    parametros.put("periodo", periodo);
    parametros.put("concepto", concepto);
    parametros.put("areaPrestacion", areaPrestacion);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, (rs, columns) -> getVarcVarcalRaco(rs));
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto y área de prestación.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VarcVarcalculo consultarVerificarRegistro(VarcVarcalculo datosVariable)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT var.* ")
            .append("FROM aseo.varc_varcalculo var ")
            .append("WHERE var.per_ideregistro = :periodo ")
            .append(" AND var.con_ideregistro = :concepto ")
            .append(" AND var.arpr_ideregistro = :areaPrestacion ")
            .append(" AND var.emp_ideregistro = :idEmpresa ")
            .append(" AND var.varc_estado = :estado ")
            .append(" AND var.varc_estadoregistro = :estadoRegistro ")
            .append(" AND var.raco_ideregistro ISNULL ")
            .append("ORDER BY var.varc_ideregistro DESC ")
            .append("LIMIT 1");
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("estado", EEstadoGenerico.CERTIFICADO);
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVarcVarcalculo(rs);
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto y área de prestación.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VarcVarcalculo consultarVerificarRegistroPendiente(VarcVarcalculo datosVariable)
          throws PersistenciaExcepcion
  {
    String complemento = "";
    if (datosVariable.getRacoIderegistro() != null
            && datosVariable.getRacoIderegistro().getRacoIderegistr() != null) {
      Integer idRaco = datosVariable.getRacoIderegistro().getRacoIderegistr();
      complemento = " AND var.raco_ideregistro = :idRaco ";
      parametros.put("idRaco", idRaco);
    }
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT var.* ")
            .append("FROM aseo.varc_varcalculo var ")
            .append("WHERE var.per_ideregistro = :periodo ")
            .append(" AND var.con_ideregistro = :concepto ")
            .append(" AND var.arpr_ideregistro = :areaPrestacion ")
            .append(" AND var.emp_ideregistro = :idEmpresa ")
            .append(" AND var.varc_estado = :estado ")
            .append(" AND var.varc_estadoregistro = :estadoRegistro ")
            .append(complemento)
            .append("ORDER BY var.varc_ideregistro DESC ")
            .append("LIMIT 1");
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("estado", EEstadoGenerico.PENDIENTE);
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVarcVarcalculo(rs);
    });
  }

  /**
   * Método encargado de consultar si ya existe la variable según el periodo,
   * concepto, área de prestación identificador del raco.
   *
   * @param datosVariable Datos de la variable.
   * @return Lista de variables que cumplan con los criterios de búsqueda.
   * @throws PersistenciaExcepcion
   */
  public VarcVarcalculo consultarVerificarRegistroRaco(VarcVarcalculo datosVariable)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT var.* ")
            .append("FROM aseo.varc_varcalculo var ")
            .append("WHERE var.per_ideregistro = :periodo ")
            .append("  AND var.con_ideregistro = :concepto ")
            .append("  AND var.arpr_ideregistro = :areaPrestacion ")
            .append("  AND var.emp_ideregistro = :idEmpresa ")
            .append("  AND var.varc_estado = :estado ")
            .append("  AND var.varc_estadoregistro = :estadoRegistro ")
            .append("  AND var.raco_ideregistro = :idRaco ")
            .append("ORDER BY var.varc_ideregistro DESC ")
            .append("LIMIT 1 ");
    parametros.put("periodo", datosVariable.getPerIderegistro().getPerIderegistro());
    parametros.put("concepto",
            datosVariable.getConIderegistro().getUniConcepto().getUniIderegistro());
    parametros.put("areaPrestacion", datosVariable.getArprIderegistro().getArprIderegistro());
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("estadoRegistro", EEstadoGenerico.ACTIVO);
    parametros.put("estado", EEstadoGenerico.CERTIFICADO);
    parametros.put("idRaco", datosVariable.getRacoIderegistro().getRacoIderegistr());
    return ejecutarConsultaSimple(sql, parametros, (rs, columns) -> {
      return getVarcVarcalculo(rs);
    });
  }

    /**
     * Consulta las variables en estado pendiente según área de prestación y
     * periodo.
     *
     * @param periodo Identificador del periodo.
     * @param areaPrestacion Identificador del área de prestación.
     * @return Lista de variables en estado pendiente.
     * @throws PersistenciaExcepcion
     */
        public List<VarcVarcalculoDTO> consultarVariablesPendientes(
            Integer periodo,
            Integer areaPrestacion
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("      SELECT ")
                .append("		varc.varc_ideregistro idvcalculo , ")
                .append("		varc.con_ideregistro idconcepto, ")
                .append("		varc.raco_ideregistro idrango , ")
                .append("		con.uni_nombre1 nombreconcepto, ")
                .append("		substr((con.uni_propiedad -> 'decimalesVisualiza')::TEXT, 2 ,  ")
                .append("		length((con.uni_propiedad -> 'decimalesVisualiza')::TEXT) - 2 ) decimales , ")
                .append("		varc.varc_fecgrabacion fecgrabacion, ")
                .append("		varc.varc_valor nuevovalor, ")
                .append("		varc.varc_estado estadovariable ,")
                .append("		varc.varc_descripcion observacion,  ")
                .append("		rcco.raco_raninicial raninicial ,")
                .append("		rcco.raco_ranfinal ranfinal ,")
                .append("		(SELECT varc2.varc_valor ")
                .append("		 FROM aseo.varc_varcalculo varc2 ")
                .append("		 WHERE  varc2.per_ideregistro = varc.per_ideregistro ")
                .append("                   AND varc2.con_ideregistro = varc.con_ideregistro ")
                .append("                   AND varc2.arpr_ideregistro = varc.arpr_ideregistro ")
                .append("                   AND varc2.emp_ideregistro = varc.emp_ideregistro ")
                .append("                   AND (varc2.raco_ideregistro = varc.raco_ideregistro OR varc2.raco_ideregistro IS NULL) ")
                .append("                   AND varc2.varc_estado = :certificado AND varc2.varc_estadoregistro  = :activo ")
                .append("		) valoranterior ")
                .append("	FROM aseo.varc_varcalculo varc   ")
                .append("           INNER JOIN uni_unidad con ON varc.con_ideregistro = con.uni_ideregistro  ")
                .append("         INNER JOIN public.prun_prgunidad pp on pp.uni_ideregistro = con.uni_ideregistro ")//se agrega validacion para filtrar concepto por usuarios
                .append("         INNER JOIN public.uspu_usuprgunid uu on pp.prun_ideregistr = uu.prun_ideregistr ")//se agrega validacion para filtrar concepto por usuarios
                .append("           LEFT JOIN raco_ranconcept rcco ON rcco.raco_ideregistr = varc.raco_ideregistro ")
                .append("	WHERE varc.emp_ideregistro = :idempresa ")
                .append("           AND uu.usu_ideregistro = :usuario   ")
                .append("           AND varc.varc_estado = :pendiente   ")
                .append("           AND pp.prg_ideregistro = :idprograma  ")
                .append("           AND varc.varc_estadoregistro = :activo ")
                .append("           AND varc.per_ideregistro = :periodo   ")
                .append("           AND varc.arpr_ideregistro = :areaprestacion ")
                .append("	ORDER BY con.uni_nombre1 , rcco.raco_raninicial ");
        parametros.put("periodo", periodo);
        parametros.put("areaprestacion", areaPrestacion);
        parametros.put("idempresa", auditoria.getIdEmpresa());
        parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
        parametros.put("activo", EEstadoGenerico.ACTIVO);
        parametros.put("pendiente", EEstadoGenerico.PENDIENTE);
        parametros.put("usuario", auditoria.getIdUsuario());
        parametros.put("idprograma", EPrograma.PROGRAMA_CERTIFICACION_VARIABLES);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            VarcVarcalculoDTO variableDTO = new VarcVarcalculoDTO();
            String valor_ant = getObject("valoranterior", String.class, rs);
            String valor_act = getObject("nuevovalor", String.class, rs);
            Integer vlr_redon = getObject("decimales", Integer.class, rs);
            variableDTO.setValor(valor_act);
            variableDTO.setValorAnt(valor_ant);
//            if ((valor_act == null) || (valor_act.equals(""))) {
//                variableDTO.setValor(valor_act);
//            } else {
//                double vlr_act = Double.parseDouble(valor_act);
//                vlr_act = new BigDecimal(vlr_act)
//                        .setScale(vlr_redon, RoundingMode.HALF_EVEN).doubleValue();
//                variableDTO.setValor(String.valueOf(vlr_act));
//            }
//            if ((valor_ant == null) || (valor_ant.equals(""))) {
//                variableDTO.setValorAnt(valor_ant);
//            } else {
//                double vlr_ant = Double.parseDouble(valor_ant);
//                vlr_ant = new BigDecimal(vlr_ant)
//                        .setScale(vlr_redon, RoundingMode.HALF_EVEN).doubleValue();
//                variableDTO.setValorAnt(String.valueOf(vlr_ant));
//            }
            variableDTO.setIdConcepto(getObject("idconcepto", Integer.class, rs));
            variableDTO.setIdVcalculo(getObject("idvcalculo", Integer.class, rs));
            variableDTO.setIdRango(getObject("idrango", Integer.class, rs));
            variableDTO.setEstado(getObject("estadovariable", String.class, rs));
            variableDTO.setNombreConcepto(getObject("nombreconcepto", String.class, rs));
            variableDTO.setFecGrabacion(getObject("fecgrabacion", String.class, rs));
            variableDTO.setObservacion(getObject("observacion", String.class, rs));
            variableDTO.setRanInical(getObject("raninicial", String.class, rs));
            variableDTO.setRanFinal(getObject("ranfinal", String.class, rs));
            return variableDTO;
        });
    }
    /**
   * Método encargado de consultar los conceptos para el base calculo.
   *
   * @param idLiquidacion  
   * @param idPeriodo  
   * @param areaPrestacion  
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<VarcVarcalculoBaseDTO> consultarVariablesBaseCalcuclo(
          Integer idLiquidacion , 
          Integer idPeriodo,
          Integer areaPrestacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT               ")
        .append("	variables.*,      ")
        .append("	(	SELECT ARRAY_TO_JSON(ARRAY_AGG(datos.*)) ")
        .append("		FROM (        ")
        .append("		select * from        ")
        .append("				(SELECT distinct prr.per_nombre ,  replace(vcc.varc_valor::varchar,'.',',') as varc_valor, prr.per_ideorden  ")
        .append("				FROM aseo.smper_semperiodo smm   ")
        .append("					INNER JOIN per_periodo prr   ")
        .append("						ON prr.per_ideregistro = smm.per_ideregistro  ")
        .append("					INNER JOIN per_periodo pant  ")
        .append("						ON smm.per_idepadre = pant.per_ideregistro ")
        .append("					INNER JOIN per_periodo pact ON pact.cic_ideregistro =  pant.cic_ideregistro  ")
        .append("						AND pact.per_ideregistro = :idPeriodo  ")
        .append("						AND pact.per_fecinicial::DATE = CAST(pant.per_fecfinal  AS DATE) + CAST('1 days' AS INTERVAL) ")
        .append("					INNER JOIN aseo.varc_varcalculo vcc ")
        .append("						ON vcc.per_ideregistro = smm.per_ideregistro  ")
        .append("						AND con_ideregistro = variables.uni_concepto  ")
        .append("						AND varc_estado = :certificado   ")
        .append("						AND varc_estadoregistro = :activo  ")
        .append("						AND (raco_ideregistro = variables.raco_ideregistr  OR vcc.raco_ideregistro is NULL ) ")
        .append("						AND arpr_ideregistro = :areaPrestacion ")
        .append("				WHERE smm.smper_swtact = :activo  ")
        .append("				) ordenados order by ordenados.per_ideorden  ")
        .append("			) datos   ")
        .append("	) as valores_meses   ")
        .append(" FROM (               ")
        .append("		SELECT DISTINCT  ")
        .append("			con.con_nombre ,   ")
        .append("			con.uni_concepto , ")
        .append("			rc.raco_ideregistr ,  ")
        .append("			rc.raco_raninicial ,  ")
        .append("			rc.raco_ranfinal   ")
        .append("		FROM public.con_concepto con ")
        .append("			INNER JOIN public.uni_unidad unn  ")
        .append("				ON unn.uni_ideregistro = con.uni_concepto ")
        .append("				AND unn.uni_propiedad -> 'tipoVariable' = '\"B\"' ")
        .append("			INNER JOIN public.est_estructura est ")
        .append("				ON con.est_concepto = est.est_ideregistro ")
        .append("			INNER JOIN public.esem_estempresa ee ")
        .append("				ON est.est_ideregistro = ee.est_ideregistro  ")
        .append("			INNER JOIN public.core_conrelacio crr   ")
        .append("				ON crr.uni_conrelacion = con.uni_concepto ")
        .append("			INNER JOIN public.coli_conliquida cll   ")
        .append("				ON cll.uni_concepto = crr.uni_concepto ")
        .append("			INNER JOIN public.liq_liquidacion liq   ")
        .append("				ON cll.uni_liquidacion = liq.uni_liquidacion ")
        .append("			LEFT JOIN public.raco_ranconcept rc  ")
        .append("				ON con.uni_concepto = rc.uni_concepto  ")
        .append("		WHERE liq.uni_liquidacion = :idLiquidacion   ")
        .append("			AND ee.emp_ideregistro = :idempresa   ")
        .append("		ORDER BY con.con_nombre , rc.raco_raninicial   ")
        .append("	) variables      ") 
        .append("	 WHERE uni_concepto NOT IN ") 
        .append("	(SELECT uni_ideregistro  ") 
        .append("	FROM prun_prgunidad ") 
        .append("	where prg_ideregistro = :prg_apr) ; ") ;
    parametros.put("idLiquidacion", idLiquidacion);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("areaPrestacion", areaPrestacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("prg_apr", 513 );
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
        VarcVarcalculoBaseDTO varibleBase = new VarcVarcalculoBaseDTO() ;
        varibleBase.setIdConcepto(getObject("uni_concepto", Integer.class, rs));
        varibleBase.setIdRango(getObject("raco_ideregistr", Integer.class, rs));
        varibleBase.setNombreConcepto(getObject("con_nombre", String.class, rs));
        varibleBase.setRanInical(getObject("raco_raninicial", String.class, rs));
        varibleBase.setRanFinal(getObject("raco_ranfinal", String.class, rs));
        varibleBase.setValorPeriodos(getObject("valores_meses", String.class, rs));
        return varibleBase;
    });
  }
//
//  /**
//   * Consulta las variables en estado pendiente según área de prestación y
//   * periodo.
//   *
//   * @param periodo Identificador del periodo.
//   * @param areaPrestacion Identificador del área de prestación.
//   * @return Lista de variables en estado pendiente.
//   * @throws PersistenciaExcepcion
//   */
//  public List<VarcVarcalculoDTO> consultarVariablesPendientes(
//          Integer periodo,
//          Integer areaPrestacion
//  )
//          throws PersistenciaExcepcion
//  {
//    StringBuilder sql = new StringBuilder();
//    sql.append("SELECT *, ")
//            .append("       (SELECT ARRAY_TO_JSON(ARRAY_AGG(rangos.*)) ")
//            .append("        FROM (SELECT raco.raco_raninicial, ")
//            .append("                     raco.raco_ranfinal, ")
//            .append("                     varr.varc_ideregistro, ")
//            .append("                     raco.raco_ideregistr, ")
//            .append("                     ant.varc_valor  valoranterior, ")
//            .append("                     varr.varc_valor nuevovalor, ")
//            .append("                     varr.varc_estado ")
//            .append("              FROM aseo.varc_varcalculo varr ")
//            .append("                       INNER JOIN raco_ranconcept raco ")
//            .append("                                  ON varr.raco_ideregistro = raco.raco_ideregistr ")
//            .append("                       LEFT JOIN LATERAL (SELECT * ")
//            .append("                                          FROM aseo.varc_varcalculo vara ")
//            .append("                                          WHERE vara.raco_ideregistro = varr.raco_ideregistro ")
//            .append("                                            AND vara.varc_estado = :certificado ")
//            .append("                                            AND vara.varc_estadoregistro = :inactivo ")
//            .append("                                          ORDER BY vara.varc_ideregistro DESC ")
//            .append("                                          LIMIT 1) AS ant ")
//            .append("                                 ON varr.raco_ideregistro = ant.raco_ideregistro ")
//            .append("              WHERE varr.con_ideregistro = variables.idConcepto ")
//            .append("                AND varr.con_ideregistro IS NOT NULL ")
//            .append("                AND varr.varc_fecgrabacion = variables.varc_fecgrabacion ")
//            .append("              ORDER BY varr.varc_fecgrabacion) rangos) lista_rangos, ")
//            .append("       (SELECT to_json(valores.*) ")
//            .append("        FROM (SELECT varv.varc_ideregistro, ")
//            .append("                     ant.varc_valor  valoranterior, ")
//            .append("                     varv.varc_valor nuevovalor, ")
//            .append("                     varv.varc_estado ")
//            .append("              FROM aseo.varc_varcalculo varv ")
//            .append("                       LEFT JOIN LATERAL (SELECT * ")
//            .append("                                          FROM aseo.varc_varcalculo vari ")
//            .append("                                          WHERE vari.con_ideregistro = varv.con_ideregistro ")
//            .append("                                            AND vari.varc_estado = :certificado ")
//            .append("                                            AND vari.varc_estadoregistro = :inactivo ")
//            .append("                                          ORDER BY vari.varc_ideregistro DESC ")
//            .append("                  ) as ant ON varv.con_ideregistro = ant.con_ideregistro ")
//            .append("              WHERE varv.raco_ideregistro IS NULL ")
//            .append("                AND varv.varc_fecgrabacion = variables.varc_fecgrabacion ")
//            .append("                AND varv.con_ideregistro = variables.idConcepto ")
//            .append("                AND varv.per_ideregistro = :periodo ")
//            .append("                AND varv.arpr_ideregistro = :areaprestacion ")
//            .append("              LIMIT 1) as valores)                     valor ")
//            .append("FROM (SELECT DISTINCT varc.con_ideregistro idConcepto, ")
//            .append("                      con.con_nombre       nombreconcepto, ")
//            .append("                      varc.varc_fecgrabacion, ")
//            .append("                      varc.varc_descripcion observacion ")
//            .append("      FROM aseo.varc_varcalculo varc ")
//            .append("               INNER JOIN con_concepto con on varc.con_ideregistro = con.uni_concepto ")
//            .append("      WHERE varc.emp_ideregistro = :idempresa ")
//            .append("        AND varc.varc_estado = :pendiente ")
//            .append("        AND varc.varc_estadoregistro = :activo ")
//            .append("        AND varc.per_ideregistro = :periodo ")
//            .append("        AND varc.arpr_ideregistro = :areaprestacion ")
//            .append("     ) variables ");
//    parametros.put("periodo", periodo);
//    parametros.put("areaprestacion", areaPrestacion);
//    parametros.put("idempresa", auditoria.getIdEmpresa());
//    parametros.put("inactivo", EEstadoGenerico.INACTIVAR);
//    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
//    parametros.put("activo", EEstadoGenerico.ACTIVO);
//    parametros.put("pendiente", EEstadoGenerico.PENDIENTE);
//    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
//      String valorAnterior = getObject("valor", String.class, rs);
//      String rangos = getObject("lista_rangos", String.class, rs);
//      String nombre = getObject("nombreconcepto", String.class, rs);
//      String observacion = getObject("observacion", String.class, rs);
//      Integer idConcepto = getObject("idconcepto", Integer.class, rs);
//      VarcVarcalculoDTO variableDTO = new VarcVarcalculoDTO();
//      variableDTO.setIdConcepto(idConcepto);
//      variableDTO.setListaRangos(rangos);
//      variableDTO.setNombreConcepto(nombre);
//      variableDTO.setListaValores(valorAnterior);
//      variableDTO.setObservacion(observacion);
//      return variableDTO;
//    });
//  }

  /**
   * Método encargado de inactivar los registros al momento de guardar.
   *
   * @param idRegistro Identificador de la variable.
   * @throws PersistenciaExcepcion
   */
  public void editarVariable(
          Integer idRegistro
  )
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append("UPDATE aseo.varc_varcalculo ")
            .append("SET varc_estadoregistro = :estadoInactivo ")
            .append("WHERE varc_estadoregistro = :estado ")
            .append("  AND varc_ideregistro = :idRegistro ");
    parametros.put("estadoInactivo", EEstadoGenerico.INACTIVAR);
    parametros.put("estado", EEstadoGenerico.ACTIVO);
    parametros.put("idRegistro", idRegistro);
    ejecutarEdicion(sql, parametros);
  }

    /**
     * Método encargado de actualizar el cambio de base en la variación de un período.
     *
     * @param idVariacion Identificador de la variación.
     * @param valorActualizacion Valor a cambiar base.
     * @throws PersistenciaExcepcion
     */
    public void cambioBaseVariaciones(
            Integer idVariacion,
            Double  valorActualizacion
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("UPDATE aseo.varc_varcalculo ")
                .append("SET    varc_valor = :valorActualizacion ,")
                .append("       varc_descripcion = 'cambio_base' ")
                .append("WHERE varc_ideregistro = :idVariacion ");
        parametros.put("valorActualizacion", valorActualizacion);
        parametros.put("idVariacion", idVariacion);
        ejecutarEdicion(sql, parametros);
    }

  /**
   * Registra una variable en la base de datos.
   *
   * @param varcVarcalculo Datos de la variable a insertar.
   * @throws PersistenciaExcepcion Error al insertar la información.
   */
  @Override
  public void insertar(VarcVarcalculo varcVarcalculo)
          throws PersistenciaExcepcion
  {
    if (varcVarcalculo.getVarcIderegistro() != null) {
      editar(varcVarcalculo);
      return;
    }
    super.insertar(varcVarcalculo);
  }
  
/**
    * Registra una variable en la base de datos.
    *
    * @param varcVarcalculo Datos de la variable a insertar.
    * @throws PersistenciaExcepcion Error al insertar la información.
    */
    @Override
    public Integer agregarRegistro(VarcVarcalculo varcVarcalculo)
            throws PersistenciaExcepcion {        
        return super.agregarRegistro(varcVarcalculo);
    }
    
    public ValorCalculadoVariacionDTO comprobarVariacionMesPeriodo(Integer idArea, Integer idConcepto, Integer idMes)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("select v.varc_valor as valor, ")
                .append("   v.varc_estado_variacion  as bandera ")
                .append("   from aseo.varc_varcalculo v ")
                .append("  where v.arpr_ideregistro = :idArea ")
                .append("  and v.emp_ideregistro = :idEmpresa ")
                .append("  and v.con_ideregistro = :idConcepto ")
                .append("  and v.varc_estado_variacion = 'A' ")
                .append("  order by v.varc_fecgrabacion desc ")
                .append("  LIMIT 1 ");
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idMes", idMes);
        List<ValorCalculadoVariacionDTO> listValorCalculado = ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            ValorCalculadoVariacionDTO valorCalculadoVariacionDTO
                    = valorInicialCalculado(rs, columns, "varc_varcalculo");
            return valorCalculadoVariacionDTO;
        });
        return listValorCalculado.size() > 0 ? listValorCalculado.get(0) : null;
    }

    public Double obtenerValorProductividad(Integer idArea, Integer idConcepto, Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select v.varc_valor as valor  ")
                .append("  from aseo.varc_varcalculo v ")
                .append("  where v.arpr_ideregistro = :idArea ")
                .append("  and v.emp_ideregistro = :idEmpresa ")
                .append("  and v.con_ideregistro = :idConcepto ")
                .append("  and v.per_ideregistro  = :idPeriodo ")
                .append("  order by v.varc_fecgrabacion desc ")
                .append(" limit 1 ");
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        parametros.put("idPeriodo", idPeriodo);
        List<Object> valoresProductividad =  ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject(columns.get("1_valor"), Double.class, rs);
        });
        return valoresProductividad.isEmpty()? null: (Double)valoresProductividad.get(0);
    }
    

    public List<ValorCalculadoVariacionDTO> comprobarProductividadPrevia
        (Integer idArea,Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" (select v.varc_valor as valor, ")
                .append(" 	v.varc_estado_variacion  as bandera, ")
                .append("     v.con_ideregistro, ")
                .append("     v.varc_ideregistro, ")
                .append("     v.per_ideregistro ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" inner join per_periodo pp ")
                .append(" 	on v.per_ideregistro = pp.per_ideregistro ")
                .append(" inner  join per_periodo pp2 ")
                .append(" 	on pp2.per_ideregistro = :idPeriodo ")
                .append(" where v.arpr_ideregistro = :idArea ")
                .append(" 	and v.emp_ideregistro = :idEmpresa ")
                .append("     and v.con_ideregistro = (select uni_concepto ")
                .append(" 		from con_concepto cc ")
                .append(" 			where con_alias = 'VAR_SMLMV' limit 1) ")
                .append("       		and v.varc_estado_variacion = 'A' ")
                //.append("     and pp.per_fecinicial >= pp2.per_fecinicial ")
                .append("     and pp.per_fecfinal  <= (date_trunc('MONTH', pp2.per_fecinicial::date) + INTERVAL '1 MONTH - 1 day')::date ")
                .append("     order by pp.per_fecinicial desc ")
                .append("     limit 1) ")
                .append(" UNION ")
                .append(" (select v.varc_valor as valor, ")
                .append(" 	v.varc_estado_variacion  as bandera, ")
                .append("     v.con_ideregistro, ")
                .append("     v.varc_ideregistro, ")
                .append("     v.per_ideregistro ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" inner join per_periodo pp ")
                .append(" 	on v.per_ideregistro = pp.per_ideregistro  ")
                .append(" inner  join per_periodo pp2 ")
                .append(" 	on pp2.per_ideregistro = :idPeriodo ")
                .append(" where v.arpr_ideregistro = :idArea ")
                .append(" 	and v.emp_ideregistro = :idEmpresa ")
                .append("     and v.con_ideregistro = (select uni_concepto ")
                .append(" 		from con_concepto cc ")
                .append(" 			where con_alias = 'VAR_IPCc' limit 1) ")
                .append("       		and v.varc_estado_variacion = 'A' ")
                //.append("     and pp.per_fecinicial >= pp2.per_fecinicial ")
                .append("     and pp.per_fecfinal  <= (date_trunc('MONTH', pp2.per_fecinicial::date) + INTERVAL '1 MONTH - 1 day')::date ")
                .append("     order by pp.per_fecinicial desc ")
                .append("     limit 1) ")
                .append(" union ")
                .append(" (select v.varc_valor as valor, ")
                .append(" 	v.varc_estado_variacion  as bandera, ")
                .append("     v.con_ideregistro, ")
                .append("     v.varc_ideregistro, ")
                .append("     v.per_ideregistro ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" inner join per_periodo pp ")
                .append(" 	on v.per_ideregistro = pp.per_ideregistro ")
                .append(" inner  join per_periodo pp2 ")
                .append(" 	on pp2.per_ideregistro = :idPeriodo ")
                .append(" where v.arpr_ideregistro = :idArea ")
                .append(" 	and v.emp_ideregistro = :idEmpresa ")
                .append("     and v.con_ideregistro = (select uni_concepto ")
                .append(" 		from con_concepto cc ")
                .append(" 			where con_alias = 'VAR_IOAMB' limit 1) ")
                .append("       		and v.varc_estado_variacion = 'A' ")
                //.append("     and pp.per_fecinicial >= pp2.per_fecinicial ")
                .append("     and pp.per_fecfinal  <= (date_trunc('MONTH', pp2.per_fecinicial::date) + INTERVAL '1 MONTH - 1 day')::date ")
                .append("     order by pp.per_fecinicial desc ")
                .append("     limit 1) ")
                .append(" union ")
                .append(" (select v.varc_valor as valor, ")
                .append(" 	v.varc_estado_variacion  as bandera, ")
                .append("     v.con_ideregistro, ")
                .append("     v.varc_ideregistro, ")
                .append("     v.per_ideregistro ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" inner join per_periodo pp ")
                .append(" 	on v.per_ideregistro = pp.per_ideregistro ")
                .append(" inner  join per_periodo pp2 ")
                .append(" 	on pp2.per_ideregistro = :idPeriodo ")
                .append(" where v.arpr_ideregistro = :idArea ")
                .append(" 	and v.emp_ideregistro = :idEmpresa ")
                .append("     and v.con_ideregistro = (select uni_concepto ")
                .append(" 		from con_concepto cc ")
                .append(" 			where con_alias = 'VAR_IPC' limit 1) ")
                .append("       		and v.varc_estado_variacion = 'A' ")
                //.append("     and pp.per_fecinicial >= pp2.per_fecinicial ")
                .append("     and pp.per_fecfinal  <= (date_trunc('MONTH', pp2.per_fecinicial::date) + INTERVAL '1 MONTH - 1 day')::date ")
                .append("     order by pp.per_fecinicial desc ")
                .append("     limit 1) ")
                .append(" order by con_ideregistro asc ");
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idArea", idArea);
        parametros.put("idPeriodo", idPeriodo);
        return  ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            ValorCalculadoVariacionDTO valorCalculadoVariacionDTO
                    = valorInicialCalculado(rs, columns, "");
            return valorCalculadoVariacionDTO;
        });
    }
  
   /**
   * Método encargado de consultar los conceptos para el base calculo que no estan certificados.
   *
   * @param idLiquidacion  
   * @param idPeriodo  
   * @param areaPrestacion  
   * @return
   * @throws PersistenciaExcepcion
   */
  public List<VarcVarcalculoBaseDTO> consultarVarBaseCalcucloScertif(
          Integer idLiquidacion , 
          Integer idPeriodo,
          Integer areaPrestacion)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT               ")
        .append("	variables.*,      ")
        .append("	(	SELECT ARRAY_TO_JSON(ARRAY_AGG(datos.*)) ")
        .append("		FROM (        ")
        .append("				SELECT prr.per_nombre ,  vcc.varc_valor  ")
        .append("				FROM aseo.smper_semperiodo smm   ")
        .append("					INNER JOIN per_periodo prr   ")
        .append("						ON prr.per_ideregistro = smm.per_ideregistro  ")
        .append("					INNER JOIN per_periodo pant  ")
        .append("						ON smm.per_idepadre = pant.per_ideregistro ")
        .append("					INNER JOIN per_periodo pact ON pact.cic_ideregistro =  pant.cic_ideregistro  ")
        .append("						AND pact.per_ideregistro = :idPeriodo  ")
        .append("						AND pact.per_fecinicial::DATE = CAST(pant.per_fecfinal  AS DATE) + CAST('1 days' AS INTERVAL) ")
        .append("					LEFT JOIN aseo.varc_varcalculo vcc ")
        .append("						ON vcc.per_ideregistro = smm.per_ideregistro  ")
        .append("						AND con_ideregistro = variables.uni_concepto  ")
        .append("						AND varc_estado = :certificado   ")
        .append("						AND varc_estadoregistro = :activo  ")
        .append("						AND (raco_ideregistro = variables.raco_ideregistr  OR vcc.raco_ideregistro is NULL ) ")
        .append("						AND arpr_ideregistro = :areaPrestacion ")
        .append("				WHERE smm.smper_swtact = :activo  ")
        .append("				ORDER BY prr.per_ideorden  ")
        .append("			) datos   ")
        .append("	) as valores_meses   ")
        .append(" FROM (               ")
        .append("		SELECT DISTINCT  ")
        .append("			con.con_nombre ,   ")
        .append("			con.uni_concepto , ")
        .append("			rc.raco_ideregistr ,  ")
        .append("			rc.raco_raninicial ,  ")
        .append("			rc.raco_ranfinal   ")
        .append("		FROM public.con_concepto con ")
        .append("			INNER JOIN public.uni_unidad unn  ")
        .append("				ON unn.uni_ideregistro = con.uni_concepto ")
        .append("				AND unn.uni_propiedad -> 'tipoVariable' = '\"B\"' ")
        .append("			INNER JOIN public.est_estructura est ")
        .append("				ON con.est_concepto = est.est_ideregistro ")
        .append("			INNER JOIN public.esem_estempresa ee ")
        .append("				ON est.est_ideregistro = ee.est_ideregistro  ")
        .append("			INNER JOIN public.core_conrelacio crr   ")
        .append("				ON crr.uni_conrelacion = con.uni_concepto ")
        .append("			INNER JOIN public.coli_conliquida cll   ")
        .append("				ON cll.uni_concepto = crr.uni_concepto ")
        .append("			INNER JOIN public.liq_liquidacion liq   ")
        .append("				ON cll.uni_liquidacion = liq.uni_liquidacion ")
        .append("			LEFT JOIN public.raco_ranconcept rc  ")
        .append("				ON con.uni_concepto = rc.uni_concepto  ")
        .append("		WHERE liq.uni_liquidacion = :idLiquidacion   ")
        .append("			AND ee.emp_ideregistro = :idempresa   ")
        .append("		ORDER BY con.con_nombre , rc.raco_raninicial   ")
        .append("	) variables      ") 
        .append("	 WHERE uni_concepto NOT IN ") 
        .append("	(SELECT uni_ideregistro  ") 
        .append("	FROM prun_prgunidad ") 
        .append("	where prg_ideregistro = :prg_apr) ; ") ;
    parametros.put("idLiquidacion", idLiquidacion);
    parametros.put("idPeriodo", idPeriodo);
    parametros.put("areaPrestacion", areaPrestacion);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
    parametros.put("prg_apr", 513 );
    return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
        VarcVarcalculoBaseDTO varibleBase = new VarcVarcalculoBaseDTO() ;
        varibleBase.setIdConcepto(getObject("uni_concepto", Integer.class, rs));
        varibleBase.setIdRango(getObject("raco_ideregistr", Integer.class, rs));
        varibleBase.setNombreConcepto(getObject("con_nombre", String.class, rs));
        varibleBase.setRanInical(getObject("raco_raninicial", String.class, rs));
        varibleBase.setRanFinal(getObject("raco_ranfinal", String.class, rs));
        varibleBase.setValorPeriodos(getObject("valores_meses", String.class, rs));
        return varibleBase;
    });
  }
  
    /**
     * Método encargado de consultar los meses por periodo
     *
     * @param idArea Identificador del área de prestación
     * @param idPeriodo Identificador del periodo de prestación
     * @param idConcepto Identificador del concepto de prestación
     * @return
     * @throws PersistenciaExcepcion
     */
    public List<VariacionCostosProductividadDTO> consultarVariaciones(Integer idArea,
            Integer idPeriodo, Integer idConcepto)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("select distinct v.varc_ideregistro, ")
                .append(" v.varc_valor valor, ")
                .append(" smper.per_ideregistro per_id_registro, ")
                .append(" 'valor' as accessor, ")
                .append(" smper.smper_descripcion || ' - ' || date_part('year', pp.per_fecinicial)  as header, ")
                .append(" v.varc_estado_variacion as bandera, v.varc_id_concepto_aplicado ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" inner join aseo.smper_semperiodo smper on v.per_ideregistro  = smper.per_ideregistro ")
                .append(" inner join public.per_periodo pp on pp.per_ideregistro = v.per_ideregistro ")
                .append(" where varc_ideregistro in ( ")
                .append(" select MAX(v.varc_ideregistro) ")
                .append(" from aseo.varc_varcalculo v ")
                .append(" where v.arpr_ideregistro = :idArea ")
                .append(" and v.emp_ideregistro = :idEmpresa ")
                .append(" and v.con_ideregistro = :idConcepto ")
                .append(" group by v.varc_id_mes ")
                .append(" order by varc_id_mes desc); ");
        parametros.put("idEmpresa", auditoria.getIdEmpresa());
        parametros.put("idArea", idArea);
        parametros.put("idConcepto", idConcepto);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            VariacionCostosProductividadDTO variacionCostosProductividadDTO
                    = getVariacionesCostos(rs, columns, "");
            return variacionCostosProductividadDTO;
        });
    }
    
  /**
   *
   * @return Cantidad de registros
   * @throws PersistenciaExcepcion
   */
  public Double VaracionTotales(Integer idConcepto, Integer idArea, Integer idPeriodo)
          throws PersistenciaExcepcion
  {

    StringBuilder sql = new StringBuilder();
    sql.append(" WITH v(i) AS ")
    .append(" (select case when v.varc_id_concepto_aplicado is not null ")
    .append(" then (select vv2.varc_valor from aseo.varc_varcalculo vv2 ")
    .append(" where vv2.varc_ideregistro = v.varc_id_concepto_aplicado) ")
    .append("else round(v.varc_valor, 4) end ")
    .append(" from aseo.varc_varcalculo v ") 
    .append(" inner join aseo.smper_semperiodo smper on v.per_ideregistro  = smper.per_ideregistro ") 
    .append(" inner join public.per_periodo pp on pp.per_ideregistro = v.per_ideregistro ") 
    .append(" where varc_ideregistro in ") 
    .append(" (select MAX(v.varc_ideregistro) from aseo.varc_varcalculo v ") 
    .append(" where v.arpr_ideregistro = :idArea and  ")
    .append(" v.emp_ideregistro = :idEmpresa and  ")
    .append(" v.con_ideregistro = :idConcepto ")
    .append(" and V.varc_estado_variacion = 'A'  ")
    .append(" group by v.varc_id_mes  ")	
    .append(" order by varc_id_mes desc) ")	
    .append(" and pp.per_fecfinal <=  ")
    .append(" (select per_fecfinal from public.per_periodo pp  ")
    .append(" where pp.per_ideregistro = :idPeriodo limit 1) ")
    .append(" order by varc_id_mes desc)  ")
    .append(" SELECT  coalesce(case WHEN SUM (CASE WHEN i = 0 THEN 1 END) > 0  ")   
    .append(" THEN 0 WHEN SUM (CASE WHEN i < 0 THEN -1 END) % 2 < 0 ")     
    .append(" THEN -1 ELSE 1  ")  
    .append(" END * EXP(SUM(LN(ABS(NULLIF(i, 0))))),0) valor FROM v; ");
    parametros.put("idConcepto", idConcepto);
    parametros.put("idEmpresa", auditoria.getIdEmpresa());
    parametros.put("idArea", idArea);
    parametros.put("idPeriodo", idPeriodo);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Double cantidad = getObject("valor", Double.class, rs);
              return cantidad;
            });
  }
}
