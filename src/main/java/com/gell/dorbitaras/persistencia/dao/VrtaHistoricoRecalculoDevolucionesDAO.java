/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.persistencia.dao.crud.VrtaHistoricoRecalculoDevolucionesCRUD;
import com.gell.dorbitaras.persistencia.dto.ConceptosCalculadosDTO;
import com.gell.dorbitaras.persistencia.dto.ConceptosDevolucionesDTO;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculoDevoluciones;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
 
/**
 *
 * @author jonat
 */
public class VrtaHistoricoRecalculoDevolucionesDAO extends VrtaHistoricoRecalculoDevolucionesCRUD {

    /**
     * Super clase.
     *
     * @param datasource Objeto de la conexión a la base de datos.
     * @param auditoria Datos prioritarios.
     * @throws PersistenciaExcepcion
     */
    public VrtaHistoricoRecalculoDevolucionesDAO(DataSource datasource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(datasource, auditoria);
    }

    /**
     * Registra una variable en la base de datos.
     *
     * @param vrtaHistoricoRecalculos Datos de la variable a insertar.
     * @throws PersistenciaExcepcion Error al insertar la información.
     */
    @Override
    public void insertar(VrtaHistoricoRecalculoDevoluciones vrtaHistoricoRecalculoDevoluciones)
            throws PersistenciaExcepcion {
        if (vrtaHistoricoRecalculoDevoluciones.getVrtaHistoricoRecalculoDevolucionesIde() != null) {
            editar(vrtaHistoricoRecalculoDevoluciones);
            return;
        }
        super.insertar(vrtaHistoricoRecalculoDevoluciones);
    }
    
    public List<VrtaHistoricoRecalculoDevoluciones> obtenerHistoricoDevoluciones(
            Integer idPeriodo
    )   throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select numero_actualizacion, max(varperreg_ide) as varperreg_ide, periodo_hijo_ide, estado, vrta_observacion, max(vrta_fecgrabacion) as vrta_fecgrabacion, max(vrta_feccertificacion) as vrta_feccertificacion ")
                .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd ")
                .append(" where periodo_hijo_ide = :idPeriodo group by numero_actualizacion, periodo_hijo_ide, estado, vrta_observacion order  by numero_actualizacion; ");
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new VrtaHistoricoRecalculoDevoluciones(
                    getObject("numero_actualizacion", Integer.class, rs),
                    getObject("varperreg_ide", Integer.class, rs),
                    getObject("periodo_hijo_ide", Integer.class, rs),
                    getObject("estado", String.class, rs),
                    getObject("vrta_observacion", String.class, rs),
                    getObject("vrta_fecgrabacion", Date.class, rs),
                    getObject("vrta_feccertificacion", Date.class, rs));
        });
    }

    public List<ConceptosDevolucionesDTO> obtenerConceptosCalculados(
            Integer idArea,
            Integer numeroActualizacion,
            Integer idPeriodo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select * ")
            .append("from  ")
            .append("(select   ")
            .append("	vv.varpr_valor as valor,  ")
            .append("	'TA_AJUSTADO' as segmento,  ")
            .append("	cc.con_abreviatura  as con_alias  ")
            .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
            .append(" inner join aseo.varpr_varperreg vv  ")
            .append(" 	on vhrd.varperreg_ide = vv.varpr_ideregistro  ")
            .append(" inner join con_concepto cc  ")
            .append(" 	on vv.con_ideregistro = cc.uni_concepto  ")
            .append(" where vhrd.numero_actualizacion = :numeroActualizacion  ")
            .append(" 	and vhrd.periodo_hijo_ide = :idPeriodo  ")
            .append(" 	and vv.arpr_ideregistro = :idArea  ")
            .append(" union all  ")
            .append(" select   ")
            .append(" 	vv.varpr_valor as valor,  ")
            .append("	'TA_INICIAL' as segmento,  ")
            .append("	cc.con_abreviatura  as con_alias  ")
            .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
            .append(" inner join aseo.varpr_varperreg vv  ")
            .append(" 	on vhrd.varperreg_ide = vv.varpr_ideregistro  ")
            .append(" inner join con_concepto cc  ")
            .append(" 	on vv.con_ideregistro = cc.uni_concepto  ")
            .append(" where vhrd.numero_actualizacion = (:numeroActualizacion - 1)  ")
            .append(" 	and vhrd.periodo_hijo_ide = :idPeriodo  ")
            .append("    and vv.arpr_ideregistro = :idArea ")
            .append(" union all ")
            .append("select   ")
            .append("	vv.varpr_valor as valor,  ")
            .append("	case when (cl.uni_liquidacion = 5568) then 'TC ENERGIA' when (cl.uni_liquidacion = 5569) then 'TC GAS' when (cl.uni_liquidacion = 5674) then 'TLU' when (cl.uni_liquidacion = 5675) then 'TBL' ")
            .append("	when (cl.uni_liquidacion = 5676) then 'TRT' when (cl.uni_liquidacion = 5677) then 'TDF' when (cl.uni_liquidacion = 5678) then 'TTL' when (cl.uni_liquidacion = 5679) then 'TA' ")
            .append("	when (cl.uni_liquidacion = 5680) then 'TFE' when (cl.uni_liquidacion = 5681) then 'TTA' when (cl.uni_liquidacion = 5893) then 'TA-DINC' when (cl.uni_liquidacion = 5894) then 'TFE-DINC' when (cl.uni_liquidacion = 5895) then 'TTA-DINC' ")
            .append("	end as segmento, ")
            .append("	cc.con_abreviatura  as con_alias  ")
            .append("from coli_conliquida cl  ")
            .append("inner join aseo.varpr_varperreg vv  ")
            .append("	on cl.uni_concepto  = vv.con_ideregistro ")
            .append("inner join aseo.vrta_historico_recalculo_devoluciones vhrd  ")
            .append("	on vhrd.varperreg_ide = vv.varpr_ideregistro ")
            .append("inner join con_concepto cc  ")
            .append("	on vv.con_ideregistro = cc.uni_concepto  ")
            .append("where uni_liquidacion in (5681, 5680, 5679, 5678, 5677, 5676, 5675, 5674, 5569, 5568) ")
            .append("	and vhrd.numero_actualizacion = :numeroActualizacion ")
            .append("	and vhrd.periodo_hijo_ide = :idPeriodo  ")
            .append("	and vv.arpr_ideregistro = :idArea ")
            .append(" ) as query_1  ")
            .append("order by segmento asc; ");
        parametros.put("idArea", idArea);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ConceptosDevolucionesDTO(
                    getObject("valor", Double.class, rs),
                    getObject("segmento", String.class, rs),
                    getObject("con_alias", String.class, rs));
        });
    }

    public List<ConceptosDevolucionesDTO> obtenerConceptosCalculadosIniciales(
            Integer idArea,
            Integer numeroActualizacion,
            Integer idPeriodo
    )
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select *  ")
            .append("from  ")
            .append("(select   ")
            .append(" vv.varpr_valor as valor,  ")
            .append(" 'TA_AJUSTADO' as segmento,  ")
            .append(" cc.con_abreviatura  as con_alias  ")
            .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
            .append(" inner join aseo.varpr_varperreg vv  ")
            .append(" on vhrd.varperreg_ide = vv.varpr_ideregistro  ")
            .append(" inner join con_concepto cc  ")
            .append(" on vv.con_ideregistro = cc.uni_concepto  ")
            .append(" where vhrd.numero_actualizacion = :numeroActualizacion  ")
            .append(" and vhrd.periodo_hijo_ide = :idPeriodo  ")
            .append(" union all  ")
            .append(" select   ")
            .append("	vv.varpr_valor as valor, ")
            .append("	'TA_INICIAL' as segmento, ")
            .append("	cc.con_abreviatura  as con_alias  ")
            .append("from aseo.varpr_varperreg vv  ")
            .append(" inner join con_concepto cc  ")
            .append("on vv.con_ideregistro = cc.uni_concepto  ")
            .append("where arpr_ideregistro = :idArea   ")
            .append("and vv.varpr_ideregistro in ( ")
            .append("	select max(varpr_ideregistro) as varpr_ideregistro ")
            .append("		from aseo.varpr_varperreg vv  ")
            .append("		where vv.per_ideregistro = :idPeriodo ")
            .append("		and vv.varpr_estado = 'CE'  ")
            .append("		and vv.varpr_estado_registro = 'I'  ")
            .append("		group by con_ideregistro ")
            .append("		order by  max(varpr_ideregistro) ")
            .append("		) ")
                        .append(" union all ")
            .append("select   ")
            .append("	vv.varpr_valor as valor,  ")
            .append("	case when (cl.uni_liquidacion = 5568) then 'TC ENERGIA' when (cl.uni_liquidacion = 5569) then 'TC GAS' when (cl.uni_liquidacion = 5674) then 'TLU' when (cl.uni_liquidacion = 5675) then 'TBL' ")
            .append("	when (cl.uni_liquidacion = 5676) then 'TRT' when (cl.uni_liquidacion = 5677) then 'TDF' when (cl.uni_liquidacion = 5678) then 'TTL' when (cl.uni_liquidacion = 5679) then 'TA' ")
            .append("	when (cl.uni_liquidacion = 5680) then 'TFE' when (cl.uni_liquidacion = 5681) then 'TTA' ")
            .append("	end as segmento, ")
            .append("	cc.con_abreviatura  as con_alias  ")
            .append("from coli_conliquida cl  ")
            .append("inner join aseo.varpr_varperreg vv  ")
            .append("	on cl.uni_concepto  = vv.con_ideregistro ")
            .append("inner join aseo.vrta_historico_recalculo_devoluciones vhrd  ")
            .append("	on vhrd.varperreg_ide = vv.varpr_ideregistro ")
            .append("inner join con_concepto cc  ")
            .append("	on vv.con_ideregistro = cc.uni_concepto  ")
            .append("where uni_liquidacion in (5681, 5680, 5679, 5678, 5677, 5676, 5675, 5674, 5569, 5568) ")
            .append("	and vhrd.numero_actualizacion = :numeroActualizacion ")
            .append("	and vhrd.periodo_hijo_ide = :idPeriodo  ")
            .append("	and vv.arpr_ideregistro = :idArea ")
            .append(" ) as query_1  ")
            .append(" order by segmento asc; ");
        parametros.put("idArea", idArea);
        parametros.put("numeroActualizacion", numeroActualizacion);
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return new ConceptosDevolucionesDTO(
                    getObject("valor", Double.class, rs),
                    getObject("segmento", String.class, rs),
                    getObject("con_alias", String.class, rs));
        });
    }
    
    public void actualizarHistorico(
            Integer idArea,
            Integer numeroActualziacion,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            String accion
    ) throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append("update aseo.vrta_historico_recalculo_devoluciones ")
            .append("set estado = :accion, ")
            .append("vrta_observacion = case when (:accion = 'A') then 'Calculo aprobado' else 'Calculo denegado' end, ")
            .append("vrta_feccertificacion = now() ")
            .append("where periodo_hijo_ide = :idPeriodo ")
            .append("and numero_actualizacion = :numeroActualizacion; ")
            .append("update aseo.vrta_historico_recalculo_devoluciones ")
            .append("set estado = :accion, ")
            .append("vrta_observacion = case when (:accion = 'A') then 'Calculo aprobado' else 'Calculo denegado' end, ")
            .append("vrta_feccertificacion = now() ")
            .append("where periodo_hijo_ide = :idPeriodoPadre ")
            .append("and numero_actualizacion = :numeroActualizacion; ");
        parametros.put("numeroActualizacion", numeroActualziacion);
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("accion", accion);
        ejecutarEdicion(sql, parametros);
    }
}
