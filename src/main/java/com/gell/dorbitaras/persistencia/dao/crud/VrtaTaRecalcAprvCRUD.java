/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaTaRecalcAprvCRUD extends GenericoCRUD {

    public VrtaTaRecalcAprvCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }

    public void insertar(VrtaTaRecalcAprv vrtaTaRecalcAprv)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_ta_recalc_aprv(numero_actualizacion, periodo_recalculo,valor,fecha_registro,usuario_registro,con_dinc) VALUES (?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object numeroActualizacion = (vrtaTaRecalcAprv.getNumeroActualizacion() == null) ? null : vrtaTaRecalcAprv.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object periodoRecalculo = (vrtaTaRecalcAprv.getPeriodoRecalculo() == null) ? null : vrtaTaRecalcAprv.getPeriodoRecalculo();
            sentencia.setObject(i++, periodoRecalculo);
            Object valor = (vrtaTaRecalcAprv.getValor()== null) ? null : vrtaTaRecalcAprv.getValor();
            sentencia.setObject(i++, valor);
            Object fechaRegistro = (vrtaTaRecalcAprv.getFechaRegistro() == null) ? null : vrtaTaRecalcAprv.getFechaRegistro();
            sentencia.setObject(i++, fechaRegistro);
            Object usuarioRegistro = (vrtaTaRecalcAprv.getUsuarioRegistro() == null) ? null : vrtaTaRecalcAprv.getUsuarioRegistro();
            sentencia.setObject(i++, usuarioRegistro);
            Object conDinc = (vrtaTaRecalcAprv.isConDinc() == null) ? null : vrtaTaRecalcAprv.isConDinc();
            sentencia.setObject(i++, conDinc);
            sentencia.executeUpdate();
            /*ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
              vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }

    public static VrtaTaRecalcAprv getVrtaTaRecalcAprv(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaTaRecalcAprv vrtaTaRecalcAprv = new VrtaTaRecalcAprv();
        vrtaTaRecalcAprv.setVrtaTaRecalcIdeRegistro(getObject("vrta_ta_recalc_ideregistro", Integer.class, rs));
        vrtaTaRecalcAprv.setNumeroActualizacion(getObject("numero_actualizacion", Integer.class, rs));
        vrtaTaRecalcAprv.setPeriodoRecalculo(getObject("periodo_recalculo", Integer.class, rs));
        vrtaTaRecalcAprv.setValor(getObject("valor", Double.class, rs));
        vrtaTaRecalcAprv.setFechaRegistro(getObject("fecha_registro", Timestamp.class, rs));
        vrtaTaRecalcAprv.setUsuarioRegistro(getObject("usuario_registro", Integer.class, rs));
        return vrtaTaRecalcAprv;
    }
    
 
}
