/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.ArprAreaprestacion;
import com.gell.dorbitaras.persistencia.entidades.ConConcepto;
import com.gell.dorbitaras.persistencia.entidades.PerPeriodo;
import com.gell.dorbitaras.persistencia.entidades.RutRuta;
import com.gell.dorbitaras.persistencia.entidades.UniUnidad;
import com.gell.dorbitaras.persistencia.entidades.VrmrVarmicroruta;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import javax.sql.DataSource;

/**
 *
 * @author jcpacheco
 */
public class VrmrVarmicrorutaCRUD extends GenericoCRUD{
    
    public VrmrVarmicrorutaCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }
    
    public void insertar(VrmrVarmicroruta vrmrVarmicroruta)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrmr_varmicroruta(rut_idemicroruta, per_ideregistro, con_ideregistro, arpr_ideregistro, vrmr_valor, vrmr_descripcion, vrmr_estado, usu_ideregistro_gb, vrmr_feccerficicacion, vrmr_fecgrabacion, usu_ideregistro_cer, emp_ideregistro, vrmr_estadoregistro) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Object rutIderegistro = (vrmrVarmicroruta.getRutIdemicroruta().getRutIderegistro() == null) ? null : vrmrVarmicroruta.getRutIdemicroruta().getRutIderegistro();
            sentencia.setObject(i++, rutIderegistro);
            Object perIderegistro = (vrmrVarmicroruta.getPerIderegistro() == null) ? null : vrmrVarmicroruta.getPerIderegistro().getPerIderegistro();
            sentencia.setObject(i++, perIderegistro);
            Object conIderegistro = (vrmrVarmicroruta.getConIderegistro() == null) ? null : vrmrVarmicroruta.getConIderegistro().getUniConcepto().getUniIderegistro();
            sentencia.setObject(i++, conIderegistro);
            Object arprIderegistro = (vrmrVarmicroruta.getArprIderegistro() == null) ? null : vrmrVarmicroruta.getArprIderegistro().getArprIderegistro();
            sentencia.setObject(i++, arprIderegistro);
            sentencia.setObject(i++, vrmrVarmicroruta.getVrmrValor());
            sentencia.setObject(i++, vrmrVarmicroruta.getVrmrDescripcion());
            sentencia.setObject(i++, vrmrVarmicroruta.getVrmrEstado());
            sentencia.setObject(i++, vrmrVarmicroruta.getUsuIderegistrogb());
            sentencia.setObject(i++, DateUtil.parseTimestamp(vrmrVarmicroruta.getVrmrFeccerficicacion()));
            sentencia.setObject(i++, DateUtil.parseTimestamp(vrmrVarmicroruta.getVrmrFecgrabacion()));
            sentencia.setObject(i++, vrmrVarmicroruta.getUsuIderegistrocer());            
            sentencia.setObject(i++, vrmrVarmicroruta.getEmpIderegistro());
            sentencia.setObject(i++, vrmrVarmicroruta.getVrmrEstadoregistro());
            sentencia.executeUpdate();
            ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
                vrmrVarmicroruta.setVrmrIderegistro(rs.getInt("vrmr_ideregistro"));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }
    
    public static VrmrVarmicroruta getVrmrVarmicroruta(ResultSet rs)
          throws PersistenciaExcepcion
  {
    VrmrVarmicroruta vrmrVarmicroruta = new VrmrVarmicroruta();
    vrmrVarmicroruta.setVrmrIderegistro(getObject("vrmr_ideregistro", Integer.class, rs));
    PerPeriodo per_ideregistro = new PerPeriodo();
    per_ideregistro.setPerIderegistro(getObject("per_ideregistro", Integer.class, rs));
    vrmrVarmicroruta.setPerIderegistro(per_ideregistro);
    RutRuta rutIdemicroruta = new RutRuta();
    rutIdemicroruta.setRutIderegistro(getObject("rut_idemicroruta", Integer.class, rs));
    rutIdemicroruta.setRutNombre(getObject("rut_nombre", String.class, rs));
    vrmrVarmicroruta.setRutIdemicroruta(rutIdemicroruta);
    ConConcepto con_ideregistro = new ConConcepto();
    con_ideregistro.setUniConcepto(new UniUnidad(getObject("con_ideregistro", Integer.class, rs)));
    vrmrVarmicroruta.setConIderegistro(con_ideregistro);
    ArprAreaprestacion arpr_ideregistro = new ArprAreaprestacion();
    arpr_ideregistro.setArprIderegistro(getObject("arpr_ideregistro", Integer.class, rs));
    vrmrVarmicroruta.setArprIderegistro(arpr_ideregistro);
    vrmrVarmicroruta.setUsuIderegistrogb(getObject("usu_ideregistro_gb", Integer.class, rs));
    vrmrVarmicroruta.setVrmrFeccerficicacion(getObject("vrmr_feccerficicacion", Timestamp.class, rs));
    vrmrVarmicroruta.setVrmrFecgrabacion(getObject("vrmr_fecgrabacion", Timestamp.class, rs));
    vrmrVarmicroruta.setUsuIderegistrocer(getObject("usu_ideregistro_cer", Integer.class, rs));
    vrmrVarmicroruta.setVrmrValor(getObject("vrmr_valor", Double.class, rs));
    vrmrVarmicroruta.setVrmrDescripcion(getObject("vrmr_valor", String.class, rs));
    vrmrVarmicroruta.setVrmrEstado(getObject("vrmr_estado", String.class, rs));
    vrmrVarmicroruta.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));
    vrmrVarmicroruta.setVrmrEstadoregistro(getObject("vrmr_estadoregistro", String.class, rs));

    return vrmrVarmicroruta;
  }
    
}
