package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import static com.gell.dorbitaras.negocio.util.AuditoriaUtil.auditoria;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.EttaEstttarifasCRUD;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import java.sql.ResultSet;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class EttaEstttarifasDAO extends EttaEstttarifasCRUD
{

  public EttaEstttarifasDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   *
   * Método para la inserción y edición de datos
   *
   * @param estrato Informacíon de estrado área presentación
   * @throws PersistenciaExcepcion
   */
  @Override
  public void insertar(EttaEstttarifas estrato)
          throws PersistenciaExcepcion
  {
    Integer ideRegistro = estrato.getEttaIderegistro();
    ideRegistro = ideRegistro == null ? -1 : ideRegistro;
    EttaEstttarifas documentoGuardada = consultar(ideRegistro.longValue());
    if (documentoGuardada != null) {
      estrato.setUsuIderegistroAct(auditoria().getIdUsuario())
              .setEttaFecact(new Date())
              .setEttaFecgrabacion(documentoGuardada.getEttaFecgrabacion());
      this.editar(estrato);
      return;
    }
    estrato.setEttaSwtestado(EEstadoGenerico.ACTIVO);
    estrato.setEttaFecgrabacion(new Date());
    super.insertar(estrato);
  }

  /**
   * Consulta todos los estratos
   *
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<EttaEstttarifas> consultarEstrato()
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT * FROM aseo.etta_estttarifas")
            .append(" WHERE etta_swtestado = :activo; ");
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<EttaEstttarifas>()
    {
      @Override
      public EttaEstttarifas siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        EttaEstttarifas estratos = EttaEstttarifasDAO.getEttaEstttarifas(rs, columns, "etta_estttarifas1");
        return estratos;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }
}
