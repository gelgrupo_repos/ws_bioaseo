package com.gell.dorbitaras.persistencia.dao;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.dao.crud.*;
import com.gell.dorbitaras.persistencia.entidades.RgtaRgitarifario;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.ConsultaAdaptador;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.FuncionesDatoUtil;
import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

public class RgtaRgitarifarioDAO extends RgtaRgitarifarioCRUD
{

  public RgtaRgitarifarioDAO(DataSource datasource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(datasource, auditoria);
  }

  /**
   * Método que expone el método de guardarRegimen y si se envía el
   * identificador del régimen se edita
   *
   * @param regimen Información el registro
   * @throws com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion
   * @throws com.gell.dorbitaras.negocio.excepcion.NegocioExcepcion
   */
  public void guardarRegimen(RgtaRgitarifario regimen)
          throws PersistenciaExcepcion, NegocioExcepcion
  {
    Integer rgtaIdederegistro = regimen.getRgtaIderegistro();
    rgtaIdederegistro = rgtaIdederegistro == null ? -1 : rgtaIdederegistro;
    RgtaRgitarifario regimenGuardada = consultar(rgtaIdederegistro.longValue());
    if (regimenGuardada != null) {
      this.editar(regimen);
      return;
    }
    super.insertar(regimen);
  }

  /**
   * Consulta todas las unidades dependiendo el parametro de busqueda
   *
   * @param nombre parametro de búsqueda
   * @return Lista de las unidades dependiendo de la estructura
   * @throws PersistenciaExcepcion Error al ejecutar la sentencia
   */
  public List<RgtaRgitarifario> consultar(String nombre)
          throws PersistenciaExcepcion
  {
    StringBuilder sql = new StringBuilder();
    sql.append(" SELECT rgta.*, ")
            .append("       to_json(( ")
            .append("         SELECT ARRAY_AGG(info.*) ")
            .append("         FROM (SELECT * ")
            .append("               FROM aseo.dcta_dctotarifas dcta ")
            .append("                      INNER JOIN ter_tercero ter on dcta.ter_ideregistro = ter.ter_ideregistro ")
            .append("               WHERE dcta.rgta_ideregistro = rgta.rgta_ideregistro ")
            .append("                 AND dcta.dcta_estado = :activo) as info ")
            .append("       )) documentos ")
            .append("FROM aseo.rgta_rgitarifario rgta ")
            .append("WHERE rgta.emp_ideregistro = :ideEmpresa ")
            .append("  AND rgta.rgta_nombre ILIKE :nombre;");
    parametros.put("nombre", '%' + nombre + '%');
    parametros.put("activo", EEstadoGenerico.ACTIVO);
    parametros.put("ideEmpresa", auditoria.getIdEmpresa());
    return ejecutarConsulta(sql, parametros, new ConsultaAdaptador<RgtaRgitarifario>()
    {
      @Override
      public RgtaRgitarifario siguiente(ResultSet rs, Map<String, Integer> columns)
              throws PersistenciaExcepcion
      {
        RgtaRgitarifario regimen = getRgtaRgitarifario(rs, columns, "rgta_rgitarifario1");
        String documentos = getObject("documentos", String.class, rs);
        if (documentos != null) {
          documentos = FuncionesDatoUtil.deSeparadoPorGuionesACamelCase(documentos);
        }
        regimen.setInfo(documentos);
        return regimen;
      }

      @Override
      public void sinResultados()
              throws PersistenciaExcepcion
      {
        throw new PersistenciaExcepcion(EMensajePersistencia.NO_RESULTADOS);
      }
    });
  }

  /**
   * Consulta si el nombre ya existe en la base de datos
   *
   * @param nombre Nombre del área de prestación
   * @param idRegistro Identificador del registro
   * @return Cantidad de registros
   * @throws PersistenciaExcepcion
   */
  public Integer validarNombre(String nombre, Integer idRegistro)
          throws PersistenciaExcepcion
  {
    idRegistro = idRegistro == null ? -1 : idRegistro;
    StringBuilder sql = new StringBuilder();
    sql.append("SELECT count(*) existente ")
            .append("FROM aseo.rgta_rgitarifario rgt ")
            .append("WHERE rgt.rgta_nombre ILIKE :nombre ")
            .append("  AND rgt.emp_ideregistro = :idempresa ")
            .append("  AND rgt.rgta_ideregistro <> :idregistro;");
    parametros.put("nombre", nombre);
    parametros.put("idempresa", auditoria.getIdEmpresa());
    parametros.put("idregistro", idRegistro);
    return ejecutarConsultaSimple(sql, parametros,
            (ResultSet rs, Map<String, Integer> columns) -> {
              Integer existente = getObject("existente", Integer.class, rs);
              return existente;
            });
  }

}
