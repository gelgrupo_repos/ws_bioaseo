package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.sql.Time;
import java.util.Date;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class RgtaRgitarifarioCRUD extends GenericoCRUD {

  public RgtaRgitarifarioCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(RgtaRgitarifario rgtaRgitarifario)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.rgta_rgitarifario(rgta_nombre,rgta_descripcion,rgta_fecinivigencia,rgta_fecfinvigencia,emp_ideregistro) VALUES (?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaNombre());
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaDescripcion());
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecinivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecfinvigencia()));
      sentencia.setObject(i++, rgtaRgitarifario.getEmpIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        rgtaRgitarifario.setRgtaIderegistro(rs.getInt("rgta_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(RgtaRgitarifario rgtaRgitarifario)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.rgta_rgitarifario(rgta_ideregistro,rgta_nombre,rgta_descripcion,rgta_fecinivigencia,rgta_fecfinvigencia,emp_ideregistro) VALUES (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaIderegistro());
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaNombre());
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaDescripcion());
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecinivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecfinvigencia()));
      sentencia.setObject(i++, rgtaRgitarifario.getEmpIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(RgtaRgitarifario rgtaRgitarifario)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.rgta_rgitarifario SET rgta_nombre=?,rgta_descripcion=?,rgta_fecinivigencia=?,rgta_fecfinvigencia=?,emp_ideregistro=? where rgta_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaNombre());
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaDescripcion());
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecinivigencia()));
      sentencia.setObject(i++, DateUtil.parseTimestamp(rgtaRgitarifario.getRgtaFecfinvigencia()));
      sentencia.setObject(i++, rgtaRgitarifario.getEmpIderegistro());
      sentencia.setObject(i++, rgtaRgitarifario.getRgtaIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<RgtaRgitarifario> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<RgtaRgitarifario> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.rgta_rgitarifario";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getRgtaRgitarifario(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public RgtaRgitarifario consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    RgtaRgitarifario obj = null;
    try {

      String sql = "SELECT * FROM aseo.rgta_rgitarifario WHERE rgta_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getRgtaRgitarifario(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static RgtaRgitarifario getRgtaRgitarifario(ResultSet rs)
          throws PersistenciaExcepcion
  {
    RgtaRgitarifario rgtaRgitarifario = new RgtaRgitarifario();
    rgtaRgitarifario.setRgtaIderegistro(getObject("rgta_ideregistro", Integer.class, rs));
    rgtaRgitarifario.setRgtaNombre(getObject("rgta_nombre", String.class, rs));
    rgtaRgitarifario.setRgtaDescripcion(getObject("rgta_descripcion", String.class, rs));
    rgtaRgitarifario.setRgtaFecinivigencia(getObject("rgta_fecinivigencia", Timestamp.class, rs));
    rgtaRgitarifario.setRgtaFecfinvigencia(getObject("rgta_fecfinvigencia", Timestamp.class, rs));
    rgtaRgitarifario.setEmpIderegistro(getObject("emp_ideregistro", Integer.class, rs));

    return rgtaRgitarifario;
  }

//  public static void getRgtaRgitarifario(ResultSet rs, Map<String, Integer> columnas, RgtaRgitarifario rgtaRgitarifario)
//          throws PersistenciaExcepcion
//  {
//    Integer columna = columnas.get("rgta_rgitarifario_rgta_ideregistro");
//    if (columna != null) {
//      rgtaRgitarifario.setRgtaIderegistro(getObject(columna, Integer.class, rs));
//    }
//    columna = columnas.get("rgta_rgitarifario_rgta_nombre");
//    if (columna != null) {
//      rgtaRgitarifario.setRgtaNombre(getObject(columna, String.class, rs));
//    }
//    columna = columnas.get("rgta_rgitarifario_rgta_descripcion");
//    if (columna != null) {
//      rgtaRgitarifario.setRgtaDescripcion(getObject(columna, String.class, rs));
//    }
//    columna = columnas.get("rgta_rgitarifario_rgta_fecinivigencia");
//    if (columna != null) {
//      rgtaRgitarifario.setRgtaFecinivigencia(getObject(columna, Timestamp.class, rs));
//    }
//    columna = columnas.get("rgta_rgitarifario_rgta_fecfinvigencia");
//    if (columna != null) {
//      rgtaRgitarifario.setRgtaFecfinvigencia(getObject(columna, Timestamp.class, rs));
//    }
//    columna = columnas.get("rgta_rgitarifario_emp_ideregistro");
//    if (columna != null) {
//      rgtaRgitarifario.setEmpIderegistro(getObject(columna, Integer.class, rs));
//    }
//  }

  public static void getRgtaRgitarifario(ResultSet rs, Map<String, Integer> columnas, RgtaRgitarifario rgtaRgitarifario, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_rgta_ideregistro");
    if (columna != null) {
      rgtaRgitarifario.setRgtaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_rgta_nombre");
    if (columna != null) {
      rgtaRgitarifario.setRgtaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_rgta_descripcion");
    if (columna != null) {
      rgtaRgitarifario.setRgtaDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_rgta_fecinivigencia");
    if (columna != null) {
      rgtaRgitarifario.setRgtaFecinivigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_rgta_fecfinvigencia");
    if (columna != null) {
      rgtaRgitarifario.setRgtaFecfinvigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_emp_ideregistro");
    if (columna != null) {
      rgtaRgitarifario.setEmpIderegistro(getObject(columna, Integer.class, rs));
    }
  }

//  public static RgtaRgitarifario getRgtaRgitarifario(ResultSet rs, Map<String, Integer> columnas)
//          throws PersistenciaExcepcion
//  {
//    RgtaRgitarifario rgtaRgitarifario = new RgtaRgitarifario();
//    getRgtaRgitarifario(rs, columnas, rgtaRgitarifario);
//    return rgtaRgitarifario;
//  }

  public static RgtaRgitarifario getRgtaRgitarifario(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    RgtaRgitarifario rgtaRgitarifario = new RgtaRgitarifario();
    getRgtaRgitarifario(rs, columnas, rgtaRgitarifario, alias);
    return rgtaRgitarifario;
  }

}
