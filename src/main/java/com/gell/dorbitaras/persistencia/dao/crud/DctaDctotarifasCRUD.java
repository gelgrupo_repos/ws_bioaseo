package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.sql.Timestamp;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.util.PreparedStatementNamed;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class DctaDctotarifasCRUD extends GenericoCRUD
{

  public DctaDctotarifasCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(DctaDctotarifas dctaDctotarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      String sql = "INSERT INTO aseo.dcta_dctotarifas(ter_ideregistro,rgta_ideregistro,dcta_nombre,dcta_descripcion,dcta_fecinivigencia,dcta_fecfinvigencia,uni_idetipodocumento,dcta_numero,dcta_fecemision,dcta_fecpublicacion,dcta_urldocumento,dcta_estado) VALUES (:ter_ideregistro,:rgta_ideregistro,:dcta_nombre,:dcta_descripcion,:dcta_fecinivigencia,:dcta_fecfinvigencia,:uni_idetipodocumento,:dcta_numero,:dcta_fecemision,:dcta_fecpublicacion,:dcta_urldocumento,:dcta_estado)";
      sentencia = new PreparedStatementNamed(cnn, sql, true);
      Object terIderegistro = (dctaDctotarifas.getTerIderegistro() == null) ? null : dctaDctotarifas.getTerIderegistro().getTerIderegistro();
      sentencia.setObject("ter_ideregistro", terIderegistro);
      Object rgtaIderegistro = (dctaDctotarifas.getRgtaIderegistro() == null) ? null : dctaDctotarifas.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      sentencia.setObject("dcta_nombre", dctaDctotarifas.getDctaNombre());
      sentencia.setObject("dcta_descripcion", dctaDctotarifas.getDctaDescripcion());
      sentencia.setObject("dcta_fecinivigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecinivigencia()));
      sentencia.setObject("dcta_fecfinvigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecfinvigencia()));
      Object uniIdetipodocumento = (dctaDctotarifas.getUniIdetipodocumento() == null) ? null : dctaDctotarifas.getUniIdetipodocumento().getUniIderegistro();
      sentencia.setObject("uni_idetipodocumento", uniIdetipodocumento);
      sentencia.setObject("dcta_numero", dctaDctotarifas.getDctaNumero());
      sentencia.setObject("dcta_fecemision", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecemision()));
      sentencia.setObject("dcta_fecpublicacion", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecpublicacion()));
      sentencia.setObject("dcta_urldocumento", dctaDctotarifas.getDctaUrldocumento());
      sentencia.setObject("dcta_estado", dctaDctotarifas.getDctaEstado());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        dctaDctotarifas.setDctaIderegistro(rs.getInt("dcta_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(DctaDctotarifas dctaDctotarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.dcta_dctotarifas(dcta_ideregistro,ter_ideregistro,rgta_ideregistro,dcta_nombre,dcta_descripcion,dcta_fecinivigencia,dcta_fecfinvigencia,uni_idetipodocumento,dcta_numero,dcta_fecemision,dcta_fecpublicacion,dcta_urldocumento,dcta_estado) VALUES (:dcta_ideregistro,:ter_ideregistro,:rgta_ideregistro,:dcta_nombre,:dcta_descripcion,:dcta_fecinivigencia,:dcta_fecfinvigencia,:uni_idetipodocumento,:dcta_numero,:dcta_fecemision,:dcta_fecpublicacion,:dcta_urldocumento,:dcta_estado)";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("dcta_ideregistro", dctaDctotarifas.getDctaIderegistro());
      Object terIderegistro = (dctaDctotarifas.getTerIderegistro() == null) ? null : dctaDctotarifas.getTerIderegistro().getTerIderegistro();
      sentencia.setObject("ter_ideregistro", terIderegistro);
      Object rgtaIderegistro = (dctaDctotarifas.getRgtaIderegistro() == null) ? null : dctaDctotarifas.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      sentencia.setObject("dcta_nombre", dctaDctotarifas.getDctaNombre());
      sentencia.setObject("dcta_descripcion", dctaDctotarifas.getDctaDescripcion());
      sentencia.setObject("dcta_fecinivigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecinivigencia()));
      sentencia.setObject("dcta_fecfinvigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecfinvigencia()));
      Object uniIdetipodocumento = (dctaDctotarifas.getUniIdetipodocumento() == null) ? null : dctaDctotarifas.getUniIdetipodocumento().getUniIderegistro();
      sentencia.setObject("uni_idetipodocumento", uniIdetipodocumento);
      sentencia.setObject("dcta_numero", dctaDctotarifas.getDctaNumero());
      sentencia.setObject("dcta_fecemision", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecemision()));
      sentencia.setObject("dcta_fecpublicacion", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecpublicacion()));
      sentencia.setObject("dcta_urldocumento", dctaDctotarifas.getDctaUrldocumento());
      sentencia.setObject("dcta_estado", dctaDctotarifas.getDctaEstado());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(DctaDctotarifas dctaDctotarifas)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.dcta_dctotarifas SET ter_ideregistro=:ter_ideregistro,rgta_ideregistro=:rgta_ideregistro,dcta_nombre=:dcta_nombre,dcta_descripcion=:dcta_descripcion,dcta_fecinivigencia=:dcta_fecinivigencia,dcta_fecfinvigencia=:dcta_fecfinvigencia,uni_idetipodocumento=:uni_idetipodocumento,dcta_numero=:dcta_numero,dcta_fecemision=:dcta_fecemision,dcta_fecpublicacion=:dcta_fecpublicacion,dcta_urldocumento=:dcta_urldocumento,dcta_estado=:dcta_estado where dcta_ideregistro = :dcta_ideregistro ";
      sentencia = new PreparedStatementNamed(cnn, sql);
      Object terIderegistro = (dctaDctotarifas.getTerIderegistro() == null) ? null : dctaDctotarifas.getTerIderegistro().getTerIderegistro();
      sentencia.setObject("ter_ideregistro", terIderegistro);
      Object rgtaIderegistro = (dctaDctotarifas.getRgtaIderegistro() == null) ? null : dctaDctotarifas.getRgtaIderegistro().getRgtaIderegistro();
      sentencia.setObject("rgta_ideregistro", rgtaIderegistro);
      sentencia.setObject("dcta_nombre", dctaDctotarifas.getDctaNombre());
      sentencia.setObject("dcta_descripcion", dctaDctotarifas.getDctaDescripcion());
      sentencia.setObject("dcta_fecinivigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecinivigencia()));
      sentencia.setObject("dcta_fecfinvigencia", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecfinvigencia()));
      Object uniIdetipodocumento = (dctaDctotarifas.getUniIdetipodocumento() == null) ? null : dctaDctotarifas.getUniIdetipodocumento().getUniIderegistro();
      sentencia.setObject("uni_idetipodocumento", uniIdetipodocumento);
      sentencia.setObject("dcta_numero", dctaDctotarifas.getDctaNumero());
      sentencia.setObject("dcta_fecemision", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecemision()));
      sentencia.setObject("dcta_fecpublicacion", DateUtil.parseTimestamp(dctaDctotarifas.getDctaFecpublicacion()));
      sentencia.setObject("dcta_urldocumento", dctaDctotarifas.getDctaUrldocumento());
      sentencia.setObject("dcta_estado", dctaDctotarifas.getDctaEstado());
      sentencia.setObject("dcta_ideregistro", dctaDctotarifas.getDctaIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<DctaDctotarifas> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    List<DctaDctotarifas> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.dcta_dctotarifas";
      sentencia = new PreparedStatementNamed(cnn, sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getDctaDctotarifas(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public DctaDctotarifas consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatementNamed sentencia = null;
    DctaDctotarifas obj = null;
    try {

      String sql = "SELECT * FROM aseo.dcta_dctotarifas WHERE dcta_ideregistro= :id";
      sentencia = new PreparedStatementNamed(cnn, sql);
      sentencia.setObject("id", id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getDctaDctotarifas(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static DctaDctotarifas getDctaDctotarifas(ResultSet rs)
          throws PersistenciaExcepcion
  {
    DctaDctotarifas dctaDctotarifas = new DctaDctotarifas();
    dctaDctotarifas.setDctaIderegistro(getObject("dcta_ideregistro", Integer.class, rs));
    TerTercero ter_ideregistro = new TerTercero();
    ter_ideregistro.setTerIderegistro(getObject("ter_ideregistro", Long.class, rs));
    dctaDctotarifas.setTerIderegistro(ter_ideregistro);
    RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
    rgta_ideregistro.setRgtaIderegistro(getObject("rgta_ideregistro", Integer.class, rs));
    dctaDctotarifas.setRgtaIderegistro(rgta_ideregistro);
    dctaDctotarifas.setDctaNombre(getObject("dcta_nombre", String.class, rs));
    dctaDctotarifas.setDctaDescripcion(getObject("dcta_descripcion", String.class, rs));
    dctaDctotarifas.setDctaFecinivigencia(getObject("dcta_fecinivigencia", Timestamp.class, rs));
    dctaDctotarifas.setDctaFecfinvigencia(getObject("dcta_fecfinvigencia", Timestamp.class, rs));
    UniUnidad uni_idetipodocumento = new UniUnidad();
    uni_idetipodocumento.setUniIderegistro(getObject("uni_idetipodocumento", Integer.class, rs));
    dctaDctotarifas.setUniIdetipodocumento(uni_idetipodocumento);
    dctaDctotarifas.setDctaNumero(getObject("dcta_numero", String.class, rs));
    dctaDctotarifas.setDctaFecemision(getObject("dcta_fecemision", Timestamp.class, rs));
    dctaDctotarifas.setDctaFecpublicacion(getObject("dcta_fecpublicacion", Timestamp.class, rs));
    dctaDctotarifas.setDctaUrldocumento(getObject("dcta_urldocumento", String.class, rs));
    dctaDctotarifas.setDctaEstado(getObject("dcta_estado", String.class, rs));

    return dctaDctotarifas;
  }

  public static DctaDctotarifas getDctaDctotarifas(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    DctaDctotarifas dctaDctotarifas = new DctaDctotarifas();
    Integer columna = columnas.get(alias + "_dcta_ideregistro");
    if (columna != null) {
      dctaDctotarifas.setDctaIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_ter_ideregistro");
    if (columna != null) {
      TerTercero ter_ideregistro = new TerTercero();
      ter_ideregistro.setTerIderegistro(getObject(columna, Long.class, rs));
      dctaDctotarifas.setTerIderegistro(ter_ideregistro);
    }
    columna = columnas.get(alias + "_rgta_ideregistro");
    if (columna != null) {
      RgtaRgitarifario rgta_ideregistro = new RgtaRgitarifario();
      rgta_ideregistro.setRgtaIderegistro(getObject(columna, Integer.class, rs));
      dctaDctotarifas.setRgtaIderegistro(rgta_ideregistro);
    }
    columna = columnas.get(alias + "_dcta_nombre");
    if (columna != null) {
      dctaDctotarifas.setDctaNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_dcta_descripcion");
    if (columna != null) {
      dctaDctotarifas.setDctaDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_dcta_fecinivigencia");
    if (columna != null) {
      dctaDctotarifas.setDctaFecinivigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_dcta_fecfinvigencia");
    if (columna != null) {
      dctaDctotarifas.setDctaFecfinvigencia(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_uni_idetipodocumento");
    if (columna != null) {
      UniUnidad uni_idetipodocumento = new UniUnidad();
      uni_idetipodocumento.setUniIderegistro(getObject(columna, Integer.class, rs));
      dctaDctotarifas.setUniIdetipodocumento(uni_idetipodocumento);
    }
    columna = columnas.get(alias + "_dcta_numero");
    if (columna != null) {
      dctaDctotarifas.setDctaNumero(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_dcta_fecemision");
    if (columna != null) {
      dctaDctotarifas.setDctaFecemision(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_dcta_fecpublicacion");
    if (columna != null) {
      dctaDctotarifas.setDctaFecpublicacion(getObject(columna, Timestamp.class, rs));
    }
    columna = columnas.get(alias + "_dcta_urldocumento");
    if (columna != null) {
      dctaDctotarifas.setDctaUrldocumento(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_dcta_estado");
    if (columna != null) {
      dctaDctotarifas.setDctaEstado(getObject(columna, String.class, rs));
    }
    return dctaDctotarifas;
  }

}
