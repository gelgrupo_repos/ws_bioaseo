package com.gell.dorbitaras.persistencia.dao.crud;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.dorbitaras.persistencia.entidades.*;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;

public class RutRutaCRUD extends GenericoCRUD
{

  public RutRutaCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public void insertar(RutRuta rutRuta)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO rut_ruta(rut_nombre,uni_tiporuta,rut_tipo,cic_ideregistro, rut_codigo, usu_ideregistro) VALUES (?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, rutRuta.getRutNombre());
      Object uniTiporuta = (rutRuta.getUniTiporuta() == null) ? null : rutRuta.getUniTiporuta();
      sentencia.setObject(i++, uniTiporuta);
      sentencia.setObject(i++, rutRuta.getRutTipo());
      Object cicIderegistro = (rutRuta.getCicIderegistro() == null) ? null : rutRuta.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
       sentencia.setObject(i++, rutRuta.getRutCodigo());
      sentencia.setObject(i++, rutRuta.getUsuIderegistro());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        rutRuta.setRutIderegistro(rs.getInt("rut_ideregistro"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(RutRuta rutRuta)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO rut_ruta(rut_ideregistro,rut_nombre,uni_tiporuta,rut_tipo,cic_ideregistro,usu_ideregistro) VALUES (?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, rutRuta.getRutIderegistro());
      sentencia.setObject(i++, rutRuta.getRutNombre());
      Object uniTiporuta = (rutRuta.getUniTiporuta() == null) ? null : rutRuta.getUniTiporuta();
      sentencia.setObject(i++, uniTiporuta);
      sentencia.setObject(i++, rutRuta.getRutTipo());
      Object cicIderegistro = (rutRuta.getCicIderegistro() == null) ? null : rutRuta.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
      sentencia.setObject(i++, rutRuta.getUsuIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(RutRuta rutRuta)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE rut_ruta SET rut_nombre=?,uni_tiporuta=?,rut_tipo=?,cic_ideregistro=?,rut_codigo=?,usu_ideregistro=? where rut_ideregistro=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, rutRuta.getRutNombre());
      Object uniTiporuta = (rutRuta.getUniTiporuta() == null) ? null : rutRuta.getUniTiporuta();
      sentencia.setObject(i++, uniTiporuta);
      sentencia.setObject(i++, rutRuta.getRutTipo());
      Object cicIderegistro = (rutRuta.getCicIderegistro() == null) ? null : rutRuta.getCicIderegistro().getCicIderegistro();
      sentencia.setObject(i++, cicIderegistro);
      sentencia.setObject(i++, rutRuta.getRutCodigo());
      sentencia.setObject(i++, rutRuta.getUsuIderegistro());
      sentencia.setObject(i++, rutRuta.getRutIderegistro());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<RutRuta> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<RutRuta> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM rut_ruta";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getRutRuta(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public RutRuta consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    RutRuta obj = null;
    try {

      String sql = "SELECT * FROM rut_ruta WHERE rut_ideregistro=?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getRutRuta(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static RutRuta getRutRuta(ResultSet rs)
          throws PersistenciaExcepcion
  {
    RutRuta rutRuta = new RutRuta();
    rutRuta.setRutIderegistro(getObject("rut_ideregistro", Integer.class, rs));
    rutRuta.setRutNombre(getObject("rut_nombre", String.class, rs));
    rutRuta.setUniTiporuta(getObject("uni_tiporuta", Integer.class, rs));
    rutRuta.setRutTipo(getObject("rut_tipo", String.class, rs));
    CicCiclo cic_ideregistro = new CicCiclo();
    cic_ideregistro.setCicIderegistro(getObject("cic_ideregistro", Integer.class, rs));
    rutRuta.setCicIderegistro(cic_ideregistro);
    rutRuta.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));

    return rutRuta;
  }

  public static void getRutRuta(ResultSet rs, Map<String, Integer> columnas, RutRuta rutRuta)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get("rut_ruta_rut_ideregistro");
    if (columna != null) {
      rutRuta.setRutIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get("rut_ruta_rut_nombre");
    if (columna != null) {
      rutRuta.setRutNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get("rut_ruta_rut_codigo");
    if (columna != null) {
      rutRuta.setRutCodigo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("rut_ruta_uni_tiporuta");
    if (columna != null) {
      rutRuta.setUniTiporuta(getObject("uni_tiporuta", Integer.class, rs));
    }
    columna = columnas.get("rut_ruta_rut_tipo");
    if (columna != null) {
      rutRuta.setRutTipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get("rut_ruta_cic_ideregistro");
    if (columna != null) {
      CicCiclo cic_ideregistro = new CicCiclo();
      cic_ideregistro.setCicIderegistro(getObject(columna, Integer.class, rs));
      rutRuta.setCicIderegistro(cic_ideregistro);
    }
    columna = columnas.get("rut_ruta_usu_ideregistro");
    if (columna != null) {
      rutRuta.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static void getRutRuta(ResultSet rs, Map<String, Integer> columnas, RutRuta rutRuta, String alias)
          throws PersistenciaExcepcion
  {
    Integer columna = columnas.get(alias + "_rut_ideregistro");
    if (columna != null) {
      rutRuta.setRutIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_rut_nombre");
    if (columna != null) {
      rutRuta.setRutNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_rut_codigo");
    if (columna != null) {
      rutRuta.setRutCodigo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_rut_tipo");
    if (columna != null) {
      rutRuta.setRutTipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      rutRuta.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
  }

  public static RutRuta getRutRuta(ResultSet rs, Map<String, Integer> columnas)
          throws PersistenciaExcepcion
  {
    RutRuta rutRuta = new RutRuta();
    getRutRuta(rs, columnas, rutRuta);
    return rutRuta;
  }

  public static RutRuta getRutRuta(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    RutRuta rutRuta = new RutRuta();
    getRutRuta(rs, columnas, rutRuta, alias);
    return rutRuta;
  }

}
