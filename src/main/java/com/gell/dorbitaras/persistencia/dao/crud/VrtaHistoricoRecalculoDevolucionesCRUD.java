/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.negocio.constante.EEstadoGenerico;
import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.VrtaHistoricoRecalculoDevoluciones;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.DateUtil;
import com.gell.estandar.util.LogUtil;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;

/**
 *
 * @author jonat
 */
public class VrtaHistoricoRecalculoDevolucionesCRUD extends GenericoCRUD {
    public VrtaHistoricoRecalculoDevolucionesCRUD(DataSource dataSource, AuditoriaDTO auditoria)
            throws PersistenciaExcepcion {
        super(PostgresBD.getConexion(dataSource), auditoria);
    }
    
    public void insertar(VrtaHistoricoRecalculoDevoluciones vrtaHistoricoRecalculoDevoluciones)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "INSERT INTO aseo.vrta_historico_recalculo_devoluciones (numero_actualizacion,"
                    + " varperreg_ide, periodo_hijo_ide, estado, vrta_observacion, vrta_fecgrabacion, vrta_feccertificacion)"
                    + " VALUES (?,?,?,?,?,?,?)";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
           /* Object vrtaHistoricoRecalculoIde = (vrtaHistoricoRecalculos.getVrtaHistoricoRecalculoIde() == null) ? null : vrtaHistoricoRecalculos.getVrtaHistoricoRecalculoIde();
            sentencia.setObject(i++, vrtaHistoricoRecalculoIde);*/
            Object numeroActualizacion = (vrtaHistoricoRecalculoDevoluciones.getNumeroActualizacion() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object varperregIde = (vrtaHistoricoRecalculoDevoluciones.getVarperregIde() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getVarperregIde();
            sentencia.setObject(i++, varperregIde);
            Object periodoHijoIde = (vrtaHistoricoRecalculoDevoluciones.getPeriodoHijoIde() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getPeriodoHijoIde();
            sentencia.setObject(i++, periodoHijoIde);
            Object estado = (vrtaHistoricoRecalculoDevoluciones.getEstado() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getEstado();
            sentencia.setObject(i++, estado);
            Object vrtaObservacion = (vrtaHistoricoRecalculoDevoluciones.getVrtaObservacion() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getVrtaObservacion();
            sentencia.setObject(i++, vrtaObservacion);
            Object vrtaFecgrabacion = (vrtaHistoricoRecalculoDevoluciones.getVrtaFecgrabacion() == null) ? null : DateUtil.parseTimestamp(vrtaHistoricoRecalculoDevoluciones.getVrtaFecgrabacion());
            sentencia.setObject(i++, vrtaFecgrabacion);
            Object vrtaFeccertificacion = (vrtaHistoricoRecalculoDevoluciones.getVrtaFeccertificacion()== null) ? null : DateUtil.parseTimestamp(vrtaHistoricoRecalculoDevoluciones.getVrtaFeccertificacion());
            sentencia.setObject(i++, vrtaFeccertificacion);
            sentencia.executeUpdate();
            /*ResultSet rs = sentencia.getGeneratedKeys();
            if (rs.next()) {
              vrtaVarterapr.setVrtaIderegistro(rs.getInt("vrta_ideregistro"));
            }*/
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
        } finally {
            desconectar(sentencia);
        }
    }
    
    public void editar(VrtaHistoricoRecalculoDevoluciones vrtaHistoricoRecalculoDevoluciones)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        try {
            int i = 1;
            String sql = "UPDATE aseo.vrta_historico_recalculo_devoluciones SET numero_actualizacion=?,varperreg_ide=?,periodo_hijo_ide=?";
            sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                       Object numeroActualizacion = (vrtaHistoricoRecalculoDevoluciones.getNumeroActualizacion() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getNumeroActualizacion();
            sentencia.setObject(i++, numeroActualizacion);
            Object varperregIde = (vrtaHistoricoRecalculoDevoluciones.getVarperregIde() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getVarperregIde();
            sentencia.setObject(i++, varperregIde);
            Object periodoHijoIde = (vrtaHistoricoRecalculoDevoluciones.getPeriodoHijoIde() == null) ? null : vrtaHistoricoRecalculoDevoluciones.getPeriodoHijoIde();
            sentencia.setObject(i++, periodoHijoIde);

            sentencia.executeUpdate();
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
        } finally {
            desconectar(sentencia);
        }
    }
    
    public Integer obtenerValorDeUltimaInsercionTabla(
            Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select max(vhrd.numero_actualizacion) as numero_actualizacion ")
                .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
                .append(" where vhrd.periodo_hijo_ide = :idPeriodo ");
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("numero_actualizacion", Integer.class, rs);
        }).get(0);
    }
    
    public Integer obtenerUltimoRecalculoAprobado(
            Integer idPeriodo)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        sql.append(" select max(vhrd.numero_actualizacion) as numero_actualizacion ")
                .append(" from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
                .append(" where vhrd.periodo_hijo_ide = :idPeriodo ")
                .append(" and vhrd.estado = 'A' ");
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("idPeriodo", idPeriodo);
        return ejecutarConsulta(sql, parametros, (ResultSet rs, Map<String, Integer> columns) -> {
            return getObject("numero_actualizacion", Integer.class, rs);
        }).get(0);
    }
    
    public void denegarRecalculo(
            Integer idArea,
            Integer idPeriodoPadre,
            Integer idPeriodo,
            Integer numeroActualizacion)
            throws PersistenciaExcepcion {
        StringBuilder sql = new StringBuilder();
        
        if (numeroActualizacion != null)  {
            sql.append("update aseo.varpr_varperreg set varpr_estado_registro = :inactivo ")
                .append("where varpr_estado = :certificado ")
                .append("and per_ideregistro in (:idPeriodoPadre, :idPeriodo) ")
                .append("and arpr_ideregistro = :idArea; ")
                .append("update aseo.varpr_varperreg set varpr_estado_registro = :activo ")
                .append("where varpr_ideregistro in ( ")
                .append("			select varperreg_ide  ")
                .append("			from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
                .append("			where periodo_hijo_ide in (:idPeriodo, :idPeriodoPadre)  ")
                .append("			and numero_actualizacion = ( ")
                .append("                           SELECT numero_actualizacion ")
                .append("                           FROM ASEO.vrta_historico_recalculo_devoluciones vhrd ")
                .append("                           where numero_actualizacion < :numeroActualizacion ")
                .append("                           and periodo_hijo_ide in (:idPeriodo, :idPeriodoPadre) ")
                .append("                           and estado = 'A' group by numero_actualizacion order by numero_actualizacion desc limit 1 ")
                .append("                       ); ");
            parametros.put("numeroActualizacion", numeroActualizacion);
        } else {
            sql.append("update aseo.varpr_varperreg set varpr_estado_registro = :inactivo ")
                .append("where varpr_estado = :certificado ")
                .append("and per_ideregistro in (:idPeriodoPadre, :idPeriodo) ")
                .append("and arpr_ideregistro = :idArea; ")
                .append("update aseo.varpr_varperreg set varpr_estado_registro = :activo ")
                .append("where varpr_ideregistro in ( ")
                .append("							select max(varpr_ideregistro) varpr_ideregistro from aseo.varpr_varperreg vv  ")
                .append("							where varpr_ideregistro not in ( ")
                .append("									select varperreg_ide  ")
                .append("									from aseo.vrta_historico_recalculo_devoluciones vhrd  ")
                .append("									where periodo_hijo_ide in (:idPeriodo, :idPeriodoPadre)	 ")
                .append("								) ")
                .append("							and per_ideregistro in (:idPeriodo, :idPeriodoPadre) ")
                .append("							and arpr_ideregistro = :idArea ")
                .append("							and varpr_estado = :certificado ")
                .append("							group by per_ideregistro, con_ideregistro ")
                .append("							); ");
        }
        parametros.put("idArea", idArea);
        parametros.put("idPeriodoPadre", idPeriodoPadre);
        parametros.put("idPeriodo", idPeriodo);
        parametros.put("certificado", EEstadoGenerico.CERTIFICADO);
        parametros.put("activo", EEstadoGenerico.ACTIVO);
        parametros.put("inactivo", EEstadoGenerico.INACTIVAR);
        
        ejecutarEdicion(sql, parametros);
    }

    public List<VrtaHistoricoRecalculoDevoluciones> consultar()
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        List<VrtaHistoricoRecalculoDevoluciones> lista = new ArrayList<>();
        try {

            String sql = "SELECT * FROM aseo.vrta_historico_recalculo_devoluciones";
            sentencia = cnn.prepareStatement(sql);
            ResultSet rs = sentencia.executeQuery();
            while (rs.next()) {
                lista.add(getVrtaHistoricoRecalculoDevoluciones(rs));
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return lista;
    }

    public VrtaHistoricoRecalculoDevoluciones consultar(long id)
            throws PersistenciaExcepcion {
        PreparedStatement sentencia = null;
        VrtaHistoricoRecalculoDevoluciones obj = null;
        try {

            String sql = "SELECT * FROM aseo.vrta_historico_recalculo_devoluciones WHERE vrta_historico_recalculo_devolucioneside=?";
            sentencia = cnn.prepareStatement(sql);
            sentencia.setLong(1, id);
            ResultSet rs = sentencia.executeQuery();
            if (rs.next()) {
                obj = getVrtaHistoricoRecalculoDevoluciones(rs);
            }
        } catch (SQLException e) {
            LogUtil.error(e);
            throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
        } finally {
            desconectar(sentencia);
        }
        return obj;
    }

    public static VrtaHistoricoRecalculoDevoluciones getVrtaHistoricoRecalculoDevoluciones(ResultSet rs)
            throws PersistenciaExcepcion {
        VrtaHistoricoRecalculoDevoluciones historicoRecalculos = new VrtaHistoricoRecalculoDevoluciones();
        historicoRecalculos.setNumeroActualizacion(getObject("numero_actualizacion", Integer.class, rs));
        historicoRecalculos.setVarperregIde(getObject("periodo_ejecucion", Integer.class, rs));
        historicoRecalculos.setPeriodoHijoIde(getObject("periodo_padre_calculo", Integer.class, rs));

        return historicoRecalculos;
    }
    
}
