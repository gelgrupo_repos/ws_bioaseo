package com.gell.dorbitaras.persistencia.dao.crud;

import com.gell.dorbitaras.persistencia.basedatos.PostgresBD;
import com.gell.dorbitaras.persistencia.constante.EMensajePersistencia;
import com.gell.dorbitaras.persistencia.entidades.FunFuncion;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import java.sql.Statement;
import java.util.Map;
import javax.sql.DataSource;
import com.gell.estandar.dto.AuditoriaDTO;
import com.gell.estandar.persistencia.excepcion.PersistenciaExcepcion;
import com.gell.estandar.util.LogUtil;
import com.gell.estandar.persistencia.abstracto.GenericoCRUD;
import java.sql.Connection;

public class FunFuncionCRUD extends GenericoCRUD
{

  public FunFuncionCRUD(DataSource dataSource, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(PostgresBD.getConexion(dataSource), auditoria);
  }

  public FunFuncionCRUD(Connection cnn, AuditoriaDTO auditoria)
          throws PersistenciaExcepcion
  {
    super(cnn, auditoria);
  }

  public void insertar(FunFuncion funFuncion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.fun_funcion(fun_descripcion,fun_ubicacion,fun_tipo,fun_ideregistro,fun_parametro,usu_ideregistro,fun_sql,fun_clase) VALUES (?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
      sentencia.setObject(i++, funFuncion.getFunDescripcion());
      sentencia.setObject(i++, funFuncion.getFunUbicacion());
      sentencia.setObject(i++, funFuncion.getFunTipo());
      sentencia.setObject(i++, funFuncion.getFunIderegistro());
      sentencia.setObject(i++, funFuncion.getFunParametro());
      sentencia.setObject(i++, funFuncion.getUsuIderegistro());
      sentencia.setObject(i++, funFuncion.getFunSql());
      sentencia.setObject(i++, funFuncion.getFunClase());

      sentencia.executeUpdate();
      ResultSet rs = sentencia.getGeneratedKeys();
      if (rs.next()) {
        funFuncion.setFunNombre(rs.getString("fun_nombre"));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void insertarTodos(FunFuncion funFuncion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "INSERT INTO aseo.fun_funcion(fun_nombre,fun_descripcion,fun_ubicacion,fun_tipo,fun_ideregistro,fun_parametro,usu_ideregistro,fun_sql,fun_clase) VALUES (?,?,?,?,?,?,?,?,?)";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, funFuncion.getFunNombre());
      sentencia.setObject(i++, funFuncion.getFunDescripcion());
      sentencia.setObject(i++, funFuncion.getFunUbicacion());
      sentencia.setObject(i++, funFuncion.getFunTipo());
      sentencia.setObject(i++, funFuncion.getFunIderegistro());
      sentencia.setObject(i++, funFuncion.getFunParametro());
      sentencia.setObject(i++, funFuncion.getUsuIderegistro());
      sentencia.setObject(i++, funFuncion.getFunSql());
      sentencia.setObject(i++, funFuncion.getFunClase());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_INSERTAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public void editar(FunFuncion funFuncion)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    try {
      int i = 1;
      String sql = "UPDATE aseo.fun_funcion SET fun_descripcion=?,fun_ubicacion=?,fun_tipo=?,fun_ideregistro=?,fun_parametro=?,usu_ideregistro=?,fun_sql=?,fun_clase=? where fun_nombre=? ";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setObject(i++, funFuncion.getFunDescripcion());
      sentencia.setObject(i++, funFuncion.getFunUbicacion());
      sentencia.setObject(i++, funFuncion.getFunTipo());
      sentencia.setObject(i++, funFuncion.getFunIderegistro());
      sentencia.setObject(i++, funFuncion.getFunParametro());
      sentencia.setObject(i++, funFuncion.getUsuIderegistro());
      sentencia.setObject(i++, funFuncion.getFunSql());
      sentencia.setObject(i++, funFuncion.getFunClase());
      sentencia.setObject(i++, funFuncion.getFunNombre());

      sentencia.executeUpdate();
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_EDITAR);
    } finally {
      desconectar(sentencia);
    }
  }

  public List<FunFuncion> consultar()
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    List<FunFuncion> lista = new ArrayList<>();
    try {

      String sql = "SELECT * FROM aseo.fun_funcion";
      sentencia = cnn.prepareStatement(sql);
      ResultSet rs = sentencia.executeQuery();
      while (rs.next()) {
        lista.add(getFunFuncion(rs));
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return lista;

  }

  public FunFuncion consultar(long id)
          throws PersistenciaExcepcion
  {
    PreparedStatement sentencia = null;
    FunFuncion obj = null;
    try {

      String sql = "SELECT * FROM aseo.fun_funcion WHERE fun_ideregistro = ?";
      sentencia = cnn.prepareStatement(sql);
      sentencia.setLong(1, id);
      ResultSet rs = sentencia.executeQuery();
      if (rs.next()) {
        obj = getFunFuncion(rs);
      }
    } catch (SQLException e) {
      LogUtil.error(e);
      throw new PersistenciaExcepcion(EMensajePersistencia.ERROR_CONSULTAR);
    } finally {
      desconectar(sentencia);
    }
    return obj;
  }

  public static FunFuncion getFunFuncion(ResultSet rs)
          throws PersistenciaExcepcion
  {
    FunFuncion funFuncion = new FunFuncion();
    funFuncion.setFunNombre(getObject("fun_nombre", String.class, rs));
    funFuncion.setFunDescripcion(getObject("fun_descripcion", String.class, rs));
    funFuncion.setFunUbicacion(getObject("fun_ubicacion", String.class, rs));
    funFuncion.setFunTipo(getObject("fun_tipo", String.class, rs));
    funFuncion.setFunIderegistro(getObject("fun_ideregistro", Integer.class, rs));
    funFuncion.setFunParametro(getObject("fun_parametro", Integer.class, rs));
    funFuncion.setUsuIderegistro(getObject("usu_ideregistro", Integer.class, rs));
    funFuncion.setFunSql(getObject("fun_sql", String.class, rs));
    funFuncion.setFunClase(getObject("fun_clase", String.class, rs));

    return funFuncion;
  }

  public static FunFuncion getFunFuncion(ResultSet rs, Map<String, Integer> columnas, String alias)
          throws PersistenciaExcepcion
  {
    FunFuncion funFuncion = new FunFuncion();
    Integer columna = columnas.get(alias + "_fun_nombre");
    if (columna != null) {
      funFuncion.setFunNombre(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_fun_descripcion");
    if (columna != null) {
      funFuncion.setFunDescripcion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_fun_ubicacion");
    if (columna != null) {
      funFuncion.setFunUbicacion(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_fun_tipo");
    if (columna != null) {
      funFuncion.setFunTipo(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_fun_ideregistro");
    if (columna != null) {
      funFuncion.setFunIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_fun_parametro");
    if (columna != null) {
      funFuncion.setFunParametro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_usu_ideregistro");
    if (columna != null) {
      funFuncion.setUsuIderegistro(getObject(columna, Integer.class, rs));
    }
    columna = columnas.get(alias + "_fun_sql");
    if (columna != null) {
      funFuncion.setFunSql(getObject(columna, String.class, rs));
    }
    columna = columnas.get(alias + "_fun_clase");
    if (columna != null) {
      funFuncion.setFunClase(getObject(columna, String.class, rs));
    }
    return funFuncion;
  }

}
